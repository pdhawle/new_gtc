<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNextInspectors"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsInspectors = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
</head>
<body>
<form name="reportList" method="post" action="InspActResults.asp" >
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Inspector Activity</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												Select inspector to run activity report on.<br>
												<select name="inspector" onChange="return dept_onchange(projectList)">
													<option value="all">--all inspectors--</option>
													<%do until rsInspectors.eof
														if trim(rsInspectors("userID")) = trim(inspector) then%>
															<option selected="selected" value="<%=rsInspectors("userID")%>"><%=rsInspectors("firstName") & " " & rsInspectors("lastName")%></option>
														<%else%>
															<option value="<%=rsInspectors("userID")%>"><%=rsInspectors("firstName") & " " & rsInspectors("lastName")%></option>
														<%end if%>
													<%rsInspectors.movenext
													loop%>
												</select>&nbsp;&nbsp;&nbsp;&nbsp;
												
												From: <input type="text" name="fromDate" maxlength="10" size="10" value="<%=formatdatetime(dateadd("d", -7,now()),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('fromDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a> &nbsp;&nbsp;&nbsp;&nbsp;    
												To: <input type="text" name="toDate" maxlength="10" size="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('toDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a> &nbsp;&nbsp;&nbsp;&nbsp;
			
												<!--Sort By: <select name="sortBy">
													<option value="customerName">Customer</option>
													<option value="projectName">Project</option>
													<option value="lastName">Inspector</option>
													<option value="inspectionDate">Inspection Date</option>
													<option value="reportType">Report Type</option>
												</select>&nbsp;&nbsp;&nbsp;&nbsp;-->
												
												<input type="submit" value=" Select " class="formButton"> 
											</td>
										</tr>
									</table>
		
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rsCustomer.close
DataConn.close
Set rsCustomer = nothing
Set DataConn = nothing
Set oCmd = nothing
%>