<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iUser = request("inspector")
dtFrom = request("fromDate")
dtTo = request("toDate")
'sSort = request("sortBy")

ssort = request("sort")
if ssort = "" then
	columnName = "lastName"
else
	columnName = ssort
end if


If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
if iUser = "all" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetInspectorActivityAllSort"
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
	   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
	
else
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetInspectorActivitySort" 'specific project
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, iUser)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
	   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
end if

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Inspector Activity</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expInspActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="chooseInspAct.asp" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr><td></td><td colspan="5">Date Range: <strong><%=dtFrom%> - <%=dtTo%></strong></td></tr>
										<tr bgcolor="#666666" height="35">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText"><a href="InspActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&sort=reportID" style="color:#721A32;text-decoration:underline;font-weight:bold;">Report ID</a></span></td>
											<td><span class="searchText">Customer</span></td>
											<td><span class="searchText"><a href="InspActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&sort=projectName" style="color:#721A32;text-decoration:underline;font-weight:bold;">Project</a></span></td>
											<td><span class="searchText"><a href="InspActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&sort=lastName" style="color:#721A32;text-decoration:underline;font-weight:bold;">Inspector</a></span></td>
											<td><span class="searchText"><a href="InspActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&sort=reportType" style="color:#721A32;text-decoration:underline;font-weight:bold;">Report Type</a></span></td>
											<td><span class="searchText"><a href="InspActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&sort=inspectionDate" style="color:#721A32;text-decoration:underline;font-weight:bold;">Inspection Date</a></span></td>
											<td><span class="searchText"><a href="InspActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&sort=dateAdded" style="color:#721A32;text-decoration:underline;font-weight:bold;">Inspection Date Entered</a></span></td>
											<td><span class="searchText">Inspection Timestamped</span></td>
										</tr>
								
										<%If rsReport.eof then %>
												<tr><td></td><td colspan="9">there are no reports to display</td></tr>
											<%else
												blnChange = false
												dim i
												i = 0
												Do until rsReport.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td><a href="activityLog.asp?reportID=<%=rsReport("reportID")%>" title="" rel="gb_page_fs[]"><%=rsReport("reportID")%></a></td>
														<td><%=rsReport("customerName")%></td>
														<td><%=rsReport("projectName")%></td>
														<td><%=rsReport("firstName") & " " & rsReport("lastName")%></td>
														<td><%=rsReport("reportType")%></td>
														<td><%=rsReport("inspectionDate")%></td>
														<td><%=FormatDateTime(rsReport("dateAdded"),2)%></td>
														<td><%=FormatDateTime(rsReport("dateAdded"),3)%></td>
													</tr>
												<%
												
												rsReport.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												i = i + 1
												loop%>
												
												<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td colspan="9" align="right"><strong>Total Reports:&nbsp;<%=i%>&nbsp;&nbsp;&nbsp;</strong></td>
												</tr>
												
											<%end if%>
										
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>