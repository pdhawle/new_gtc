<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

reportID = Request("reportID")
sType = request("type")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOSHASubsByID"
	.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<form name="openItems" method="post" action="process.asp">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<tr>
		<td colspan="3" class="colorBars">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="15">
												<b><u>Entire report</u></b><br>
												<%if sType = 1 then%>
													<a href="downloads/Report_<%=reportID%>Ent.pdf" target="_blank">Full unbroken report</a><br>
													<a href="downloads/Report_<%=reportID%>.pdf" target="_blank">Full report broken into separate pages</a><br><br>
												<%else%>
													<a href="downloads/Report_<%=reportID%>Ent.pdf" target="_blank">Full report</a><br><br>
												<%end if%>
												
												<b><u>Sub-Contractor Reports</u></b><br>
												<%if rs.eof then
													response.Write "There are no subcontractors listed on this report"
												else
													Do until rs.eof%>
														<a href="downloads/Report_<%=reportID%>_<%=rs("subID")%>.pdf" target="_blank"><%=rs("subName")%></a><br>
													<%rs.movenext
													loop
												end if%>
												
												<strong><%'=rs("headline")%></strong><br>
												<em><%'=rs("dateAdded")%></em><br><br>
												<%'=rs("story")%>
												
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#000000">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>