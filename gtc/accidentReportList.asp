<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
view = request("view")
msg = request("msg")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

sSeeAll = False

If Session("empType") <> 0 then
If Session("empType") <> 2 then
If not isNull(Session("empType")) then
	sSeeAll = True	
end if
end if
end if

If sSeeAll = True then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	if view = "ar" then
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetApprovedAccidentReports"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
		   .CommandType = adCmdStoredProc   
		End With
	else	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAccidentReports"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
		   .CommandType = adCmdStoredProc   
		End With
	end if
else
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAccidentReportsByUser"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
end if
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this project?")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Accident Report List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addAccidentReport&clientID=<%=clientID%>" class="footerLink">add accident report</a>&nbsp;&nbsp;
						<%if view = "ar" then%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="accidentReportList.asp?clientID=<%=clientID%>" class="footerLink">view accident reports</a>&nbsp;&nbsp;
							
						<%else%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="accidentReportList.asp?clientID=<%=clientID%>&view=ar" class="footerLink">view approved accident reports</a>&nbsp;&nbsp;
						<%end if%>
						<!--<span class="footerLink">|</span>&nbsp;&nbsp;<a href="divisionList.asp?ID=<%'=customerID%>" class="footerLink">division list</a>	-->		
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="10"><font color="#EE0000"><%=msg%></font></td>
										</tr>
										<tr>
											<td colspan="10">
											Notes: Once a report is added or edited, you will need to click the "send" link to send to general superintendent.<br>
											General Superintendent will need to edit, approve or deny the report.
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Accident ReportID</span></td>
											<td><span class="searchText">Created By</span></td>
											<td><span class="searchText">Date Created</span></td>
											<td><span class="searchText">Accident Type</span></td>			
											<td align="center"><span class="searchText">Action</span></td>
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="10">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("accidentID")%></td>
													<!--<td>
														<%'If isNull(rs("projectName")) then
															'response.Write "N/A"
														'else
														'	response.Write rs("projectName")
														'end if%>
													</td>-->
													<td><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></td>
													<td><%=formatDateTime(rs("dateEntered"),2)%></td>
													<td><%=rs("accidentType")%></td>
													<td align="center">
														<a href="downloads/accidentReport_<%=rs("accidentID")%>.pdf" target="_blank">view</a>&nbsp;&nbsp;|&nbsp;
														<a href="process.asp?processType=sendAccidentReport&clientID=<%=clientID%>&accidentID=<%=rs("accidentID")%>&userID=<%=rs("userID")%>&accidentTypeID=<%=rs("accidentTypeID")%>&generalSuper=<%=rs("generalSuper")%>">send</a>&nbsp;&nbsp;|&nbsp;
														<a href="form.asp?formType=editAccidentReport&clientID=<%=clientID%>&accidentID=<%=rs("accidentID")%>">edit</a>
														<%If Session("empType") <> 0 then
															If Session("empType") <> 2 then
															If not isNull(Session("empType")) then%>&nbsp;&nbsp;|&nbsp;
																<a href="process.asp?processType=approveAccidentReport&clientID=<%=clientID%>&accidentID=<%=rs("accidentID")%>&userID=<%=rs("userID")%>">approve</a>&nbsp;&nbsp;|&nbsp;
																<a href="process.asp?processType=denyAccidentReport&clientID=<%=clientID%>&accidentID=<%=rs("accidentID")%>&userID=<%=rs("userID")%>">deny</a>&nbsp;&nbsp;|&nbsp;
																<a href="uploadImageAccident.asp?accidentID=<%=rs("accidentID")%>"  title="" rel="gb_page_fs[]">upload/view photo(s)</a>
														<%end if
														end if
														end if%>
													</td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>