<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
projectID = trim(Request("project"))



Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'may need to add user to this because may want to be only for specific user
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetContactsByProject"
   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script>
<!-- Begin
var checkflag = "false";
function check(field)
{
 var i;
 if (eval(field[0].checked))
 {
  for (i=0;i<field.length;i++)
    field[i].checked=true;
  LL(field); 
  //return "Uncheck All";
 } 
 else
 { 
   for(i=0;i<field.length;i++)
     field[i].checked=false;
   UU(field); 
   return "Check All";
 } 
}
function LL(field){field.disabled=true;}
function UU(field){field.disabled=false;}


function getCheckValuesS(checkBoxArray, separator){
var tempString = ""
var count = 0
for (i = 0; i < checkBoxArray.length; i++){
   if (checkBoxArray[i].checked){
      if (count > 0) tempString += separator   //don't put the separator before the first value
      tempString += checkBoxArray[i].value
      count++
      }
   }
return tempString
}


function showCheckValuesString(formRef){
var rValue = getCheckValuesS(formRef.addEmail, ";")           //use the newline character as a separator.
//if (rValue == "")                                               //empty string means nothing was checked
 //  alert("No check boxes were checked!")
//else
  // alert(rValue)   //mention the newline character and display
 self.opener.document.forms[0].emailTo.value = ''
 self.opener.document.forms[0].emailTo.value = rValue 
 window.close();
}
//  End -->
</script>
</head>
<body>
<form action="" method="get" name="frmAddress">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Address Book</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addContact&id=<%=projectID%>&f=ab" class="footerLink">add contact</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr bgcolor="#666666">
											<td>&nbsp;&nbsp;<span class="searchText">Contact Name</span></td>
											<td><span class="searchText">Email Address</span></td>
											<td><span class="searchText">Company Name</span></td>					
											<td><span class="searchText"><!--<INPUT type="checkbox" name="addEmail"  value="Check All" onClick="this.value=check(this.form.addEmail);">--> Select</span></td>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td colspan="9">there are no records to display</td></tr>
										<%else%>
											<%blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td>&nbsp;&nbsp;<%=rs("firstName")%>&nbsp;<%=rs("lastName")%></td>							
													<td><a href="mailto:<%=rs("email")%>"><%=rs("email")%></a></td>
													<td><%=rs("companyName")%></td>
													<td><input type="checkbox" name="addEmail" value="<%=rs("email")%>" ></td>
												</tr>
											<%
											'get the values from all the checkboxes
											sAllVal = sAllVal & rs("contactID")
											
											rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="9" align="right">
												<input type="button" name="Select" value="Select" class="formButton" onClick="showCheckValuesString(this.form)">
												  &nbsp;
												  <input type="button" value="Cancel" onClick="window.close();" class="formButton" />
											</td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>