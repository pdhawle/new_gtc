<!--#include file="includes/constants.inc"-->
<%
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Administrative Reports</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												<a href="chooseAgent.asp"><b>Agent Pay Report</b></a>  - Select and view the pay rate for a given agent/inspector and project.<br>
												<a href="chooseAgent2.asp"><b>Agent Pay Report 2 Test</b></a>  - Select and view the pay rate for a given agent/inspector and project.<br>
												<a href="chooseAgentRevenue.asp"><b>Agent Revenue Report</b></a>  - Select and view the revenue and pay rate for a given agent/inspector and project.<br>
												<a href="chooseBillRec.asp?clientID=<%=session("clientID")%>"><b>Billing Reconciliation Report (Inspections)</b></a>  - Select and view the billing within a given date range for inspection work.<br>
												<a href="chooseBillRecSoft.asp?clientID=<%=session("clientID")%>"><b>Billing Reconciliation Report (Software)</b></a>  - Select and view the billing within a given date range for software work.<br>
												<a href="customerListAdmin.asp?clientID=<%=session("clientID")%>"><b>Customer List</b></a>  - View customers with assigned area and account managers.<br>
												<a href="ChooseECPerf.asp"><b>Erosion Control Performance</b></a>  - Select and view the performance of EC contractors based on the inspector involved.<br>
												<a href="ChooseInspAct.asp"><b>Inspector Activity Report</b></a>  - Select and view the inspection reports and when they were generated.<br>
												<a href="ChooseInspRec.asp"><b>Inspector Reconciliation Report</b></a>  - Select and view the number and type of inspection reports for an inspector.<br>
												<a href="ChooseJournalAct.asp"><b>Journal Activity</b></a>  - Select and view the journal entries for a user within date range.<br>
												<a href="ChooseJournalActProj.asp"><b>Journal Activity By Project</b></a>  - Select and view the journal entries for a project within date range.<br>
												<a href="chooseMonth.asp"><b>Lot Status and Rate Report</b></a>  - Select and view the lot rate and status for a given month.<br>
												<a href="chooseMileage.asp?clientID=<%=session("clientID")%>"><b>Mileage Report</b></a>  - Select and view the mileage for a given user.<br>
												<a href="chooseNewStart.asp?clientID=<%=session("clientID")%>"><b>New Start Report</b></a>  - Select and view the projects starting within a given date range.<br>
												<a href="projectAlerts.asp?clientID=<%=session("clientID")%>"><b>Project Alerts</b></a>  - Gives a snapshot of all active projects and which alerts are selected.<br>
												<a href="listProjects.asp?s=All"><b>Project Lists</b></a>  - Select and view the list of projects in the database.<br>
												<a href="listProjectLocations.asp?s=All"><b>Project Location List</b></a>  - View the list of projects and their locations.<br>
												<a href="chooseStabil.asp?clientID=<%=session("clientID")%>"><b>Project Stabilization Report</b></a> - Select and view project stabilization data per project or time period.<br>
												
												<a href="chooseRecProj.asp"><b>Reconciliation Report</b></a>   - Select and view a count of reports within a give date range.<br>
												<a href="chooseSiteAssignment.asp?clientID=<%=session("clientID")%>"><b>Site Assignment Report</b></a>  - Select and view the reports completedby user within a given date range.<br>
												<a href="chooseStop.asp?clientID=<%=session("clientID")%>"><b>Stop Report</b></a>  - Select and view the projects ending within a given date range.<br>
												<a href="chooseTimesheet.asp?clientID=<%=session("clientID")%>"><b>Timesheet Report</b></a>  - Select and view the timesheet for a given user.<br>
												<a href="chooseUserLog.asp?clientID=<%=session("clientID")%>"><b>User Log</b></a>  - A list of users who logged in and and when.<br>
												<%if session("clientID") = 1 then%>												 
													<a href="ChooseWeekProjSumm.asp?clientID=<%=session("clientID")%>"><b>Weekly Project Summary Report</b></a>  - Select and view the weekly project summaries within a given date ranges.<br>
												<%end if%>
												<a href="chooseRecWO.asp?clientID=<%=session("clientID")%>"><b>Work Order Reconciliation Report</b></a>  - Select and view the work orders.<br>

												
												<!--<a href="billRecResults.asp?clientID=<%'=session("clientID")%>"><b>Billing Reconciliation Report</b></a>  - Select and view the billing within a given month.<br>-->
												
												
												
												
												
												
												
												
												
												<%'if session("clientID") = 1 then%>												 
													<!--<a href="cwMatthewsProjects.asp?clientID=<%'=session("clientID")%>"><b>CW Matthews Project List</b></a>  - List of projects and last report date for CW Matthews projects.<br>-->
												<%'end if%>
												
												
												<!--<a href="">Query Closed Action Items</a>  Select and view reports with resolved <em>Action Required</em> items.<br>
												<a href="">Archive Reports</a>  Select reports to download for archival purposes.  <br>-->
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>