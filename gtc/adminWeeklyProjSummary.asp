<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
projectID = trim(Request("projectID"))
dtFrom = request("fromDate")
dtTo = request("toDate")

dtFromOrig = dtFrom
dtToOrig = dtTo

'dtFrom = DateAdd("d", -1, dtFrom)
dtTo = DateAdd("d", 1, dtTo)


Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

If projectID = "all" then

	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWeeklySummaryListByDateAll"
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
else

	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWeeklySummaryListByDate"
	   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectID)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
end if
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Weekly Project Summary Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expWeeklyProjSummary.asp?projectID=<%=projectID%>&toDate=<%=dtTo%>&fromDate=<%=dtFrom%>" class="footerLink">export to excel</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="30"></td></tr>
										<tr><td colspan="6">Date Range: <strong><%=dtFromOrig%> - <%=dtToOrig%></strong></td></tr>
										<tr bgcolor="#666666">												
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td valign="top"><span class="searchText">Customer</span></td>
											<td width="10"></td>
											<td valign="top"><span class="searchText">Project</span></td>
											<td width="10"></td>
											<td valign="top"><span class="searchText">Entered By</span></td>
											<td width="10"></td>
											<td valign="top" width="100"><span class="searchText">Date Entered</span></td>
											<td width="10"></td>
											<td valign="top"><span class="searchText">Summary Entry</span></td>
											<td width="20"></td>							
										</tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="9">there are no records to display</td></tr>
										<%else
											blnChange = false
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td valign="top"><%=rs("customerName")%></td>
													<td></td>
													<td valign="top"><%=rs("projectName")%></td>
													<td></td>
													<td valign="top"><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></td>
													<td></td>
													<td valign="top"><%=formatdatetime(rs("dateEntered"),2)%></td>
													<td></td>
													<td valign="top"><%=rs("summary")%></td>	
													<td></td>						
													<!--<td align="center"><a href="form.asp?id=<%'=rs("contactID")%>&formType=editContact&projectID=<%'=projectID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a></td>
													<td align="center"><a href=""><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%'=rs("contactID")%>&processType=deleteContact&projectID=<%'=projectID%>')"></a></td>-->
												</tr>
												<tr><td colspan="8" height="10"></td></tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>