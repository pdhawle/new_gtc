<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

clientID = Request("clientID")
quoteID = Request("quoteID")
red = request("red")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAreaManagers"
	.parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rs = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetQuote"
   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsQuote = oCmd.Execute
Set oCmd = nothing

%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript" src="includes/jquery.js"></script>
</head>
<body>
<form name="openItems" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Suggest Area Manager/Customers</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="6">
												<strong>Project:</strong>&nbsp;<%=rsQuote("projectName")%>
											</td>
										</tr>
										<tr><td colspan="6" height="5"></td></tr>
										<tr>
											<td colspan="6">
												Please select any customer that will beed to be quoted on this project.<br>
												For each area manager, select "show" and then check all that apply.<br>
												An email will be sent to the area manager with a list of customers that you suggest need to be quoted for this project. You will recieve a copy of the email(s) for your records.
											</td>
										</tr>
										<tr><td colspan="6" height="5"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Area Manager</span></td>
											<td width="5"></td>
											<td><span class="searchText">Email</span></td>
											<td width="5"></td>
											<td width="5"></td>
										</tr>
										<tr><td colspan="6"></td><td><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="6">there are no records to display</td></tr>
										<%else
											blnChange = true
											i = 1
											Do until rs.eof%>
												<style>
													#myform<%=i%> {
													  padding: 0px;
													  background-color: #FFFFFF;
													  /*width: 600px;*/
													}
													
													#myform<%=i%> label {
													  font-size: 11px;
													}
													 													
													#custList<%=i%> {
													  margin: 0px 0px 0px 0px;
													  display: none;
													}
												</style>
												<div id="myform<%=i%>">
												<%If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><strong><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></strong></td>
													<td></td>
													<td>
														<a href="mailto:<%=rs("email")%>"><%=rs("email")%></a>
													</td>
													<td></td>
													<td align="right">
														<input type="radio" name="showCustomers<%=i%>" onclick="javascript: $('#custList<%=i%>').show('fast');"  />&nbsp;Show&nbsp;&nbsp;
														<input checked="checked" type="radio" name="showCustomers<%=i%>" onclick="javascript: $('#custList<%=i%>').hide('fast');"  />&nbsp;Hide&nbsp;&nbsp;
													</td>
												</tr>
												
												
												<%If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td></td>
													<td colspan="5">
														<br>
														<div id="custList<%=i%>">
															<%
															Set oCmd = Server.CreateObject("ADODB.Command")	
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetAreaManagerCustomers"
																.parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, rs("userID"))
															   .CommandType = adCmdStoredProc
															   
															End With
																
															Set rsCustomer = oCmd.Execute
															Set oCmd = nothing%>
															
															<table width="100%" cellpadding="0" cellspacing="0">
																<tr bgcolor="#666666">
																	<td><img src="images/pix.gif" width="5" height="1"></td>
																	<td><span class="searchText">Customer</span></td>
																	<td><span class="searchText">Address</span></td>
																	<td><span class="searchText">City</span></td>
																	<td><span class="searchText">State</span></td>
																	<td><span class="searchText">County</span></td>
																</tr>
															
															
																<%do until rsCustomer.eof%>
																	<tr>
																		<td></td>
																		<td><input type="checkbox" name="customer<%=i%>" value="<%=rsCustomer("customerID")%>">&nbsp;<%=rsCustomer("customerName")%></td>
																		<td><%=rsCustomer("address1")%></td>
																		<td><%=rsCustomer("city")%></td>
																		<td><%=rsCustomer("state")%></td>
																		<td><%=rsCustomer("county")%></td>															
																	</tr>
																<%sAllVal = sAllVal & rsCustomer("customerID")
																rsCustomer.movenext
																if not rsCustomer.eof then
																	sAllVal = sAllVal & ","
																end if
																loop%>
															</table>
															<input type="hidden" name="allBoxes<%=i%>" value="<%=sAllVal%>">
															<input type="hidden" name="email<%=i%>" value="<%=rs("email")%>">
														</div><br><br>
													</td>
												</tr>
												
												</div>
											<%
											'get the values from all the checkboxes
											'sAllVal = sAllVal & rs("responsiveActionID")
											
											rs.movenext
											
											if not rs.eof then
												i = i + 1
											end if
											
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											
											
											
											loop
										end if%>
										<tr>
											<td colspan="6" align="right">
												<input type="hidden" name="sEmail" value="<%=session("email")%>">
												<input type="hidden" name="red" value="<%=red%>">
												<input type="hidden" name="numManagers" value="<%=i%>">
												<input type="hidden" name="quoteID" value="<%=quoteID%>">
												<input type="hidden" name="clientID" value="<%=clientID%>">
												<input type="hidden" name="processType" value="suggestCustomers" />
												<br><input type="submit" value="Update" class="formButton"/>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>