<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
classID = request("classID")
customerID = request("customerID")
clientID = request("clientID")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
'	Set oCmd = Server.CreateObject("ADODB.Command")
		
'	With oCmd
'	   .ActiveConnection = DataConn
'	   .CommandText = "spGetStudents"
'	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
'	   .CommandType = adCmdStoredProc   
'	End With
'				
'	Set rs = oCmd.Execute
'	Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing

if customerID <> "0" then
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAvailableStudents"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, classID)
	   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsAvailStudents = oCmd.Execute
	Set oCmd = nothing
else
	
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAllAvailableStudents"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, classID)
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsAvailStudents = oCmd.Execute
	Set oCmd = nothing
	
end if

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStudentClass"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, classID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsStudentClass = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClass"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, classID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsClass = oCmd.Execute
Set oCmd = nothing
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<link rel="stylesheet" type="text/css" href="menu/popupmenu.css" />
<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/popupmenu.js"></script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Assign/Manage Students</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addStudent&customerID=<%=customerID%>&sList=assign&classID=<%=classID%>" class="footerLink">add student</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="classList.asp?customerID=<%=customerID%>" class="footerLink">class list</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="9">
												<%if customerID <> "0" then%>
												<!--#include file="custDropdown.asp"-->
												<%end if%>
												<strong>Class: </strong><%=rsClass("classType")%><br><br>
												
											</td>
										</tr>
										
										<tr>
											<td colspan="9">
											<form name="assignStudent" method="post" action="process.asp">
												<b>Assign students to this class</b><br><br>
												<%if rsAvailStudents.eof then%>
													There are no students to assign to this class.<br>
													Please click the link above to add students to this customer.
												<%else
												do until rsAvailStudents.eof%>
													<input type="checkbox" name="assignStudent" value="<%=rsAvailStudents("studentID")%>">&nbsp;<%=rsAvailStudents("firstName")%>&nbsp;<%=rsAvailStudents("lastName")%><br>
												<%
												sAllVal = sAllVal & rsAvailStudents("studentID")
												rsAvailStudents.movenext
												if not rs.eof then
													sAllVal = sAllVal & ","
												end if
												loop
												rsAvailStudents.moveFirst%>
												<br />
												<input type="hidden" name="classID" value="<%=classID%>" />
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="allBoxes" value="<%=sAllVal%>">
												<input type="hidden" name="processType" value="assignStudents" /><!-- onClick="selectAll(document.assignStudent.students);"-->
												<input type="submit" value="  Assign  " class="formButton"/>
												<%end if%>
											</form>
											</td>
										</tr>
										<tr><td colspan="5" height="10"></td></tr>
										<tr>
											<td colspan="9">
												<b>Assigned Students</b>												
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Student Name</span></td>
											<td><span class="searchText">Email</span></td>
											<td align="center"><span class="searchText">Attend</span></td>
											<td align="center"><span class="searchText">Remove</span></td>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rsStudentClass.eof then%>
											<tr><td></td><td colspan="7">there are no students assigned to this class</td></tr>
										<%else
											blnChange = true
											Do until rsStudentClass.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rsStudentClass("firstName")%>&nbsp;<%=rsStudentClass("lastName")%></td>
													<td><a href="mailto:<%=rsStudentClass("email")%>"><%=rsStudentClass("email")%></a></td>
													<td align="center" valign="middle">
														<%if rsStudentClass("attended") = "False" then%>
															<a href="process.asp?processType=attendClass&studentID=<%=rsStudentClass("studentID")%>&classID=<%=classID%>&customerID=<%=customerID%>&clientID=<%=clientID%>&sType=true"><img src="images/checkbox.gif" border="0"></a>
														<%else%>
															<a href="process.asp?processType=attendClass&studentID=<%=rsStudentClass("studentID")%>&classID=<%=classID%>&customerID=<%=customerID%>&clientID=<%=clientID%>&sType=false"><img src="images/checked.gif" border="0"></a>
														<%end if%>
													</td>
													<td align="center" valign="middle">
														<a href="process.asp?processType=removeStudent&studentID=<%=rsStudentClass("studentID")%>&classID=<%=classID%>&customerID=<%=customerID%>&clientID=<%=clientID%>"><img src="images/remove.gif" border="0"></a>
													</td>
												</tr>
											<%rsStudentClass.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rsStudentClass.close
DataConn.close
Set rsStudentClass = nothing
Set DataConn = nothing
Set oCmd = nothing
%>