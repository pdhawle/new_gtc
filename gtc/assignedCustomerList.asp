<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

clientID = request("clientID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedQuoteCustomers"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Assigned Customers List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expAssignedCustomerList.asp?clientID=<%=clientID%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="quoteList.asp?clientID=<%=clientID%>" class="footerLink">quote list</a>	
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<!--<tr>
											<td colspan="10">
												<span class="activeCustomer">Green</span> = Active Customer<br>
												<span class="inActiveCustomer">Red</span> = Inactive Customer
											</td>
										</tr>-->
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Customer Name</span></td>
											<td width="3"></td>
											<td><span class="searchText">Address</span></td>
											<td width="3"></td>
											<td><span class="searchText">City, State, Zip</span></td>
											<td width="3"></td>
											<td><span class="searchText">County</span></td>
											<td width="3"></td>
											<td><span class="searchText">Phone</span></td>
											<td width="3"></td>
											<td><span class="searchText">Primary Contact</span></td>
											<td width="3"></td>
											<td><span class="searchText">Account Manager</span></td>
											<td width="3"></td>
											<td><span class="searchText">Area Manager</span></td>
											<td width="3"></td>
											<td align="center"><span class="searchText">Active</span></td>
											<td width="3"></td>
											<td align="center"><span class="searchText">Quotes</span></td>
										</tr>
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td colspan="20">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td></td>
													<td>
														<a href="form.asp?id=<%=rs("customerID")%>&formType=editCustomer" title="Edit Customer" rel="gb_page_fs[]"><%=rs("customerName")%></a>
													</td>
													<td width="3"></td>
													<td>
														<%=rs("address1")%>
													</td>
													<td width="3"></td>
													<td>
														<%=rs("city")%>,&nbsp;<%=rs("state")%>&nbsp;<%=rs("zip")%>
													</td>
													<td width="3"></td>
													<td>
														<%=rs("county")%>
													</td>
													<td width="3"></td>
													<td>
														<%=rs("phone")%>
													</td>
													<td width="3"></td>
													<td>
														<%=rs("contactName")%>
													</td>
													<td width="3"></td>
													<td>
														<%=rs("accountManagerFirst")%>&nbsp;<%=rs("accountManagerLast")%>
													</td>
													<td width="3"></td>
													<td>
														<%=rs("areaManagerFirst")%>&nbsp;<%=rs("areaManagerLast")%>
													</td>
													<td width="3"></td>
													<td align="center">
														<%if rs("isActive") = "True" then%>
															<img src="images/check.gif">
														<%end if%>
													</td>
													<td width="3"></td>
													<td align="center"><a href="customerQuoteListLight.asp?id=<%=rs("customerID")%>" title="Customer Quotes" rel="gb_page_fs[]">List Quotes</a></td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>