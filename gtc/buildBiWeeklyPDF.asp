<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetBiWeeklyReport"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsBWReport = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top colspan="2">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle">
						<%if rsReport("logo") = "" then%>
							<img src="images/logo.jpg">
						<%else%>
							<img src="<%=rsReport("logo")%>">
						<%end if%>
					</td>
					<td valign="middle" align="right"><span class="Header">Report #<%=reportID%></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td valign="top">
						<b><%=rsBWReport("districtOffice")%></b><br>
						<b><%=rsBWReport("EPDDivision")%></b><br>
						<b><%=rsBWReport("address")%></b><br>
						<b><%=rsBWReport("city")%>, <%=rsBWReport("state")%>&nbsp;<%=rsBWReport("zip")%></b><br>
					
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr align="center">
					<td valign="top" align="center">
						<strong>NPDES Summary of Violations</strong><br>
						<strong><em>Per compliance with permit No. GAR 100003 part V.A.2</em></strong>					
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td valign="top">						
						Report Covering:&nbsp;&nbsp;<u><%=rsBWReport("reportCovering")%></u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Month/Year)<br>
						Project:&nbsp;&nbsp;<u><%=rsBWReport("projectName")%></u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;County: &nbsp;&nbsp;<u><%=rsBWReport("county")%></u><br><br>
						
						Please find enclosed the required inspection reports for the subject project, which began prior to August 1, 2008. This information includes 
						weekly and post rainfall inspections. The scope of the inspection, names of the qualified persons making the inspections, major observations, 
						and actions taken are included on the individual report forms, attached.<br><br>
						
						An assessment has been made of the following:<br>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
							<tr>
								<td valign="top"><strong>�</strong>&nbsp;</td>
								<td>
									Streams, stream buffer areas, wetlands?
								</td>
							</tr>
							<tr>
								<td valign="top">&nbsp;</td>
								<td>
									Observation: <u><%=rsBWReport("observation1")%></u>
								</td>
							</tr>
							<tr>
								<td valign="top">&nbsp;</td>
								<td>
									Action Taken: <u><%=rsBWReport("actionTaken1")%></u>
								</td>
							</tr>
							<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
							<tr>
								<td valign="top"><strong>�</strong>&nbsp;</td>
								<td>
									Are erosion control measures effective in preventing significant impacts to receiving water(s)?
								</td>
							</tr>
							<tr>
								<td valign="top">&nbsp;</td>
								<td>
									Observation: <u><%=rsBWReport("observation2")%></u>
								</td>
							</tr>
							<tr>
								<td valign="top">&nbsp;</td>
								<td>
									Action Taken: <u><%=rsBWReport("actionTaken2")%></u>
								</td>
							</tr>
						</table><br>
						
						<em>Builder or Land Development Manager to sign the following certification:</em><br><br>
						
						I certify under penalty of law that this document and all attachments were prepared under my direction 
						or supervision in accordance with a system designed to assure that qualified personnel properly gather and 
						evaluate the information submitted. Based on my inquiry of the person or persons who manage the system, 
						or those persons directly responsible for gathering the information, the information submitted is, to the best 
						of my knowledge and belief, true, accurate and complete. I am aware that there are significant penalties for 
						submitting false information, including the possibility of fine and imprisonment for knowing violations.<br><br>
						
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<!--<%'if rsReport("sigFileName") <> "" then%> 
										<img src="userUploads/<%'=rsReport("userID")%>/<%'=rsReport("sigFileName")%>">&nbsp;&nbsp;&nbsp;&nbsp;
									<%'else%>-->
									______________________________
									<%'end if%>
								</td>
								<td valign="bottom">__________________<!--<u><%'=rsReport("jobTitle")%></u>-->&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td valign="bottom">__________________</td>
							</tr>
							<tr>
								<td align="center">Name</td>
								<td align="center">Title</td>
								<td align="center">Date</td>
							</tr>
						</table><br>
						
						<em>If <strong>no</strong> incidents of non-compliance occurred on site this month, the Builder or Land Development Manager 
						should sign the following:</em><br><br>
						
						I certify that there were no incidents of non-compliance and the facility is in compliance with the 
						Erosion, Sedimentation and Pollution Control Plan and Permit No. GAR 100003 Part V.A.2.<br><br>
						
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>_______________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td>_____________________</td>
							</tr>
							<tr>
								<td>Name, Title</td>
								<td align="center">Date</td>
							</tr>
						</table><br>
						
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">Enclosures:</td>
								<td>
									1. Weekly Inspection Forms<br>
									2. Post Rainfall Inspection Forms
								</td>
							</tr>
						</table>
						
						
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
</table>
</body>
</html>
