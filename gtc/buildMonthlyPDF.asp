<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetMonthlyRainReport"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
			
Set rsMonthly = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("projectID"))
   .CommandType = adCmdStoredProc   
End With
			
Set rsProjInfo = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top colspan="2">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle">
						<%if rsReport("logo") = "" then%>
							<img src="images/logo.jpg">
						<%else%>
							<img src="<%=rsReport("logo")%>">
						<%end if%>
					</td>
					<td valign="middle" align="left"><span style="font-size:16px;">Technician Monthly Report #<%=reportID%></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td valign="top">
						<b>Project Name:</b> <%=rsReport("projectName")%><br>
						<b>Technician:</b> <%=rsReport("lastName")%>,&nbsp;<%=rsReport("firstName")%><br>	
						
											
					</td>
					<td></td>
					<td valign="top">
						<b>Report Month & Year:</b> <!--<%'=rsReport("inspectionDate")%>--><%=month(rsReport("inspectionDate"))%>/<%=year(rsReport("inspectionDate"))%><br>
						<b>LDA Date:</b> <%if rsProjInfo("startDate") <> "" then
							response.Write rsProjInfo("startDate")
						else
							response.Write "N/A"
						end if%><br>
						<b>NOT Date:</b> <%if rsProjInfo("endDate") <> "" then
							response.Write rsProjInfo("endDate")
						else
							response.Write "N/A"
						end if%><br>
					</td>
				</tr>
			</table><br>
			
			<b>Perimeter Control BMP's Installation Date:</b> <%if rsMonthly("perimControlBMPInstallDate") <> "" then
				response.Write rsMonthly("perimControlBMPInstallDate")
			else
				response.Write "N/A"
			end if%><br>
			<b>Initial Sediment Storage Requirements Installation Date:</b> <%if rsMonthly("initSedStorageInstallDate") <> "" then
				response.Write rsMonthly("initSedStorageInstallDate")
			else
				response.Write "N/A"
			end if%><br>
			<b>Sediment Storage/Perimeter Control BMP'sfor T/L "Initial Segment" Installation Date:</b> <%if rsMonthly("sedStorageBMPInstallDate") <> "" then
				response.Write rsMonthly("sedStorageBMPInstallDate")
			else
				response.Write "N/A"
			end if%><br>
			
		</td>
	</tr>
	
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan=2>
			<table cellpadding="3" cellspacing="0">	
				<tr>
					<td bgcolor="#CCCCCC" width="40" align="center">
						<strong class="smallCopy"><%=rsMonthly("landDisturbanceWeekendHoliday")%></strong>&nbsp;
					</td>
					<td>
						<span class="smallCopy">Did land disturbance occur on a weekend or holiday?</span>
					</td>
					<td width="20">&nbsp;</td>
					<%if rsMonthly("landDisturbanceWeekendHolidayListDates") <> "" then%>
					<td bgcolor="#CCCCCC" width="290" colspan="2" valign="top">
						<span class="smallCopy">If yes - list dates:</span>&nbsp;<strong class="smallCopy"><%=rsMonthly("landDisturbanceWeekendHolidayListDates")%></strong>
					</td>
					<%else%>
					<td></td>
					<%end if%>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan=2 align="center"><strong>Daily Inspection Forms</strong></td></tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table cellpadding="3" cellspacing="0">
				<tr>
					<td bgcolor="#CCCCCC" width="40" align="center">
						<strong class="smallCopy"><%=rsMonthly("tlNumConstructionDays")%></strong>&nbsp;
					</td>
					<td width="260">
						<span class="smallCopy"># of construction days</span>
					</td>
					<td width="20">&nbsp;</td>
					<td width="40" align="center">
						<!--<strong class="smallCopy"><%'=rsMonthly("ssNumConstructionDays")%></strong>&nbsp;-->
					</td>
					<td width="290">
						<!--<span class="smallCopy">S/S # of construction days</span>-->
					</td>
				</tr>
				<tr><td colspan="8" height="3"></td></tr>
				<tr>
					<td bgcolor="#CCCCCC" align="center">
						<strong class="smallCopy"><%=rsMonthly("tlDailyReportsAllDays")%></strong>&nbsp;
					</td>
					<td>
						<span class="smallCopy">Daily Reports for all Construction Days?</span>
					</td>
					<td width="20">&nbsp;</td>
					<td align="center">
						<!--<strong class="smallCopy"><%'=rsMonthly("ssDailyReportsAllDays")%></strong>&nbsp;-->
					</td>
					<td>
						<!--<span class="smallCopy">Daily Reports for all Construction Days?</span>-->
					</td>
				</tr>
				<tr><td colspan="8" height="3"></td></tr>
				<tr>
					<td bgcolor="#CCCCCC" align="center">
						<strong class="smallCopy"><%=rsMonthly("complianceGTCPolicy")%></strong>&nbsp;
					</td>
					<td>
						<span class="smallCopy">Compliance with GTC policy and procedures for Daily Inspection Forms?</span>
					</td>
					<td width="20">&nbsp;</td>
					<%if rsMonthly("complianceGTCPolicyWhyNot") <> "" then%>
					<td bgcolor="#CCCCCC" colspan="2" valign="top">
						<span class="smallCopy">If no - why?</span>&nbsp;<strong class="smallCopy"><%=rsMonthly("complianceGTCPolicyWhyNot")%></strong>
					</td>
					<%else%>
					<td></td>
					<%end if%>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2 align="center"><strong>Stabilization</strong></td></tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td bgcolor="#CCCCCC" width="40" align="center">
						<strong class="smallCopy"><%=rsMonthly("stabilizationAchieved")%></strong>&nbsp;
					</td>
					<td width="240">
						<span class="smallCopy">Is stabilization achieved?</span>
					</td>
					<td width="20">&nbsp;</td>
					<%if rsMonthly("explainStabilizationAchieved") <> "" then%>
					<td width="350" bgcolor="#CCCCCC" colspan="2" valign="top">
						<span class="smallCopy">If yes, then provide specifics on stabilization:</span>&nbsp;<strong class="smallCopy"><%=rsMonthly("explainStabilizationAchieved")%></strong>
					</td>
					<%else%>
					<td></td>
					<%end if%>
				</tr>
				<tr>
					<td colspan="4">
						<b>Site Stabilization Date:</b> <%if rsMonthly("siteStabilizationDate") <> "" then
							response.Write rsMonthly("siteStabilizationDate")
						else
							response.Write "N/A"
						end if%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2 align="center"><strong>BMP Inspection Forms</strong></td></tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td bgcolor="#CCCCCC" width="40" align="center">
						<strong class="smallCopy"><%=rsMonthly("inspectionOccurEvery14Days")%></strong>&nbsp;
					</td>
					<td width="240">
						<span class="smallCopy">Did an inspection occur every 14 days during the month?</span>
					</td>
					<td width="20">&nbsp;</td>
					<%if rsMonthly("inspectionOccurEvery14DaysWhyNot") <> "" then%>
					<td width="350" bgcolor="#CCCCCC" colspan="2" valign="top">
						<span class="smallCopy">If no - why?</span>&nbsp;<strong class="smallCopy"><%=rsMonthly("inspectionOccurEvery14DaysWhyNot")%></strong>
					</td>
					<%else%>
					<td></td>
					<%end if%>
				</tr>
				<tr><td colspan="8" height="3"></td></tr>
				<tr>
					<td bgcolor="#CCCCCC" align="center">
						<strong class="smallCopy"><%=rsMonthly("inspectionOccur24HourRain")%></strong>&nbsp;
					</td>
					<td>
						<span class="smallCopy">Did inspections occur within 24 hours of a "= or >" 0.5 inch rainfall event or the next business day if the rainfall ended during non-business hours?</span>
					</td>
					<td width="20">&nbsp;</td>
					<%if rsMonthly("inspectionOccur24HourRainWhyNot") <> "" then%>
					<td bgcolor="#CCCCCC" colspan="2" valign="top">
						<span class="smallCopy">If no - why?</span>&nbsp;<strong class="smallCopy"><%=rsMonthly("inspectionOccur24HourRainWhyNot")%></strong>
					</td>
					<%else%>
					<td></td>
					<%end if%>
				</tr>
				<tr><td colspan="8" height="3"></td></tr>
				<tr>
					<td bgcolor="#CCCCCC" align="center">
						<strong class="smallCopy"><%=rsMonthly("monthlyInspectionOccur")%></strong>&nbsp;
					</td>
					<td colspan="3">
						<span class="smallCopy">If stabilization has been achieved, did a monthly inspection occur?</span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2 align="center"><strong>BMP Installation and Maintenance</strong></td></tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td bgcolor="#CCCCCC" width="40" align="center">
						<strong class="smallCopy"><%=rsMonthly("bmpComplianceGTCPolicy")%></strong>&nbsp;
					</td>
					<td width="240">
						<span class="smallCopy">Compliance with GTC policy and procedures for BMP Installation and Maintenance?</span>
					</td>
					<td width="20">&nbsp;</td>
					<%if rsMonthly("bmpComplianceGTCPolicyWhyNot") <> "" then%>
					<td width="350" bgcolor="#CCCCCC" colspan="2" valign="top">
						<span class="smallCopy">If no - why?</span>&nbsp;<strong class="smallCopy"><%=rsMonthly("bmpComplianceGTCPolicyWhyNot")%></strong>
					</td>
					<%else%>
					<td></td>
					<%end if%>
				</tr>
			</table>
		</td>
	</tr>
	
	
	
		<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2 align="center"><strong>Water Quality Monitoring</strong></td></tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<%'get the data
			Set oCmd = Server.CreateObject("ADODB.Command")
	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetMonthlyRainData"
				.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
			   .CommandType = adCmdStoredProc
			   
			End With
						
			Set rsData = oCmd.Execute
			Set oCmd = nothing
			
			'create arrays for each item
			i = 0
			Dim arrSamplerID(10)
			'Dim arrGCSamplerID(10)
			'Dim arrSamplerType(10)
			Dim arrDateEndClearingGrubbing(10)
			'Dim arrDateSamplerInstalled(10)			
			Dim arrDateSampleTakenAfterClearGrub(10)
			Dim arrDateEndGrading(10)
			Dim arrDateAfterEndGradingorNinety(10)
			
			Dim arrSamplingRequired(10)
			Dim arrSamplingComplete(10)
			
			
			'Dim arrBMPInstalledMaintRetrieved(10)
			'Dim arrDateSamplerRemoved(10)
			Dim arrRainDateEvent(10)
			Dim arrRainAmount(10)
			Dim arrCodeCol1(10)
			Dim arrCodeCol2(10)
			Dim arrCodeCol3(10)
			Dim arrCodeCol4(10)
			Dim arrCodeCol5(10)
			Dim arrCodeCol6(10)
			Dim arrCodeCol7(10)
			Dim arrCodeCol8(10)
			Dim arrCodeCol9(10)
			Dim arrCodeCol10(10)
			
			do until rsData.eof
				arrSamplerID(i) = rsData("samplerID")
				'arrGCSamplerID(i) = rsData("GCSamplerID")
				'arrSamplerType(i) = rsData("samplerType")
				arrDateEndClearingGrubbing(i) = rsData("dateEndClearingGrubbing")
				'arrDateSamplerInstalled(i) = rsData("dateSamplerInstalled")
				arrDateSampleTakenAfterClearGrub(i) = rsData("dateSampleTakenAfterClearGrub")
				arrDateEndGrading(i) = rsData("dateEndGrading")
				arrDateAfterEndGradingorNinety(i) = rsData("dateAfterEndGradingorNinety")
				
				arrSamplingRequired(i) = rsData("samplingRequired")
				arrSamplingComplete(i) = rsData("samplingComplete")
				
				
				'arrBMPInstalledMaintRetrieved(i) = rsData("BMPInstalledMaintRetrieved")
				'arrDateSamplerRemoved(i) = rsData("dateSamplerRemoved")
				arrRainDateEvent(i) = rsData("rainDateEvent")
				arrRainAmount(i) = rsData("rainAmount")
				arrCodeCol1(i) = rsData("codeCol1")
				arrCodeCol2(i) = rsData("codeCol2")
				arrCodeCol3(i) = rsData("codeCol3")
				arrCodeCol4(i) = rsData("codeCol4")
				arrCodeCol5(i) = rsData("codeCol5")
				arrCodeCol6(i) = rsData("codeCol6")
				arrCodeCol7(i) = rsData("codeCol7")
				arrCodeCol8(i) = rsData("codeCol8")
				arrCodeCol9(i) = rsData("codeCol9")
				arrCodeCol10(i) = rsData("codeCol10")
			rsData.movenext
			i=i+1
			loop
			%>
			<table width="100%" class="borderTable2" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Sampler ID<br>(See CMP for ID)</span>
					</td>
					<%for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%=arrSamplerID(i)%>&nbsp;</span>
						</td>
					<%next%>
				</tr>
				<!--<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">GTC Sampler ID</span>
					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrGCSamplerID(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Type (Upstream, Downstream, Outfall)</span>
					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrSamplerType(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>-->
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date end clearing and grubbing</span>
					</td>
					<%for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%=arrDateEndClearingGrubbing(i)%>&nbsp;</span>
						</td>
					<%next%>
				</tr>
				<!--<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date Sampler Installed</span>	
					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrDateSamplerInstalled(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>-->
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date sample taken after clearing/grubbing.</span>
					</td>
					<%for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%=arrDateSampleTakenAfterClearGrub(i)%>&nbsp;</span>
						</td>
					<%next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date of end of grading.</span>
					</td>
					<%for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%=arrDateEndGrading(i)%>&nbsp;</span>
						</td>
					<%next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date sample take after end or grading or within 90 days of first sampling event.</span>
					</td>
					<%for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%=arrDateAfterEndGradingorNinety(i)%>&nbsp;</span>
						</td>
					<%next%>
				</tr>
				<!--<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Were BMP's designed, installed and maintained when sample retrieved?</span>
					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrBMPInstalledMaintRetrieved(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date Sampler Removed & reason</span>

					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrDateSamplerRemoved(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>-->
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Is sampling required for this project per the ES&PC Plans?</span>

					</td>
					<%for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%=arrSamplingRequired(i)%>&nbsp;</span>
						</td>
					<%next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Sampling complete?</span>

					</td>
					<%for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%=arrSamplingComplete(i)%>&nbsp;</span>
						</td>
					<%next%>
				</tr>
				<tr>
					<td class="tCol"><span class="smallCopy">Date of >= 0.5<br /> inch event</span></td>
					<td class="tCol"><span class="smallCopy">Amt. Of<br />Rainfall</span></td>
					<td class="tCol" colspan="10" bgcolor="#000000">&nbsp;</td>
				</tr>	
				<%for i = 0 to 9%>
				<tr>
					<td class="tCol" bgcolor="#CCCCCC" align="center">
						<span class="smallCopy"><%=arrRainDateEvent(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCCCCC" align="center">
						<span class="smallCopy"><%=arrRainAmount(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol1(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol2(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol3(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol4(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol5(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol6(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol7(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol8(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol9(i)%>&nbsp;</span>
					</td>
					<td class="tCol" bgcolor="#CCFFFF" align="center">
						<span class="smallCopy"><%=arrCodeCol10(i)%>&nbsp;</span>
					</td>
				</tr>
				<%next%>
				<tr>
					<td class="tCol" align="center" bgcolor="#CCCCCC">
						<b class="smallCopy"><%=rsMonthly("samplesCollectedInMonth")%>&nbsp;</b>
					</td>
					<td class="tCol" colspan="6">
						<span class="smallCopy">Sample(s) collected during this month?</span>
					</td>
					<td class="tCol" align="center" bgcolor="#CCCCCC">
						<b class="smallCopy"><%=rsMonthly("exceedNTULimit")%>&nbsp;</b>
					</td>
					<td class="tCol" colspan="6">
						<span class="smallCopy">Exceedence of NTU limit</span>
					</td>
				</tr>
				<tr>
					<td class="tCol" align="center" bgcolor="#CCCCCC" valign="top">
						<b class="smallCopy"><%=rsMonthly("waterQualityMonitoringCompliance")%>&nbsp;</b>
					</td>
					<td class="tCol" colspan="6" valign="top">
						<span class="smallCopy">Compliance with GTC policy and procedures for Water Quality Monitoring?</span>
					</td>
					<%if rsMonthly("waterQualityMonitoringComplianceWhyNot") <> "" then%>
					<td class="tCol" align="left" bgcolor="#CCCCCC" colspan="7" valign="top">
						<span class="smallCopy">If no - why?</span>&nbsp;<b class="smallCopy"><%=rsMonthly("waterQualityMonitoringComplianceWhyNot")%>&nbsp;</b>
					</td>
					<%else%>
					<td class="tCol" align="left" bgcolor="#CCCCCC" colspan="7" valign="top">&nbsp;</td>
					<%end if%>
				</tr>
				<tr>
					<td class="tCol" align="center" bgcolor="#CCCCCC">
						<b class="smallCopy"><%=rsMonthly("BMPInstalledMaintRetrieved")%>&nbsp;</b>
					</td>
					<td class="tCol" colspan="12">
						<span class="smallCopy">Were BMP's designed, installed and maintained when sample retrieved?</span>
					</td>
				</tr>
				<tr>
					<td colspan=12 align="center" class="tCol"><strong>Impaired Streams</strong></td>
				</tr>
				<tr>
					<td class="tCol" align="center" bgcolor="#CCCCCC">
						<b class="smallCopy"><%=rsMonthly("projectDrainUpstream")%>&nbsp;</b>
					</td>
					<td class="tCol" colspan="12">
						<span class="smallCopy">Does this project or a portion of this project drain within one mile upstream of an impaired stream segment?</span>
					</td>
				</tr>
				<tr>
					<td class="tCol" align="center" bgcolor="#CCCCCC">
						<b class="smallCopy"><%=rsMonthly("projectDrainUpstreamYesOutlined")%>&nbsp;</b>
					</td>
					<td class="tCol" colspan="12">
						<span class="smallCopy">If Yes, were the additional BMPs and/or requirements implemented as outlined in the certified ES&PC Plan?</span>
					</td>
				</tr>
				<tr>
					<td class="tCol" align="center" bgcolor="#CCCCCC">
						<b class="smallCopy">If no - Explain&nbsp;</b>
					</td>
					<td class="tCol" colspan="12">
						<span class="smallCopy"><%=rsMonthly("projectDrainUpstreamExplain")%></span>
					</td>
				</tr>
				
				
				
				<tr>
					<td colspan=12 align="center" class="tCol"><strong>Daily Rainfall Data</strong></td>
				</tr>
				<tr>
					<td class="tCol" align="center" bgcolor="#CCCCCC">
						<b class="smallCopy"><%=rsMonthly("hobo")%>&nbsp;</b>
					</td>
					<td class="tCol" colspan="2">
						<span class="smallCopy">iNet</span>
					</td>
					<td class="tCol" align="center" bgcolor="#CCCCCC">
						<b class="smallCopy"><%=rsMonthly("dataTable")%>&nbsp;</b>
					</td>
					<td class="tCol" colspan="3">
						<span class="smallCopy">Other</span>
					</td>
					<td class="tCol" align="center" bgcolor="#CCCCCC">
						<b class="smallCopy"><%=rsMonthly("rainfallCollectedDaily")%>&nbsp;</b>
					</td>
					<td class="tCol" colspan="6">
						<span class="smallCopy">Was rainfall data collected daily?</span>
					</td>
				</tr>
				<tr>
					<td colspan="12" class="tCol">
						<span class="smallCopy">1) Enter data in gray areas.<br>
						2) In blue boxes use the following codes.  Can use multiple codes to convey information.</span><br><br>
						
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td valign="top">
									<span class="smallCopy">
									<strong>ST-E</strong>	Exceedence of NTU limit	<br>		
									<strong>ST</strong>	Sample taken	<br>		
									<strong>N/A</strong> 	Not a qualifying event for sampling	<br>
									<strong>NBH</strong> 	Non Business Hours / Holidays	<br>
									<strong>Q-MS</strong>	Qualifying event, missed sample<br> 
									</span>
								</td>
								<td width="20">&nbsp;</td>
								<td valign="top">
									<span class="smallCopy">
									<strong>Q-NO</strong>	Qualifying event- no sample due to no outlet<br> 						
									<strong>Q-ND</strong>	Qualifying event - no discharge 	<br>					
									<strong>Q-DB</strong>	Qualifying event- no sample due to dead battery<br>						
									<strong>Q-SM</strong>	Qualifying event- no sample due to sampler malfunction<br>	
									<strong>Q-IU</strong>   Qualifying event- no sample, area unsafe/impossible				

									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2"><br>
			
			<span class="smallCopy"><em>"I certify under the penalty of law that this report and all attachments were prepared
			under my direction or supervision in accordance with a system designed to assure that
			certified personnel properly gather and evaluate the information submitted. Based on my
			inquiry of the person or persons who manage the system, or those persons directly
			responsible for gathering the information, the information submitted is, to the best of my
			knowledge and belief, true, accurate, and complete. I am aware that there are significant
			penalties for submitting false information, including the possibility of fine and imprisonment
			for knowing violations"</em></span></u><br><br>
			
			<strong class="smallCopy">Signature:</strong> 
			<%if rsReport("sigFileName") <> "" then%> 
				<img src="userUploads/<%=rsReport("userID")%>/<%=rsReport("sigFileName")%>">&nbsp;&nbsp;
			<%else%>
			______________________________ 
			<%end if%> <br><br>
			<strong class="smallCopy">Date:</strong> <span class="smallCopy"><%=rsReport("inspectionDate")%></span><br>
			<span class="smallCopy">Erosion Control Inspector</span>
			
						
		</td>
	</tr>
</table>
</body>
</html>