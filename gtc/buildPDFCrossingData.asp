<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCrossingDataReport"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProjectLocationByLocation"
   .parameters.Append .CreateParameter("@ID", adVarChar, adParamInput, 500, rs("crossingID"))
   .CommandType = adCmdStoredProc   
End With
			
Set rsLocation = oCmd.Execute
Set oCmd = nothing

'response.Write rs("crossingID")

%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top colspan="2">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle">
						<%if rsReport("logo") = "" then%>
							<img src="images/logo.jpg">
						<%else%>
							<img src="<%=rsReport("logo")%>">
						<%end if%>
					</td>
					<td valign="middle" align="left"><span style="font-size:16px;">GTC COE Permit Compliance Data #<%=reportID%></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td valign="top">
						<b>Project Name:</b> <%=rsReport("projectName")%><br>
						<b>Date of Inspection:</b> <%=rsReport("inspectionDate")%><br>
						<b>Install Date:</b> <%=rsLocation("dateInstalled")%><br>	
												
					</td>
					<td></td>
					<td valign="top">
						<b>Crossing ID:</b> <%=rs("crossingID")%><br>
						<b>Technician:</b> <%=rsReport("lastName")%>,&nbsp;<%=rsReport("firstName")%><br>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan=2 align="center"><strong>Crossing Data</strong></td></tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table width="100%" class="borderTable2" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td class="tCol" colspan="2">
						<strong>Type of Crossing</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("typeOfCrossing")%><%if rs("otherTypeOfCrossing") <> "" then%>: <%=rs("otherTypeOfCrossing")%><%end if%>
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Length of Crossing (ft.)</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("lengthOfCrossing")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Width of Crossing (ft.)</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("widthOfCrossing")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Pipe length (ft.)</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("pipeLength")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Outlet Protection Length (ft.)</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("outletProtectionLength")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Outlet Protection at Grade</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("outletProtectionAboveGrade")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Inlet Protection Length (ft.)</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("inletProtectionLength")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Inlet Protection at Grade (in.)</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("inletProtectionAboveGrade")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Pipe Embedded?</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("pipeEmbedded")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Rock Crossing At-Grade?</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("rockCrossingAtGrade")%>&nbsp;
					</td>
				</tr>
				<!--<tr>
					<td class="tCol" colspan="2">
						<strong>Rock Crossing Depth above Grade (in.)</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%'=rs("rockCrossingDepthAboveGrade")%>&nbsp;
					</td>
				</tr>-->
				<tr>
					<td class="tCol" colspan="2">
						<strong>Geotextile Underliner?</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("geotextileUnderliner")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Rock Washed Downstream?</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("rockWashedDownstream")%>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tCol" colspan="2">
						<strong>Geoweb?</strong>
					</td>
					<td width="30%" class="tCol" bgcolor="#CCCCCC" align="center">
						<%=rs("geoweb")%>&nbsp;
					</td>
				</tr>
	
			</table><br>
			
			<strong>Other Issues/ Comments</strong><br>
			<%=rs("issuesComments")%>
		</td>
	</tr>
</table>
</body>
</html>