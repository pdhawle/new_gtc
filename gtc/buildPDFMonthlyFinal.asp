<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetMonthlyFinalReport"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
			
Set rsMonthly = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top colspan="2">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle">
						<%if rsReport("logo") = "" then%>
							<img src="images/logo.jpg">
						<%else%>
							<img src="<%=rsReport("logo")%>">
						<%end if%>
					</td>
					<td valign="middle" align="left"><span style="font-size:16px;">Final Monthly Report #<%=reportID%></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td valign="top">
						<b>Project Name:</b> <%=rsReport("projectName")%><br>
						<b>Month:</b> <%=getMonth(rsMonthly("month"))%>&nbsp;<%=rsMonthly("year")%><br>
						<b>Project Number:</b> <%=rsReport("projectNumber")%><br>	
						<b>County:</b> 
						<%sCounty = split(rsReport("county"), ",")
						if sCounty <> "" then
							For isLoop = LBound(sCounty) to UBound(sCounty)
								'get the county
								Set oCmd = Server.CreateObject("ADODB.Command")	
								With oCmd
								   .ActiveConnection = DataConn
								   .CommandText = "spGetCounty"
								   .parameters.Append .CreateParameter("@countyID", adInteger, adParamInput, 8, trim(sCounty(isLoop)))
								   .CommandType = adCmdStoredProc							   
								End With
							
								Set rsCounty = oCmd.Execute
								Set oCmd = nothing
								
								do until rsCounty.eof
									response.Write rsCounty("county")
								rsCounty.movenext
								loop
								
								if isLoop <> UBound(sCounty) then
									response.Write ", "
								end if 
							next
						end if
						%>
											
					</td>
					<td></td>
					<td valign="top">
						<strong>EPD Regional Office</strong><br>
						<%if rsReport("contactName") <> "" then
							response.Write rsReport("contactName") & "<br>"
						end if%>
						<%if rsReport("office") <> "" then
							response.Write rsReport("office") & "<br>"
						end if%>
						<%if rsReport("officeAddress1") <> "" then
							response.Write rsReport("officeAddress1") & "<br>"
						end if%>
						<%if rsReport("officeAddress2") <> "" then
							response.Write rsReport("officeAddress2") & "<br>"
						end if%>
						<%=rsReport("officeCity")%>,&nbsp;<%=rsReport("officeState")%>&nbsp;<%=rsReport("officeZip")%> <br><br>
						
						<%if rsMonthly("finalStabilization") = "True" then%>
						<em class="footer">*Final Stabilization Achieved</em>
						<%end if%>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan=2><strong>Stormwater Data</strong></td></tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table cellpadding="3" cellspacing="0">
				<tr>
					<td width="30">&nbsp;</td>
					<td>
						<%if rsMonthly("stormwaterData") = 1 then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>
					</td>
					<td>
						Enclosed is GTC�s Monthly NPDES Stormwater Monitoring Report
					</td>
				</tr>
				<tr>
					<td width="30">&nbsp;</td>
					<td>
						<%if rsMonthly("stormwaterData") = 2 then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>
					</td>
					<td>
						There were no stormwater samples retrieved at this project site for this reporting month.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan=2>
			
			<strong>Potential Violations</strong><br><em class="footer">(Efforts are continuously being made to prevent, correct, and reduce the likelihood of potential permit violations.)</em>
		</td>
	</tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table cellpadding="3" cellspacing="0">
				<tr>
					<td width="30">&nbsp;</td>
					<td valign="top">
						<%if rsMonthly("potentialViolationsPermit") = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>
					</td>
					<td>
						Following is a list of what may be considered permit violations for the project.<br>  
						
						<%
						sViolation = split(rsMonthly("violationList"), ",") 
						For iLoop = LBound(sViolation) to UBound(sViolation) 
							sViolation(iLoop) = Trim(sViolation(iLoop)) 
						Next 
						
						For i = LBound(sViolation) to UBound(sViolation) 
							
							
							if sViolation(i) = "Other" then
								sViol = sViol & "<i>Other: " & rsMonthly("violationOther") & "</i><br>"
							else
								sViol = sViol & "<i>" & sViolation(i) & "</i><br>"
							end if
						Next 
						
						
						response.Write sViol%>
					</td>
				</tr>
				<tr>
					<td width="30">&nbsp;</td>
					<td valign="top">
						<%if rsMonthly("potentialViolationsInfo") = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>
					</td>
					<td>
						Based on the information available to us, we believe that the project site is in compliance with the General Permit.  
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan=2><strong>Certification</strong></td></tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td width="30">&nbsp;</td>
					<td colspan="3">
						<em class="footer">I certify under the penalty of law that this report and all attachments were prepared
						under my direction or supervision in accordance with a system designed to assure that
						certified personnel properly gather and evaluate the information submitted. Based on my
						inquiry of the person or persons who manage the system, or those persons directly
						responsible for gathering the information, the information submitted is, to the best of my
						knowledge and belief, true, accurate, and complete. I am aware that there are significant
						penalties for submitting false information, including the possibility of fine and imprisonment
						for knowing violations.</em>
					</td>
				</tr>
				<tr>
					<td></td>
					<td align="right" valign="bottom">
						<%if rsReport("signID") <> "" then%> 
							<img src="userUploads/<%=rsReport("signID")%>/<%=rsReport("signSigFile")%>">
						<%else%>
							________________________ 
						<%end if%>
						<br>
						<%if rsReport("signID") <> "" then%> 
						<span class="footer"><%=rsReport("signlastName")%>,&nbsp;<%=rsReport("signfirstName")%>&nbsp;<%=rsReport("signTitle")%></span>
						<%else%>
							&nbsp;
						<%end if%>
							
					</td>
					<td width="10">&nbsp;</td>
					<td valign="bottom">
						<%'if rsReport("dateSigned") <> "" then%> 
							<span class="footer"><u><%=formatdatetime(rsReport("inspectionDate"),2)%></u>
						<%'else%>
							<!--___________________-->
						<%'end if%>
						<br>
						
						Date</span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>