<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<!--#include file="letterArray.asp"-->

<%'need customerID, reportID

iDivisionID = request("divisionID")
reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With			
Set rsReport = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategories"'ByDivision
    .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCategories = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>Storm Water Site Inspection Report</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/reportswsir.css" type="text/css">
</head>
<body>

<table border="0" width=650 cellpadding=0 cellspacing=0>
	<tr>		
		<td valign="top" width="450">
			<span class="Header">Storm Water Site Evaluation Report</span><br>
			<strong>Evaluation Date:</strong>&nbsp;<u><%=rsReport("inspectionDate")%></u><br>
			<strong>Division:</strong>&nbsp;<u><%=rsReport("division")%></u><br>
			<strong>Site Name:</strong>&nbsp;<u><%=rsReport("projectName")%></u><br>
			<strong>Project Number:</strong>&nbsp;<u><%=rsReport("projectNumber")%></u>
		</td>
		<td valign=top align="right">
			<%if rsReport("logo") = "" then%>
				<img src="images/logo.jpg">
			<%else%>
				<img src="<%=rsReport("logo")%>">
			<%end if%>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td><strong>Qualified Inspector:</strong>&nbsp;<u><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></u></td>
					<td><strong>Phone #:</strong>&nbsp;<u><%=rsReport("contactNumber")%></u></td>
					<td>
						<strong>Last Evaluation Date:</strong>&nbsp; 
						<%if rsReport("lastInspectionDate") <> "" then
							response.Write "<u>" & rsReport("lastInspectionDate") & "</u>"
						else
							response.Write "<u>N/A</u>"
						end if
						%>
					</td>
				</tr>
				<tr>
					<td><strong>Inspection Type:</strong>&nbsp;<u><%=rsReport("inspectionType")%></u></td>
					<td colspan="2">
						<strong>Rainfall Amount:</strong>&nbsp;<u><%=rsReport("rainfallAmount")%>&nbsp;
						<%if rsReport("rainfallAmount") > 1 then%>
							inches
						<%else%>
							inch
						<%end if%></u>
					</td>
				</tr>
				<tr>
					<td colspan="3"><strong>Weather:</strong>&nbsp;<u><%=rsReport("weatherType")%></u></td>
				</tr>
				<!--<tr>
					<%
				'	sStageList = split(rsReport("stages"), ",") 
				'	For iLoop = LBound(sStageList) to UBound(sStageList) 
				'		sStageList(iLoop) = Trim(sStageList(iLoop)) 
				'	Next 
					
				'	For i = LBound(sStageList) to UBound(sStageList) 
				'		sStage = sStage & "<u>" & sStageList(i) & "</u>" & "&nbsp;&nbsp;&nbsp;" 
				'	Next 
					%>
					<td colspan="3"><strong>Construction Stage(s):</strong>&nbsp;<%'=sStage%></td>
				</tr>-->
				<tr><td colspan="3" headers="5"></td></tr>
				<tr>
					<td colspan="3"><strong>Note: Keep this completed report and accompanying Responsive Action Log with the Storm Water Plan ("SWP").</strong></td>
				</tr>		
			</table>
			
		</td>
	</tr>
	<tr>
		<td colspan=2>
			<table border="0" width=650 cellpadding="0" cellspacing="0">
				<%i = 0
				blnChange = true
				do until rsCategories.eof%>
					<tr><td colspan=4 height="10"></td></tr>
					<tr><td colspan=4 valign="top"><img src="images/doubleLine.gif" width="650" height="3"></td></tr>
					<tr class="rowColor"><td colspan=4>&nbsp;<b><%=rsCategories("category")%></b></td></tr>
					<tr><td colspan=4 valign="top"><img src="images/doubleLine.gif" width="650" height="3"></td></tr>
							
					<%
						Set oCmd = Server.CreateObject("ADODB.Command")

						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spGetQuestionandAnswers"
						   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCategories("categoryID"))
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID) 'reportID
						   .CommandType = adCmdStoredProc
						   
						End With
									
						Set rsQuestion = oCmd.Execute
						Set oCmd = nothing%>
						
						<%'blnChange = true
						do until rsQuestion.eof%>
							<tr>
								<td valign=top>&nbsp;<%=alphArray(i)%>.&nbsp;</td>
								<td width="475">
									<%=rsQuestion("question")%>
								</td>
								<td></td>
								<td align=right width="150">
									
									<%	
																
									if rsQuestion("addNoteIfAI") = "True" then
										'check to see if there are open items for this question
										'get the percent complete for each question
										Set oCmd = Server.CreateObject("ADODB.Command")
										With oCmd
										   .ActiveConnection = DataConn
										   .CommandText = "spGetOpenAIByQuestion"
										   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID"))
										   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, rsQuestion("questionID"))
										   .CommandType = adCmdStoredProc
										   
										End With
													
										Set rsQuestionComplete = oCmd.Execute
										Set oCmd = nothing
										'if not isnull(rsQuestionComplete("openAI")) then
											'bNote = "True"%>
											<!--<strong><em>**</em></strong> -->
										<%'end if%>
									<%end if%>
									<%=rsQuestion("answer")%>
								</td>
							</tr>
							<tr><td colspan=4 height="5"></td></tr>
						<%rsQuestion.movenext
						i = i + 1
						loop%>
						
					<tr><td colspan=4 height="10"></td></tr>
				<%rsCategories.movenext
				loop%>
				<%'If bNote = "True" then%>
				
				<!--<tr>
					<td colspan=4>
						<em><strong>** uncompleted items from previous report(s)</strong></em>
					</td>
				</tr>
				<tr><td colspan=4 height="10"></td></tr>-->
				<%'end if%>
			</table>
		</td>
	</tr>
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=5></td></tr>
	<!--previous items will go here-->
	
	<tr>
		<td colspan="2">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<%
			'	Set oCmd = Server.CreateObject("ADODB.Command")
			'	With oCmd
			'	   .ActiveConnection = DataConn
			'	   .CommandText = "spGetOpenItemsByReport"
			'	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, reportID)
			'	   .CommandType = adCmdStoredProc
			'	   
			'	End With							
			'	Set rsOI = oCmd.Execute
			'	Set oCmd = nothing%>
					<tr><td colspan="3" height="20"></td></tr>
							
			</table>
		</td>
	</tr>
</table>
</body>
</html>