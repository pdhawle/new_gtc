<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

reportID = request("reportID")
iDivisionID = request("divisionID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategories"'ByDivision
    .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsCategories = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top colspan="2">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle">
						<%if rsReport("logo") = "" then%>
							<img src="images/logo.jpg">
						<%else%>
							<img src="<%=rsReport("logo")%>">
						<%end if%>
					</td>
					<td valign="middle" align="right"><span class="Header">Report #<%=reportID%></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td valign="top">
						<b>Inspection Type:</b> <%=rsReport("inspectionType")%><br>
						<b>Customer:</b> <%=rsReport("customerName")%><br>
						<b>Qualified Inspector:</b> <%=rsReport("lastName")%>,&nbsp;<%=rsReport("firstName")%><br>
						<b>Project Status:</b> <%=rsReport("projectStatus")%><br>						
					</td>
					<td></td>
					<td>
						<b>Report Type:</b> <%=rsReport("reportType")%><br>
						<b>Project:</b> <%=rsReport("projectName")%><br>
						<b>Report Date:</b> <%=rsReport("inspectionDate")%><br>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr>
		<td colspan=2><hr /><br><br></td></tr>
	</tr>
	<tr>
		<td colspan=2><strong>PETROLEUM PRODUCTS STORAGE/TRANSFER</strong><br><br></td></tr>
	</tr>
	<tr>
		<td colspan=2>
			<table width=650 cellpadding="0" cellspacing="0">
				<%i = 1
				blnChange = true
				do until rsCategories.eof%>
					<tr>
						<td colspan=2>
							<!--&nbsp;<br><b><u><%'=rsCategories("category")%></u></b><br>&nbsp;<br>-->
							<%
								Set oCmd = Server.CreateObject("ADODB.Command")

								With oCmd
								   .ActiveConnection = DataConn
								   .CommandText = "spGetPetroleumQuestionandAnswers"
								   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCategories("categoryID"))
								   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID) 'reportID
								   .CommandType = adCmdStoredProc
								   
								End With
									
									If Err Then
								%>
									1	<!--#include file="includes/FatalError.inc"-->
								<%
									End If
											
								Set rsQuestion = oCmd.Execute
								Set oCmd = nothing%>
								<table width=650 cellpadding=0 cellspacing=0>
									<%blnChange = true
									do until rsQuestion.eof
										If blnChange = true then%>
											<tr class="rowColor">
										<%else%>
											<tr>
										<%end if%>
											<td valign=top width="15">
												&nbsp;<%=i%>.&nbsp; 
											</td>
											<td valign=top>
												<%=rsQuestion("question")%>
											</td>
											<td valign=top>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</td>
											<td valign=top align=right>
												<%=rsQuestion("answer")%>&nbsp;<br><img src=images/pix.gif width=200 height=1>
											</td>
										</tr>
										<%if rsQuestion("remark") <> "" then
											If blnChange = true then%>
											<tr class="rowColor">
											<%else%>
												<tr>
											<%end if%><td></td><td colspan="3"><br><strong>Remarks:</strong> <%=rsQuestion("remark")%><br></td></tr>
										<%end if%>
										<tr><td></td><td colspan="3"><img src=images/pix.gif width=1 height=10></td></tr>
									<%rsQuestion.movenext
									if blnChange = true then
										blnChange = false
									else
										blnChange = true
									end if
									i = i + 1
									loop%>
								</table>
						</td>
					</tr>
				<%rsCategories.movenext
				loop%>
			</table>
		</td>
	</tr>
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td colspan="2">
			<b>Comments:</b> <%=rsReport("comments")%>
		</td>
	</tr>
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=30></td></tr>
	<tr>
		<td colspan="2">
			<em>I certify under the penalty of law that this document and all attachments were prepared
			under my direction or supervision in accordance with a system designed to assure that
			qualified personnel properly gather and evaluate the information submitted. Based on my
			inquiry of the person or persons who manage the system, or those persons directly
			responsible for gathering the information, the information submitted is, to the best of my
			knowledge and belief, true, accurate, and complete. I am aware that there are significant
			penalties for submitting false information, including the possibility of fine and imprisonment
			for knowing violations.</em><br><br>
			
			<strong>Inspectors Signature:</strong> 
			<%if rsReport("sigFileName") <> "" then%> 
				<img src="userUploads/<%=rsReport("userID")%>/<%=rsReport("sigFileName")%>">&nbsp;&nbsp;
			<%else%>
			______________________________ 
			<%end if%>
			&nbsp;&nbsp;<strong>Date:</strong> _____________<br><br>
			<strong>Title:</strong> _____________
						
		</td>
	</tr>
</table>
</body>
</html>