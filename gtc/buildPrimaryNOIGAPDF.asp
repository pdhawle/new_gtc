<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
primaryFormID = request("primaryFormID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetPrimaryNOI"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, primaryFormID) 'primaryFormID
   .CommandType = adCmdStoredProc
   
End With
			
Set rsNOI = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td align="right">
			<table width="130" border="0" cellpadding="2" cellspacing="1" style="border-color:#000000;border-style:solid;border-width:thin;">
				<tr><td height="10">&nbsp;</td></tr>
			</table>
			For Official Use Only
		</td>
	</tr>
	<tr>
		<td align="center">
			<strong>NOTICE OF INTENT<br><br>

			VERSION 2008<br><br>
			
			State of Georgia<br>
			Department of Natural Resources<br>
			Environmental Protection Division<br>
			
			For Coverage Under the 2008 Re-Issuance of the NPDES General Permits <br>
			To Discharge Storm Water Associated With Construction Activity<br><br>
			
			PRIMARY PERMITTEE</strong>

		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td>
			<strong>NOTICE OF INTENT (Check Only One):</strong><br><br>
			<%select case rsNOI("noticeOfIntent")
				Case 1%>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td><img src="images/checked.gif">&nbsp;&nbsp;</td>
							<td>Initial Notification (New Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Re-Issuance Notification (Existing Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Change of Information (Applicable only if the NOI was submitted after August 1, 2008)</td>
						</tr>
					</table>
				<%case 2%>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Initial Notification (New Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checked.gif">&nbsp;&nbsp;</td>
							<td>Re-Issuance Notification (Existing Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Change of Information (Applicable only if the NOI was submitted after August 1, 2008)</td>
						</tr>
					</table>
				<%case 3%>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Initial Notification (New Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Re-Issuance Notification (Existing Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checked.gif">&nbsp;&nbsp;</td>
							<td>Change of Information (Applicable only if the NOI was submitted after August 1, 2008)</td>
						</tr>
					</table>
			<%end select%>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td>
			<strong>COVERAGE DESIRED (Check Only One):</strong><br><br>
			<%
			select case rsNOI("coverageDesired")
				Case 1%>
					<img src="images/checked.gif">&nbsp;GAR100001 - Stand Alone&nbsp;&nbsp;<img src="images/checkbox.gif">&nbsp;GAR100002 - Infrastructure &nbsp;&nbsp;<img src="images/checkbox.gif">&nbsp;GAR100003 - Common Development
				<%case 2%>
					<img src="images/checkbox.gif">&nbsp;GAR100001 - Stand Alone&nbsp;&nbsp;<img src="images/checked.gif">&nbsp;GAR100002 - Infrastructure &nbsp;&nbsp;<img src="images/checkbox.gif">&nbsp;GAR100003 - Common Development
				<%case 3%>
					<img src="images/checkbox.gif">&nbsp;GAR100001 - Stand Alone&nbsp;&nbsp;<img src="images/checkbox.gif">&nbsp;GAR100002 - Infrastructure &nbsp;&nbsp;<img src="images/checked.gif">&nbsp;GAR100003 - Common Development
			<%end select%>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>I.	SITE/OWNER/OPERATOR INFORMATION</strong><br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2">Project Construction Site Name:&nbsp;<u><%=rsNOI("siteProjectName")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">GPS Location of Construction Exit (degrees/minutes/seconds):</td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2" align="center">Latitude <u><%=rsNOI("GPSDegree1")%></u>&deg; <u><%=rsNOI("GPSMinute1")%></u>' <u><%=rsNOI("GPSSecond1")%></u>&quot;&nbsp;&nbsp;&nbsp;Longitude <u><%=rsNOI("GPSDegree2")%></u>&deg; <u><%=rsNOI("GPSMinute2")%></u>' <u><%=rsNOI("GPSSecond2")%></u>&quot; </td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Construction Site Street Address:&nbsp;<u><%=rsNOI("projectAddress")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">City (applicable if the site is located within the jurisdictional boundaries of the municipality):&nbsp;<u><%=rsNOI("projectCity")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">County:&nbsp;<u><%=rsNOI("projectCounty")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Common Development Name (applicable only to General NPDES Permit No. GAR100003):&nbsp;<u><%=rsNOI("subdivision")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									Owner�s Name:&nbsp;<u><%=rsNOI("ownerName")%></u>
								</td>
								<td></td>
								<td>
									<!--Phone:&nbsp;<u><%'=rsNOI("ownerPhone")%></u>-->
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Address:&nbsp;<u><%=rsNOI("ownerAddress")%></u>&nbsp;&nbsp;City:&nbsp;<u><%=rsNOI("ownerCity")%></u>
								</td>
								<td></td>
								<td>
									State:&nbsp;<u><%=rsNOI("ownerState")%></u>&nbsp;&nbsp;Zip Code:&nbsp;<u><%=rsNOI("ownerZip")%></u>
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Duly Authorized Representative (optional):&nbsp;<u><%=rsNOI("authorizedRep")%></u>
								</td>
								<td></td>
								<td>
									Phone:&nbsp;<u><%=rsNOI("authorizedRepPhone")%></u>
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Operator�s Name (optional):&nbsp;<u><%=rsNOI("operatorName")%></u>
								</td>
								<td></td>
								<td>
									Phone:&nbsp;<u><%=rsNOI("operatorPhone")%></u>
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Address:&nbsp;<u><%=rsNOI("operatorAddress")%></u>&nbsp;&nbsp;City:&nbsp;<u><%=rsNOI("operatorCity")%></u>
								</td>
								<td></td>
								<td>
									State:&nbsp;<u><%=rsNOI("operatorState")%></u>&nbsp;&nbsp;Zip Code:&nbsp;<u><%=rsNOI("operatorZip")%></u>
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Facility/Construction Site Contact:&nbsp;<u><%=rsNOI("facilityContact")%></u>
								</td>
								<td></td>
								<td>
									Phone:&nbsp;<u><%=rsNOI("facilityContactPhone")%></u>
								</td>
							</tr>
						</table>
					
					</td>
				</tr>
	

				
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>II.	CONSTRUCTION SITE ACTIVITY INFORMATION</strong><br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>Start Date <em>(month/day/year)</em>:&nbsp;<u><%=rsNOI("startDate")%></u></td>
					<td width="50"></td>
					<td>Completion Date <em>(month/day/year)</em>:&nbsp;<u><%=rsNOI("completionDate")%></u></td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td colspan="3">Estimated Disturbed Acreage (acres, to the nearest tenth (1/10th) acre):&nbsp;<u><%=rsNOI("estimatedDisturbedAcerage")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="3">
						Does the Erosion, Sedimentation and Pollution Control Plan (Plan) provide for disturbing more than 50 acres at any one time for each 
						individual permittee (i.e., primary, secondary or tertiary permittees), or more than 50 contiguous acres total at any one 
						time ? (Check Only One):<br><br>
						
						<%select case rsNOI("disturb50")
							Case 1%>
								<img src="images/checked.gif">&nbsp;YES<br>
								<img src="images/checkbox.gif">&nbsp;NO<br>
								<img src="images/checkbox.gif">&nbsp;N/A - if the Plan was submitted prior to the effective date of the General NPDES Permit <br>&nbsp;&nbsp;&nbsp;&nbsp;No. GAR100001 and No. GAR100003 for Stand Alone and Common Development <br>&nbsp;&nbsp;&nbsp;&nbsp;construction activities.<br>
								<img src="images/checkbox.gif">&nbsp;N/A � if construction activities are covered under the General NPDES Permit No. <br>&nbsp;&nbsp;&nbsp;&nbsp;GAR100002 for Infrastructure construction projects.<br>
								
							<%case 2%>
								<img src="images/checkbox.gif">&nbsp;YES<br>
								<img src="images/checked.gif">&nbsp;NO<br>
								<img src="images/checkbox.gif">&nbsp;N/A - if the Plan was submitted prior to the effective date of the General NPDES Permit <br>&nbsp;&nbsp;&nbsp;&nbsp;No. GAR100001 and No. GAR100003 for Stand Alone and Common Development <br>&nbsp;&nbsp;&nbsp;&nbsp;construction activities.<br>
								<img src="images/checkbox.gif">&nbsp;N/A � if construction activities are covered under the General NPDES Permit No. <br>&nbsp;&nbsp;&nbsp;&nbsp;GAR100002 for Infrastructure construction projects.<br>
							<%case 3%>
								<img src="images/checkbox.gif">&nbsp;YES<br>
								<img src="images/checkbox.gif">&nbsp;NO<br>
								<img src="images/checked.gif">&nbsp;N/A - if the Plan was submitted prior to the effective date of the General NPDES Permit <br>&nbsp;&nbsp;&nbsp;&nbsp;No. GAR100001 and No. GAR100003 for Stand Alone and Common Development <br>&nbsp;&nbsp;&nbsp;&nbsp;construction activities.<br>
								<img src="images/checkbox.gif">&nbsp;N/A � if construction activities are covered under the General NPDES Permit No. <br>&nbsp;&nbsp;&nbsp;&nbsp;GAR100002 for Infrastructure construction projects.<br>
							<%case 4%>
								<img src="images/checkbox.gif">&nbsp;YES<br>
								<img src="images/checkbox.gif">&nbsp;NO<br>
								<img src="images/checkbox.gif">&nbsp;N/A - if the Plan was submitted prior to the effective date of the General NPDES Permit <br>&nbsp;&nbsp;&nbsp;&nbsp;No. GAR100001 and No. GAR100003 for Stand Alone and Common Development <br>&nbsp;&nbsp;&nbsp;&nbsp;construction activities.<br>
								<img src="images/checked.gif">&nbsp;N/A � if construction activities are covered under the General NPDES Permit No. <br>&nbsp;&nbsp;&nbsp;&nbsp;GAR100002 for Infrastructure construction projects.<br>
						<%end select%>
					</td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td valign="top">Construction Activity Type:</td>
					<td colspan="2">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>
									<%if rsNOI("typeConstructionCommercial") = "True" then%>
										<img src="images/checked.gif">&nbsp;Commercial
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Commercial
									<%end if%>									
								</td>
								<td width="40"></td>
								<td>
									<%if rsNOI("typeConstructionIndustrial") = "True" then%>
										<img src="images/checked.gif">&nbsp;Industrial
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Industrial
									<%end if%>									
								</td>
								<td width="40"></td>
								<td>
									<%if rsNOI("typeConstructionMunicipal") = "True" then%>
										<img src="images/checked.gif">&nbsp;Municipal
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Municipal
									<%end if%>										
								</td>
							</tr>							
							<tr>
								<td>
									<%if rsNOI("typeConstructionLinear") = "True" then%>
										<img src="images/checked.gif">&nbsp;Linear
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Linear
									<%end if%>										
								</td>
								<td></td>
								<td>
									<%if rsNOI("typeConstructionUtility") = "True" then%>
										<img src="images/checked.gif">&nbsp;Utility
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Utility
									<%end if%>									
								</td>
								<td></td>
								<td>									
									<%if rsNOI("typeConstructionResidential") = "True" then%>
										<img src="images/checked.gif">&nbsp;Residential
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Residential
									<%end if%>	
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="3">Number of Secondary Permittees (applicable only to General NPDES Permit No. GAR100003):&nbsp;<u><%=rsNOI("secondaryPermittees")%></u></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>III.	RECEIVING WATER INFORMATION</strong><br><br>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="4">A.&nbsp;&nbsp;Name of Initial Receiving Water(s):&nbsp;<u><%=rsNOI("IRWName")%></u></td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4" align="center">
						<%if rsNOI("IRWTrout") = "True" then%>
							<img src="images/checked.gif">&nbsp;Trout Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Trout Stream
						<%end if%>&nbsp;&nbsp;&nbsp;
						<%if rsNOI("IRWWarmWater") = "True" then%>
							<img src="images/checked.gif">&nbsp;Warm Water Fisheries Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Warm Water Fisheries Stream
						<%end if%>
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4">B.&nbsp;&nbsp;Name of MS4 Owner/Operator (if applicable):&nbsp;<u><%=rsNOI("MSSSOwnerOperator")%></u></td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name of Receiving Water(s):&nbsp;<u><%=rsNOI("RWName")%></u></td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4" align="center">
						<%if rsNOI("RWTrout") = "True" then%>
							<img src="images/checked.gif">&nbsp;Trout Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Trout Stream
						<%end if%>&nbsp;&nbsp;&nbsp;
						<%if rsNOI("RWWarmWater") = "True" then%>
							<img src="images/checked.gif">&nbsp;Warm Water Fisheries Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Warm Water Fisheries Stream
						<%end if%>
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td>C.&nbsp;&nbsp;</td>
					<td>
						<%if rsNOI("samplingOfRecievingStream") = "True" then%>
							<img src="images/checked.gif">&nbsp;Sampling of Receiving Stream(s):
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Sampling of Receiving Stream(s):
						<%end if%>						
					</td>
					<td>
						<%if rsNOI("troutStreamRecieve") = "True" then%>
							<img src="images/checked.gif">&nbsp;Trout Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Trout Stream
						<%end if%>						
					</td>
					<td>
						<%if rsNOI("warmWaterRecieve") = "True" then%>
							<img src="images/checked.gif">&nbsp;Warm Water Fisheries Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Warm Water Fisheries Stream
						<%end if%>						
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td>D.&nbsp;&nbsp;</td>
					<td>
						<%if rsNOI("samplingOfOutfall") = "True" then%>
							<img src="images/checked.gif">&nbsp;Sampling of Outfall(s):
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Sampling of Outfall(s):
						<%end if%>						
					</td>
					<td>
						<%if rsNOI("troutStream") = "True" then%>
							<img src="images/checked.gif">&nbsp;Trout Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Trout Stream
						<%end if%>						
					</td>
					<td>
						<%if rsNOI("warmWater") = "True" then%>
							<img src="images/checked.gif">&nbsp;Warm Water Fisheries Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Warm Water Fisheries Stream
						<%end if%>						
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td></td>
					<td colspan="3">
						<table cellpadding="0" cellspacing="0">	
							<tr>
								<td>Number of Sampling Outfalls:&nbsp;<u><%=rsNOI("numberOfOutfalls")%></u></td>
								<td width="50"></td>
								<td>Construction Site Size (acres):&nbsp;<u><%=rsNOI("constructionSiteSize")%></u></td>
							</tr>
							<tr><td height="5" colspan="3"></td></tr>
							<tr>
								<td>Appendix B NTU Value:&nbsp;<u><%=rsNOI("appendixBNTUValue")%></u></td>
								<td width="50"></td>
								<td>Surface Water Drainage Area (square miles):&nbsp;<u><%=rsNOI("SWDrainageArea")%></u></td>
							</tr>
						</table> 	
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">E.&nbsp;&nbsp;</td>
								<td>
									Does the facility/construction site discharge storm water into an Impaired Stream Segment, or within one (1) linear mile upstream of and 
									within the same watershed as, any portion of an Impaired Stream Segment identified as �not supporting� its designated use(s), as shown on Georgia�s 2008 
									and subsequent �305(b)/303(d) List Documents (Final)� listed for the criteria violated, �Bio F� (Impaired Fish Community) and/or �Bio M� (Impaired 
									Macroinvertebrate Community), within Category 4a, 4b or 5, and the potential cause is either �NP� (nonpoint source) or �UR� (urban runoff)?  
									(Check Only One):<br><br>
									
									<%select case rsNOI("siteDischargeStormWater")
										Case 1%>
											<img src="images/checked.gif">&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;<u><%=rsNOI("impairedStream")%></u><br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br>&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100002 for Infrastructure construction activities.<br>
											
										<%case 2%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checked.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br>&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100002 for Infrastructure construction activities.<br>
										<%case 3%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checked.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br>&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100002 for Infrastructure construction activities.<br>
										<%case 4%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br>&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br>
											<img src="images/checked.gif">&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100002 for Infrastructure construction activities.<br>
									<%end select%>
								</td>
							</tr>
							<tr><td colspan="4" height="5"></td></tr>
							<tr>
								<td valign="top">F.&nbsp;&nbsp;</td>
								<td>
									Does the facility/construction site discharge storm water into an Impaired Stream Segment where a Total Maximum Daily Load (TMDL) Implementation 
									Plan for �sediment� was finalized at least six (6) months prior to the submittal of the NOI ? (Check Only One):<br><br>
									
									<%select case rsNOI("siteDischargeStormWater2")
										Case 1%>
											<img src="images/checked.gif">&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;<u><%=rsNOI("impairedStream2")%></u><br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br>&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100002 for Infrastructure construction activities.<br>
											
										<%case 2%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checked.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br>&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100002 for Infrastructure construction activities.<br>
										<%case 3%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checked.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br>&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100002 for Infrastructure construction activities.<br>
										<%case 4%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br>&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br>
											<img src="images/checked.gif">&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100002 for Infrastructure construction activities.<br>
									<%end select%>
								</td>
							</tr>
						</table>
						
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>IV.	ATTACHMENTS (Applicable Only to New Facilities/Construction Sites)</strong><br><br>
			Indicate if the items listed below are attached to this Notice of Intent:<br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("locationMap") = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>	
					</td>
					<td width="5"></td>
					<td valign="top">
						Location map identifying the receiving water(s), outfall(s) or combination thereof to be monitored.
					</td>
				</tr>
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("ESPControlPlan") = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>	
					</td>
					<td width="5"></td>
					<td valign="top">
						Erosion, Sedimentation and Pollution Control Plan (if the project is greater than 50 acres regardless of   
						the existence of a certified Local Issuing Authority in the jurisdiction OR if the project is in a  
						jurisdiction where there is no certified Local Issuing Authority regulating that project regardless 
						of acreage).
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("writtenAuth") = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>	
					</td>
					<td width="5"></td>
					<td valign="top">
						Written authorization from the appropriate EPD District Office if the Plan disturbs more than 50 acres  
						at any one time for each individual permittee (i.e., primary, secondary or tertiary permittees), or more  than 50 
						contiguous acres total at any one time (applicable only to General NPDES Permits No. GAR100001 and No. GAR100003).
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("knownSecondaryPermittees") = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;&nbsp;	
					</td>
					<td width="5"></td>
					<td valign="top">
						List of known secondary permittees (applicable only to General NPDES Permit No. GAR100003).
					</td>
				</tr>
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("timingSchedule") = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;&nbsp;	
					</td>
					<td width="5"></td>
					<td valign="top">
						Schedule for the timing of the major construction activities.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>V.	CERTIFICATIONS (Owner or Operator or Both to Initial as Applicable)</strong><br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("certify1") <> "" then%>
							<u><%=rsNOI("certify1")%></u>
						<%else%>
							___
						<%end if%>	
					</td>
					<td width="5"></td>
					<td valign="top">
						I certify that the receiving water(s) or the outfall(s) or a combination of receiving water(s) and outfall(s) will be monitored in accordance 
						with the Erosion, Sedimentation and Pollution Control Plan.
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("certify2") <> "" then%>
							<u><%=rsNOI("certify2")%></u>
						<%else%>
							___
						<%end if%>	
					</td>
					<td width="5"></td>
					<td valign="top">
						I certify that the Erosion, Sedimentation and Pollution Control Plan (Plan) has been prepared in accordance with Part IV of the General NPDES Permit 
						No. GAR100001, No. GAR100002 or No. GAR100003, the Plan will be implemented, and that such Plan will provide for compliance with this permit.
					</td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("certify3") <> "" then%>
							<u><%=rsNOI("certify3")%></u>
						<%else%>
							___
						<%end if%>	
					</td>
					<td width="5"></td>
					<td valign="top">
						I certify under penalty of law that this document and all attachments were prepared under my direction or supervision in accordance with a system designed 
						to assure that certified personnel properly gather and evaluate the information submitted.  Based upon my inquiry of the person or persons who manage the 
						system, or those persons directly responsible for gathering the information, the information submitted is, to the best of my knowledge and belief, true, 
						accurate and complete.  I am aware that there are significant penalties for submitting false information, including the possibility of fines and imprisonment 
						for knowing violations.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>Owner�s Printed Name:&nbsp;<u><%=rsNOI("ownerPrintedName")%></u></td>
					<td width="100"></td>
					<td>Title:&nbsp;<u><%=rsNOI("ownerTitle")%></u></td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
				<tr>
					<td>Signature:&nbsp;__________________</td>
					<td></td>
					<td>Date:&nbsp;<u><%=rsNOI("ownerSignDate")%></u></td>
				</tr>
				<tr><td colspan="3" height="15"></td></tr>
				<tr>
					<td>Operator�s Printed Name:&nbsp;<u><%=rsNOI("operatorPrintedName")%></u></td>
					<td></td>
					<td>Title:&nbsp;<u><%=rsNOI("operatorTitle")%></u></td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
				<tr>
					<td>Signature:&nbsp;__________________</td>
					<td></td>
					<td>Date:&nbsp;<u><%=rsNOI("operatorSignDate")%></u></td>
				</tr>
			</table>
			   								  			
		</td>
	</tr>
</table>
</body>
</html>