<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetWaterSamplingReport"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsWSReport = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>
<%i = 1
do until rsWSReport.eof%>
<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top colspan="2">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle">
						<%if rsReport("logo") = "" then%>
							<img src="images/logo.jpg">
						<%else%>
							<img src="<%=rsReport("logo")%>">
						<%end if%>
					</td>
					<td valign="middle" align="right"><b>Report #<%=reportID%></b></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td valign="top">
						<b>Inspection Type:</b> <%=rsReport("inspectionType")%><br>
						<b>Customer:</b> <%=rsReport("customerName")%><br>
						<b>Qualified Inspector:</b> <%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%><br>
						<!--<b>Project Status:</b> <%'=rsReport("projectStatus")%><br>	
						<b>NTU Limit:</b> <%'=rsReport("NTULimit")%><br>-->						
					</td>
					<td></td>
					<td valign="top">
						<b>Report Type:</b> <%=rsReport("reportType")%><br>
						<b>Project:</b> <%=rsReport("projectName")%><br>
						<b>Report Date:</b> <%=month(rsReport("inspectionDate")) & "/" & year(rsReport("inspectionDate"))%><br>
						<b>Rainfall Amount:</b> <%=formatnumber(rsReport("rainfallAmount"),1)%> 
						<%if rsReport("rainfallAmount") > 1 then%>
							inches
						<%else%>
							inch
						<%end if%><br>
					</td>
				</tr>
				<%if rsReport("comments") <> "" then%>
				<tr>
					<td colspan="3">
						<b>Comments:</b> <%=rsReport("comments")%>
					</td>
				</tr>
				<%end if%>
			</table>
			
		</td>
	</tr>
	<tr><td colspan="3"><img src=images/pix.gif width=20 height=10></td></tr>
</table>

	<table width=650 cellpadding="0" cellspacing="0">
			<tr>
				<td colspan=2>
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="rowColor"><strong><u>Collection <%=i%></u></strong></td>
						</tr>
						<!--<tr>
							<td><strong>City: </strong><%'=rsWSReport("city")%>&nbsp;&nbsp;&nbsp;&nbsp;<strong>County: </strong><%'=rsWSReport("county")%></td>
						</tr>-->
						<tr>
							<td><strong>Sample #: </strong><%=rsWSReport("sampleNumber")%></td>
						</tr>
						<tr>
							<td><strong>Date Sample was taken: </strong><%=rsWSReport("dateSampleTaken")%></td>
						</tr>
						<tr>
							<td><strong>Exact Location of sample: </strong><%=rsWSReport("exactLocation")%></td>
						</tr>
						<tr>
							<td><strong>Rain Amount: </strong><%=rsWSReport("rain")%></td>
						</tr>
						<tr>
							<td>
								<strong>Time Sampled: </strong><%=rsWSReport("timeSampled")%>&nbsp;&nbsp;&nbsp;&nbsp;
								<strong>Sampling Method: </strong><%If rsWSReport("isAutomatic") = True then%> 
									Automatic 
								<%end if%>&nbsp;
								<%If rsWSReport("isManual") = True then%> 
									Manual (Grab) 
								<%end if%>&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong>Sampled By: </strong><%=rsWSReport("sampledBy")%></td>
						</tr>
						<tr>
							<td><img src=images/pix.gif width=20 height=10></td>
						</tr>
						<tr>
							<td class="rowColor"><strong><u>Analysis <%=i%></u></strong></td>
						</tr>
						<tr>
							<td><strong>Date of Analysis: </strong><%=rsWSReport("analysisDate")%>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Time of Analysis: </strong><%=rsWSReport("analysisTime")%></td>
						</tr>
						<tr>
							<td><strong>Analyzed By: </strong><%=rsWSReport("analysisBy")%></td>
						</tr>
						<tr>
							<td><strong>Analytical Method: </strong><%=rsWSReport("analysisMethod")%></td>
						</tr>
						<!--<tr>
							<td><strong>Calibration Date: </strong><%'=rsWSReport("calibrationDate")%>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Calibration Time: </strong><%'=rsWSReport("calibrationTime")%></td>
						</tr>-->
						<tr>
							<td><strong>Results (NTU): </strong><%=rsWSReport("resultsNTU")%></td>
						</tr>
					
					</table>
					
				</td>
			</tr>
	</table>
	
<%rsWSReport.movenext%>

<%if not rsWSReport.eof then%>
	<BR style="page-break-after: always">
<%end if

i = i + 1
loop%>

<table width=650 cellpadding="0" cellspacing="0">
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan="2">
			<em>"I certify under the penalty of law that this report and all attachments were prepared
			under my direction or supervision in accordance with a system designed to assure that
			certified personnel properly gather and evaluate the information submitted. Based on my
			inquiry of the person or persons who manage the system, or those persons directly
			responsible for gathering the information, the information submitted is, to the best of my
			knowledge and belief, true, accurate, and complete. I am aware that there are significant
			penalties for submitting false information, including the possibility of fine and imprisonment
			for knowing violations"<br><br>
			
			"The sampling in this report was conducted in accordance with the applicable Erosion, Sedimentation, 
			and Pollution Control Plan. I certify under the penalty of law that this report and all 
			attachments were prepared under my direction or supervision in accordance with a system designed 
			to assure that certified personnel properly gather and evaluate the information submitted. Based on my
			inquiry of the person or persons who manage the system, or those persons directly
			responsible for gathering the information, the information submitted is, to the best of my
			knowledge and belief, true, accurate, and complete. I am aware that there are significant
			penalties for submitting false information, including the possibility of fine and imprisonment
			for knowing violations."</em><br><br>
			
			<strong>Inspectors Signature:</strong> 
			<%if rsReport("sigFileName") <> "" then%> 
				<img src="userUploads/<%=rsReport("userID")%>/<%=rsReport("sigFileName")%>">&nbsp;&nbsp;
			<%else%>
			______________________________ 
			<%end if%> &nbsp;&nbsp;<strong>Date:</strong> <u><%=rsReport("inspectionDate")%></u><br><br>
			<strong>Title:</strong> <u><%=rsReport("jobtitle")%></u>			
		</td>
	</tr>
</table>
</body>
</html>
<%
function getAnswer(resp)
	If resp = "True" Then
		resp = "Yes"
	else
		resp = "No"
	end if
	getAnswer = resp
end function
%>