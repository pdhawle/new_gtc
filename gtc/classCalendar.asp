<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<%
userID = 0

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")



Dim dDate     ' Date we're displaying calendar for
Dim iDIM      ' Days In Month
Dim iDOW      ' Day Of Week that month starts on
Dim iCurrent  ' Variable we use to hold current day of month as we write table
Dim iPosition ' Variable we use to hold current position in table


' Get selected date.  There are two ways to do this.
' First check if we were passed a full date in RQS("date").
' If so use it, if not look for seperate variables, putting them togeter into a date.
' Lastly check if the date is valid...if not use today
If IsDate(Request.QueryString("date")) Then
	dDate = CDate(Request.QueryString("date"))
Else
	If IsDate(Request.QueryString("month") & "-" & Request.QueryString("day") & "-" & Request.QueryString("year")) Then
		dDate = CDate(Request.QueryString("month") & "-" & Request.QueryString("day") & "-" & Request.QueryString("year"))
	Else
		dDate = Date()
		' The annoyingly bad solution for those of you running IIS3
		If Len(Request.QueryString("month")) <> 0 Or Len(Request.QueryString("day")) <> 0 Or Len(Request.QueryString("year")) <> 0 Or Len(Request.QueryString("date")) <> 0 Then
			Response.Write "The date you picked was not a valid date.  The calendar was set to today's date.<BR><BR>"
		End If
		' The elegant solution for those of you running IIS4
		'If Request.QueryString.Count <> 0 Then Response.Write "The date you picked was not a valid date.  The calendar was set to today's date.<BR><BR>"
	End If
End If

'Now we've got the date.  Now get Days in the choosen month and the day of the week it starts on.
iDIM = GetDaysInMonth(Month(dDate), Year(dDate))
iDOW = GetWeekdayMonthStartsOn(dDate)
%>


<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this event?")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left">
									<span class="grayHeader"><%=sLoginTitle%></span>
									<!--<%'If isUser = "True" then%>
										<span class="Header"> - My Calendar</span>
									<%'else%>
										<span class="Header"> - <%'=sFName%>&nbsp;<%'=sLName%>'s Calendar</span>
									<%'end if%>-->
									<span class="Header"> - Calendar</span>
								</td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						<%if session("superAdmin") = "True" then%>
							&nbsp;&nbsp;<a href="form.asp?formType=addClass&customerID=0" class="footerLink">add open class</a>&nbsp;&nbsp;
						<%end if%>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<span class="required">**</span> class(es)<br>
									&raquo; open class<br>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top">
												<!--start of the calendar-->
													<TABLE width="550" height="500" BORDER=1 CELLSPACING=0 CELLPADDING=1 style="border-collapse: collapse; border: solid;">
														<TR>
															<TD ALIGN="center" COLSPAN=7>
																<TABLE WIDTH=100% BORDER=0 CELLSPACING=0 CELLPADDING=0>
																	<TR>
																		<TD ALIGN="right"><A HREF="classCalendar.asp?date=<%=SubtractOneMonth(dDate)%>&userID=<%=userID%>&sFName=<%=sFName%>&sLName=<%=sLName%>"><img src="images/prev.gif" border="0"></A></TD>
																		<TD ALIGN="center"><B><%= MonthName(Month(dDate)) & "  " & Year(dDate) %></B></TD>
																		<TD ALIGN="left"><A HREF="classCalendar.asp?date=<%=AddOneMonth(dDate)%>&userID=<%=userID%>&sFName=<%=sFName%>&sLName=<%=sLName%>"><img src="images/next.gif" border="0"></A></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR bgcolor="#E3E3E3">
															<TD ALIGN="center"><B>Sun</B></TD>
															<TD ALIGN="center"><B>Mon</B></TD>
															<TD ALIGN="center"><B>Tue</B></TD>
															<TD ALIGN="center"><B>Wed</B></TD>
															<TD ALIGN="center"><B>Thu</B></TD>
															<TD ALIGN="center"><B>Fri</B></TD>
															<TD ALIGN="center"><B>Sat</B></TD>
														</TR>
													<%
													' Write spacer cells at beginning of first row if month doesn't start on a Sunday.
													If iDOW <> 1 Then
														Response.Write vbTab & "<TR>"' & vbCrLf
														iPosition = 1
														Do While iPosition < iDOW
															Response.Write vbTab & vbTab & "<TD>&nbsp;</TD>"' & vbCrLf
															iPosition = iPosition + 1
														Loop
													End If
													
													' Write days of month in proper day slots
													iCurrent = 1
													iPosition = iDOW
													Do While iCurrent <= iDIM
														' If we're at the begginning of a row then write TR
														If iPosition = 1 Then
															Response.Write vbTab & "<TR>"' & vbCrLf
														End If
														
														curDate = Trim(Month(dDate)) & "/" & Trim(iCurrent) & "/" & Trim(Year(dDate))
														
														' If the day we're writing is the selected day then highlight it somehow.
														If iCurrent = Day(dDate) Then
															'write the event(s) here				
															Set oCmd = Server.CreateObject("ADODB.Command")
													
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetDaysClasses"
															   .parameters.Append .CreateParameter("@endDate", adDBTimeStamp, adParamInput, 8, curDate)
															'   .parameters.Append .CreateParameter("@day", adInteger, adParamInput, 8, Trim(iCurrent))
															'   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, Trim(Month(dDate)))
															 '  .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, Trim(Year(dDate)))
															   .CommandType = adCmdStoredProc
															End With
															
															If Err Then
														%>
																<!--#include file="includes/FatalError.inc"-->
														<%
															End If
																	
															Set rsEvent = oCmd.Execute
															If Err Then
														%>
																<!--#include file="includes/FatalError.inc"-->
														<%
															End If
															Set oCmd = nothing
															
															If not rsEvent.eof then
																bEvent = true
															else
																bEvent = false
															end if
						
															set rsEvent = Nothing
															
															If bEvent = true then
																'#" onClick="MM_openBrWindow('./showEvents.asp?date=<%=AddZero(Month(dDate)) & "/" & AddZero(iCurrent) & "/" & Year(dDate)
																
																Response.Write vbTab & vbTab
																'if isUser = "True" then%>
																	<TD valign="top" BGCOLOR="#E3E3E3">
																		<A HREF="classCalendar.asp?date=<%=AddZero(Month(dDate)) & "/" & AddZero(iCurrent) & "/" & Year(dDate)%>"><strong><%=iCurrent%></strong><span class="required">**</span></A>
																	</TD>
																<%'else%>
																	<!--<TD valign="top" BGCOLOR="#E3E3E3"><A HREF="classCalendar.asp?date=<%'=AddZero(Month(dDate)) & "/" & AddZero(iCurrent) & "/" & Year(dDate)%>&userID=<%'=userID%>&sFName=<%'=sFName%>&sLName=<%'=sLName%>"><%'=iCurrent%></A></TD>-->
																<%'end if%>
																	
															<%else
																Response.Write vbTab & vbTab & "<TD BGCOLOR=#E3E3E3 valign=top><B>" & iCurrent & "</B> " & "</TD>"' & vbCrLf
															end if
															
															'clear the text
															 bEvent = false
														Else
															'write the event(s) here
															Set oCmd = Server.CreateObject("ADODB.Command")
													
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetDaysClasses"
															   .parameters.Append .CreateParameter("@endDate", adDBTimeStamp, adParamInput, 8, curDate)
															   .CommandType = adCmdStoredProc
															End With
															
															If Err Then
														%>
																<!--#include file="includes/FatalError.inc"-->
														<%
															End If
																	
															Set rsEvent = oCmd.Execute
															If Err Then
														%>
																<!--#include file="includes/FatalError.inc"-->
														<%
															End If
															Set oCmd = nothing
															
															If not rsEvent.eof then
																bEvent = true
															else
																bEvent = false
															end if
															set rsEvent = Nothing
															
															If bEvent = true then
																Response.Write vbTab & vbTab%>
																	<TD valign="top">
																	<%'if isUser = "True" then%>
																		
																		<A HREF="classCalendar.asp?date=<%=AddZero(Month(dDate)) & "/" & AddZero(iCurrent) & "/" & Year(dDate)%>"><b><%=iCurrent%></b><span class="required">**</span></A></TD>
																	<%'else%>
																		<!--<A HREF="classCalendar.asp?date=<%'=AddZero(Month(dDate)) & "/" & AddZero(iCurrent) & "/" & Year(dDate)%>&userID=<%'=userID%>&sFName=<%'=sFName%>&sLName=<%'=sLName%>"><b><%'=iCurrent%></b></A></TD>-->
																	<%'end if%>
															<%else
																Response.Write vbTab & vbTab & "<TD valign=top><B>" & iCurrent & "</B></TD>"' & vbCrLf
															end if
															'clear the text
															bEvent = false
														End If
														
														' If we're at the endof a row then write /TR
														If iPosition = 7 Then
															Response.Write vbTab & "</TR>"' & vbCrLf
															iPosition = 0
														End If
														
														' Increment variables
														iCurrent = iCurrent + 1
														iPosition = iPosition + 1
													Loop
													
													' Write spacer cells at end of last row if month doesn't end on a Saturday.
													If iPosition <> 1 Then
														Do While iPosition <= 7
															Response.Write vbTab & vbTab & "<TD>&nbsp;</TD>"' & vbCrLf
															iPosition = iPosition + 1
														Loop
														Response.Write vbTab & "</TR>"' & vbCrLf
													End If
													%>
												</TABLE>
												<!--end of the calendar-->				

												</td>
												<td valign="top" width="280">
													<strong>
												
													Classes for <%=MonthName(Month(dDate))%>&nbsp;<%=day(dDate)%>,&nbsp;<%=Year(dDate)%></strong><br>
		
													<%
														Set oCmd = Server.CreateObject("ADODB.Command")
														
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetDaysClasses"
														   .parameters.Append .CreateParameter("@endDate", adDBTimeStamp, adParamInput, 8, dDate)
														   .CommandType = adCmdStoredProc
														End With
													
														Set rsEvent = oCmd.Execute
														Set oCmd = nothing
														if rsEvent.eof then
																response.Write "there are no classes scheduled <br>for " & MonthName(Month(dDate)) & "  " & day(Now()) & ", " & Year(dDate)
														else
															do until rsEvent.eof%>
																<!--open the event in the grey box-->
																<%if rsEvent("customerID") = "0" then
																	bOpen = "True"
																else
																	bOpen = "False"
																end if
																
																if bOpen = "True" Then%>
																	&raquo;
																<%end if%>
																<a href="classDetails.asp?classID=<%=rsEvent("classID")%>" title="Class Details" rel="gb_page_fs[]"><%=rsEvent("classType")%>&nbsp;(<%=rsEvent("startDate")%>&nbsp;-&nbsp;<%=rsEvent("endDate")%>)</a>&nbsp;
																<%if session("superAdmin") = "True" then%>
																	<br>
																	<!--<%'if bOpen = "True" Then%>
																		(<a href="">assign customers/students</a>)
																	<%'end if%>-->
																	(<a href="form.asp?formType=editClass&classID=<%=rsEvent("classID")%>">edit</a>)&nbsp;
																	<input type="image" src="images/delete.gif" width="8" height="9" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rsEvent("classID")%>&processType=deleteClass')">
																<%end if%><br><br>
															<%rsEvent.movenext
															loop
														end if
														
														set rsEvent = Nothing
											
													%>
												</td>
											</tr>
										</table>
										
															
									<!--end of content for the page-->
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>

<%

' ***Begin Function Declaration***
Function GetDaysInMonth(iMonth, iYear)
	Dim dTemp
	dTemp = DateAdd("d", -1, DateSerial(iYear, iMonth + 1, 1))
	GetDaysInMonth = Day(dTemp)
End Function

Function GetWeekdayMonthStartsOn(dAnyDayInTheMonth)
	Dim dTemp
	dTemp = DateAdd("d", -(Day(dAnyDayInTheMonth) - 1), dAnyDayInTheMonth)
	GetWeekdayMonthStartsOn = WeekDay(dTemp)
End Function

Function SubtractOneMonth(dDate)
	SubtractOneMonth = DateAdd("m", -1, dDate)
End Function

Function AddOneMonth(dDate)
	AddOneMonth = DateAdd("m", 1, dDate)
End Function

Function AddZero(dDate)
	If Len(dDate) < 2 Then
		AddZero = 0 & dDate
	Else
		AddZero = dDate
	end If
End Function

' ***End Function Declaration***
%>
