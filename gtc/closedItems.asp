<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

projectID = Request("projectID")
questionID = Request("questionID")
rte = request("rte")
clientID = request("clientID")

ssort = request("sort")
if ssort <> "" then
	columnName = "referenceNumber"
else
	columnName = "inspectionDate"
end if

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
if questionID <> "" then	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetClosedItemsByQuestion"
		.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		.parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, questionID)
	   .CommandType = adCmdStoredProc
	   
	End With
else

	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetClosedItemsByProjectSort"
		.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		.parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
	   .CommandType = adCmdStoredProc
	   
	End With

end if
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerByProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsInfo = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClient"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsClient = oCmd.Execute
Set oCmd = nothing
'response.Write "attach: " & rsClient("clientName")
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<form name="openItems" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Corrected Items</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="openItems.asp?questionID=<%=questionID%>&projectID=<%=projectID%>&rte=<%=rte%>" class="footerLink">view open items</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="15">Customer: <strong><%=rsInfo("customerName")%></strong></td></tr>
										<tr><td colspan="15">Project: <strong><%=rsInfo("projectName")%></strong></td></tr>
										<tr><td colspan="15">Please Note: To sort by item number, click the "Reference #" link below.</td></tr>
										<tr><td colspan="15"><img src="images/pix.gif" width="1" height="3"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">ReportID</span></td>
											<td><span class="searchText">Report Date</span></td>
											<td><span class="searchText"><a href="closedItems.asp?sort=true&projectID=<%=projectID%>&questionID=<%=questionID%>&rte=<%=rte%>" class="footerLink">Reference #(sort)</a></span></td>
											<td><span class="searchText">Corrective Action Needed</span></td>
											<td><span class="searchText">Location</span></td>
											<td><span class="searchText">Corrected  By</span></td>
											<td><span class="searchText">Date Corrected</span></td>
										</tr>
										<tr><td colspan="6"></td><td><img src="images/pix.gif" width="1" height="10"><!--<INPUT type="checkbox" name="closeItem"  value="Check All" onClick="this.value=check(this.form.closeItem);"> check all--></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="10">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><a href="downloads/swsir_<%=rs("reportID")%>.pdf" target="_blank"><%=rs("reportID")%></a></td>
													<td><%=rs("inspectionDate")%></td>
													<td><%=rs("reportID") & "-" & rs("referenceNumber")%></td>
													<td><%=rs("actionNeeded")%></td>
													<td><%=rs("location")%></td>
													<td><%=rs("addressedBy")%></td>
													<td><%=rs("addressedByDate")%></td>
												</tr>
												<%
												if rs("comments") <> "" then
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td colspan="20" align="right">
															<strong>Action Item Comments:</strong>&nbsp;<%=trim(rs("comments"))%>&nbsp;&nbsp;
														</td>
													</tr>
												<%end if%>
												
												<%
												
												If rsClient("attachPhotos") = "True" then
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td colspan="15"><br>
															<strong>Uploaded images:</strong><br>
															<%'this is where we are listing the images
						
															'Dim dirname, mypath, fso, folder, filez, FileCount
															Set fso = CreateObject("Scripting.FileSystemObject")
															
															
															sPath = "downloads/project_" & projectID
															If not fso.FolderExists(server.mappath(sPath)) Then 
																'response.Write server.mappath(sPath)
																fso.CreateFolder(server.mappath(sPath))
															end if
															
															sPath = "downloads/project_" & projectID & "/report_" & rs("reportID")
															If not fso.FolderExists(server.mappath(sPath)) Then 
																'response.Write server.mappath(sPath)
																fso.CreateFolder(server.mappath(sPath))
															end if
															
															sPath = "downloads/project_" & projectID & "/report_" & rs("reportID") & "/responsiveAction_" & rs("responsiveActionID")
															If not fso.FolderExists(server.mappath(sPath)) Then 
																'response.Write server.mappath(sPath)
																fso.CreateFolder(server.mappath(sPath))
															end if
															
											
															Set folder = fso.GetFolder(server.mappath(sPath))
															'Response.Write folder
															Set filez = folder.Files			
															'FileCount = folder.Files.Count
															%>
															<%' Now To the Runtime code:
															Dim strPath 'Path of directory to show
															Dim objFSO 'FileSystemObject variable
															Dim objFolder 'Folder variable
															Dim objItem 'Variable used To Loop through the contents of the folder
															strPath = sPath & "/"
															
															'response.Write strPath & "<br>"
															'response.Write strPath
															' Create our FSO
															Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
															' Get a handle on our folder
											
															Set objFolder = objFSO.GetFolder(Server.MapPath(strPath))
															%>
															<table>
															<%i = 0
															For Each objItem In objFolder.Files
															'replaceText()%>
																<tr>
																	<!--<td><input type="image" src="images/remove.gif" width="11" height="13" border="0" alt="Delete" onClick="return confirmDelete('filedelete.asp?file=<%'=objItem.Name%>&projectID=<%'=rsProject("projectID")%>&reportID=<%'=reportID%>&responsiveActionID=<%'=rs("responsiveActionID")%>')"></td>-->
																	<td valign="top"><%response.Write "&nbsp;<a href=" & strPath&objItem.Name & " target=_blank>" & objItem.Name & "</a><br>"%></td>																	
																</tr>
															<%i=i+1
															NEXT%>
															</table>
															<%
															
															'response.Write i & "<br>"
															
															If i=0 then
																response.Write "there are no images attached to this responsive action"
															end if
															
															Set objItem = Nothing
															Set objFolder = Nothing
															Set objFSO = Nothing
															i=0
															%><!--<br>
															there are no images attached to this responsive action--><br><br>
														</td>
													</tr>
												<%end if%>
											<%
								
											rs.movenext
											
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											
											
											
											loop
										end if%>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>