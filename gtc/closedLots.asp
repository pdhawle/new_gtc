<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

projectID = Request("id")
divisionID = Request("divisionID")
customerID = Request("customerID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClosedLots"
	.parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsInfo = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Closed Lots</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addLots&projectID=<%=projectID%>&divisionID=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">add lot inventory</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="openLots.asp?id=<%=projectID%>&divisionID=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">open lots</a>&nbsp;&nbsp;
						<%if customerID <> "" then%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="projectList.asp?id=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">project list</a>&nbsp;&nbsp;
						<%else%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="javascript:window.close();" class="footerLink">close window</a>&nbsp;&nbsp;
						<%end if%>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="12">Project: <strong><%=rsInfo("projectName")%></strong></td></tr>
										<tr><td colspan="12"><img src="images/pix.gif" width="1" height="3"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Lot #</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Phase #</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Open Date</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Date Closed</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Closed By</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td align="center"><span class="searchText">Utility Locate Log</span></td>
										</tr>
										<tr><td></td><tdcolspan="11"><img src="images/pix.gif" width="1" height="10"><!--<INPUT type="checkbox" name="closeItem"  value="Check All" onClick="this.value=check(this.form.closeItem);"> check all--></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="11">there are no lots to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("lotNumber")%></a></td>
													<td></td>
													<td><%=rs("phase")%></td>
													<td></td>
													<td><%=rs("openDate")%></td>
													<td></td>
													<td><%=rs("closeDate")%></td>
													<td></td>
													<td><%=rs("closedBy")%></td>
													<td></td>
													<td align="center">
														<%if isNull(rs("locateLogID")) or rs("locateLogID") = "" Then%>
															<a href="form.asp?formType=addUtility&lotID=<%=rs("lotID")%>&lotNumber=<%=rs("lotNumber")%>&projectName=<%=rsInfo("projectName")%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>&projectID=<%=projectID%>&pg=cl">Add Entry</a>
														<%else%>
															<a href="viewUtility.asp?locateLogID=<%=rs("locateLogID")%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>&projectID=<%=projectID%>&pg=cl">Edit/View Entry</a>
														<%End if%>
													</td>
												</tr>
											<%
											'get the values from all the checkboxes
											sAllVal = sAllVal & rs("lotID")
											
											rs.movenext
											
											if not rs.eof then
												sAllVal = sAllVal & ","
											end if
											
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											
											
											
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>