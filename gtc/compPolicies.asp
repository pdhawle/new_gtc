<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<form name="openItems" method="post" action="process.asp">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<tr>
		<td colspan="3" class="colorBars">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="15">
												NOTE: Guardrail systems, safety net systems, or personal fall arrest systems <u>must</u> be considered first.  Employees engaged 
												in �leading edge activities�, �precast concrete erection activities� or �residential construction activities� � <u>after 
												proving</u> it would be infeasible or more dangerous to consider one of those conventional three fall protection measures, 
												may devise a fall protection plan that is adequate in preventing fall hazards.<br><br>
												
												Company policies:<br><br>
													
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><b>Ladders:</b>&nbsp;</td>
														<td valign="top">
															In addition to the rules covered in Subpart X and our Company Safety Manual, BARCO, Inc. also requires personal fall 
															protection for employees working on a stepladder or supported ladder within 10� of an unprotected side, edge, or hoist 
															area which is over 6� above a lower level.  A second employee will hold and steady the ladder until the personal fall 
															arrest system is installed for the employee using the ladder, and likewise when the same is removed.
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><b>Scaffolds:</b>&nbsp;</td>
														<td valign="top">
															In addition to the rules covered in Subpart L, BARCO, Inc. also requires guard rail on all scaffolds on working surfaces 
															over 10� above a lower level.
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top" width="50"><b>Boom lifts:</b>&nbsp;</td>
														<td valign="top">
															Any and all employees in a boom lift will wear the proper personal fall protection devices which will be properly attached 
															to a point in the basket so designated by the manufacturer.
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><b>Hoist areas:</b>&nbsp;</td>
														<td valign="top">
															Where guardrail has to be taken down temporarily, it is the strict policy of BARCO, Inc. to establish a controlled access 
															zone with a safety monitor and with working personnel attached to either a tether or a retractable lifeline.  When the 
															guardrail system is reinstalled, either the superintendent or site safety person will inspect before removing the CAZ and 
															safety monitor. Should any of this occur after or before the working hours of the superintendent or safety person, the CAZ 
															will remain until clearance is given to remove it.
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><b>Holes:</b>&nbsp;</td>
														<td valign="top">
															Holes over 2� in their least dimension must be properly covered, secured from moving, color coded and/or identified as a 
															hole cover (�Hole Cover�).
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><b>Harnesses:</b>&nbsp;</td>
														<td valign="top">
															Only full body harnesses are allowed; no safety belts.  Lanyards shall be 6� or less in length and shall be made of synthetic 
															fibers only.  When using lanyards, a �shock absorber� shall be installed between the anchorage and the lanyard.  When attaching 
															the body harness to an anchorage using a retractable device, no shock absorber shall be used.  Double locking snap hooks shall 
															be utilized at all times.  Snap hooks can only be attached to body harnesses or anchorages; never to another snap hook and never 
															back to the same lanyard.
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><b>Anchorages:</b>&nbsp;</td>
														<td valign="top">
															Anchorage points for most applications will have a capacity of at least 5000# and 3000# for retractables.  Most scaffold 
															manufacturers are on record as saying the scaffold shall not be used as an anchorage for fall protection; we concur.  All 
															anchorages shall be inspected by a competent person before utilizing, and such inspection shall be documented.  Manufacturer�s 
															anchorages such as in an aerial lift need only be inspected at delivery and the before each use.
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><b>Inspections:</b>&nbsp;</td>
														<td valign="top">
															Our designated competent person, shall inspect body harnesses, lanyards, shock absorbers, life lines, retractables, rope grabs, 
															and anchorages before each shift.  Such inspections shall be documented and retained for six months past the completion of the 
															project involved.  For that purpose, each item will carry an identification number where practical.
														</td>
													</tr>
												</table>
												
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#002046">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</form>
</body>
</html>