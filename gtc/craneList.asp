<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
projectID = trim(Request("projectID"))
customerID = trim(Request("customerID"))
divisionID = trim(Request("divisionID"))

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCranes"
   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
	1	<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsDiv = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this crane?")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Crane List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addCrane&projectID=<%=projectID%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>" class="footerLink">add new crane</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="projectList.asp?id=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">project list</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="divisionList.asp?ID=<%=customerID%>" class="footerLink">division list</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">	
										<tr>
											<td colspan="9">
												<strong>Customer: </strong><%=rsCust("customerName")%><br>
												<strong>Division: </strong><%=rsDiv("division")%>
											</td>
										</tr>									
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Crane Name</span></td>
											<td><span class="searchText">Crane Number</span></td>
											<td><span class="searchText">Crane Type</span></td>					
											<td align="center"><span class="searchText">Edit</span></td>
											<td align="center"><span class="searchText">Delete</span></td>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="9">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("craneName")%></td>							
													<td><%=rs("craneNumber")%></td>
													<td><%=rs("craneType")%></td>
													<td align="center"><a href="form.asp?craneID=<%=rs("craneID")%>&formType=editCrane&projectID=<%=projectID%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a></td>
													<td align="center"><a href=""><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("craneID")%>&processType=deleteCrane&projectID=<%=projectID%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>')"></a></td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>