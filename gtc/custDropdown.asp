<%if sType <> "frm" then%>
<form name="customerList" method="post" action="customerList.asp">
	<input type="text" name="search">&nbsp;&nbsp;<input type="submit" name="searchBtn" value="Search" class="formButton">
</form>
<%end if%>

<a href="#" data-popupmenu="popmenu"><strong><%=rsCust("customerName")%></strong></a><br>
<!--popup menu here-->
<ul id="popmenu" class="jqpopupmenu">
	<li><a href="form.asp?id=<%=rsCust("customerID")%>&formType=editCustomer">Edit Customer</a></li>
	<li><a href="customerContactList.asp?id=<%=rsCust("customerID")%>">Contacts</a>
		<!--<ul>
			<li><a href="form.asp?formType=addCustomerContact&customerID=<%'=rsCust("customerID")%>">Add Contact</a></li>
			<li><a href="form.asp?formType=addInfoPack&clientID=<%'=session("clientID")%>&customer=<%'=rsCust("customerID")%>">Add Info Pack</a></li>
		</ul>-->
	</li>
	<li><a href="divisionList.asp?id=<%=rsCust("customerID")%>">Divisions</a>
		<%'get a list of the divisions
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetDivisionsByCustomer"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCust("customerID"))
		   .CommandType = adCmdStoredProc																   
		End With
		Set rsDiv = oCmd.Execute
		Set oCmd = nothing
		%>
		<ul>
			<%do until rsDiv.eof%>
				<li><a href="divisionList.asp?id=<%=rsCust("customerID")%>"><%=rsDiv("division")%></a></li>
					<ul>
						<li><a href="projectList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rsCust("customerID")%>">Projects</a></li>
						<li><a href="questionList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rsCust("customerID")%>">NPDES Questions</a></li>
						<!--<li><a href="OSHAquestionList.asp?id=<%'=rsDiv("divisionID")%>&customerID=<'%=rsCust("customerID")%>">OSHA Questions</a></li>
						<li><a href="craneQuestionList.asp?id=<%'=rsDiv("divisionID")%>&customerID=<%'=rsCust("customerID")%>">Crane Questions</a></li>-->
						<li><a href="inspectionTypeList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rsCust("customerID")%>">Inspection Types</a></li>
						<li><a href="projectStatusList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rsCust("customerID")%>">Project Status</a></li>
						<li><a href="weatherList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rsCust("customerID")%>">Weather Conditions</a></li>
						<!--<li><a href="ecDeviceList.asp?id=<%'=rsDiv("divisionID")%>&customerID=<%'=rsCust("customerID")%>">Erosion Control Devices</a></li>-->
						<li><a href="form.asp?formType=addDivision&customerID=<%=rsCust("customerID")%>">Add Division</a></li>
						<li><a href="form.asp?id=<%=rsDiv("divisionID")%>&formType=editDivision&customerID=<%=rsCust("customerID")%>">Edit Division</a></li>
					</ul>
			<%rsDiv.movenext
			loop%>
		</ul>
	</li>
	<li><a href="#">Activities</a>
		<ul>
			<%'if Session("viewQuotes") = "True" then%>
			<!--<li><a href="form.asp?formType=addInfoPack&clientID=<%'=session("clientID")%>&customer=<%'=rsCust("customerID")%>">Add Info Pack</a></li>
			<li><a href="infoPackList.asp?clientID=<%'=session("clientID")%>&customer=<%'=rsCust("customerID")%>">View Info Packs</a></li>
			<li><a href="customerQuoteList.asp?id=<%'=rsCust("customerID")%>">View Quotes</a></li>
			<li><a href="form.asp?formType=addQuote&clientID=<%'=session("clientID")%>">Add Quote</a></li>-->
			<%'end if%>

			<li><a href="form.asp?formType=editConversations&id=<%=rsCust("customerID")%>">Conversations</a></li>
			<li><a href="form.asp?id=<%=rsCust("customerID")%>&formType=editCustomerBilling">Billing</a></li>
		</ul>
	</li>
	<li><a href="#">Documents</a>
		<ul>
			<li><a href="docManagerCust.asp?customerID=<%=rsCust("customerID")%>">Customer Documents</a></li>
			<li><a href="docManager.asp">Project Documents</a></li>
		</ul>
	</li>
	<li><a href="#">Users</a>
		<%'get a list of the divisions
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUsersByCustomer"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCust("customerID"))
		   .CommandType = adCmdStoredProc																   
		End With
		Set rsUser = oCmd.Execute
		Set oCmd = nothing
		%>
		<ul>
			<%do until rsUser.eof%>
			<li><a href="form.asp?id=<%=rsUser("userID")%>&formType=editUser"><%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></a></li>
			<%rsUser.movenext
			loop%>
		</ul>
</ul>
<!--end of popup menu-->
<%=rsCust("address1")%><br>
<%=rsCust("city")%>,&nbsp;<%=rsCust("state")%>&nbsp;<%=rsCust("zip")%><br><br />
