<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

iCustomerID = request("customer")
iDivisionID = request("division")
iProjectID = request("projectID")
dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

if iDivisionID <> "" then

	If iProjectID = "All" then
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetRecReportByDivision" 'specific customer and division
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
		
	else
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetRecReportByProject" 'specific customer and division
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	
	end if
	
else
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetRecReportByCustomer" 'specific customer and all divisions
	   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, iCustomerID)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing

end if

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sCustomerTitle%></span><span class="Header"> - Archive Reports</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="custReportsArchive.asp?customer=<%=iCustomerID%>&division=<%=iDivisionID%>&projectID=<%=iProjectID%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="7">
												<form action="makezip.asp" method="post">
													<%
													'Use the full version or trial as appropriate
												'	Set FileObj = Server.CreateObject("csASPZipFile.MakeZip")
												'	FileObj.DirName = FileObj.CurrentDir & "downloads\"
													%>
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr bgcolor="#666666">
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td><span class="searchText">Report ID</span></td>
															<!--<td><span class="searchText">Customer</span></td>-->
															<td><span class="searchText">Division</span></td>
															<td><span class="searchText">Project</span></td>
															<td><span class="searchText">Report Date</span></td>
															<td><span class="searchText">Report Type</span></td>
															<td align="center"><span class="searchText">View/Print</span></td>
															<td align="center"><span class="searchText">Download</span></td>
														</tr>
												
														<%If rsReport.eof then %>
																<tr><td></td><td colspan="7">there are no reports to display</td></tr>
															<%else
																sFilePath = sRootPathZip '& "downloads\"
															
																blnChange = false
																Do until rsReport.eof
																	If blnChange = true then%>
																		<tr class="rowColor">
																	<%else%>
																		<tr>
																	<%end if%>
																		<td></td>
																		<td><a href="activityLog.asp?reportID=<%=rsReport("reportID")%>" title="" rel="gb_page_fs[]"><%=rsReport("reportID")%></a></td>
																		<!--<td><%'=rsReport("customerName")%></td>-->
																		<td><%=rsReport("division")%></td>
																		<td><%=rsReport("projectName")%></td>
																		<td><%=rsReport("inspectionDate")%></td>
																		<td><%=rsReport("reportType")%></td>
																		<td align="center"><a href="downloads/swsir_<%=rsReport("reportID")%>.pdf" target="_blank">view/print</a></td>
																		<td align="center">
																			<input type="checkbox" name="download" value="<%=rsReport("reportID")%>" &  checked="checked">
																		</td>
																	</tr>
																
															<%
															'get the values from all the checkboxes
															sAllVal = sAllVal & rsReport("reportID")
															rsReport.movenext
															
															if not rs.eof then
																sAllVal = sAllVal & ","
															end if
															
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
															loop
														end if%>
													</table>
													<input type="hidden" name="allBoxes" value="<%=sAllVal%>">
													<!--<input type="hidden" name="directory" value="<%'= Server.HTMLEncode(FileObj.DirName) %>">-->
													<input type="hidden" name="directory" value="<%=sFilePath%>">
												
											</td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td></td>
											<td colspan="6" align="right">
												<input type="submit" value="Download" class="formButton">
											</td>
										</tr>
										</form>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>