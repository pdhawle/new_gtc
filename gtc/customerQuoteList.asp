<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

customerID = request("id")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerQuotes"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

'Create command for customer
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsCust = oCmd.Execute
Set oCmd = nothing
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<link rel="stylesheet" type="text/css" href="menu/popupmenu.css" />
<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/popupmenu.js"></script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Quote List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>
						<!--&nbsp;&nbsp;<a href="form.asp?formType=addDivision&customerID=<%'=customerID%>" class="footerLink">add division</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>-->			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="22">
												<!--#include file="custDropdown.asp"-->
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td width="5"></td>
											<td><span class="searchText">Project Name</span></td>
											<td width="5"></td>
											<td><span class="searchText">Contact Name</span></td>
											<td width="5"></td>
											<td><span class="searchText">Date Created</span></td>
											<td width="5"></td>
											<td><span class="searchText">Created By</span></td>
											<td width="5"></td>
											<td><span class="searchText">Bid Date</span></td>
											<td width="5"></td>
											<td align="center"><span class="searchText">Action</span></td>
										</tr>
										<tr><td colspan="22"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="22">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
											
												If cdate(formatDateTime(rs("bidDate"),2)) < cdate(formatDateTime(now(),2)) then
													bPassed = "True"
												else
													bPassed = "False"
												end if
											
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>									
														<%=rs("projectName")%>														
													</td>
													<td width="5"></td>
													<td>									
														<%=rs("contactName")%>														
													</td>
													<td width="5"></td>
													<td>									
														<%=rs("createDate")%>														
													</td>
													<td width="5"></td>
													<td>									
														<%=rs("whoCreated")%>														
													</td>
													<td width="5"></td>
													<td>									
														<%if bPassed = "True" then
															response.Write "<font color=#EE0000>" & formatDateTime(rs("bidDate"),2) & "</font>"
														else
															response.Write formatDateTime(rs("bidDate"),2)
														end if
														%>													
													</td>
													<td width="5"></td>
													<td align="center">									
														<a href="downloads/quote_<%=rs("inspectionQuoteCustomerID")%>.pdf" target="_blank">view quote</a>													
													</td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>