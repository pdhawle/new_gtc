<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

customerID = request("id")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerQuotes"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

'Create command for customer
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<tr>
		<td colspan="3" bgcolor="#FA702B">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="20">
												<strong>Note:</strong> The PDF of the quote will not be generated until you assign the customer.<br>
												Bid dates in <font color="#EE0000">red</font> have expired.<br>
												<img src="images/statusGreen.gif" align="awarded"> = have been awarded.<br>
												<img src="images/statusYellow.gif" alt="not awarded yet"> = have not been awarded yet.<br>
												<img src="images/statusBlue.gif" alt="contract won"> = contract won by <%=rsClient("clientName")%>.<br>
												<img src="images/statusRed.gif" alt="contract lost by <%=rsClient("clientName")%>"> = contract lost by <%=rsClient("clientName")%>.
											</td>
										</tr>
										<tr><td colspan="22"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="22">
												<strong>Customer:</strong>&nbsp;<%=rsCustomer("customerName")%>
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td width="5"></td>
											<td><span class="searchText">Project Name</span></td>
											<td width="5"></td>
											<td><span class="searchText">Status</span></td>
											<td width="5"></td>
											<td><span class="searchText">Contact Name</span></td>
											<td width="5"></td>
											<td><span class="searchText">Date Created</span></td>
											<td width="5"></td>
											<td><span class="searchText">Created By</span></td>
											<td width="5"></td>
											<td><span class="searchText">Bid Date</span></td>
											<td width="5"></td>
											<td align="center"><span class="searchText">Action</span></td>
										</tr>
										<tr><td colspan="22"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="22">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
											
												If cdate(formatDateTime(rs("bidDate"),2)) < cdate(formatDateTime(now(),2)) then
													bPassed = "True"
												else
													bPassed = "False"
												end if
											
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>									
														<%=rs("projectName")%>														
													</td>
													<td width="5"></td>
													<td>									
														<%'this is where the status icons will appear
														If rs("awarded") = "True" then%>
															<img src="images/statusGreen.gif" alt="awarded">
															<%select case rs("wonContract")
																case "False"%>
																	<img src="images/statusRed.gif" alt="contract lost by <%=rsClient("clientName")%>">
																<%case "True"%>
																	<img src="images/statusBlue.gif" alt="contract won by <%=rsClient("clientName")%>">
																<%case else%>
															<%end select%>
														<%else%>
															<img src="images/statusYellow.gif" alt="not awarded yet">
														<%end if%>													
													</td>
													<td width="5"></td>
													<td>									
														<%=rs("contactName")%>														
													</td>
													<td width="5"></td>
													<td>									
														<%=rs("createDate")%>														
													</td>
													<td width="5"></td>
													<td>									
														<%=rs("whoCreated")%>														
													</td>
													<td width="5"></td>
													<td>									
														<%if bPassed = "True" then
															response.Write "<font color=#EE0000>" & formatDateTime(rs("bidDate"),2) & "</font>"
														else
															response.Write formatDateTime(rs("bidDate"),2)
														end if
														%>													
													</td>
													<td width="5"></td>
													<td align="center">									
														<a href="downloads/quote_<%=rs("inspectionQuoteCustomerID")%>.pdf" target="_blank">view quote</a>	
														&nbsp;|&nbsp;<a href="form.asp?formType=addQuoteComments&quoteID=<%=rs("quoteID")%>&clientID=<%=clientID%>">add/view comments</a>												
													</td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>