<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

customerID = request("id")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivisionsByCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<link rel="stylesheet" type="text/css" href="menu/popupmenu.css" />
<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/popupmenu.js"></script>
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this division?")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Division List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addDivision&customerID=<%=customerID%>" class="footerLink">add division</a>&nbsp;&nbsp;
						<!--<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>	-->		
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="9">
												<!--#include file="custDropdown.asp"-->
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Division</span></td>
											<td align="center"><span class="searchText">Projects</span></td>
											<td align="center"><span class="searchText">NPDES Questions</span></td>
											<!--<td align="center"><span class="searchText">OSHA Questions</span></td>-->
											<td align="center"><span class="searchText">Inspection Types</span></td>
											<td align="center"><span class="searchText">Project Status</span></td>
											<td align="center"><span class="searchText">Weather Conditions</span></td>
											<!--<td align="center"><span class="searchText">Erosion Control Devices</span></td>-->
											<td align="right"><span class="searchText">Edit/Delete</span></td>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="7">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
									
														<%=rs("division")%>
														
													</td>
													<td align="center"><a href="projectList.asp?id=<%=rs("divisionID")%>&customerID=<%=customerID%>">List</a></td>
													<td align="center"><a href="questionList.asp?id=<%=rs("divisionID")%>&customerID=<%=customerID%>">List</a></td>	
													<!--<td align="center"><a href="OSHAquestionList.asp?id=<%'=rs("divisionID")%>&customerID=<%'=customerID%>">List</a></td>	-->					
													<td align="center"><a href="inspectionTypeList.asp?id=<%=rs("divisionID")%>&customerID=<%=customerID%>">List</a></td>
													<td align="center"><a href="projectStatusList.asp?id=<%=rs("divisionID")%>&customerID=<%=customerID%>">List</a></td>
													<td align="center"><a href="weatherList.asp?id=<%=rs("divisionID")%>&customerID=<%=customerID%>">List</a></td>
													<!--<td align="center"><a href="ecDeviceList.asp?id=<%=rs("divisionID")%>&customerID=<%'=customerID%>">List</a></td>-->
													<td align="right"><a href="form.asp?id=<%=rs("divisionID")%>&formType=editDivision&customerID=<%=customerID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("divisionID")%>&processType=deleteDivision&customerID=<%=customerID%>')">&nbsp;</td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>