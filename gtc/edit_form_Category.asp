<%
dim iCategoryID

iCategoryID = request("id")
customerID = Request("customerID")
divisionID = Request("divisionID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategory"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCategoryID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

%>
<form name="editCategory" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Question Category</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top"><strong>Category:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="category" size="50" value="<%=rs("category")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
											
												<input type="hidden" name="divisionID" value="<%=divisionID%>" />
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="CategoryID" value="<%=iCategoryID%>" />
												<input type="hidden" name="processType" value="editCategory" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editCategory");
  frmvalidator.addValidation("category","req","Please enter a category");
</script>

<%
rs.close
set rs = nothing
rsCategory.close
set rsCategory = nothing
DataConn.close
set DataConn = nothing
%>
