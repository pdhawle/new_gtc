<%
On Error Resume Next

iCustomerID = request("id")
sMsgConv=request("sMsgConv")
sType = "frm"

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command for customer
Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCommentTypes"
   .CommandType = adCmdStoredProc   
End With	
Set rsTypes = oCmd.Execute
Set oCmd = nothing

'	Set oCmd = Server.CreateObject("ADODB.Command")	
'	With oCmd
'	   .ActiveConnection = DataConn
'	   .CommandText = "spGetCustomer"
'	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
'	   .CommandType = adCmdStoredProc   
'	End With	
'	Set rsCustomer = oCmd.Execute
'	Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerContacts"
   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, iCustomerID)
   .CommandType = adCmdStoredProc   
End With
Set rsContacts = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<link rel="stylesheet" type="text/css" href="menu/popupmenu.css" />
<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/popupmenu.js"></script>
<script type="text/javascript" src="includes/slicker.js"></script>
<form name="editConversations" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Manage Conversations</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="customerList.asp?search=<%=rsCust("customerName")%>" class="footerLink">customer list</a>&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="form.asp?formType=addCustomerContact&customerID=<%=iCustomerID%>&red=conv" class="footerLink">add contact</a>&nbsp;&nbsp;			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td colspan="3">
												<%if sMsgConv = "True" then%>
													<font color="#EE0000"><strong>Your changes have been made</font></span>
												<%else%>
													<span class="redText">&nbsp;</span>
												<%end if%>
											</td>
										</tr>				
										<tr><td colspan="3" height="5"></td></tr>
										<tr><td colspan="3"><!--#include file="custDropdown.asp"--></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="customerContactID">
													<option value="0">--select contact (optional)--</option>
													<%do until rsContacts.eof%>
														<option value="<%=rsContacts("customerContactID")%>"><%=rsContacts("contactName")%>&nbsp;<%=rsContacts("contactLastName")%></option>
													<%rsContacts.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Conversation Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="commentTypeID">
													
													<%do until rsTypes.eof%>
														<option value="<%=rsTypes("commentTypeID")%>"><%=rsTypes("commentType")%></option>
													<%rsTypes.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>* Comments:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="billComments" rows="8" cols="60"></textarea><br />
												<% 
												dim myLink
												set myLink = new AspSpellLink
												myLink.fields="billComments" 'Sets the target Text Field(s) to be Spell-Checked
												response.write myLink.imageButtonHTML("","","")
												set myLink=nothing
												%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="customerName" value="<%=rsCust("customerName")%>" />
												<input type="hidden" name="customerID" value="<%=iCustomerID%>" />
												<input type="hidden" name="userID" value="<%=session("ID")%>" />
												<input type="hidden" name="processType" value="addConversation" />
												<input type="submit" value="  Save  " class="formButton"/>
												
											</td>
										</tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3">
												<strong class="Header">Previous Conversations</strong><br /><br />
												
												<%Set oCmd = Server.CreateObject("ADODB.Command")	
												With oCmd
												   .ActiveConnection = DataConn
												   .CommandText = "spGetCommentTypes"
												   .CommandType = adCmdStoredProc   
												End With	
												Set rsTypes = oCmd.Execute
												Set oCmd = nothing
												
												'get the comments by comment type												
												do until rsTypes.eof
												
													Set oCmd = Server.CreateObject("ADODB.Command")	
													With oCmd
													   .ActiveConnection = DataConn
													   .CommandText = "spGetCommentsByType"
													   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
													   .parameters.Append .CreateParameter("@commentTypeID", adInteger, adParamInput, 8, rsTypes("commentTypeID"))
													   .CommandType = adCmdStoredProc   
													End With	
													Set rsComments = oCmd.Execute
													Set oCmd = nothing%>
													
													<script type="text/javascript">
														$(document).ready(function() {
														  // we'll put our code here
														  $('#slickbox<%=rsTypes("commentTypeID")%>').hide();
														  
														 // shows the slickbox on clicking the noted link  
														  $('a#slick-show<%=rsTypes("commentTypeID")%>').click(function() {
															$('#slickbox<%=rsTypes("commentTypeID")%>').show('slow');
															return false;
														  });
														  
														 // hides the slickbox on clicking the noted link  
														  $('a#slick-hide<%=rsTypes("commentTypeID")%>').click(function() {
															$('#slickbox<%=rsTypes("commentTypeID")%>').hide('fast');
															return false;
														  });
														  
														 $('a#slick-toggle<%=rsTypes("commentTypeID")%>').click(function() {
															$('#slickbox<%=rsTypes("commentTypeID")%>').toggle(400);
															return false;
														  });
														});
													
													</script>
													
													<strong class="Header"><u><%=rsTypes("commentType")%></u></strong>&nbsp;&nbsp;
													<!--<a href="#" id="slick-show<%'=rsTypes("commentTypeID")%>">+</a>&nbsp;&nbsp;<a href="#" id="slick-hide<%'=rsTypes("commentTypeID")%>">-</a>
													&nbsp;&nbsp;--><a href="#" id="slick-toggle<%=rsTypes("commentTypeID")%>">show/hide</a>
													<br />
													<div id="slickbox<%=rsTypes("commentTypeID")%>">
													<table width="848" cellpadding="0" cellspacing="0" border="0">
													<%if rsComments.eof then
														response.Write "There have been no conversations recorded for this type"
													else
														blnChange = true
														do until rsComments.eof
															If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
																<td valign="top" width="50" align="right"><strong><%=formatdatetime(rsComments("dateAdded"),2)%>&nbsp;</strong></td>
																<td valign="top" width="200">
																	<%if rsComments("customerContactID") = 0 then
																		sContact = "NA"
																	else
																		sContact = rsComments("contactName") & " " & rsComments("contactLastName")
																	end if%>
																	<em>(<%=rsComments("firstName")%>&nbsp;<%=rsComments("lastName")%>&nbsp;to&nbsp;<%=sContact%>)</em>
																</td>
																<td width="10"></td>
																<td valign="top"><%=rsComments("comments")%></td>
															</tr>
														<%rsComments.movenext
														if blnChange = true then
															blnChange = false
														else
															blnChange = true
														end if
														loop
													end if%>
													</table><br />
													</div>
												<%rsTypes.movenext
												loop%>
												
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>

<%
rsCust.close
rsTypes.close
set rsCust = nothing
set rsTypes = nothing
DataConn.close
set DataConn = nothing
%>