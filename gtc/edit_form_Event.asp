<%
userID = request("userID")
eventID = request("eventID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetEvent"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, eventID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

dtEventDate = rs("eventMonth") & "/" & rs("eventDay") & "/" & rs("eventYear")
%>

<form name="editEvent" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Edit Event</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Event Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="eventName" size="30" value="<%=rs("eventName")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Event Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="eventDate" size="10" maxlength="10" value="<%=dtEventDate%>"/>&nbsp;<a href="javascript:displayDatePicker('eventDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<table>
													<tr>
														<td align="right"><span class="required">*</span> <strong>From:</strong> </td>
														<td><input id="timeFrom" name="timeFrom" type="text" value="<%=rs("timeFrom")%>" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,timeFrom)" STYLE="cursor:hand"></td>
													</tr>
													<tr>
														<td align="right"><span class="required">*</span> <strong>To:</strong> </td>
														<td><input id="timeTo" name="timeTo" type="text" value="<%=rs("timeTo")%>" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,timeTo)" STYLE="cursor:hand"></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Event Description/Notes:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="eventDescription" rows="3" cols="23"><%=rs("eventDescription")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>

										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="userID" value="<%=userID%>" />
												<input type="hidden" name="eventID" value="<%=eventID%>" />
												<input type="hidden" name="processType" value="editEvent" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editEvent");
  frmvalidator.addValidation("eventName","req","Please enter an event");
  frmvalidator.addValidation("eventDate","req","Please enter the date of the event");
  frmvalidator.addValidation("timeFrom","req","Please enter the start time of the event");
  frmvalidator.addValidation("timeTo","req","Please the time the event ends");
</script>