<%
infoPackID = request("infoPackID")
clientID = request("clientID")
customerID = request("customerID")
customerContactID = request("customerContactID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerContacts"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetInfoPack"
   .parameters.Append .CreateParameter("@infoPackID", adInteger, adParamInput, 8, infoPackID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsInfoPack = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
<!--

function cust_onchange(addInfoPack) {
   document.addInfoPack.action = "form.asp?formType=addInfoPack";
   addInfoPack.submit(); 
}
// -->
</script>
</head>
<body>
<form name="addInfoPack" method="post" action="form.asp?formType=previewInfoPack">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Verify/Send Info Pack</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<%'if Session("Admin") = "True" then%>
				<!--<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addCustomer&clientID=<%'=clientID%>" class="footerLink" target="_blank">add customer</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>-->
				<%'end if%>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><strong>Customer:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsCustomer("customerName")%>
											</td>
										</tr>						
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date Created:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsInfoPack("date")%>
												
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong>Select Contact:</strong>&nbsp;(verify that the correct contact is selected)<!--&nbsp;<a href="form.asp?formType=addCustomerContact&customerID=<%'=customerID%>" title="Customer Contact" rel="gb_page_center[820, 500]">add contact</a>--></td>
										</tr>
										<tr>
											<td valign="top" align="right"></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%do until rs.eof%>
													<table>
														<tr>
															<td valign="top"><input type="radio" name="contactID" value="<%=rs("customerContactID")%>" <%=isCheckedRadio(rsInfoPack("contactID"),rs("customerContactID"))%>>&nbsp;</td>
															<td>
																<%=rs("contactName")%>&nbsp;<%=rs("contactLastName")%><br>
																<%=rs("customerName")%><br>
																<%=rs("address1")%>
																<%if rs("address2") <> "" then
																	response.Write ", " & rs("address2") & "<br>"
																else
																	response.Write "<br>"
																end if%>
																<%=rs("city")%>,&nbsp;<%=rs("state")%>&nbsp;<%=rs("zip")%>
															</td>
														</tr>
													</table>
													
												<%rs.movenext
												loop%>
												
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Opening Paragraph:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												'	Dim strFormName
												'	Dim strTextAreaName
												'	strFormName = "addInfoPack"
												'	strTextAreaName = "openingParagraph"
													sMsg = rsInfoPack("openingParagraph")
												%>
												<%'if reportType <> "Daily" then response.Write sOIBody end if%>
											<!--	<textarea name="openingParagraph" cols="70" rows="10"><%'=sMsg%></textarea>
												<script>
													//STEP 2: Replace the textarea (txtContent)
													oEdit1 = new InnovaEditor("oEdit1");
													oEdit1.features=["FullScreen","Preview","Print","Search",
														"Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
														"ForeColor","BackColor","|","Bookmark","Hyperlink","XHTMLSource","BRK",
														"Numbering","Bullets","|","Indent","Outdent","LTR","RTL","|",
														"Image","Flash","Media","|","Table","Guidelines","Absolute","|",
														"Characters","Line","Form","RemoveFormat","ClearAll","BRK",
														"StyleAndFormatting","TextFormatting","ListFormatting","BoxFormatting",
														"ParagraphFormatting","CssText","|",
														"Paragraph","FontName","FontSize","|",
														"Bold","Italic","Underline","Strikethrough","Superscript","Subscript","|",
														"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"];
													oEdit1.REPLACE("openingParagraph");
												</script>-->
												<textarea name="openingParagraph" cols="60" rows="7"><%=sMsg%></textarea>
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="v" value="True" />
												<input type="hidden" name="infoPackID" value="<%=infoPackID%>"/>
												<input type="hidden" name="customer" value="<%=customerID%>"/>
												<input type="hidden" name="userID" value="<%=rsInfoPack("userID")%>"/>
												<input type="hidden" name="clientID" value="<%=clientID%>"/>
												<input type="hidden" name="processType" value="addInfoPack" />
												<input type="submit" value="  Preview  " class="formButton"/>
											</td>
										</tr>
										
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addInfoPack");
 // frmvalidator.addValidation("numberOfLots","req","Please enter the number of lots you need");
 // frmvalidator.addValidation("numberOfLots","numeric");
 // frmvalidator.addValidation("startingLotNumber","req","Please enter the starting lot number");
 // frmvalidator.addValidation("startingLotNumber","numeric");
</script>
<%
rsPhase.close
set rsPhase = nothing
DataConn.close
set DataConn = nothing
%>
