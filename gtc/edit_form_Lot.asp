<%
lotID = request("lotID")
projectID = request("projectID")
customerID = request("customerID")
divisionID = request("divisionID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetLot"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, lotID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsLot = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetPhases"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsPhase = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStreetsByProject"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsStreets = oCmd.Execute
Set oCmd = nothing
%>

<form name="editLot" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Lot</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top"><strong>Lot #:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="lotNumber" size="2" maxlength="5" value="<%=rsLot("lotNumber")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top"><strong>Phase #:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="phase">
													<%do until rsPhase.eof
														If trim(rsLot("phase")) = trim(rsPhase("phase")) then%>
															<Option selected="selected" value="<%=rsPhase("phase")%>"><%=rsPhase("phase")%></Option>
														<%else%>
															<Option value="<%=rsPhase("phase")%>"><%=rsPhase("phase")%></Option>
													<%   end if
													rsPhase.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top"><strong>Street Name #:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="streetID">
													<%do until rsStreets.eof
														If trim(rsLot("streetID")) = trim(rsStreets("streetID")) then%>
															<Option selected="selected" value="<%=rsStreets("streetID")%>"><%=rsStreets("streetName")%></Option>
														<%else%>
															<Option value="<%=rsStreets("streetID")%>"><%=rsStreets("streetName")%></Option>
													<%end if
													rsStreets.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top"><strong>Lot Status:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="lotStatusID">
													<%If trim(rsLot("lotStatusID")) = "1" then%>
														<Option selected="selected" value="1">Active</Option>
													<%else%>
														<Option value="1">Active</Option>
													<%end if%>
													<%If trim(rsLot("lotStatusID")) = "2" then%>
														<Option selected="selected" value="2">Completed</Option>
													<%else%>
														<Option value="2">Completed</Option>
													<%end if%>
													<%If trim(rsLot("lotStatusID")) = "3" then%>
														<Option selected="selected" value="3">Vacant</Option>
													<%else%>
														<Option value="3">Vacant</Option>
													<%end if%>
													
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><strong>Outfall:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="outFall" <%=isChecked(rsLot("outFall"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><strong>Curb Inlet:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="curbInlet" <%=isChecked(rsLot("curbInlet"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><strong>Drop Inlet:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="dropInlet" <%=isChecked(rsLot("dropInlet"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><strong>Retention Pond:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="retentionPond" <%=isChecked(rsLot("retentionPond"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><strong>Concrete Washout:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="concreteWashout" <%=isChecked(rsLot("concreteWashout"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><strong>Job Trailer:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="jobTrailer" <%=isChecked(rsLot("jobTrailer"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top"><strong>Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="rate" size="5" maxlength="5" value="<%=rsLot("rate")%>"/> put in whole dollar amount - ex. 50, 75, 25
											</td>
										</tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="lotID" value="<%=lotID%>" />
												<input type="hidden" name="customer" value="<%=customerID%>" />
												<input type="hidden" name="projectID" value="<%=projectID%>" />
												<input type="hidden" name="divisionID" value="<%=divisionID%>" />
												<input type="hidden" name="processType" value="editLot" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editLot");
  frmvalidator.addValidation("lotNumber","req","Please enter a lot number");
</script>