<%
userID = request("userID")
clientID = request("clientID")
mileageID = request("mileageID")

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
													
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc
End With
		
Set rsUser = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
													
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetMiles"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, mileageID)
   .CommandType = adCmdStoredProc
End With
		
Set rs = oCmd.Execute
Set oCmd = nothing

%>
<script language="javascript">
<!--
function rept_onchange(addEvent,catID,userID) {
   window.location.href = 'form.asp?formType=addTime&catID='+catID+'&userID='+userID;
}

function calcMiles(form) {
	a = form.odometerStart.value;
	b = form.odometerEnd.value;
	var c = a * 1
	var d = b * 1				
			
										
	e = d - c;
	
	if(isNaN(parseInt(e))){
		e = 0
	} 
	
	if(e < 0) {
		e = 0
	}
	
	form.miles.value = e;
	}
//-->
</script>
<form name="addEvent" method="post" action="process.asp">

<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Edit Miles</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><strong>Employee Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="date" size="10" maxlength="10" value="<%=rs("date")%>"/>&nbsp;<a href="javascript:displayDatePicker('date')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Odometer Start:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="odometerStart" size="5" maxlength="10" value="<%=rs("odometerStart")%>" onchange="calcMiles(this.form)"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Odometer End:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="odometerEnd" size="5" maxlength="10" value="<%=rs("odometerEnd")%>" onchange="calcMiles(this.form)"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Total Miles:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="miles" size="5" maxlength="10" value="<%=rs("miles")%>"/>
											</td>
										</tr>		
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Comments:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="comments" rows="3" cols="23"><%=rs("comments")%></textarea>
											</td>
										</tr>								
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="mileageID" value="<%=mileageID%>" />
												<input type="hidden" name="userID" value="<%=userID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="editMiles" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<%if catID <> "" then%>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addEvent");
  //frmvalidator.addValidation("eventName","req","Please enter an event");
  frmvalidator.addValidation("date","req","Please enter the date");
  frmvalidator.addValidation("miles","req","Please enter the total miles");
</script>
<%end if%>