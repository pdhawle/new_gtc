<%
On Error Resume Next

projectID = request("projectID")
primaryFormID = request("id")
notFormID = request("notid")

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNOT"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, notFormID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsNOT = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCoverage"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for county list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCounties"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCounty = oCmd.Execute
Set oCmd = nothing


'Create command for secondary permittee list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNumSecPermittees"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsSP = oCmd.Execute
Set oCmd = nothing
%>

<script language="javascript" type="text/javascript">
<!--

function KeepCount() {

	var NewCount = 0
						
	if (document.NOT.typeConstructionCommercial.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionIndustrial.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionMunicipal.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionDOT.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionUtility.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionResidential.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount2() {

	var NewCount = 0
						
	if (document.NOT.IRWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.IRWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount3() {

	var NewCount = 0
						
	if (document.NOT.RWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.RWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount4() {
  
	var NewCount = 0
						
	if (document.NOT.samplingOfOutfall.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.samplingOfRecievingStream.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.troutStream.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount5() {
  
	var NewCount = 0
						
	if (document.NOT.typePrimary.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeSecondary.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeTertiary.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount6() {

	var NewCount = 0
						
	if (document.NOT.constructionActivityCompleted.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.noLongerOwnerOperator.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
}

function KeepCount7() {
  
	var NewCount = 0
						
	if (document.NOT.primaryPermitteeSubdivision.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.individualLot.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.individualLotWithinSWDA.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
}

//-->
</script>

<form name="NOT" method="post" action="processNOI.asp" onSubmit="return ValidateDate()">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Edit Notice of Termination</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<a href="instructions_NOT.asp" target="_blank">Instructions</a><br><br />
												<strong>To Cease Coverage Under General Permit</strong><br>
												<strong>To Discharge Storm Water Associated With Construction Activity</strong>
											</td>
											<td></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><strong>Permit Type:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("coverageDesiredName")%>
															<input type="hidden" name="coverageDesired" size="30" value="<%=rsNOT("coverageDesiredID")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>I. SITE/PERMITTEE INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Site Project Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("siteProjectName")%>
															<input type="hidden" name="siteProjectName" size="30" value="<%=rsNOT("siteProjectName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>GPS Location of Const. Exit:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<table>
																<tr>
																	<td>
																		Degrees<br /><span class="footer">From 0 to 90</span><br />
																		<%=rsNOT("GPSDegree1")%>
																		<input type="hidden" name="degree1" maxlength="2" size="5" value="<%=rsNOT("GPSDegree1")%>">
																	</td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td>
																		Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																		<%=rsNOT("GPSMinute1")%>&nbsp;/&nbsp; 
																		<%=rsNOT("GPSSecond1")%>
																		<input type="hidden" name="minute1" maxlength="2" size="5" value="<%=rsNOT("GPSMinute1")%>"> 
																		<input type="hidden" name="second1" maxlength="2" size="5" value="<%=rsNOT("GPSSecond1")%>"> 
																		<input type="hidden" name="latitude" value="<%=rsNOT("GPSLat")%>"> North Latitude
																	</td>
																</tr>
																<tr>
																	<td>
																		Degrees<br /><span class="footer">From 0 to 90</span><br />
																		<%=rsNOT("GPSDegree2")%>
																		<input type="hidden" name="degree2" size="5" maxlength="2" value="<%=rsNOT("GPSDegree2")%>">
																	</td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td>
																		Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																		<%=rsNOT("GPSMinute2")%>&nbsp;/&nbsp;
																		<%=rsNOT("GPSSecond2")%>
																		<input type="hidden" name="minute2" size="5" maxlength="2" value="<%=rsNOT("GPSMinute2")%>"> 
																		<input type="hidden" name="second2" maxlength="2" size="5" value="<%=rsNOT("GPSSecond2")%>"> 
																		<input type="hidden" name="longitude" value="<%=rsNOT("GPSLon")%>"> West Longitude
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Street Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("projectAddress")%>
															<input type="hidden" name="projectAddress" size="30" value="<%=rsNOT("projectAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City(if applicable):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("projectCity")%>
															<input type="hidden" name="projectCity" size="30" value="<%=rsNOT("projectCity")%>">&nbsp;&nbsp;
															<strong>County:</strong>&nbsp;&nbsp;
															<%=rsNOT("projectCounty")%>
															<input type="hidden" name="projectCounty" size="30" value="<%=rsNOT("projectCounty")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Subdivision Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("subdivision")%>
															<input type="hidden" name="subdivision" size="30" value="<%=rsNOT("subdivision")%>">&nbsp;&nbsp;<strong>Lot Number:</strong>&nbsp;&nbsp;<input type="text" name="lotNumber" size="30" value="<%=rsNOT("lotNumber")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Owner�s Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("ownerName")%>
															<input type="hidden" name="ownerName" size="30" value="<%=rsNOT("ownerName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("ownerAddress")%>
															<input type="hidden" name="ownerAddress" size="30" value="<%=rsNOT("ownerAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("ownerCity")%>
															<input type="hidden" name="ownerCity" size="20" value="<%=rsNOT("ownerCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<%=rsNOT("ownerState")%>
															<input type="hidden" name="ownerState" size="20" value="<%=rsNOT("ownerState")%>">
															&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<%=rsNOT("ownerZip")%><input type="hidden" name="ownerZip" size="10" value="<%=rsNOT("ownerZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Operator�s Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("operatorName")%>
															<input type="hidden" name="operatorName" size="30" value="<%=rsNOT("operatorName")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<%=rsNOT("operatorPhone")%><input type="hidden" name="operatorPhone" size="30" value="<%=rsNOT("operatorPhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("operatorAddress")%>
															<input type="hidden" name="operatorAddress" size="30" value="<%=rsNOT("operatorAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("operatorCity")%>
															<input type="hidden" name="operatorCity" size="20" value="<%=rsNOT("operatorCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<%=rsNOT("operatorState")%>
															<input type="hidden" name="operatorState" size="20" value="<%=rsNOT("operatorState")%>">
															&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<%=rsNOT("operatorZip")%><input type="hidden" name="operatorZip" size="10" value="<%=rsNOT("operatorZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>Type of Permittee:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="typePrimary" <%=isChecked(rsNOT("typePrimary"))%> onClick="return KeepCount5()" />&nbsp;Primary&nbsp;&nbsp;
															<input type="checkbox" name="typeSecondary" <%=isChecked(rsNOT("typeSecondary"))%> onClick="return KeepCount5()" />&nbsp;Secondary&nbsp;&nbsp;
															<input type="checkbox" name="typeTertiary" <%=isChecked(rsNOT("typeTertiary"))%> onClick="return KeepCount5()" />&nbsp;Tertiary
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Facility Contact:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("facilityContact")%>
															<input type="hidden" name="facilityContact" size="30" value="<%=rsNOT("facilityContact")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<%=rsNOT("facilityContactPhone")%><input type="hidden" name="facilityContactPhone" size="30" value="<%=rsNOT("facilityContactPhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>If Applicable:</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Primary Permittee's Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("primaryPermitteeName")%><input type="hidden" name="primaryPermitteeName" size="30" value="<%=rsNOT("primaryPermitteeName")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;<input type="text" name="primaryPermitteePhone" size="30" value="<%=rsNOT("primaryPermitteePhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("primaryPermitteeAddress")%><input type="hidden" name="primaryPermitteeAddress" size="30" value="<%=rsNOT("primaryPermitteeAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("primaryPermitteeCity")%><input type="hidden" name="primaryPermitteeCity" size="20" value="<%=rsNOT("primaryPermitteeCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<%=rsNOT("primaryPermitteeState")%><input type="hidden" name="primaryPermitteeState" size="2" value="<%=rsNOT("primaryPermitteeState")%>">
															&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<%=rsNOT("primaryPermitteeZip")%><input type="hidden" name="primaryPermitteeZip" size="10" value="<%=rsNOT("primaryPermitteeZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Number of Secondary Permittees:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("secondaryPermittees")%>
															<input type="hidden" name="secondaryPermittees" size="20" value="<%=rsNOT("secondaryPermittees")%>">
														</td>
													</tr>
													
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>II. SITE ACTIVITY INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="constructionActivityCompleted" <%=isChecked(rsNOT("constructionActivityCompleted"))%> onClick="return KeepCount6()" />&nbsp;Construction Activity Completed&nbsp;&nbsp;
															<input type="checkbox" name="noLongerOwnerOperator" <%=isChecked(rsNOT("noLongerOwnerOperator"))%> onClick="return KeepCount6()" />&nbsp;No Longer Owner / Operator of Construction Activity
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
													<tr>
														<td valign="top"><strong>Construction Activity:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="typeConstructionCommercial" <%=isChecked(rsNOT("typeConstructionCommercial"))%> onClick="return KeepCount()" />&nbsp;Commercial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionIndustrial" <%=isChecked(rsNOT("typeConstructionIndustrial"))%> onClick="return KeepCount()" />&nbsp;Industrial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionMunicipal" <%=isChecked(rsNOT("typeConstructionMunicipal"))%> onClick="return KeepCount()" />&nbsp;Municipal&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionDOT" <%=isChecked(rsNOT("typeConstructionDOT"))%> onClick="return KeepCount()" />&nbsp;DOT<br />
															<input type="checkbox" name="typeConstructionUtility" <%=isChecked(rsNOT("typeConstructionUtility"))%> onClick="return KeepCount()" />&nbsp;Utility&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionResidential" <%=isChecked(rsNOT("typeConstructionResidential"))%> onClick="return KeepCount()" />&nbsp;Residential/Subdivision Development
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="primaryPermitteeSubdivision" <%=isChecked(rsNOT("primaryPermitteeSubdivision"))%> onClick="return KeepCount7()" />&nbsp;Primary Permittee of a Subdivision Development, or<br />
															<input type="checkbox" name="individualLot" <%=isChecked(rsNOT("individualLot"))%> onClick="return KeepCount7()" />&nbsp;Individual Lot, or<br />
															<input type="checkbox" name="individualLotWithinSWDA" <%=isChecked(rsNOT("individualLotWithinSWDA"))%> onClick="return KeepCount7()" />&nbsp;Individual Lot within a Surface Water Drainage Area where the Primary Permittee has ceased Permit Coverage
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td><strong>Initial Receiving Water:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("IRWName")%>
															<input type="hidden" name="IRWName" size="30" value="<%=rsNOT("IRWName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Municipal Storm Sewer<br /> System Owner/Operator:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("MSSSOwnerOperator")%>
															<input type="hidden" name="MSSSOwnerOperator" size="30" value="<%=rsNOT("MSSSOwnerOperator")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Name of Receiving Water:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("RWName")%>
															<input type="hidden" name="RWName" size="30" value="<%=rsNOT("RWName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>III. CERTIFICATIONS. (Owner or Operator or both to initial as applicable.)</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify1" size="5" value="<%=rsNOT("certify1")%>">&nbsp;1. I certify under penalty of law that either: (a) all storm water discharges associated
															with construction activity from the portion of the construction activity where I was an Owner or
															Operator have ceased or have been eliminated; (b) all storm water discharges associated with
															construction activity from the identified site that are authorized by General NPDES Permit
															number indicated in Section I of this form have ceased; (c) I am no longer an Owner or
															Operator at the construction site and a new Owner or Operator has assumed operational
															control for those portions of the construction site where I previously had ownership or
															operational control; and/or if I am a primary permittee filing this Notice of Termination under
															Part VI.A.2. of this permit, I will notify by written correspondence to the subsequent legal title
															holder of any remaining lots that these lot Owners and /or Operators will become tertiary
															permittees for purposes of this permit and I will provide these tertiary permittees with the
															primary permittee�s Erosion, Sedimentation and Pollution Control Plan. I understand that by
															submitting this Notice of Termination, that I am no longer authorized to discharge storm water
															associated with construction activity by the general permit, and that discharging pollutants in
															storm water associated with construction activity to waters of Georgia is unlawful under the
															Georgia Water Quality Control Act and the Clean Water Act where the discharge is not
															authorized by a NPDES permit.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify2" size="5" value="<%=rsNOT("certify2")%>">&nbsp;2. I certify under penalty of law that this document and all attachments were
															prepared under my direction or supervision in accordance with a system designed to assure
															that qualified personnel properly gather and evaluate the information submitted. Based upon
															my inquiry of the person or persons who manage the system, or those persons directly
															responsible for gathering the information, the information submitted is, to the best of my
															knowledge and belief, true, accurate, and complete. I am aware that there are significant
															penalties for submitting false information, including the possibility of fine and imprisonment for
															knowing violations.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												</table>
												<table>
													<tr>
														<td valign="top"><strong>Owner�s Printed Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("ownerPrintedName")%>
															<input type="hidden" name="ownerPrintedName" size="30" value="<%=rsNOT("ownerPrintedName")%>">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<%=rsNOT("ownerTitle")%><input type="hidden" name="ownerTitle" size="30" value="<%=rsNOT("ownerTitle")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td colspan="3"><input type="text" name="ownerSignDate" size="10" value="<%=rsNOT("ownerSignDate")%>">&nbsp;<a href="javascript:displayDatePicker('ownerSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>Operator�s Printed Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsNOT("operatorPrintedName")%>
															<input type="hidden" name="operatorPrintedName" size="30" value="<%=rsNOT("operatorPrintedName")%>">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<%=rsNOT("operatorTitle")%><input type="hidden" name="operatorTitle" size="30" value="<%=rsNOT("operatorTitle")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td colspan="3"><input type="text" name="operatorSignDate" size="10" value="<%=rsNOT("operatorSignDate")%>">&nbsp;<a href="javascript:displayDatePicker('operatorSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>				
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<input type="hidden" name="projectID" value="<%=projectID%>" />
															<input type="hidden" name="primaryFormID" value="<%=primaryFormID%>" />
															<input type="hidden" name="notFormID" value="<%=notFormID%>" />
															<input type="hidden" name="processType" value="editNOT" />
															<input type="submit" value="Submit Data" class="formButton"/>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
										
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("NOT");
  //frmvalidator.addValidation("lotNumber","req","Please enter the lot number");
  frmvalidator.addValidation("primaryPermitteeName","req","Please enter the name of the primary permittee");
  frmvalidator.addValidation("primaryPermitteePhone","req","Please enter the primary permittee's phone number");
  frmvalidator.addValidation("primaryPermitteeAddress","req","Please enter the street address of the primary permittee");
  frmvalidator.addValidation("primaryPermitteeCity","req","Please enter the primary permittee's city");
  frmvalidator.addValidation("primaryPermitteeZip","req","Please enter the primary permittee's zip code");    
  //frmvalidator.addValidation("certify1","req","Please enter initials")
  //frmvalidator.addValidation("certify2","req","Please enter initials")
  frmvalidator.addValidation("ownerSignDate","req","Please enter the date the owner signs the form");
  frmvalidator.addValidation("operatorSignDate","req","Please enter the date the operator signs the form"); 

</script>
<%
rs.close
rsState.close
set rs = nothing
set rsState = nothing
DataConn.close
set DataConn = nothing
%>