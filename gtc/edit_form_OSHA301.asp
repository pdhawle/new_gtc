<%
reportID = request("reportID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOSHA301"
   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
   .CommandType = adCmdStoredProc   
End With

Set rsOSHA = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing
%>
<script type="text/javascript">
<!--
function rep_onchange(addOSHA301) {
   document.addOSHA301.action = "form.asp?formType=addOSHA301";
   addOSHA301.submit(); 
}
// -->
</script>
<form name="addOSHA301" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Injury and Illness Incident Report (OSHA 301)</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
									
										<tr>
											<td valign="top" align="right"><strong>Customer:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsOSHA("customerName")%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3"><strong>Information about the employee</strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Full Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="name" value="<%=rsOSHA("name")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Job Title:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="jobTitle" value="<%=rsOSHA("jobTitle")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Street:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="street" value="<%=rsOSHA("street")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="city" value="<%=rsOSHA("city")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="state">
													<%do until rsState.eof
														if trim(rsOSHA("state")) = trim(rsState("stateID")) then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%end if
													rsState.movenext
													loop%>
												</select>&nbsp;&nbsp;<strong>Zip: </strong>	<input type="text" name="zip" size="5" value="<%=rsOSHA("zip")%>" />					
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date of Birth:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateOfBirthMonth" maxlength="2" size="2" value="<%=rsOSHA("dateOfBirthMonth")%>" />&nbsp;<input type="text" name="dateOfBirthDay" maxlength="2" size="2" value="<%=rsOSHA("dateOfBirthDay")%>" />&nbsp;<input type="text" name="dateOfBirthYear" maxlength="4" size="5" value="<%=rsOSHA("dateOfBirthYear")%>" />&nbsp;(mm/dd/yyyy)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date Hired:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateHiredMonth" maxlength="2" size="2" value="<%=rsOSHA("dateHiredMonth")%>" />&nbsp;<input type="text" name="dateHiredDay" maxlength="2" size="2" value="<%=rsOSHA("dateHiredDay")%>" />&nbsp;<input type="text" name="dateHiredYear" maxlength="4" size="5" value="<%=rsOSHA("dateHiredYear")%>" />&nbsp;(mm/dd/yyyy)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Sex:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="sex" value="Male" <%=isSelectedRadio("Male",rsOSHA("sex"))%>/>&nbsp;Male&nbsp;&nbsp;<input type="radio" name="sex" value="Female" <%=isSelectedRadio("Female",rsOSHA("sex"))%>/>&nbsp;Female
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td colspan="3"><strong>Information about the physician or other health care professional</strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Name of Physician:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="physicianName" value="<%=rsOSHA("physicianName")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3"><strong>If treatment was given away from the worksite, where was it given?</strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Facility:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="facility" value="<%=rsOSHA("facility")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Street:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="facilityStreet" value="<%=rsOSHA("facilityStreet")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="facilityCity" value="<%=rsOSHA("facilityCity")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												Set oCmd = Server.CreateObject("ADODB.Command")
	
												With oCmd
												   .ActiveConnection = DataConn
												   .CommandText = "spGetStates"
												   .CommandType = adCmdStoredProc   
												End With
													
												Set rsState = oCmd.Execute
												Set oCmd = nothing
												%>
												<select name="facilityState">
													<%do until rsState.eof
														if trim(rsOSHA("facilityState")) = trim(rsState("stateID")) then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%end if
													rsState.movenext
													loop%>
												</select>&nbsp;&nbsp;<strong>Zip: </strong>	<input type="text" name="facilityZip" size="5" value="<%=rsOSHA("facilityZip")%>" />					
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">
												<strong>Was employee treated in an emergency room?</strong>&nbsp;&nbsp;
												<input type="radio" name="emergencyRoom" value="Yes" <%=isSelectedRadio("Yes",rsOSHA("emergencyRoom"))%>/>&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="emergencyRoom" value="No" <%=isSelectedRadio("No",rsOSHA("emergencyRoom"))%>/>&nbsp;No
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">
												<strong>Was employee hospitalized overnight as an in-patient?</strong>&nbsp;&nbsp;
												<input type="radio" name="hospOvernight" value="Yes" <%=isSelectedRadio("Yes",rsOSHA("hospOvernight"))%>/>&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="hospOvernight" value="No" <%=isSelectedRadio("No",rsOSHA("hospOvernight"))%>/>&nbsp;No
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td colspan="3"><strong>Information about the case</strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Case number from the log:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="caseNumber" size="10" value="<%=rsOSHA("caseNumber")%>"/>&nbsp;<em>(transfer the case number from the Log after you record the case)</em>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date of Injury or Illness:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateOfInjuryMonth" maxlength="2" size="2" value="<%=rsOSHA("dateOfInjuryMonth")%>" />&nbsp;<input type="text" name="dateOfInjuryDay" maxlength="2" size="2" value="<%=rsOSHA("dateOfInjuryDay")%>" />&nbsp;<input type="text" name="dateOfInjuryYear" maxlength="4" size="5" value="<%=rsOSHA("dateOfInjuryYear")%>" />&nbsp;(mm/dd/yyyy)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Time employee began work:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input id="beganWork" name="beganWork" type="text" value="<%=rsOSHA("beganWork")%>" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,beganWork)" STYLE="cursor:hand">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Time of Event:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input id="eventTime" name="eventTime" type="text" value="<%=rsOSHA("eventTime")%>" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,eventTime)" STYLE="cursor:hand">&nbsp;
												<input type="checkbox" name="timeNotDetermined" <%=isChecked(rsOSHA("timeNotDetermined"))%> />&nbsp; Check if time cannot be determined
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">
												<strong>What was the employee doing just before the incident occurred?</strong> Describe the activity, as well as the <br />
												tools, equipment, or material the employee was using. Be Specific. <em>Examples: </em> "climbing a ladder while <br />
												carrying roofing materials"; "spraying chlorine from hand sprayer"; "daily computer key-entry" <br />
												
												<textarea name="doingBefore" cols="50" rows="4"><%=rsOSHA("doingBefore")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">
												<strong>What happened?</strong> Tell us how the injury occurred.  <em>Examples: </em> "when ladder slipped on wet floor, worker <br />
												fell 20 feet"; "worker was sprayed with chlorine when gasket broke during replacement"; "worker <br />
												developed soreness in wrist over time" <br />
												
												<textarea name="whatHappened" cols="50" rows="4"><%=rsOSHA("whatHappened")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">
												<strong>What was the injury or illness?</strong> Tell us the part of the body that was affected and how it was affected; be <br />
												more specific than "hurt", "pain", or "sore". <em>Examples: </em> "strained back"; "chemical burn, hand"; "carpal <br />
												tunnel syndrome"<br />
												
												<textarea name="whatWasInjury" cols="50" rows="4"><%=rsOSHA("whatWasInjury")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">
												<strong>What object or substance directly harmed the employee?</strong>  <em>Examples: </em> "concrete floor"; "chlorine"; <br />
												"radial arm saw". <em>If this question does not apply to the incident, leave it blank.</em><br />
												
												<textarea name="whatObject" cols="50" rows="4"><%=rsOSHA("whatObject")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">
												<strong>If the employee died, when did death occur?</strong> <br />
												Date of Death <input type="text" name="dateOfDeathMonth" maxlength="2" size="2" value="<%=rsOSHA("dateOfDeathMonth")%>" />&nbsp;<input type="text" name="dateOfDeathDay" maxlength="2" size="2" value="<%=rsOSHA("dateOfDeathDay")%>" />&nbsp;<input type="text" name="dateOfDeathYear" maxlength="4" size="5" value="<%=rsOSHA("dateOfDeathYear")%>" />&nbsp;(mm/dd/yyyy)
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr bgcolor="#666666">
											<td colspan="3" height="20">
												&nbsp;<strong class="searchText">Classify The Case</strong>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										
										<tr bgcolor="#DDDDDD">
											<td colspan="3" height="15">												
												&nbsp;CHECK ONLY ONE box for each case
												based on the most serious outcome for
												that case:
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="classifyCase" value="1" <%=isSelectedRadio("1",rsOSHA("classifyCase"))%> />&nbsp;Death
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="classifyCase" value="2" <%=isSelectedRadio("2",rsOSHA("classifyCase"))%>  />&nbsp;Days Away From Work
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="classifyCase" value="3" <%=isSelectedRadio("3",rsOSHA("classifyCase"))%>  />&nbsp;Job Transfer or Restriction
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="classifyCase" value="4" <%=isSelectedRadio("4",rsOSHA("classifyCase"))%>  />&nbsp;Other Recordable Cases
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr bgcolor="#DDDDDD">
											<td colspan="3" height="15">												
												&nbsp;Enter the number of
												days the injured or
												ill worker was:
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="text" name="daysAwayFromWork" size="3" value="<%=rsOSHA("daysAwayFromWork")%>" />&nbsp;Away From Work
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="text" name="daysOnJobTransfer" size="3" value="<%=rsOSHA("daysOnJobTransfer")%>" />&nbsp;On Job Transfer or Restriction										
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr bgcolor="#DDDDDD">
											<td colspan="3" height="15">												
												&nbsp;Check the �Injury� column or
												choose one type of illness:
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="injury" value="1" <%=isSelectedRadio("1",rsOSHA("injury"))%> />&nbsp;Injury
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="injury" value="2" <%=isSelectedRadio("2",rsOSHA("injury"))%> />&nbsp;Skin Disorder
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="injury" value="3" <%=isSelectedRadio("3",rsOSHA("injury"))%> />&nbsp;Respitory Condition
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="injury" value="4" <%=isSelectedRadio("4",rsOSHA("injury"))%> />&nbsp;Posioning
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="injury" value="5" <%=isSelectedRadio("5",rsOSHA("injury"))%> />&nbsp;Hearing Loss
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3">												
												<input type="radio" name="injury" value="6" <%=isSelectedRadio("6",rsOSHA("injury"))%> />&nbsp;All Other Illnesses
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="reportID" value="<%=reportID%>" />
												<input type="hidden" name="customerID" value="<%=rsOSHA("customerID")%>" />
												<input type="hidden" name="userID" value="<%=session("ID")%>" />
												<input type="hidden" name="processType" value="editOSHA301" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  //var frmvalidator  = new Validator("addInspectionType");
  //frmvalidator.addValidation("inspectionType","req","Please enter an inspection type");
</script>