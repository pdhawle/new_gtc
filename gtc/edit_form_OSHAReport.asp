<%
On Error Resume Next
iReportID = request("reportID")



Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOSHAReport"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iReportID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsReport = oCmd.Execute
Set oCmd = nothing

	
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID"))
   .CommandType = adCmdStoredProc
   
End With

Set rsProjInfo = oCmd.Execute
Set oCmd = nothing

%>
<script type="text/javascript">
<!--

function addEvent(ival,ques)
{
//alert (ival)
//ival = the letter
//ques = the question number
var ni = document.getElementById("myDiv"+ival);
//alert (ni)
var numi = document.getElementById("theValue"+ival);
var num = (document.getElementById("theValue"+ival).value -1)+ 2;
numi.value = num;
var divIdName = "answer"+ival+num;
var newdiv = document.createElement('div');
newdiv.setAttribute("id",divIdName);
newdiv.innerHTML = "<table><tr><td>Corrective action "+ival+ "-" +num+"&nbsp;&nbsp;<a href=javascript:displayAnswerPicker('answer_R"+ival+ "-" +num+"@"+ques+"')>select action</a><br><input type=text name=answer_R"+ival+ "-" +num+"@"+ques+" size=40></td><td>Location"+ival+ "-" +num+"<br><input type=text name=location_"+ival+ "-" +num+" size=30></td><td valign=bottom> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a></td></tr></table>";

ni.appendChild(newdiv);

}

function removeEvent(divNum,ival)
{
//alert (divNum)
var d = document.getElementById("myDiv"+ival);
var olddiv = document.getElementById(divNum);
d.removeChild(olddiv);
}

function rept_onchange(addReport,repID) {
   window.location.href = 'form.asp?formType=addOSHAReport&reportType='+repID;
}

function dept_onchange(addReport,repID,divID) {
   window.location.href = 'form.asp?formType=addOSHAReport&division='+divID+'&reportType='+repID;
}

function proj_onchange(addReport,repID,divID,projID) {
   window.location.href = 'form.asp?formType=addOSHAReport&division='+divID+'&project='+projID+'&reportType='+repID;
}


function formControl(submitted) 
{
   if(submitted=="1") 
    {
   addReport.Submit.disabled=true
   document.addReport.submit();
  // alert("Thanks for your comments!")
    }
}

var myWind = ""
function doNew() {
	if (myWind == "" || myWind.closed || myWind.name == undefined) {
		myWind = window.open("uploadImage.asp?project=<%=iProjectID%>","subWindow","HEIGHT=600,WIDTH=1000")
	} else{
		myWind.focus();
	}
}





function addFormFieldGen() {
	var id = document.getElementById("idGen").value;
	//alert (id);
	$("#divTxtGen").append("<span id='row" + id + "'><label for='txt" + id + "'><b>" + id + ".</b>&nbsp;&nbsp;<textarea name=genquestion" + id + " id=genquestion" + id + " rows=5 cols=95 class=textArea></textarea>&nbsp;&nbsp<a href='#' onClick='removeFormFieldGeneral(\"#row" + id + "gen\"); return false;'>Remove</a><span><br><br>");
	
	
	$('#row' + id + 'gen').highlightFade({
		speed:1000
	});
	
	id = (id - 1) + 2;
	document.getElementById("idGen").value = id;
}

function removeFormFieldGeneral(idGen) {
alert (idGen);
	$(idGen + 'gen').remove();
}



function addFormFieldCust() {
	var id = document.getElementById("idCust").value;
	$("#divTxtCust").append("<span id='row" + id + "'><label for='txt" + id + "'><b>" + id + ".</b>&nbsp;&nbsp<textarea name=customer_question@answer" + id + " id=customer_question@answer" + id + " rows=5 cols=95 class=textArea></textarea>&nbsp;&nbsp<a href='#' onClick='removeFormFieldCust(\"#row" + id + "\"); return false;'>Remove</a><span><br><br>");
	
	
	$('#row' + id).highlightFade({
		speed:1000
	});
	
	id = (id - 1) + 2;
	document.getElementById("idCust").value = id;
}

function removeFormFieldCust(idCust) {
//alert (id);
	$(idCust).remove();
}
// -->
</script>
<form name="addReport" method="post" action="process.asp" onsubmit="return submitRTEForm();">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Add Mock OSHA/EPA Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><img src="images/pix.gif" width="10" height="1"></td>
														<td valign="top" align="right"><strong>Report Type:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="reportType" value="<%=rsReport("reportType")%>" />
															<%select case rsReport("reportType")
																case 1%>
																	Mock OSHA
																<%case 2%>
																	Mock EPA
															<%end select%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><img src="images/pix.gif" width="10" height="1"></td>
														<td valign="top" align="right"><strong>Customer/Division:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="division" value="<%=rsReport("divisionID")%>" />
															<%=rsReport("division")%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><strong>Job Site Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="project" value="<%=rsReport("projectID")%>" />
															<%=rsReport("projectName")%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><strong>Inspector:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=Session("Name")%>
															<input type="hidden" name="inspector" value="<%=Session("ID")%>" />
														</td>
													</tr>													
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><strong>Report Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="reportDate" value="<%=rsReport("reportDate")%>" />
															<%=rsReport("reportDate")%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><strong>Inspection Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="inspectionDate" value="<%=rsReport("inspectionDate")%>" />
															<%=rsReport("inspectionDate")%>	
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>													
													<tr>
														<td></td>
														<td valign="top" align="right"><span class="required">*</span> <strong>Jobsite Status:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td valign="top">
															<textarea name="jobsiteStatus" rows="3" cols="70" class="textArea"><%=rsReport("jobsiteStatus")%></textarea>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><span class="required">*</span> <strong>Person Attending Walk-Around:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td valign="top">
															<input type="text" name="attendWalkAround" size="30" maxlength="50" value="<%=rsReport("attendWalkAround")%>" />
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="30"></td></tr>			
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" colspan="3">
															<%														
															Select Case rsReport("reportType")									
																Case 1 'Mock OSHA%>
																	<!--#include file="inc_edit_form_OSHA.asp"-->
																<%case 2 'Mock EPA%>
																	<!--#include file="inc_edit_form_EPA.asp"-->								
																
															<%end select%>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3" align="right">
																<input type="hidden" name="customerID" value="<%=rsProjInfo("customerID")%>" />
																<input type="hidden" name="reportID" value="<%=iReportID%>" />
																<%select case rsReport("reportType")
																	case 1 'Mock OSHA%>
																		<input type="hidden" name="processType" value="editOSHAReport" />
																	<%case 2 'Mock EPA%>
																		<input type="hidden" name="processType" value="editEPAReport" />
																<%end select%>
																 <!--onClick="formControl(1)"-->
																<input type="submit" name="Submit" value="  Save  " class="formButton"/>									
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<!--<script language="JavaScript" type="text/javascript">

  //var frmvalidator  = new Validator("addReport");
  
  //frmvalidator.addValidation("contactNumber","req","Please enter a contact number");
 // frmvalidator.addValidation("reportDate","req","Please enter the report date");
 // frmvalidator.addValidation("inspectionDate","req","Please enter the inspection date");
 // frmvalidator.addValidation("jobsiteStatus","req","Please enter the jobsiteStatus");
 // frmvalidator.addValidation("attendWalkAround","req","Please enter the person that attended the walkaround with you.");
  
</script>-->

<%
rsStatus.close
set rsStatus = nothing
rsDivisions.close
set rsDivisions = nothing
rsInspectors.close
set rsInspectors = nothing
rsInspectionType.close
set rsInspectionType = nothing
rsWeather.close
set rsWeather = nothing
rsCategories.close
set rsCategories = nothing
rsQuestion.close
set rsQuestion = nothing

DataConn.close
set DataConn = nothing
%>