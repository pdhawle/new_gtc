<%
quoteID = request("quoteID")
customerQuoteID = request("customerQuoteID")
userID = request("userID")
customerID = request("customerID")
clientID = request("clientID")
'divisionID = request("divisionID")


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerQuote"
   .parameters.Append .CreateParameter("@customerQuoteID", adInteger, adParamInput, 8, customerQuoteID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsQuote = oCmd.Execute
Set oCmd = nothing

sState = rsQuote("projectState")
if sState = "" then
	sState = request("projectState")
end if

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

If sState <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCountiesByState"
	   .parameters.Append .CreateParameter("@ID", adVarchar, adParamInput, 50, trim(sState))
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing

end if

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc   
End With
	
Set rsUser = oCmd.Execute
Set oCmd = nothing

%>
<script type="text/javascript">
<!--

function proj_onchange(editPLS) {
   document.editPLS.action = "form.asp?formType=editPLS";
   editPLS.submit(); 
}
 -->
</script>
<form name="editPLS" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Project Launch Information</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><strong>Customer/Client:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsCustomer("customerName")%>	
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="quoteID" value="<%=quoteID%>" />
												<input type="hidden" name="customerQuoteID" value="<%=customerQuoteID%>" />
												<input type="hidden" name="userID" value="<%=userID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												projectName = rsQuote("projectName")
												if projectName = "" then
													projectName = request("projectName")
												end if
												%>
												<input type="text" name="projectName" size="30" value="<%=projectName%>" />
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												projectAddress = rsQuote("projectAddress")
												if projectAddress = "" then
													projectAddress = request("projectAddress")
												end if
												%>	
												<input type="text" name="projectAddress" value="<%=projectAddress%>" size="30" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<%
												projectCity = rsQuote("projectCity")
												if projectCity = "" then
													projectCity = request("projectCity")
												end if
												%>	
												<input type="text" name="projectCity" value="<%=projectCity%>" size="30" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<select name="projectState" onChange="return proj_onchange(addPLS)">
													<option value="">--select state--</option>
													<%do until rsState.eof
														if sState = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%end if
													rsState.movenext
													loop
													srState.movefirst%>
												</select>&nbsp;&nbsp;<span class="required">*</span> <b>Zip:</b>&nbsp;
												<%
												projectZip = rsQuote("projectZip")
												if projectZip = "" then
													projectZip = request("projectZip")
												end if
												%>
												<input type="text" name="projectZip" value="<%=projectZip%>" size="3" />									
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>County:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if sState = "" then%>
													Please select a state above.
												<%else
													projectCounty = rsQuote("projectCounty")%>
													<select name="projectCounty" tooltipText="Please select the county of where the project is located.">
														<%do until rsCounty.eof
															if projectCounty = rsCounty("countyID") then%>
																<option selected="selected" value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
															<%else%>
																<option value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
														<%end if
														rsCounty.movenext
														loop%>
													</select>
												<%end if%>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Latitude:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="latitude" size="20" value="<%=rsQuote("latitude")%>" maxlength="20"/>&nbsp;example: 32.81527												
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Longitude:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="longitude" size="20" value="<%=rsQuote("longitude")%>" maxlength="20"/>&nbsp;example: -84.743189												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Directions:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="projectDirections" cols="30" rows="3"><%=rsQuote("projectDirections")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Acres/Phases:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="acresPhases" size="20" value="<%=rsQuote("acresPhases")%>"/>												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Stage:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="stage" value="existing" <%=isCheckedRadio("existing",rsQuote("stage"))%>/>&nbsp;existing&nbsp;&nbsp;
												<input type="radio" name="stage" value="clearing" <%=isCheckedRadio("clearing",rsQuote("stage"))%>/>&nbsp;clearing&nbsp;&nbsp;
												<input type="radio" name="stage" value="grading" <%=isCheckedRadio("grading",rsQuote("stage"))%>/>&nbsp;grading&nbsp;&nbsp;
												<input type="radio" name="stage" value="utilities" <%=isCheckedRadio("utilities",rsQuote("stage"))%>/>&nbsp;utilities&nbsp;&nbsp;
												<input type="radio" name="stage" value="construction" <%=isCheckedRadio("construction",rsQuote("stage"))%>/>&nbsp;construction&nbsp;&nbsp;												
											</td>
										</tr>										
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactName = rsQuote("contactName")
												if contactName = "" then
													contactName = request("contactName")
												end if
												%>	
												<input type="text" name="contactName" maxlength="50" value="<%=contactName%>" size="30"/>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactPhone = rsQuote("contactPhone")
												if contactPhone = "" then
													contactPhone = request("contactPhone")
												end if
												%>
												<input type="text" name="contactPhone" maxlength="50" value="<%=contactPhone%>" size="30"/>
												
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Cell:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactCell = rsQuote("contactCell")
												if contactCell = "" then
													contactCell = request("contactCell")
												end if
												%>
												<input type="text" name="contactCell" maxlength="50" value="<%=contactCell%>" size="30"/>
												
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Fax:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactFax = rsQuote("contactFax")
												if contactFax = "" then
													contactFax = request("contactFax")
												end if
												%>
												<input type="text" name="contactFax" maxlength="50" value="<%=contactFax%>" size="30"/>
												
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactEmail = rsQuote("contactEmail")
												if contactEmail = "" then
													contactEmail = request("contactEmail")
												end if
												%>
												<input type="text" name="contactEmail" value="<%=contactEmail%>" size="50"/>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="processType" value="editPLS" />
												<%if sState = "" then%>
													<input type="submit" value="  Save  " class="formButton" disabled="disabled"/>
												<%else%>
													<input type="submit" value="  Save  " class="formButton"/>
												<%end if%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editPLS");
  frmvalidator.addValidation("projectName","req","Please enter the project name");
  frmvalidator.addValidation("projectAddress","req","Please enter the address for the project");
  frmvalidator.addValidation("projectCity","req","Please enter the city that the project is in");
  frmvalidator.addValidation("projectZip","req","Please enter the project zip code");
  frmvalidator.addValidation("projectDirections","req","Please enter the general directions to the project");
  frmvalidator.addValidation("stage","selone_radio","Please select the stage of the project");
  frmvalidator.addValidation("contactName","req","Please enter the contact name");
  frmvalidator.addValidation("contactPhone","req","Please enter the contact phone number");
  frmvalidator.addValidation("contactCell","req","Please enter the contact cell number");
  frmvalidator.addValidation("contactFax","req","Please enter the contact fax number");
  frmvalidator.addValidation("contactEmail","req","Please enter the contact email address");
</script>