<%
projectID = request("id")
customerID = Request("customerID")
divisionID = request("divisionID")
sState = request("state")
sMsgProjectInfo=request("sMsgProjectInfo")
sMsgRates=request("sMsgRates")
sMsgAlerts=request("sMsgAlerts")

'response.Write sState

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command for customer list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsProject = oCmd.Execute
Set oCmd = nothing


'get the division name and customer name
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsDivision = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedUsers"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsUsers = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'check th value of the state
if rsProject("state") <> "" then
	if sState = "" then
		sState = rsProject("state")
	end if
else
	sState = sState							
end if

'response.Write "s " & sState

If sState <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCountiesByState"
	   .parameters.Append .CreateParameter("@ID", adVarchar, adParamInput, 50, trim(sState))
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing

end if

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetRateTypes"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsRateTypes = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetRegions"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsRegions = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetRegionalOffices"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsRegionalOffices = oCmd.Execute
Set oCmd = nothing
%>
<script type="text/javascript">
<!--
//function proj_onchange(addProject,stateID,customerID,divisionID) {
 //  window.location.href = 'form.asp?formType=addProject&state='+stateID+'&id='+customerID+'&divisionID='+divisionID;
//}

function proj_onchange(editProject) {
   document.editProject.action = "form.asp?formType=editProject#projectInfo";
   editProject.submit(); 
}
// -->
</script>



<!--*****************for the tabs-->
<script src="jquery-1.1.3.1.pack.js" type="text/javascript"></script>
<script src="jquery.history_remote.pack.js" type="text/javascript"></script>
<script src="jquery.tabs.pack.js" type="text/javascript"></script>
<script type="text/javascript">
	<!--
	$(document).ready(function ()
	{	
		$('#myTab').tabs({ fxSlide: true });
	
		
	});
	-->
</script>
<link rel="stylesheet" href="jquery.tabs.css" type="text/css" media="print, projection, screen">
<!-- Additional IE/Win specific style sheet (Conditional Comments) -->
<!--[if lte IE 7]>
<link rel="stylesheet" href="jquery.tabs-ie.css" type="text/css" media="projection, screen">
<![endif]-->
<style type="text/css" media="screen, projection">

	/* Not required for Tabs, just to make this demo look better... */

	body {
		font-size: 14px; /* @ EOMB */
	}
	* html body {
		font-size: 100%; /* @ IE */
	}
	body * {
		font-size: 87.5%;
		font-family: Arial, Trebuchet, Verdana, Helvetica, "Trebuchet MS", sans-serif;
	}
	body * * {
		font-size: 100%;
	}
</style>
<!--*************************end of the tabs-->

<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Project Info</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;&nbsp;<a href="projectList.asp?id=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">project list</a>&nbsp;&nbsp;		
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>

						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									
									<div id="myTab">
										<ul>
											<li><a href="#projectInfo"><span>Project Info</span></a></li>
											<li><a href="#alerts"><span>Alerts</span></a></li>
											<li><a href="#rates"><span>Rates</span></a></li>
											
										</ul>											
										<div id="projectInfo">
											<form name="editProject" method="post" action="process.asp">
											<input type="hidden" name="id" value="<%=projectID%>" />
											<input type="hidden" name="customerID" value="<%=customerID%>" />
											<input type="hidden" name="divisionID" value="<%=divisionID%>" />
											<table border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="3">
														<%if sMsgProjectInfo = "True" then%>
															<font color="#EE0000"><strong>Your changes have been made</font></span>
														<%else%>
															<span class="redText">&nbsp;</span>
														<%end if%><br />
														<strong>Customer:</strong> <%=rsCustomer("customerName")%><br />
														<strong>Division:</strong> <%=rsDivision("division")%><br /><br />
													</td>
												</tr>		
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><span class="required">*</span> <strong>Project Type:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<select name="projectTypeID" onChange="return proj_onchange(editProject)">
															<option value="0">--select project type--</option>
															<option value="1" <%=isSelected("1",rsProject("projectTypeID"))%>>Substation</option>
															<option value="2" <%=isSelected("2",rsProject("projectTypeID"))%>>Transmission Line</option>
														</select>
														
													</td>
												</tr>		
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
																						
												
												<tr>
													<td valign="top" align="right"><span class="required">*</span> <strong>Project Name:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="projectName" size="30" value="<%=rsProject("projectName")%>" tooltipText="Please enter the name of the project."/>
														
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Primary Env-Tech:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<select name="primaryInspector">
															<option value="0">--Select Env-Tech--</option>
															<%do until rsUsers.eof
																if rsProject("primaryInspector") = rsUsers("userID") then%>
																	<option selected="selected" value="<%=rsUsers("userID")%>"><%=rsUsers("firstName") & " " & rsUsers("lastName")%></option>
																<%else%>
																	<option value="<%=rsUsers("userID")%>"><%=rsUsers("firstName") & " " & rsUsers("lastName")%></option>
															<%end if
															rsUsers.movenext
															loop%>
														</select>
													
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><span class="required">*</span> <strong>Project Number:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="projectNumber" size="30" value="<%=rsProject("projectNumber")%>" maxlength="50" tooltipText="Please enter the project number. This is optional and mainly used for DOT projects."/>
														
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>NTU Limit:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="NTULimit" size="10" maxlength="50" value="<%=rsProject("NTULimit")%>"/>
														
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Region:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<select name="regionID">
															<%do until rsRegions.eof
																if trim(rsRegions("regionID")) = trim(rsProject("regionID")) then%>
																	<option selected="selected" value="<%=rsRegions("regionID")%>"><%=rsRegions("region")%></option>
																<%else%>
																	<option value="<%=rsRegions("regionID")%>"><%=rsRegions("region")%></option>
															<%end if
															rsRegions.movenext
															loop%>
														</select>						
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><span class="required">*</span> <strong>State:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><!--,this.getElementsByTagName('option')[this.selectedIndex].value,<%'=customerID%>,<%'=divisionID%>-->
													<%'=sState%>
														<select name="state" onChange="return proj_onchange(editProject)">
															<option value="">--select state--</option>
															<%do until rsState.eof
																if trim(sState) = trim(rsState("stateID")) then%>
																	<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																<%else%>
																	<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
															<%end if
															rsState.movenext
															loop%>
														</select>						
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												
												
												<script type="text/javascript" src="jquery/jquery.ui.js"></script>
												<script type="text/javascript" src="jquery/jquery.asmselect.js"></script>
												<script type="text/javascript">
													$(document).ready(function() {
														$("select[multiple]").asmSelect({
															addItemTarget: 'bottom',
															animate: true,
															highlight: true,
															sortable: true
														});
														
													}); 
											
												</script>
												<link rel="stylesheet" type="text/css" href="jquery/jquery.asmselect.css" />
												
												<tr>
													<td valign="top" align="right"><span class="required">*</span> <strong>County:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<%if sState = "" then%>
															Please select a state above.
														<%else%>
															<select id="county" multiple="multiple" name="county" title="select all that apply">
																<%
																sCounty = split(rsProject("county"), ",") 																	
																do until rsCounty.eof%>
																	<option <%=getmatch(rsCounty("countyID"),sCounty)%> value="<%=trim(rsCounty("countyID"))%>"><%=trim(rsCounty("county"))%></option>
																<%rsCounty.movenext
																loop%>
															</select>
															<%function getmatch(val,sARR)
																For isLoop = LBound(sARR) to UBound(sARR)
																	if trim(val) = trim(sARR(isLoop)) then
																		getmatch = "selected=selected"
																	end if
																next
															end function%>
														<%end if%>
														
													</td>
												</tr>	
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>City:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="city" size="30" value="<%=rsProject("city")%>" maxlength="100" tooltipText="Please enter the city of where the project is located." />
														
													</td>
												</tr>
												
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Regional Office:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<select name="officeID">
															<option></option>
															<%do until rsRegionalOffices.eof%>
																<option <%=isSelected(rsRegionalOffices("officeID"),rsProject("officeID"))%> value="<%=rsRegionalOffices("officeID")%>"><%=rsRegionalOffices("office")%></option>
															<%rsRegionalOffices.movenext
															loop%>
														</select>						
													</td>
												</tr>
												
												<%if request("projectTypeID") = "" then
													iProjType = rsProject("projectTypeID")
												else
													iProjType = request("projectTypeID")
												end if
												
												if iProjType = "2" then%>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Latitude:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="latitude" size="20" value="<%=rsProject("latitude")%>" maxlength="20"/>&nbsp;example: 32.81527												
														</td>
													</tr>	
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Longitude:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="longitude" size="20" value="<%=rsProject("longitude")%>" maxlength="20"/>&nbsp;example: -84.743189												
														</td>
													</tr>				
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Latitude End:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="latitude2" size="20" value="<%=rsProject("latitude2")%>" maxlength="20"/>&nbsp;example: 32.81527												
														</td>
													</tr>	
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Longitude End:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="longitude2" size="20" value="<%=rsProject("longitude2")%>" maxlength="20"/>&nbsp;example: -84.743189												
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<%else%>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Latitude:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="latitude" size="20" value="<%=rsProject("latitude")%>" maxlength="20"/>&nbsp;example: 32.81527												
															<input type="hidden" name="latitude2" value="" />
															<input type="hidden" name="longitude2" value="" />
														</td>
													</tr>	
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Longitude:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="longitude" size="20" value="<%=rsProject("longitude")%>" maxlength="20"/>&nbsp;example: -84.743189												
														</td>
													</tr>				
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<%end if%>	
												
												
												<tr>
													<td valign="top" align="right"><strong>LDA Date:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="startDate" value="<%=rsProject("startDate")%>" maxlength="10" size="10" />&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														
													</td>
												</tr>				
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>NOT Date:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="endDate" value="<%=rsProject("endDate")%>" maxlength="10" size="10" />&nbsp;<a href="javascript:displayDatePicker('endDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														
													</td>
												</tr>				
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Is Active:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="isActive" <%=isChecked(rsProject("isActive"))%>/>						
													</td>
		
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Auto Email:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="autoEmail" <%=isChecked(rsProject("autoEmail"))%>/> *
														
													</td>
												</tr>
									
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td colspan="3" align="right">
														<input type="hidden" name="userName" value="<%=session("name")%>" />
														<input type="hidden" name="projectActive" value="<%=rsProject("isActive")%>"/>		
														<input type="hidden" name="clientID" value="<%=session("clientID")%>"/>	
														<input type="hidden" name="customerName" value="<%=rsCustomer("customerName")%>"/>										
														<input type="hidden" name="division" value="<%=divisionID%>"/>
														<input type="hidden" name="customer" value="<%=rsProject("customerID")%>"/>
														<input type="hidden" name="projectID" value="<%=projectID%>"/>
														<input type="hidden" name="tabName" value="#projectInfo" />
														<input type="hidden" name="processType" value="editProject" />
														<%if sState = "" then%>
															<input type="submit" value="  Save  " class="formButton" disabled="disabled"/>
														<%else%>
															<input type="submit" value="  Save  " class="formButton"/>
														<%end if%>
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
											</table>
											<table>
												<tr>
													<td>
														* When checked, emails will automatically be sent to the contacts we have on file for that project when a report is created. Default is manual.<br />
														Note: You must have contacts added to the project in order for the auto email function to work.<br /><br />
														
														Latitude and Longitude are used for locating the project on the map.
													</td>
												</tr>
											</table>
											</form>
											<script language="JavaScript" type="text/javascript">
											  var frmvalidator  = new Validator("editProject");
											  frmvalidator.addValidation("projectName","req","Please enter the project name");
											  //frmvalidator.addValidation("city","req","Please enter the city that the project is in");
											</script>
										</div>
										<div id="alerts">
											<form name="editProject" method="post" action="process.asp">
											<table border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="3">
														<%if sMsgAlerts = "True" then%>
															<font color="#EE0000"><strong>Your changes have been made</font></span>
														<%else%>
															<span class="redText">&nbsp;</span>
														<%end if%><br />
														<strong>Customer:</strong> <%=rsCustomer("customerName")%><br />
														<strong>Division:</strong> <%=rsDivision("division")%><br />
														<strong>Project:</strong> <%=rsProject("projectName")%><br /><br />
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>* 7 Day Alert:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="sevenDayAlert" <%=isChecked(rsProject("sevenDayAlert"))%>/>
														
													</td>
												</tr>			
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>* 14 Day Alert:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="fourteenDayAlert" <%=isChecked(rsProject("fourteenDayAlert"))%>/>
														
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>** Open Item Alert:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="openItemAlert" <%=isChecked(rsProject("openItemAlert"))%>/>
														
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td colspan="3" align="right">
														<input type="hidden" name="division" value="<%=divisionID%>"/>
														<input type="hidden" name="customer" value="<%=customerID%>"/>
														<input type="hidden" name="projectID" value="<%=projectID%>"/>
														<input type="hidden" name="tabName" value="#alerts" />
														<input type="hidden" name="processType" value="editAlerts" />
														<input type="submit" value="  Save  " class="formButton"/>
													</td>
												</tr>
											</table>
											<table>
												<tr>
													<td>
									
														* When checked, a daily email will automatically be sent to the administrator if there has not been a weekly or post rain report generated<br /> for this project after seven or fourteen days.<br /><br />
														** When checked, a daily email will automatically be sent to the administrator if there are open items for this project that are over seven days old.
													</td>
												</tr>
											</table>
											</form>
										</div>
										<div id="rates">
											<form name="editProject" method="post" action="process.asp">
											<table border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="3">
														<%if sMsgRates = "True" then%>
															<font color="#EE0000"><strong>Your changes have been made</font></span>
														<%else%>
															<span class="redText">&nbsp;</span>
														<%end if%><br />
														<strong>Customer:</strong> <%=rsCustomer("customerName")%><br />
														<strong>Division:</strong> <%=rsDivision("division")%><br />
														<strong>Project:</strong> <%=rsProject("projectName")%><br />
														<strong>Start Date:</strong> <%=rsProject("startDate")%><br /><br />
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												
												<tr>
													<td valign="top" align="right"><strong>New Start Billing Days:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<select name="newStartBillingDays">	
															<option value=""></option>												
															<option value="1" <%=isSelected(rsProject("newStartBillingDays"),1)%>>1</option>
															<option value="2" <%=isSelected(rsProject("newStartBillingDays"),2)%>>2</option>
															<option value="3" <%=isSelected(rsProject("newStartBillingDays"),3)%>>3</option>
															<option value="4" <%=isSelected(rsProject("newStartBillingDays"),4)%>>4</option>
															<option value="5" <%=isSelected(rsProject("newStartBillingDays"),5)%>>5</option>
															<option value="6" <%=isSelected(rsProject("newStartBillingDays"),6)%>>6</option>
															<option value="7" <%=isSelected(rsProject("newStartBillingDays"),7)%>>7</option>
															<option value="8" <%=isSelected(rsProject("newStartBillingDays"),8)%>>8</option>
															<option value="9" <%=isSelected(rsProject("newStartBillingDays"),9)%>>9</option>
															<option value="10" <%=isSelected(rsProject("newStartBillingDays"),10)%>>10</option>
															<option value="11" <%=isSelected(rsProject("newStartBillingDays"),11)%>>11</option>
															<option value="12" <%=isSelected(rsProject("newStartBillingDays"),12)%>>12</option>
															<option value="13" <%=isSelected(rsProject("newStartBillingDays"),13)%>>13</option>
															<option value="14" <%=isSelected(rsProject("newStartBillingDays"),14)%>>14</option>
															<option value="15" <%=isSelected(rsProject("newStartBillingDays"),15)%>>15</option>
															<option value="16" <%=isSelected(rsProject("newStartBillingDays"),16)%>>16</option>
															<option value="17" <%=isSelected(rsProject("newStartBillingDays"),17)%>>17</option>
															<option value="18" <%=isSelected(rsProject("newStartBillingDays"),18)%>>18</option>
															<option value="19" <%=isSelected(rsProject("newStartBillingDays"),19)%>>19</option>
															<option value="20" <%=isSelected(rsProject("newStartBillingDays"),20)%>>20</option>
															<option value="21" <%=isSelected(rsProject("newStartBillingDays"),21)%>>21</option>
														</select>
														<input type="hidden" name="shellBox" />&nbsp;<a href="javascript:displayDatePicker('shellBox')"><img alt="Open Calendar" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Activation Fee:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<%
														if isNull(rsProject("activationFee")) then
															activationFee = 0
														else
															activationFee = rsProject("activationFee")
														end if
														%>
														<input type="text" name="activationFee" size="5" value="<%=formatNumber(activationFee,2)%>" tooltipText="Please enter the activation fee for this project. Example - 250 or 250.00" />
														
													</td>
												</tr>				
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Billing Rate:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<%
														if isNull(rsProject("billRate")) then
															billRate = 0
														else
															billRate = rsProject("billRate")
														end if
														%>
														<input type="text" name="billRate" size="5" value="<%=formatNumber(billRate,2)%>" tooltipText="Please enter the PER MONTH rate that you bill the customer. Example - 600 or 600.00" />
														
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Rate Type/Frequency:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<select name="rateType">
															<%do until rsRateTypes.eof
																if rsProject("rateType") = rsRateTypes("rateTypeID") then%>
																	<option selected="selected" value="<%=rsRateTypes("rateTypeID")%>"><%=rsRateTypes("rateType")%></option>
																<%else%>
																	<option value="<%=rsRateTypes("rateTypeID")%>"><%=rsRateTypes("rateType")%></option>
																
															<%end if
															rsRateTypes.movenext
															loop%>
														</select>
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Water Sampling Rate:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<%
														if isNull(rsProject("waterSamplingRate")) then
															waterSamplingRate = 0
														else
															waterSamplingRate = rsProject("waterSamplingRate")
														end if
														%>
														<input type="text" name="waterSamplingRate" size="5" value="<%=formatNumber(waterSamplingRate,2)%>" tooltipText="Please enter the PER SAMPLE rate that you bill the customer. Example - 125 or 125.00" />
														
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
												<tr><td colspan="3"><strong>Project/Inspection Rates</strong></td></tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Weekly/Post Rain:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<%
														if isNull(rsProject("wprRate")) then
															wprRate = 0
														else
															wprRate = rsProject("wprRate")
														end if
														%>
														<input type="text" name="wprRate" size="5" value="<%=formatNumber(wprRate,2)%>" tooltipText="Please enter the PER INSPECTION rate for weekly and post rain inspections. Example - 200 or 200.00" />
														
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Daily:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<%
														if isNull(rsProject("dRate")) then
															dRate = 0
														else
															dRate = rsProject("dRate")
														end if
														%>
														<input type="text" name="dRate" size="5" value="<%=formatNumber(dRate,2)%>" tooltipText="Please enter PER INSPECTION the rate for daily inspections. Example - 100 or 100.00" />
														
													</td>
												</tr>
												
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td colspan="3" align="right">
														<input type="hidden" name="division" value="<%=divisionID%>"/>
														<input type="hidden" name="customer" value="<%=customerID%>"/>
														<input type="hidden" name="projectID" value="<%=projectID%>"/>
														<input type="hidden" name="tabName" value="#rates" />
														<input type="hidden" name="processType" value="editRates" />
														<input type="submit" value="  Save  " class="formButton"/>
													</td>
												</tr>
											</table>
											</form>
										</div>
									</div>							
									
								
									<!--end of content for the page-->
									
								</td>
							</tr>

						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>

<%
rsCustomer.close
set rsCustomer = nothing
DataConn.close
set DataConn = nothing
%>