<%
dim iQuestionID

iQuestionID = request("id")
customerID = Request("customerID")
divisionID = Request("divisionID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetQuestionByID"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iQuestionID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategories"
   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCategory = oCmd.Execute
Set oCmd = nothing

'Create command
'Set oCmd = Server.CreateObject("ADODB.Command")
	
'With oCmd
'   .ActiveConnection = DataConn
'   .CommandText = "spGetQuestionReportType"
'   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iQuestionID)
'   .CommandType = adCmdStoredProc
   
'End With
			
'Set rsType = oCmd.Execute
'Set oCmd = nothing

%>
<form name="editQuestion" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Question</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top"><strong>Question:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="question" rows="6" cols="30"><%=rs("question")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Spanish:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="secondLanguage" rows="6" cols="30"><%=rs("secondLanguage")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Sort Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="sortNumber" size="5" value="<%=rs("sortNumber")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Category:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="categoryID">
													<%do until rsCategory.eof
														If rsCategory("categoryID") = rs("categoryID") then%>
															<option selected="selected" value="<%=rsCategory("categoryID")%>"><%=rsCategory("category")%></option>
														<%else%>
															<option value="<%=rsCategory("categoryID")%>"><%=rsCategory("category")%></option>
														<%end if
													rsCategory.movenext
													loop%>
												</select>
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Reports:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="weekly" <%=isChecked(rs("weekly"))%> /> Weekly&nbsp;&nbsp;&nbsp;
												<!--<input type="checkbox" name="biweekly" <%'=isChecked(rs("biweekly"))%>/> Bi-Weekly&nbsp;&nbsp;&nbsp;-->
												<input type="checkbox" name="monthly" <%=isChecked(rs("monthly"))%> /> Monthly&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="postRain" <%=isChecked(rs("postRain"))%> /> Post-Rainfall&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="annual" <%=isChecked(rs("annual"))%>/> Annual&nbsp;&nbsp;<br /><br />
												<strong>DOT Specific</strong><br />
												<input type="checkbox" name="petroleum" <%=isChecked(rs("petroleum"))%>/> Petroleum (DOT)&nbsp;&nbsp;&nbsp;<br /><br />
												<strong>Utility Specific</strong><br />
												<input type="checkbox" name="UtilBWPR" <%=isChecked(rs("UtilBWPR"))%>/> Bi-Weekly/Post-Rainfall&nbsp;&nbsp;&nbsp;
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Question Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if rs("questionType") = "1" then%>
													<input type="radio" name="questionType" value="1" checked="checked"/> No Action Recommended/Add Corrective Action/NA<br />
												<%else%>
													<input type="radio" name="questionType" value="1"/> No Action Recommended/Add Corrective Action/NA<br />
												<%end if%>
												<%if rs("questionType") = "2" then%>
													<input type="radio" name="questionType" value="2" checked="checked"/> Yes/No/NA
												<%else%>
													<input type="radio" name="questionType" value="2"/> Yes/No/NA 
												<%end if%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="allBoxes" value="1,3,4">
												<input type="hidden" name="divisionID" value="<%=divisionID%>" />
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="questionID" value="<%=iQuestionID%>" />
												<input type="hidden" name="processType" value="editQuestion" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editQuestion");
  frmvalidator.addValidation("question","req","Please enter a question");
  frmvalidator.addValidation("sortNumber","req","Please enter a sort number");
  frmvalidator.addValidation("sortNumber","num");
</script>

<%
rs.close
set rs = nothing
rsCategory.close
set rsCategory = nothing
DataConn.close
set DataConn = nothing
%>
