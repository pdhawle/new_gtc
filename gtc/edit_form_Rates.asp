<%
projectID = request("id")
customerID = Request("customerID")
divisionID = request("divisionID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command for customer list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsProject = oCmd.Execute
Set oCmd = nothing


'get the division name and customer name
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsDivision = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetRateTypes"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsRateTypes = oCmd.Execute
Set oCmd = nothing
%>
<form name="editProject" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Rates</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td colspan="3">
												<strong>Customer:</strong> <%=rsCustomer("customerName")%><br />
												<strong>Division:</strong> <%=rsDivision("division")%><br />
												<strong>Project:</strong> <%=rsProject("projectName")%><br />
												<strong>Start Date:</strong> <%=rsProject("startDate")%><br /><br />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										
										<tr>
											<td valign="top"><strong>New Start Billing Days:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="newStartBillingDays">	
													<option value=""></option>												
													<option value="1" <%=isSelected(rsProject("newStartBillingDays"),1)%>>1</option>
													<option value="2" <%=isSelected(rsProject("newStartBillingDays"),2)%>>2</option>
													<option value="3" <%=isSelected(rsProject("newStartBillingDays"),3)%>>3</option>
													<option value="4" <%=isSelected(rsProject("newStartBillingDays"),4)%>>4</option>
													<option value="5" <%=isSelected(rsProject("newStartBillingDays"),5)%>>5</option>
													<option value="6" <%=isSelected(rsProject("newStartBillingDays"),6)%>>6</option>
													<option value="7" <%=isSelected(rsProject("newStartBillingDays"),7)%>>7</option>
													<option value="8" <%=isSelected(rsProject("newStartBillingDays"),8)%>>8</option>
													<option value="9" <%=isSelected(rsProject("newStartBillingDays"),9)%>>9</option>
													<option value="10" <%=isSelected(rsProject("newStartBillingDays"),10)%>>10</option>
													<option value="11" <%=isSelected(rsProject("newStartBillingDays"),11)%>>11</option>
													<option value="12" <%=isSelected(rsProject("newStartBillingDays"),12)%>>12</option>
													<option value="13" <%=isSelected(rsProject("newStartBillingDays"),13)%>>13</option>
													<option value="14" <%=isSelected(rsProject("newStartBillingDays"),14)%>>14</option>
													<option value="15" <%=isSelected(rsProject("newStartBillingDays"),15)%>>15</option>
													<option value="16" <%=isSelected(rsProject("newStartBillingDays"),16)%>>16</option>
													<option value="17" <%=isSelected(rsProject("newStartBillingDays"),17)%>>17</option>
													<option value="18" <%=isSelected(rsProject("newStartBillingDays"),18)%>>18</option>
													<option value="19" <%=isSelected(rsProject("newStartBillingDays"),19)%>>19</option>
													<option value="20" <%=isSelected(rsProject("newStartBillingDays"),20)%>>20</option>
													<option value="21" <%=isSelected(rsProject("newStartBillingDays"),21)%>>21</option>
												</select>
												<input type="hidden" name="shellBox" />&nbsp;<a href="javascript:displayDatePicker('shellBox')"><img alt="Open Calendar" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top"><strong>Activation Fee:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												if isNull(rsProject("activationFee")) then
													activationFee = 0
												else
													activationFee = rsProject("activationFee")
												end if
												%>
												<input type="text" name="activationFee" size="5" value="<%=formatNumber(activationFee,2)%>" tooltipText="Please enter the activation fee for this project. Example - 250 or 250.00" />
												
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top"><strong>Billing Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												if isNull(rsProject("billRate")) then
													billRate = 0
												else
													billRate = rsProject("billRate")
												end if
												%>
												<input type="text" name="billRate" size="5" value="<%=formatNumber(billRate,2)%>" tooltipText="Please enter the PER MONTH rate that you bill the customer. Example - 600 or 600.00" />
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top"><strong>Rate Type/Frequency:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="rateType">
													<%do until rsRateTypes.eof
														if rsProject("rateType") = rsRateTypes("rateTypeID") then%>
															<option selected="selected" value="<%=rsRateTypes("rateTypeID")%>"><%=rsRateTypes("rateType")%></option>
														<%else%>
															<option value="<%=rsRateTypes("rateTypeID")%>"><%=rsRateTypes("rateType")%></option>
														
													<%end if
													rsRateTypes.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top"><strong>Water Sampling Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												if isNull(rsProject("waterSamplingRate")) then
													waterSamplingRate = 0
												else
													waterSamplingRate = rsProject("waterSamplingRate")
												end if
												%>
												<input type="text" name="waterSamplingRate" size="5" value="<%=formatNumber(waterSamplingRate,2)%>" tooltipText="Please enter the PER SAMPLE rate that you bill the customer. Example - 125 or 125.00" />
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr><td colspan="3"><strong>Project/Inspection Rates</strong></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Weekly/Post Rain:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												if isNull(rsProject("wprRate")) then
													wprRate = 0
												else
													wprRate = rsProject("wprRate")
												end if
												%>
												<input type="text" name="wprRate" size="5" value="<%=formatNumber(wprRate,2)%>" tooltipText="Please enter the PER INSPECTION rate for weekly and post rain inspections. Example - 200 or 200.00" />
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Daily:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												if isNull(rsProject("dRate")) then
													dRate = 0
												else
													dRate = rsProject("dRate")
												end if
												%>
												<input type="text" name="dRate" size="5" value="<%=formatNumber(dRate,2)%>" tooltipText="Please enter PER INSPECTION the rate for daily inspections. Example - 100 or 100.00" />
												
											</td>
										</tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="division" value="<%=divisionID%>"/>
												<input type="hidden" name="customer" value="<%=customerID%>"/>
												<input type="hidden" name="projectID" value="<%=projectID%>"/>
												<input type="hidden" name="processType" value="editRates" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<%
rsCustomer.close
set rsCustomer = nothing
DataConn.close
set DataConn = nothing
%>