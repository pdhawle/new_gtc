<%
projectID = request("projectID")
iSecondaryID = request("id")
primaryID = request("primaryID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSecondaryNOI"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iSecondaryID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsSecondary = oCmd.Execute
Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for county list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCounties"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCounty = oCmd.Execute
Set oCmd = nothing
%>

<script language="javascript" type="text/javascript">
<!--
function KeepCount() {

	var NewCount = 0
						
	if (document.noiSecondaryEdit.typeConstructionCommercial.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondaryEdit.typeConstructionIndustrial.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondaryEdit.typeConstructionMunicipal.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondaryEdit.typeConstructionDOT.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondaryEdit.typeConstructionUtility.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondaryEdit.typeConstructionResidential.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiSecondaryEdit; return false;
	}
} 

function KeepCount2() {

	var NewCount = 0
						
	if (document.noiSecondaryEdit.IRWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondaryEdit.IRWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiSecondaryEdit; return false;
	}
} 

function KeepCount3() {

	var NewCount = 0
						
	if (document.noiSecondaryEdit.MSSSTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondaryEdit.MSSSWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiSecondaryEdit; return false;
	}
} 
//-->
</script>

<form name="noiSecondaryEdit" method="post" action="processNOI.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Edit Notice of Intent</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<a href="instructions_secondary.asp" target="_blank">Instructions</a><br><br />
												<strong>For Coverage Under General Permit GAR100003</strong><br>
												<strong>To Discharge Storm Water Associated With Construction Activity</strong><br><br>
												<strong>SECONDARY PERMITTEE</strong>
											</td>
											<td></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td colspan="3"><strong>I. SITE/SECONDARY PERMITTEE INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Site/Project Name:</strong><br /><img src="images/pix.gif" width="180" height="1"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="siteProjectName" size="30" value="<%=rsSecondary("siteProjectName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Site Location/Street Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="siteLocation" size="30" value="<%=rsSecondary("siteLocation")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City(if applicable):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%If primaryID <> 0 then%>
															<%=rsSecondary("city")%><input type="hidden" name="city" size="30" value="<%=rsSecondary("city")%>">&nbsp;&nbsp;<strong>County:</strong>
															<%=rsSecondary("county")%><input type="hidden" name="county" size="30" value="<%=rsSecondary("county")%>">&nbsp;&nbsp;
															<%else%>
														
															<input type="text" name="city" size="30" value="<%=rsSecondary("city")%>">&nbsp;&nbsp;
															<strong>County:</strong>&nbsp;&nbsp;<select name="county">
																<%do until rsCounty.eof%>
																	<%if rsCounty("county") = rsSecondary("county") then%>
																		<option selected="selected" value="<%=rsCounty("county")%>"><%=rsCounty("county")%></option>
																	<%else%>
																		<option value="<%=rsCounty("county")%>"><%=rsCounty("county")%></option>
																	<%end if%>
																<%rsCounty.movenext
																loop%>
															</select>&nbsp;&nbsp;
															<%end if%>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Subdivision Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%If primaryID <> 0 then%>
															<%=rsSecondary("subdivision")%><input type="hidden" name="subdivision" size="30" value="<%=rsSecondary("subdivision")%>">
															<%else%>
															<input type="text" name="subdivision" size="30" value="<%=rsSecondary("subdivision")%>">
															<%end if%>
															&nbsp;&nbsp;<strong>Lot Number:</strong>&nbsp;&nbsp;<input type="text" name="lotNumber" size="30" value="<%=rsSecondary("lotNumber")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Secondary Permittee Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="secondaryPermitteeName" size="30" value="<%=rsSecondary("secondaryPermitteeName")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="permitteePhone" size="30" value="<%=rsSecondary("permitteePhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="permitteeAddress" size="30" value="<%=rsSecondary("permitteeAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="permitteeCity" size="20" value="<%=rsSecondary("permitteeCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<select name="permitteeState">
																<%do until rsState.eof%>
																	<%if rsSecondary("permitteeState") = rsState("stateID") then%>
																		<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%else%>
																		<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%end if%>
																<%rsState.movenext
																loop
																rsState.moveFirst%>
															</select>
															&nbsp;&nbsp;<strong>Zip:</strong>&nbsp;&nbsp;<input type="text" name="permitteeZip" size="10" value="<%=rsSecondary("permitteeZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Primary Permittee Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeName" size="30" value="<%=rsSecondary("primaryPermitteeName")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="primaryPermitteePhone" size="30" value="<%=rsSecondary("primaryPermitteePhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeAddress" size="30" value="<%=rsSecondary("primaryPermitteeAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeCity" size="20" value="<%=rsSecondary("primaryPermitteeCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<select name="primaryPermitteeState">
																<%do until rsState.eof%>
																	<%if rsSecondary("primaryPermitteeState") = rsState("stateID") then%>
																		<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%else%>
																		<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%end if%>
																<%rsState.movenext
																loop
																rsState.moveFirst%>
															</select>
															&nbsp;&nbsp;<strong>Zip:</strong>&nbsp;&nbsp;<input type="text" name="primaryPermitteeZip" size="10" value="<%=rsSecondary("primaryPermitteeZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Facility Contact:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="facilityContact" size="30" value="<%=rsSecondary("facilityContact")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="facilityContactPhone" size="30" value="<%=rsSecondary("facilityContactPhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>II. SITE ACTIVITY INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Start Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td valign="top">
															<input type="text" name="startDate" size="10" value="<%=rsSecondary("startDate")%>">&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Completion Date:</strong>&nbsp;&nbsp;<input type="text" name="completionDate" size="10" value="<%=rsSecondary("completionDate")%>">&nbsp;<a href="javascript:displayDatePicker('completionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Estimated Disturbed Acreage:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td valign="top">
															<input type="text" name="estimatedDisturbedAcerage" size="5" value="<%=rsSecondary("estimatedDisturbedAcerage")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>Type Construction Activity:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="typeConstructionCommercial" <%=isChecked(rsSecondary("typeConstructionCommercial"))%> onClick="return KeepCount()"/>&nbsp;Commercial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionIndustrial" <%=isChecked(rsSecondary("typeConstructionIndustrial"))%> onClick="return KeepCount()"/>&nbsp;Industrial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionMunicipal" <%=isChecked(rsSecondary("typeConstructionMunicipal"))%> onClick="return KeepCount()"/>&nbsp;Municipal&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionDOT" <%=isChecked(rsSecondary("typeConstructionDOT"))%> onClick="return KeepCount()"/>&nbsp;DOT<br />
															<input type="checkbox" name="typeConstructionUtility" <%=isChecked(rsSecondary("typeConstructionUtility"))%> onClick="return KeepCount()"/>&nbsp;Utility&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionResidential" <%=isChecked(rsSecondary("typeConstructionResidential"))%> onClick="return KeepCount()"/>&nbsp;Residential
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>III. RECEIVING WATER INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>A. Initial Receiving Water(s):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="IRWName" size="30" value="<%=rsSecondary("IRWName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="IRWTrout" <%=isChecked(rsSecondary("IRWTrout"))%> onClick="return KeepCount2()"/>&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="IRWWarmWater" <%=isChecked(rsSecondary("IRWWarmWater"))%> onClick="return KeepCount2()"/>&nbsp;Warm Water Fisheries Stream
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>B. Municipal Storm Sewer<br />&nbsp;&nbsp;&nbsp; System Owner/Operator:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="MSSSOwnerOperator" size="30" value="<%=rsSecondary("MSSSOwnerOperator")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="MSSSTrout" <%=isChecked(rsSecondary("MSSSTrout"))%> onClick="return KeepCount3()"/>&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="MSSSWarmWater" <%=isChecked(rsSecondary("MSSSWarmWater"))%> onClick="return KeepCount3()"/>&nbsp;Warm Water Fisheries Stream
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>IV. CERTIFICATIONS. (Owner or Operator or both to initial as applicable.)</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify1" size="5" value="<%=rsSecondary("certify1")%>">&nbsp;I certify that I will adhere to the Primary Permittee�s Erosion, Sedimentation and Pollutant Control Plan
															(Plan) or the portion of the Plan applicable to my construction activities.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify2" size="5" value="<%=rsSecondary("certify2")%>">&nbsp;I certify under penalty of law that this document and all attachments were prepared under my direction or
															supervision in accordance with a system designed to assure that qualified personnel properly gather and
															evaluate the information submitted. Based upon my inquiry of the person or persons who manage the
															system, or those persons directly responsible for gathering the information, the information submitted is,
															to the best of my knowledge and belief, true, accurate, and complete. I am aware that there are
															significant penalties for submitting false information, including the possibility of fine and imprisonment for
															knowing violations.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												</table>
												<table>
													<tr>
														<td valign="top"><strong>Secondary Permittee�s Printed Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><input type="text" name="printedName" size="30" value="<%=rsSecondary("printedName")%>">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<input type="text" name="title" size="30" value="<%=rsSecondary("title")%>"></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td colspan="3"><input type="text" name="signDate" size="10" value="<%=rsSecondary("signDate")%>">&nbsp;<a href="javascript:displayDatePicker('signDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<input type="hidden" name="projectID" value="<%=projectID%>" />
															<input type="hidden" name="primaryID" value="<%=primaryID%>" />
															<input type="hidden" name="secondaryFormID" value="<%=iSecondaryID%>" />
															<input type="hidden" name="processType" value="editSecondary" />
															<input type="submit" value="Update Data" class="formButton"/>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("noiSecondaryEdit");
//  frmvalidator.addValidation("siteProjectName","req","Please enter the site project name");
//  frmvalidator.addValidation("siteLocation","req","Please enter the site location/street address");
//  frmvalidator.addValidation("city","req","Please enter the city where the project is located");
//  frmvalidator.addValidation("county","req","Please enter the county where the project is located");
//  frmvalidator.addValidation("subdivision","req","Please enter the name of the subdivision");
//  frmvalidator.addValidation("lotNumber","req","Please enter the lot number");
//  frmvalidator.addValidation("secondaryPermitteeName","req","Please enter the secondary permittee name");
//  frmvalidator.addValidation("permitteePhone","req","Please enter the secondary permittee's phone number");
//  frmvalidator.addValidation("permitteeAddress","req","Please enter the secondary permittee's address");
//  frmvalidator.addValidation("permitteeCity","req","Please enter the secondary permittee's city");
//  frmvalidator.addValidation("permitteeState","req","Please enter the secondary permittee's state");
//  frmvalidator.addValidation("permitteeZip","req","Please enter the secondary permittee's zip code");
//  frmvalidator.addValidation("primaryPermitteeName","req","Please enter the primary permittee name");
//  frmvalidator.addValidation("primaryPermitteePhone","req","Please enter the primary permittee's phone number");
//  frmvalidator.addValidation("primaryPermitteeAddress","req","Please enter the primary permittee's address");
//  frmvalidator.addValidation("primaryPermitteeCity","req","Please enter the primary permittee's city");
//  frmvalidator.addValidation("primaryPermitteeState","req","Please enter the primary permittee's state");
//  frmvalidator.addValidation("primaryPermitteeZip","req","Please enter the primary permittee's zip code")  
//  frmvalidator.addValidation("facilityContact","req","Please enter the facility contact name")
//  frmvalidator.addValidation("facilityContactPhone","req","Please enter the facility contact phone number") 
//  frmvalidator.addValidation("startDate","req","Please enter the start date")
//  frmvalidator.addValidation("completionDate","req","Please enter the completion date")
//  frmvalidator.addValidation("estimatedDisturbedAcerage","req","Please enter the estimated disturbed acerage")
//  frmvalidator.addValidation("estimatedDisturbedAcerage","numeric")  
//  frmvalidator.addValidation("IRWName","req","Please enter the name of Initial Receiving Water(s)")
//  frmvalidator.addValidation("MSSSOwnerOperator","req","Please enter the Municipal Storm Sewer System Owner/Operator")
//  frmvalidator.addValidation("certify1","req","Please enter initials")
//  frmvalidator.addValidation("certify2","req","Please enter initials")
//  frmvalidator.addValidation("printedName","req","Please enter the secondary permittee's printed name");
//  frmvalidator.addValidation("title","req","Please enter the secondary permittee's title");
//  frmvalidator.addValidation("signDate","req","Please enter the date the secondary permittee signs the form");

</script>
<%
rsSecondary.close
rsState.close
set rsSecondary = nothing
set rsState = nothing
DataConn.close
set DataConn = nothing
%>