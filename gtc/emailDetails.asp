<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->

<%
Dim rs, oCmd, DataConn

emailID = request("id")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")

With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetEmail"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, emailID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Sent Email</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><strong>Date Sent:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><%=rs("dateSent")%></td>
										</tr>
										<tr>
											<td><strong>Sent To:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><a href="mailto:<%=rs("sendTo")%>"><%=rs("sendTo")%></a></td>
										</tr>
										<tr>
											<td><strong>CC To:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><a href="mailto:<%=rs("cc")%>"><%=rs("cc")%></a></td>
										</tr>
										<tr>
											<td><strong>BCC To:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><a href="mailto:<%=rs("bcc")%>"><%=rs("bcc")%></a></td>
										</tr>
										<tr>
											<td valign="top"><strong>Attachment 1:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if rs("attach2") <> "" then%>
													<a href="downloads/openItems/<%=rs("attach2")%>" target="_blank"><%=rs("attach2")%></a><br>
												<%end if%>
											</td>
										</tr>
										<tr>
											<td valign="top"><strong>Attachment 2:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if rs("attach3") <> "" then%>
													<a href="downloads/openItems/<%=rs("attach3")%>" target="_blank"><%=rs("attach3")%></a><br>
												<%end if%>
											</td>
										</tr>
										<tr>
											<td valign="top"><strong>Attachment 3:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if rs("attach") <> "" then%>
													<a href="downloads/<%=rs("attach")%>" target="_blank"><%=rs("attach")%></a><br>
												<%end if%>
											</td>
										</tr>
										<tr>
											<td><strong>Subject:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><%=rs("subject")%></td>
										</tr>
										<tr>
											<td valign="top"><strong>Message:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><%=rs("message")%></td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>