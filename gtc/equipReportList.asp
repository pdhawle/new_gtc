<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

sMan = request("man")

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

reportID = request("findReport")


'if the user is assigned to a single project
'get the project, customer, and division and populate the dropdowns and list
If Session("singleProjectUser") = "True" then
	'get the assigned project
	'spGetSingleProjectInfo
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	'may need to change later
	'may want to only display divisions
	'that sre assigned to the current user
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetSingleProjectInfo"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsSingle = oCmd.Execute
	Set oCmd = nothing
	
	iDivisionID = rsSingle("divisionID")
	'reportID = rsSingle("reportID")
	projectID = rsSingle("projectID")
	
else

	iDivisionID = request("division")
	projectID = request("project")

end if	

if reportID <> "" then
	projectID = ""
end if

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
'may need to change later
'may want to only display divisions
'that sre assigned to the current user
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedDivisions"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsDivision = oCmd.Execute
Set oCmd = nothing

	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

If iDivisionID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedProjectsByDivision"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsProject = oCmd.Execute
	Set oCmd = nothing
end if
	

	
if reportID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetReportByID"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, reportID)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReportByID = oCmd.Execute
	Set oCmd = nothing
end if


if projectID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetReportListByProject"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReportByProject = oCmd.Execute
	Set oCmd = nothing
end if
	


%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript" src="includes/rounded-corners.js"></script>
<script type="text/javascript" src="includes/form-field-tooltip.js"></script>
</head>
<script language="javascript">
<!--
function dept_onchange(reportList) {
   reportList.submit(); 
}
//-->
</script>
</head>
<body>
<form name="reportList" method="post" action="equipReportList.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Equipment Report List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr class="colorBars">
											<td colspan="6" valign="middle">
												&nbsp;<span class="searchText">Find report by ID:</span>&nbsp;&nbsp;<input type="text" name="findReport" size="5" tooltipText="To search for a report by it's ID number, enter it here.">&nbsp;&nbsp;<input type="submit" name="search" value="Search" class="formButton">
											</td>
											<td align="right">
												<%if sMan = "" then%>
												<a href="form.asp?formType=addEquipReport" class="footerLink">add new report</a>&nbsp;&nbsp;
												<%end if%>
											</td>
										</tr>
										<tr>
											<td></td>
											<td colspan="7">
												<table>
													<tr><td colspan="2"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr><td colspan="2">Please select a customer/division and a project to view reports.</td></tr>
													<tr><td colspan="2"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td valign="top" align="right">
															<strong>Division:</strong> 
														</td>
														<td>
															<select name="division" onChange="return dept_onchange(reportList)" tooltipText="Select the customer and division here.">
																<option value="">--Select a customer/division--</option>
																<%do until rsDivision.eof
																	If trim(rsDivision("divisionID")) = trim(iDivisionID) then%>
																		<option selected="selected" value="<%=rsDivision("divisionID")%>"><%=rsDivision("customerName")%> - <%=rsDivision("division")%></option>
																	<%else%>
																		<option value="<%=rsDivision("divisionID")%>"><%=rsDivision("customerName")%> - <%=rsDivision("division")%></option>
																	<%end if
																rsDivision.movenext
																loop%>
															</select>
														</td>
													</tr>
													<tr>
														<td valign="top" align="right">
															<strong>Project:</strong> 
														</td>
														<td>
															<select name="project">
																<option value="">--Select a project--</option>
																<%If iDivisionID <> "" then
																	do until rsProject.eof
																		If trim(rsProject("projectID")) = trim(projectID) then%>
																			<option selected="selected" value="<%=rsProject("projectID")%>"><%=rsProject("projectName")%></option>
																		<%else%>
																			<option value="<%=rsProject("projectID")%>"><%=rsProject("projectName")%></option>
																		<%end if
																	rsProject.movenext
																	loop
																end if%>
															</select>
														</td>
													</tr>
													<tr><td colspan="2"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="2" align="right">
															<input type="submit" name="viewReports" value="View Reports" class="formButton">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td colspan="7">
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr bgcolor="#666666">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><span class="searchText">Report ID</span></td>
														<td><span class="searchText">Date</span></td>
														<td><span class="searchText">Division</span></td>
														<td><span class="searchText">Project</span></td>
														<td><span class="searchText">Report Type</span></td>
														<td><span class="searchText">Inspection Type</span></td>
														<td align="center"><span class="searchText">Action</span></td>
														<td><span class="searchText">&nbsp;<!--initial report here--></span></td>
													</tr>
													
													
													<%if reportID <> "" then
														If rsReportByID.eof then %>
															<tr><td></td><td colspan="7">there are no reports to display</td></tr>
														<%else
															Do until rsReportByID.eof%>
																<tr>
																	<td></td>
																	<td><%=rsReportByID("reportID")%></td>
																	<td><%=rsReportByID("inspectionDate")%></td>
																	<td><%=rsReportByID("division")%></td>
																	<td><%=rsReportByID("projectName")%></td>
																	<td><%=rsReportByID("reportType")%></td>
																	<td><%=rsReportByID("inspectionType")%></td>
																	<td align="center">
																		<%if rsReportByID("reportTypeID") <> 10 then%>
																			<a href="form.asp?formType=editReport&reportID=<%=rsReportByID("reportID")%>">edit</a> | 
																		<%end if%>
																		<a href="downloads/swsir_<%=rsReportByID("reportID")%>.pdf" target="_blank">view</a> | 
																		<a href="form.asp?formType=sendEmail&reportID=<%=rsReportByID("reportID")%>&projName=<%=rsReportByID("projectName")%>&repType=<%=rsReportByID("reportType")%>&custName=<%=rsReportByID("customerName")%>&projectID=<%=rsReportByID("projectID")%>&divisionID=<%=rsReportByID("divisionID")%>">email</a>
																	</td>
																	<td>&nbsp;</td>
																</tr>
															<%rsReportByID.movenext
															loop
														end if
													end if%>
													
													<%if projectID <> "" then
														If rsReportByProject.eof then %>
															<tr><td></td><td colspan="7">there are no reports to display</td></tr>
														<%else
															blnChange = false
															Do until rsReportByProject.eof
																If blnChange = true then%>
																	<tr class="rowColor">
																<%else%>
																	<tr>
																<%end if%>
																	<td></td>
																	<td><%=rsReportByProject("reportID")%></td>
																	<td><%=rsReportByProject("inspectionDate")%></td>
																	<td><%=rsReportByProject("division")%></td>
																	<td><%=rsReportByProject("projectName")%></td>
																	<td><%=rsReportByProject("reportType")%></td>
																	<td><%=rsReportByProject("inspectionType")%></td>
																	<td align="center">
																		<%if rsReportByProject("reportTypeID") <> 10 then%>
																			<a href="form.asp?formType=editReport&reportID=<%=rsReportByProject("reportID")%>">edit</a> | 
																		<%end if%>
																		<a href="downloads/swsir_<%=rsReportByProject("reportID")%>.pdf" target="_blank">view</a> | 
																		<!--<a href="buildPDF.asp?reportID=<%'=rsReportByProject("reportID")%>&divisionID=<%'=iDivisionID%>" target="_blank">html</a> | -->
																		<a href="form.asp?formType=sendEmail&reportID=<%=rsReportByProject("reportID")%>&projName=<%=rsReportByProject("projectName")%>&repType=<%=rsReportByProject("reportType")%>&custName=<%=rsReportByProject("customerName")%>&projectID=<%=rsReportByProject("projectID")%>&divisionID=<%=rsReportByProject("divisionID")%>">email</a></td>
																	<td></td>
																</tr>
															<%rsReportByProject.movenext
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
															loop
														end if
													end if%>
													
													
												</table>
											</td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rsDivision.close
rsProject.close
DataConn.close
Set rsDivision = nothing
Set rsProject = nothing
Set DataConn = nothing
Set oCmd = nothing
%>