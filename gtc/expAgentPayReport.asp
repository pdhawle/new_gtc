<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

iProjectID = request("projectID")
inspector = request("inspector")
smonth = request("month")
syear = request("year")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

If iProjectID = "All" Then

	If inspector = "All" then
		'response.Write "1"
		'all projects fo all inspectors
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAgentPayAllProjectsAllAgents"
		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	
	else
		'response.Write "2"
		'all projects fo an inspector
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAgentPayByAgent"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	end if
else

	If inspector = "All" then
		'response.Write "3"
		'all inspectors for a project
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAgentPayByProject"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	else
		'response.Write "4"
		'1 inspector for a project
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAgentPayByProjectandAgent"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	end if

end if


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=agentPayReport.xls" 
%>
<table border="1">
	<tr>
		<td align="left">
			<strong>AGENT PAY REPORT</strong>
		</td>
	</tr>
	<tr><td><img src="images/pix.gif" width="1" height="20"></td></tr>
	<tr>
		<td align="left">
			<strong><%=getMonth(smonth) & " " & sYear%></strong>
		</td>
	</tr>
	<tr>
		<td align="left">
			<strong>Total # of inspection days <%=InspectionDaysInMonth(sYear,smonth)%></strong>
		</td>
	</tr>
	<tr><td><img src="images/pix.gif" width="1" height="20"></td></tr>
	<tr>
		<td>
			<table border="1">
				<tr bgcolor="#666666">
					<td><font color="#EEEEEE">ReportID</font></td>
					<td><font color="#EEEEEE">Customer</font></td>
					<td><font color="#EEEEEE">Project</font></td>
					<td><font color="#EEEEEE">Agent/Inspector</font></td>
					<td><font color="#EEEEEE">Date Inspected</font></td>
					<td><font color="#EEEEEE">Inspection Type</font></td>
					<td><font color="#EEEEEE">Monthly Rate</font></td>
					<td><font color="#EEEEEE">Comp</font></td>
				</tr>
				<%if rsReport.eof then%>
					<tr><td colspan="8">There are no result for the items you chose. Please select again.</td></tr>
				<%else
				blnChange = false
				do until rsReport.eof
					
						If blnChange = true then%>
							<tr class="reportRowColor">
						<%else%>
							<tr>
						<%end if
						if rsReport("reportTypeID") = 1 or rsReport("reportTypeID") = 2 or rsReport("reportTypeID") = 4 Then%>
							<td><%=rsReport("reportID")%></td>
							<td><%=rsReport("customerName")%></td>
							<td><%=rsReport("projectName")%></td>
							<td><%=rsReport("firstName") & " " & rsReport("lastName")%></td>
							<td><%=rsReport("inspectionDate")%></td>
							<td><%=rsReport("reportType")%></td>
							<!--<td>Total # Inspections</td>-->
							<td>
								<%'put the correct rate here
									Select case rsReport("reportTypeID")
										Case 1 'weekly
											response.Write formatcurrency(rsReport("wprRate"),2)
										Case 2 'daily
											response.Write formatcurrency(rsReport("dRate"),2)
										Case 4 ' postrain
											response.Write formatcurrency(rsReport("wprRate"),2)
										Case else
											response.Write formatcurrency(0,2)
									end select
								%>
							</td>
							<td>&nbsp;</td>
						</tr>
				<%
					if blnChange = true then
						blnChange = false
					else
						blnChange = true
					end if
					end if
				rsReport.movenext
				
				
				loop
				end if%>							
			</table>
		</td>
	</tr>
</table>

<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>
