<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iProjectID = request("projectID")
'inspector = request("inspector")
smonth = request("month")
syear = request("year")
clientID = request("clientID")

arrVals = split(Request("uIDs"), ",")
		

'response.Write iProjectID & "<br>"
'response.Write smonth & "<br>"
'response.Write syear & "<br>"

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=agentRevenue.xls"%>
<table border=1>
	<tr bgcolor="#666666">
		<td><font color="#EEEEEE"><strong>Customer</strong></font></td>
		<td><font color="#EEEEEE"><strong>Project Name</strong></font></td>
		<td><font color="#EEEEEE"><strong>Monthly Revenue Rate</strong></font></td>
		<td><font color="#EEEEEE"><strong>Monthly Pay Rate</strong></font></td>
		<td><font color="#EEEEEE"><strong>Total # Reports</strong></font></td>
		<td><font color="#EEEEEE"><strong>Total Reports By Inspector</strong></font></td>
		<td><font color="#EEEEEE"><strong>Rate Per Inspection</strong></font></td>
		<td><font color="#EEEEEE"><strong>Agent Pay</strong></font></td>
		<td><font color="#EEEEEE"><strong>Revenue by Inspector</strong></font></td>
	</tr>
	<tr><td colspan="8"></td></tr>
	<%
	blnChange = false
	iReportCount = 0
	
	for i = 0 to ubound(arrVals)
		
		Set oCmd = Server.CreateObject("ADODB.Command")			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUser"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, arrVals(i))
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsUser = oCmd.Execute
		Set oCmd = nothing
		
		%>

		<tr bgcolor="#666666">
			<td colspan="8">
				<font color="#EEEEEE"><strong>Inspector/Agent:&nbsp;<%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></strong></font>
			</td>
		</tr>
		<%Set oCmd = Server.CreateObject("ADODB.Command")

		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAgentPayByAgent"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, arrVals(i))
		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
		
		iInspRate = 0
		iTotInspRate = 0
		iTotalReports = 0
		iMonthlyPayRate = 0
		iMonthlyBillRate = 0
		iTotNumRep = 0
		do until rsReport.eof
	
			'get the total number of reports for this project															
			Set oCmd = Server.CreateObject("ADODB.Command")
				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetNumReportsByProjectandDate"
			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID"))
			   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
			   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
			   .CommandType = adCmdStoredProc   
			End With																		
			Set rsTot = oCmd.Execute
			Set oCmd = nothing%>
			<tr>
				<%iReportCount = iReportCount + 1%>
				<td><%=rsReport("customerName")%></td>
				<td><%=rsReport("projectName")%></td>
				<td align="right">
					<%
					'response.Write month(rsReport("startDate")) & "<br>"
					if trim(smonth) = trim(month(rsReport("startDate"))) then
						if trim(syear) = trim(year(rsReport("startDate"))) then

							iBillRate = (rsReport("billRate")/21)*rsReport("newStartBillingDays")																			
																								
							if rsReport("primaryInspector") = rsReport("userID") then%>
								<a href="form.asp?id=<%=rsReport("projectID")%>&formType=editRates&customerID=<%=rsReport("customerID")%>&divisionID=<%=rsReport("divisionID")%>" target="_blank"><%response.Write formatcurrency(iBillRate,2)%></a>
							<%else%>
								Not PI (<%response.Write formatcurrency(iBillRate,2)%>)
							<%end if%>	
							
																								
							
						<%else
							iBillRate = rsReport("billRate")%>
							<%'if not the primary inspector, do not display rate
							if rsReport("primaryInspector") = rsReport("userID") then%>
								<a href="form.asp?id=<%=rsReport("projectID")%>&formType=editRates&customerID=<%=rsReport("customerID")%>&divisionID=<%=rsReport("divisionID")%>" target="_blank"><%response.Write formatcurrency(iBillRate,2)%></a>
							<%else%>
								Not PI (<%response.Write formatcurrency(iBillRate,2)%>)
							<%end if%>
						<%end if
					else
						iBillRate = rsReport("billRate")%>
						<%'if not the primary inspector, do not display rate
						if rsReport("primaryInspector") = rsReport("userID") then%>
							<a href="form.asp?id=<%=rsReport("projectID")%>&formType=editRates&customerID=<%=rsReport("customerID")%>&divisionID=<%=rsReport("divisionID")%>" target="_blank"><%response.Write formatcurrency(iBillRate,2)%></a>
						<%else%>
							Not PI (<%response.Write formatcurrency(iBillRate,2)%>)
						<%end if%>
					<%end if%>
				</td>
				<td>
					<%response.Write formatcurrency(rsReport("wprRate"),2)%>
				</td>
				<td>
					<%=rsTot("totalNumReports")%>
					<%iTotNumRep = iTotNumRep + rsTot("totalNumReports")%>
				</td>
				<td>
					<%=rsReport("totalReports")%>
					<%iTotalReports = iTotalReports + rsReport("totalReports")%>
				</td>
				<%

				'get the monthlyRate
				if isNull(rsReport("wprRate")) then																
				else
					iMonthlyPayRate = iMonthlyPayRate + rsReport("wprRate")
				end if
				
				if rsReport("primaryInspector") = rsReport("userID") then
					if isNull(iBillRate) then
					else
						iMonthlyBillRate = iMonthlyBillRate + iBillRate
					end if
				end if
				
				%>
				<td>
					<%
					'get the rate per inspection
					if isNull(rsReport("wprRate")) then	
						iInspRate = 0															
					else
						iInspRate = rsReport("wprRate") / rsTot("totalNumReports")
					end if
					
					response.Write formatcurrency(iInspRate,2)
					%>
				</td>
				<td>
					<%
					if trim(smonth) = trim(month(rsReport("startDate"))) then
						if trim(syear) = trim(year(rsReport("startDate"))) then
							iInspRate = iBillRate/2
							iTotInspRate = iTotInspRate + iInspRate
							response.Write formatcurrency(iInspRate,2)
						else
							'calculate the pay for the agent
							iInspRate = iInspRate * rsReport("totalReports")
							iTotInspRate = iTotInspRate + iInspRate
							response.Write formatcurrency(iInspRate,2)
						end if
						
					else
						'calculate the pay for the agent
						iInspRate = iInspRate * rsReport("totalReports")
						iTotInspRate = iTotInspRate + iInspRate
						response.Write formatcurrency(iInspRate,2)
					end if%>
				</td>
				<td>
					<%
					revByInsp = (iBillRate/rsTot("totalNumReports")) * rsReport("totalReports")
					totRevByInsp = totRevByInsp + revByInsp
					response.Write formatcurrency(revByInsp,2)
					%>			
				
				</td>
			</tr>
			
			<%
			if blnChange = true then
				blnChange = false
			else
				blnChange = true
			end if
	
		rsReport.movenext

		loop
		rsReport.close
		set rsReport = nothing%>	
		
		<tr><td colspan="8"></td></tr>
		<tr>
			<td colspan="2" align="right"><strong>Totals: </strong>&nbsp;</td>
			<td><strong><%=formatcurrency(iMonthlyBillRate,2)%></strong></td>
			<td><strong><%=formatcurrency(iMonthlyPayRate,2)%></strong></td>
			<td><strong><%=iTotNumRep%></strong></td>
			<td><strong><%=iTotalReports%></strong></td>
			<td></td>
			<td><strong><%=formatcurrency(iTotInspRate,2)%></strong></td>
			<td><strong><%=formatcurrency(totRevByInsp,2)%></strong></td>	
		</tr>												
		<tr><td colspan="8"></td></tr>
		
	<%next%>
</table>
<%
rsReport.close
DataConn.close
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing%>

