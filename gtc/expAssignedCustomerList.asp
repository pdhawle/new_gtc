<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn
clientID = request("clientID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedQuoteCustomers"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=AssignedCustomerList.xls" 

%>
<table border=1>
	<tr>
		<td><b>Customer Name</b></td>
		<td><b>Address</b></td>
		<td><b>City</b></td>
		<td><b>State</b></td>
		<td><b>Zip</b></td>
		<td><b>County</b></td>
		<td><b>Phone</b></td>
		<td><b>Primary Contact</b></td>
		<td><b>Account Manager</b></td>
		<td><b>Area Manager</b></td>
		<td><b>Active Customer</b></td>
	</tr>
<%do until rs.eof%>
	<tr>
		<td><%=rs("customerName")%></td>
		<td><%=rs("address1")%></td>
		<td><%=rs("city")%></td>
		<td><%=rs("state")%></td>
		<td><%=rs("zip")%></td>
		<td><%=rs("county")%></td>
		<td><%=rs("phone")%></td>
		<td><%=rs("contactName")%></td>
		<td><%=rs("accountManagerFirst")%>&nbsp;<%=rs("accountManagerLast")%></td>
		<td><%=rs("areaManagerFirst")%>&nbsp;<%=rs("areaManagerLast")%></td>
		<td>
			<%if rs("isActive") = "True" then%>
				Yes
			<%end if%>
		</td>
	</tr>
<%
rs.movenext
loop%>
</table>
<%

rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>