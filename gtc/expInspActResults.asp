<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iUser = request("inspector")
dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
if iUser = "all" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetInspectorActivityAll"
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
	
else
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetInspectorActivity" 'specific project
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, iUser)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
end if


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=inspectorActivity.xls"
%> 
<table border="1">
	<tr><td colspan="8">Date Range: <strong><%=dtFrom%> - <%=dtTo%></strong></td></tr>
	<tr bgcolor="#666666">
		<td><font color="#EEEEEE">Report ID</font></td>
		<td><font color="#EEEEEE">Customer</font></td>
		<td><font color="#EEEEEE">Project</font></td>
		<td><font color="#EEEEEE">Inspector</font></td>
		<td><font color="#EEEEEE">Report Type</font></td>
		<td><font color="#EEEEEE">Inspection Date</font></td>
		<td><font color="#EEEEEE">Inspection Date Entered</font></td>
		<td><font color="#EEEEEE">Inspection Timestamped</font></td>
	</tr>

	<%If rsReport.eof then %>
			<tr><td colspan="8">there are no reports to display</td></tr>
		<%else
			blnChange = false
			dim i
			i = 0
			Do until rsReport.eof
				If blnChange = true then%>
					<tr class="reportRowColor">
				<%else%>
					<tr>
				<%end if%>
					<td><%=rsReport("reportID")%></td>
					<td><%=rsReport("customerName")%></td>
					<td><%=rsReport("projectName")%></td>
					<td><%=rsReport("firstName") & " " & rsReport("lastName")%></td>
					<td><%=rsReport("reportType")%></td>
					<td><%=rsReport("inspectionDate")%></td>
					<td><%=FormatDateTime(rsReport("dateAdded"),2)%></td>
					<td><%=FormatDateTime(rsReport("dateAdded"),3)%></td>
				</tr>
			<%
			
			rsReport.movenext
			if blnChange = true then
				blnChange = false
			else
				blnChange = true
			end if
			i = i + 1
			loop%>
			
			<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
			<tr>
				<td colspan="8" align="right"><strong>Total Reports:&nbsp;<%=i%>&nbsp;&nbsp;&nbsp;</strong></td>
			</tr>
			
		<%end if%>
	
</table>

<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>
