<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn
clientID = request("clientID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetQuotes"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=quoteList.xls" 

%>
<table border=1>
	<tr>
		<td><b>Quote ID</b></td>
		<td><b>Project Name</b></td>
		<td><b>Bid Date</b></td>
		<td><b>Date Created</b></td>
		<td><b>Created By</b></td>
		<td><b>Customers Assigned</b></td>
	</tr>
<%do until rs.eof%>
	<tr>
		<td><%=rs("quoteID")%></td>
		<td><%=rs("projectName")%></td>
		<td><%=formatDateTime(rs("bidDate"),2)%></td>
		<td><%=formatDateTime(rs("dateCreated"),2)%></td>
		<td><%=rs("createdBy")%></td>
		<%'get the number of customers assigned to this quote
		
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetQuotesAssigned"
		   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, rs("quoteID"))
		   .CommandType = adCmdStoredProc   
		End With
				
		Set rsAssigned = oCmd.Execute
		Set oCmd = nothing
		%>
		<td><%=rsAssigned("numberAssigned")%></td>
	</tr>
<%
rs.movenext
loop%>
</table>
<%

rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>