<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iCustomerID = request("customer")
iDivisionID = request("division")
iProjectID = request("projectID")
dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	
DataConn.Open Session("Connection"), Session("UserID")
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

if iProjectID = "All" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetStabilizationByDivision" 'by division
	   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
else
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetStabilizationByProject" 'specific project
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
end if


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=projectStabilization.xls" %>
<table border=1>
	<tr>
		<td colspan="6"><strong>Customer:</strong> <%=rsCustomer("customerName")%></td>
	</tr>
		<td colspan="6"><strong>Date Range:</strong> <%=dtFrom%> - <%=dtTo%></td>
	</tr>
	<tr>
		<td><strong>Report ID</strong></td>	
		<td><strong>Project</strong></td>														
		<td><strong>Report Date</strong></td>
		<td><strong>Stabilization Achieved</strong></td>
		<td><strong>Comments</strong></td>
		<td><strong>Stabilization Date</strong></td>
	</tr>
<%
i = 0
do until rs.eof%>
	<tr>
		<td><%=rs("reportID")%></td>
		<td><%=rs("projectName")%></td>
		<td><%=rs("inspectionDate")%></td>
		<td><%=rs("stabilizationAchieved")%></td>
		<td><%=rs("explainStabilizationAchieved")%></td>
		<td><%=rs("siteStabilizationDate")%></td>
	</tr>
<%
rs.movenext
i = i + 1
loop%>
<tr>
	<td colspan="6"><strong>Total Reports:</strong> <%=i%></td>
</tr>
</table>
<%

rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing%>

