<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
userID = request("userID")
dtFrom = request("fromDate")
dtTo = request("toDate")
exclude = request("exclude")


If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetTimesheetByUserAndDate"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)   
   .CommandType = adCmdStoredProc   
End With
			
Set rsReport = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)  
   .CommandType = adCmdStoredProc   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing




Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=timesheet.xls" 
if rsReport.eof <> true then%>
		<table border=1>
			<tr><td colspan="5"><strong>Employee: <%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></strong></td></tr>
			<tr><td colspan="5"><strong>Date Range: <%=dtFrom%> - <%=dtTo%></strong></td></tr>
			<tr>
				<td><strong>Date</strong></td>
				<td><strong>Project</strong></td>
				<td><strong>Comments</strong></td>
				<td><strong>Code</strong></td>
				<td><strong>Code Description</strong></td>
				<td><strong>Billable</strong></td>
				<td><strong>Hours</strong></td>
			</tr>
	<%dim i
	i = 0
	do until rsReport.eof%>
		<tr>
			<td><%=rsReport("date")%></td>
			<td><%=rsReport("customerName")%> - <%=rsReport("projectName")%></td>
			<td><%=rsReport("comments")%></td>
			<td><%=rsReport("code")%></td>
			<td><%=rsReport("type")%></td>
			<td>
				<%if rsReport("billable") then
					iTotalBill = iTotalBill + rsReport("hours")%>
					Yes
				<%else
					iNTotalBill = iNTotalBill + rsReport("hours")%>
					No
				<%end if%>
			</td>
			<td><%=rsReport("hours")%></td>
		</tr>
	<%
	iTotal = iTotal + rsReport("hours")
	rsReport.movenext
	i = i + 1
	loop
	if iTotalBill = "" then
		iTotalBill = 0
	end if
	if iNTotalBill = "" then
		iNTotalBill = 0
	end if%>
		<tr>
			<td colspan="6" align="right"><strong>Billable Hours:</strong>&nbsp;&nbsp;</td>
			<td><%=iTotalBill%></td>
		</tr>
		<tr>
			<td colspan="6" align="right"><strong>Non-Billable Hours:</strong>&nbsp;&nbsp;</td>
			<td><%=iNTotalBill%></td>
		</tr>
		<tr>
			<td colspan="6" align="right"><strong>Total Hours:</strong>&nbsp;&nbsp;</td>
			<td><%=iTotal%></td>
		</tr>
	</table>
<%end if

rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing%>

