<%
'' This code is absolute freeware. you can do with it as you please. 
'' There are no restrictions what so ever, but the code is AS IS, no warrenty or liabilty
'' Please leave these commentlines intact
'' Netherlands, February 2004
'' M.Blokdijk
'' maarten@blokdijk.com
'' V 1.0
%>
<!--#Include File="fm_includeCust.asp"--> 
<% 

Dim fs, sAction, sFile, sPath, sFolder, sFileType, scriptname, dbfile, ReadStream, WriteStream, WriteFile, fileobject, filecollection, file, startpath, lineid, bgcolor, bgcolor_on, bgcolor_off, foldercollection, folder, errornum, errorcode

' Reset our errorcode values 
errornum = 0
errorcode = ""



scriptname=Request.ServerVariables("Script_Name")
sAction = Request.Querystring("action")
sFileType = Request.Querystring("filetype")
customerID = Request("customerID")

If Request.Querystring("path") = "" Then

''setting the base path or rootfolder
''*****************************************************
'sRootPath = "/SWSIR/downloads/Empty"
'sRootPath = "/downloads/Empty"
'if customerID <> "" then
 'sPath = "/SWSIR/downloads/project_" & iProjectID''the root folder for this sytem. will be dynamic based on which project is chosen.
 sPath = "/downloads/customer_" & customerID''the root folder for this sytem. will be dynamic based on which project is chosen.
'else
'  sPath = sRootPath
'end if
 'sPath = "/SWSIR/downloads"''the root folder for this sytem. can be dynamic based on which project is chosen.
 'response.Write sPath
''*****************************************************
''setting the base path or rootfolder

Else
 sPath = Request.Querystring("path")
 If InStr(sPath,"../") Then
	 errornum = errornum+1
	 errorcode = errorcode & "<li><b>Invalid use of ""../""</b>. You can only edit your own folders.</li>"
 End If
End If

If sPath="/" Then
 If Request.Querystring("file") = "" Then
  sFile = sPath & Request.Form("file")
 Else
  sFile = sPath & Request.Querystring("file")
 End If 
 If Request.Querystring("folder") = "" Then
  sFolder = sPath & Request.Form("folder")
 Else
  sFolder = sPath & Request.Querystring("folder")
 End If
Else
 If Request.Querystring("file") = "" Then
  sFile = sPath & "/" & Request.Form("file")
 Else
  sFile = sPath & "/" & Request.Querystring("file")
 End If
 If Request.Querystring("folder") = "" Then
  sFolder = sPath & "/" & Request.Form("folder")
 Else
  sFolder = sPath & "/" & Request.Querystring("folder")
 End If
End If
session("foldername")=spath
' Make sure that no errors have occurred and no illegal actions have been taken before doing our stuff... 

If errornum < 1 Then
  Set fs = Server.CreateObject("Scripting.FileSystemObject")

  Select Case sAction
    Case "editfile"
      Select Case sFileType
        Case "htm", "asp", "txt", "inc", "html", "shtml", "shtm", "js", "css"
          EditFile
        Case "mdb", "dat"
          EditDb
        Case else
          FileTypeUnsupported
      End Select

    Case "savefile"
      SaveFile

    Case "viewfolder"
      Showlist

    Case "newfile"
      CreateFile

    Case "newfolder"
      CreateFolder

    Case "deletefile"
      DeleteFile

    Case "deletefolder"
      DeleteFolder
	
	Case "CreateNewFolder"
	  CreateNewFolder
	
	Case "UploadFiles"
		UploadFiles
		
	Case "RenameFolder"
		RenameFolder

	Case "RenameFile"
		RenameFile

    Case Else
      Showlist 
  End Select
  Set fs = Nothing
Else
  DisplayErrors
End If
%>


