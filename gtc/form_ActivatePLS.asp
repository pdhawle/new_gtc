<%
quoteID = request("quoteID")
customerQuoteID = request("customerQuoteID")
userID = request("userID")
customerID = request("customerID")
clientID = request("clientID")
'divisionID = request("divisionID")


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
'Create command
'get the division name and customer name
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivisionsByCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsDivision = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerQuote"
   .parameters.Append .CreateParameter("@customerQuoteID", adInteger, adParamInput, 8, customerQuoteID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsQuote = oCmd.Execute
Set oCmd = nothing

sState = rsQuote("projectState")
if sState = "" then
	sState = request("projectState")
end if

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc   
End With
	
Set rsUser = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNextInspectors"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsUsers = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetRateTypes"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsRateTypes = oCmd.Execute
Set oCmd = nothing

%>
<script type="text/javascript">
<!--

function proj_onchange(activatePLS) {
   document.activatePLS.action = "form.asp?formType=activatePLS";
   activatePLS.submit(); 
}
 -->
</script>
<form name="activatePLS" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Project Launch Information</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" colspan="3"><strong><u>SITE/PROJECT INFORMATION</u></strong>&nbsp;<a href="form.asp?formType=editPLS&quoteID=<%=quoteID%>&customerQuoteID=<%=customerQuoteID%>&userID=<%=userID%>&customerID=<%=customerID%>&clientID=<%=clientID%>">(edit project info)</a></td>
										</tr>										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Project Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("projectName")%>	
												<input type="hidden" name="projectName"  value="<%=rsQuote("projectName")%>"/>										
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Project Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("projectAddress")%>	
												<input type="hidden" name="projectAddress"  value="<%=rsQuote("projectAddress")%>"/>										
											</td>
										</tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("projectCity")%>		
												<input type="hidden" name="city"  value="<%=rsQuote("projectCity")%>"/>									
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State, Zip:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("projectState")%>, <%=rsQuote("projectZip")	%>	
												<input type="hidden" name="state"  value="<%=rsQuote("projectState")%>"/>
												<input type="hidden" name="zip"  value="<%=rsQuote("projectZip")%>"/>									
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>County:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("county")%>
												<input type="hidden" name="countyID"  value="<%=rsQuote("projectCounty")%>"/>											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Latitude:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("latitude")%>	
												<input type="hidden" name="latitude"  value="<%=rsQuote("latitude")%>"/>										
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Longitude:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("longitude")%>
												<input type="hidden" name="longitude"  value="<%=rsQuote("longitude")%>"/>											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Directions:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("projectDirections")%>	
												<input type="hidden" name="directions"  value="<%=rsQuote("projectDirections")%>"/>											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Acres/Phases:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("acresPhases")%>	
												<input type="hidden" name="acresPhases"  value="<%=rsQuote("acresPhases")%>"/>											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Stage:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("stage")%>	
												<input type="hidden" name="stage"  value="<%=rsQuote("stage")%>"/>											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("contactName")%>	
												<input type="hidden" name="contactName"  value="<%=rsQuote("contactName")%>"/>										
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("contactPhone")%>		
												<input type="hidden" name="contactPhone"  value="<%=rsQuote("contactPhone")%>"/>									
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact Cell:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("contactCell")%>		
												<input type="hidden" name="contactCell"  value="<%=rsQuote("contactCell")%>"/>									
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact Fax:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("contactFax")%>	
												<input type="hidden" name="contactFax"  value="<%=rsQuote("contactFax")%>"/>										
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsQuote("contactEmail")%>	
												<input type="hidden" name="contactEmail"  value="<%=rsQuote("contactEmail")%>"/>										
											</td>
										</tr>
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Project Creation Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%'=formatDatetime(now(),2)%>
												<input type="hidden" name="creationDate"  value="<%'=now()%>"/>											
											</td>
										</tr>-->
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Start Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="startDate" maxlength="10" size="10" value="<%=formatDatetime(now(),2)%>" />&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Primary Inspector:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="primaryInspector">
													<%do until rsUsers.eof%>
														<option value="<%=rsUsers("userID")%>"><%=rsUsers("firstName") & " " & rsUsers("lastName")%></option>
													<%rsUsers.movenext
													loop%>
												</select>
											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Auto Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="autoEmail" checked="checked"/>												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										
										
										<tr>
											<td valign="top" colspan="3"><strong><u>CUSTOMER/CLIENT INFORMATION</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Customer/Client:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsCustomer("customerName")%>	
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="quoteID" value="<%=quoteID%>" />
												<input type="hidden" name="customerQuoteID" value="<%=customerQuoteID%>" />
												<input type="hidden" name="userID" value="<%=userID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="name" value="<%=rsUser("firstName") & " " & rsUser("lastName")%>" />
												<input type="hidden" name="email" value="<%=rsUser("email")%>" />
												<input type="hidden" name="customerName" value="<%=rsCustomer("customerName")%>" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Division:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="divisionID">
													<%do until rsDivision.eof%>
														<option value="<%=rsDivision("divisionID")%>"><%=rsDivision("division")%></option>
													<%rsDivision.movenext
													loop%>
												</select><!--&nbsp;If default, will need to edit under dashboard.	-->											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										
										
										<tr>
											<td valign="top" colspan="3"><strong><u>RATE INFORMATION</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>New Start Billing Days:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="newStartBillingDays">	
													<option value=""></option>
													<%for i=1 to 21%>
														<option value="<%=i%>"><%=i%></option>
													<%next%>
												</select>&nbsp;<input type="hidden" name="shellBox" />&nbsp;<a href="javascript:displayDatePicker('shellBox')"><img alt="Open Calendar" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;number of days the client should be billed for the first month, pro-rated
												<!--<input type="hidden" name="shellBox" />&nbsp;<a href="javascript:displayDatePicker('shellBox')"><img alt="Open Calendar" src="images/date.gif" border="0" width="17" height="16"></a>-->
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Activation Fee:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="activationFee" size="5" value="<%=rsQuote("siteActivationFee")%>"/>&nbsp;number only - ex. 200, 200.00
												
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Billing Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billRate" size="5" value="<%=rsQuote("wprRate")%>"/>&nbsp;(what we bill the customer) number only - ex. 200, 200.00
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Rate Type/Frequency:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="rateType">
													<%do until rsRateTypes.eof%>
														<option value="<%=rsRateTypes("rateTypeID")%>"><%=rsRateTypes("rateType")%></option>														
													<%rsRateTypes.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Water Sampling Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="waterSamplingRate" size="5" value="<%=rsQuote("waterSampRate")%>"/>&nbsp;Per sample
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr><td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Project/Inspection Rates</strong></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Weekly/Post Rain:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="wprRate" size="5" value="0.00"/>&nbsp;Per inspection - number only - ex. 200, 200.00												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Daily:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dRate" size="5" value="<%=rsQuote("dRate")%>"/>&nbsp;Per inspection - number only - ex. 200, 200.00
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										
										<tr>
											<td valign="top" colspan="3"><strong><u>ALERT INFORMATION</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>* 7 Day Alert:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="sevenDayAlert"/>
												
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>* 14 Day Alert:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="fourteenDayAlert"/>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>** Open Item Alert:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="openItemAlert"/>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td colspan="3">
												<strong>PLEASE NOTE:</strong> The primary inspector will be automatically assigned to this project.<br /><br />
												* When checked, a daily email will automatically be sent to assigned users if there has not been a weekly or post rain report generated<br /> for this project after seven or fourteen days.<br /><br />
												** When checked, a daily email will automatically be sent to assigned users if there are open items for this project that are over seven days old.
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="processType" value="activatePLS" />
												<%'if sState = "" then%>
													<!--<input type="submit" value="  Save  " class="formButton" disabled="disabled"/>-->
												<%'else%>
													<input type="submit" value="  Save  " class="formButton"/>
												<%'end if%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("activatePLS");
  frmvalidator.addValidation("startDate","req","Please enter the project start date");
</script>