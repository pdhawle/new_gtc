<%
clientID = request("clientID")
quoteID = request("quoteID")
customerID = request("customerID")
contactID = request("customerContactID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing


'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomersByClient"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCustomerList = oCmd.Execute
Set oCmd = nothing

if customerID <> "" then

	'Create command for state list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomerContacts"
	   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
	   .CommandType = adCmdStoredProc
	   
	End With
		
	Set rsContact = oCmd.Execute
	Set oCmd = nothing
	
	If contactID <> "" then
		'Create command for state list
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetCustomerContact"
		   .parameters.Append .CreateParameter("@customerContactID", adInteger, adParamInput, 8, contactID)
		   .CommandType = adCmdStoredProc
		   
		End With
			
		Set rsCustomer = oCmd.Execute
		Set oCmd = nothing
	end if
end if
%>
<script language="JavaScript">
<!-- Begin
//function dept_onchange(addCustomer) {
//   document.addCustomer.action = "form.asp?formType=assignCustomer";
//   addCustomer.submit(); 
//}


function cust_onchange(addCustomer,custID,quoteID,clientID) {
   window.location.href = 'form.asp?formType=assignCustomer&customerID='+custID+'&quoteID='+quoteID+'&clientID='+clientID;
}

function cont_onchange(addCustomer,custID,quoteID,clientID,contactID) {
   window.location.href = 'form.asp?formType=assignCustomer&customerID='+custID+'&quoteID='+quoteID+'&clientID='+clientID+'&customerContactID='+contactID;
}
//  End -->
</script>
<form name="addCustomer" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Assign Customer to Quote</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<%'If Session("Admin") = "True" then%>
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addCustomer&clientID=<%=clientID%>" class="footerLink">add customer</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="quoteList.asp?clientID=<%=clientID%>" class="footerLink">quote list</a>&nbsp;&nbsp;
						<%if customerID <> "" then%>
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="form.asp?formType=addCustomerContact&customerID=<%=customerID%>&quoteID=<%=quoteID%>&clientID=<%=clientID%>&red=quote" class="footerLink">add customer contact</a>&nbsp;&nbsp;
						<%end if%>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<%'end if%>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr><td colspan="5"><b>Note:</b> If you don't see a customer in the list, create the customer, but <br />leave inactive. (Must be administrator to add customer)<br /><strong>Inactive customers are in <span class="inActiveCustomer">red</span>.</strong></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Customer Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><!-- onChange="return dept_onchange(addCustomer)"-->
												<select name="customerID" onChange="return cust_onchange(addCustomer,this.getElementsByTagName('option')[this.selectedIndex].value,<%=quoteID%>,<%=clientID%>)">
													<option value="">--Select Customer---</option>
													<%do until rsCustomerList.eof
														if trim(customerID) = trim(rsCustomerList("customerID")) then
															if rsCustomerList("isActive") = "False" then%>
																<option class="inActiveCustomer" selected="selected" value="<%=rsCustomerList("customerID")%>"><%=rsCustomerList("customerName")%></option>
															<%else%>
																<option selected="selected" value="<%=rsCustomerList("customerID")%>"><%=rsCustomerList("customerName")%></option>
															<%end if%>
														<%else
															if rsCustomerList("isActive") = "False" then%>
																<option class="inActiveCustomer" value="<%=rsCustomerList("customerID")%>"><%=rsCustomerList("customerName")%></option>
															<%else%>
																<option value="<%=rsCustomerList("customerID")%>"><%=rsCustomerList("customerName")%></option>
															<%end if%>
													<%end if
													rsCustomerList.movenext
													loop%>
												</select>
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="quoteID" value="<%=quoteID%>" />
											</td>
										</tr>
										<%if customerID <> "" then%>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="customerContactID" onChange="return cont_onchange(addCustomer,customerID.getElementsByTagName('option')[customerID.selectedIndex].value,<%=quoteID%>,<%=clientID%>,this.getElementsByTagName('option')[this.selectedIndex].value)">
													<option value="">--Select Contact---</option>
													<%do until rsContact.eof
														if trim(contactID) = trim(rsContact("customerContactID")) then%>
															<option selected="selected" value="<%=rsContact("customerContactID")%>"><%=rsContact("contactName")%>&nbsp;<%=rsContact("contactLastName")%></option>
														<%else%>
															<option value="<%=rsContact("customerContactID")%>"><%=rsContact("contactName")%>&nbsp;<%=rsContact("contactLastName")%></option>
													<%end if
													rsContact.movenext
													loop%>
												</select>
											</td>
										</tr>
										<%end if%>
										<%if contactID <> "" then%>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><span class="required">*</span> <strong>Address:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="address" size="30" value="<%=rsCustomer("address1")%>"/>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><span class="required">*</span> <strong>City:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="city" size="30" value="<%=rsCustomer("city")%>"/>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><span class="required">*</span> <strong>State:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<select name="state">
														<%do until rsState.eof
															if trim(rsCustomer("state")) = trim(rsState("stateID")) then%>
																<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
															<%else%>
																<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%end if
														rsState.movenext
														loop%>
													</select>&nbsp;&nbsp;<span class="required">*</span> <strong>Zip:</strong>&nbsp;<input type="text" name="zip" size="5" value="<%=rsCustomer("zip")%>" />
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><span class="required">*</span> <strong>Contact Phone:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="contactPhone" size="30"  value="<%=rsCustomer("phone")%>"/>
													<input type="hidden" name="contactName" size="30" value="<%=rsCustomer("contactName") & " " & rsCustomer("contactLastName")%>"/>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Contact Cell:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="contactCell" size="30"  value="<%=rsCustomer("cell")%>"/>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><span class="required">*</span> <strong>Contact Fax:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="contactFax" size="30"  value="<%=rsCustomer("fax")%>"/>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Contact Email:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="contactEmail" size="30" value="<%=rsCustomer("contactEmail")%>"/>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Quote Preference:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<select name="quotePref">
														<option value="Email">Email</option>
														<option value="Fax">Fax</option>
													</select>
												</td>
											</tr>	
											
											
											
											
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>Inspections</u></strong></td>
										</tr>
										<tr>
											<td valign="top" align="right"><strong>Weekly Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="wRate" size="5" value="<%=request("wRate")%>"/>&nbsp;&nbsp;<select name="wFreq"><option>Monthly</option><option>Each</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Post Rain Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="prRate" size="5" value="<%=request("prRate")%>"/>&nbsp;&nbsp;<select name="prFreq"><option>Monthly</option><option>Each</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Weekly/Post Rain Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="wprRate" size="5" value="<%=request("wprRate")%>"/>&nbsp;&nbsp;<select name="wprFreq"><option>Monthly</option><option>Each</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Weekly/Post Rain/Daily Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="wprdRate" size="5" value="<%=request("wprdRate")%>"/>&nbsp;&nbsp;<select name="wprdFreq"><option>Monthly</option><option>Each</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Daily/Petroleum/CO pad Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dRate" size="5" value="<%=request("dRate")%>"/>&nbsp;&nbsp;<select name="dFreq"><option>Monthly</option><option>Each</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Daily/Petroleum/CO pad Log:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												included
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>14 Day/Post Rain Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="fourteenwprRate" size="5" value="<%=request("fourteenwprRate")%>"/>&nbsp;&nbsp;<select name="fourteenwprFreq"><option>Monthly</option><option>Each</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>Water Sampling</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>4 Water Samples & Analysis:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												included
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>Water Sampling & Analysis Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="waterSampRate" size="5" value="<%=request("waterSampRate")%>"/> each sample. enter number only (ex. 50 or 500)
											</td>
										</tr>
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>Site Audits</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>BMP performance audit and estimate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="bmpPerfAudit" size="5" value="<%'=request("bmpPerfAudit")%>"/> each. enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>NEXT Sequence License</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>Single Project:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="licSingle" size="5" value="<%'=request("licSingle")%>"/>&nbsp;&nbsp;<select name="licSingleFreq"><option>Monthly</option><option>Each</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>Multiple Project:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="licMultiple" size="5" value="<%'=request("licMultiple")%>"/>&nbsp;&nbsp;<select name="licMultipleFreq"><option>Monthly</option><option>Quarterly</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>-->
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>Site setup</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Site Activation Fee:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="siteActivationFee" size="5" value="<%=request("siteActivationFee")%>"/> one time fee. enter number only (ex. 50 or 500)
											</td>
										</tr>
											
											
											
											
											
											
										
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<tr>
												<td colspan="3" align="right">
													<input type="hidden" name="processType" value="assignCustomerQuote" />
													<input type="submit" value="  Save  " class="formButton"/>
												</td>
											</tr>
										<%end if%>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addCustomer");
  <%if contactID <> "" then%>
  frmvalidator.addValidation("address","req","Please enter the customer's address");
  frmvalidator.addValidation("city","req","Please enter the customer's city");
  frmvalidator.addValidation("zip","req","Please enter the customer's zip");
  //frmvalidator.addValidation("contactName","req","Please enter the contact name");
  frmvalidator.addValidation("contactPhone","req","Please enter the contact phone");
  frmvalidator.addValidation("contactFax","req","Please enter the contact fax");
 // frmvalidator.addValidation("contactEmail","req","Please enter the cantact's email address");
 // frmvalidator.addValidation("contactEmail","email");
 <%end if%>
</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>