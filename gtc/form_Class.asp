<%
customerID = request("customerID")
clientID = session("clientID")
location = request("location")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command for customer list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClassTypes"
   .CommandType = adCmdStoredProc   
End With
	
Set rsClassTypes = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

%>
<script type="text/javascript">
<!--
function loc_onchange(addClass,customerID) {
   document.addClass.action = 'form.asp?formType=addClass&customerID='+customerID;
   addClass.submit(); 
}

function setOpenSlots(form) {
	for (var i = 0; i < form.classSize.length; i++) {
        if (form.classSize[i].checked) {
            break
        }
    }

	form.openSlots.value = form.classSize[i].value;
}

// -->
</script>
<form name="addClass" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Add Class</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><strong>Customer:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if customerID = 0 then
													response.Write "Open Class"
												else
													response.Write rsCust("customerName")
												end if%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Class:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="classTypeID">
												<%do until rsClassTypes.eof
													if trim(request("classTypeID")) = trim(rsClassTypes("classTypeID")) then%>
														<option selected="selected" value="<%=rsClassTypes("classTypeID")%>"><%=rsClassTypes("classType")%></option>
													<%else%>
														<option value="<%=rsClassTypes("classTypeID")%>"><%=rsClassTypes("classType")%></option>
												<%end if
												rsClassTypes.movenext
												loop%>
												</select>
											</td>
										</tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Class Size:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="classSize" value="10" onclick="setOpenSlots(this.form)" <%=isCheckedRadio("10",request("classSize"))%> />&nbsp;Up to 10<br />
												<input type="radio" name="classSize" value="15" onclick="setOpenSlots(this.form)" <%=isCheckedRadio("15",request("classSize"))%> />&nbsp;Up to 15<br />
												<input type="radio" name="classSize" value="20" onclick="setOpenSlots(this.form)" <%=isCheckedRadio("20",request("classSize"))%> />&nbsp;Up to 20<br />
												<input type="radio" name="classSize" value="35" onclick="setOpenSlots(this.form)" <%=isCheckedRadio("35",request("classSize"))%> />&nbsp;Up to 35<br />
												<input type="radio" name="classSize" value="50" onclick="setOpenSlots(this.form)" <%=isCheckedRadio("50",request("classSize"))%> />&nbsp;Up to 50<br />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Open Slots:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="openSlots" maxlength="3" size="3" value="<%=request("openSlots")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong># Student Manuals:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="numberManuals" maxlength="3" size="3" value="<%=request("numberManuals")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Class Start Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="startDate" maxlength="10" size="10" value="<%=request("startDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Class Start Time:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if request("startTime") <> "" then
													startTime = request("startTime")
												else
													startTime = "8:00 am"
												end if%>
												<input id="startTime" name="startTime" type="text" value="<%=startTime%>" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,startTime)" STYLE="cursor:hand">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Class End Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="endDate" maxlength="10" size="10" value="<%=request("endDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('endDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Class End Time:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if request("endTime") <> "" then
													endTime = request("endTime")
												else
													endTime = "5:00 pm"
												end if%>
												<input id="endTime" name="endTime" type="text" value="<%=endTime%>" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,endTime)" STYLE="cursor:hand">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Evening/Split Class:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="eveningClass" <%=requestIsChecked(request("eveningClass"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Saturday Class:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="saturdayClass" <%=requestIsChecked(request("saturdayClass"))%> />
											</td>
										</tr>
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Split Class:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="splitClass" <%'=requestIsChecked(request("splitClass"))%> />
											</td>
										</tr>-->
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Association Discount:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="associationDiscount" <%=requestIsChecked(request("associationDiscount"))%> />&nbsp;<strong>Amt:</strong> <input type="text" size="5" name="associationDiscountAmt" value="<%=request("associationDiscountAmt")%>" />&nbsp;(number only)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Location:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="location" onChange="return loc_onchange(addClass,<%=customerID%>)">
													<option></option>
													<option value="Onsite" <%=isSelected(request("location"),"Onsite")%>>Onsite</option>
													<option value="Offsite" <%=isSelected(request("location"),"Offsite")%>>Offsite</option>
												</select>
											</td>
										</tr>
										<%if location = "Offsite" then%>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Address 1:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="address1" maxlength="100" />
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Address 2:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="address2" maxlength="50" />
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>City:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="city" maxlength="50" />
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>State:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<select name="state">
														<%do until rsState.eof%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%rsState.movenext
														loop%>
													</select>&nbsp;<strong>Zip:</strong>&nbsp;<input type="text" name="zip" maxlength="50" size="5" />
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Need Projector:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="checkbox" name="projector"/>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Need Screen:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="checkbox" name="screen"/>
												</td>
											</tr>
										<%else%>
											<input type="hidden" name="address1" value="" />
											<input type="hidden" name="address2" value="" />
											<input type="hidden" name="city" value="" />
											<input type="hidden" name="state" value="" />
											<input type="hidden" name="zip" value="" />
											<input type="hidden" name="projector" value="" />
											<input type="hidden" name="screen" value="" />
										<%end if%>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="addClass" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addClass");
  frmvalidator.addValidation("classSize","selone_radio=0");
  frmvalidator.addValidation("startDate","req","Please enter a start date");
  frmvalidator.addValidation("startTime","req","Please enter a start time");
  frmvalidator.addValidation("endDate","req","Please enter a end date");
  frmvalidator.addValidation("endTime","req","Please enter a end time");
  frmvalidator.addValidation("location","dontselect=0");
</script>