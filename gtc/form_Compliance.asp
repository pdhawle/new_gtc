<%
On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing


'Create command for state list
'	Set oCmd = Server.CreateObject("ADODB.Command")
		
'	With oCmd
'	   .ActiveConnection = DataConn
'	   .CommandText = "spGetCustomers"
'	   .CommandType = adCmdStoredProc
'	   
'	End With
'		
'	Set rsCustomerList = oCmd.Execute
'	Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOrgTypes"
   .CommandType = adCmdStoredProc
   
End With
	
	
Set rsTypes = oCmd.Execute
Set oCmd = nothing

%>
<style>
	.bodyCopy{
		font-size:14px;
	}
</style>
<form name="addCompliance" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Add Subcontractor/Vendor Compliance Form</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>	
													
									<!--start of content for the page-->
									<span class="bodyCopy">Thank you for logging in.  Please complete sections 1-4.</span><br /><br />
									
									<span class="bodyCopy"><strong>Section 1</strong></span><br /><br />
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Company Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="companyName" size="30" maxlength="50" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Company Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="companyAddress" size="30" maxlength="50" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="companyCity" size="30" maxlength="50" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="companyState">
													<%do until rsState.eof%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%rsState.movenext
													loop%>
												</select>&nbsp;&nbsp;<span class="required">*</span> <strong>Zip:</strong>&nbsp;<input type="text" name="companyZip" size="5" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Company Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="companyPhone" size="10" maxlength="50" />
											</td>
										</tr>
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Service Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="serviceType" size="30" maxlength="50" />
											</td>
										</tr>-->
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Federal Tax ID #:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="taxID" size="30" maxlength="50" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Years Operating Under Current Company Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="yearsOperating" size="30" maxlength="50" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Previous Company Name (if any):</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="previousCompanyName" size="30" maxlength="50" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="25"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Type of Organization:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="orgTypeID">
													<%do until rsTypes.eof%>
															<option value="<%=rsTypes("orgTypeID")%>"><%=rsTypes("orgType")%></option>
													<%rsTypes.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>If Other:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="otherOrgType" size="30" maxlength="50" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
									</table>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" colspan="3">
											<strong>Services Provided:</strong><br />
												Type of services and/or supplies provided by Subcontractor/Vendor<br />(rental equipment, office supplies, consulting services, drilling, etc.):<br />
												<input type="text" name="servicesProvided" size="30" maxlength="50" /><br />
												<input type="text" name="servicesProvided2" size="30" maxlength="50" /><br />
												<input type="text" name="servicesProvided3" size="30" maxlength="50" /><br />
												<input type="text" name="servicesProvided4" size="30" maxlength="50" /><br />
												<input type="text" name="servicesProvided5" size="30" maxlength="50" /><br />
												<input type="text" name="servicesProvided6" size="30" maxlength="50" /><br />
												<input type="text" name="servicesProvided7" size="30" maxlength="50" /><br />
												<input type="text" name="servicesProvided8" size="30" maxlength="50" /><br />
												<input type="text" name="servicesProvided9" size="30" maxlength="50" /><br />
												<input type="text" name="servicesProvided10" size="30" maxlength="50" />
											</td>											
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" colspan="3">
												<span class="bodyCopy"><strong>Section 2</strong><br /><br />
												<input type="hidden" name="EEOAAPStatement" value="" />
												<input type="hidden" name="I9Acknowledgement" value="" />
												<input type="hidden" name="nonDiscriminationPolicy" value="" />
												<input type="hidden" name="nonSegregationPolicy" value="" />
												
												<strong>Please be advised that HR is a federal governmental contractor subject to the<br />
												affirmative action laws and that in all contracts, subcontracts or purchase orders between<br />
												Sequence and your company, if applicable, the affirmative action clause for disabled workers <br />
												(41 CFR �60-741.4), the equal opportunity clause in Section 202 of Executive Order 11246, and <br />
												the affirmative action clause for disabled veterans and veterans of the Vietnam era (41 CFR �60-250.4) <br />
												are hereby incorporated by reference.  Further, if applicable, you agree to file Standard Form <br />
												100 (EEO-1) and comply with Department of Homeland Security�s I-9 Employment Eligibility Verification.</strong>  
												<!--Below you will find Sequence, Inc�s policies on Equal Employment Opportunity,<br /> Affirmative Action, I-9 Employment 
												Eligibility Verification, Non-Discrimination <br />and Non-Segregation. It is the responsibility of each 
												subcontractor/vendor <br />to comply with these policies and to comply with all applicable federal, 
												state and local laws.<br /><br />
												
												<span class="required">*</span> <strong>EEO-AAP Statement Acknowledgement</strong><br />
												It is, has been, and will continue to be the policy ofSequence, Inc. (Sequence) to provide equal <br />
												employment opportunity without regard to race, color, age, religion, sex, national origin, <br />
												disability or veterans. Further, it is required that all subcontractors/vendors of <br />
												Sequence undertake affirmative action in compliance with all federal, state, and local requirements.<br />
												<input type="radio" name="EEOAAPStatement" value="Yes" />&nbsp;Yes, we do and will comply with the EEO-AAP Statement<br />
												<input type="radio" name="EEOAAPStatement" value="No" />&nbsp;No, we do not and will not comply with the EEO-AAP Statement<br /><br />
												
																					
												<span class="required">*</span> <strong>I-9 Acknowledgement</strong><br />
												It is the responsibility of the subcontractor/vendor to comply with Department of <br />
												Homeland Security�s I-9 Employment Eligibility Verification..<br />
												<input type="radio" name="I9Acknowledgement" value="Yes" />&nbsp;Yes, we do comply with the I-9 Employment Eligibility Verification <br />
												<input type="radio" name="I9Acknowledgement" value="No" />&nbsp;No, we do not comply with the I-9 Employment Eligibility Verification<br /><br /> 

												<span class="required">*</span> <strong>Non-Discrimination Policy Acknowledgement</strong><br /> 
												Sequence, Inc. (Sequence) does not tolerate the harassment of applicants, employees, <br />customers, or vendors. Any form of harassment relating to 
												an individual�s race; <br />color; religion; national origin; sex (including same sex); pregnancy, childbirth, or <br />related medical conditions; 
												age; disability or handicap; citizenship status; service <br />member status; or any other category protected by federal, state, or local law is 
												a <br />violation of this policy and will be treated as a disciplinary matter.<br />
												<input type="radio" name="nonDiscriminationPolicy" value="Yes" />&nbsp;Yes, we do and will comply with the Non-Discrimination/Harassment Statement <br />
												<input type="radio" name="nonDiscriminationPolicy" value="No" />&nbsp;No, we do not and will not comply with the Non-Discrimination/Harassment Statement<br /><br /> 

 												<span class="required">*</span> <strong>Non-Segregation Policy Acknowledgement</strong><br />
												Segregated facilities as used in this policy means any waiting rooms, work areas, <br />
												rest rooms and wash rooms, restaurants and other eating areas, time clocks, locker <br />rooms 
												and other storage or dressing areas, parking lots, drinking fountains, recreation <br />or 
												entertainment areas, transportation and housing facilities provided for employees <br />that are 
												segregated by explicit directive or are in fact segregated on the basis of <br />race, color, religion, 
												sex or national origin because of written or oral policies or <br />employee custom.  The term does not 
												include separate or single-use rest rooms or <br />necessary dressing or sleeping areas provided to assure 
												privacy between the <br />sexes.  The subcontractor/vendor agrees that it does not and will not maintain <br />
												or provide for its employees any segregated facilities at any of it's establishments, <br />and that it does not 
												and will not permit its employees to perform their services at <br />any location under its control where segregated 
												facilities are maintained. <br />
												<input type="radio" name="nonSegregationPolicy" value="Yes" />&nbsp;Yes, we do and will comply with the Non-Segregation Policy <br />
												<input type="radio" name="nonSegregationPolicy" value="No" />&nbsp;No, we do not and will not comply with the Non-Segregation Policy<br /><br />
												</span>-->
											</td>											
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" colspan="3">
												<span class="bodyCopy"><strong>Section 3</strong><br /><br />
												
												For NAICS Code classifications go to:<br />
												<a href="http://www.census.gov/eos/www/naics/" target="_blank">www.census.gov/eos/www/naics/</a><br /><br />
												
												NAICS Code(s):<br />
												<input type="text" name="NAICSCode" size="10" maxlength="50" /><br />
												<input type="text" name="NAICSCode2" size="10" maxlength="50" /><br />
												<input type="text" name="NAICSCode3" size="10" maxlength="50" /><br />
												<input type="text" name="NAICSCode4" size="10" maxlength="50" /><br />
												<input type="text" name="NAICSCode5" size="10" maxlength="50" /><br />
												<input type="text" name="NAICSCode6" size="10" maxlength="50" /><br />
												
												
												
												<br />
												
												<span class="required">*</span> <strong>Business Classification</strong> (For assistance with Small Business Classification go<br />
												to <a href="http://www.sba.gov/idc/groups/public/documents/sba_homepage/serv_sstd_tablepdf.pdf" target="_blank">http://www.sba.gov/idc/groups/public/documents/sba_homepage/serv_sstd_tablepdf.pdf</a>)<br />
												<input type="radio" name="businessClassification" value="Large Business" />&nbsp;Large Business (continue to Section 4) <br />
												<input type="radio" name="businessClassification" value="Small Business" />&nbsp;Small Business (continue to Small Business Classification)<br /><br />

												<strong>Small Business Classification (check all that apply)</strong></span><br /><br />
												
												<table width="400" cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="smallBusiness"/>&nbsp;</td>
														<td>
															<span class="bodyCopy"><strong>Small business (SB)</strong> � located in U.S., organized for profit, including affiliates is independently owned & operated, not 
															dominant in field of operations in which it is bidding on Government contracts, AND meets Small Business Administration (SBA) size standards 
															included in solicitation. The size standard is based upon the North American Industrial Classification System (NAICS) assigned to the specific 
															procurement dependent upon product/service purchased. FAR 52.219-9 also includes subcontracts awarded to Alaska Native Corporation (ANC) or 
															Indian tribe, regardless of size or SBA certification status of ANC or Indian tribe. DFARS 252.219-7003 also includes subcontracts awarded to 
															qualified non-profit agencies approved by Committee for Purchase from People Who Are Blind or Severely Disabled, the independent federal agency 
															that administers AbilityOne Program, formerly JWOD (Javits-Wagner-O�Day Act) (41 USC 46-48(c). </span>
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr><td colspan="2"><span class="bodyCopy"><strong>Please select all classifications that apply:</strong></span></td></tr>
													
													<tr>
														<td valign="top"><input type="checkbox" name="womanOwnedSmallBusiness"/>&nbsp;</td>
														<td>
															<span class="bodyCopy"><strong>Woman-owned Small Business (WOSB)</strong> � Small Business, at least 51% owned by <u>></u> 1 women, AND management & daily business operations controlled by <u>></u> 1 women.</span>
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><input type="checkbox" name="HUBZone"/>&nbsp;</td>
														<td>
															<span class="bodyCopy"><strong>Historically Underutilized Business Zone (HUBZone)</strong> � Small Business, owned & controlled 51% or more by <u>></u> 1 
															U.S. citizens, AND SBA-certified as a HUBZone concern (principal office located in a designated HUBZone AND <u>></u> 35% of employees 
															live in a HUBZone).  </span>
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><input type="checkbox" name="VOSB"/>&nbsp;</td>
														<td>
															<span class="bodyCopy"><strong>Veteran-Owned Small Business (VOSB)</strong> � Small Business, veteran-owned as defined in 38 USC 101(2), <u>></u> 51% owned by <u>></u> 1 veterans, AND management 
															& daily business operations controlled by <u>></u> 1 veterans. </span>
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><input type="checkbox" name="SDVOSB"/>&nbsp;</td>
														<td>
															<span class="bodyCopy"><strong>Service-Disabled Veteran-Owned Small Business (SD-VOSB)</strong> � Small Business, veteran-owned, <u>></u> 51% owned by <u>></u> 1 
															service-disabled veterans, AND management & daily business operations controlled by <u>></u> 1 service-disabled veterans OR in the case 
															of veteran with permanent & severe disability, the spouse or permanent caregiver of such veteran, AND with 0% - 100% 
															service-connected disability as defined in 38 USC 101(16) & documented on DD 214 or equivalent.</span> 
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><input type="checkbox" name="SDB"/>&nbsp;</td>
														<td>
															<span class="bodyCopy"><strong>Small Disadvantaged Business (SDB)</strong> � Small Business unconditionally owned & controlled by <u>></u> 1 socially & economically disadvantaged individuals who are in good character & citizens of the U.S.</span>   
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><input type="checkbox" name="DBE"/>&nbsp;</td>
														<td>
															<span class="bodyCopy"><strong>Disadvantaged Business Enterprises (DBEs)</strong>�DBEs are for-profit small business concerns where socially and 
															economically disadvantaged individuals own at least a 51% interest and control management and daily business operations.</span> <br />
															<table>
																<tr>
																	<td valign="top">&bull;&nbsp;</td>
																	<td>
																		<span class="bodyCopy">African Americans, Hispanics, Native Americans, Asian-Pacific and Subcontinent Asian Americans, and women are presumed to be socially and economically disadvantaged.</span> 
																	</td>
																</tr>
																<tr>
																	<td valign="top">&bull;&nbsp;</td>
																	<td>
																		<span class="bodyCopy">Other individuals can be characterized as socially and economically disadvantaged on a case-by-case basis. </span>
																	</td>
																</tr>
																<tr>
																	<td valign="top">&bull;&nbsp;</td>
																	<td>
																		<span class="bodyCopy">To participate in the program, a small business owned and controlled by socially and economically disadvantaged 
																		individuals must receive DBE certification from their relevant state or local government agency.  <em>Note: this is not a federal certification and is not applicable to federal contracts.</em> </span>
																	</td>
																</tr>
																<tr>
																	<td valign="top">&bull;&nbsp;</td>
																	<td>
																		<span class="bodyCopy">Irrespective of what the size standard is, a firm cannot exceed the size of $22.41 million and still be seen as a <em>Small Business</em>.  This size limit is periodically adjusted for inflation. </span>
																	</td>
																</tr>
															</table> 
														</td>
													</tr>
													<tr><td colspan="2" height="10"></td></tr>
													<tr>
														<td valign="top"><input type="checkbox" name="otherClassification"/>&nbsp;</td>
														<td>
															<span class="bodyCopy">Other Classification (please explain):</span><br />
															<textarea name="otherClassificationExplain" rows="5" cols="40"></textarea>  
														</td>
													</tr>
												</table>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" colspan="3">
												<span class="bodyCopy"><strong>Section 4</strong><br /><br />
												<strong>Certification</strong><br />
												Complete the information below to certify the representations are accurate, current and<br />complete.  The subcontractor/vendor 
												is responsible for notifying Sequence regarding any<br />
												business change related to the contents of this document.<br /><br />
												
												Company Representative (completing the section below will be considered an e-signature)</span><br /><br />
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top" align="right"><strong>Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="name" size="30" maxlength="50" />
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Title:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="title" size="30" maxlength="50" />
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Phone #:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="phoneNumber" size="30" maxlength="50" />
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="dateSigned" maxlength="50" value="<%=formatdatetime(now(),2)%>" size="10" />
														</td>
													</tr>
												</table>
												

											</td>
										</tr>

										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="userID" value="<%=session("ID")%>" />
												<input type="hidden" name="clientID" value="<%=session("clientID")%>" />
												<input type="hidden" name="processType" value="addSubVendor" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addCompliance");
  frmvalidator.addValidation("companyName","req","Please enter the company's name");
  frmvalidator.addValidation("companyAddress","req","Please enter the address");
  frmvalidator.addValidation("companyPhone","req","Please enter the phone number");
  //frmvalidator.addValidation("serviceType","req","Please enter the service type");
  frmvalidator.addValidation("taxID","req","Please enter the Federal Tax ID #");
  //frmvalidator.addValidation("EEOAAPStatement","selone_radio=0","Please select an option from the EEO-AAP Statement Acknowledgement");
  //frmvalidator.addValidation("I9Acknowledgement","selone_radio=0","Please select an option from the I-9 Acknowledgement");
  //frmvalidator.addValidation("nonDiscriminationPolicy","selone_radio=0","Please select an option from the Non-Discrimination Policy Acknowledgement");
  //frmvalidator.addValidation("nonSegregationPolicy","selone_radio=0","Please select an option from the Non-Segregation Policy Acknowledgement");
  frmvalidator.addValidation("businessClassification","selone_radio=0","Please select an option from the Business Classification");
</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>