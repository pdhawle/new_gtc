<%
projectID = request("id")
f = request("f")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for contact type list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetContactTypes"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsContact = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript" src="includes/validator.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
var isNN = (navigator.appName.indexOf("Netscape")!=-1);
function autoTab(input,len, e) {
var keyCode = (isNN) ? e.which : e.keyCode; 
var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
if(input.value.length >= len && !containsElement(filter,keyCode)) {
input.value = input.value.slice(0, len);
input.form[(getIndex(input)+1) % input.form.length].focus();
}
function containsElement(arr, ele) {
var found = false, index = 0;
while(!found && index < arr.length)
if(arr[index] == ele)
found = true;
else
index++;
return found;
}
function getIndex(input) {
var index = -1, i = 0, found = false;
while (i < input.form.length && index == -1)
if (input.form[i] == input)index = i;
else i++;
return index;
}
return true;
}
//  End -->
</script>
</head>
<body>
<form name="addContact" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - New Contact</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><strong>Contact Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="contactType">
													<%do until rsContact.eof%>
														<option value="<%=rsContact("contactTypeID")%>"><%=rsContact("contactType")%></option>
													<%rsContact.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>*First Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="firstName" size="30" maxlength="50">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>*Last Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="lastName" size="30" maxlength="50">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Company Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="companyName" size="30" maxlength="100">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>*Email Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="email" size="30" maxlength="100">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>*Office Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="officeNumber1" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3"/> 
												<input type="text" name="officeNumber2" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" /> 
												<input type="text" name="officeNumber3" onKeyUp="return autoTab(this, 4, event);" size="5" maxlength="4"/> 
												ext.<input type="text" name="officeNumberExt" size="5" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>*Cell Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="cellNumber1" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3"/> 
												<input type="text" name="cellNumber2" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" /> 
												<input type="text" name="cellNumber3" onKeyUp="return autoTab(this, 4, event);" size="5" maxlength="4"/> 
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="address" size="30" maxlength="100">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="city" size="30" maxlength="50">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>State/Zip:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="state">
													<%do until rsState.eof%>
														<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%rsState.movenext
													loop
													rsState.moveFirst%>
												</select> <input type="text" name="zip" size="5" maxlength="50">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Gets Auto Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="getAutoEmail" checked="checked">
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>				
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="f" value="<%=f%>" />
												<input type="hidden" name="projectID" value="<%=projectID%>" />
												<input type="hidden" name="processType" value="addContact" />
												<input type="submit" value="Add Contact" class="formButton"/>
											</td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addContact");
  frmvalidator.addValidation("firstName","req","Please enter your first name");
  frmvalidator.addValidation("lastName","req","Please enter your last name"); 
  frmvalidator.addValidation("email","req","Please enter your email address");
  frmvalidator.addValidation("email","email");
  frmvalidator.addValidation("officeNumber1","req","Please enter your office phone number");
  frmvalidator.addValidation("officeNumber2","req","Please enter your office phone number");
  frmvalidator.addValidation("officeNumber3","req","Please enter your office phone number");
  frmvalidator.addValidation("cellNumber1","req","Please enter your cell phone number");
  frmvalidator.addValidation("cellNumber2","req","Please enter your cell phone number");
  frmvalidator.addValidation("cellNumber3","req","Please enter your cell phone number");
</script>
</body>
</html>
<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>