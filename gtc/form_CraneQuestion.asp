<%
On Error Resume Next

customerID = Request("customerID")
divisionID = Request("divisionID")

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCraneCategories"
   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCategory = oCmd.Execute
Set oCmd = nothing

%>
<form name="addQuestion" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Add Question</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top"><strong>Question:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="question" rows="6" cols="30"></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Sort Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="sortNumber" size="5" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Category:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="categoryID">
													<%do until rsCategory.eof%>
															<option value="<%=rsCategory("categoryID")%>"><%=rsCategory("category")%></option>
													<%rsCategory.movenext
													loop%>
												</select>
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Reports:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="towerCrane"/> Tower Crane&nbsp;&nbsp;&nbsp;
												<!--<input type="checkbox" name="biweekly"/> Bi-Weekly&nbsp;&nbsp;&nbsp;-->
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Question Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="questionType" value="1"/> No Action Recommended/Add Corrective Action/NA<br />
												<input type="radio" name="questionType" value="2" checked="checked"/> Yes/No/NA 
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="divisionID" value="<%=divisionID%>" />
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="processType" value="addCraneQuestion" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addQuestion");
  //frmvalidator.addValidation("referenceLetter","req","Please enter a reference letter");
  frmvalidator.addValidation("question","req","Please enter a question");
  frmvalidator.addValidation("sortNumber","req","Please enter a sort number");
  frmvalidator.addValidation("sortNumber","num");
</script>

<%
rsCategory.close
set rsCategory = nothing
DataConn.close
set DataConn = nothing
%>