<%
'Dim rs, oCmd, DataConn, sForm, sAttach, id, sp

sForm = request("type")
id = request("id")
docID = request("docID")
projName = request("projName")
projectID = request("projectID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next	
Set DataConn = Server.CreateObject("ADODB.Connection")
DataConn.Open Session("Connection"), Session("UserID") 

If sForm = "Primary" then	
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	sp = "spCheckForAttachmentsPrimary"
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = sp
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, id)
	   .CommandType = adCmdStoredProc
	   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
End If

If sForm = "secondary" then
		
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	sp = "spCheckForAttachmentsSecondary"
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = sp
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, id)
	   .CommandType = adCmdStoredProc
	   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
End If


Select Case sForm
	Case "Primary"
		sAttach = "NOIPrimary_" & docID & ".pdf"
		if rs("feeFormID") <> "" then
			bAttach2 = true
			sAttach2 = "npdesFeeForm_" & docID & ".pdf"
		end if
		if rs("notFormID") <> "" then
			bAttach3 = true
			sAttach3 = "noticeOfTermination_" & docID & ".pdf"
		end if
	Case "secondary"
		sAttach = "NOISecondary_" & docID & ".pdf"
		if rs("notFormID") <> "" then
			bAttach3 = true
			sAttach3 = "noticeOfTermination_" & docID & ".pdf"
		end if
End Select

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
   .CommandType = adCmdStoredProc
   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing

%>
<SCRIPT LANGUAGE="JavaScript">
var myWind = ""
function doNew() {
	if (myWind == "" || myWind.closed || myWind.name == undefined) {
		myWind = window.open("addressBook.asp?project=<%=projectID%>","subWindow","HEIGHT=600,WIDTH=1000,scrollbars=yes")
	} else{
		myWind.focus();
	}
}
</SCRIPT>
<form action="processNOI.asp" method="post" name="myForm">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Send Email</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<strong>Project Name:</strong> <%=projName%>
											</td>
											<td></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td colspan="3">
															<strong>Check below to send as an attachment</strong> <br />
															<input type="checkbox" name="form1"/> <strong><a href="downloads/NOI/<%=sAttach%>" target="_blank"><%=sAttach%></a></strong><br />
															<%if sForm = "Primary" then
																if bAttach2 = true then%>
																<input type="checkbox" name="form2"/> <strong><a href="downloads/NOI/<%=sAttach2%>" target="_blank"><%=sAttach2%></a></strong><br />
																<%end if
																
															end if
															if bAttach3 = true then%>
																<input type="checkbox" name="form3"/> <strong><a href="downloads/NOI/<%=sAttach3%>" target="_blank"><%=sAttach3%></a></strong><br />
															<%end if%><br />
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><input type="button" name="storage" value="To:.." onClick="doNew()" class="formButton"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="emailTo" size="50">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>CC:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="emailCC" size="50">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Bcc:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="emailBCC" size="50">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>	
													<tr>
														<td><strong>Subject:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="subject" size="50" value="<%=projName%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>			
													<tr>
														<td valign="top"><strong>Message:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%
																'Dim strFormName
																'Dim strTextAreaName
																strFormName = "myForm"
																strTextAreaName = "smessage"
															%>
															
															<textarea name="smessage" cols="70" rows="10"><br /><br><%=rsUser("emailSignature")%></textarea>
															<script>
																//STEP 2: Replace the textarea (txtContent)
																oEdit1 = new InnovaEditor("oEdit1");
																oEdit1.features=["FullScreen","Preview","Print","Search",
																	"Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
																	"ForeColor","BackColor","|","Bookmark","Hyperlink","XHTMLSource","BRK",
																	"Numbering","Bullets","|","Indent","Outdent","LTR","RTL","|",
																	"Image","Flash","Media","|","Table","Guidelines","Absolute","|",
																	"Characters","Line","Form","RemoveFormat","ClearAll","BRK",
																	"StyleAndFormatting","TextFormatting","ListFormatting","BoxFormatting",
																	"ParagraphFormatting","CssText","|",
																	"Paragraph","FontName","FontSize","|",
																	"Bold","Italic","Underline","Strikethrough","Superscript","Subscript","|",
																	"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"];
																oEdit1.REPLACE("smessage");
															</script>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<input type="hidden" name="projectID" value="<%=projectID%>" />
															<input type="hidden" name="processType" value="sendEmail" />
															<input type="hidden" name="attachment1" value="<%=sAttach%>" />
															<input type="hidden" name="attachment2" value="<%=sAttach2%>" />
															<input type="hidden" name="attachment3" value="<%=sAttach3%>" />
															<input type="hidden" name="projName" value="<%=projName%>" />
															<input type="submit" value="Send Email" class="formButton"/>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>

								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>