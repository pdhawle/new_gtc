<%
clientID = request("clientID")
On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClient"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
	
Set rsClient = oCmd.Execute
Set oCmd = nothing
%>
<form name="addWorkComp" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Add Notice Of Separation</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" colspan="3">
												<%=rsClient("clientName")%><br />
												<%=rsClient("address")%><br />
												<%=rsClient("city")%>, <%=rsClient("state")%>&nbsp;<%=rsClient("zip")%><br />
												<%=rsClient("contactPhone")%><br />
												Georgia D.O.L. Account No. <%=rsClient("GADOLNo")%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Employee's Name</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="empName" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Original Date of Hire</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateOfHire" size="10"/>&nbsp;<a href="javascript:displayDatePicker('dateOfHire')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Last Rate of Pay $</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="lastPayRate" size="10"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Rehire Date (if applicable)</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="rehireDate" size="10"/>&nbsp;<a href="javascript:displayDatePicker('rehireDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Last Date Worked</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="lastDateWorked" maxlength="10" size="10"/>&nbsp;<a href="javascript:displayDatePicker('lastDateWorked')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="employeeType" value="Hourly" />&nbsp;Hourly&nbsp;&nbsp;
												<input type="radio" name="employeeType" value="Weekly" />&nbsp;Weekly
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr><td colspan="3"><strong>Select Reason Below and Explain in Remarks Section</strong></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>Voluntary (quit/resigned)</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="reasonVoluntary">
													<option value="">--select one--</option>
														<option>Personal Illness. With Doctor's Excuse</option>
														<option>Personal Illness. Without Doctor's Excuse</option>
														<option>Illness In Family</option>
														<option>Change Of Residence</option>
														<option>Accepted Job With Another Employer</option>
														<option>Personal Reasons</option>
														<option>Personal Transportation Problems</option>
														<option>To Become Self-Employed</option>
														<option>Returned To School</option>
														<option>3 Consecutive Days Unreported Absences</option>
														<option>No Reason Given By Employee</option>
														<option>Walked Off The Job</option>
														<option>Dissatisfied With Wages</option>
														<option>Quit Rather Than Except Transfer(Explain)</option>
														<option>Other Reason(Explain)</option>
														<option>Retired</option>
														<option>Deceased</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Involuntary (discharged)</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="reasonInvoluntary">
													<option value="">--select one--</option>
														<option>LAY OFF</option>
														<option>Frequent Tardiness(Show Dates)</option>
														<option>Unapproved/Excessive Absences (Show Dates)</option>
														<option>Drinking On Duty/On Company Premises</option>
														<option>Reporting To Work Under The Influence</option>
														<option>Leaving Work/Jobsite Without Permission</option>
														<option>Improper Conduct(Explain)</option>
														<option>Violation Of A Company Rule Or Policy(Explain)</option>
														<option>Refused To Do Assigned Work(Explain)</option>
														<option>Destruction Of Company Property(Explain)</option>
														<option>Insubordination(Explain)</option>
														<option>Unsatisfactory Motor Vehicle/Driving Record</option>
														<option>Incarcerated-Unable To Return To Work</option>
														<option>Unable To Do Assigned Work(Explain)</option>
														<option>Completion Of Temporary Work Assignment</option>
														<option>Other Reason(Explain)</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Were warnings given?</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="warningsGiven" value="Yes" />&nbsp;Yes&nbsp;&nbsp;
												<input type="radio" name="warningsGiven" value="No" />&nbsp;No&nbsp;&nbsp;(show dates below)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr><td colspan="3"><strong>Remarks (List Specific Details Regarding The Reason For Separation)</strong></td></tr>
										<tr>
											<td valign="top" align="right"></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="remarks" cols="37" rows="5"></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Eligible For Rehire?</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="eligibleForRehire" value="Yes" />&nbsp;Yes&nbsp;&nbsp;
												<input type="radio" name="eligibleForRehire" value="No" />&nbsp;No
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><strong>Company Official With First-Hand Knowledge Of The Separation</strong></td></tr>
										<tr>
											<td valign="top" align="right"></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="compOfficial" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Company Official's Title</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="compOfficialTitle" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date Form Completed</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=formatDateTime(Now(),2)%>
											</td>
										</tr>
											
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="addNoticeOfSeparation" />
												<input type="submit" value="Save" class="formButton" />													
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addWorkComp");
  	frmvalidator.addValidation("empName","req","Please enter the employees name");
	frmvalidator.addValidation("lastDateWorked","req","Please enter the last date the employee worked");
	frmvalidator.addValidation("employeeType","selone_radio","Please select hourly or weekly");
	frmvalidator.addValidation("warningsGiven","selone_radio","Were warnings given?");
	frmvalidator.addValidation("remarks","req","Please enter your remarks for the reason for separation");
	frmvalidator.addValidation("eligibleForRehire","selone_radio","Eligible For Rehire?");
	frmvalidator.addValidation("compOfficial","req","Please enter company official's name");
	frmvalidator.addValidation("compOfficialTitle","req","Please enter company official's title");
	
	
	//frmvalidator.addValidation("injuryDate","req","Please enter the date of the injury");
	//frmvalidator.addValidation("injuryTime","req","Please enter the time of the injury");
	//frmvalidator.addValidation("injuryType","req","Please enter the type of injury");
	//frmvalidator.addValidation("bodyPartAffected","req","Please enter the body part affected");
	//frmvalidator.addValidation("howInjuryOccurred","req","Please describe how the injury occurred");
	//frmvalidator.addValidation("preparedBy","req","Who prepared the report?");
	
	//frmvalidator.addValidation("unitNumber","req","Please enter the unit number");
	//frmvalidator.addValidation("foreman","req","Please enter the foreman's name");
	//frmvalidator.addValidation("doingPriorToAccident","req","Describe in detail what you were doing immediately prior to the accident that required your use of the company vehicle?");
	//frmvalidator.addValidation("accidentDate","req","Please enter the accident date");
	//frmvalidator.addValidation("timeofAccident","req","Please enter the time of the accident");
	//frmvalidator.addValidation("describeWhatHappened","req","Please describe what happened");
	//frmvalidator.addValidation("reportedBy","req","Please enter who reported accident");
	

</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>