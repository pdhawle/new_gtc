<%
clientID = request("clientID")
sState = request("projectState")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

If sState <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCountiesByState"
	   .parameters.Append .CreateParameter("@ID", adVarchar, adParamInput, 50, trim(sState))
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing

end if


Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserListForClient"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With

Set rsUsers = oCmd.Execute
Set oCmd = nothing

do until rsUsers.eof
	sUserList = sUserList & chr(34) & rsUsers("firstName") & " " & rsUsers("lastName") & chr(34)
rsUsers.movenext
if not rsUsers.eof then
	sUserList = sUserList & ","
end if
loop

%>
<script type="text/javascript" src="jquery/jquery.js"></script>
<script type='text/javascript' src='jquery/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery/jquery.autocomplete.css" />
<script type="text/javascript">
var users = [
	<%=sUserList%>
];
$().ready(function() {	
	$("#leadGeneratedBy").autocomplete(users);
});


function proj_onchange(addQuote) {
   document.addQuote.action = "form.asp?formType=addQuote";
   addQuote.submit(); 
}
</script>

<form name="addQuote" method="post" action="process.asp" autocomplete="off">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Build Project/Add Quote</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">									
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Bid Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if request("bidDate") = "" then
													dBidDate = formatdatetime(now(),2)
												else
													dBidDate = request("bidDate")
												end if
												%>
												<input type="text" name="bidDate" maxlength="10" size="10" value="<%=dBidDate%>"/>&nbsp;<a href="javascript:displayDatePicker('bidDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="projectName" size="30" value="<%=request("projectName")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="projectAddress" size="30" value="<%=request("projectAddress")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="projectCity" size="30" value="<%=request("projectCity")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="projectState" onChange="return proj_onchange(addQuote)">
													<option value="">--select state--</option>
													<%do until rsState.eof
														if sState = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%end if
													rsState.movenext
													loop%>
												</select>&nbsp;&nbsp;<strong>Zip:</strong>&nbsp;<input type="text" name="projectZip" size="5" value="<%=request("projectZip")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>County:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if sState = "" then%>
													Please select a state above.
												<%else%>
													<select name="county">
														<%do until rsCounty.eof%>
															<option value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
														<%rsCounty.movenext
														loop%>
													</select>
												<%end if%>
												
											</td>
										</tr>											
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Invitation Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="invitationNumber" size="30" value="<%'=request("invitationNumber")%>"/> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Reed Connect Project Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="reedConnect" size="30" value="<%'=request("reedConnect")%>"/> enter number only (ex. 50 or 500)
											</td>
										</tr>-->
										
										
										
										
										
										
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>Inspections</u></strong></td>
										</tr>
										<tr>
											<td valign="top" align="right"><strong>Weekly Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="wRate" size="5" value="<%=request("wRate")%>"/>&nbsp;&nbsp;<select name="wFreq"><option>Each</option><option>Monthly</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Post Rain Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="prRate" size="5" value="<%=request("prRate")%>"/>&nbsp;&nbsp;<select name="prFreq"><option>Each</option><option>Monthly</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Weekly/Post Rain Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="wprRate" size="5" value="<%=request("wprRate")%>"/>&nbsp;&nbsp;<select name="wprFreq"><option>Each</option><option>Monthly</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Weekly/Post Rain/Daily Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="wprdRate" size="5" value="<%=request("wprdRate")%>"/>&nbsp;&nbsp;<select name="wprdFreq"><option>Each</option><option>Monthly</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Daily/Petroleum/CO pad Insp. Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dRate" size="5" value="<%=request("dRate")%>"/>&nbsp;&nbsp;<select name="dFreq"><option>Each</option><option>Monthly</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Daily/Petroleum/CO pad Log:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												included
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>Water Sampling</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>4 Water Samples & Analysis:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												included
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>Water Sampling & Analysis Rate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="waterSampRate" size="5" value="<%=request("waterSampRate")%>"/> each sample. enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>Site Audits</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>BMP performance audit and estimate:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="bmpPerfAudit" size="5" value="<%=request("bmpPerfAudit")%>"/> each. enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>NEXT Sequence License</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>Single Project:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="licSingle" size="5" value="<%=request("licSingle")%>"/>&nbsp;&nbsp;<select name="licSingleFreq"><option>Each</option><option>Monthly</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr>
											<td valign="top" align="right"><strong>Multiple Project:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="licMultiple" size="5" value="<%=request("licMultiple")%>"/>&nbsp;&nbsp;<select name="licMultipleFreq"><option>Quarterly</option><option>Monthly</option></select> enter number only (ex. 50 or 500)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong><u>Site setup</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Site Activation Fee:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="siteActivationFee" size="5" value="<%=request("siteActivationFee")%>"/> one time fee. enter number only (ex. 50 or 500)
											</td>
										</tr>-->
										
										
										
										
										
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Project Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="projectType">
													<option>Land Development</option>
													<option>Linear</option>													
													<option>Lot</option>
													<option>Vertical Construction</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Project Size:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" id="projectSize" name="projectSize" size="30" value="<%=request("projectSize")%>" maxlength="50" />
											</td>
										</tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Lead Generated By:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<div id="content">
													<input type="text" id="leadGeneratedBy" name="leadGeneratedBy" size="30" value="<%=request("leadGeneratedBy")%>" maxlength="50" />
												</div>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Lead Source:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="leadSource">
													<option>LDI</option>
													<option>Reed Connect</option>													
													<option>Newspaper</option>
													<option>Customer</option>
													<option>Employee</option>
													<option>Other</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong>24-Hour Site Contact</strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="TFHourName" size="30" value="<%=request("TFHourName")%>" maxlength="50" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Phone Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="TFHourPhone" size="20" value="<%=request("TFHourPhone")%>" maxlength="20" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="addQuote" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addQuote");
  frmvalidator.addValidation("bidDate","req","Please enter the bid date");
  frmvalidator.addValidation("projectName","req","Please enter the project name");
  frmvalidator.addValidation("projectAddress","req","Please enter the project address");
  frmvalidator.addValidation("projectCity","req","Please enter the project city");
  frmvalidator.addValidation("projectSize","req","Please enter the project size");
  frmvalidator.addValidation("leadGeneratedBy","req","Who generated the lead for this quote?");
  
  
//  frmvalidator.addValidation("contactEmail","email");
//  frmvalidator.addValidation("address1","req","Please enter the customer's address");
//  frmvalidator.addValidation("city","req","Please enter the customer's city");
//  frmvalidator.addValidation("zip","req","Please enter the customer's zip code");
//  frmvalidator.addValidation("phone","req","Please enter the customer's phone number");
</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>