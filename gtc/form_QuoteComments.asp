<%
quoteID = request("quoteID")
clientID = request("clientID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")


Set oCmd = Server.CreateObject("ADODB.Command")

With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetQuoteComments"
   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
   .CommandType = adCmdStoredProc   
End With
	
Set rs = oCmd.Execute
Set oCmd = nothing

%>
<form name="addQuoteComment" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Add/View Quote Comments</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="quoteList.asp?clientID=<%=clientID%>" class="footerLink">quote list</a>&nbsp;&nbsp;
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">									
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Comment:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="comment" rows="5" cols="40"></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="quoteID" value="<%=quoteID%>" />
												<input type="hidden" name="processType" value="addQuoteComment" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td colspan="2"></td>
											<td>
												<%if rs.eof then%>
													There have been no comments added for this quote.
												<%else%>
													<table width="350">
														<%do until rs.eof%>
															<tr>
																<td valign="top">
																	<i>added by&nbsp;<strong><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></strong>&nbsp;on&nbsp;<%=rs("dateAdded")%></i>
																	
																</td>
															</tr>
															<tr>
																<td><%=rs("comment")%><br /><br /></td>
															</tr>
														<%rs.movenext
														loop%>
													</table>
												<%end if%>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addQuoteComment");
  frmvalidator.addValidation("comment","req","Please enter a comment");
</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>