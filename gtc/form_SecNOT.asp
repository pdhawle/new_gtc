<%
On Error Resume Next

projectID = request("projectID")
secondaryFormID = request("id")
primaryID = request("primaryID")

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSecondaryNOI"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, secondaryFormID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsSecondary = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCoverage"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for county list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCounties"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCounty = oCmd.Execute
Set oCmd = nothing


'Create command for secondary permittee list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNumSecPermittees"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsSP = oCmd.Execute
Set oCmd = nothing
%>

<script language="javascript" type="text/javascript">
<!--

function KeepCount() {

	var NewCount = 0
						
	if (document.NOT.typeConstructionCommercial.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionIndustrial.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionMunicipal.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionDOT.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionUtility.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionResidential.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount2() {

	var NewCount = 0
						
	if (document.NOT.IRWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.IRWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount3() {

	var NewCount = 0
						
	if (document.NOT.RWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.RWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount4() {
  
	var NewCount = 0
						
	if (document.NOT.samplingOfOutfall.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.samplingOfRecievingStream.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.troutStream.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount5() {
  
	var NewCount = 0
						
	if (document.NOT.typePrimary.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeSecondary.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeTertiary.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount6() {

	var NewCount = 0
						
	if (document.NOT.constructionActivityCompleted.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.noLongerOwnerOperator.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
}

function KeepCount7() {
  
	var NewCount = 0
						
	if (document.NOT.primaryPermitteeSubdivision.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.individualLot.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.individualLotWithinSWDA.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
}

//-->
</script>

<form name="NOT" method="post" action="processNOI.asp" onSubmit="return ValidateDate()">

<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Notice of Termination</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<a href="instructions_NOT.asp" target="_blank">Instructions</a><br><br />
												<strong>To Cease Coverage Under General Permit</strong><br>
												<strong>To Discharge Storm Water Associated With Construction Activity</strong><br /><br />
												
												<strong>NOTE: Disabled data must be changed on the NOI primary data form.</strong>
											</td>
											<td></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><strong>Permit Type:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															GAR 100003-Common Development<input type="hidden" name="coverageDesired" size="30" value="3" />
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>I. SITE/PERMITTEE INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Site Project Name:</strong><br /><img src="images/pix.gif" width="180" height="1"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsSecondary("siteProjectName")%><input type="hidden" name="siteProjectName" size="30" value="<%=rsSecondary("siteProjectName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>GPS Location of Const. Exit:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<table>
																<tr>
																	<td>
																		Degrees<br /><span class="footer">From 0 to 90</span><br />
																		<input type="text" name="degree1" maxlength="2" size="5">
																	</td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td>
																		Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																		<input type="text" name="minute1" maxlength="2" size="5">
																		<input type="text" name="second1" maxlength="2" size="5">
																		<input type="hidden" name="latitude" value="North"> North Latitude
																	</td>
																</tr>
																<tr>
																	<td>
																		Degrees<br /><span class="footer">From 0 to 90</span><br />
																		<input type="text" name="degree2" size="5" maxlength="2">
																	</td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td>
																		Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																		<input type="text" name="minute2" size="5" maxlength="2">									
																		<input type="text" name="second2" maxlength="2" size="5">
																		<input type="hidden" name="longitude" value="West"> West Longitude
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Street Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsSecondary("siteLocation")%><input type="hidden" name="projectAddress" size="30" value="<%=rsSecondary("siteLocation")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City(if applicable):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsSecondary("city")%><input type="hidden" name="projectCity" size="30" value="<%=rsSecondary("city")%>">&nbsp;&nbsp;
															<strong>County:</strong>&nbsp;&nbsp;
															<%=rsSecondary("county")%><input type="hidden" name="projectCounty" size="30" value="<%=rsSecondary("county")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Subdivision Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsSecondary("subdivision")%><input type="hidden" name="subdivision" size="30" value="<%=rsSecondary("subdivision")%>">&nbsp;&nbsp;<strong>Lot Number:</strong>&nbsp;&nbsp;<%=rsSecondary("lotNumber")%><input type="hidden" name="lotNumber" size="30" value="<%=rsSecondary("lotNumber")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Owner�s Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="ownerName" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="ownerAddress" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="ownerCity" size="20">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<select name="ownerState">
																<%do until rsState.eof%>
																	<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																<%rsState.movenext
																loop
																rsState.moveFirst%>
															</select>&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<input type="text" name="ownerZip" size="10">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Operator�s Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="operatorName" size="30">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="operatorPhone" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="operatorAddress" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="operatorCity" size="20">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<select name="operatorState">
																<%do until rsState.eof%>
																	<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																<%rsState.movenext
																loop
																rsState.moveFirst%>
															</select>&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<input type="text" name="operatorZip" size="10">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>Type of Permittee:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="typePrimary" onClick="return KeepCount5()" />&nbsp;Primary&nbsp;&nbsp;
															<input type="checkbox" name="typeSecondary" onClick="return KeepCount5()" />&nbsp;Secondary&nbsp;&nbsp;
															<input type="checkbox" name="typeTertiary" onClick="return KeepCount5()" />&nbsp;Tertiary
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Facility Contact:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="facilityContact" size="30">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;
															<input type="text" name="facilityContactPhone" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>If Applicable:</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Primary Permittee's Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsSecondary("primaryPermitteeName")%><input type="hidden" name="primaryPermitteeName" size="30" value="<%=rsSecondary("primaryPermitteeName")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;<%=rsSecondary("primaryPermitteePhone")%><input type="hidden" name="primaryPermitteePhone" size="30" value="<%=rsSecondary("primaryPermitteePhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsSecondary("primaryPermitteeAddress")%><input type="hidden" name="primaryPermitteeAddress" size="30" value="<%=rsSecondary("primaryPermitteeAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsSecondary("primaryPermitteeCity")%><input type="hidden" name="primaryPermitteeCity" size="20" value="<%=rsSecondary("primaryPermitteeCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<%=rsSecondary("primaryPermitteeState")%><input type="hidden" name="primaryPermitteeState" size="2" value="<%=rsSecondary("primaryPermitteeState")%>">
															&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<%=rsSecondary("primaryPermitteeZip")%><input type="hidden" name="primaryPermitteeZip" size="10" value="<%=rsSecondary("primaryPermitteeZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Num. of Secondary Permittees:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<select name="secondaryPermittees">
																<%do until rsSP.eof%>
																	<option id="<%=rsSP("secondaryPermittees")%>"><%=rsSP("secondaryPermittees")%></option>
																<%rsSP.movenext
																loop%>
															</select>
														</td>
													</tr>
													
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>II. SITE ACTIVITY INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="constructionActivityCompleted" onClick="return KeepCount6()" />&nbsp;Construction Activity Completed&nbsp;&nbsp;
															<input type="checkbox" name="noLongerOwnerOperator" onClick="return KeepCount6()" />&nbsp;No Longer Owner / Operator of Construction Activity
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
													<tr>
														<td valign="top"><strong>Construction Activity:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="typeConstructionCommercial" <%=isChecked(rsSecondary("typeConstructionCommercial"))%> onClick="return KeepCount()" />&nbsp;Commercial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionIndustrial" <%=isChecked(rsSecondary("typeConstructionIndustrial"))%> onClick="return KeepCount()" />&nbsp;Industrial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionMunicipal" <%=isChecked(rsSecondary("typeConstructionMunicipal"))%> onClick="return KeepCount()" />&nbsp;Municipal&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionDOT" <%=isChecked(rsSecondary("typeConstructionDOT"))%> onClick="return KeepCount()" />&nbsp;DOT<br />
															<input type="checkbox" name="typeConstructionUtility" <%=isChecked(rsSecondary("typeConstructionUtility"))%> onClick="return KeepCount()" />&nbsp;Utility&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionResidential" <%=isChecked(rsSecondary("typeConstructionResidential"))%> onClick="return KeepCount()" />&nbsp;Residential/Subdivision Development
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="primaryPermitteeSubdivision" onClick="return KeepCount7()" />&nbsp;Primary Permittee of a Subdivision Development, or<br />
															<input type="checkbox" name="individualLot" onClick="return KeepCount7()" />&nbsp;Individual Lot, or<br />
															<input type="checkbox" name="individualLotWithinSWDA" onClick="return KeepCount7()" />&nbsp;Individual Lot within a Surface Water Drainage Area where the Primary Permittee has ceased Permit Coverage
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td><strong>Initial Receiving Water(s):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsSecondary("IRWName")%><input type="hidden" name="IRWName" size="30" value="<%=rsSecondary("IRWName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Municipal Storm Sewer<br /> System Owner/Operator:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsSecondary("MSSSOwnerOperator")%><input type="hidden" name="MSSSOwnerOperator" size="30" value="<%=rsSecondary("MSSSOwnerOperator")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Name of Receiving Water(s):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="RWName" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>III. CERTIFICATIONS. (Owner or Operator or both to initial as applicable.)</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify1" size="5">&nbsp;1. I certify under penalty of law that either: (a) all storm water discharges associated
															with construction activity from the portion of the construction activity where I was an Owner or
															Operator have ceased or have been eliminated; (b) all storm water discharges associated with
															construction activity from the identified site that are authorized by General NPDES Permit
															number indicated in Section I of this form have ceased; (c) I am no longer an Owner or
															Operator at the construction site and a new Owner or Operator has assumed operational
															control for those portions of the construction site where I previously had ownership or
															operational control; and/or if I am a primary permittee filing this Notice of Termination under
															Part VI.A.2. of this permit, I will notify by written correspondence to the subsequent legal title
															holder of any remaining lots that these lot Owners and /or Operators will become tertiary
															permittees for purposes of this permit and I will provide these tertiary permittees with the
															primary permittee�s Erosion, Sedimentation and Pollution Control Plan. I understand that by
															submitting this Notice of Termination, that I am no longer authorized to discharge storm water
															associated with construction activity by the general permit, and that discharging pollutants in
															storm water associated with construction activity to waters of Georgia is unlawful under the
															Georgia Water Quality Control Act and the Clean Water Act where the discharge is not
															authorized by a NPDES permit.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify2" size="5">&nbsp;2. I certify under penalty of law that this document and all attachments were
															prepared under my direction or supervision in accordance with a system designed to assure
															that qualified personnel properly gather and evaluate the information submitted. Based upon
															my inquiry of the person or persons who manage the system, or those persons directly
															responsible for gathering the information, the information submitted is, to the best of my
															knowledge and belief, true, accurate, and complete. I am aware that there are significant
															penalties for submitting false information, including the possibility of fine and imprisonment for
															knowing violations.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												</table>
												<table>
													<tr>
														<td valign="top"><strong>Owner�s Printed Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="ownerPrintedName" size="30">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<input type="text" name="ownerTitle" size="30" />
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td colspan="3"><input type="text" name="ownerSignDate" size="10">&nbsp;<a href="javascript:displayDatePicker('ownerSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>Operator�s Printed Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="operatorPrintedName" size="30">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<input type="text" name="operatorTitle" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td colspan="3"><input type="text" name="operatorSignDate" size="10">&nbsp;<a href="javascript:displayDatePicker('operatorSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>				
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<input type="hidden" name="projectID" value="<%=projectID%>" />
															<input type="hidden" name="secondaryFormID" value="<%=secondaryFormID%>" />
															<input type="hidden" name="primaryID" value="<%=primaryID%>" />
															<input type="hidden" name="processType" value="addNOTSec" />
															<input type="submit" value="Submit Data" class="formButton"/>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
										
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("NOT");
//  frmvalidator.addValidation("degree1","req","Please enter the degree");  
//  frmvalidator.addValidation("minute1","req","Please enter the minute");
//  frmvalidator.addValidation("second1","req","Please enter the second");
//  frmvalidator.addValidation("degree2","req","Please enter the degree");  
//  frmvalidator.addValidation("minute2","req","Please enter the minute");
//  frmvalidator.addValidation("second2","req","Please enter the second");  
//  frmvalidator.addValidation("ownerName","req","Please enter the owner's name");
//  frmvalidator.addValidation("ownerAddress","req","Please enter the owner's address");
//  frmvalidator.addValidation("ownerCity","req","Please enter the owner's city");
//  frmvalidator.addValidation("ownerZip","req","Please enter the owner's zip code");  
//  frmvalidator.addValidation("operatorName","req","Please enter the operator's name");
//  frmvalidator.addValidation("operatorPhone","req","Please enter the operator's address");
//  frmvalidator.addValidation("operatorAddress","req","Please enter the operator's city");
//  frmvalidator.addValidation("operatorCity","req","Please enter the operator's zip code");
//  frmvalidator.addValidation("operatorZip","req","Please enter the operator's zip code");  
//  frmvalidator.addValidation("facilityContact","req","Please enter the facility contact name");
//  frmvalidator.addValidation("facilityContactPhone","req","Please enter the facility contact phone number");
//  frmvalidator.addValidation("RWName","req","Please enter the name of receiving water(s)");      
//  frmvalidator.addValidation("certify1","req","Please enter initials")
//  frmvalidator.addValidation("certify2","req","Please enter initials")
//  frmvalidator.addValidation("ownerPrintedName","req","Please enter the owner's printed name");
//  frmvalidator.addValidation("ownerTitle","req","Please enter the owner's title");  
//  frmvalidator.addValidation("ownerSignDate","req","Please enter the date the owner signs the form");
//  frmvalidator.addValidation("operatorPrintedName","req","Please enter the operators's printed name");
//  frmvalidator.addValidation("operatorTitle","req","Please enter the operators's title");  
//  frmvalidator.addValidation("operatorSignDate","req","Please enter the date the operator signs the form"); 

</script>
<%
rs.close
rsState.close
set rs = nothing
set rsState = nothing
DataConn.close
set DataConn = nothing
%>