<%
topicID = request("topicID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetToolboxTopics"
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

if topicID <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetToolboxTopic"
	   .parameters.Append .CreateParameter("@topicID", adInteger, adParamInput, 8, topicID)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsTopic = oCmd.Execute
	Set oCmd = nothing
	
	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAllUsers"
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsUsers = oCmd.Execute
	Set oCmd = nothing

end if

%>
<script language="JavaScript">
<!-- Begin

function topic_onchange(toolboxSession,topicID) {
   window.location.href = 'form.asp?formType=toolboxSession&topicID='+topicID;
}

//  End -->
</script>
<form name="toolboxSession" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Conduct Toolbox Session</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Session Topic:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><!-- -->
												<select name="topicID" onChange="return topic_onchange(toolboxSession,this.getElementsByTagName('option')[this.selectedIndex].value)">
													<option value="">--Select Topic---</option>
													<%do until rs.eof
														if trim(topicID) = trim(rs("topicID")) then%>
															<option selected="selected" value="<%=rs("topicID")%>"><%=rs("topic")%></option>
														<%else%>
															<option value="<%=rs("topicID")%>"><%=rs("topic")%></option>
													<%end if
													rs.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										
										
										<%if topicID <> "" then%>
											
											<tr>
												<td valign="top" align="right"><strong>View/Download Document:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<a href="topics/<%=rsTopic("document")%>" target="_blank"><%=rsTopic("document")%></a>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Related Info:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<a href="safetyTopic.asp?topicID=<%=topicID%>" title="Related Information" rel="gb_page_fs[]">View</a>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
											<!--get all users in the system so we can check -->
											<script type="text/javascript" src="jquery/jquery.js"></script>
											<script type="text/javascript" src="jquery/jquery.ui.js"></script>
											<script type="text/javascript" src="jquery/jquery.asmselect.js"></script>
											<script type="text/javascript">
												$(document).ready(function() {
													$("select[multiple]").asmSelect({
														addItemTarget: 'bottom',
														animate: true,
														highlight: true,
														sortable: true
													});
													
												}); 
										
											</script>
											<link rel="stylesheet" type="text/css" href="jquery/jquery.asmselect.css" />
											<tr>
												<td valign="top" align="right"><strong>Participants:</strong></td>
												<td></td>
												<td valign="top">															
													<select id="participants" multiple="multiple" name="participants" title="select all that apply">
														<%do until rsUsers.eof%>
															<option value="<%=rsUsers("userID")%>"><%=rsUsers("firstName")%>&nbsp;<%=rsUsers("lastName")%></option>
														<%rsUsers.movenext
														loop%>
													</select>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
											<tr>
												<td colspan="3" align="right">
													<input type="hidden" name="userID" value="<%=session("ID")%>" />
													<input type="hidden" name="processType" value="addToolboxSession" />
													<input type="submit" name="submitForm" value="Save" class="formButton" />
												</td>
											</tr>
										<%end if%>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<%
rs.close
set rs = nothing
DataConn.close
set DataConn = nothing
%>