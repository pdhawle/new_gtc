<%
projectID = Request("projectID")
clientID = request("clientID")
bNew = request("bNew")

'get all of the approved, open items for the project
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetWOOpenItems"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsItems = oCmd.Execute
Set oCmd = nothing


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsProject = oCmd.Execute
Set oCmd = nothing
%>

<script type="text/javascript">
	<!--
	function formControl(submitted) 
	{
	   if(submitted=="1") 
		{
	   addWorkOrder.Submit.disabled=true
	   document.addWorkOrder.submit();
	  // alert("Thanks for your comments!")
		}
	}
	

	function addItem(responsiveAction,num,projectID,clientID) {
		//form.asp?formType=addWorkOrder&projectID=8
	   addWorkOrder.action = 'form.asp?formType=addWorkOrder&responsiveActionID='+responsiveAction+'&num='+num+'&projectID='+projectID+'&clientID='+clientID;
	   addWorkOrder.submit(); 
	}


	function addItemGen(num,projectID,clientID) {
		//form.asp?formType=addWorkOrder&projectID=8
	   addWorkOrder.action = 'form.asp?formType=addWorkOrder&gen=True&num2='+num+'&projectID='+projectID+'&clientID='+clientID;
	   addWorkOrder.submit(); 
	}
// -->
</script>

<form name="addWorkOrder" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Generate Work Order</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">						
						&nbsp;&nbsp;<a href="workOrdersList.asp?projectID=<%=projectID%>&clientID=<%=clientID%>" class="footerLink">work orders list</a>				
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										
										<tr><td colspan="10"><strong>Project:</strong> <%=rsProject("projectName")%></td></tr>
										<tr><td colspan="10">For each item, add materials, amounts and any comments you may have.</td></tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">ReportID</span></td>
											<td><span class="searchText">Report Date</span></td>
											<td><span class="searchText">Ref #</span></td>
											<td><span class="searchText">Corrective Action Needed</span></td>
											<td><span class="searchText">Location</span></td>	
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%
			
										If rsItems.eof then%>
											<tr><td></td><td colspan="10">there are no records to display</td></tr>
										<%else
											blnChange = true
											
											Do until rsItems.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><a href="downloads/swsir_<%=rsItems("reportID")%>.pdf" target="_blank"><%=rsItems("reportID")%></a></td>
													<td><%=rsItems("inspectionDate")%></td>
													<td><%=rsItems("reportID")%>-<%=rsItems("referenceNumber")%></td>
													<td><%=rsItems("actionNeeded")%></td>
													<td><%=rsItems("location")%></td>
												</tr>
												<%If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<!--add the materials list here-->
													<td></td>
													<td colspan="10"><br />
														<%
														
														
														If trim(rsItems("responsiveActionID")) = trim(request("responsiveActionID")) then
															session("num" & rsItems("responsiveActionID")) = request("num")														
														else
															session("num" & rsItems("responsiveActionID")) = session("num" & rsItems("responsiveActionID"))
														end if
														iItem = 0
														
														itemNumber = session("num" & rsItems("responsiveActionID"))
														if bNew = "True" then
															session("num" & rsItems("responsiveActionID")) = 0
														end if
														'response.Write itemNumber & "<br>"
														
														for counter = 1 to itemNumber
															iItem = iItem + 1
															
															
															'Create command
															Set oCmd = Server.CreateObject("ADODB.Command")
																
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetMaterials"
															   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
															   .CommandType = adCmdStoredProc   
															End With
																		
															Set rsMaterials = oCmd.Execute
															Set oCmd = nothing%>
													
															<table cellpadding="0" cellspacing="0">
																<tr>
																	<td><strong>Material Needed:</strong>&nbsp;</td>
																	<td>
																		<select name="item&material<%=counter%>_<%=rsItems("responsiveActionID")%>">
																			<%do until rsMaterials.eof
																				if trim(rsMaterials("materialID")) = trim(request("item&material" & counter & "_" & rsItems("responsiveActionID"))) then%>
																					<option selected="selected" value="<%=rsMaterials("materialID")%>"><%=rsMaterials("material")%> - <%=formatCurrency(rsMaterials("cost"),2)%>/<%=rsMaterials("unit")%></option>
																				<%else%>
																					<option value="<%=rsMaterials("materialID")%>"><%=rsMaterials("material")%> - <%=formatCurrency(rsMaterials("cost"),2)%>/<%=rsMaterials("unit")%></option>
																			<%end if
																			rsMaterials.movenext
																			loop%>
																		</select>
																	</td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td><strong>Est. Quantity:</strong>&nbsp;<br />(number only)&nbsp;</td>
																	<td><input type="text" name="item&amount<%=counter%>_<%=rsItems("responsiveActionID")%>" size="3" value="<%=request("item&amount" & counter & "_" & rsItems("responsiveActionID"))%>" /></td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td><strong>Comments:</strong>&nbsp;</td>
																	<td><input type="text" name="item&comments<%=counter%>_<%=rsItems("responsiveActionID")%>" size="30" value="<%=request("item&comments" & counter & "_" & rsItems("responsiveActionID"))%>" /></td>
																	<td>&nbsp;&nbsp;<strong>Warranty/No Charge:</strong>&nbsp;</td>
																	<td><input type="checkbox" name="item&warrantyNoCharge<%=counter%>_<%=rsItems("responsiveActionID")%>" <%=requestIsChecked(request("item&warrantyNoCharge" & counter & "_" & rsItems("responsiveActionID")))%>/></td>
																</tr>
																<!--<tr><td colspan="8"><a href="#" onclick="addItem(<%'=projectID%>,1)">add material</a></td></tr>
																<tr><td colspan="8"><img src="images/pix.gif" width="1" height="3"></td></tr>-->
															</table><br />
														<%next%>
														<%If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if
														'session("item" & rsItems("responsiveActionID")) + 1%>
															<td></td>
															<td colspan="10">
																<a href="#" onclick="addItem(<%=rsItems("responsiveActionID")%>,<%=iItem +1%>,<%=projectID%>,<%=clientID%>)">add item</a><br />
																<%if itemNumber > 0 then%>
																	<a href="#" onclick="addItem(<%=rsItems("responsiveActionID")%>,<%=iItem -1%>,<%=projectID%>,<%=clientID%>)">remove item</a><br />
																<%end if%>
																<br />
															</td>
														</tr>
													</td>
												</tr>
												
												
											<%
											sAllVal = sAllVal & rsItems("responsiveActionID")
											rsItems.movenext
											if not rsItems.eof then
												sAllVal = sAllVal & ","
											end if
											
											
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
										
										
						<!--add general items to the work order-->

												<%If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><strong>General Items</strong></td>
													<td><strong>N/A</strong></td>
													<td><strong>N/A</strong></td>
													<td><strong>N/A</strong></td>
													<td><strong>N/A</strong></td>
												</tr>
												<%If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<!--add the materials list here-->
														<td></td>
														<td colspan="10"><br />
															<%
															
															if request("gen") = "True" then
																session("num2") = request("num2")
															end if
																													
															iItem2 = 0
														
															'response.write "itemNum2" &   session("num2")
															if bNew = "True" then
																session("num2") = 0
															end if

															for counter2 = 1 to session("num2")'itemNumber2
																iItem2 = iItem2 + 1
																'Create command
																Set oCmd = Server.CreateObject("ADODB.Command")
																	
																With oCmd
																   .ActiveConnection = DataConn
																   .CommandText = "spGetMaterials"
																   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
																   .CommandType = adCmdStoredProc   
																End With
																	
																		
																Set rsMaterials = oCmd.Execute
																Set oCmd = nothing%>
														
																<table cellpadding="0" cellspacing="0">
																	<tr>
																		<td><strong>Material Needed:</strong>&nbsp;</td>
																		<td>
																			<select name="gItem&material<%=counter2%>">
																				<%do until rsMaterials.eof
																					if trim(rsMaterials("materialID")) = trim(request("gItem&material" & counter2)) then%>
																						<option selected="selected" value="<%=rsMaterials("materialID")%>"><%=rsMaterials("material")%> - <%=formatCurrency(rsMaterials("cost"),2)%>/<%=rsMaterials("unit")%></option>
																					<%else%>
																						<option value="<%=rsMaterials("materialID")%>"><%=rsMaterials("material")%> - <%=formatCurrency(rsMaterials("cost"),2)%>/<%=rsMaterials("unit")%></option>
																				<%end if
																				rsMaterials.movenext
																				loop%>
																			</select>
																		</td>
																		<td><img src="images/pix.gif" width="10" height="1"></td>
																		<td><strong>Est. Quantity:</strong>&nbsp;<br />(number only)&nbsp;</td>
																		<td><input type="text" name="gItem&amount<%=counter2%>" size="3" value="<%=request("gItem&amount" & counter2)%>" /></td>
																		<td><img src="images/pix.gif" width="10" height="1"></td>
																		<td><strong>Comments:</strong>&nbsp;</td>
																		<td><input type="text" name="gItem&comments<%=counter2%>" size="30" value="<%=request("gItem&comments" & counter2)%>" /></td>
																		<td>&nbsp;&nbsp;<strong>Warranty/No Charge:</strong>&nbsp;</td>
																		<td><input type="checkbox" name="gItem&warrantyNoCharge<%=counter2%>" <%=requestIsChecked(request("gItem&warrantyNoCharge" & counter2))%>/></td>
																	</tr>
																</table><br />
															<%next%>
															<%If blnChange = true then%>
																<tr class="rowColor">
															<%else%>
																<tr>
															<%end if
															'session("item" & rsItems("responsiveActionID")) + 1%>
																<td></td>
																<td colspan="10">
																	<a href="#" onclick="addItemGen(<%=iItem2 +1%>,<%=projectID%>,<%=clientID%>)">add item</a><br />
																	<%if session("num2") > 0 then'if itemNumber2 > 0 then%>
																		<a href="#" onclick="addItemGen(<%=iItem2 -1%>,<%=projectID%>,<%=clientID%>)">remove item</a><br />
																	<%end if%>
																	<br />
																</td>
															</tr>
														</td>
													</tr>
													
									<!--end of adding general items to the work order-->
										<tr>
											<td colspan="6"><br />
												<strong>General Comments</strong> (will be displayed on the Work Order in it's own section.)<br />
												<textarea name="generalComments" rows="3" cols="35"><%=request("generalComments")%></textarea>
											</td>
										</tr>
										<!--<tr>
											<td colspan="6"><br />
												<strong>Warranty/No Charge</strong>&nbsp;&nbsp;<input type="checkbox" name="warrantyNoCharge" <%'=requestIsChecked(request("warrantyNoCharge"))%>/>
											</td>
										</tr>-->
										
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
							<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
							<tr>
								<td colspan="3" align="right">
									<input type="hidden" name="allItems" value="<%=sAllVal%>">
									<input type="hidden" name="itemNumber" value="<%=iItem%>" />
									<input type="hidden" name="itemNumber2" value="<%=iItem2%>" />
									<input type="hidden" name="projectID" value="<%=projectID%>" />
									<input type="hidden" name="clientID" value="<%=clientID%>" />
									<input type="hidden" name="processType" value="addWorkOrder" />
									<input type="submit" name="Submit" value="  Save  " class="formButton" onClick="formControl(1)"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  //var frmvalidator  = new Validator("addWorkOrder");
  //frmvalidator.addValidation("category","req","Please enter a category");
</script>