<%
projectID = request("projectID")
iPrimaryID = request("id")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetPrimaryNOI"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iPrimaryID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsPrimary = oCmd.Execute
Set oCmd = nothing
	
'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for county list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCounties"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCounty = oCmd.Execute
Set oCmd = nothing
%>

<script language="javascript" type="text/javascript">
<!-- 
function DoMath1(){
	var Units = document.noiFeeForm.acresDistirbedLocal.value
	var Rate = 40
	var Total = Units * Rate
	document.noiFeeForm.acresDistirbedLocalCost.value = ""
	document.noiFeeForm.acresDistirbedLocalCost.value = Total
}

function DoMath2(){
	var Units = document.noiFeeForm.acresDistirbedNoLocal.value
	var Rate = 80
	var Total = Units * Rate
	document.noiFeeForm.acresDistirbedNoLocalCost.value = ""
	document.noiFeeForm.acresDistirbedNoLocalCost.value = Total
}

function DoMath3(){
	var Units = document.noiFeeForm.acresDistirbedExempt.value
	var Rate = 80
	var Total = Units * Rate
	document.noiFeeForm.acresDistirbedExemptCost.value = ""
	document.noiFeeForm.acresDistirbedExemptCost.value = Total
}

function calcTotal(f){
	total = 0;
	if (f.acresDistirbedLocalCost.value && !isNaN(f.acresDistirbedLocalCost.value))
	total += parseInt(f.acresDistirbedLocalCost.value);
	if (f.acresDistirbedNoLocalCost.value && !isNaN(f.acresDistirbedNoLocalCost.value))
	total += parseInt(f.acresDistirbedNoLocalCost.value);
	if (f.acresDistirbedExemptCost.value && !isNaN(f.acresDistirbedExemptCost.value))
	total += parseInt(f.acresDistirbedExemptCost.value);
	f.totalFees.value=total;
}
//-->
</script>

<form name="noiFeeForm" method="post" action="processNOI.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Add Fee Form</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<b>National Pollutant Discharge Elimination System<br />
												General Permit Fee Form<br />
												Georgia Dept. of Natural Resources<br />
												Environmental Protection Division</b><br /><br />
												
												<strong>NOTE: Disabled data must be changed on the NOI primary data form.</strong>
											</td>
											<td></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td><strong>Primary Permittee:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("ownerName")%><input type="hidden" name="primaryPermittee" size="30" value="<%=rsPrimary("ownerName")%>" />
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>				
													<tr>
														<td><strong>Permittee Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("ownerAddress")%><input type="hidden" name="permitteeAddress" size="30" value="<%=rsPrimary("ownerAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Permittee City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("ownerCity")%><input type="hidden" name="permitteeCity" size="30" value="<%=rsPrimary("ownerCity")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Permittee State:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("ownerState")%><input type="hidden" name="permitteeState" size="5" value="<%=rsPrimary("ownerState")%>">
															&nbsp;&nbsp;<strong>Permittee Zip:</strong>&nbsp;<%=rsPrimary("ownerZip")%><input type="hidden" name="permitteeZip" size="5" value="<%=rsPrimary("ownerZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Contact Telephone:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("facilityContactPhone")%><input type="hidden" name="contactTelephone" size="30" value="<%=rsPrimary("facilityContactPhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Project Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("siteProjectName")%><input type="hidden" name="projectName" size="30" value="<%=rsPrimary("siteProjectName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Location / Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("projectAddress")%><input type="hidden" name="projectAddress" size="30" value="<%=rsPrimary("projectAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>County:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("projectCounty")%><input type="hidden" name="projectCounty" size="30" value="<%=rsPrimary("projectCounty")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("projectCity")%><input type="hidden" name="projectCity" size="30" value="<%=rsPrimary("projectCity")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												</table>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td>
															<strong>Acres Disturbed in an area with a local issuing authority:</strong><br />
															<strong>Do not include fees payable to local issuing authorities.</strong>
														</td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%
															dim iCost
									
															iCost = rsPrimary("estimatedDisturbedAcerage") * 40
															%>
															<input type="text" name="acresDistirbedLocal" size="5" value="<%=rsPrimary("estimatedDisturbedAcerage")%>" onchange="DoMath1();calcTotal(this.form)"> x $40 per acre = $ <input type="text" name="acresDistirbedLocalCost" size="5" value="<%=iCost%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td>
															<strong>Acres Disturbed in an area with no local issuing authority:</strong>
														</td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="acresDistirbedNoLocal" size="5" onchange="DoMath2();calcTotal(this.form)" value="0"> x $80 per acre = $ <input type="text" name="acresDistirbedNoLocalCost" size="5" value="0">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td>
															<strong>Acres Disturbed by an entity exempt from local issuing <br />authority regulation pursuant to statute:</strong>
														</td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="acresDistirbedExempt" size="5" onchange="DoMath3();calcTotal(this.form)" value="0"> x $80 per acre = $ <input type="text" name="acresDistirbedExemptCost" size="5" value="0">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="2"></td>
														<td align="right">
															<strong>TOTAL FEE* =</strong> $ <input type="text" name="totalFees" size="5" value="<%=iCost%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="2"></td>
														<td align="right">
															<strong>Check Number:</strong> <input type="text" name="checkNumber" size="5">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
												</table>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td colspan="3"><strong>Submitted By:</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="dateSubmitted" size="10">&nbsp;<a href="javascript:displayDatePicker('dateSubmitted')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Print Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="printedName" size="30">&nbsp;&nbsp;<strong>Title:</strong>&nbsp;<input type="text" name="title" size="30">
														</td>
													</tr>
													
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<input type="hidden" name="projectID" value="<%=projectID%>" />
															<input type="hidden" name="primaryFormID" value="<%=iPrimaryID%>" />
															<input type="hidden" name="processType" value="addFeeForm" />
															<input type="submit" value="Submit Data" class="formButton"/>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															*please double check total before submitting the form
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>

<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("noiFeeForm");
  frmvalidator.addValidation("acresDistirbedLocal","req","Please enter the acres");
  //frmvalidator.addValidation("acresDistirbedLocal","numeric")
  frmvalidator.addValidation("acresDistirbedLocalCost","req","Please enter the cost");
  //frmvalidator.addValidation("acresDistirbedLocalCost","numeric")
  frmvalidator.addValidation("acresDistirbedNoLocal","req","Please enter the acres");
  //frmvalidator.addValidation("acresDistirbedNoLocal","numeric")
  frmvalidator.addValidation("acresDistirbedNoLocalCost","req","Please enter the cost");
 // frmvalidator.addValidation("acresDistirbedNoLocalCost","numeric")
  frmvalidator.addValidation("acresDistirbedExempt","req","Please enter the acres");
 // frmvalidator.addValidation("acresDistirbedExempt","numeric")
  frmvalidator.addValidation("acresDistirbedExemptCost","req","Please enter the cost");
 // frmvalidator.addValidation("acresDistirbedExemptCost","numeric")
  frmvalidator.addValidation("totalFees","req","Please enter the total fees");
 // frmvalidator.addValidation("totalFees","numeric")
  //frmvalidator.addValidation("checkNumber","req","Please enter the check number");  
  //frmvalidator.addValidation("dateSubmitted","req","Please enter the date submitted");
  //frmvalidator.addValidation("printedName","req","Please enter the person's printed name");
  //frmvalidator.addValidation("title","req","Please enter the person's titlee");

</script>