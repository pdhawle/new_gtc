<%
projectID = request("projectID")
On Error Resume Next
Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedProjects"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsProjects = oCmd.Execute
Set oCmd = nothing
	
	
If projectID <> "" then
	
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCoverage"
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
	'Create command for state list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetStates"
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsState = oCmd.Execute
	Set oCmd = nothing
	
	'Create command for county list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCounties"
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing
	
	'Create command for NTU Value list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetNTUValue"
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsNTUValue = oCmd.Execute
	Set oCmd = nothing
	
	'Create command for secondary permittee list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetNumSecPermittees"
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsSP = oCmd.Execute
	Set oCmd = nothing
	
end if
%>

<script language="javascript" type="text/javascript">
<!--
function docheck(){
  if (document.noiPrimary.estimatedDisturbedAcerage.value > 49)  
  	document.noiPrimary.locationMap.checked=true;
  if (document.noiPrimary.estimatedDisturbedAcerage.value < 50)  
  	document.noiPrimary.locationMap.checked=false;
  if (document.noiPrimary.estimatedDisturbedAcerage.value > 49) 
	document.noiPrimary.ESPControlPlan.checked=true;
  if (document.noiPrimary.estimatedDisturbedAcerage.value < 50) 
	document.noiPrimary.ESPControlPlan.checked=false;
  if (document.noiPrimary.estimatedDisturbedAcerage.value > 49) 
	document.noiPrimary.timingSchedule.checked=true;
  if (document.noiPrimary.estimatedDisturbedAcerage.value < 50) 
	document.noiPrimary.timingSchedule.checked=false;
}


function KeepCount() {

	var NewCount = 0
						
	if (document.noiPrimary.typeConstructionCommercial.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimary.typeConstructionIndustrial.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimary.typeConstructionMunicipal.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimary.typeConstructionLinear.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimary.typeConstructionUtility.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimary.typeConstructionResidential.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiPrimary; return false;
	}
} 

function KeepCount2() {

	var NewCount = 0
						
	if (document.noiPrimary.IRWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimary.IRWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiPrimary; return false;
	}
} 

function KeepCount3() {

	var NewCount = 0
						
	if (document.noiPrimary.RWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimary.RWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiPrimary; return false;
	}
} 

function KeepCount4() {
  
	var NewCount = 0
						
	if (document.noiPrimary.samplingOfOutfall.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimary.samplingOfRecievingStream.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimary.troutStream.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiPrimary; return false;
	}
} 


function dept_onchange(noiPrimary) {
   document.noiPrimary.action = "form.asp?formType=primary";
   noiPrimary.submit(); 
}

//-->
</script>

<form name="noiPrimary" method="post" action="processNOI.asp" onSubmit="return ValidateDate()">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Notice of Intent</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr bgcolor="#FFFFFF">
											<td colspan="9">
												<!--get a dropdown of all of the assigned projects for the user-->
													&nbsp;&nbsp;&nbsp;&nbsp;Please select a project to create a NOTICE OF INTENT.<br>
													&nbsp;&nbsp;&nbsp;&nbsp;<select name="projectID" onChange="return dept_onchange(noiPrimary)">
														<option value="">--Select Project--</option>
														<%do until rsProjects.eof
														if trim(rsProjects("projectID")) = trim(ProjectID) then%>
															<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
														<%else%>
															<option value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
														<%end if
														rsProjects.movenext
														loop
														rsProjects.close
														set rsProjects = nothing%>
													</select><br /><br />
											</td>
										</tr>
										<%If projectID <> "" then%>
											<tr>
												<td><img src="images/pix.gif" width="10" height="1"></td>
												<td>
													<strong>State of Georgia</strong><br />
													<strong>Department of Natural Resources</strong><br />
													<strong>Environmental Protection Division</strong><br /><br />

													<a href="instructions_primary.asp" target="_blank">Instructions</a><br><br />
													<strong>For Coverage Under the 2008 Re-Issuance of the NPDES General Permits </strong><br />
													<strong>To Discharge Storm Water Associated With Construction Activity</strong><br><br>
													<strong>PRIMARY PERMITTEE</strong>
												</td>
												<td></td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
											<tr>
												<td><img src="images/pix.gif" width="10" height="1"></td>
												<td>
													<table border="0" cellpadding="0" cellspacing="0">	
														<tr>
															<td valign="top"><strong>Notice Of Intent:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>Initial Notification (New Facility/Construction Site)<br />
																<input type="hidden" name="noticeOfIntent" value="1" />
																<!--<input type="radio" name="noticeOfIntent" value="2" />&nbsp;Re-Issuance Notification (Existing Facility/Construction Site)<br />
																<input type="radio" name="noticeOfIntent" value="3" />&nbsp;Change of Information (Applicable only if the NOI was submitted after August 1, 2008)-->
															</td>
														</tr>												
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td><strong>Coverage Desired:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																GAR 100002-Infrastructure
																<input type="hidden" name="coverageDesired" value="2" />
																<!--<select name="coverageDesired">
																	<%'do until rs.eof%>
																		<option value="<%'=rs("coverageDesiredID")%>"><%'=rs("coverageDesiredName")%></option>
																	<%'rs.movenext
																	'loop%>
																</select>-->
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3"><strong>I. SITE/OWNER/OPERATOR INFORMATION</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td><strong>Project Construction Site Name:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=siteProjectName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a><br /><img src="images/pix.gif" width="180" height="1"></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="siteProjectName" size="70" maxlength="200">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top"><strong>GPS Location of Const. Exit:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=GPSLocation','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<table>
																	<tr>
																		<td>
																			Degrees<br /><span class="footer">From 0 to 90</span><br />
																			<input type="text" name="degree1" size="5" maxlength="2">
																		</td>
																		<td><img src="images/pix.gif" width="10" height="1"></td>
																		<td>
																			Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																			<input type="text" name="minute1" size="5" maxlength="2"> <input type="text" name="second1" size="5" maxlength="5"> <input type="hidden" name="latitude" value="North">North Latitude
																		</td>
																	</tr>
																	<tr>
																		<td>
																			Degrees<br /><span class="footer">From 0 to 90</span><br />
																			<input type="text" name="degree2" size="5" maxlength="2">
																		</td>
																		<td><img src="images/pix.gif" width="10" height="1"></td>
																		<td>
																			Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																			<input type="text" name="minute2" size="5" maxlength="2"> <input type="text" name="second2" size="5" maxlength="5"> <input type="hidden" name="longitude" value="West">West Longitude
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Construction Site Street Address:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=projectAddress','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="projectAddress" size="70" maxlength="1000">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>City(if applicable):</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=projectCity','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="projectCity" size="30">&nbsp;&nbsp;<strong>County:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=projectCounty','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>
																&nbsp;&nbsp;
																<select name="projectCounty">
																	<%do until rsCounty.eof%>
																		<option value="<%=rsCounty("county")%>"><%=rsCounty("county")%></option>
																	<%rsCounty.movenext
																	loop
																	rsCounty.moveFirst%>
																</select>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Common Development Name:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=subdivision','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="subdivision" size="30" value="N/A">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Owner�s Name:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="ownerName" size="30" value="Georgia Transmission Corporation">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Address:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerAddress','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="ownerAddress" size="30" value="2100 E. Exchange Place">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>City:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerCity','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="ownerCity" size="20" value="Tucker">&nbsp;&nbsp;<strong>State:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerState','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;
																<select name="ownerState">
																	<%do until rsState.eof
																		if trim(rsState("stateID")) = "GA" then%>
																			<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																		<%else%>
																			<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																		<%end if%>
																	<%rsState.movenext
																	loop
																	rsState.moveFirst%>
																</select>&nbsp;&nbsp;
																<strong>Zip:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerZip','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;<input type="text" name="ownerZip" size="10" value="30084">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Duly Authorized Representative:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="authorizedRep" size="30" value="N/A">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;<input type="text" name="authorizedRepPhone" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Operator�s Name:&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="operatorName" size="30" value="Georgia Transmission Corporation">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="operatorPhone" size="30" value="770-270-7400">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Address:&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorAddress','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="operatorAddress" size="30" value="2100 E. Exchange Place">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>City:&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorCity','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="operatorCity" size="20" value="Tucker">&nbsp;&nbsp;<strong>State:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorState','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;
																<select name="operatorState">
																	<%do until rsState.eof
																		if trim(rsState("stateID")) = "GA" then%>
																			<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																		<%else%>
																			<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																		<%end if%>
																	<%rsState.movenext
																	loop%>
																</select>&nbsp;&nbsp;
																<strong>Zip:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorZip','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;<input type="text" name="operatorZip" size="10" value=" 30084">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Facility Contact:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=facilityContact','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="facilityContact" size="30">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=facilityContactPhone','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;<input type="text" name="facilityContactPhone" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3"><strong>II. SITE ACTIVITY INFORMATION</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td><strong>Start Date:&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=startDate','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td valign="top">
																<input type="text" name="startDate" size="10">&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Completion Date:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=completionDate','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;<input type="text" name="completionDate" size="10">&nbsp;<a href="javascript:displayDatePicker('completionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Estimated Disturbed Acreage:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=estimatedDisturbedAcerage','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td valign="top">
																<input type="text" name="estimatedDisturbedAcerage" size="5" onchange="docheck()">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td colspan="3" valign="top">
																<strong>Does the Erosion, Sedimentation and Pollution Control Plan (Plan) provide for disturbing more than 50 acres at any one time for each individual permittee (i.e., primary, secondary or tertiary permittees), or more than 50 contiguous acres total at any one time ?</strong>
															</td>
														</tr>
														<tr>
															<td valign="top" colspan="3">
																<input type="radio" name="disturb50" value="1" />&nbsp;YES<br />
																<input type="radio" name="disturb50" value="2" />&nbsp;NO<br />
																<input type="radio" name="disturb50" value="3" />&nbsp;N/A  - if the Plan was submitted prior to the effective date of the General NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br />
																<input type="radio" name="disturb50" value="4" checked="checked" />&nbsp;N/A � if construction activities are covered under the General NPDES Permit No. GAR100002 for Infrastructure construction projects.
															</td>
														</tr>
					
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td valign="top"><strong>Type Construction Activity:</strong><br />(please choose only one)</td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="checkbox" name="typeConstructionCommercial" onClick="return KeepCount()" />&nbsp;Commercial&nbsp;&nbsp;<input type="checkbox" name="typeConstructionIndustrial" onClick="return KeepCount()" />&nbsp;Industrial&nbsp;&nbsp;<input type="checkbox" name="typeConstructionMunicipal" onClick="return KeepCount()" />&nbsp;Municipal&nbsp;&nbsp;<input type="checkbox" name="typeConstructionLinear" onClick="return KeepCount()" />&nbsp;Linear<br />
																<input type="checkbox" name="typeConstructionUtility" checked="checked" onClick="return KeepCount()" />&nbsp;Utility&nbsp;&nbsp;<input type="checkbox" name="typeConstructionResidential" onClick="return KeepCount()" />&nbsp;Residential/Subdivision Development
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Number of Secondary Permittees:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>N/A
																<input type="hidden" name="secondaryPermittees" value="N/A" />
																<!--<select name="secondaryPermittees">
																	<%'do until rsSP.eof%>
																		<option value="<%'=rsSP("secondaryPermittees")%>"><%'=rsSP("secondaryPermittees")%></option>
																	<%'rsSP.movenext
																	'loop%>
																</select>-->
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3"><strong>III. RECEIVING WATER INFORMATION</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td><strong>A. Initial Receiving Water(s):</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=IRWName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="IRWName" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top"></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="checkbox" name="IRWTrout" onClick="return KeepCount2()" />&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="IRWWarmWater" onClick="return KeepCount2()" />&nbsp;Warm Water Fisheries Stream
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>B. Municipal Storm Sewer<br />&nbsp;&nbsp;&nbsp; System Owner/Operator:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=MSSSOwnerOperator','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="MSSSOwnerOperator" size="30" value="N/A">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Name of Receiving Water(s):</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=RWName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="RWName" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top"></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="checkbox" name="RWTrout" onClick="return KeepCount3()" />&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="RWWarmWater" onClick="return KeepCount3()" />&nbsp;Warm Water Fisheries Stream
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td colspan="3">
																<strong>C.</strong>&nbsp;<input type="checkbox" name="samplingOfRecievingStream" onClick="return KeepCount4()" />&nbsp;Sampling of Receiving Stream(s)&nbsp;&nbsp;<input type="checkbox" name="troutStreamRecieve" onClick="return KeepCount4()" />&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="warmWaterRecieve" onClick="return KeepCount4()" />&nbsp;Warm Water Fisheries Stream
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td colspan="3">
																<strong>D.</strong>&nbsp;<input type="checkbox" name="samplingOfOutfall" onClick="return KeepCount5()" />&nbsp;Sampling of Outfall(s)&nbsp;&nbsp;<input type="checkbox" name="troutStream" onClick="return KeepCount5()" />&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="warmWater" onClick="return KeepCount5()" />&nbsp;Warm Water Fisheries Stream
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td colspan="3">
																&nbsp;&nbsp;<strong>Number of Outfalls:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=numberOfOutfalls','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a> <input type="text" name="numberOfOutfalls" size="5" />
																&nbsp;&nbsp;
																<strong>Construction Site Size (acres):</strong>&nbsp;<input type="text" name="constructionSiteSize" size="5" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td colspan="3">
																<strong>Appendix B NTU Value:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=appendixBNTUValue','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a> <select name="appendixBNTUValue">
																	<%do until rsNTUValue.eof%>
																		<option value="<%=rsNTUValue("ntuValue")%>"><%=rsNTUValue("ntuValue")%></option>
																	<%rsNTUValue.movenext
																	loop%>
																</select>
																&nbsp;&nbsp;<strong>Surface Water Drainage Area:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=SWDrainageArea','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a> <input type="text" name="SWDrainageArea" size="5" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3" valign="top">
																<strong>Does the facility/construction site discharge storm water into an Impaired Stream Segment, or within one (1) linear mile upstream of and within the same watershed as, any portion of an Impaired Stream Segment identified as �not supporting� its designated use(s), as shown on Georgia�s 2008 and subsequent �305(b)/303(d) List Documents (Final)� listed for the criteria violated, �Bio F� (Impaired Fish Community) and/or �Bio M� (Impaired Macroinvertebrate Community), within Category 4a, 4b or 5, and the potential cause is either �NP� (nonpoint source) or �UR� (urban runoff)?</strong>
															</td>
														</tr>
														<tr>
															<td valign="top" colspan="3">
																<input type="radio" name="siteDischargeStormWater" value="1" checked="checked" />&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;&nbsp;<input type="text" name="impairedStream" /><br />
																<input type="radio" name="siteDischargeStormWater" value="2" />&nbsp;NO<br />
																<input type="radio" name="siteDischargeStormWater" value="3" />&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Common Development construction activities.<br />
																<input type="radio" name="siteDischargeStormWater" value="4" />&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES Permit No. GAR100002 for Infrastructure construction activities.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3" valign="top">
																<strong>Does the facility/construction site discharge storm water into an Impaired Stream Segment where a Total Maximum Daily Load (TMDL) Implementation Plan for �sediment� was finalized at least six (6) months prior to the submittal of the NOI ?</strong>
															</td>
														</tr>
														<tr>
															<td valign="top" colspan="3">
																<input type="radio" name="siteDischargeStormWater2" value="1" checked="checked" />&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;&nbsp;<input type="text" name="impairedStream2" /><br />
																<input type="radio" name="siteDischargeStormWater2" value="2" />&nbsp;NO<br />
																<input type="radio" name="siteDischargeStormWater2" value="3" />&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Common Development construction activities.<br />
																<input type="radio" name="siteDischargeStormWater2" value="4" />&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES Permit No. GAR100002 for Infrastructure construction activities.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3"><strong>IV. ATTACHMENTS. (Check those that apply.)</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="checkbox" name="locationMap" />&nbsp;Location map showing the receiving stream(s), outfall(s) or combination thereof to be monitored.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="checkbox" name="ESPControlPlan" checked="checked" />&nbsp;Erosion, Sedimentation and Pollution Control Plan (if project is greater than 50 acres or if project in areas <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;without local Issuing Authorities regardless of acreage).
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="checkbox" name="writtenAuth" />&nbsp;Written authorization from the appropriate EPD District Office if the Plan disturbs more than 50 acres at any one time for each individual permittee (i.e., <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;primary, secondary or tertiary permittees), or more  than 50 contiguous acres total at any one time (applicable only to General NPDES Permits No. <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GAR100001 and No. GAR100003).

															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="checkbox" name="knownSecondaryPermittees" />&nbsp;List of known secondary permittees.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="checkbox" name="timingSchedule" />&nbsp;Schedule for the timing of the major construction activities.
															</td>
														</tr>				
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3"><strong>III. CERTIFICATIONS. (Owner or Operator or both to initial as applicable.)</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="text" name="certify1" size="5">&nbsp;I certify that the receiving water(s) or the outfall(s) or a combination of receiving water(s) and
																outfall(s) will be monitored in accordance with the Erosion, Sedimentation and Pollution Control Plan.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="text" name="certify2" size="5">&nbsp;I certify that the Erosion, Sedimentation, and Pollution Control Plan (Plan) has been prepared in
																accordance with Part IV of the General NPDES Permit GAR100001, GAR 100002 or GAR 100003, the Plan will
																be implemented, and that such Plan will provide for compliance with this permit.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="text" name="certify3" size="5">&nbsp;I certify under penalty of law that this document and all attachments were prepared under my direction
																or supervision in accordance with a system designed to assure that qualified personnel properly gather and
																evaluate the information submitted. Based upon my inquiry of the person or persons who manage the system, or
																those persons directly responsible for gathering the information, the information submitted is, to the best of my
																knowledge and belief, true, accurate, and complete. I am aware that there are significant penalties for submitting
																false information, including the possibility of fine and imprisonment for knowing violations.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													</table>
													<table>
														<tr>
															<td valign="top"><strong>Owner�s Printed Name:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td><input type="text" name="ownerPrintedName" size="30" value="N/A">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<input type="text" name="ownerTitle" size="30"></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top" align="right"><strong>Date:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td colspan="3"><input type="text" name="ownerSignDate" size="10">&nbsp;<a href="javascript:displayDatePicker('ownerSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top"><strong>Operator�s Printed Name:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td><input type="text" name="operatorPrintedName" size="30">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<input type="text" name="operatorTitle" size="30"></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top" align="right"><strong>Date:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td colspan="3"><input type="text" name="operatorSignDate" size="10">&nbsp;<a href="javascript:displayDatePicker('operatorSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
														</tr>				
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3" align="right">
																<input type="hidden" name="processType" value="addPrimary" />
																<input type="submit" value="Submit Data" class="formButton"/>
															</td>
														</tr>
													</table>
												</td>
												<td><img src="images/pix.gif" width="10" height="1"></td>
											</tr>
										<%end if%>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<%If projectID <> "" then%>
	<script language="JavaScript" type="text/javascript">
	
	  var frmvalidator  = new Validator("noiPrimary");
//	  frmvalidator.addValidation("siteProjectName","req","Please enter the site project name");
//	  frmvalidator.addValidation("degree1","req","Please enter the degrees for latitude");
//	  frmvalidator.addValidation("degree1","lessthan=91");
//	  frmvalidator.addValidation("minute1","req","Please enter the minutes for latitude");
//	  frmvalidator.addValidation("minute1","lessthan=60");
//	  frmvalidator.addValidation("second1","req","Please enter the seconds for latitude");
//	  frmvalidator.addValidation("second1","lessthan=60");
//	  frmvalidator.addValidation("degree2","req","Please enter the degrees for longitude");
//	  frmvalidator.addValidation("degree2","lessthan=91");
//	  frmvalidator.addValidation("minute2","req","Please enter the minutes for longitude");
//	  frmvalidator.addValidation("minute2","lessthan=60");
//	  frmvalidator.addValidation("second2","req","Please enter the seconds for longitude");
//	  frmvalidator.addValidation("second2","lessthan=60");
//	  frmvalidator.addValidation("projectAddress","req","Please enter the street address of the Project");
//	  frmvalidator.addValidation("projectCity","req","Please enter the city where the project is located");
//	  frmvalidator.addValidation("subdivision","req","Please enter the name of the subdivision");
//	  frmvalidator.addValidation("ownerName","req","Please enter the name of the owner");
//	  frmvalidator.addValidation("ownerAddress","req","Please enter the street address of the owner");
//	  frmvalidator.addValidation("ownerCity","req","Please enter the owner's city");
//	  frmvalidator.addValidation("ownerZip","req","Please enter the owner's zip code");
//	  frmvalidator.addValidation("operatorName","req","Please enter the name of the operator");
//	  frmvalidator.addValidation("operatorPhone","req","Please enter the operator's phone number");
//	  frmvalidator.addValidation("operatorAddress","req","Please enter the street address of the operator");
//	  frmvalidator.addValidation("operatorCity","req","Please enter the operator's city");
//	  frmvalidator.addValidation("operatorZip","req","Please enter the operator's zip code");
//	  frmvalidator.addValidation("facilityContact","req","Please enter the facility contact");
//	  frmvalidator.addValidation("facilityContactPhone","req","Please enter the facility contact's phone number");
//	  frmvalidator.addValidation("startDate","req","Please enter the start date");
//	  frmvalidator.addValidation("completionDate","req","Please enter the completion date")  
//	  frmvalidator.addValidation("estimatedDisturbedAcerage","req","Please enter the estimated disturbed Acerage")
//	  frmvalidator.addValidation("estimatedDisturbedAcerage","numeric")  
//	  frmvalidator.addValidation("IRWName","req","Please enter the name of initial receiving water(s)")
//	  frmvalidator.addValidation("MSSSOwnerOperator","req","Please enter the municipal storm sewer system owner/operator")
//	  frmvalidator.addValidation("RWName","req","Please enter the name of receiving water(s)")  
//	  frmvalidator.addValidation("numberOfOutfalls","req","Please enter the number of outfalls")
//	  frmvalidator.addValidation("numberOfOutfalls","numeric")  
///	  frmvalidator.addValidation("SWDrainageArea","req","Please enter the surface water drainage area")
//	  frmvalidator.addValidation("SWDrainageArea","numeric")  
	 // frmvalidator.addValidation("certify1","req","Please enter initials")
	  //frmvalidator.addValidation("certify2","req","Please enter initials")
	 // frmvalidator.addValidation("certify3","req","Please enter initials")
//	  frmvalidator.addValidation("ownerPrintedName","req","Please enter the owner's printed name");
//	  frmvalidator.addValidation("ownerTitle","req","Please enter the owner's title");
//	  frmvalidator.addValidation("ownerSignDate","req","Please enter the date the owner signs the form");
//	  frmvalidator.addValidation("operatorPrintedName","req","Please enter the operator's printed name");
//	  frmvalidator.addValidation("operatorTitle","req","Please enter the operator's title");
	  //frmvalidator.addValidation("operatorSignDate","req","Please enter the date the operator signs the form"); 
	
	</script>
<%
end if
rs.close
rsState.close
set rs = nothing
set rsState = nothing
DataConn.close
set DataConn = nothing
%>