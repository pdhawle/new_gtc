<html>
<head>
<title>On-Demand NOI</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="400" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<tr>
		<td colspan="3" bgcolor="#FA702B">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<%
									dim sTopic
									sTopic = request("topic")
									
									Select Case sTopic
										Case "siteProjectName"%>
											<strong>Site Project Name</strong><br><br>
											What is the exact name of the project or subdivision?
											
										<%Case "GPSLocation"%>
											<strong>GSP Location of Construction Exit</strong><br><br>
											What is the Latitude and Longitude of the Construction Exit?
											
										<%Case "projectAddress"%>
											<strong>Street Address</strong><br><br>
											What is the specific street address of the Site Project? If the site lacks a street address, sufficiently describe the facility location.
											
										<%Case "projectCity"%>
											<strong>City (If applicable)</strong><br><br>
											What city where the project or subdivision is located.  If it is not within a city limits, insert N/A.
										
										<%Case "projectCounty"%>
											<strong>County</strong><br><br>
											What county is the project located in?
										
										<%Case "subdivision"%>
											<strong>Subdivision</strong><br><br>
											What is the exact name of the subdivision?
										
										<%Case "ownerName"%>
											<strong>Owner�s Name</strong><br><br>
											Who is the owner of the subdivision or project?
										
										<%Case "ownerAddress"%>
											<strong>Address</strong><br><br>
											What is the Owner�s address?  (Include the mailing address)
										
										<%Case "ownerCity"%>
											<strong>City</strong><br><br>
											What city is the Owner�s address in?  
											
										<%Case "ownerState"%>
											<strong>State</strong><br><br>
											What State is the Owner�s address in?
											
										<%Case "ownerZip"%>
											<strong>Zip Code</strong><br><br>
											What is the Zip Code?
											
										<%Case "operatorName"%>
											<strong>Operator�s Name</strong><br><br>
											Who is the Operator of the project?
											
										<%Case "operatorPhone"%>
											<strong>Phone </strong><br><br>
											What is the Operator�s telephone number?
											
										<%Case "operatorAddress"%>
											<strong>Address</strong><br><br>
											What is the Operator�s mailing address?
											
										<%Case "operatorCity"%>
											<strong>City</strong><br><br>
											What city is the Operator�s mailing address located?
											
										<%Case "operatorState"%>
											<strong>State</strong><br><br>	
											What State is the Operator�s mailing address located?
										
										<%Case "operatorZip"%>
											<strong>Zip Code</strong><br><br>
											What is the Zip Code of the Operator�s mailing address?
											
										<%Case "facilityContact"%>
											<strong>Facility Contact</strong><br><br>
											Who is the Primary Permittee assigned the responsibility for daily on-site operational control?
											
										<%Case "facilityContactPhone"%>
											<strong>Phone</strong><br><br>
											What number can the Facility Contact be reached?
											
										<%Case "startDate"%>
											<strong>Start Date</strong><br><br>
											What is the date construction will begin?
											
										<%Case "completionDate"%>
											<strong>Completion Date</strong><br><br>
											When will construction be completed?
											
										<%Case "estimatedDisturbedAcerage"%>
											<strong>Estimated Disturbed Acreage</strong><br><br>
											How many acres <u>will be disturbed</u> as a result of the construction activity?  
											
										<%Case "IRWName"%>
											<strong>Name of Initial Receiving Waters</strong><br><br>
											What is the name of the creek, stream, river, etc. where the water will flow when leaving the property?
											
										<%Case "MSSSOwnerOperator"%>
											<strong>Name of Municipal Storm Sewer System Owner/Operator</strong><br><br>
											What is the name of the city or county treatment plant where the storm water discharge will be flowing?
											
										<%Case "RWName"%>
											<strong>Name of Receiving Waters</strong><br><br>
											What is the name of the receiving waters from the city or county treatment plant?
											
										<%Case "samplingOfOutfall"%>
											<strong>Sampling Outfalls/Sampling Receiving Streams/Trout Stream</strong><br><br>
											Where will water samples be taken?
											
										<%Case "numberOfOutfalls"%>
											<strong>Outfalls</strong><br><br>
											What is the number of outfalls on the property?
											
										<%Case "appendixBNTUValue"%>
											<strong>Appendix B NTU Value</strong><br><br>
											What is the NTU limit of the water samples shown on Appendix B of the Permit?  (Be sure the NTU limit is taken from the correct table.)
											
										<%Case "SWDrainageArea"%>
											<strong>Surface Water Drainage Area</strong><br><br>
											What is the total square miles of the surface water drainage area of the project?
											
										
										<%Case else%>
											Sorry. There is no help topic for this item.
									<%End Select
									%>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</body>
</html>