<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

divisionID = request("divisionID")
customerID = request("customerID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDefaultECDevices"
   .CommandType = adCmdStoredProc
   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="includes/jquery.js"></SCRIPT>

<script type="text/javascript">
	$(document).ready(function()
	{
		$("#device_all").click(function()				
		{
			var checked_status = this.checked;
			$("input[@name=device]").each(function()
			{
				this.checked = checked_status;
			});
		});					
	});
	
	</script>
</head>
<body>
<form name="ecDevices" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Import Erosion Control Devices</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="ecDeviceList.asp?id=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">erosion control device list</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="divisionList.asp?ID=<%=customerID%>" class="footerLink">division list</a>	
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<%If rs.eof then%>
											<tr><td colspan="7">there are no records to display</td></tr>
										<%else%>
											<tr><td colspan="3">Select the erosion control devices that you want added to this division</td></tr>
											<tr><td colspan="3" height="10"></td></tr>
											<tr><td colspan="3"><input type="checkbox" id="device_all"><strong>Select All</strong></td></tr>
											<tr><td colspan="3" height="10"></td></tr>
											<%blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><input type="checkbox" name="device" value="<%=rs("deviceID")%>"></td>
													<td>									
														&nbsp;&nbsp;<%=rs("device")%>														
													</td>
													<!--<td align="right"><a href="form.asp?id=<%'=rs("deviceID")%>&formType=editECDevice&divisionID=<%'=divisionID%>&customerID=<%'=customerID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%'=rs("deviceID")%>&processType=deleteECDevice&divisionID=<%'=divisionID%>&customerID=<%'=customerID%>')">&nbsp;</td>-->
												</tr>
											<%
											'get the values from all the checkboxes
											sAllVal = sAllVal & rs("deviceID")
											rs.movenext
											
											if not rs.eof then
												sAllVal = sAllVal & ","
											end if
											
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="divisionID" value="<%=divisionID%>">
												<input type="hidden" name="customerID" value="<%=customerID%>">
												<input type="hidden" name="allBoxes" value="<%=sAllVal%>">
												<input type="hidden" name="processType" value="addDefaultECDevices" />
												<br><input type="submit" value="Add Devices" class="formButton"/>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>