<table class="borderTable" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr bgcolor="#666666">
		<td><span class="searchText">&nbsp;Date</span></td>
		<td align="center"><span class="searchText">Rainfall Amount</span></td>
		<td align="center"><span class="searchText">Petroleum Storage Area <br />Is there any evidence of Spills or leaks?</span></td>
		<td align="center"><span class="searchText">Construction Exits <br />Are any repairs or corrections needed?</span></td>
		<td align="center"><span class="searchText">Inspected By</span></td>
	</tr>
	<%
	'Dim counter
	counter=1
	blnChange = true
	for counter = 1 to 31
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetDailyReport"
			.parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iReportID)
			.parameters.Append .CreateParameter("@dayOfMonth", adInteger, adParamInput, 8, counter)
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rsDaily = oCmd.Execute
		Set oCmd = nothing
	
		If blnChange = true then%>
			<tr class="rowColor">
		<%else%>
			<tr>
		<%end if%>
		
			<%if rsDaily.eof then%>
				<td align="center"><%=counter%></td>
				<td align="center"><input type="text" name="rainfall<%=counter%>" size="3" /></td>
				<td align="center"><select name="question1_<%=counter%>"><option>No</option><option>Yes</option></select></td>
				<td align="center"><select name="question2_<%=counter%>"><option>No</option><option>Yes</option></select></td>
				<td align="center">
					<%
					'get the assigned users for this project
					Set oCmd = Server.CreateObject("ADODB.Command")

					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetAssignedUsers"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("projectID"))
					   .CommandType = adCmdStoredProc
					   
					End With
						
						If Err Then
					%>
							<!--#include file="includes/FatalError.inc"-->
					<%
						End If
						
					Set rsInspector = oCmd.Execute
					Set oCmd = nothing
					%>
					<select name="inspector<%=counter%>">
						<option value="">--inspected by--</option>
						<%'rsInspector.movefirst
						do until rsInspector.eof%>
							<option value="<%=rsInspector("userID")%>"><%=rsInspector("lastName") & ", " & rsInspector("firstName")%></option>
						<%rsInspector.movenext
						loop
						%>
					</select>
				</td>
			<%else%>
			
				<td align="center"><%=counter%></td>
				<td align="center"><input type="text" name="rainfall<%=counter%>" size="3" value="<%=formatnumber(rsDaily("rainfallAmount"),2)%>" /></td>
				<td align="center">
					<select name="question1_<%=counter%>">
						<%if rsDaily("question1Response") = "True" then%>
							<option>No</option>
							<option selected="selected">Yes</option>
						<%else%>
							<option selected="selected">No</option>
							<option>Yes</option>
						<%end if%>
					</select>
				</td>
				<td align="center">
					<select name="question2_<%=counter%>">
						<%if rsDaily("question2Response") = "True" then%>
							<option>No</option>
							<option selected="selected">Yes</option>
						<%else%>
							<option selected="selected">No</option>
							<option>Yes</option>
						<%end if%>
					</select>
				</td>
				<td align="center">
					<%
					'get the assigned users for this project
					Set oCmd = Server.CreateObject("ADODB.Command")

					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetAssignedUsers"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("projectID"))
					   .CommandType = adCmdStoredProc
					   
					End With
						
						If Err Then
					%>
							<!--#include file="includes/FatalError.inc"-->
					<%
						End If
						
					Set rsInspector = oCmd.Execute
					Set oCmd = nothing
					%>
					<select name="inspector<%=counter%>">
						<option value="">--inspected by--</option>
						<%'rsInspector.movefirst
						do until rsInspector.eof
							if rsDaily("inspector") = rsInspector("userID") then%>
								<option selected="selected" value="<%=rsInspector("userID")%>"><%=rsInspector("lastName") & ", " & rsInspector("firstName")%></option>
							<%else%>
								<option value="<%=rsInspector("userID")%>"><%=rsInspector("lastName") & ", " & rsInspector("firstName")%></option>
						<%end if
						rsInspector.movenext
						loop
						%>
					</select>
				</td>
			<%end if%>
	   </tr>
	 
	<%if blnChange = true then
		blnChange = false
	else
		blnChange = true
	end if
	
	next %>
	</table>