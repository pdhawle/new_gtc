<%

	blnMonth = request("vm")
	
	if blnMonth = "true" then
		blnMonth = True
	else
		blnMonth = False
	end if
	
	If request("reportNumber") <> "" then
		iRepNum = request("reportNumber")
	else
		iRepNum = rsReport("reportNumber")
		'if there is no report number in the previous report, get the total number and post here
		'get the report number from the previous report
		
	'	Set oCmd = Server.CreateObject("ADODB.Command")
	
	'	With oCmd
	'	   .ActiveConnection = DataConn
	'	   .CommandText = "spGetLatestReportNumber"
	'	   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, iReportTypeID)
	'	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
	'	   .CommandType = adCmdStoredProc
	'	   
	'	End With
	'		
	'	Set rsReportNumber = oCmd.Execute
	'	Set oCmd = nothing
		
	'	if isnull(rsReportNumber) then
	'		'get the count of the report previously entered
	'		Set oCmd = Server.CreateObject("ADODB.Command")
'
'			With oCmd
'			   .ActiveConnection = DataConn
'			   .CommandText = "spGetECChecklistCount"
'				.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, iProjectID) 'reportID
'			   .CommandType = adCmdStoredProc
'			   
'			End With							
'			Set rsCount = oCmd.Execute
'			Set oCmd = nothing
'			
'			iRepNum = rsCount("reportNumber")
'		else
'			'set the report number to be the last in the DB
'			iRepNum = rsReportNumber("reportNumber")
'		end if
	end if
	
%>
<table>
	<tr>
		<td>
			<%if blnMonth = False then%>
				&nbsp;<a href="form.asp?formType=editReport&reportID=<%=iReportID%>&vm=true">view entire month</a>
			<%else%>
				&nbsp;<a href="form.asp?formType=editReport&reportID=<%=iReportID%>&vm=false">view current day</a>
			<%end if%>
		</td>
	</tr>
</table><br />
<table class="borderTable" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="2"><br />
			&nbsp;<strong>Report Number:</strong>&nbsp;<input type="text" name="reportNumber" size="3" value="<%=iRepNum%>"/>&nbsp;this is the number that will be displayed at the top of the report
		</td>
	</tr>
	<tr bgcolor="#666666">
		<td><span class="searchText">&nbsp;Date</span></td>
		<td align="left"><span class="searchText">Rainfall Amount</span></td>
	</tr>
	<%

	counter=1
	blnChange = true
	for counter = 1 to 31
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetGDOTDailyReport"
			.parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iReportID)
			.parameters.Append .CreateParameter("@dayOfMonth", adInteger, adParamInput, 8, counter)
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rsDaily = oCmd.Execute
		Set oCmd = nothing
	
		If blnChange = true then%>
			<tr class="rowColor">
		<%else%>
			<tr>
		<%end if%>
		
			<%if rsDaily.eof then
				If blnMonth = False then
					If counter = day(date()) then%>
						<td align="left">&nbsp;<%=counter%></td>
						<td align="left"><input type="text" name="rainfall<%=counter%>" size="3" /></td>

					<%end if%>
				<%else%>
					<td align="left">&nbsp;<%=counter%></td>
					<td align="left"><input type="text" name="rainfall<%=counter%>" size="3" /></td>

				<%end if%>
			<%else%>
				
				<%If blnMonth = False then
					if counter = day(date()) then%>
						<td align="left">&nbsp;<%=counter%></td>
						<td align="left"><input type="text" name="rainfall<%=counter%>" size="3" value="<%=formatnumber(rsDaily("rainfallAmount"),2)%>" /></td>
					<%else%>
						<td align="center"></td>
						<td align="left"><input type="hidden" name="rainfall<%=counter%>" size="3" value="<%=formatnumber(rsDaily("rainfallAmount"),2)%>" /></td>

					<%end if%>	
				
				<%else%>
					<td align="left">&nbsp;<%=counter%></td>
					<td align="left"><input type="text" name="rainfall<%=counter%>" size="3" value="<%=formatnumber(rsDaily("rainfallAmount"),2)%>" /></td>
				<%end if%>
			<%end if%>
	   </tr>
	 
	<%if blnChange = true then
		blnChange = false
	else
		blnChange = true
	end if
	
	next %>
	</table>