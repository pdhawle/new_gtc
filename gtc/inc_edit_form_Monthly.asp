
<%
on error resume next

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetMonthlyRainReport"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, iReportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
			
Set rsMonthly = oCmd.Execute
Set oCmd = nothing
%>
<table cellpadding="0" cellspacing="0" border="0">

	<tr>
		<td>
			<strong>Perimeter Control BMP's Installation Date</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="perimControlBMPInstallDate" size="5" value="<%=rsMonthly("perimControlBMPInstallDate")%>" />&nbsp;<a href="javascript:displayDatePicker('perimControlBMPInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<strong>Initial Sediment Storage Requirements Installation Date</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="initSedStorageInstallDate" size="5" value="<%=rsMonthly("initSedStorageInstallDate")%>" />&nbsp;<a href="javascript:displayDatePicker('initSedStorageInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<strong>Sediment Storage/Perimeter Control BMP'sfor T/L "Initial Segment" Installation Date</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="sedStorageBMPInstallDate" size="5" value="<%=rsMonthly("sedStorageBMPInstallDate")%>" />&nbsp;<a href="javascript:displayDatePicker('sedStorageBMPInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
		</td>
	</tr>
	<tr><td height="20"></td></tr>
	
	
	
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="landDisturbanceWeekendHoliday">
				<option></option>
				<option <%=isSelected(rsMonthly("landDisturbanceWeekendHoliday"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("landDisturbanceWeekendHoliday"),"No")%>>No</option>
			</select>
			&nbsp;Did land disturbance occur on a weekend or holiday?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If yes - list dates<br />
			<textarea name="landDisturbanceWeekendHolidayListDates" cols="30" rows="3"><%=rsMonthly("landDisturbanceWeekendHolidayListDates")%></textarea>
		</td>
	</tr>
	<tr><td height="20"></td></tr>


	<tr>
		<td>
			<strong>Site Stabilization Date</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="siteStabilizationDate" size="5" value="<%=rsMonthly("siteStabilizationDate")%>" />&nbsp;<a href="javascript:displayDatePicker('siteStabilizationDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<strong>Daily Inspection Forms</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="tlNumConstructionDays" size="1" value="<%=rsMonthly("tlNumConstructionDays")%>" />&nbsp;# of construction days
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="tlDailyReportsAllDays">
				<option></option>
				<option <%=isSelected(rsMonthly("tlDailyReportsAllDays"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("tlDailyReportsAllDays"),"No")%>>No</option>
				<option <%=isSelected(rsMonthly("tlDailyReportsAllDays"),"N/A")%>>N/A</option>
			</select>
			&nbsp;Daily Reports for all Construction Days?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="complianceGTCPolicy">
				<option></option>
				<option <%=isSelected(rsMonthly("complianceGTCPolicy"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("complianceGTCPolicy"),"No")%>>No</option>
			</select>
			&nbsp;Compliance with GTC policy and procedures for Daily Inspection Forms?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="complianceGTCPolicyWhyNot" cols="30" rows="3"><%=rsMonthly("complianceGTCPolicyWhyNot")%></textarea>
		</td>
	</tr>
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>Stabilization</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="stabilizationAchieved">
				<option></option>
				<option <%=isSelected(rsMonthly("stabilizationAchieved"),"Yes with temporary vegetation")%>>Yes with temporary vegetation</option>
				<option <%=isSelected(rsMonthly("stabilizationAchieved"),"Yes with permanent vegetation")%>>Yes with permanent vegetation</option>
				<option <%=isSelected(rsMonthly("stabilizationAchieved"),"No")%>>No</option>
			</select>
			&nbsp;Is stabilization achieved?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If yes, then provide specifics on stabilization<br />
			<textarea name="explainStabilizationAchieved" cols="30" rows="3"><%=rsMonthly("explainStabilizationAchieved")%></textarea>
		</td>
	</tr>
	
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>BMP Inspection Forms</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="inspectionOccurEvery14Days">
				<option></option>
				<option <%=isSelected(rsMonthly("inspectionOccurEvery14Days"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("inspectionOccurEvery14Days"),"No")%>>No</option>
			</select>
			&nbsp;Did an inspection occur every 14 days during the month?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="inspectionOccurEvery14DaysWhyNot" cols="30" rows="3"><%=rsMonthly("inspectionOccurEvery14DaysWhyNot")%></textarea>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="inspectionOccur24HourRain">
				<option></option>
				<option <%=isSelected(rsMonthly("inspectionOccur24HourRain"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("inspectionOccur24HourRain"),"No")%>>No</option>
			</select>
			&nbsp;Did inspections occur within 24 hours of a "= or >" 0.5 inch rainfall event or the next business day if the rainfall ended during non-business hours?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="inspectionOccur24HourRainWhyNot" cols="30" rows="3"><%=rsMonthly("inspectionOccur24HourRainWhyNot")%></textarea>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="monthlyInspectionOccur">
				<option></option>
				<option <%=isSelected(rsMonthly("monthlyInspectionOccur"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("monthlyInspectionOccur"),"No")%>>No</option>
			</select>
			&nbsp;If stabilization has been achieved, did a monthly inspection occur?
		</td>
	</tr>
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>BMP Installation and Maintenance</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="bmpComplianceGTCPolicy">
				<option></option>
				<option <%=isSelected(rsMonthly("bmpComplianceGTCPolicy"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("bmpComplianceGTCPolicy"),"No")%>>No</option>
			</select>
			&nbsp;Compliance with GTC policy and procedures for BMP Installation and Maintenance?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="bmpComplianceGTCPolicyWhyNot" cols="30" rows="3"><%=rsMonthly("bmpComplianceGTCPolicyWhyNot")%></textarea>
		</td>
	</tr>
	
	
	
	
	
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>Water Quality Monitoring</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<%'bring forth the data from the previous report
			Set oCmd = Server.CreateObject("ADODB.Command")
	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetMonthlyRainData"
				.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, iReportID) 'reportID
			   .CommandType = adCmdStoredProc
			   
			End With
						
			Set rsData = oCmd.Execute
			Set oCmd = nothing
			
			'create arrays for each item
			i = 0
			Dim edarrSamplerID(10)
			'Dim edarrGCSamplerID(10)
			'Dim edarrSamplerType(10)
			Dim edarrDateEndClearingGrubbing(10)
			'Dim edarrDateSamplerInstalled(10)			
			Dim edarrDateSampleTakenAfterClearGrub(10)
			Dim edarrDateEndGrading(10)
			Dim edarrDateAfterEndGradingorNinety(10)
			
			Dim edarrSamplingRequired(10)
			Dim edarrSamplingComplete(10)
			
			'Dim edarrDateSamplerRemoved(10)
			Dim edarrRainDateEvent(10)
			Dim edarrRainAmount(10)
			Dim edarrCodeCol1(10)
			Dim edarrCodeCol2(10)
			Dim edarrCodeCol3(10)
			Dim edarrCodeCol4(10)
			Dim edarrCodeCol5(10)
			Dim edarrCodeCol6(10)
			Dim edarrCodeCol7(10)
			Dim edarrCodeCol8(10)
			Dim edarrCodeCol9(10)
			Dim edarrCodeCol10(10)
			
			do until rsData.eof
				edarrSamplerID(i) = rsData("samplerID")
				'edarrGCSamplerID(i) = rsData("GCSamplerID")
				'response.Write arrGCSamplerID(i)
				'edarrSamplerType(i) = rsData("samplerType")
				edarrDateEndClearingGrubbing(i) = rsData("dateEndClearingGrubbing")
				'edarrDateSamplerInstalled(i) = rsData("dateSamplerInstalled")
				edarrDateSampleTakenAfterClearGrub(i) = rsData("dateSampleTakenAfterClearGrub")
				edarrDateEndGrading(i) = rsData("dateEndGrading")
				edarrDateAfterEndGradingorNinety(i) = rsData("dateAfterEndGradingorNinety")
				
				edarrSamplingRequired(i) = rsData("samplingRequired")
				edarrSamplingComplete(i) = rsData("samplingComplete")
				
				
				'edarrDateSamplerRemoved(i) = rsData("dateSamplerRemoved")
				edarrRainDateEvent(i) = rsData("rainDateEvent")
				edarrRainAmount(i) = rsData("rainAmount")
				edarrCodeCol1(i) = rsData("codeCol1")
				edarrCodeCol2(i) = rsData("codeCol2")
				edarrCodeCol3(i) = rsData("codeCol3")
				edarrCodeCol4(i) = rsData("codeCol4")
				edarrCodeCol5(i) = rsData("codeCol5")
				edarrCodeCol6(i) = rsData("codeCol6")
				edarrCodeCol7(i) = rsData("codeCol7")
				edarrCodeCol8(i) = rsData("codeCol8")
				edarrCodeCol9(i) = rsData("codeCol9")
				edarrCodeCol10(i) = rsData("codeCol10")
			rsData.movenext
			i=i+1
			loop
			
'			%>
			<table width="1200" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right" colspan="2">
						Sampler ID (See CMP for ID)&nbsp;			
					</td>
					<%for i=1 to 10
					
						Set oCmd = Server.CreateObject("ADODB.Command")

						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spGetLocationsByProjectWS"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, rsReport("projectID"))
						   .CommandType = adCmdStoredProc
						   
						End With
									
						Set rsLoc = oCmd.Execute
						Set oCmd = nothing%>
						<td>
							<select class="graySelect" name="samplerID<%=i%>">
								<option></option>
								<%do until rsLoc.eof
									if trim(edarrSamplerID(i - 1)) = trim(rsLoc("location")) then%>
										<option selected="selected" value="<%=handleApostropheDisplay(rsLoc("location"))%>"><%=handleApostropheDisplay(rsLoc("location"))%></option>
									<%else%>
										<option value="<%=handleApostropheDisplay(rsLoc("location"))%>"><%=handleApostropheDisplay(rsLoc("location"))%></option>
								<%end if
								rsLoc.movenext
								loop%>
							</select>
							<!--<input type="text" size="10" class="grayTextBox" name="samplerID<%'=i%>" value="<%'=arrSamplerID(i - 1)%>" />-->
						</td>
					<%next%>
				</tr>
				<!--<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" colspan="2">
						GTC Sampler ID&nbsp;			
					</td>
					<%'for i=1 to 10%>
						<td>
							<input type="text" size="10" class="grayTextBox" name="GCSamplerID<%'=i%>" value="<%'=edarrGCSamplerID(i - 1)%>" />
						</td>
					<%'next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" colspan="2">
						Type (Upstream, Downstream, Outfall)&nbsp;			
					</td>
					<%'for i=1 to 10%>
						<td>
							<select class="graySelect" name="samplerType<%'=i%>">
								<option></option>
								<option <%'=isSelected("Upstream",trim(edarrSamplerType(i - 1)))%>>Upstream</option>
								<option <%'=isSelected("Downstream",trim(edarrSamplerType(i - 1)))%>>Downstream</option>
								<option <%'=isSelected("Outfall",trim(edarrSamplerType(i - 1)))%>>Outfall</option>	
							</select>
						</td>
					<%'next%>
				</tr>-->
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" colspan="2">
						Date end clearing and grubbing&nbsp;			
					</td>
					<%for i=1 to 10%>
						<td width="100">
							<input type="text" size="10" class="grayTextBox" name="dateEndClearingGrubbing<%=i%>" value="<%=edarrDateEndClearingGrubbing(i - 1)%>" />&nbsp;<a href="javascript:displayDatePicker('dateEndClearingGrubbing<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%next%>
				</tr>
				<!--<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" colspan="2">
						Date Sampler Installed&nbsp;			
					</td>
					<%'for i=1 to 10%>
						<td>
							<input type="text" size="10" class="grayTextBox" name="dateSamplerInstalled<%'=i%>" value="<%'=edarrDateSamplerInstalled(i - 1)%>" />&nbsp;<a href="javascript:displayDatePicker('dateSamplerInstalled<%'=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%'next%>
				</tr>-->
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" colspan="2">
						Date sample taken after clearing/grubbing&nbsp;			
					</td>
					<%for i=1 to 10%>
						<td>
							<input type="text" size="10" class="grayTextBox" name="dateSampleTakenAfterClearGrub<%=i%>" value="<%=edarrDateSampleTakenAfterClearGrub(i - 1)%>" />&nbsp;<a href="javascript:displayDatePicker('dateSampleTakenAfterClearGrub<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" colspan="2">
						Date of end of grading&nbsp;			
					</td>
					<%for i=1 to 10%>
						<td>
							<input type="text" size="10" class="grayTextBox" name="dateEndGrading<%=i%>" value="<%=edarrDateEndGrading(i - 1)%>" />&nbsp;<a href="javascript:displayDatePicker('dateEndGrading<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" valign="top" colspan="2">
						Date sample taken after end of grading<br /> or within 90 days of first sampling event&nbsp;			
					</td>
					<%for i=1 to 10%>
						<td>
							<input type="text" size="10" class="grayTextBox" name="dateAfterEndGradingorNinety<%=i%>" value="<%=edarrDateAfterEndGradingorNinety(i - 1)%>" />&nbsp;<a href="javascript:displayDatePicker('dateAfterEndGradingorNinety<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" valign="top">
						Is sampling required for this<br />project per the ES&PC Plans?&nbsp;			
					</td>
					<%
					for i=1 to 10%>
						<td>
							<select class="graySelect" name="samplingRequired<%=i%>">
								<option></option>
								<option <%=isSelected("Yes",trim(edarrSamplingRequired(i - 1)))%>>Yes</option>
								<option <%=isSelected("No",trim(edarrSamplingRequired(i - 1)))%>>No</option>
							</select>
						</td>
					<%ii = ii + 1
					next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" valign="top">
						Sampling Complete?&nbsp;			
					</td>
					<%ii = 0
					for i=1 to 10%>
						<td>
							<select class="graySelect" name="samplingComplete<%=i%>">
								<option></option>
								<option <%=isSelected("Yes",trim(edarrSamplingComplete(i - 1)))%>>Yes</option>
								<option <%=isSelected("No",trim(edarrSamplingComplete(i - 1)))%>>No</option>
							</select>
						</td>
					<%ii = ii + 1
					next%>
				</tr>
				<!--<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" colspan="2">
						Date Sampler Removed & reason&nbsp;			
					</td>
					<%'for i=1 to 10%>
						<td>
							<input type="text" size="10" class="grayTextBox" name="arrDateSamplerRemoved<%'=i%>" value="<%'=edarrDateAfterEndGradingorNinety(i - 1)%>" />&nbsp;<a href="javascript:displayDatePicker('dateSamplerRemoved<%'=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%'next%>
				</tr>-->
				<tr><td height="20" colspan="3"></td></tr>
				
				<tr>
					<td>Date of >= 0.5<br /> inch event</td>
					<td>Amt. Of<br />Rainfall</td>
					<td colspan="10" bgcolor="#000000">&nbsp;</td>
				</tr>
				<%for i=1 to 10%>
				<tr>
					<td><input type="text" size="10" class="grayTextBox" name="rainDateEvent<%=i%>" value="<%=edarrRainDateEvent(i - 1)%>" />&nbsp;<a href="javascript:displayDatePicker('rainDateEvent<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
					<td><input type="text" size="10" class="grayTextBox" name="rainAmount<%=i%>" value="<%=edarrRainAmount(i - 1)%>" /></td>
					<td>
						<select class="blueSelect" name="codeCol1<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol1(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol1(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol1(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol1(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol1(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol1(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol1(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol1(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol1(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol1(i - 1)))%>>NBH</option>
							
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol2<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol2(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol2(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol2(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol2(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol2(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol2(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol2(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol2(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol2(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol2(i - 1)))%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol3<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol3(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol3(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol3(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol3(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol3(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol3(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol3(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol3(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol3(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol3(i - 1)))%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol4<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol4(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol4(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol4(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol4(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol4(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol4(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol4(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol4(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol4(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol4(i - 1)))%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol5<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol5(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol5(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol5(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol5(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol5(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol5(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol5(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol5(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol5(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol5(i - 1)))%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol6<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol6(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol6(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol6(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol6(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol6(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol6(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol6(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol6(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol6(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol6(i - 1)))%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol7<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol7(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol7(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol7(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol7(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol7(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol7(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol7(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol7(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol7(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol7(i - 1)))%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol8<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol8(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol8(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol8(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol8(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol8(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol8(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol8(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol8(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol8(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol8(i - 1)))%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol9<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol9(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol9(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol9(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol9(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol9(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol9(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol9(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol9(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol9(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol9(i - 1)))%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol10<%=i%>">
							<option></option>
							<option <%=isSelected("ST-E",trim(edarrCodeCol10(i - 1)))%>>ST-E</option>
							<option <%=isSelected("ST",trim(edarrCodeCol10(i - 1)))%>>ST</option>
							<option <%=isSelected("N/A",trim(edarrCodeCol10(i - 1)))%>>N/A</option>	
							<option <%=isSelected("Q-NO",trim(edarrCodeCol10(i - 1)))%>>Q-NO</option>
							<option <%=isSelected("Q-ND",trim(edarrCodeCol10(i - 1)))%>>Q-ND</option>
							<option <%=isSelected("Q-DB",trim(edarrCodeCol10(i - 1)))%>>Q-DB</option>
							<option <%=isSelected("Q-MS",trim(edarrCodeCol10(i - 1)))%>>Q-MS</option>
							<option <%=isSelected("Q-SM",trim(edarrCodeCol10(i - 1)))%>>Q-SM</option>
							<option <%=isSelected("Q-IU",trim(edarrCodeCol10(i - 1)))%>>Q-IU</option>
							<option <%=isSelected("NBH",trim(edarrCodeCol10(i - 1)))%>>NBH</option>
						</select>						
					</td>
				</tr>
				<%next%>
				
			</table>
		</td>
	</tr>

	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<select name="samplesCollectedInMonth">
				<option></option>
				<option <%=isSelected(rsMonthly("samplesCollectedInMonth"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("samplesCollectedInMonth"),"No")%>>No</option>
			</select>
			&nbsp;Sample(s) collected during this month?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="exceedNTULimit">
				<option></option>
				<option <%=isSelected(rsMonthly("exceedNTULimit"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("exceedNTULimit"),"No")%>>No</option>
			</select>
			&nbsp;Exceedence of NTU limit
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="waterQualityMonitoringCompliance">
				<option></option>
				<option <%=isSelected(rsMonthly("waterQualityMonitoringCompliance"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("waterQualityMonitoringCompliance"),"No")%>>No</option>
			</select>
			&nbsp;Compliance with GTC policy and procedures for Water Quality Monitoring?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="waterQualityMonitoringComplianceWhyNot" cols="30" rows="3"><%=rsMonthly("waterQualityMonitoringComplianceWhyNot")%></textarea>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="BMPInstalledMaintRetrieved">
				<option></option>
				<option <%=isSelected(rsMonthly("BMPInstalledMaintRetrieved"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("BMPInstalledMaintRetrieved"),"No")%>>No</option>
				<option <%=isSelected(rsMonthly("BMPInstalledMaintRetrieved"),"N/A")%>>N/A</option>
			</select>
			&nbsp;Were BMP's designed, installed and maintained when sample retrieved?
		</td>
	</tr>
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>Impaired Streams</strong>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<select name="projectDrainUpstream">
				<option></option>
				<option <%=isSelected(rsMonthly("projectDrainUpstream"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("projectDrainUpstream"),"No")%>>No</option>
			</select>
			&nbsp;Does this project or a portion of this project drain within one mile upstream of an impaired stream segment?
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			If no - Explain<br />
			<textarea name="projectDrainUpstreamExplain" cols="30" rows="3"><%=rsMonthly("projectDrainUpstreamExplain")%></textarea>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<select name="projectDrainUpstreamYesOutlined">
				<option></option>
				<option <%=isSelected(rsMonthly("projectDrainUpstreamYesOutlined"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("projectDrainUpstreamYesOutlined"),"No")%>>No</option>
			</select>
			&nbsp;If Yes, were the additional BMPs and/or requirements implemented as outlined in the certified ES&PC Plan?
		</td>
	</tr>
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>Daily Rainfall Data</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="hobo">
				<option></option>
				<option <%=isSelected(rsMonthly("hobo"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("hobo"),"No")%>>No</option>
			</select>
			&nbsp;iNet
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<!--<select name="dataTable">
				<option></option>
				<option <%'=isSelected(rsMonthly("dataTable"),"Yes")%>>Yes</option>
				<option <%'=isSelected(rsMonthly("dataTable"),"No")%>>No</option>
			</select>-->
			<strong>Other</strong><br />
			<input type="text" name="dataTable" maxlength="50" value="<%=rsMonthly("dataTable")%>" />
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="rainfallCollectedDaily">
				<option></option>
				<option <%=isSelected(rsMonthly("rainfallCollectedDaily"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("rainfallCollectedDaily"),"No")%>>No</option>
			</select>
			&nbsp;Was rainfall data collected daily?
		</td>
	</tr>
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			1) Enter data in gray areas.<br />
			2) In blue boxes use the following codes.  Can use multiple codes to convey information.<br /><br />
			
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="right">
						<strong>ST-E</strong>&nbsp;
					</td>
					<td>
						Exceedence of NTU limit
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-NO</strong>&nbsp;
					</td>
					<td>
						Qualifying event- no sample due to no outlet 
					</td>
				</tr>
				<tr>
					<td align="right">
						<strong>ST</strong>&nbsp;
					</td>
					<td>
						Sample taken
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-ND</strong>&nbsp;
					</td>
					<td>
						Qualifying event - no discharge  
					</td>
				</tr>
				<tr>
					<td align="right">
						<strong>N/A </strong>&nbsp;
					</td>
					<td>
						Not a qualifying event for sampling
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-DB</strong>&nbsp;
					</td>
					<td>
						Qualifying event- no sample due to dead battery
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;
						<strong>NBH</strong>&nbsp;
					</td>
					<td>
						Non Business Hours / Holidays
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-SM</strong>&nbsp;
					</td>
					<td>
						Qualifying event- no sample due to sampler malfunction
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;
						<strong>Q-MS</strong>&nbsp;
					</td>
					<td>
						Qualifying event, missed sample
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-IU</strong>&nbsp;
					</td>
					<td>
						Qualifying event- no sample, area unsafe/impossible
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>