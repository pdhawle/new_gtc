<script language="javascript">
<!--
function addSample() {
   document.addReport.action = "form.asp?formType=editReport#jumpto";
   addReport.submit(); 
}

//-->
</script>
<%
iDCount = request("count")
if iDCount = "" then
	iDCount = 0
end if

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetWaterSamplingReport"
   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iReportID)
   .CommandType = adCmdStoredProc
   
End With
Set rsWaterSampling = oCmd.Execute
Set oCmd = nothing%>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>
			&nbsp;&nbsp;Number of Samples to add: <input type="text" name="count" size="5" value="<%=iDCount%>" />&nbsp;&nbsp;<input type="button" name="refresh" value="Refresh" class="formButton" onclick="return addSample()"/><br />
			&nbsp;&nbsp;If you need more samples, add to the number displayed.
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="1" height="10"></td>
	</tr>
</table>
<%
iNum = 0
do until rsWaterSampling.eof
iCount = iCount + 1
iNum = iNum + 1%>	
	<table width="100%" class="borderTable" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><span class="required">*</span> <strong>Sample #:</strong>&nbsp;</td>
						<td>
							<input type="hidden" name="waterSamplingReportID<%=iNum%>" value="<%=rsWaterSampling("waterSamplingReportID")%>" />
							<input type="text" name="sampleNumber<%=iNum%>" size="3" value="<%=rsWaterSampling("sampleNumber")%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td></td>
						<td align="right"><span class="required">*</span> <strong>Date Sample Was Taken:</strong>&nbsp;</td>
						<td>
							<input type="text" name="dateSampleTaken<%=iNum%>" maxlength="10" size="10" value="<%=rsWaterSampling("dateSampleTaken")%>"/>&nbsp;<a href="javascript:displayDatePicker('dateSampleTaken<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
					<tr><td></td><td colspan="2"><strong><u>Sampling Point</u></strong></td></tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<table>
								<tr>
									<td align="right">
										<span class="required">*</span> <strong>Exact Location:</strong>&nbsp;
									</td>
									<td>
										<%
										Set oCmd = Server.CreateObject("ADODB.Command")

										With oCmd
										   .ActiveConnection = DataConn
										   .CommandText = "spGetLocationsByProjectWS"
										   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, rsReport("projectID")) 'reportID
										   .CommandType = adCmdStoredProc
										   
										End With
													
										Set rsLoc = oCmd.Execute
										Set oCmd = nothing
										%>
										
										<select name="exactLocation<%=iNum%>">
											<option></option>
										<%do until rsLoc.eof%>
											<option value="<%=handleApostropheDisplay(rsLoc("location"))%>" <%=isSelected(trim(handleApostropheDisplay(rsLoc("location"))),trim(rsWaterSampling("exactLocation")))%>><%=handleApostropheDisplay(rsLoc("location"))%></option>
										<%rsLoc.movenext
										loop%>
										</select>
										
										<!--<input type="text" name="exactLocation<%'=iNum%>" maxlength="150" value="<%'=rsWaterSampling("exactLocation")%>" />-->
									</td>
								</tr>
								<tr>
									<td align="right">
										<strong>Rain Amount:</strong>&nbsp;
									</td>
									<td>
										<input type="text" name="rain<%=iNum%>" maxlength="50" size="10" value="<%=rsWaterSampling("rain")%>" />
									</td>
								</tr>
								<!--<tr>
									<td align="right">
										<span class="required">*</span> <strong>City:</strong>&nbsp;
									</td>
									<td>
										<input type="text" name="city<%'=iNum%>" maxlength="100" value="<%'=rsWaterSampling("city")%>" />
									</td>
								</tr>
								<tr>
									<td align="right">
										<span class="required">*</span> <strong>County:</strong>&nbsp;
									</td>
									<td>
										<input type="text" name="county<%'=iNum%>" maxlength="100" value="<%'=rsWaterSampling("county")%>" />
									</td>
								</tr>-->
							</table>													
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Time Sampled:</strong>&nbsp;</td>
						<td>
							<input type="text" name="timeSampled<%=iNum%>" size="5" maxlength="50" value="<%=rsWaterSampling("timeSampled")%>" />
							 
							&nbsp;<input type="radio" name="sampleMethod<%=iNum%>" <%=isCheckedRadio(trim(rsWaterSampling("isAutomatic")),"0")%> value="0" />
							&nbsp;Automatic &nbsp;<input type="radio" name="sampleMethod<%=iNum%>" <%=isCheckedRadio(trim(rsWaterSampling("isManual")),"1")%> value="1" />&nbsp;Manual (Grab)
							 
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><span class="required">*</span> <strong>Sampled By:</strong>&nbsp;</td>
						<td>
							<input type="text" name="sampledBy<%=iNum%>" maxlength="150" value="<%=rsWaterSampling("sampledBy")%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				</table>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="colorBars"></td>
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr><td></td><td colspan="2"><strong><u>Analysis</u></strong></td></tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Date:</strong>&nbsp;</td>
						<td>
							<input type="text" name="analysisDate<%=iNum%>" maxlength="10" size="10" value="<%=rsWaterSampling("analysisDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('analysisDate<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;
							<strong>Time: </strong>&nbsp;<input type="text" name="analysisTime<%=iNum%>" size="5" maxlength="50" value="<%=rsWaterSampling("analysisTime")%>"/>
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>By:</strong>&nbsp;</td>
						<td>
							<input type="text" name="analysisBy<%=iNum%>" maxlength="150" value="<%=rsWaterSampling("analysisBy")%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Method:</strong>&nbsp;</td>
						<td>
							<input type="text" name="analysisMethod<%=iNum%>" maxlength="150" value="<%=rsWaterSampling("analysisMethod")%>" />
						</td>
					</tr>
					<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Calibration Date:</strong>&nbsp;</td>
						<td>
							<input type="text" name="calibrationDate<%'=iNum%>" maxlength="10" size="10" value="<%'=rsWaterSampling("calibrationDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('calibrationDate<%'=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;
							<strong>Time: </strong>&nbsp;<input type="text" name="calibrationTime<%'=iNum%>" size="5" maxlength="50" value="<%'=rsWaterSampling("calibrationTime")%>" />
						</td>
					</tr>-->
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Results (NTU):</strong>&nbsp;</td>
						<td>
							<input type="text" name="resultsNTU<%=iNum%>" size="5" maxlength="50" value="<%=rsWaterSampling("resultsNTU")%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				</table>
			</td>
		</tr>
	</table><br />	
<%rsWaterSampling.movenext
loop

iDCount = iNum + iDCount
iNum = iNum + 1

'add the new samples here
for iNum = iNum to iDCount%>
	<table width="100%" class="borderTable" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><span class="required">*</span> <strong>Sample #:</strong>&nbsp;</td>
						<td>
							<input type="text" name="sampleNumber<%=iNum%>" size="3" value="<%=request("sampleNumber" & iNum)%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td></td>
						<td align="right"><span class="required">*</span> <strong>Date Sample Was Taken:</strong>&nbsp;</td>
						<td>
							<%if request("dateSampleTaken" & iNum) = "" then
								dSampleTaken = formatdatetime(now(),2)
							else
								dSampleTaken = request("dateSampleTaken" & iNum)
							end if%>
							<input type="text" name="dateSampleTaken<%=iNum%>" maxlength="10" size="10" value="<%=dSampleTaken%>"/>&nbsp;<a href="javascript:displayDatePicker('dateSampleTaken<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
					<tr><td></td><td colspan="2"><strong><u>Sampling Point</u></strong></td></tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<table>
								<tr>
									<td align="right">
										<span class="required">*</span> <strong>Exact Location:</strong>&nbsp;
									</td>
									<td>
										<%
										if iProjectID <> "" then
										'get the location list for this project
										Set oCmd = Server.CreateObject("ADODB.Command")

										With oCmd
										   .ActiveConnection = DataConn
										   .CommandText = "spGetLocationsByProjectWS"
										   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iProjectID) 'reportID
										   .CommandType = adCmdStoredProc
										   
										End With
													
										Set rsLoc = oCmd.Execute
										Set oCmd = nothing%>
										
										<select name="exactLocation<%=iNum%>">
											<option></option>
										<%do until rsLoc.eof%>
											<option value="<%=handleApostropheDisplay(rsLoc("location"))%>"><%=handleApostropheDisplay(rsLoc("location"))%></option>
										<%rsLoc.movenext
										loop%>
										</select>
										<%end if%>
									</td>
								</tr>
								<tr>
									<td align="right">
										<strong>Rain Amount:</strong>&nbsp;
									</td>
									<td>
										<input type="text" name="rain<%=iNum%>" maxlength="50" size="10" value="0.0" />
									</td>
								</tr>
							</table>													
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Time Sampled:</strong>&nbsp;</td>
						<td>
							<input type="text" name="timeSampled<%=iNum%>" size="5" maxlength="50" value="<%=request("timeSampled" & iNum)%>" /> 
							
							&nbsp;<input type="radio" name="sampleMethod<%=iNum%>" value="0" />&nbsp;Automatic 
							&nbsp;<input type="radio" name="sampleMethod<%=iNum%>" value="1" />&nbsp;Manual (Grab) 
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><span class="required">*</span> <strong>Sampled By:</strong>&nbsp;</td>
						<td>
							<input type="text" name="sampledBy<%=iNum%>" maxlength="150" value="<%=request("sampledBy" & iNum)%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				</table>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="colorBars"></td>
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr><td></td><td colspan="2"><strong><u>Analysis</u></strong></td></tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Date:</strong>&nbsp;</td>
						<td>
							<input type="text" name="analysisDate<%=iNum%>" maxlength="10" size="10" value="<%=request("analysisDate" & iNum)%>"/>&nbsp;<a href="javascript:displayDatePicker('analysisDate<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;
							<strong>Time: </strong>&nbsp;<input type="text" name="analysisTime<%=iNum%>" size="5" maxlength="50" value="<%=request("analysisTime" & iNum)%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>By:</strong>&nbsp;</td>
						<td>
							<input type="text" name="analysisBy<%=iNum%>" maxlength="150" value="<%=request("analysisBy" & iNum)%>"/>
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Method:</strong>&nbsp;</td>
						<td>
							<input type="text" name="analysisMethod<%=iNum%>" maxlength="150" value="<%=request("analysisMethod" & iNum)%>" />
						</td>
					</tr>
					<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Calibration Date:</strong>&nbsp;</td>
						<td>
							<input type="text" name="calibrationDate<%'=iNum%>" maxlength="10" size="10" value="<%'=request("calibrationDate" & iNum)%>"/>&nbsp;<a href="javascript:displayDatePicker('calibrationDate<%'=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;
							<strong>Time: </strong>&nbsp;<input type="text" name="calibrationTime<%'=iNum%>" size="5" maxlength="50" value="<%'=request("calibrationTime" & iNum)%>" />
						</td>
					</tr>-->
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Results (NTU):</strong>&nbsp;</td>
						<td>
							<input type="text" name="resultsNTU<%=iNum%>" size="5"  maxlength="50" value="<%=request("resultsNTU" & iNum)%>"/>
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				</table>
			</td>
		</tr>
	</table><br />
<%next%>
<div align="right">
<input type="checkbox" name="isBillable" <%=isChecked(rsReport("isBillable"))%> />&nbsp;Billable
</div>
<input type="hidden" name="dataCount" value="<%=iDCount%>" />