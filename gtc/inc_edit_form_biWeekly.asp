<%
'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for customer list
Set oCmd = Server.CreateObject("ADODB.Command")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetBiWeeklyReport"
   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iReportID)
   .CommandType = adCmdStoredProc
   
End With
Set rsBiWeekly = oCmd.Execute
Set oCmd = nothing%>	
<table width="100%" class="borderTable" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="5" height="0"></td>
					<td align="right"><span class="required">*</span> <strong>District Office:</strong>&nbsp;</td>
					<td>
						<input type="hidden" name="biWeeklyReportID" value="<%=rsBiWeekly("biWeeklyReportID")%>" />
						<input type="text" name="districtOffice" size="40" value="<%=rsBiWeekly("districtOffice")%>" />
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="5" height="0"></td>
					<td align="right"><span class="required">*</span> <strong>EPD Division:</strong>&nbsp;</td>
					<td>
						<input type="text" name="EPDDivision" size="40" value="<%=rsBiWeekly("EPDDivision")%>" />
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="5" height="0"></td>
					<td align="right"><span class="required">*</span> <strong>Address:</strong>&nbsp;</td>
					<td>
						<input type="text" name="address" size="30" value="<%=rsBiWeekly("address")%>" />
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="5" height="0"></td>
					<td align="right"><span class="required">*</span> <strong>City:</strong>&nbsp;</td>
					<td>
						<input type="text" name="city" size="30" value="<%=rsBiWeekly("city")%>" />&nbsp;&nbsp;
						<span class="required">*</span> <strong>State</strong>&nbsp;<select name="state">
											<%do until rsState.eof
												If rsState("stateID") = rsBiWeekly("state") then%>
													<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
												<%else%>
													<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
											<%end if
											rsState.movenext
											loop
											rsState.moveFirst%>
										</select>&nbsp;&nbsp;
						<span class="required">*</span> <strong>Zip</strong>&nbsp;<input type="text" name="zip" size="8" value="<%=rsBiWeekly("zip")%>" />						
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
			</table>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="colorBars" colspan="3"></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="5" height="0"></td>
					<td align="right"><span class="required">*</span> <strong>Report Covering:</strong>&nbsp;</td>
					<td>
						<input type="text" name="reportCovering" size="30" value="<%=rsBiWeekly("reportCovering")%>" /> (Month/Year)
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="5" height="0"></td>
					<td align="right"><strong>Project:</strong>&nbsp;</td>
					<td>
						<input type="text" name="projectName" size="30" value="<%=rsBiWeekly("projectName")%>" disabled="disabled" />
						<!--<input type="hidden" name="projectID"  value="<%'=rsProjectInfo("projectID")%>"/>-->
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="5" height="0"></td>
					<td align="right"><span class="required">*</span> <strong>County:</strong>&nbsp;</td>
					<td>
						<input type="text" name="county" size="30"   value="<%=rsBiWeekly("county")%>"/>
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
			</table>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="colorBars" colspan="3"></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td colspan="3">
						&nbsp;&nbsp;<strong>An assessment has been made of the following:</strong>
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				<tr>
					<td colspan="3">
						<table>
							<tr>
								<td valign="top" colspan="2">
									�&nbsp;<strong>Streams, stream buffer areas, wetlands?</strong>
								</td>
							</tr>
							<tr>
								<td valign="top">
									&nbsp;&nbsp;&nbsp;<span class="required">*</span> <strong>Observation:</strong>
								</td>
								<td align="right">
									<textarea name="observation1" rows="2" cols="30"><%=rsBiWeekly("observation1")%></textarea>
								</td>
							</tr>
							<tr>
								<td valign="top">
									&nbsp;&nbsp;&nbsp;<strong>Action Taken:</strong>
								</td>
								<td align="right">
									<textarea name="actionTaken1" rows="2" cols="30"><%=rsBiWeekly("actionTaken1")%></textarea>
								</td>
							</tr>
							<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
							
							<tr>
								<td valign="top" colspan="2">
									�&nbsp;<strong>Are erosion control measures effective in preventing <br />&nbsp;&nbsp;&nbsp;significant impacts to receiving water(s)?</strong>
								</td>
							</tr>
							<tr>
								<td valign="top">
									&nbsp;&nbsp;&nbsp;<span class="required">*</span> <strong>Observation:</strong>
								</td>
								<td align="right">
									<textarea name="observation2" rows="2" cols="30"><%=rsBiWeekly("observation2")%></textarea>
								</td>
							</tr>
							<tr>
								<td valign="top">
									&nbsp;&nbsp;&nbsp;<strong>Action Taken:</strong>
								</td>
								<td align="right">
									<textarea name="actionTaken2" rows="2" cols="30"><%=rsBiWeekly("actionTaken2")%></textarea>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
			</table>
		</td>
	</tr>
</table>