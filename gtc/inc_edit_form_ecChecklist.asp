<script language="javascript">
<!--
function addLines() {
   document.addReport.action = "form.asp?formType=editReport#jumpto";
   addReport.submit(); 
}

//-->
</script>
<%
iDCount = request("count")
if iDCount = "" then
	iDCount = 10
end if


'get the ECChecklist
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetECChecklist"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iReportID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsECChecklist = oCmd.Execute
Set oCmd = nothing
%>
<table class="borderTable" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td><br />
			&nbsp;&nbsp;Number of rows to add: <input type="text" name="count" size="5" value="<%=iDCount%>" />&nbsp;&nbsp;<input type="button" name="refresh" value="Refresh" class="formButton" onclick="return addLines()"/><br />
			&nbsp;&nbsp;If you need more lines, add to the number displayed.
		</td>
	</tr>
	<tr><td><img src="images/pix.gif" width="1" height="20"></td>
	<tr>
		<td>
		<input type="checkbox" name="periodWeekly" <%=requestIsChecked(request("periodWeekly"))%> />&nbsp;Weekly - Period ending Sunday</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="periodHour" <%=requestIsChecked(request("periodHour"))%> />&nbsp;Hr-Significant Rainfall Event</td>
	</tr>
	<tr><td><img src="images/pix.gif" width="1" height="20"></td>	
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" width="100%">
					<a name="jumpto"></a><tr bgcolor="#666666">
						<td><span class="searchText">&nbsp;Control Device</span></td>
						<td align="left"><span class="searchText">Location</span></td>
						<td align="left"><span class="searchText">Date of install</span></td>
						<td align="left"><span class="searchText">% Filled</span></td>
						<td align="left"><span class="searchText">Maintenance&nbsp;&nbsp;<br />Required</span></td>
						<td align="left"><span class="searchText">Comments <br />WECS/DOT Engineer</span></td>
						<td align="left"><span class="searchText">Date Fixed</span></td>
					</tr>
					<tr><td colspan="7"><img src="images/pix.gif" width="1" height="10"></td>
				<%blnChange = true
				iCount = 0
				iNum = 0
				do until rsECChecklist.eof
				iCount = iCount + 1
				iNum = iNum + 1
				'for iNum = 1 to iDCount
					'get the devices from database based on division
					Set oCmd = Server.CreateObject("ADODB.Command")
		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetECDevicesByDivision"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("divisionID"))
					   .CommandType = adCmdStoredProc
					   
					End With
						
						If Err Then
					%>
							<!--#include file="includes/FatalError.inc"-->
					<%
						End If
						
					Set rsECDevice = oCmd.Execute
					Set oCmd = nothing
				
					If blnChange = true then%>
						<tr class="rowColor">
					<%else%>
						<tr>
					<%end if%>
						<td>&nbsp;
							<select name="device<%=iNum%>">
								<option value="">--select--</option>
								<%do until rsECDevice.eof
									if trim(rsECChecklist("deviceID")) = trim(rsECDevice("deviceID")) then%>
										<option selected="selected" value="<%=rsECDevice("deviceID")%>"><%=rsECDevice("device")%></option>
									<%else%>
										<option value="<%=rsECDevice("deviceID")%>"><%=rsECDevice("device")%></option>
								<%end if
								rsECDevice.movenext
								loop%>
							</select>
						</td>
						<td>
							<input type="text" name="location<%=iNum%>" size="15" value="<%=rsECChecklist("location")%>"/>
						</td>
						<td>
							<input type="text" name="installDate<%=iNum%>" size="6" maxlength="10" value="<%=rsECChecklist("installDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('installDate<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
						<td>
							<input type="text" name="percentFilled<%=iNum%>" size="2" value="<%=rsECChecklist("percentFilled")%>"/>&nbsp;&nbsp;
						</td>
						<td>
							<select name="maintenanceRequired<%=iNum%>">
								<%if trim(rsECChecklist("maintenanceRequired")) = "No" then%>
									<option selected="selected" value="No">No</option>
								<%else%>
									<option value="No">No</option>
								<%end if%>
								
								<%if trim(rsECChecklist("maintenanceRequired")) = "Yes" then%>
									<option selected="selected" value="Yes">Yes</option>
								<%else%>
									<option value="Yes">Yes</option>
								<%end if%>
								
							</select>
						</td>
						<td>
							<input type="text" name="comments<%=iNum%>" size="15" value="<%=rsECChecklist("comments")%>"/>
						</td>
						<td>
							<input type="text" name="dateFixed<%=iNum%>" size="6" maxlength="10" value="<%=rsECChecklist("dateFixed")%>"/>&nbsp;<a href="javascript:displayDatePicker('dateFixed<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					</tr>
					<tr><td colspan="7"><img src="images/pix.gif" width="1" height="5"></td>
				<%
				if blnChange = true then
					blnChange = false
				else
					blnChange = true
				end if
				rsECChecklist.movenext
				loop
				'next
				
				iDCount = iNum + iDCount
				iNum = iNum + 1
				
				'add the new lines here
				for iNum = iNum to iDCount
					'get the devices from database based on division
					Set oCmd = Server.CreateObject("ADODB.Command")
		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetECDevicesByDivision"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("divisionID"))
					   .CommandType = adCmdStoredProc
					   
					End With
						
						If Err Then
					%>
							<!--#include file="includes/FatalError.inc"-->
					<%
						End If
						
					Set rsECDevice = oCmd.Execute
					Set oCmd = nothing
				
					If blnChange = true then%>
						<tr class="rowColor">
					<%else%>
						<tr>
					<%end if%>
						<td>&nbsp;
							<select name="device<%=iNum%>">
								<option value="">--select--</option>
								<%do until rsECDevice.eof
									if trim(request("device" & iNum)) = trim(rsECDevice("deviceID")) then%>
										<option selected="selected" value="<%=rsECDevice("deviceID")%>"><%=rsECDevice("device")%></option>
									<%else%>
										<option value="<%=rsECDevice("deviceID")%>"><%=rsECDevice("device")%></option>
								<%end if
								rsECDevice.movenext
								loop%>
							</select>
						</td>
						<td>
							<input type="text" name="location<%=iNum%>" size="15" value="<%=request("location" & iNum)%>"/>
						</td>
						<td>
							<input type="text" name="installDate<%=iNum%>" size="6" maxlength="10" value="<%=request("installDate" & iNum)%>"/>&nbsp;<a href="javascript:displayDatePicker('installDate<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
						<td>
							<input type="text" name="percentFilled<%=iNum%>" size="2" value="<%=request("percentFilled" & iNum)%>"/>&nbsp;&nbsp;
						</td>
						<td>
							<select name="maintenanceRequired<%=iNum%>">
								<%if trim(request("maintenanceRequired" & iNum)) = "No" then%>
									<option selected="selected" value="No">No</option>
								<%else%>
									<option value="No">No</option>
								<%end if%>
								
								<%if trim(request("maintenanceRequired" & iNum)) = "Yes" then%>
									<option selected="selected" value="Yes">Yes</option>
								<%else%>
									<option value="Yes">Yes</option>
								<%end if%>
								
							</select>
						</td>
						<td>
							<input type="text" name="comments<%=iNum%>" size="15" value="<%=request("comments" & iNum)%>"/>
						</td>
						<td>
							<input type="text" name="dateFixed<%=iNum%>" size="6" maxlength="10" value="<%=request("dateFixed" & iNum)%>"/>&nbsp;<a href="javascript:displayDatePicker('dateFixed<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					</tr>
					<tr><td colspan="7"><img src="images/pix.gif" width="1" height="5"></td>
				<%
				if blnChange = true then
					blnChange = false
				else
					blnChange = true
				end if
				next%>
				<input type="hidden" name="dataCount" value="<%=iNum - 1%>" />
			</table>
		</td>
	</tr>
</table>	
