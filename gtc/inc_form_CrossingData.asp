<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>
		
			<%
			crossingID = request("crossingID")
			
			
			'response.Write iLen & "<br>"
			'response.Write iFindSign & "<br>"
			
			
			Set oCmd = Server.CreateObject("ADODB.Command")
	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetLocationsByProject"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
			   .CommandType = adCmdStoredProc
			   
			End With
						
			Set rsLoc = oCmd.Execute
			Set oCmd = nothing
			%>
		
			<strong>Crossing ID</strong><br />
			
			<!--<script type="text/javascript" src="jquery/jquery.ui.js"></script>
			<script type="text/javascript" src="jquery/jquery.asmselect.js"></script>
			<script type="text/javascript">
			//	$(document).ready(function() {
			//		$("select[multiple]").asmSelect({
			//			addItemTarget: 'bottom',
			//			animate: true,
			//			highlight: true,
			//			sortable: true
			//		});
					
			//	}); 
		
			</script>
			<link rel="stylesheet" type="text/css" href="jquery/jquery.asmselect.css" />-->
			<select name="crossingID" onChange="return loc_onchange(addReport,reportType.getElementsByTagName('option')[reportType.selectedIndex].value,division.getElementsByTagName('option')[division.selectedIndex].value,project.getElementsByTagName('option')[project.selectedIndex].value,this.getElementsByTagName('option')[this.selectedIndex].value)">
				<option></option>
				<%do until rsLoc.eof%>					
					<option <%=isSelected(trim(request("crossingID")),trim(rsLoc("location") & "$" & rsLoc("locationID")))%> value="<%=rsLoc("location")%>$<%=rsLoc("locationID")%>"><%=rsLoc("location")%></option>
				<%rsLoc.movenext
				loop%>
			</select><br /><br />
			
			<!--<input type="text" name="crossingID" size="5" /><br />-->
	<%if crossingID <> "" then
			'response.Write crossingID & "<br>"
			
			iLen = len(crossingID)
			iFindSign = InStr(crossingID,"$")
			crossingID = right(crossingID,iLen - iFindSign)
			
			'get the date of install, if applicable
			Set oCmd = Server.CreateObject("ADODB.Command")	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetProjectLocation"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, crossingID)
			   .CommandType = adCmdStoredProc   
			End With
						
			Set rsLocation = oCmd.Execute
			Set oCmd = nothing
	
			%>		
	
			<strong>Install Date</strong><br />
			<input type="text" name="dateInstalled" maxlength="10" size="10" value="<%=rsLocation("dateInstalled")%>"/>&nbsp;<a href="javascript:displayDatePicker('dateInstalled')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a><br /><br />
	
			<strong><u>Crossing Data</u></strong><br /><br />
			
			<strong>Type of Crossing</strong><br />
			<select name="typeOfCrossing">
				<option></option>
				<option>Pipe</option>
				<option>At grade rock crossing</option>
				<option>Other</option>
			</select><br /><br />
			
			<strong>If Other Type of Crossing</strong><br />
			<input type="text" name="otherTypeOfCrossing" maxlength="50" /><br /><br />
			
			<strong>Length of Crossing (ft.)</strong>&nbsp;<span class="required">(rock crossings only)</span><br />
			<input type="text" name="lengthOfCrossing" size="5" /><br /><br />
			
			<strong>Width of Crossing (ft.)</strong><br />
			<input type="text" name="widthOfCrossing" size="5" /><br /><br />
			
			<strong>Pipe length (ft.)</strong><br />
			<input type="text" name="pipeLength" size="5" /><br /><br />
			
			<strong>Outlet Protection Length (ft.)</strong><br />
			<input type="text" name="outletProtectionLength" size="5" /><br /><br />
			
			<strong>Outlet Protection at Grade</strong><br />
			<select name="outletProtectionAboveGrade">
				<option></option>
				<option>Yes</option>
				<option>No</option>
				<!--<option>N/A</option>-->
			</select><br /><br />
			
			<strong>Inlet Protection Length (ft.)</strong><br />
			<input type="text" name="inletProtectionLength" size="5" /><br /><br />
			
			<strong>Inlet Protection at Grade</strong><br />
			<select name="inletProtectionAboveGrade">
				<option></option>
				<option>Yes</option>
				<option>No</option>
				<!--<option>N/A</option>-->
			</select><br /><br />
			
			<strong>Pipe Embedded</strong><br />
			<select name="pipeEmbedded">
				<option></option>
				<option>Yes</option>
				<option>No</option>
				<!--<option>N/A</option>-->
			</select><br /><br />
			
			<strong>Rock Crossing At-Grade?</strong><br />
			<select name="rockCrossingAtGrade">
				<option></option>
				<option>Yes</option>
				<option>No</option>
				<!--<option>N/A</option>-->
			</select><br /><br />
			
			<input type="hidden" name="rockCrossingDepthAboveGrade" value="" />
			<!--<strong>Rock Crossing Depth above Grade (in.)</strong><br />
			<input type="text" name="rockCrossingDepthAboveGrade" size="5" /><br /><br />-->
			
			<strong>Geotextile Underliner?</strong><br />
			<select name="geotextileUnderliner">
				<option></option>
				<option>Yes</option>
				<option>No</option>
				<option>N/A</option>
			</select><br /><br />
			
			<strong>Rock Washed Downstream?</strong><br />
			<select name="rockWashedDownstream">
				<option></option>
				<option>Yes</option>
				<option>No</option>
				<option>N/A</option>
			</select><br /><br />
			
			<strong>Geoweb?</strong><br />
			<select name="geoweb">
				<option></option>
				<option>Yes</option>
				<option>No</option>
				<option>N/A</option>
			</select><br /><br />
			
			<strong>Other Issues/ Comments</strong><br />
			<textarea name="issuesComments" cols="30" rows="4"></textarea><br /><br />
			
	<%end if%>
		</td>
	</tr>
</table>
