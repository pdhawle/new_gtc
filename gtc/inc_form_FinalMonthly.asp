<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="2">
			<strong>Month:</strong>&nbsp;
			<select name="month">
				<option value="1" <%=isSelected(month(DateAdd("m",-1,now())),1)%>>January</option>
				<option value="2" <%=isSelected(month(DateAdd("m",-1,now())),2)%>>February</option>
				<option value="3" <%=isSelected(month(DateAdd("m",-1,now())),3)%>>March</option>
				<option value="4" <%=isSelected(month(DateAdd("m",-1,now())),4)%>>April</option>
				<option value="5" <%=isSelected(month(DateAdd("m",-1,now())),5)%>>May</option>
				<option value="6" <%=isSelected(month(DateAdd("m",-1,now())),6)%>>June</option>
				<option value="7" <%=isSelected(month(DateAdd("m",-1,now())),7)%>>July</option>
				<option value="8" <%=isSelected(month(DateAdd("m",-1,now())),8)%>>August</option>
				<option value="9" <%=isSelected(month(DateAdd("m",-1,now())),9)%>>September</option>
				<option value="10" <%=isSelected(month(DateAdd("m",-1,now())),10)%>>October</option>
				<option value="11" <%=isSelected(month(DateAdd("m",-1,now())),11)%>>November</option>
				<option value="12" <%=isSelected(month(DateAdd("m",-1,now())),12)%>>December</option>
				
			</select>&nbsp;
			<select name="year">
				<option value="<%=year(now()) - 1%>"><%=year(now()) - 1%></option>
				<option value="<%=year(now())%>" selected="selected"><%=year(now())%></option>
				<option value="<%=year(now()) + 1%>"><%=year(now()) + 1%></option>
			</select>
		</td>
	</tr>

	<tr><td height="20"></td></tr>
	
	<tr>
		<td colspan="2"><strong>Stormwater Data</strong></td>
	</tr>
	<tr>
		<td colspan="2">
			<table>
				<tr>
					<td>
						<input type="radio" name="stormwaterData" value="1" />&nbsp;Enclosed is GTC�s Monthly NPDES Stormwater Monitoring Report. <br />
						<input type="radio" name="stormwaterData" value="2" checked="checked" />&nbsp;There were no stormwater samples retrieved at this project site for this reporting month.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr><td height="10"></td></tr>
	
	<tr>
		<td colspan="2"><strong>Potential Violations</strong> <em>(Efforts are continuously being made to prevent, correct, and reduce the likelihood of potential permit violations.)</em></td>
	</tr>
	<tr>
		<td colspan="2">
			<table>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0">
							<tr>
								<td colspan="2"><input type="checkbox" name="potentialViolationsPermit"/>&nbsp;Following is a list of what may be considered permit violations for the project. </td>
							</tr>
							<tr>
								<td width="25">&nbsp;</td>
								<td>
									<script type="text/javascript" src="jquery/jquery.js"></script>
									<script type="text/javascript" src="jquery/jquery.ui.js"></script>
									<script type="text/javascript" src="jquery/jquery.asmselect.js"></script>
									<script type="text/javascript">
										$(document).ready(function() {
											$("select[multiple]").asmSelect({
												addItemTarget: 'bottom',
												animate: true,
												highlight: true,
												sortable: true
											});
											
										}); 
								
									</script>
									<link rel="stylesheet" type="text/css" href="jquery/jquery.asmselect.css" />
									<select id="violationList" multiple="multiple" name="violationList" title="select all that apply">
										<option value="Failure to conduct daily inspection.">Failure to conduct daily inspection.</option>
										<option value="Failure to conduct BMP inspection every 14 days.">Failure to conduct BMP inspection every 14 days.</option>
										<option value="Failure to collect daily rainfall data.">Failure to collect daily rainfall data.</option>
										<option value="Failure to conduct BMP inspection within 24 hours of a 0.5 inch rainfall event.">Failure to conduct BMP inspection within 24 hours of a 0.5 inch rainfall event.</option>
										<option value="Exceedence of NTU limit.">Exceedence of NTU limit.</option>
										<option value="Failure to collect stormwater sample.">Failure to collect stormwater sample.</option>
										<option value="Other">Other</option>
									</select><br />
									
									If other:<br />
									<input type="text" name="violationOther" size="50" maxlength="100" /><br /><br />
								</td>
							</tr>
						</table>

						
						<input type="checkbox" name="potentialViolationsInfo"/>&nbsp;Based on the information available to us, we believe that the project site is in compliance with the General Permit.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr><td height="10"></td></tr>
	
	<tr>
		<td colspan="2">
			<input type="checkbox" name="finalStabilization" />&nbsp;Final Stabilization has been acheived
		</td>
	</tr>
	
</table>
