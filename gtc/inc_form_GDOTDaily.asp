<%
	'dim blnMonth
	
	blnMonth = request("vm")
	
	if blnMonth = "true" then
		blnMonth = True
	else
		blnMonth = False
	end if
	
	If request("reportNumber") <> "" then
		iRepNum = request("reportNumber")
	else
		'if there is no report number in the previous report, get the total number and post here
		'get the report number from the previous report
		
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLatestReportNumber"
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, iReportTypeID)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
		   .CommandType = adCmdStoredProc
		   
		End With
			
		Set rsReportNumber = oCmd.Execute
		Set oCmd = nothing
		
		if isnull(rsReportNumber) then
			'get the count of the report previously entered
			Set oCmd = Server.CreateObject("ADODB.Command")

			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetECChecklistCount"
				.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, iProjectID) 'reportID
			   .CommandType = adCmdStoredProc
			   
			End With							
			Set rsCount = oCmd.Execute
			Set oCmd = nothing
			
			iRepNum = rsCount("reportNumber")
		else
			'set the report number to be the last in the DB
			iRepNum = rsReportNumber("reportNumber")
		end if
	end if

	
%>
<table>
	<tr>
		<td>
			<%if blnMonth = False then%>
				&nbsp;<a href="form.asp?formType=addReport&division=<%=iDivisionID%>&project=<%=iProjectID%>&reportType=<%=iReportTypeID%>&vm=true&reportNumber=<%=iRepNum%>">view entire month</a>
			<%else%>
				&nbsp;<a href="form.asp?formType=addReport&division=<%=iDivisionID%>&project=<%=iProjectID%>&reportType=<%=iReportTypeID%>&vm=false&reportNumber=<%=iRepNum%>">view current day</a>
			<%end if%>
		</td>
	</tr>
</table><br />
<table class="borderTable" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="2"><br />
			&nbsp;<strong>Report Number:</strong>&nbsp;<input type="text" name="reportNumber" size="3" value="<%=iRepNum%>"/>&nbsp;this is the number that will be displayed at the top of the report
		</td>
	</tr>
	<tr bgcolor="#666666">
		<td><span class="searchText">&nbsp;Date</span></td>
		<td align="left"><span class="searchText">Rainfall Amount</span></td>
		<!--<td align="center"><span class="searchText">Petroleum Storage Area <br />Is there any evidence of Spills or leaks?</span></td>
		<td align="center"><span class="searchText">Construction Exits <br />Are any repairs or corrections needed?</span></td>
		<td align="center"><span class="searchText">Compliance <br />Is this project in compliance?</span></td>
		<td align="center"><span class="searchText">Inspected By</span></td>-->
	</tr>
	<%
	'Dim counter
	counter=1
	blnChange = true
	'if the user is not administrator, then only show today
	If blnMonth = False then
	'put only today's line item in and just put the person's name%>
		<tr class="rowColor">
			<td align="left">&nbsp;<%=day(date())%></td>
				<td align="left"><input type="text" name="rainfall<%=day(date())%>" size="3" /></td>
				<!--<td align="center"><select name="question1_<%'=day(date())%>"><option>No</option><option>Yes</option></select></td>
				<td align="center"><select name="question2_<%'=day(date())%>"><option>No</option><option>Yes</option></select></td>
				<td align="center"><select name="compliant_<%'=day(date())%>"><option>Yes</option><option>No</option></select></td>
				<td align="center"><%'=session("name")%><input type="hidden" name="inspector<%'=day(date())%>" value="<%'=session("ID")%>" /></td>-->
		</tr>
	<%else
		for counter = 1 to 31
			If blnChange = true then%>
				<tr class="rowColor">
			<%else%>
				<tr>
			<%end if%>
				<td align="left">&nbsp;<%=counter%></td>
				<td align="left"><input type="text" name="rainfall<%=counter%>" size="3" /></td>
				<!--<td align="center"><select name="question1_<%'=counter%>"><option>No</option><option>Yes</option></select></td>
				<td align="center"><select name="question2_<%'=counter%>"><option>No</option><option>Yes</option></select></td>
				<td align="center"><select name="compliant_<%'=counter%>"><option>Yes</option><option>No</option></select></td>-->
				<!--<td align="center">
					<%
					'get the assigned users for this project
				'	Set oCmd = Server.CreateObject("ADODB.Command")
	
				'	With oCmd
				'	   .ActiveConnection = DataConn
				'	   .CommandText = "spGetAssignedUsers"
				'	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
				'	   .CommandType = adCmdStoredProc
				'	   
				'	End With
						
						
				'	Set rsInspector = oCmd.Execute
				'	Set oCmd = nothing
					%>
					<select name="inspector<%'=counter%>">
						<option value="">--inspected by--</option>
						<%'rsInspector.movefirst
						'do until rsInspector.eof%>
							<option value="<%'=rsInspector("userID")%>"><%'=rsInspector("lastName") & ", " & rsInspector("firstName")%></option>
						<%'rsInspector.movenext
						'loop
						%>
					</select>
				</td>-->
		   </tr>
		<%if blnChange = true then
			blnChange = false
		else
			blnChange = true
		end if
		
		next
	end if%>
</table>	