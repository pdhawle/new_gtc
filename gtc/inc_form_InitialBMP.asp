<table cellpadding="0" cellspacing="0" border="0">
	
	<tr>
		<td>
			<strong>Inspection of:</strong><br /><br />
			<!--<select name="siteInspected">
				<option></option>
				<option value="have">has</option>
				<option value="have not">has not</option>
			</select><br /><br />-->
			
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="perimControlBMP" />&nbsp;Perimeter Control BMPs - Installation Date&nbsp;<input type="text" name="perimControlBMPInstallDate" size="5" />&nbsp;<a href="javascript:displayDatePicker('perimControlBMPInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a><br /><br />
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="initialSedimentStorage" />&nbsp;Initial sediment storage requirements - Installation Date&nbsp;<input type="text" name="initialSedimentStorageInstallDate" size="5" />&nbsp;<a href="javascript:displayDatePicker('initialSedimentStorageInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a><br /><br />
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="sedimentStoragePerimControlBMP" />&nbsp;Sediment storage requirements and perimeter Control BMPs for the initial segment of the linear infrastructure project - Installation Date&nbsp;<input type="text" name="sedimentStoragePerimControlBMPInstallDate" size="5" />&nbsp;<a href="javascript:displayDatePicker('sedimentStoragePerimControlBMPInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a><br /><br />
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="allSedimentBasins" />&nbsp;All sediment basins within the entire linear infrastructure - Installation Date&nbsp;<input type="text" name="allSedimentBasinsInstallDate" size="5" />&nbsp;<a href="javascript:displayDatePicker('allSedimentBasinsInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a><br /><br />
			
		
			<br /><br /><br />
			
			<input type="checkbox" name="inspectionOccurred7" />&nbsp;This inspection occurred with  seven(7) days of the date checked above.<br /><br /><br />
			
			<input type="checkbox" name="bmpInstalledmaintained" />&nbsp;The Best Management Practices have been installed and are being maintained as designed;<br />
			<textarea name="bmpInstalledmaintainedComments" cols="90" rows="6"></textarea><br /><br />
			
			<input type="checkbox" name="actionItems" />&nbsp;The following action items were identified regarding the installation and maintenance of Best Management Practices;<br />
			<textarea name="actionItemComments" cols="90" rows="6"></textarea><br /><br />
		
		</td>
	</tr>

	
</table>
