<!--get the three different types for questions
1 = daily
2 = weekly
3 = monthly-->
<%
'get daily questions
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetEquipQuestions"
   .parameters.Append .CreateParameter("@questionTypeID", adInteger, adParamInput, 8, 1)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsDaily = oCmd.Execute
Set oCmd = nothing

'get weekly questions
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetEquipQuestions"
   .parameters.Append .CreateParameter("@questionTypeID", adInteger, adParamInput, 8, 2)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsWeekly = oCmd.Execute
Set oCmd = nothing

'get monthly questions
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetEquipQuestions"
   .parameters.Append .CreateParameter("@questionTypeID", adInteger, adParamInput, 8, 3)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsMonthly = oCmd.Execute
Set oCmd = nothing
	
%>

<table border="0" cellpadding="0" cellspacing="0">										
	<tr>
		<td colspan="4">
			<table width="850" cellpadding="0" cellspacing="0" class="borderTable2">
				<tr bgcolor="#666666">
					<td align="center"><span class="searchText">Inspect Daily</span></td>
					<%i = 1
					for i = 1 to 31%>
					<td width="18" align="center"><span class="searchText"><%=i%></span></td>
					<%next
					i = 1%>
				</tr>
				<!--list the items here.-->
				<%do until rsDaily.eof%>
					<tr>
						<td class="tColLeft">&nbsp;<%=rsDaily("question")%></td>
						<%i = 1
						for i = 1 to 31%>
						<td width="5" class="tCol"><input type="checkbox" name="daily_<%=i%>_<%=rsDaily("equipQuestionID")%>" /></td>
						<%next
						i = 1%>
					</tr>				
				<%rsDaily.movenext
				loop%>
			</table>
		</td>
	</tr>
</table><br />
<table border="0" cellpadding="0" cellspacing="0">		
	<tr>
		<td valign="top">
			<table width="270" cellpadding="0" cellspacing="0" class="borderTable2">
				<tr bgcolor="#666666">
					<td align="center"><span class="searchText">Inspect Weekly</span></td>
					<%i = 1
					for i = 1 to 5%>
					<td width="18" align="center"><span class="searchText"><%=i%></span></td>
					<%next
					i = 1%>
				</tr>
				<!--list the items here.-->
				<%do until rsWeekly.eof%>
					<tr>
						<td class="tColLeft">&nbsp;<%=rsWeekly("question")%></td>
						<%i = 1
						for i = 1 to 5%>
						<td width="5" class="tCol"><input type="checkbox" name="weekly_<%=i%>_<%=rsWeekly("equipQuestionID")%>" /></td>
						<%next
						i = 1%>
					</tr>
				<%rsWeekly.movenext
				loop%>
			</table>
		</td>
		<td width="10"></td>
		<td valign="top">
			<table width="200" cellpadding="0" cellspacing="0" class="borderTable2">
				<tr bgcolor="#666666">
					<td align="center"><span class="searchText">Inspect Monthly</span></td>
					<td width="18" align="center"></td>
				</tr>
				<!--list the items here. may be dynamic later-->
				<%do until rsMonthly.eof%>
					<tr>
						<td class="tColLeft">&nbsp;<%=rsMonthly("question")%></td>
						<td width="5" class="tCol"><input type="checkbox" name="monthly_<%=i%>_<%=rsMonthly("equipQuestionID")%>" /></td>
					</tr>
				<%rsMonthly.movenext
				loop%>
			</table>		
		</td>
		<td width="10"></td>
		<td valign="top">
			<table width="340" cellpadding="0" cellspacing="0" class="borderTable2">
				<tr bgcolor="#666666">
					<td align="center"><span class="searchText">Repairs or maintenance needed</span></td>
					<td width="18" align="center"><span class="searchText">Date</span>&nbsp;</td>
				</tr>
				<!--list the items here. may be dynamic later-->
				<%i = 1
				for i = 1 to 10%>
				<tr>
					<td class="tColLeft">&nbsp;<input type="text" size="30" name="maintNeeded<%=i%>" /></td>
					<td class="tCol"><input type="text" name="maintNeededDate<%=i%>" maxlength="10" size="10" value=""/>&nbsp;<a href="javascript:displayDatePicker('maintNeededDate<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
				</tr>
				<%next
				i = 1%>
				
			</table><br />
			
			<table width="340" cellpadding="0" cellspacing="0" class="borderTable2">
				<tr bgcolor="#666666">
					<td align="center"><span class="searchText">Maintenance & Repairs Completed</span></td>
					<td width="18" align="center"><span class="searchText">Date</span>&nbsp;</td>
				</tr>
				<!--list the items here. may be dynamic later-->
				<%i = 1
				for i = 1 to 10%>
				<tr>
					<td class="tColLeft">&nbsp;<input type="text" size="30" name="maintCompleted<%=i%>" /></td>
					<td class="tCol"><input type="text" name="maintCompletedDate<%=i%>" maxlength="10" size="10" value=""/>&nbsp;<a href="javascript:displayDatePicker('maintCompletedDate<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
				</tr>
				<%next
				i = 1%>
				
			</table>
		
		</td>
		
	</tr>
</table>