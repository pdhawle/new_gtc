var datePickerDivID = "datepicker";
var iFrameDivID = "datepickeriframe";


function displayAnswerPicker(dateFieldName, displayBelowThisObject, dtFormat, dtSep)
{
  var targetDateField = document.getElementsByName (dateFieldName).item(0);
 
  // if we weren't told what node to display the datepicker beneath, just display it
  // beneath the date field we're updating
  if (!displayBelowThisObject)
    displayBelowThisObject = targetDateField;
 
  // if a date separator character was given, update the dateSeparator variable
  if (dtSep)
    dateSeparator = dtSep;
  else
    dateSeparator = defaultDateSeparator;
 
  // if a date format was given, update the dateFormat variable
  if (dtFormat)
    dateFormat = dtFormat;
  else
    dateFormat = defaultDateFormat;
 
  var x = displayBelowThisObject.offsetLeft;
  var y = displayBelowThisObject.offsetTop + displayBelowThisObject.offsetHeight ;
 
  // deal with elements inside tables and such
  var parent = displayBelowThisObject;
  while (parent.offsetParent) {
    parent = parent.offsetParent;
    x += parent.offsetLeft;
    y += parent.offsetTop ;
  }
 
  drawAnswerPicker(targetDateField, x, y);
}


/**
Draw the datepicker object (which is just a table with calendar elements) at the
specified x and y coordinates, using the targetDateField object as the input tag
that will ultimately be populated with a date.

This function will normally be called by the displayDatePicker function.
*/
function drawAnswerPicker(targetDateField, x, y)
{
  var dt = getFieldDate(targetDateField.value );
 
  // the datepicker table will be drawn inside of a <div> with an ID defined by the
  // global datePickerDivID variable. If such a div doesn't yet exist on the HTML
  // document we're working with, add one.
  if (!document.getElementById(datePickerDivID)) {
    // don't use innerHTML to update the body, because it can cause global variables
    // that are currently pointing to objects on the page to have bad references
    //document.body.innerHTML += "<div id='" + datePickerDivID + "' class='dpDiv'></div>";
    var newNode = document.createElement("div");
    newNode.setAttribute("id", datePickerDivID);
    newNode.setAttribute("class", "dpDiv");
    newNode.setAttribute("style", "visibility: hidden;");
    document.body.appendChild(newNode);
  }
 
  // move the datepicker div to the proper x,y coordinate and toggle the visiblity
  var pickerDiv = document.getElementById(datePickerDivID);
  pickerDiv.style.position = "absolute";
  pickerDiv.style.left = x + "px";
  pickerDiv.style.top = y + "px";
  pickerDiv.style.visibility = (pickerDiv.style.visibility == "visible" ? "hidden" : "visible");
  pickerDiv.style.display = (pickerDiv.style.display == "block" ? "none" : "block");
  pickerDiv.style.zIndex = 10000;
 
  // draw the datepicker table
  refreshAnswerPicker(targetDateField.name);
}


/**
This is the function that actually draws the datepicker calendar.
*/
function refreshAnswerPicker(dateFieldName)
{
  var crlf = "\r\n";
  var TABLE = "<table cols=7 class='dpTableA'>" + crlf;
  var xTABLE = "</table>" + crlf;
  var TR = "<tr class='dpTRA'>";
  var TR_title = "<tr class='dpTitleTRA'>";
  var TR_days = "<tr class='dpDayTRA'>";
  var TR_todaybutton = "<tr class='dpTodayButtonTRA'>";
  var xTR = "</tr>" + crlf;
  var TD = "<td class='dpTDA' onMouseOut='this.className=\"dpTDA\";' onMouseOver=' this.className=\"dpTDHoverA\";' ";    // leave this tag open, because we'll be adding an onClick event
  var TD_title = "<td colspan=5 class='dpTitleTDA'>";
  var TD_buttons = "<td class='dpButtonTDA'>";
  var TD_todaybutton = "<td colspan=7 class='dpTodayButtonTDA'>";
  var TD_days = "<td class='dpDayTDA'>";
  var TD_selected = "<td class='dpDayHighlightTDA' onMouseOut='this.className=\"dpDayHighlightTDA\";' onMouseOver='this.className=\"dpTDHoverA\";' ";    // leave this tag open, because we'll be adding an onClick event
  var xTD = "</td>" + crlf;
  var DIV_title = "<div class='dpTitleTextA'>";
  var DIV_selected = "<div class='dpDayHighlightA'>";
  var xDIV = "</div>";
  
  var myAns1 = "Maintain Silt Fence Type C - Full";
  var myAns2 = "Maintain Silt Fence Type C - Damaged";
  var myAns3 = "Maintain Silt Fence Type C - Undermined";
  var myAns4 = "Maintain Silt Fence Type A - Full";
  var myAns5 = "Maintain Silt Fence Type A - Damaged";
  var myAns6 = "Maintain Silt Fence Type A - Undermined";
  var myAns7 = "Maintain Slope Erosion";
  var myAns8 = "Maintain Slope Vegetation";
  var myAns9 = "Maintain Check Dam - Full";
  var myAns10 = "Maintain Construction Exit - Refresh";
  var myAns11 = "Observed Sediment";
  var myAns12 = "Stabilize Exposed Areas With Much or Temporary Vegetation";
  //var myAns13 = "Remove sediment from storm inlet cover";
  //var myAns14 = "Repair and maintain grass mesh";
  //var myAns15 = "Repair and maintain outfall";
  //var myAns16 = "Repair and maintain silt fence";
  //var myAns17 = "Repair and maintain storm inlet cover";
  //var myAns18 = "Repair and maintain wind fence";
  //var myAns19 = "Sweep street";
  //var myAns20 = "";
  //var myAns21 = "";
  //var myAns22 = "";
  //var myAns23 = "";
  //var myAns24 = "";
 // var myAns25 = "";
 // var myAns26 = "";
 // var myAns27 = "";
 // var myAns28 = "";
 // var myAns29 = "";
 // var myAns30 = "";
 // var myAns31 = "";
 // var myAns32 = "";
 // var myAns33 = "";
  
  
  var TDContent = "<span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns1 + "');\">" + myAns1 + "</span>" 
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns2 + "');\">" + myAns2 + "</span>"
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns3 + "');\">" + myAns3 + "</span>"
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns4 + "');\">" + myAns4 + "</span>"
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns5 + "');\">" + myAns5 + "</span>"
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns6 + "');\">" + myAns6 + "</span>"
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns7 + "');\">" + myAns7 + "</span>"
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns8 + "');\">" + myAns8 + "</span>"
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns9 + "');\">" + myAns9 + "</span>"
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns10 + "');\">" + myAns10 + "</span>"
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns11 + "');\">" + myAns11 + "</span>"  
  TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns12 + "');\">" + myAns12 + "</span>"
  //TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns13 + "');\">" + myAns13 + "</span>"
  //TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns14 + "');\">" + myAns14 + "</span>"
  //TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns15 + "');\">" + myAns15 + "</span>"
  //TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns16 + "');\">" + myAns16 + "</span>"
  //TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns17 + "');\">" + myAns17 + "</span>"
  //TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns18 + "');\">" + myAns18 + "</span>"
  //TDContent += "<br><span class='dpTodayButtonA' onclick=\"updateDateField('" + dateFieldName + "', '" + myAns19 + "');\">" + myAns19 + "</span>"
  // start generating the code for the calendar table
  var html = TABLE;
  
  html += TR_title;
  html += TD_buttons + TDContent + xTD;
  html += xTR;

  html += TR_todaybutton + TD_todaybutton;
  html += "<br><span class='dpTodayButtonAA' onClick='updateDateField(\"" + dateFieldName + "\");'>close</span>";
  html += xTD + xTR;
 
  // and finally, close the table
  html += xTABLE;
 
  document.getElementById(datePickerDivID).innerHTML = html;
  // add an "iFrame shim" to allow the datepicker to display above selection lists
  adjustiFrame();
}


function updateDateField(dateFieldName, dateString)
{
  var targetDateField = document.getElementsByName (dateFieldName).item(0);
  if (dateString)
	targetDateField.value = dateString;
 
  var pickerDiv = document.getElementById(datePickerDivID);
  pickerDiv.style.visibility = "hidden";
  pickerDiv.style.display = "none";
 
  adjustiFrame();
  targetDateField.focus();
 
  // after the datepicker has closed, optionally run a user-defined function called
  // datePickerClosed, passing the field that was just updated as a parameter
  // (note that this will only run if the user actually selected a date from the datepicker)
  if ((dateString) && (typeof(datePickerClosed) == "function"))
	datePickerClosed(targetDateField);
}


/**
Use an "iFrame shim" to deal with problems where the datepicker shows up behind
selection list elements, if they're below the datepicker. The problem and solution are
described at:

http://dotnetjunkies.com/WebLog/jking/archive/2003/07/21/488.aspx
http://dotnetjunkies.com/WebLog/jking/archive/2003/10/30/2975.aspx
*/
function adjustiFrame(pickerDiv, iFrameDiv)
{
  // we know that Opera doesn't like something about this, so if we
  // think we're using Opera, don't even try
  var is_opera = (navigator.userAgent.toLowerCase().indexOf("opera") != -1);
  if (is_opera)
    return;
  
  // put a try/catch block around the whole thing, just in case
  try {
    if (!document.getElementById(iFrameDivID)) {
      // don't use innerHTML to update the body, because it can cause global variables
      // that are currently pointing to objects on the page to have bad references
      //document.body.innerHTML += "<iframe id='" + iFrameDivID + "' src='javascript:false;' scrolling='no' frameborder='0'>";
      var newNode = document.createElement("iFrame");
      newNode.setAttribute("id", iFrameDivID);
      newNode.setAttribute("src", "javascript:false;");
      newNode.setAttribute("scrolling", "no");
      newNode.setAttribute ("frameborder", "0");
      document.body.appendChild(newNode);
    }
    
    if (!pickerDiv)
      pickerDiv = document.getElementById(datePickerDivID);
    if (!iFrameDiv)
      iFrameDiv = document.getElementById(iFrameDivID);
    
    try {
      iFrameDiv.style.position = "absolute";
      iFrameDiv.style.width = pickerDiv.offsetWidth;
      iFrameDiv.style.height = pickerDiv.offsetHeight ;
      iFrameDiv.style.top = pickerDiv.style.top;
      iFrameDiv.style.left = pickerDiv.style.left;
      iFrameDiv.style.zIndex = pickerDiv.style.zIndex - 1;
      iFrameDiv.style.visibility = pickerDiv.style.visibility ;
      iFrameDiv.style.display = pickerDiv.style.display;
    } catch(e) {
    }
 
  } catch (ee) {
  }
 
}