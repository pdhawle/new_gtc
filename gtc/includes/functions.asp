<%

function handleApostrophe(val)
	handleApostrophe = Replace(val, "'", "''") 
end function

function handleApostropheDisplay(val)
	handleApostropheDisplay = Replace(val, "''", "'") 
end function

function isChecked(checkvalue)
	If checkvalue = true then
		isChecked = "checked"
	else
		isChecked = ""
	end if
end function

function requestIsChecked(checkvalue)
	If checkvalue = "on" then
		requestIsChecked = "checked"
	else
		requestIsChecked = ""
	end if
end function

function isOn(checkvalue)
	If checkvalue = "on" Then
		isOn = true
	else
		isOn = false
	end if
end function

function setx(checkvalue)
	If checkvalue = "on" Then
		setx = "x"
	else
		setx = ""
	end if
end function

function setxtf(checkvalue)
	If checkvalue = "True" Then
		setxtf = "x"
	else
		setxtf = ""
	end if
end function

function IsUserActive(userstate)
	If userstate = "True" Then
		IsUserActive = "<img src=images/check.gif width=17 height=17>"
	else
		IsUserActive = "<a href=process.asp?processType=activateUser>activate</a>"
	end if
end function


Function InspectionDaysInMonth(WhatYear, WhatMonth)
'On Error GoTo Err_WorkingDaysInMonth
  
Dim intCount
Dim dtmStart
Dim dtmEnd

  dtmStart = DateSerial(WhatYear, WhatMonth, 1)
  dtmEnd = DateSerial(WhatYear, WhatMonth + 1, 0)
  intCount = 0
  Do While dtmStart <= dtmEnd
    Select Case Weekday(dtmStart)
      Case 2, 3, 4, 5, 6
        intCount = intCount + 1
    End Select
    dtmStart = dtmStart + 1
  Loop
  
  InspectionDaysInMonth = intCount
   
End Function

Function getMonth(imonth)
	Select Case iMonth
		Case 1
			getMonth = "January"
		Case 2
			getMonth = "February"
		Case 3
			getMonth = "March"
		Case 4
			getMonth = "April"
		Case 5
			getMonth = "May"		
		Case 6
			getMonth = "June"
		Case 7
			getMonth = "July"
		Case 8
			getMonth = "August"
		Case 9
			getMonth = "September"
		Case 10
			getMonth = "October"
		Case 11
			getMonth = "November"
		Case 12
			getMonth = "December"
	end Select
end function

Function getDay(iDay)
	Select Case iDay
		Case 1
			getDay = "Sunday"
		Case 2
			getDay = "Monday"
		Case 3
			getDay = "Tuesday"
		Case 4
			getDay = "Wednesday"
		Case 5
			getDay = "Thursday"		
		Case 6
			getDay = "Friday"
		Case 7
			getDay = "Saturday"
	end Select
end function

function isSelected(iVal1,iVal2)
	if trim(iVal1) = trim(iVal2) then
		isSelected = "selected"
	else
		isSelected = ""
	end if
end function

function isCheckedRadio(iVal1,iVal2)
	if trim(iVal1) = trim(iVal2) then
		isCheckedRadio = "checked"
	else
		isCheckedRadio = ""
	end if
end function

function isSelectedRadio(iVal1,iVal2)
	if trim(iVal1) = trim(iVal2) then
		isSelectedRadio = "checked"
	else
		isSelectedRadio = ""
	end if
end function

function checkNull(val)
	If val = "" then
		checkNull = null
	else
		checkNull = val
	end if
end function

function checkNullValue(val)
	If isNull(val) then
		checkNullValue = ""
	else
		checkNullValue = val
	end if
end function

function setZero(val)
	If val = "" then
		setZero = 0
	else
		setZero = val
	end if
end function


function logUserIn(userID)

	On Error Resume Next
	
	Set DataConnLog = Server.CreateObject("ADODB.Connection") 
	DataConnLog.Open Session("Connection"), Session("UserID")

	Set oCmdLog = Server.CreateObject("ADODB.Command")
	
	With oCmdLog
	   .ActiveConnection = DataConnLog
	   .CommandText = "spAddUserLog"
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
	   .parameters.Append .CreateParameter("@loginDate", adDBTimeStamp, adParamInput, 8, now())
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsLog = oCmdLog.Execute
	Set oCmdLog = nothing
	
	logUserIn = rsLog("identity")

end function

function hilightrow(val,roweven)
	if val = 1 or val = 2 then
		hilightrow = "bgcolor=#FFFF99"
	else
		if roweven = "True" then
			hilightrow = "class=rowColor"
		else
			hilightrow = ""
		end if
	end if
end function

function replaceDash(val)
	replaceDash=Replace(val,"-","&ndash;")
end function

function stripPhone(val)
	sPhone=Replace(val,"-","")
	sPhone=Replace(sPhone,".","")
	sPhone=Replace(sPhone,"(","")
	sPhone=Replace(sPhone,")","")
	sPhone=Replace(sPhone," ","")
	
	stripPhone = sPhone
	
end function

function firstThree(val)
	
	firstThree = left(val,3)
	
end function

function secondThree(val)
	sVal = right(val,7)
	secondThree = left(sVal,3)
	
end function

function lastFour(val)
	
	lastFour = right(val,4)
	
end function

function addDotFirstThree(val)
	
	addDotFirstThree = left(val,3) & "-"
	
end function

function addDotSecondThree(val)
	sVal = right(val,7)
	addDotSecondThree = left(sVal,3) & "-"
	
end function

function calculateClassCost(classID,classTypeID)
	On Error Resume Next
	Set DataConn = Server.CreateObject("ADODB.Connection")
	DataConn.Open Session("Connection"), Session("UserID")

	'get the class so we know which options were selected
	Set oCmd = Server.CreateObject("ADODB.Command")	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetClass"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, classID)
	   .CommandType = adCmdStoredProc	   
	End With				
	Set rsClass = oCmd.Execute
	Set oCmd = nothing
	
	'get the amounts for the type of class
	Set oCmd = Server.CreateObject("ADODB.Command")	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetClasstype"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, classTypeID)
	   .CommandType = adCmdStoredProc	   
	End With				
	Set rsClassType = oCmd.Execute
	Set oCmd = nothing

	'get the base amount based on what size is selected
	select case rsClass("classSize")
		case 10
			iAmt = setZero(rsClassType("upTo10"))
		case 15
			iAmt = setZero(rsClassType("upTo15"))
		case 20
			iAmt = setZero(rsClassType("upTo20"))
		case 35
			iAmt = setZero(rsClassType("upTo35"))
		case 50
			iAmt = setZero(rsClassType("upTo50"))
	end select
	
	'calculate the number of student manuals
	if rsClass("numberManuals") <> "" then
		iManCost = setZero(rsClass("numberManuals")) * setZero(rsClassType("studentManuals"))
		iAmt = iAmt + iManCost	
	end if
	
	'calculate saturday class if selected
	if rsClass("saturdayClass") = "True" then
		iAmt = iAmt + setZero(rsClassType("saturdayCharge"))
		'if there is a deposit, calculate it
		if rsClassType("saturdayDeposit") <> "" then
			iAmt = iAmt + setZero(rsClassType("saturdayDeposit"))
		end if
	end if
	
	'calculate evening/split class if selected
	if rsClass("eveningClass") = "True" then
		iAmt = iAmt + setZero(rsClassType("eveningSplitCharge"))
	end if
	
	'if there needs to be projector, calculate
	if trim(rsClass("projector")) = "True" then
		iAmt = iAmt + setZero(rsClassType("AVCharge"))	
	end if
	
	'if there needs to be screen, calculate
	if trim(rsClass("screen")) = "True" then		
		iAmt = iAmt + setZero(rsClassType("AVCharge"))		
	end if
	
	calculateClassCost = formatCurrency(iAmt,2)
end function

function getRainfallAmount(iSiteID,dteDate)
	sProjectName = "gtc"

	sUserID = "HBNextSrv"
	sPassword = "RainSrv2011"
	
	on error resume next
	'set up xmlhttp to checkout server
	Dim oRequest
	Set oRequest = Server.CreateObject("MSXML2.ServerXMLHTTP")
	
	'setting this option will allow ServerXMLHTTP to ignore the certificate errors it encounters.
	oRequest.setOption(2) = SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS
	
	' resolve, connect, send, receive - in milliseconds
	oRequest.setTimeouts 10000, 10000, 10000, 10000
	
	'set the URL
	msURL = "http://rainwave.hbnext.com/services/DataService.asmx"
	
	msSOAP = "<?xml version=""1.0"" encoding=""utf-8""?>"
	msSOAP = msSOAP & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
	msSOAP = msSOAP & "<soap:Body>"
	msSOAP = msSOAP & "<GetPrecipitationData xmlns=""http://rainwave.hbnext.com/services"">"
	msSOAP = msSOAP & "<ProjectName>" & sProjectName & "</ProjectName>"
	msSOAP = msSOAP & "<SiteID>" & iSiteID & "</SiteID>"
	msSOAP = msSOAP & "<RequestedDate>" & dteDate & "</RequestedDate>"
	msSOAP = msSOAP & "<UserID>" & sUserID & "</UserID>"
	msSOAP = msSOAP & "<Password>" & sPassword & "</Password>"
	msSOAP = msSOAP & "</GetPrecipitationData>"
	msSOAP = msSOAP & "</soap:Body>"
	msSOAP = msSOAP & "</soap:Envelope>"
	
	oRequest.Open "POST", msURL, False
	oRequest.setRequestHeader "Content-Type", "text/xml"
	'oRequest.setRequestHeader "SOAPAction", ""
	oRequest.send msSOAP
	
	if err then
		response.Write err.description
	end if 
	
	WSXML = oRequest.responseText
	
	'response.Write WSXML
	
	
	set xml = Server.CreateObject("Chilkat.Xml")
	
	success = xml.LoadXml(WSXML)
'	If (success <> 1) Then
'		Response.Write "<pre>" & Server.HTMLEncode( xml.LastErrorText) & "</pre>"
'	End If
	strPrecip = xml.ChilkatPath("soap:Body|GetPrecipitationDataResponse|GetPrecipitationDataResult|diffgr:diffgram|NewDataSet|Rain|fldPrecipitation|*")
	if strPrecip <> "" then
		getRainfallAmount = round(strPrecip,2)
	else
		getRainfallAmount = "0.0"
	end if
end function

function addZeroToNumber(ival)
	if ival < 10 then
		addZeroToNumber = "0" & ival
	else
		addZeroToNumber = ival
	end if
end function


function checkForAttachments(reportID,projectID)
	
	
	Set fso = CreateObject("Scripting.FileSystemObject")									
	sPath = "downloads/project_" & projectID
	
	If not fso.FolderExists(server.mappath(sPath)) Then 
		'fso.CreateFolder(server.mappath(sPath))
		checkForAttachments = ""
	end if
	
	sPath = "downloads/project_" & projectID & "/report_" & reportID
	If not fso.FolderExists(server.mappath(sPath)) Then 
		'response.Write server.mappath(sPath)
		'fso.CreateFolder(server.mappath(sPath))
		checkForAttachments = ""
	end if									


	If fso.FolderExists(server.mappath(sPath)) Then
		Set folder = fso.GetFolder(server.mappath(sPath))
		'Response.Write folder
		Set filez = folder.Files			
	
		strPath = sPath & "/"
		
		Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
		' Get a handle on our folder
		Set objFolder = objFSO.GetFolder(Server.MapPath(strPath))
		
		For Each objItem In objFolder.Files
			checkForAttachments = "<img src=""images/attachment.gif"">"
		NEXT
		
	end if	
	
	Set objItem = Nothing
	Set objFolder = Nothing
	Set objFSO = Nothing


end function


function checkForRA(projectID)

	On Error Resume Next
	
	Set DataConnLog = Server.CreateObject("ADODB.Connection") 
	DataConnLog.Open Session("Connection"), Session("UserID")

	Set oCmdRA = Server.CreateObject("ADODB.Command")
	
	With oCmdRA
	   .ActiveConnection = DataConnLog
	   .CommandText = "spCheckForRA"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsRA = oCmdRA.Execute
	Set oCmdRA = nothing
	
	if rsRA.eof then
		checkForRA = "False"
	else
		checkForRA = "True"
	end if

end function


%>