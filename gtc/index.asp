<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%appLogo = request("appLogo")
bLoggedInInfo = "False"
sURLRed = request("sURLRed")

if appLogo <> "" then
	Session("appLogo") = appLogo
end if
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript" src="includes/validator.js" type="text/javascript"></script>
</head>
<body onLoad="document.frmLogin.txtUserName.focus()">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<%
									If Request("Posted") = "" Then%>
										<form method="post" action="index.asp" name="frmLogin">
										<input type="hidden" name="Posted" value="Yes">
										<input type="hidden" name="sURLRed" value="<%=sURLRed%>">
										<!--<fieldset>
										<legend class="frameSet">Log-In</legend>-->
										<table>
											<tr><td colspan="2"></td></tr>
											<tr>
												<td><strong>Username:</strong></td>
												<td><input type="text" name="txtUserName" size="30" tooltipText="Please enter your username."></td>
											</tr>
											<tr>
												<td valign="top"><strong>Password:</strong></td>
												<td>
													<input type="password" name="txtPassword" size="30" tooltipText="Please enter your password.">&nbsp;&nbsp;<input type="submit" value="Login" name="login" class="formButton"><br><br>
													<a href="form_addUser.asp?clientID=1">New User?</a><br><br>
													
													<a href="emailUP.asp">Forgot Username and or Password?</a><br><br>
													
													This site has been tested and is best viewed using <strong>Microsoft Internet Explorer 6.0 and higher</strong>.
												</td>
											</tr>
										</table>
										<!--</fieldset>-->
										</form>
									<%else
									
										Dim DataConn
										Dim oCmd
										Dim rs
										Dim sUserName
										Dim sPassword
										
										sUserName = Trim(Request("txtUserName"))
										sPassword = Trim(Request("txtPassword"))
										
										On Error Resume Next
										
											Set DataConn = Server.CreateObject("ADODB.Connection") 
											If Err Then
										%>
												<!--#include file="includes/FatalError.inc"-->
										<%
											End If
											
											DataConn.Open Session("Connection"), Session("UserID")
											If Err Then
										%>
												<!--#include file="includes/FatalError.inc"-->
										<%
											End If
											
											'Create command
											Set oCmd = Server.CreateObject("ADODB.Command")
												
											With oCmd
											   .ActiveConnection = DataConn
											   .CommandText = "spCheckLogin"
											   .parameters.Append .CreateParameter("@userName", adVarchar, adParamInput, 50, Trim(sUserName))
											   .parameters.Append .CreateParameter("@password", adVarchar, adParamInput, 50, Trim(sPassword))
											   .CommandType = adCmdStoredProc
											   
											End With
											
											If Err Then
										%>
												<!--#include file="includes/FatalError.inc"-->
										<%
											End If
													
											Set rs = oCmd.Execute
											
											If Err Then
										%>
												<!--#include file="includes/FatalError.inc"-->
										<%
											End If
											
											If rs.eof Then
												Session("LoggedIn") = "False"%>
												<form method="post" action="index.asp" name="frmLogin">
												<input type="hidden" name="Posted" value="Yes">
												<input type="hidden" name="sURLRed" value="<%=sURLRed%>">
													<table>
														<tr><td colspan="2" align="center"><font color="#FF0000">bad username/password. please try again.</font></td></tr>
														<tr>
															<td><strong>Username:</strong></td>
															<td><input type="text" name="txtUserName" size="30" tooltipText="Please enter your username."></td>
														</tr>
														<tr>
															<td valign="top"><strong>Password:</strong></td>
															<td>
																<input type="password" name="txtPassword" size="30" tooltipText="Please enter your password.">&nbsp;&nbsp;<input type="submit" value="Login" name="login" class="formButton"><br><br>
																<a href="form_addUser.asp?clientID=1">New User?</a><br><br>
																
																<a href="emailUP.asp">Forgot Username and or Password?</a><br><br>
																
																This site has been tested and is best viewed using <strong>Microsoft Internet Explorer 6.0 and higher</strong>.
															</td>
														</tr>
													</table>
												</form>
											<%Else
												'set some session variables
												Session.Timeout = 400
												Session("singleProjectUser") = Trim(rs("isSingleProject"))
												Session("maintenanceUser") = Trim(rs("maintenanceUser"))
												Session("inspector") = Trim(rs("inspector"))
												'Session("bmpContractor") = Trim(rs("bmpContractor"))
												Session("CustomerAdmin") = Trim(rs("customerAdmin"))
												Session("Admin") = Trim(rs("isAdmin"))
												Session("UserName") = Trim(rs("userName"))
												Session("Password") = Trim(rs("password"))
												Session("LoggedIn") = "True"
												Session("FirstName") = Trim(rs("firstName"))					
												Session("Name") = Trim(rs("firstName")) & " " & Trim(rs("lastName"))
												Session("Email") = Trim(rs("email"))
												Session("Company") = Trim(rs("company"))
												Session("JobTitle") = Trim(rs("jobtitle"))
												Session("OffiecPhone") = Trim(rs("officePhone"))
												Session("cellPhone") = Trim(rs("cellPhone"))
												Session("ID") = Trim(rs("userID"))
												if appLogo <> "" then
													Session("appLogo") = appLogo
												else
													Session("appLogo") = Trim(rs("appLogo"))
												end if
												Session("customerReports") = Trim(rs("customerReports"))
												Session("openCorrectedLog") = Trim(rs("openCorrectedLog"))
												Session("projectDocuments") = Trim(rs("projectDocuments"))
												Session("noiUser") = Trim(rs("noiUser"))
												Session("canApprove") = Trim(rs("canApprove"))
												'Session("Division") = Trim(rs("division"))
												'Session("DivisionID") = Trim(rs("divisionID"))
												'Session("Logo") = Trim(rs("logo"))
												
												Session("clientID") = rs("clientID")
												Session("superAdmin") = rs("isSuperAdmin")
												Session("viewQuotes") = rs("viewQuotes")
												Session("deleteWorkOrder") = rs("deleteWorkOrder")
												Session("closeWorkOrder") = rs("closeWorkOrder")
												Session("hrAdmin") = rs("hrAdmin")
												Session("empType") = rs("empType")
												Session("OSHAManager") = rs("OSHAManager")
												
												'log the user info into the userLog table
												Session("logID") = logUserIn(Trim(rs("userID")))												
												
												If Err Then
											%>
													<!--#include file="includes/FatalError.inc"-->
											<%
												End If
												
												if sURLRed = "" then
													Response.Redirect("main.asp")
												else
													Response.Redirect sURLRed
												end if
						
											end if
										end if%>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("frmLogin");
  frmvalidator.addValidation("txtUserName","req","Please enter you user name");
   frmvalidator.addValidation("txtPassword","req","Please enter you password");
</script>
</body>
</html>