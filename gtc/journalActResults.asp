<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iUser = request("inspector")
dtFrom = request("fromDate")
dtTo = request("toDate")
projectID = request("projectID")

ssort = request("sort")
if ssort = "" then
	columnName = "postedOn"
else
	columnName = ssort
end if


If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
if projectID <> "" then


	if projectID = "all" then
		'get journal entries based on all users
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetJournalActivityAllSort"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
		
	else
	
		'get journal entries based on specific user
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetJournalActivityByProjectSort"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	end if



	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProject"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc
	   
	End With
		
	Set rsProject = oCmd.Execute
	Set oCmd = nothing


else
	
	if iUser = "all" then
		'get journal entries based on all users
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetJournalActivityAllSort"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
		
	else
	
		'get journal entries based on specific user
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetJournalActivitySort"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, iUser)
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	end if

end if

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Journal Activity</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<!--<a href="expInspActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;--><a href="chooseJournalAct.asp" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr><td></td><td colspan="10">
											Date Range: <strong><%=dtFrom%> - <%=dtTo%></strong>
											<%if projectID <> "" then%>
												<br>
												Project Name: <strong><%=rsProject("projectName")%></strong>
											<%end if%>
										</td></tr>
										<tr bgcolor="#666666" height="35">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td width="65"><span class="searchText"><a href="journalActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&projectID=<%=projectID%>&sort=postedOn" style="color:#721A32;text-decoration:underline;font-weight:bold;">Date Posted</a></span></td>
											<td><span class="searchText"><a href="journalActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&projectID=<%=projectID%>&sort=lastName" style="color:#721A32;text-decoration:underline;font-weight:bold;">Posted By</a></span></td>
											<td width="8"></td>
											<td><span class="searchText"><a href="journalActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&projectID=<%=projectID%>&sort=itemName" style="color:#721A32;text-decoration:underline;font-weight:bold;">Title</a></span></td>
											<td width="8"></td>
											<td><span class="searchText">Project</span></td>
											<td width="8"></td>
											<td><span class="searchText"><a href="journalActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&projectID=<%=projectID%>&sort=itemEntry" style="color:#721A32;text-decoration:underline;font-weight:bold;">Message</a></span></td>
										</tr>
								
										<%If rsReport.eof then%>
												<tr><td></td><td colspan="9">there are no reports to display</td></tr>
											<%else
												blnChange = false
												dim i
												i = 0
												Do until rsReport.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td valign="top"><%=formatDateTime(rsReport("postedOn"),2)%></td>
														<td valign="top"><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></td>
														<td></td>
														<td valign="top"><%=rsReport("itemName")%></td>
														<td></td>
														<td valign="top">
															<%
															'parse out the reportID from the item name
															reportID = cint(getReportIDFromItem(rsReport("itemName")))
															
															if reportID <> "" then
																Set oCmd = Server.CreateObject("ADODB.Command")
																With oCmd
																   .ActiveConnection = DataConn
																   .CommandText = "spGetProjectByReport"
																   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, reportID)
																   .CommandType = adCmdStoredProc   
																End With
																			
																Set rsProject = oCmd.Execute
																Set oCmd = nothing
															end if
															
															%>
															<%=rsProject("projectName")%>
														</td>
														<td></td>
														<td valign="top"><%=rsReport("itemEntry")%></td>
													</tr>
												<%
												
												rsReport.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												i = i + 1
												loop%>
											
											<%end if%>
										
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing

function getReportIDFromItem(val)
	getReportIDFromItem = replace(val,"Report #","")
end function
%>