<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

journalID = Request("journalID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetJournal"
   .parameters.Append .CreateParameter("@journalID", adInteger, adParamInput, 8, journalID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<form name="openItems" method="post" action="process.asp">
<table width="600" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<tr>
		<td colspan="3" bgcolor="#FA702B">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="15"><strong>Title:</strong> <%=rs("itemName")%></td></tr>
										<tr><td colspan="15"><strong>Entry Date:</strong> <%=MonthName(rs("itemMonth"))%>&nbsp;<%=rs("itemDay")%>, <%=rs("itemYear")%></td></tr>
										<tr><td colspan="15"><strong>Blog/Journal Entry:</strong> <%=rs("itemEntry")%></td></tr>
										<tr><td colspan="15"><strong>Posted On:</strong> <%=rs("postedOn")%></td></tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>