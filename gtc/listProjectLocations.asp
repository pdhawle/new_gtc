<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
'sState = Request("s")
inspector = request("primaryInspector")
clientID = session("clientID")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

If 	inspector = "" then

	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProjectsActive"'sp
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
else

	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProjectsActiveInspector"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, inspector)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
end if


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNextInspectors"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsInspectors = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function dept_onchange(projectList) {
   projectList.submit(); 
}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>						
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Project Locations</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expProjectLocationList.asp?primaryInspector=<%=inspector%>&clientID=<%=clientID%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<%if sState <> "ActiveNI" then%>
										<tr>
											<td></td>
											<td colspan="10">
								
												<form name="projectList" method="post" action="listProjectLocations.asp">
												Filter By Primary Inspector:<br>
												<select name="primaryInspector" onChange="return dept_onchange(projectList)">
													<option value="">--all inspectors--</option>
													<%do until rsInspectors.eof
														if trim(rsInspectors("userID")) = trim(inspector) then%>
															<option selected="selected" value="<%=rsInspectors("userID")%>"><%=rsInspectors("firstName") & " " & rsInspectors("lastName")%></option>
														<%else%>
															<option value="<%=rsInspectors("userID")%>"><%=rsInspectors("firstName") & " " & rsInspectors("lastName")%></option>
														<%end if%>
													<%rsInspectors.movenext
													loop%>
												</select>
												</form>
											</td>
										</tr>
										<%end if%>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Customer</span></td>
											<td><span class="searchText">Project Name</span></td>
											<td><span class="searchText">Primary Inspector</span></td>
											<td align="center"><span class="searchText">City</span></td>
											<td align="center"><span class="searchText">State</span></td>					
											<td align="center"><span class="searchText">County</span></td>
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="10">there are no records to display</td></tr>
										<%else
											iBillRate = 0
											idRate = 0
											iwprRate = 0
										
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><a href="customerList.asp?id=<%=left(rs("customerName"),1)%>"><%=rs("customerName")%></a></td>
													<td><a href="form.asp?id=<%=rs("projectID")%>&formType=editProject&customerID=<%=rs("customerID")%>&divisionID=<%=rs("divisionID")%>"><%=rs("projectName")%></a></td>
													<td><%=rs("firstName") & " " & rs("lastName")%></td>
													<td align="center"><%=rs("city")%></td>
													<td align="center"><%=rs("state")%></td>
													<td align="center"><%=rs("countyName")%></td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>

									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>