<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
projectID = request("projectID")
customerID = request("customerID")
divisionID = request("divisionID")
red = request("red")
sPath = request("path")

if red <> "" then
	bLoggedInInfo = "False"
end if

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")


if sPath = "Home" then

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProjectsByDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsProjects = oCmd.Execute
Set oCmd = nothing

end if

if projectID <> "" then


Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsProject = oCmd.Execute
Set oCmd = nothing

if projectID = "" then
	projectID = rsProject("projectID")
end if
if customerID = "" then
	customerID = rsProject("customerID")
end if
if divisionID = "" then
	divisionID = rsProject("divisionID")
end if



'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetLocationsByProject"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing
end if
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this location?")) {
    document.location = delUrl;
  }


}
//-->
</script>
<script type="text/javascript">
<!--

function proj_onchange(frmlocations) {
   document.frmlocations.action = "locationList.asp";
   frmlocations.submit(); 
}
// -->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Location List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<%if projectID <> "" then%>
				<tr class="colorBars">
					<td colspan="5">
						
						&nbsp;&nbsp;<a href="form.asp?formType=addLocation&customerID=<%=customerID%>&divisionID=<%=divisionID%>&projectID=<%=projectID%>&path=<%=sPath%>" class="footerLink">add location</a>&nbsp;&nbsp;
						
						<%if sPath <> "Home" then%>
						<%if red = "" then%>
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="projectList.asp?id=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">project list</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="divisionList.asp?ID=<%=customerID%>" class="footerLink">division list</a>			
						<%end if%>
						<%end if%>
					</td>
				</tr>
				<%end if%>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>
									<%if sPath = "Home" then%>
									<form name="frmlocations" method="post" action="locationList.asp">
									<select name="projectID" onChange="return proj_onchange(frmlocations)">
										<option>--select project--</option>
										<%do until rsProjects.eof	
											if trim(projectID) = trim(rsProjects("projectID")) then%>
											<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
											<%else%>
											<option value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
										<%end if
										rsProjects.movenext
										loop%>
									</select>
									<%if projectID = "" then%>
										please select a project
									<%end if%>
									<input type="hidden" name="path" value="<%=sPath%>"/>									
									<input type="hidden" name="divisionID" value="<%=divisionID%>"/>
									<input type="hidden" name="customerID" value="<%=customerID%>"/>
									</form>	
									<br><br>
									<%end if%>
									<%if projectID <> "" then%>					
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr bgcolor="#666666">
											<td>&nbsp;&nbsp;<span class="searchText">Location</span></td>
											<td><span class="searchText">Date Installed</span></td>
											<td><span class="searchText">Proposed Activity</span></td>
											<td><span class="searchText">BMP/Sensitive Area</span></td>
											<td align="right"><span class="searchText">Edit/Delete</span></td>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%
										If rs.eof then%>
											<tr><td colspan="7">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td>			
														&nbsp;&nbsp;<%=handleApostropheDisplay(rs("location"))%>								
													</td>
													<td><%=rs("dateInstalled")%></td>
													<td><%=handleApostropheDisplay(rs("proposedActivity"))%></td>
													<td><%=rs("question")%></td>
													<td align="right"><a href="form.asp?divisionID=<%=divisionID%>&formType=editLocation&customerID=<%=customerID%>&projectID=<%=projectID%>&locationID=<%=rs("locationID")%>&path=<%=sPath%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("locationID")%>&processType=deleteLocation&customer=<%=customerID%>&projectID=<%=projectID%>&divisionID=<%=divisionID%>&path=<%=sPath%>')">&nbsp;</td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									<%end if%>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>