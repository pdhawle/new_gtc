<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
'dtFrom = request("fromDate")
'dtTo = request("toDate")

iMonth = request("month")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOpenLotsForMonthHC"
   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, iMonth)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Lot Status and Rate</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<!--<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expRecReportResults.asp?customer=<%'=iCustomerID%>&division=<%'=iDivisionID%>&projectID=<%'=iProjectID%>&fromDate=<%'=dtFrom%>&toDate=<%'=dtTo%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="chooseRecProj.asp" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>-->
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Customer</span></td>
											<td><span class="searchText">Project Name</span></td>
											<td><span class="searchText">Lot Number</span></td>
											<td><span class="searchText">Open Date</span></td>
											<td><span class="searchText">Close Date</span></td>
											<td><span class="searchText">Rate</span></td>
											<td><span class="searchText">Amount</span></td>
										</tr>
											<%If rsReport.eof then %>
												<tr><td></td><td colspan="7">there are no lots to display</td></tr>
											<%else
												dim bNew
												bNew = false
												oldCustID = ""
												custID = ""
												dim iGrandTot
												iGrandRate = 0
												
												blnChange = false
												Do until rsReport.eof																													
					
												'If rsReport("closeDate") >= iMonth then
												'if isNull(month(rsReport("closeDate"))) or  month(rsReport("closeDate")) <= iMonth then
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td><%'=rsReport("customerID")%>&nbsp;<%=rsReport("customerName")%></td>
														<td><%=rsReport("projectName")%></td>
														<td><%=rsReport("lotNumber")%></td>
														<td><%=rsReport("openDate")%></td>
														<td><%=rsReport("closeDate")%></td>
														<td><%=formatcurrency(rsReport("rate"),2)%></td>
														<td>
															<%
																if isNull(rsReport("closeDate")) then'rsReport("closeDate") = "" or
																	'if opened this month
																	if trim(month(rsReport("openDate"))) = trim(iMonth) then
																		numDays = 28 - day(rsReport("openDate"))
																		iRate = rsReport("rate") / 28 'based on rate / 28 days
																		iTotRate = formatcurrency(iRate * numDays,2)'numDays
																	
																		isubRate = iRate * numDays
																		'response.Write numDays
																	else
																		iTotRate = formatcurrency(rsReport("rate"),2)
																		isubRate = rsReport("rate")
																	end if
			
																else
																	'if closed in this month
																	numDays = day(rsReport("closeDate"))
																	iRate = rsReport("rate") / 28 'based on rate / 28 days
																	iTotRate = formatcurrency(iRate * numDays,2)'numDays
																	
																	isubRate = iRate * numDays
																end if
																
																response.Write iTotRate
																
																iSubTot = iSubTot + isubRate
																if isubRate <> "" then 
																	iGrandTot = iGrandTot + isubRate 
																end if
			
															%>
														
														</td>
													</tr>
												
												
													
													<%
													'response.Write "custID " & custID & "<br>"
													rsReport.movenext									
													
													'break the line and sub total each customer
													if oldCustID = custID then
														custID = trim(rsReport("customerID"))
													else
														oldCustID = custID
													end if	
													
													If oldCustID <> "" then
														if 	custID <> oldCustID or rsReport.eof then%>	
															<tr><td colspan="9"><img src="images/pix.gif" width="1" height="5"></td></tr>
															<tr>
																<td></td>
																<td colspan="7" align="right"><strong>SubTotal <%=formatcurrency(iSubTot,2)%></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
															</tr>
															<tr><td colspan="9"><img src="images/pix.gif" width="1" height="20"></td></tr>
									
														<%iSubTot = 0
														end if
													end if
													'end of subtotal
												
												
													if blnChange = true then
														blnChange = false
													else
														blnChange = true
													end if
			
												loop%>
											<tr><td colspan="9"><img src="images/pix.gif" width="1" height="15"></td></tr>
											<tr><td colspan="9" align="right"><strong>Grand Total: <%=formatcurrency(iGrandTot,2)%></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
										<%end if%>
											
									</table>
														
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>