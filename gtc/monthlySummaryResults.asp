<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iYear = request("year")
iMonth = request("month")
projectID = request("project")

'response.Write projectID
On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'If Session("LoggedIn") <> "True" Then
'	Response.Redirect("index.asp")
'End If


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerByProject"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsProject = oCmd.Execute
Set oCmd = nothing

'check to see if there are responsive actions for this report
columnName = "inspectionDate"
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOpenItemsByProjectSortMonth"
	.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	.parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
	.parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, iMonth)
   .CommandType = adCmdStoredProc
   
End With

			
Set rsProj = oCmd.Execute
Set oCmd = nothing

if not rsProj.eof then
	sDef = "No" 
else
	sDef = "Yes" 
end if
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<body>
<table width="650" cellpadding="0" cellspacing="0" border="1">
	<tr>
		<td colspan="8" align="center">
			<span class="Header">EROSION, SEDIMENTATION AND POLLUTION CONTROL<br>SITE INSPECTION MONTHLY SUMMARY REPORT</span><br>
			To be completed and submitted by the 10th day of each month
		</td>
	</tr>
	<tr>
		<td bgcolor="#EEEEEE" width="150">&nbsp;<strong>Owner</strong></td>
		<td width="200">&nbsp;<%=rsProject("customerName")%></td>
		<td width="35">&nbsp;</td>
		<td width="35">&nbsp;</td>
		<td width="35">&nbsp;</td>
		<td width="35">&nbsp;</td>
		<td width="35">&nbsp;</td>
		<td width="35">&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#EEEEEE">&nbsp;<strong>Operator</strong></td>
		<td>&nbsp;<%=rsProject("customerName")%></td>
		<td colspan="6" width="220">&nbsp;<strong>Office Use: Lot # or project code</strong></td>
	</tr>
	<tr>
		<td bgcolor="#EEEEEE"><strong>&nbsp;Project Location<br>&nbsp;(Lot # and/or Neighborhood)</strong></td>
		<td colspan="7">&nbsp;<%=rsProject("projectName")%></td>
	</tr>
	<tr>
		<td bgcolor="#EEEEEE"><strong>&nbsp;Reporting Period<br>&nbsp;(Month/Year)</strong></td>
		<td colspan="7">&nbsp;<%=getMonthName(iMonth) & " " & iYear%></td>
	</tr>
</table>
<table align="left" width="325" cellpadding="0" cellspacing="0" border="1">
	<tr>
		<td align="center">&nbsp;<strong>Day</strong>&nbsp;</td>
		<td align="center"><strong>Inspection Type<br>(Daily, Weekly, Rainfall, Monthly)</strong></td>
		<td align="center"><strong>Rainfall Amount</strong></td>
		<td align="center">&nbsp;<strong>Compliance?<br>Yes or No</strong>&nbsp;</td>
	</tr>
	<%
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetReportByProjectandDate"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	   .parameters.Append .CreateParameter("@iMonth", adInteger, adParamInput, 8, iMonth)
	   .parameters.Append .CreateParameter("@iYear", adInteger, adParamInput, 8, iYear)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsRep = oCmd.Execute
	Set oCmd = nothing
	
	Dim repArray(2,31)
	
	Do until rsRep.eof
		
		for i = 0 to 31
			If day(rsRep("inspectionDate")) = i then
				repArray(0,i) = rsRep("ReportType")
				repArray(1,i) = rsRep("rainfallAmount")
				if isNull(rsRep("NonComp")) then
					if rsRep("ReportType") <> "Daily" then
						repArray(2,i) = "Yes"
					end if
				Else
					repArray(2,i) = "No"
				End if
			end if
		next
		
	rsRep.movenext
	loop
	
	rsRep.close
	set rsRep = nothing
	
	'Create command to do the dailies
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetDailyReportByProjectandDate"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	   .parameters.Append .CreateParameter("@iMonth", adInteger, adParamInput, 8, iMonth)
	   .parameters.Append .CreateParameter("@iYear", adInteger, adParamInput, 8, iYear)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsRepDaily = oCmd.Execute
	Set oCmd = nothing
	
	Dim repDailyArray(2,31)
	
	Do until rsRepDaily.eof
		
		for i = 0 to 31
			'response.Write "ttt"
			If cint(rsRepDaily("dayOfMonth")) = i then
				repDailyArray(0,i) = rsRepDaily("ReportType")
				repDailyArray(1,i) = rsRepDaily("rainfallAmount")
				If rsRepDaily("compliant") = True then
					repDailyArray(2,i) = "Yes"
				else
					repDailyArray(2,i) = "No"''rsRepDaily("compliant")
				end if
				
			end if
			'response.Write "repd " & repDailyArray(2,i)
		next
		
	rsRepDaily.movenext
	loop
	
	rsRepDaily.close
	set rsRepDaily = nothing
	
	
	for i = 1 to 16							
	%>
	<tr>
		<td align="center"><%=i%></td>
		<td align="center">
			<%If repArray(0,i) <> "" then
				If repArray(0,i) <> "Daily" then
					response.Write repArray(0,i)
					bComma = true
				else
					response.Write "&nbsp;"
					bComma = false
				end if
			else
				response.Write "&nbsp;"
				bComma = false
			end if%>
			
			<%If repDailyArray(0,i) <> "" then
				if bComma = true then
					response.Write ", " & repDailyArray(0,i)
				else
					response.Write repDailyArray(0,i)
				end if
			else
				response.Write "&nbsp;"
			end if%>
		</td>
		<td align="center">
			<%If repArray(1,i) <> "" then
				response.Write "&nbsp;" & formatNumber(repArray(1,i),2)
				bRain = true
			else
				response.Write "&nbsp;"
				bRain = false
			end if%>
			
			<%If repDailyArray(1,i) <> "" then
				if bRain = false then
					response.Write formatNumber(repDailyArray(1,i),2)
				end if
			else
				response.Write "&nbsp;"
			end if%>
		</td>
		<td align="center">
			<%If repArray (2,i)<> "" then '
				response.Write sDef
				bComp = true
				
				if sDef = "No" then
					response.Write "," & sDef
				else
					If repDailyArray(2,i) <> "" then
						response.Write ", " & repDailyArray(2,i)
					else
						response.Write "&nbsp;"
					end if
				end if
				
			else
				If repDailyArray(2,i) <> "" then
					response.Write repDailyArray(2,i)
				else
					response.Write "&nbsp;"
				end if
			end if
			
			'response.Write repDailyArray(2,i)%>
		</td>
	</tr>
	<%next%>
</table>
<table width="325" cellpadding="0" cellspacing="0" border="1">
	<tr>
		<td align="center">&nbsp;<strong>Day</strong>&nbsp;</td>
		<td align="center"><strong>Inspection Type<br>(Daily, Weekly, Rainfall, Monthly)</strong></td>
		<td align="center"><strong>Rainfall Amount</strong></td>
		<td align="center">&nbsp;<strong>Compliance?<br>Yes or No</strong>&nbsp;</td>								
	</tr>
	<%i = 0
	for i = 17 to 31							
	%>
	<tr>
		<td align="center"><%=i%></td>
		<td align="center">
			<%If repArray(0,i) <> "" then
				If repArray(0,i) <> "Daily" then
					response.Write repArray(0,i)
					bComma = true
				else
					response.Write "&nbsp;"
					bComma = false
				end if
			else
				response.Write "&nbsp;"
				bComma = false
			end if%>
			
			<%If repDailyArray(0,i) <> "" then
				if bComma = true then
					response.Write ", " & repDailyArray(0,i)
				else
					response.Write repDailyArray(0,i)
				end if
			else
				response.Write "&nbsp;"
			end if%>
		</td>
		<td align="center">
			<%If repArray(1,i) <> "" then
				response.Write "&nbsp;" & formatNumber(repArray(1,i),2)
				bRain = true
			else
				response.Write "&nbsp;"
				bRain = false
			end if%>
			
			<%If repDailyArray(1,i) <> "" then
				if bRain = false then
					response.Write formatNumber(repDailyArray(1,i),2)
				end if
			else
				response.Write "&nbsp;"
			end if%>
		</td>
		<td align="center">
			<%'If repArray(2,i) <> "" then
				'response.Write repArray(2,i)
			'else
			'	If repDailyArray(2,i) <> "" then
			'		response.Write repDailyArray(2,i)
			'	else
			'		response.Write "&nbsp;"
			'	end if
			'end if
			
			If repArray(2,i) <> "" then '
				response.Write sDef
				bComp = true
				
				if sDef = "No" then
					response.Write "," & sDef
				else
					If repDailyArray(2,i) <> "" then
						response.Write ", " & repDailyArray(2,i)
					else
						response.Write "&nbsp;"
					end if
				end if
			else
				If repDailyArray(2,i) <> "" then
					response.Write repDailyArray(2,i)
				else
					response.Write "&nbsp;"
				end if
			end if
			
			'response.Write repDailyArray(2,i)
			%>

		</td>
	</tr><%next%>
	<tr>
		<td align="center">&nbsp;</td>
		<td align="center">&nbsp;</td>
		<td align="center">&nbsp;</td>
		<td align="center">&nbsp;</td>
	</tr>
</table><br>
<table width="650" cellpadding="0" cellspacing="0" border="1">						
	<tr>
		<td colspan="8" align="center">
			<strong>ATTACH COPIES OF INSPECTION REPORTS FOR DAYS ON WHICH MAINTENANCE ITEMS,
			<br>DEFICIENCIES OR VIOLATIONS EXIST.</strong>
		</td>
	</tr>
	<tr>
		<td colspan="8">
			<br>&nbsp;<strong>Check the appropriate box:</strong><br><br>
			&nbsp;<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td><img src="images/pix.gif" width="10" height="1"></td>
					<td valign="top">O&nbsp;&nbsp;</td>
					<td>
						I certify that the above project was in non-compliance on the dates noted above and is currently in 
						non-compliance with the Erosion, Sedimentation and Pollution Control Plan and the General NPDES 
						Permit No. GAR 1000003.
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td></td>
					<td valign="top">O&nbsp;&nbsp;</td>
					<td>
						I certify that the above project was in non-compliance on the dates noted above, but corrective actions 
						have been taken and the project is currently in compliance with the Erosion, Sedimentation and Pollution 
						Control Plan and the General NPDES Permit No. GAR 1000003.
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td></td>
					<td valign="top">O&nbsp;&nbsp;</td>
					<td>
						I certify that to the best of my knowledge that the above project has been in compliance throughout the 
						past month with the Erosion, Sedimentation and Pollution Control Plan and the General NPDES Permit No. GAR 1000003.<br><br>
					</td>
				</tr>
			</table>									
		</td>
	</tr>
	<tr>
		<td colspan="8">
			&nbsp;<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td><img src="images/pix.gif" width="10" height="1"></td>
					<td>
						I certify under penalty of law that this document and all attachments were prepared under my direction or 
						supervision in accordance with a system designed to assure that qualified personnel properly gathered and 
						evaluated the information submitted. Based on my inquiry of the person or persons who manage the 
						system, or those persons directly responsible for gathering the information, the information submitted is, to 
						the best of my knowledge and belief, true, accurate, and complete. I am aware that there are significant 
						penalties for submitting false information, including the possibility of fine and imprisonment for knowing 
						violations.
					</td>
					<td><img src="images/pix.gif" width="10" height="1"></td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1"></td>
					<td>
						__________________________________________<br>
						Full Legal Name
					</td>
					<td><img src="images/pix.gif" width="10" height="1"></td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1"></td>
					<td>
						__________________________________________<br>
						Signature
					</td>
					<td><img src="images/pix.gif" width="10" height="1"></td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1"></td>
					<td>
						__________________________________________<br>
						Printed Name
					</td>
					<td><img src="images/pix.gif" width="10" height="1"></td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1"></td>
					<td>
						__________________________________________<br>
						Title/Office
					</td>
					<td><img src="images/pix.gif" width="10" height="1"></td>
				</tr>
				<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1"></td>
					<td>
						__________________________________________<br>
						Date<br><br>
					</td>
					<td><img src="images/pix.gif" width="10" height="1"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
				

</body>
</html>
<%
rsProject.close
DataConn.close
Set rsProject = nothing
Set DataConn = nothing
Set oCmd = nothing


Function getMonthName(iMonth)
	Select Case iMonth
		Case 1
			getMonthName = "January"
		Case 2
			getMonthName = "February"
		Case 3
			getMonthName = "March"
		Case 4
			getMonthName = "April"
		Case 5
			getMonthName = "May"
		Case 6
			getMonthName = "June"
		Case 7
			getMonthName = "July"
		Case 8
			getMonthName = "August"
		Case 9
			getMonthName = "September"
		Case 10
			getMonthName = "October"
		Case 11
			getMonthName = "November"
		Case 12
			getMonthName = "December"
	end select
End function
%>
