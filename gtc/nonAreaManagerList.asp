<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
clientID = request("clientID")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If
On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")

With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomersNoAreaManager"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
		
Set rs = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Customer List (No area manager)</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr bgcolor="#666666">
											<td width="5"></td>
											<td width="20%"><span class="searchText">Customer Name</span></td>
											<td align="right"><span class="searchText">Active</span></td>
										</tr>
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="20">all customers have an area manager assigned</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
											
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td width="5"></td>
													<td><a href="form.asp?id=<%=rs("customerID")%>&formType=editCustomer" title="<%=rs("customerName")%> - Edit Customer" rel="gb_page_fs[]"><%=rs("customerName")%></a></td>
													<td align="right">
														<%if rs("isActive") = "True" then%>
															<img src="images/check.gif">&nbsp;&nbsp;&nbsp;
														<%end if%>
													</td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>