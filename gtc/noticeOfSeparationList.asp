<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
view = request("view")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
if view = "ar" then
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetArchivedNoticeOfSeparations"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
else	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetNoticeOfSeparations"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
end if
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Notice Of Separation List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addNoticeOfSeparation&clientID=<%=clientID%>" class="footerLink">add notice of separation</a>&nbsp;&nbsp;
						<%if view = "ar" then%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="noticeOfSeparationList.asp?clientID=<%=clientID%>" class="footerLink">view notice of separation list</a>&nbsp;&nbsp;
							
						<%else%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="noticeOfSeparationList.asp?clientID=<%=clientID%>&view=ar" class="footerLink">view archived notice of separation list</a>&nbsp;&nbsp;
						<%end if%>
						<!--<span class="footerLink">|</span>&nbsp;&nbsp;<a href="divisionList.asp?ID=<%'=customerID%>" class="footerLink">division list</a>	-->		
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">ReportID</span></td>
											<td><span class="searchText">Employee Name</span></td>
											<td><span class="searchText">Company Official</span></td>
											<td><span class="searchText">Date Created</span></td>		
											<td align="center"><span class="searchText">Action</span></td>
											<!--<td align="center"><span class="searchText">Delete</span></td>-->
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="10">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("noticeOfSeparationID")%></td>
													<td><%=rs("empName")%></td>
													<td><%=rs("compOfficial")%></td>
													<td><%=formatDateTime(rs("dateCompleted"),2)%></td>
													<td align="center">
														<a href="downloads/noticeOfSeparation_<%=rs("noticeOfSeparationID")%>.pdf" target="_blank">view</a>&nbsp;&nbsp;|&nbsp;
														<!--<a href="">delete</a>&nbsp;&nbsp;|&nbsp;-->
														<a href="process.asp?processType=archiveNoticeOfSeparation&clientID=<%=clientID%>&noticeOfSeparationID=<%=rs("noticeOfSeparationID")%>">archive</a><!--&nbsp;&nbsp;|&nbsp;
														<a href="">email</a>-->
													</td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>