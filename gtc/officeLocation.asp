<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->

<%
Dim rs, oCmd, DataConn, iLocation
iLocation = request("id")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOfficeLocation"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iLocation)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title>On-Demand NOI</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table class="borderTable" width="400" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td bgcolor="#FA702B"><img src="images/pix.gif" width="25" height="1"></td>
					<td bgcolor="#FA702B" valign="top">
						<%if Session("appLogo") <> "" then%>
							<img src="<%=Session("appLogo")%>" border="0"/>
						<%else%>
							<a href="http://next-sequence.com"><img src="images/logo.gif" width="110" height="100" border="0"/></a>
						<%end if%>
						<img src="images/sequence.gif" width="171" height="100" /><img src="images/pix.gif" width="151" height="1">
					</td>
				</tr>
				<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="20" height="1"></td>
		<td>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<span class="Header">GEORGIA EPD DISTRICT OFFICE</span><br><br>
						Please send to the following District office of EPD:

					</td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<%=rs("district")%><br>
						<%=rs("officeName")%><br>
						<%=rs("address1")%><br>
						<%=rs("city") & ", " & rs("state") & " " & rs("zip")%><br>
						<%=rs("phone")%><br>
					</td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td align="right">
						<a href="javascript: self.close()">close</a>
					</td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="10"></td></tr>
			</table>
		</td>
		<td><img src="images/pix.gif" width="20" height="1"></td>
	</tr>
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>
