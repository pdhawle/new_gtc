<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

clientID = session("clientID")
projectID = Request("projectID")
questionID = Request("questionID")
rte = request("rte")
ssort = request("sort")
if ssort = "" then
	columnName = "inspectionDate"
else
	columnName = ssort
end if

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

'response.Write projectID

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")

if questionID <> "" then
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetOpenItemsByQuestion"
		.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		.parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, questionID)
	   .CommandType = adCmdStoredProc
	   
	End With
	
else
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetOpenItemsByProjectSort"
		.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		.parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
	   .CommandType = adCmdStoredProc
	   
	End With

end if
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerByProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsInfo = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClient"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsClient = oCmd.Execute
Set oCmd = nothing

%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<script>
<!-- Begin
var checkflag = "false";
function check(field)
{
 var i;
 if (eval(field[0].checked))
 {
  for (i=0;i<field.length;i++)
    field[i].checked=true;
  LL(field); 
  return "Uncheck All";
 } 
 else
 { 
   for(i=0;i<field.length;i++)
     field[i].checked=false;
   UU(field); 
   return "Check All";
 } 
}
function LL(field){field.disabled=true;}
function UU(field){field.disabled=false;}
function submitOrder(_form)
{
  var _msg  = "None Clicked:\n";
      _msg += "Air, Monitoring, Water, Pollution Prevention checkboxes have ";
      _msg += "been deselected, please select 'All' checkbox.\n";
  var anyClicked=false;
  for(var i=0;i<_form.list.length;i++)
    if(_form.list[i].checked)
	{
	  anyClicked=true;
	  _msg += "true*\n";
    }
	else
	{
	  _msg += "false*\n";
	}
  if(!anyClicked)
    alert(_msg)
//
  var _msg2  = "All Clicked:\n";
      _msg2 += "Air, Monitoring, Water, Pollution Prevention checkboxes have ";
      _msg2 += "been deselected, please select 'All' checkbox.\n";
  var allClicked2=true;
  for(var i=0;i<_form.list.length;i++)
    if(!_form.list[i].checked)
	{
	  allClicked2=false;
	  _msg2 += "false*\n";
    }
	else
	{
	  _msg2 += "true*\n";
	}
  if(allClicked2)
    alert(_msg2)
}

function setDateClose(refID)
{
	var d = new Date();
	var curr_date = d.getDate();
	var curr_month = d.getMonth() + 1;
	var curr_year = d.getFullYear();
	//document.write(curr_date + "-" + curr_month + "-" + curr_year);

	document.getElementById('addressedByDate'+refID).value = curr_month + "/" + curr_date + "/" + curr_year;
}
//  End -->
</script>
</head>
<body>
<form name="openItems" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Open Items Punch List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="closedItems.asp?questionID=<%=questionID%>&projectID=<%=projectID%>&rte=<%=rte%>&clientID=<%=clientID%>" class="footerLink">corrected items log</a>&nbsp;&nbsp;
						<%if rte = "maint" then%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="chooseProj.asp" class="footerLink">back</a>&nbsp;&nbsp;
						<%else%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="javascript:window.close();" class="footerLink">close window</a>&nbsp;&nbsp;
						<%end if%>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="15">Customer: <strong><%=rsInfo("customerName")%></strong></td></tr>
										<tr><td colspan="15">Project: <strong><%=rsInfo("projectName")%></strong></td></tr>
										<tr><td colspan="15">% Complete Chart: <strong><a href="percentCompleteChart.asp?projectID=<%=projectID%>" rel="gb_page_fs[]">view chart</a></strong></td></tr>
										
										<tr><td colspan="15">Please Note: To sort by item number, click the column headers below.</td></tr>
										<tr><td colspan="15"><img src="images/pix.gif" width="1" height="3"></td></tr>
										<tr bgcolor="#666666" height="35">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText"><a href="openItems.asp?sort=reportID&projectID=<%=projectID%>&questionID=<%=questionID%>&rte=<%=rte%>" style="color:#721A32;text-decoration:underline;font-weight:bold;">ReportID</a></span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText"><a href="openItems.asp?sort=inspectionDate&projectID=<%=projectID%>&questionID=<%=questionID%>&rte=<%=rte%>" style="color:#721A32;text-decoration:underline;font-weight:bold;">Report Date</a></span><br><img src="images/pix.gif" width="60" height="1"></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText"><a href="openItems.asp?sort=referenceNumber&projectID=<%=projectID%>&questionID=<%=questionID%>&rte=<%=rte%>" style="color:#721A32;text-decoration:underline;font-weight:bold;">Ref# (sort)</a></span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText"><a href="openItems.asp?sort=actionNeeded&projectID=<%=projectID%>&questionID=<%=questionID%>&rte=<%=rte%>" style="color:#721A32;text-decoration:underline;font-weight:bold;">Action Needed</a></span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Location</span></td>
											<td><span class="searchText">Corrected</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Date Corrected</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Corrected By</span></td>
										</tr>
										<tr><td colspan="15"></td><td><img src="images/pix.gif" width="1" height="10"><!--<INPUT type="checkbox" name="closeItem"  value="Check All" onClick="this.value=check(this.form.closeItem);"> check all--></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="15">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><a href="downloads/swsir_<%=rs("reportID")%>.pdf" target="_blank"><%=rs("reportID")%></a></td>
													<td></td>
													<td><%=rs("inspectionDate")%></td>
													<td></td>
													<td><%=rs("reportID") & "-" & rs("referenceNumber")%></td>
													<td></td>
													<td><%=rs("actionNeeded")%></td>
													<td></td>
													<td><%=rs("location")%></td>
													<td><input type="checkbox" name="closeItem" value="<%=rs("responsiveActionID")%>" onClick="setDateClose(<%=rs("responsiveActionID")%>)"></td>
													<td></td>
													<td width="100"><input type="text" name="addressedByDate<%=rs("responsiveActionID")%>" maxlength="10" size="8"/>&nbsp;<a href="javascript:displayDatePicker('addressedByDate<%=rs("responsiveActionID")%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													<td></td>
													<td>
														<%
														'Create command for contacts
														'brings in the contacts from the project section where they are set up
														Set oCmd = Server.CreateObject("ADODB.Command")
														
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetContactsByProject"
															.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
														   .CommandType = adCmdStoredProc
														   
														End With
														
																	
														Set rsContact = oCmd.Execute
														Set oCmd = nothing
														%>
														<select name="addressedBy<%=rs("responsiveActionID")%>">
																<option value="<%=session("Name")%>"><%=session("Name")%></option>
															<%do until rsContact.eof%>										
																<option value="<%=rsContact("firstName") & " " & rsContact("lastName")%>"><%=rsContact("firstName") & " " & rsContact("lastName")%></option>
															<%rsContact.movenext
															loop%>
														</select>
													</td>
												</tr>
												<%If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td colspan="20" align="right">
														<strong>Action Item Comments:</strong>&nbsp;<input type="text" size="50" name="closeComments<%=rs("responsiveActionID")%>" maxlength="200" value="<%=rs("comments")%>">&nbsp;&nbsp;
													</td>
												</tr>
												
												<%
												If rsClient("attachPhotos") = "True" then
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td colspan="15"><br>
															<strong>Uploaded images:</strong><br>
															<%'this is where we are listing the images
						
															'Dim dirname, mypath, fso, folder, filez, FileCount
															Set fso = CreateObject("Scripting.FileSystemObject")
															
															
															sPath = "downloads/project_" & projectID
															If not fso.FolderExists(server.mappath(sPath)) Then 
																'response.Write server.mappath(sPath)
																fso.CreateFolder(server.mappath(sPath))
															end if
															
															sPath = "downloads/project_" & projectID & "/report_" & rs("reportID")
															If not fso.FolderExists(server.mappath(sPath)) Then 
																'response.Write server.mappath(sPath)
																fso.CreateFolder(server.mappath(sPath))
															end if
															
															sPath = "downloads/project_" & projectID & "/report_" & rs("reportID") & "/responsiveAction_" & rs("responsiveActionID")
															If not fso.FolderExists(server.mappath(sPath)) Then 
																'response.Write server.mappath(sPath)
																fso.CreateFolder(server.mappath(sPath))
															end if
															
											
															Set folder = fso.GetFolder(server.mappath(sPath))
															'Response.Write folder
															Set filez = folder.Files			
															'FileCount = folder.Files.Count
															%>
															<%' Now To the Runtime code:
															Dim strPath 'Path of directory to show
															Dim objFSO 'FileSystemObject variable
															Dim objFolder 'Folder variable
															Dim objItem 'Variable used To Loop through the contents of the folder
															strPath = sPath & "/"
															
															'response.Write strPath & "<br>"
															'response.Write strPath
															' Create our FSO
															Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
															' Get a handle on our folder
											
															Set objFolder = objFSO.GetFolder(Server.MapPath(strPath))
															%>
															<table>
															<%i = 0
															For Each objItem In objFolder.Files
															'replaceText()%>
																<tr>
																	<!--<td><input type="image" src="images/remove.gif" width="11" height="13" border="0" alt="Delete" onClick="return confirmDelete('filedelete.asp?file=<%'=objItem.Name%>&projectID=<%'=rsProject("projectID")%>&reportID=<%'=reportID%>&responsiveActionID=<%'=rs("responsiveActionID")%>')"></td>-->
																	<td valign="top"><%response.Write "&nbsp;<a href=" & strPath&objItem.Name & " target=_blank>" & objItem.Name & "</a><br>"%></td>																	
																</tr>
															<%i=i+1
															NEXT%>
															</table>
															<%
															
															'response.Write i & "<br>"
															
															If i=0 then
																response.Write "there are no images attached to this responsive action"
															end if
															
															Set objItem = Nothing
															Set objFolder = Nothing
															Set objFSO = Nothing
															i=0
															%><!--<br>
															there are no images attached to this responsive action--><br><br>
														</td>
													</tr>
												<%end if%>
											<%
											'get the values from all the checkboxes
											sAllVal = sAllVal & rs("responsiveActionID")
											
											rs.movenext
											
											if not rs.eof then
												sAllVal = sAllVal & ","
											end if
											
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											
											
											
											loop
										end if%>
										<tr>
											<td colspan="17" align="right">
												<input type="hidden" name="projectID" value="<%=projectID%>">
												<input type="hidden" name="questionID" value="<%=questionID%>">
												<input type="hidden" name="allBoxes" value="<%=sAllVal%>">
												<input type="hidden" name="processType" value="closeItems" />
												<br><input type="submit" value="Update" class="formButton"/>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>