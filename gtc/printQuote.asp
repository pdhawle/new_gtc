<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

quoteID = request("quoteID")
customerQuoteID = request("customerQuoteID")
userID = request("userID")

Session("LoggedIn") = "True"
Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetQuoteforPDF"
    .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
	.parameters.Append .CreateParameter("@customerQuoteID", adInteger, adParamInput, 8, customerQuoteID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsQuote = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
    .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsUser = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<style>
	body, table, td, span, div, p {
	font-family: Arial, Helvetica, sans-serif;
	font-size:12px;
}
</style>
<body>
<table class="borderTable" width="600" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr bgcolor="#FFFFFF">
					<td width="7"></td>
					<td>
						<img src="images/quoteLogo.gif" /><br>
						1255 Lakes Parkway, Suite 385<br>
						Lawrenceville, GA 30043<br>
						(P) 678-336-1357 &bull; (F) 678-336-1358
					</td>
					<td align="right">
						<img src="images/logoPrint.gif" />
					</td>
				</tr>
				<tr bgcolor="#FFFFFF"><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left">
									<span class="Header">Project Name: <%=rsQuote("projectName")%></span><br><br>
									<table cellpadding="0" cellspacing="0">
										<tr><td><strong>Location:</strong></td><td>&nbsp;<%=rsQuote("projectAddress")%></td></tr>
										<tr><td><strong>County:</strong></td><td>&nbsp;<%=rsQuote("countyName")%></td></tr>
										<tr><td><strong>City:</strong></td><td>&nbsp;<%=rsQuote("projectCity")%></td></tr>
										<tr><td><strong>State:</strong></td><td>&nbsp;<%=rsQuote("projectState")%></td></tr>
										<tr><td><strong>Zip:</strong></td><td>&nbsp;<%=rsQuote("projectZip")%></td></tr>
									</table><br>
									
									<strong>Quote: NPDES Inspections and Water Quality Monitoring</strong><br>
									<strong>Bid Date: <%=formatDateTime(rsQuote("bidDate"),2)%></strong>
								</td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				
				<!--this is the nav bar for the sub pages-->
				<tr bgcolor="#666666">
					<td colspan="5" height="5">
					</td>
				</tr>
				<!--end of nav bars for the sub pages-->
				
				<tr>
					<td colspan="5">
						<table width="100%">
							<tr>								
								<td valign="middle" align="left">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top">
												<strong><%=rsQuote("customerName")%></strong><br>
												<%=rsQuote("address")%><br>
												<%=rsQuote("city")%>,&nbsp;<%=rsQuote("state")%>&nbsp;<%=rsQuote("zip")%>
											</td>
											<td>
												<table cellpadding="0" cellspacing="0">
													<tr><td><strong>Contact: </strong></td><td>&nbsp;<%=rsQuote("contactName")%></td></tr>
													<tr><td><strong>Phone: </strong></td><td>&nbsp;<%=rsQuote("contactPhone")%></td></tr>
													<tr><td><strong>Fax: </strong></td><td>&nbsp;<%=rsQuote("contactFax")%></td></tr>
													<tr><td><strong>Email: </strong></td><td>&nbsp;<%=rsQuote("contactEmail")%></td></tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>						
					</td>
				</tr>				
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<table width="600" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<%
										cEach = 0
										cMonthly = 0
										cOneTime = 0
										cQuarterly = 0
										%>	
										<tr bgcolor="#666666">
											<td></td>
											<td align="center"><span class="searchText">One Time Fee</span></td>
											<td align="center"><span class="searchText">Monthly</span></td>
											<td align="center"><span class="searchText">Each</span></td>
											<!--<td align="center"><span class="searchText">Quarterly</span></td>-->
										</tr>
										<tr>
											<td colspan="4">&nbsp;<strong><u>Inspections</u></strong></td>
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>7 Day Inspection</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("cqwFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("cqwRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("cqwRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("cqwFreq") = "Each" then
													response.Write formatCurrency(rsQuote("cqwRate"),2)
													cEach = formatcurrency(cEach + rsQuote("cqwRate"),2)
												end if%>&nbsp;
											</td>
											<!--<td></td>-->
										</tr>
										<tr>
											<td>&nbsp;<strong>Post-Rain Inspection</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("cqprFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("cqprRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("cqprRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("cqprFreq") = "Each" then
													response.Write formatCurrency(rsQuote("cqprRate"),2)
													cEach = formatcurrency(cEach + rsQuote("cqprRate"),2)
												end if%>&nbsp;
											</td>
											<!--<td></td>-->
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>7 Day and Post Rain Inspection</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("cqwprFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("cqwprRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("cqwprRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("cqwprFreq") = "Each" then
													response.Write formatCurrency(rsQuote("cqwprRate"),2)
													cEach = formatcurrency(cEach + rsQuote("cqwprRate"),2)
												end if%>&nbsp;
											</td>
											<!--<td></td>-->
										</tr>
										<tr>
											<td>&nbsp;<strong>7 Day, Post Rain and Daily Inspection</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("cqwprdFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("cqwprdRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("cqwprdRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("cqwprdFreq") = "Each" then
													response.Write formatCurrency(rsQuote("cqwprdRate"),2)
													cEach = formatcurrency(cEach + rsQuote("cqwprdRate"),2)
												end if%>&nbsp;
											</td>
											<!--<td></td>-->
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>Daily Rain, Petroleum and CO Pad Insp.</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("cqdFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("cqdRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("cqdRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("cqdFreq") = "Each" then
													response.Write formatCurrency(rsQuote("cqdRate"),2)
													cEach = formatcurrency(cEach + rsQuote("cqdRate"),2)
												end if%>&nbsp;
											</td>
											<!--<td></td>-->
										</tr>
										<tr>
											<td colspan="5">&nbsp;<strong>Daily Rain, Petroleum and CO Pad Log - included</strong></td>
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>14 Day/Post Rain Inspection</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("cqfourteenwprFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("cqfourteenwprRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("cqfourteenwprRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("cqfourteenwprFreq") = "Each" then
													response.Write formatCurrency(rsQuote("cqfourteenwprRate"),2)
													cEach = formatcurrency(cEach + rsQuote("cqfourteenwprRate"),2)
												end if%>&nbsp;
											</td>
											<!--<td></td>-->
										</tr>
										<tr><td colspan="6" height="10"></td>
										<tr>
											<td colspan="5">&nbsp;<strong><u>Water Sampling</u></strong></td>
										</tr>
										<tr class="rowColor">
											<td colspan="5">
												&nbsp;<strong>2 required sample events up to 4 sample points each - included</strong><br>
												&nbsp;<strong>(after clearing and grubbing/after mass grading)</strong>
											</td>
										</tr>
										<tr>
											<td>&nbsp;<strong>*Additional required or elective sampling and analysis</strong></td>
											<td></td>
											<td align="center"></td>
											<td align="center">
												<%response.Write formatCurrency(rsQuote("cqwaterSampRate"),2)
												cEach = formatcurrency(cEach + rsQuote("cqwaterSampRate"),2)%>&nbsp;
												
											</td>
											<!--<td></td>-->
										</tr>
										<tr><td colspan="5"><em>***Note*** non-compliance or cited violation requires samples until compliant</em></td>
										<tr><td colspan="5" height="10"></td>
										<tr>
											<td colspan="5">&nbsp;<strong><u>Site Setup</u></strong></td>
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>Activation</strong></td>
											<td align="center">
												<%response.Write formatCurrency(rsQuote("cqsiteActivationFee"),2)
												cOneTime = formatcurrency(cOneTime + rsQuote("cqsiteActivationFee"),2)
												%>&nbsp;</td>
											<td align="center"></td>
											<td align="center"></td>
											<!--<td></td>-->
										</tr>
										<tr><td colspan="5"><em>includes job box and installation, Sequence user license, field records manual, rain gauge, document review, and <strong>3 year records retention</strong><br><br>There may be additional charges for regularly faxed documentation - billed monthly.</em></td>
										<tr><td colspan="5" height="10"></td>
										<tr bgcolor="#666666">
											<td align="right"><span class="searchText">Totals:</span>&nbsp;&nbsp;</td>
											<td align="center"><span class="searchText"><%=formatCurrency(cOneTime,2)%>&nbsp;</span></td>
											<td align="center"><span class="searchText"><%=formatCurrency(cMonthly,2)%></span></td>
											<td align="center"><span class="searchText"><%=formatCurrency(cEach,2)%>&nbsp;</span></td>
											<!--<td align="center"><span class="searchText"><%'=formatCurrency(cQuarterly,2)%>&nbsp;</span></td>-->
										</tr>
									</table>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5" height="10"></td></tr>
				<tr>
					<td colspan="6">
						<strong>Billing Terms</strong><br>
						A 1.5% monthly finance charge may be added to invoices over 30 days old. Payment due upon receipt of invoice.<br><br>
						
						<strong>Cancellation Policy</strong><br>
						HB/S NEXT must receive in writing project name and cancellation date at least 15 days prior to site deactivation. 
						Failure to provide written notice may incur additional charges.<br><br>
					
					
						By signing below you agree to the terms and conditions outlined in this proposal.<br><br>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									____________________________<br>
									<strong><%=rsQuote("customerName")%></strong>
								</td>
								<td width="50"></td>
								<td>
									____________________________<br>
									<strong>Date</strong>
								</td>
							</tr>
						</table>
						
					</td>
				</tr>
				<tr><td colspan="5" height="5"></td></tr>
				<tr>
					<td colspan="5">
						<table width="100%">
							<tr>								
								<td valign="middle" align="left">
									Thanks for the opportunity to price this project.<br><br>
									<%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%><br>
									<%=rsUser("cellPhone")%><br>
									<%=rsUser("Email")%><br><br>
									<span class="footer">Quote ID: <%=rsQuote("quoteID")%></span>
								</td>
							</tr>
						</table>						
					</td>
				</tr>	
				
				
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="50"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="images/quoteFooterBW.gif" width="615" height="29">
		</td>
	</tr>
	</table>
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>