<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
'Response.Buffer = False
server.ScriptTimeout = 2000
dim sType, rs
Dim arrX
Dim arrY
Dim arrText
Dim pdf
Dim Doc
sType = Request("processType")

select case stype

	Case "testemail"
		checkInitialInspection 37
		
	Case "addInitialBMP"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = null'request("contactNumber")
		inspectionType = null'request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = null'request("weatherType")
		comments = null'request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		rainfallAmount = null'request("rainfallAmount")
		
		addToJournal = null'request("addToJournal")
		
		soilStabilizationComments = null'checknull(request("soilStabilizationComments"))
		
		'check to see if initial report has been done on this project
		'checkInitialInspection project
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")

			
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if
		
		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
		
		If Err Then
		%>
		1	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
		2	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
		3	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		siteInspected = null'checknull(request("siteInspected"))
		perimControlBMP = checknull(request("perimControlBMP"))
		perimControlBMPInstallDate = checknull(request("perimControlBMPInstallDate"))
		initialSedimentStorage = checknull(request("initialSedimentStorage"))
		initialSedimentStorageInstallDate = checknull(request("initialSedimentStorageInstallDate"))
		sedimentStoragePerimControlBMP = checknull(request("sedimentStoragePerimControlBMP"))
		sedimentStoragePerimControlBMPInstallDate = checknull(request("sedimentStoragePerimControlBMPInstallDate"))
		allSedimentBasins = checknull(request("allSedimentBasins"))
		allSedimentBasinsInstallDate = checknull(request("allSedimentBasinsInstallDate"))
		bmpInstalledmaintained = checknull(request("bmpInstalledmaintained"))
		bmpInstalledmaintainedComments = checknull(request("bmpInstalledmaintainedComments"))
		actionItems = checknull(request("actionItems"))
		actionItemComments = checknull(request("actionItemComments"))
		inspectionOccurred7 = checknull(request("inspectionOccurred7"))

		
		Set oCmd2 = Server.CreateObject("ADODB.Command")
		With oCmd2
		   .ActiveConnection = DataConn
		   .CommandText = "spAddInitialBMP"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@siteInspected", adVarChar, adParamInput, 50, siteInspected) 
		   .parameters.Append .CreateParameter("@perimControlBMP", adBoolean, adParamInput, 1, isOn(perimControlBMP))
		   .parameters.Append .CreateParameter("@perimControlBMPInstallDate", adDBTimeStamp, adParamInput, 8, perimControlBMPInstallDate)
		   .parameters.Append .CreateParameter("@initialSedimentStorage", adBoolean, adParamInput, 1, isOn(initialSedimentStorage))
		   .parameters.Append .CreateParameter("@initialSedimentStorageInstallDate", adDBTimeStamp, adParamInput, 8, initialSedimentStorageInstallDate)
		   .parameters.Append .CreateParameter("@sedimentStoragePerimControlBMP", adBoolean, adParamInput, 1, isOn(sedimentStoragePerimControlBMP))
		   .parameters.Append .CreateParameter("@sedimentStoragePerimControlBMPInstallDate", adDBTimeStamp, adParamInput, 8, sedimentStoragePerimControlBMPInstallDate)
		   .parameters.Append .CreateParameter("@allSedimentBasins", adBoolean, adParamInput, 1, isOn(allSedimentBasins))
		   .parameters.Append .CreateParameter("@allSedimentBasinsInstallDate", adDBTimeStamp, adParamInput, 8, allSedimentBasinsInstallDate)
		   .parameters.Append .CreateParameter("@bmpInstalledmaintained", adBoolean, adParamInput, 1, isOn(bmpInstalledmaintained))
		   .parameters.Append .CreateParameter("@bmpInstalledmaintainedComments", adVarChar, adParamInput, 8000, bmpInstalledmaintainedComments)
		   .parameters.Append .CreateParameter("@actionItems", adBoolean, adParamInput, 1, isOn(actionItems))
		   .parameters.Append .CreateParameter("@actionItemComments", adVarChar, adParamInput, 8000, actionItemComments)
		   .parameters.Append .CreateParameter("@inspectionOccurred7", adBoolean, adParamInput, 1, isOn(inspectionOccurred7))
 
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd2.Execute
		
		
			
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if

		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPDFInitialBMP.asp?reportID=" & reportID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		Set Pdf = Nothing

		'send auto email if true for project
'		If autoEmail(project) = true then
			'response.Write reportID
'			sendAutoEmail reportID,project,division
'		end if

		response.Redirect "reportList.asp?findReport=" & reportID
	
		
	case "flagReport"
	
		fType = request("fType")
		
		if fType = "Flag" then
			blnFlag = true
		else
			blnFlag = false
		end if
		
		reportID = request("reportID")
		
		divisionID = request("divisionID")
		projectID = request("projectID")
		userID = request("userID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spFlagReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@isFlagged", adBoolean, adParamInput, 1, blnFlag)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		if divisionID <> "" then
			response.Redirect "reportList.asp?division=" & divisionID & "&project=" & projectID
		else
			response.Redirect "reportList.asp?findReport=" & reportID
		end if
		
	Case "addCrossingData"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = null'request("contactNumber")
		inspectionType = null'request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = null'request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		rainfallAmount = null'request("rainfallAmount")
		
		addToJournal = request("addToJournal")
		
		soilStabilizationComments = null'checknull(request("soilStabilizationComments"))
		
		'check to see if initial report has been done on this project
		'checkInitialInspection project
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")

			
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if
		
		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
		
		If Err Then
		%>
		1	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
		2	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
		3	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		crossingID = checknull(request("crossingID"))
		iFindSign = InStr(crossingID,"$")
		crossingID = left(crossingID,iFindSign - 1)
		
		
		lengthOfCrossing = checknull(request("lengthOfCrossing"))
		widthOfCrossing = checknull(request("widthOfCrossing"))
		pipeLength = checknull(request("pipeLength"))
		outletProtectionLength = checknull(request("outletProtectionLength"))
		outletProtectionAboveGrade = checknull(request("outletProtectionAboveGrade"))
		inletProtectionLength = checknull(request("inletProtectionLength"))
		inletProtectionAboveGrade = checknull(request("inletProtectionAboveGrade"))
		pipeEmbedded = checknull(request("pipeEmbedded"))
		rockCrossingAtGrade = checknull(request("rockCrossingAtGrade"))
		rockCrossingDepthAboveGrade = checknull(request("rockCrossingDepthAboveGrade"))
		geotextileUnderliner = checknull(request("geotextileUnderliner"))
		rockWashedDownstream = checknull(request("rockWashedDownstream"))
		geoweb = checknull(request("geoweb"))
		issuesComments = checknull(request("issuesComments"))
		
		typeOfCrossing = checknull(request("typeOfCrossing"))
		otherTypeOfCrossing = checknull(request("otherTypeOfCrossing"))
		
		

		
		Set oCmd2 = Server.CreateObject("ADODB.Command")
		With oCmd2
		   .ActiveConnection = DataConn
		   .CommandText = "spAddCrossingData"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@crossingID", adVarChar, adParamInput, 500, crossingID) 
		   .parameters.Append .CreateParameter("@lengthOfCrossing", adVarChar, adParamInput, 50, lengthOfCrossing) 
		   .parameters.Append .CreateParameter("@widthOfCrossing", adVarChar, adParamInput, 50, widthOfCrossing) 
		   .parameters.Append .CreateParameter("@pipeLength", adVarChar, adParamInput, 50, pipeLength) 
		   .parameters.Append .CreateParameter("@outletProtectionLength", adVarChar, adParamInput, 50, outletProtectionLength) 
		   .parameters.Append .CreateParameter("@outletProtectionAboveGrade", adVarChar, adParamInput, 50, outletProtectionAboveGrade) 
		   .parameters.Append .CreateParameter("@inletProtectionLength", adVarChar, adParamInput, 50, inletProtectionLength) 
		   .parameters.Append .CreateParameter("@inletProtectionAboveGrade", adVarChar, adParamInput, 50, inletProtectionAboveGrade) 
		   .parameters.Append .CreateParameter("@pipeEmbedded", adVarChar, adParamInput, 50, pipeEmbedded) 
		   .parameters.Append .CreateParameter("@rockCrossingAtGrade", adVarChar, adParamInput, 50, rockCrossingAtGrade)
		   .parameters.Append .CreateParameter("@rockCrossingDepthAboveGrade", adVarChar, adParamInput, 50, rockCrossingDepthAboveGrade) 
		   .parameters.Append .CreateParameter("@geotextileUnderliner", adVarChar, adParamInput, 50, geotextileUnderliner) 
		   .parameters.Append .CreateParameter("@rockWashedDownstream", adVarChar, adParamInput, 50, rockWashedDownstream) 
		   .parameters.Append .CreateParameter("@geoweb", adVarChar, adParamInput, 50, geoweb)
		   .parameters.Append .CreateParameter("@issuesComments", adVarChar, adParamInput, 2500, issuesComments) 
		   .parameters.Append .CreateParameter("@typeOfCrossing", adVarChar, adParamInput, 50, typeOfCrossing) 
		   .parameters.Append .CreateParameter("@otherTypeOfCrossing", adVarChar, adParamInput, 50, otherTypeOfCrossing) 
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd2.Execute
		
		
		'update the location from the date install
		dateInstalled = checknull(request("dateInstalled"))
		crossingID = checknull(request("crossingID"))
		iLen = len(crossingID)
		iFindSign = InStr(crossingID,"$")
		crossingID = right(crossingID,iLen - iFindSign)
		
		Set oCmd3 = Server.CreateObject("ADODB.Command")
		With oCmd3
		   .ActiveConnection = DataConn
		   .CommandText = "spEditLocationDateInstalled"
		   .parameters.Append .CreateParameter("@locationID", adInteger, adParamInput, 8, crossingID) 
		   .parameters.Append .CreateParameter("@dateInstalled", adDBTimeStamp, adParamInput, 8, dateInstalled)
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs2 = oCmd3.Execute
		
		
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if

		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPDFCrossingData.asp?reportID=" & reportID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		Set Pdf = Nothing

		'send auto email if true for project
'		If autoEmail(project) = true then
			'response.Write reportID
'			sendAutoEmail reportID,project,division
'		end if

		response.Redirect "reportList.asp?findReport=" & reportID
	
		
	Case "editMonthly"
	
		'report header
		reportID = request("reportID")
		comments = request("comments")
		division = request("division")
		inspectionDate = checknull(request("inspectionDate"))
		project = request("project")
		
				
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditDaily"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		

		'add the data to the monthly rain report table
		
		tlNumConstructionDays = checknull(request("tlNumConstructionDays"))
		tlDailyReportsAllDays = checknull(request("tlDailyReportsAllDays"))
		ssNumConstructionDays = null'checknull(request("ssNumConstructionDays"))
		ssDailyReportsAllDays = null'checknull(request("ssDailyReportsAllDays"))
		complianceGTCPolicy = checknull(request("complianceGTCPolicy"))
		complianceGTCPolicyWhyNot = checknull(request("complianceGTCPolicyWhyNot"))
		inspectionOccurEvery14Days = checknull(request("inspectionOccurEvery14Days"))
		inspectionOccurEvery14DaysWhyNot = checknull(request("inspectionOccurEvery14DaysWhyNot"))
		inspectionOccur24HourRain = checknull(request("inspectionOccur24HourRain"))
		inspectionOccur24HourRainWhyNot = checknull(request("inspectionOccur24HourRainWhyNot"))
		bmpComplianceGTCPolicy = checknull(request("bmpComplianceGTCPolicy"))
		bmpComplianceGTCPolicyWhyNot = checknull(request("bmpComplianceGTCPolicyWhyNot"))		
		samplesCollectedInMonth = checknull(request("samplesCollectedInMonth"))
		exceedNTULimit = checknull(request("exceedNTULimit"))
		waterQualityMonitoringCompliance = checknull(request("waterQualityMonitoringCompliance"))
		waterQualityMonitoringComplianceWhyNot = checknull(request("waterQualityMonitoringComplianceWhyNot"))
		hobo = checknull(request("hobo"))
		dataTable = checknull(request("dataTable"))
		rainfallCollectedDaily = checknull(request("rainfallCollectedDaily"))
		BMPInstalledMaintRetrieved = checknull(request("BMPInstalledMaintRetrieved"))
		projectDrainUpstream = checknull(request("projectDrainUpstream"))
		projectDrainUpstreamExplain = checknull(request("projectDrainUpstreamExplain"))
		projectDrainUpstreamYesOutlined = checknull(request("projectDrainUpstreamYesOutlined"))
		
		siteStabilizationDate = checknull(request("siteStabilizationDate"))
		
		perimControlBMPInstallDate = checknull(request("perimControlBMPInstallDate"))
		initSedStorageInstallDate = checknull(request("initSedStorageInstallDate"))
		sedStorageBMPInstallDate = checknull(request("sedStorageBMPInstallDate"))
		
		monthlyInspectionOccur = checknull(request("monthlyInspectionOccur"))
		stabilizationAchieved = checknull(request("stabilizationAchieved"))
		explainStabilizationAchieved = checknull(request("explainStabilizationAchieved"))
		
		landDisturbanceWeekendHoliday = checknull(request("landDisturbanceWeekendHoliday"))
		landDisturbanceWeekendHolidayListDates = checknull(request("landDisturbanceWeekendHolidayListDates"))
		
		
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditMonthlyRainReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@tlNumConstructionDays", adVarChar, adParamInput, 50, tlNumConstructionDays)
		   .parameters.Append .CreateParameter("@tlDailyReportsAllDays", adVarChar, adParamInput, 50, tlDailyReportsAllDays)
		   .parameters.Append .CreateParameter("@ssNumConstructionDays", adVarChar, adParamInput, 50, ssNumConstructionDays)
		   .parameters.Append .CreateParameter("@ssDailyReportsAllDays", adVarChar, adParamInput, 50, ssDailyReportsAllDays)
		   .parameters.Append .CreateParameter("@complianceGTCPolicy", adVarChar, adParamInput, 50, complianceGTCPolicy)
		   .parameters.Append .CreateParameter("@complianceGTCPolicyWhyNot", adVarChar, adParamInput, 2500, complianceGTCPolicyWhyNot)
		   .parameters.Append .CreateParameter("@inspectionOccurEvery14Days", adVarChar, adParamInput, 50, inspectionOccurEvery14Days)
		   .parameters.Append .CreateParameter("@inspectionOccurEvery14DaysWhyNot", adVarChar, adParamInput, 2500, inspectionOccurEvery14DaysWhyNot)
		   .parameters.Append .CreateParameter("@inspectionOccur24HourRain", adVarChar, adParamInput, 50, inspectionOccur24HourRain)
		   .parameters.Append .CreateParameter("@inspectionOccur24HourRainWhyNot", adVarChar, adParamInput, 2500, inspectionOccur24HourRainWhyNot)
		   .parameters.Append .CreateParameter("@bmpComplianceGTCPolicy", adVarChar, adParamInput, 50, bmpComplianceGTCPolicy)
		   .parameters.Append .CreateParameter("@bmpComplianceGTCPolicyWhyNot", adVarChar, adParamInput, 2500, bmpComplianceGTCPolicyWhyNot)
		   .parameters.Append .CreateParameter("@samplesCollectedInMonth", adVarChar, adParamInput, 50, samplesCollectedInMonth)
		   .parameters.Append .CreateParameter("@exceedNTULimit", adVarChar, adParamInput, 50, exceedNTULimit)
		   .parameters.Append .CreateParameter("@waterQualityMonitoringCompliance", adVarChar, adParamInput, 50, waterQualityMonitoringCompliance)
		   .parameters.Append .CreateParameter("@waterQualityMonitoringComplianceWhyNot", adVarChar, adParamInput, 2500, waterQualityMonitoringComplianceWhyNot)
		   .parameters.Append .CreateParameter("@hobo", adVarChar, adParamInput, 50, hobo)
		   .parameters.Append .CreateParameter("@dataTable", adVarChar, adParamInput, 50, dataTable)
		   .parameters.Append .CreateParameter("@rainfallCollectedDaily", adVarChar, adParamInput, 50, rainfallCollectedDaily)
		   .parameters.Append .CreateParameter("@BMPInstalledMaintRetrieved", adVarChar, adParamInput, 50, BMPInstalledMaintRetrieved)
		   .parameters.Append .CreateParameter("@projectDrainUpstream", adVarChar, adParamInput, 50, projectDrainUpstream)
		   .parameters.Append .CreateParameter("@projectDrainUpstreamExplain", adVarChar, adParamInput, 2500, projectDrainUpstreamExplain)
		   .parameters.Append .CreateParameter("@projectDrainUpstreamYesOutlined", adVarChar, adParamInput, 50, projectDrainUpstreamYesOutlined)
		   .parameters.Append .CreateParameter("@siteStabilizationDate", adDBTimeStamp, adParamInput, 8, siteStabilizationDate)
		   .parameters.Append .CreateParameter("@perimControlBMPInstallDate", adVarChar, adParamInput, 50, perimControlBMPInstallDate)
		   .parameters.Append .CreateParameter("@initSedStorageInstallDate", adVarChar, adParamInput, 50, initSedStorageInstallDate)
		   .parameters.Append .CreateParameter("@sedStorageBMPInstallDate", adVarChar, adParamInput, 50, sedStorageBMPInstallDate)
		   
		   .parameters.Append .CreateParameter("@monthlyInspectionOccur", adVarChar, adParamInput, 50, monthlyInspectionOccur)		   
		   .parameters.Append .CreateParameter("@stabilizationAchieved", adVarChar, adParamInput, 50, stabilizationAchieved)
		   .parameters.Append .CreateParameter("@explainStabilizationAchieved", adVarChar, adParamInput, 2500, explainStabilizationAchieved)
		   .parameters.Append .CreateParameter("@landDisturbanceWeekendHoliday", adVarChar, adParamInput, 50, landDisturbanceWeekendHoliday)
		   .parameters.Append .CreateParameter("@landDisturbanceWeekendHolidayListDates", adVarChar, adParamInput, 2500, landDisturbanceWeekendHolidayListDates)
		   
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		
		'delete the data from the monthly rain data table
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteMonthlyRainData"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsDeleteRainData = oCmd.Execute
		Set oCmd = nothing
		
		
		'add the data to the water quality monitoring table
		For i = 1 to 10
		
		'response.Write i & "<br>"
		
			samplerID = checknull(request("samplerID" & i))
			GCSamplerID = null'checknull(request("GCSamplerID" & i))
			samplerType = null'checknull(request("samplerType" & i))
			dateEndClearingGrubbing = checknull(request("dateEndClearingGrubbing" & i))
			dateSamplerInstalled = null'checknull(request("dateSamplerInstalled" & i))
			dateSampleTakenAfterClearGrub = checknull(request("dateSampleTakenAfterClearGrub" & i))
			dateEndGrading = checknull(request("dateEndGrading" & i))
			dateAfterEndGradingorNinety = checknull(request("dateAfterEndGradingorNinety" & i))
			'BMPInstalledMaintRetrieved = checknull(request("BMPInstalledMaintRetrieved" & i))
			dateSamplerRemoved = null'checknull(request("dateSamplerRemoved" & i))
			rainDateEvent = checknull(request("rainDateEvent" & i))
			rainAmount = checknull(request("rainAmount" & i))
			codeCol1 = checknull(request("codeCol1" & i))
			codeCol2 = checknull(request("codeCol2" & i))
			codeCol3 = checknull(request("codeCol3" & i))
			codeCol4 = checknull(request("codeCol4" & i))
			codeCol5 = checknull(request("codeCol5" & i))
			codeCol6 = checknull(request("codeCol6" & i))
			codeCol7 = checknull(request("codeCol7" & i))
			codeCol8 = checknull(request("codeCol8" & i))
			codeCol9 = checknull(request("codeCol9" & i))
			codeCol10 = checknull(request("codeCol10" & i))

			samplingRequired = checknull(request("samplingRequired" & i))
			samplingComplete = checknull(request("samplingComplete" & i))

			
			
						
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddMonthlyRainData"
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
			   .parameters.Append .CreateParameter("@samplerID", adVarChar, adParamInput, 50, samplerID)
			   .parameters.Append .CreateParameter("@GCSamplerID", adVarChar, adParamInput, 50, GCSamplerID)
			   .parameters.Append .CreateParameter("@samplerType", adVarChar, adParamInput, 50, samplerType)
			   .parameters.Append .CreateParameter("@dateEndClearingGrubbing", adVarChar, adParamInput, 50, dateEndClearingGrubbing)			   
			   .parameters.Append .CreateParameter("@dateSamplerInstalled", adVarChar, adParamInput, 50, dateSamplerInstalled)
			   .parameters.Append .CreateParameter("@dateSampleTakenAfterClearGrub", adVarChar, adParamInput, 50, dateSampleTakenAfterClearGrub)
			   .parameters.Append .CreateParameter("@dateEndGrading", adVarChar, adParamInput, 50, dateEndGrading)
			   .parameters.Append .CreateParameter("@dateAfterEndGradingorNinety", adVarChar, adParamInput, 50, dateAfterEndGradingorNinety)
			   .parameters.Append .CreateParameter("@BMPInstalledMaintRetrieved", adVarChar, adParamInput, 50, null)
			   .parameters.Append .CreateParameter("@dateSamplerRemoved", adVarChar, adParamInput, 50, dateSamplerRemoved)
			   .parameters.Append .CreateParameter("@rainDateEvent", adVarChar, adParamInput, 50, rainDateEvent)
			   .parameters.Append .CreateParameter("@rainAmount", adVarChar, adParamInput, 50, rainAmount)
			   .parameters.Append .CreateParameter("@codeCol1", adVarChar, adParamInput, 50, codeCol1)
			   .parameters.Append .CreateParameter("@codeCol2", adVarChar, adParamInput, 50, codeCol2)
			   .parameters.Append .CreateParameter("@codeCol3", adVarChar, adParamInput, 50, codeCol3)
			   .parameters.Append .CreateParameter("@codeCol4", adVarChar, adParamInput, 50, codeCol4)
			   .parameters.Append .CreateParameter("@codeCol5", adVarChar, adParamInput, 50, codeCol5)
			   .parameters.Append .CreateParameter("@codeCol6", adVarChar, adParamInput, 50, codeCol6)
			   .parameters.Append .CreateParameter("@codeCol7", adVarChar, adParamInput, 50, codeCol7)
			   .parameters.Append .CreateParameter("@codeCol8", adVarChar, adParamInput, 50, codeCol8)
			   .parameters.Append .CreateParameter("@codeCol9", adVarChar, adParamInput, 50, codeCol9)
			   .parameters.Append .CreateParameter("@codeCol10", adVarChar, adParamInput, 50, codeCol10)
			   .parameters.Append .CreateParameter("@samplingRequired", adVarChar, adParamInput, 50, samplingRequired)
			   .parameters.Append .CreateParameter("@samplingComplete", adVarChar, adParamInput, 50, samplingComplete)
			   .CommandType = adCmdStoredProc
			End With
			
			Set rs = oCmd.Execute
			Set oCmd = nothing
		
		next
		
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMonthlyPDF.asp?reportID=" & reportID,  "LeftMargin=30, RightMargin=10, TopMargin=30, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if
		
		response.Redirect "reportList.asp?findReport=" & reportID
	
	Case "signReport"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		reportType = request("reportType")
		division = request("divisionID")
		reportID = request("reportID")
		userID = request("userID")
		
		On Error Resume Next

		Set DataConn = Server.CreateObject("ADODB.Connection") 	
		DataConn.Open Session("Connection"), Session("UserID")
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetReportByID"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
		   .CommandType = adCmdStoredProc
		   
		End With			
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
		
		'response.Write reportType
		
		
		select case reportType
			
			case "25" 'final monthly
			
			'	rep = request("rep")
				'add the userID of the person that signs the report to the reportTable
			'	select Case rep
			'	
			'		case "DSWCR"
			'			sSP = "spAddSignIDDSWCR"
			'			
			'		case "SiteRep"
			'			sSP = "spAddSignIDSiteRep"
			'	
			'	end select
				
				Set oCmd = Server.CreateObject("ADODB.Command")			
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spAddSign"
				   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
				   .parameters.Append .CreateParameter("@dateSigned", adDBTimeStamp, adParamInput, 8, now())
				   .CommandType = adCmdStoredProc
				   
				End With			
				Set rsSign = oCmd.Execute
				Set oCmd = nothing
			
				'create the PDF using the above report ID
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Pdf.RegKey = sRegKey
				Set Doc = Pdf.CreateDocument
				Doc.ImportFromUrl sPDFPath & "buildPDFMonthlyFinal.asp?reportID=" & reportID & "&signRep=true", "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
				Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
		
		end select

		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "editMonthlyFinal"
	
		'report header
		reportID = request("reportID")
		comments = request("comments")
		division = request("division")
		inspectionDate = checknull(request("inspectionDate"))
				
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditDaily"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing

		
		'edit the answers to the monthly final report table
		smonth = checknull(request("month"))
		syear = checknull(request("year"))
		stormwaterData = checknull(request("stormwaterData"))
		potentialViolations = null'checknull(request("potentialViolations"))
		finalStabilization = checknull(request("finalStabilization"))
		violationList = checknull(request("violationList"))
		
		potentialViolationsPermit = checknull(request("potentialViolationsPermit"))
		potentialViolationsInfo = checknull(request("potentialViolationsInfo"))
		violationOther = checknull(request("violationOther"))
		
		Set oCmd2 = Server.CreateObject("ADODB.Command")
		With oCmd2
		   .ActiveConnection = DataConn
		   .CommandText = "spEditMonthlyFinalReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
		   .parameters.Append .CreateParameter("@stormwaterData", adInteger, adParamInput, 8, stormwaterData)
		   .parameters.Append .CreateParameter("@potentialViolations", adInteger, adParamInput, 8, potentialViolations)
		   .parameters.Append .CreateParameter("@finalStabilization", adBoolean, adParamInput, 1, isOn(finalStabilization))
		   .parameters.Append .CreateParameter("@violationList", adVarChar, adParamInput, 1500, violationList) 
		   .parameters.Append .CreateParameter("@potentialViolationsPermit", adBoolean, adParamInput, 1, isOn(potentialViolationsPermit))
		   .parameters.Append .CreateParameter("@potentialViolationsInfo", adBoolean, adParamInput, 1, isOn(potentialViolationsInfo))
		   .parameters.Append .CreateParameter("@violationOther", adVarChar, adParamInput, 100, violationOther) 
		   
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd2.Execute
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Edited"
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPDFMonthlyFinal.asp?reportID=" & reportID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		Set Pdf = Nothing

		response.Redirect "reportList.asp?findReport=" & reportID
	
	Case "addMonthlyFinal"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = null'request("contactNumber")
		inspectionType = null'request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = null'request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		rainfallAmount = null'request("rainfallAmount")
		
		addToJournal = request("addToJournal")
		
		soilStabilizationComments = null'checknull(request("soilStabilizationComments"))
		
		'check to see if initial report has been done on this project
		'checkInitialInspection project
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")

			
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if
		
		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
		
		If Err Then
		%>
		1	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
		2	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
		3	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'add the answers to the monthly final report table
		smonth = checknull(request("month"))
		syear = checknull(request("year"))
		stormwaterData = checknull(request("stormwaterData"))
		potentialViolations = null'checknull(request("potentialViolations"))
		finalStabilization = checknull(request("finalStabilization"))
		violationList = checknull(request("violationList"))
		
		potentialViolationsPermit = checknull(request("potentialViolationsPermit"))
		potentialViolationsInfo = checknull(request("potentialViolationsInfo"))
		violationOther = checknull(request("violationOther"))
		
		Set oCmd2 = Server.CreateObject("ADODB.Command")
		With oCmd2
		   .ActiveConnection = DataConn
		   .CommandText = "spAddMonthlyFinalReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
		   .parameters.Append .CreateParameter("@stormwaterData", adInteger, adParamInput, 8, stormwaterData)
		   .parameters.Append .CreateParameter("@potentialViolations", adInteger, adParamInput, 8, potentialViolations)
		   .parameters.Append .CreateParameter("@finalStabilization", adBoolean, adParamInput, 1, isOn(finalStabilization))
		   .parameters.Append .CreateParameter("@violationList", adVarChar, adParamInput, 1500, violationList) 
		   .parameters.Append .CreateParameter("@potentialViolationsPermit", adBoolean, adParamInput, 1, isOn(potentialViolationsPermit))
		   .parameters.Append .CreateParameter("@potentialViolationsInfo", adBoolean, adParamInput, 1, isOn(potentialViolationsInfo))
		   .parameters.Append .CreateParameter("@violationOther", adVarChar, adParamInput, 100, violationOther) 
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd2.Execute
		
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if

		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPDFMonthlyFinal.asp?reportID=" & reportID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		Set Pdf = Nothing

		'send auto email if true for project
'		If autoEmail(project) = true then
			'response.Write reportID
'			sendAutoEmail reportID,project,division
'		end if

		response.Redirect "reportList.asp?findReport=" & reportID
	
	case "addContractorCert"
	
		contractorID = request("contractorID")
		certification = checknull(request("certification"))
		expirationDate = checknull(request("expirationDate"))
		certificationHolder = checknull(request("certificationHolder"))
		cardNumber = checknull(request("cardNumber"))
		documentID = checknull(request("documentID"))

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddContractorCert"
		   .parameters.Append .CreateParameter("@contractorID", adInteger, adParamInput, 8, contractorID)
		   .parameters.Append .CreateParameter("@certification", adVarChar, adParamInput, 100, certification)
		   .parameters.Append .CreateParameter("@expirationDate", adVarChar, adParamInput, 50, expirationDate)
		   .parameters.Append .CreateParameter("@certificationHolder", adVarChar, adParamInput, 50, certificationHolder)
		   .parameters.Append .CreateParameter("@cardNumber", adVarChar, adParamInput, 50, cardNumber)
		   .parameters.Append .CreateParameter("@documentID", adInteger, adParamInput, 8, documentID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "contractorCertList.asp?contractorID=" & contractorID
		
	case "editContractorCert"
	
		certificationID = request("certificationID")
		contractorID = request("contractorID")
		certification = checknull(request("certification"))
		expirationDate = checknull(request("expirationDate"))
		certificationHolder = checknull(request("certificationHolder"))
		cardNumber = checknull(request("cardNumber"))
		documentID = checknull(request("documentID"))

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditContractorCert"
		   .parameters.Append .CreateParameter("@certificationID", adInteger, adParamInput, 8, certificationID)
		   .parameters.Append .CreateParameter("@certification", adVarChar, adParamInput, 100, certification)
		   .parameters.Append .CreateParameter("@expirationDate", adVarChar, adParamInput, 50, expirationDate)
		   .parameters.Append .CreateParameter("@certificationHolder", adVarChar, adParamInput, 50, certificationHolder)
		   .parameters.Append .CreateParameter("@cardNumber", adVarChar, adParamInput, 50, cardNumber)
		   .parameters.Append .CreateParameter("@documentID", adInteger, adParamInput, 8, documentID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "contractorCertList.asp?contractorID=" & contractorID
		
	case "deleteContractorCert"
		certificationID = Request("id")
		contractorID = request("contractorID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteContractorCert"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, certificationID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "contractorCertList.asp?contractorID=" & contractorID
		
	case "deleteCertDoc"
		documentID = Request("id")
		contractorID = request("contractorID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteCertDoc"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, documentID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "certificationDocs.asp?contractorID=" & contractorID
	
	case "addContractor"
	
		contractorName = request("contractorName")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddContractor"
		   .parameters.Append .CreateParameter("@contractorName", adVarChar, adParamInput, 50, contractorName)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		contractorID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'response.Redirect "contractorList.asp"
		
		response.Redirect "form.asp?formType=addContractorCert&contractorID=" & contractorID
		
	case "editContractor"
	
		contractorID = request("contractorID")
		contractorName = request("contractorName")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditContractor"
		   .parameters.Append .CreateParameter("@contractorID", adInteger, adParamInput, 8, contractorID)
		   .parameters.Append .CreateParameter("@contractorName", adVarChar, adParamInput, 50, contractorName)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "contractorList.asp"
		
	case "deleteContractor"
		contractorID = Request("id")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteContractor"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, contractorID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "contractorList.asp"
		
	
	case "deleteReport"
		reportID = Request("id")
		divisionID = request("divisionID")
		projectID = request("projectID")
		reportType = request("reportType")
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
		
		
		'delete from the activity log
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteActivityLog"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		'delete from responsiveaction
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteResponsiveActions"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		'delete from reportanswers
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteReportAnswers"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
			
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		'delete the PDF if there
		dim fso
		Set fso = CreateObject("Scripting.FileSystemObject") 

		If fso.FileExists(Server.MapPath("downloads/swsir_" & reportID & ".pdf")) Then
		   fso.DeleteFile Server.MapPath("downloads/swsir_" & reportID & ".pdf")	'sServerPath & "c:\somefile.txt" 
		End If	
		
		'delete from report
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteReport"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
	
			
		
		If Err Then
		%>
		1	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
	'	response.Write Server.MapPath(sServerPath &  getReportType(reportID) & "_" & reportID & ".pdf")
		
		
		
		response.Redirect "reportList.asp?reportTypeID=" & reportType & "&division=" & division & "&project=" & project
		'response.Redirect "reportList.asp?division=" & divisionID & "&project=" & projectID
	
	
	case "addLocation"
	
		customer = request("customer")
		projectID = request("projectID")
		divisionID = request("divisionID")
		location = handleApostrophe(request("location"))
		questionID = request("questionID")
		proposedActivity = handleApostrophe(request("proposedActivity"))
		biweekly = request("biweekly")
		waterSampling = request("waterSampling")
		dateInstalled = checknull(request("dateInstalled"))
		
		sPath = request("path")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddProjectLocation"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, questionID)
		   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 500, location)
		   .parameters.Append .CreateParameter("@proposedActivity", adVarChar, adParamInput, 500, proposedActivity)
		   .parameters.Append .CreateParameter("@biweekly", adBoolean, adParamInput, 1, isOn(biweekly)) 
		   .parameters.Append .CreateParameter("@waterSampling", adBoolean, adParamInput, 1, isOn(waterSampling)) 
		   .parameters.Append .CreateParameter("@dateInstalled", adDBTimeStamp, adParamInput, 8, dateInstalled)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "locationList.asp?projectID=" & projectID & "&customerID=" & customer & "&divisionID=" & divisionID & "&path=" & sPath
		
	case "editLocation"
	
		customer = request("customer")
		projectID = request("projectID")
		divisionID = request("divisionID")
		location = handleApostrophe(request("location"))
		questionID = request("questionID")
		locationID = request("locationID")
		proposedActivity = handleApostrophe(request("proposedActivity"))
		biweekly = request("biweekly")
		waterSampling = request("waterSampling")
		dateInstalled = checknull(request("dateInstalled"))
		
		sPath = request("path")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditProjectLocation"
		   .parameters.Append .CreateParameter("@locationID", adInteger, adParamInput, 8, locationID)
		   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, questionID)
		   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 500, location)
		   .parameters.Append .CreateParameter("@proposedActivity", adVarChar, adParamInput, 500, proposedActivity)
		   .parameters.Append .CreateParameter("@biweekly", adBoolean, adParamInput, 1, isOn(biweekly)) 
		   .parameters.Append .CreateParameter("@waterSampling", adBoolean, adParamInput, 1, isOn(waterSampling))
		   .parameters.Append .CreateParameter("@dateInstalled", adDBTimeStamp, adParamInput, 8, dateInstalled)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		response.Redirect "locationList.asp?projectID=" & projectID & "&customerID=" & customer & "&divisionID=" & divisionID & "&path=" & sPath
		
	case "deleteLocation"
		locationID = Request("id")
		customer = request("customer")
		projectID = request("projectID")
		divisionID = request("divisionID")
		
		sPath = request("path")	
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteProjectLocation"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, locationID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "locationList.asp?projectID=" & projectID & "&customerID=" & customer & "&divisionID=" & divisionID & "&path=" & sPath
	
	case "deleteInfoPack"
		infoPackID = Request("id")
		clientID = request("clientID")	
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteInfoPack"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, infoPackID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "infoPackList.asp?clientID=" & clientID & "&v=True"
	
	case "addMiles"
		userID= request("userID")
		sdate = request("date")
		miles= request("miles")	
		comments= request("comments")
		odometerStart= request("odometerStart")
		odometerEnd= request("odometerEnd")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddMiles"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@date", adDBTimeStamp, adParamInput, 8, sdate)
		   .parameters.Append .CreateParameter("@miles", adInteger, adParamInput, 8, miles)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 500, comments)
		   .parameters.Append .CreateParameter("@odometerStart", adInteger, adParamInput, 8, odometerStart)
		   .parameters.Append .CreateParameter("@odometerEnd", adInteger, adParamInput, 8, odometerEnd)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		response.Redirect "mileage.asp"
		
	case "editMiles"
	
		mileageID = request("mileageID")
		userID= request("userID")
		sdate = request("date")
		miles= request("miles")	
		comments= request("comments")
		odometerStart= request("odometerStart")
		odometerEnd= request("odometerEnd")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditMiles"
		   .parameters.Append .CreateParameter("@mileageID", adInteger, adParamInput, 8, mileageID)
		   .parameters.Append .CreateParameter("@date", adDBTimeStamp, adParamInput, 8, sdate)
		   .parameters.Append .CreateParameter("@miles", adInteger, adParamInput, 8, miles)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 500, comments)
		   .parameters.Append .CreateParameter("@odometerStart", adInteger, adParamInput, 8, odometerStart)
		   .parameters.Append .CreateParameter("@odometerEnd", adInteger, adParamInput, 8, odometerEnd)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		response.Redirect "mileage.asp"
		
	case "deleteMiles"
		mileageID = Request("id")	
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteMiles"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, mileageID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "mileage.asp"
	
	case "addClassType"
	
		classType = request("classType")
		upTo10 = checknull(request("upTo10"))
		upTo15 = checknull(request("upTo15"))
		upTo20 = checknull(request("upTo20"))
		upTo35 = checknull(request("upTo35"))
		upTo50 = checknull(request("upTo50"))
		studentManuals = checknull(request("studentManuals"))
		AVCharge = checknull(request("AVCharge"))
		saturdayDeposit = checknull(request("saturdayDeposit"))
		saturdayCharge = checknull(request("saturdayCharge"))
		eveningSplitCharge = checknull(request("eveningSplitCharge"))
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddClassType"
		   .parameters.Append .CreateParameter("@classType", adVarChar, adParamInput, 100, classType)
		   .parameters.Append .CreateParameter("@upTo10", adCurrency, adParamInput, 8, upTo10)
		   .parameters.Append .CreateParameter("@upTo15", adCurrency, adParamInput, 8, upTo15)
		   .parameters.Append .CreateParameter("@upTo20", adCurrency, adParamInput, 8, upTo20)
		   .parameters.Append .CreateParameter("@upTo35", adCurrency, adParamInput, 8, upTo35)
		   .parameters.Append .CreateParameter("@upTo50", adCurrency, adParamInput, 8, upTo50)
		   .parameters.Append .CreateParameter("@studentManuals", adCurrency, adParamInput, 8, studentManuals)
		   .parameters.Append .CreateParameter("@AVCharge", adCurrency, adParamInput, 8, AVCharge)
		   .parameters.Append .CreateParameter("@saturdayDeposit", adCurrency, adParamInput, 8, saturdayDeposit)
		   .parameters.Append .CreateParameter("@saturdayCharge", adCurrency, adParamInput, 8, saturdayCharge)
		   .parameters.Append .CreateParameter("@eveningSplitCharge", adCurrency, adParamInput, 8, eveningSplitCharge)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "classTypeList.asp"
		
	case "editClassType"
	
		classTypeID = request("classTypeID")
		classType = request("classType")
		upTo10 = checknull(request("upTo10"))
		upTo15 = checknull(request("upTo15"))
		upTo20 = checknull(request("upTo20"))
		upTo35 = checknull(request("upTo35"))
		upTo50 = checknull(request("upTo50"))
		studentManuals = checknull(request("studentManuals"))
		AVCharge = checknull(request("AVCharge"))
		saturdayDeposit = checknull(request("saturdayDeposit"))
		saturdayCharge = checknull(request("saturdayCharge"))
		eveningSplitCharge = checknull(request("eveningSplitCharge"))
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditClassType"
		   .parameters.Append .CreateParameter("@classTypeID", adInteger, adParamInput, 8, classTypeID)
		   .parameters.Append .CreateParameter("@classType", adVarChar, adParamInput, 100, classType)
		   .parameters.Append .CreateParameter("@upTo10", adCurrency, adParamInput, 8, upTo10)
		   .parameters.Append .CreateParameter("@upTo15", adCurrency, adParamInput, 8, upTo15)
		   .parameters.Append .CreateParameter("@upTo20", adCurrency, adParamInput, 8, upTo20)
		   .parameters.Append .CreateParameter("@upTo35", adCurrency, adParamInput, 8, upTo35)
		   .parameters.Append .CreateParameter("@upTo50", adCurrency, adParamInput, 8, upTo50)
		   .parameters.Append .CreateParameter("@studentManuals", adCurrency, adParamInput, 8, studentManuals)
		   .parameters.Append .CreateParameter("@AVCharge", adCurrency, adParamInput, 8, AVCharge)
		   .parameters.Append .CreateParameter("@saturdayDeposit", adCurrency, adParamInput, 8, saturdayDeposit)
		   .parameters.Append .CreateParameter("@saturdayCharge", adCurrency, adParamInput, 8, saturdayCharge)
		   .parameters.Append .CreateParameter("@eveningSplitCharge", adCurrency, adParamInput, 8, eveningSplitCharge)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "classTypeList.asp"
		
	case "deleteClassType"
		classTypeID = Request("id")	
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteClassType"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, classTypeID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "classTypeList.asp"
	
	case "removeStudent"
		classID = request("classID")
		studentID = request("studentID")
		CustomerID = request("CustomerID")
		clientID = request("clientID")
		sRefer = request("sRefer")
		
		On Error Resume Next
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spRemoveStudent"
		   .parameters.Append .CreateParameter("@studentID", adInteger, adParamInput, 8, studentID)
		   .parameters.Append .CreateParameter("@classID", adInteger, adParamInput, 8, classID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute			
		Set oCmd = nothing
		
		if sRefer="cal" then
			response.redirect "classDetails.asp?classID=" & classID & "&customerID=" & customerID
		else
			response.redirect "assignStudents.asp?classID=" & classID & "&customerID=" & customerID
		end if
	
	case "attendClass"
		classID = request("classID")
		studentID = request("studentID")
		CustomerID = request("CustomerID")
		clientID = request("clientID")
		sType = request("sType")
		sRefer = request("sRefer")

		
		On Error Resume Next
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAttendClass"
		   .parameters.Append .CreateParameter("@studentID", adInteger, adParamInput, 8, studentID)
		   .parameters.Append .CreateParameter("@classID", adInteger, adParamInput, 8, classID)
		   .parameters.Append .CreateParameter("@attended", adBoolean, adParamInput, 1, sType)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute			
		Set oCmd = nothing
		
		if sRefer="cal" then
			response.redirect "classDetails.asp?classID=" & classID & "&customerID=" & customerID
		else
			response.redirect "assignStudents.asp?classID=" & classID & "&customerID=" & customerID
		end if
		
	
	case "assignStudents"
	
		classID = request("classID")
		CustomerID = request("CustomerID")
		clientID = request("clientID")
	
		arrAll = split(Request.Form ("allBoxes"), ",")
		
		'get the array with the id's that were ticked
		arrVals = split(Request.Form ("assignStudent"), ",")
		
		On Error Resume Next
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		
		for i = 0 to ubound(arrVals)
		
			'response.Write "classID " & classID & "<br>"
			'response.Write "studentID " & arrVals(i) & "<br>"

			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAssignStudent"
			   .parameters.Append .CreateParameter("@studentID", adInteger, adParamInput, 8, arrVals(i))
			   .parameters.Append .CreateParameter("@classID", adInteger, adParamInput, 8, classID)
			   .parameters.Append .CreateParameter("@attended", adBoolean, adParamInput, 1, false)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rs = oCmd.Execute			
			Set oCmd = nothing
			
		next
		
		
	
		response.redirect "assignStudents.asp?classID=" & classID & "&customerID=" & customerID
	
	case "addStudent"
	
		clientID = request("clientID")
		customerID = request("customerID")
		firstName = checknull(request("firstName"))
		lastName = checknull(request("lastName"))
		email = checknull(request("email"))
		address1 = checknull(request("address1"))
		address2 = checknull(request("address2"))
		city = checknull(request("city"))
		state = checknull(request("state"))
		zip = checknull(request("zip"))
		phone1 = request("phone1")
		phone2 = request("phone2")
		phone3 = request("phone3")
		officePhone = phone1 & phone2 & phone3
		ext = request("ext")
		cellPhone1 = request("cellPhone1")
		cellPhone2 = request("cellPhone2")
		cellPhone3 = request("cellPhone3")
		cellPhone = cellPhone1 & cellPhone2 & cellPhone3
		
		sList = request("sList")
		classID = request("classID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddStudent"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@firstName", adVarChar, adParamInput, 50, firstName) 
		   .parameters.Append .CreateParameter("@lastName", adVarChar, adParamInput, 50, lastName)
		   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 50, email)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 50, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 50, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@officePhone", adVarChar, adParamInput, 50, officePhone)
		   .parameters.Append .CreateParameter("@cellPhone", adVarChar, adParamInput, 50, cellPhone)
		   .parameters.Append .CreateParameter("@ext", adVarChar, adParamInput, 50, ext)		   

		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
		if sList = "assign" then
			response.Redirect "assignStudents.asp?classID=" & classID & "&customerID=" & customerID
		else
			response.Redirect "studentList.asp?customerID=" & customerID & "&clientID=" & clientID
		end if
		
	case "editStudent"
	
		studentID = request("studentID")
		clientID = request("clientID")
		customerID = request("customerID")
		firstName = checknull(request("firstName"))
		lastName = checknull(request("lastName"))
		email = checknull(request("email"))
		address1 = checknull(request("address1"))
		address2 = checknull(request("address2"))
		city = checknull(request("city"))
		state = checknull(request("state"))
		zip = checknull(request("zip"))
		phone1 = request("phone1")
		phone2 = request("phone2")
		phone3 = request("phone3")
		officePhone = phone1 & phone2 & phone3
		ext = request("ext")
		cellPhone1 = request("cellPhone1")
		cellPhone2 = request("cellPhone2")
		cellPhone3 = request("cellPhone3")
		cellPhone = cellPhone1 & cellPhone2 & cellPhone3

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditStudent"
		   .parameters.Append .CreateParameter("@studentID", adInteger, adParamInput, 8, studentID)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@firstName", adVarChar, adParamInput, 50, firstName) 
		   .parameters.Append .CreateParameter("@lastName", adVarChar, adParamInput, 50, lastName)
		   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 50, email)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 50, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 50, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@officePhone", adVarChar, adParamInput, 50, officePhone)
		   .parameters.Append .CreateParameter("@cellPhone", adVarChar, adParamInput, 50, cellPhone)
		   .parameters.Append .CreateParameter("@ext", adVarChar, adParamInput, 50, ext)		   

		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
		
		response.Redirect "studentList.asp?customerID=" & customerID & "&clientID=" & clientID
		
	case "deleteStudent"
		studentID = Request("id")	
		customerID = request("customerID")
		clientID = request("clientID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteStudent"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, studentID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "studentList.asp?customerID=" & customerID & "&clientID=" & clientID
	
	case "addClass"
	
		classTypeID = request("classTypeID")
		customerID = request("customerID")
		openSlots = checknull(request("openSlots"))
		startDate = checknull(request("startDate"))
		startTime = checknull(request("startTime"))
		endDate = checknull(request("endDate"))
		endTime = checknull(request("endTime"))
		location = checknull(request("location"))
		address1 = checknull(request("address1"))
		address2 = checknull(request("address2"))
		city = checknull(request("city"))
		state = checknull(request("state"))
		zip = checknull(request("zip"))
		projector = request("projector")
		screen = request("screen")
		classSize = request("classSize")
		eveningClass = request("eveningClass")
		saturdayClass = request("saturdayClass")
		splitClass = null'request("splitClass")
		associationDiscount = request("associationDiscount")
		associationDiscountAmt = request("associationDiscountAmt")
		numberManuals = checknull(request("numberManuals"))

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddClass"
		   .parameters.Append .CreateParameter("@classTypeID", adInteger, adParamInput, 8, classTypeID)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@openSlots", adInteger, adParamInput, 8, openSlots)
		   .parameters.Append .CreateParameter("@isPrivate", adBoolean, adParamInput, 1, true)
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@startTime", adVarChar, adParamInput, 50, startTime)
		   .parameters.Append .CreateParameter("@endDate", adDBTimeStamp, adParamInput, 8, endDate) 
		   .parameters.Append .CreateParameter("@endTime", adVarChar, adParamInput, 50, endTime)
		   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 50, location)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 100, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 50, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)		   
		   .parameters.Append .CreateParameter("@projector", adBoolean, adParamInput, 1, isOn(projector)) 
		   .parameters.Append .CreateParameter("@screen", adBoolean, adParamInput, 1, isOn(screen))
		   .parameters.Append .CreateParameter("@isComplete", adBoolean, adParamInput, 1, false)
		   .parameters.Append .CreateParameter("@classSize", adInteger, adParamInput, 8, classSize)
		   .parameters.Append .CreateParameter("@eveningClass", adBoolean, adParamInput, 1, isOn(eveningClass))
		   .parameters.Append .CreateParameter("@saturdayClass", adBoolean, adParamInput, 1, isOn(saturdayClass))
		   .parameters.Append .CreateParameter("@splitClass", adBoolean, adParamInput, 1, isOn(splitClass))
		   .parameters.Append .CreateParameter("@associationDiscount", adBoolean, adParamInput, 1, isOn(associationDiscount))
		   .parameters.Append .CreateParameter("@associationDiscountAmt", adCurrency, adParamInput, 8, associationDiscountAmt)
		   .parameters.Append .CreateParameter("@numberManuals", adInteger, adParamInput, 8, numberManuals)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
		if customerID = 0 then
			response.Redirect "classCalendar.asp"
		else
			response.Redirect "classList.asp?customerID=" & customerID
		end if
		
	case "editClass"
	
		classID = request("classID")
		classTypeID = request("classTypeID")
		customerID = request("customerID")
		openSlots = checknull(request("openSlots"))
		startDate = checknull(request("startDate"))
		startTime = checknull(request("startTime"))
		endDate = checknull(request("endDate"))
		endTime = checknull(request("endTime"))
		location = checknull(request("location"))
		address1 = checknull(request("address1"))
		address2 = checknull(request("address2"))
		city = checknull(request("city"))
		state = checknull(request("state"))
		zip = checknull(request("zip"))
		projector = request("projector")
		screen = request("screen")
		classSize = request("classSize")
		eveningClass = request("eveningClass")
		saturdayClass = request("saturdayClass")
		splitClass = null'request("splitClass")
		associationDiscount = request("associationDiscount")
		associationDiscountAmt = request("associationDiscountAmt")
		numberManuals = checknull(request("numberManuals"))

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditClass"
		   .parameters.Append .CreateParameter("@classID", adInteger, adParamInput, 8, classID)
		   .parameters.Append .CreateParameter("@classTypeID", adInteger, adParamInput, 8, classTypeID)
		   .parameters.Append .CreateParameter("@openSlots", adInteger, adParamInput, 8, openSlots)
		   .parameters.Append .CreateParameter("@isPrivate", adBoolean, adParamInput, 1, true)
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@startTime", adVarChar, adParamInput, 50, startTime)
		   .parameters.Append .CreateParameter("@endDate", adDBTimeStamp, adParamInput, 8, endDate) 
		   .parameters.Append .CreateParameter("@endTime", adVarChar, adParamInput, 50, endTime)
		   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 50, location)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 100, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 50, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)		   
		   .parameters.Append .CreateParameter("@projector", adBoolean, adParamInput, 1, isOn(projector)) 
		   .parameters.Append .CreateParameter("@screen", adBoolean, adParamInput, 1, isOn(screen))
		   .parameters.Append .CreateParameter("@isComplete", adBoolean, adParamInput, 1, false)
		   .parameters.Append .CreateParameter("@classSize", adInteger, adParamInput, 8, classSize)
		   .parameters.Append .CreateParameter("@eveningClass", adBoolean, adParamInput, 1, isOn(eveningClass))
		   .parameters.Append .CreateParameter("@saturdayClass", adBoolean, adParamInput, 1, isOn(saturdayClass))
		   .parameters.Append .CreateParameter("@splitClass", adBoolean, adParamInput, 1, isOn(splitClass))
		   .parameters.Append .CreateParameter("@associationDiscount", adBoolean, adParamInput, 1, isOn(associationDiscount))
		   .parameters.Append .CreateParameter("@associationDiscountAmt", adCurrency, adParamInput, 8, associationDiscountAmt)
		   .parameters.Append .CreateParameter("@numberManuals", adInteger, adParamInput, 8, numberManuals)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
		if customerID = 0 then
			response.Redirect "classCalendar.asp"
		else
			response.Redirect "classList.asp?customerID=" & customerID
		end if
		
	case "deleteClass"
		classID = Request("id")	
		customerID = request("customerID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteClass"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, classID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		if customerID = 0 then
			response.Redirect "classCalendar.asp"
		else
			response.Redirect "classList.asp?customerID=" & customerID
		end if
	
	Case "addAuditReport"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		rainfallAmount = request("rainfallAmount")
		
		addToJournal = request("addToJournal")
		
		clientID = Session("clientID")
		
		soilStabilizationComments = checknull(request("soilStabilizationComments"))
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
			
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'add the answers to the questions

		For Each Item in Request.Form
			if Left(item,6) = "answer" then
				
				'if the is no underscore, then write the data
				if Left(item,8) = "answer_&" then
					'insert the answers into the report answers table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					If Err Then
					%>
					10	<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddReportAnswers"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, cint(replace(Item,"answer_&","")))
					   .parameters.Append .CreateParameter("@answer", adVarChar, adParamInput, 50, trim(Request.Form(Item)))
					   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, now())
					   .CommandType = adCmdStoredProc
					End With
					
					If Err Then
					%>
					9	<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					Set rs = oCmd2.Execute
					
					If Err Then
					%>
					8	<!--#include file="includes/FatalError.inc"-->
					<%
					End If
				end if
				
				
				'for each no answer, add the responsive action needed
				if 	Left(item,8) = "answer_R" then
					'reportID get from above
			
					'questionID get from srting
					iLen1 = len(item)
					iLen2 = InStr(item, "@")
					quesID = Right(item,iLen1 - iLen2)
					'response.Write "question " & quesID & "<br>"
					
					'reference number
					sText = Replace(item,"answer_R", "")
					iLen3 = InStr(sText, "@")
					iLen4 = len(sText)
					sText = Left(sText,iLen3)
					sRefNum = Replace(sText,"@", "")
					
					'action needed
					sActionNeeded = trim(Request.Form(Item))
					
					If 	sActionNeeded <> "" then			
						'insert the data into the database
						Set oCmd3 = Server.CreateObject("ADODB.Command")
						If Err Then
						%>
							18<!--#include file="includes/FatalError.inc"-->
						<%
						End If
						
						With oCmd3
						   .ActiveConnection = DataConn
						   .CommandText = "spAddResponsiveActions"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
						   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
						   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
						   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, sActionNeeded)
						   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
						   .CommandType = adCmdStoredProc
						End With
						
						If Err Then
						%>
					6		<!--#include file="includes/FatalError.inc"-->
						<%
						End If
						
						Set rs2 = oCmd3.Execute
						
						If Err Then
						%>
					7		<!--#include file="includes/FatalError.inc"-->
						<%
						End If
					end if
					
				
				end if
			end if
		Next
		
		'loop through the form fields and update the reportAnswers table with the location info.
		'need reportID and reference number
		'keep the item open by inserting 0 into isClosed field
		For Each Item in Request.Form
			if 	Left(item,9) = "location_" then
				sRefNumb = Replace(item,"location_", "")
				
				'insert the data into the database
				Set oCmd4 = Server.CreateObject("ADODB.Command")
			
				With oCmd4
				   .ActiveConnection = DataConn
				   .CommandText = "spAddLocation" 'is an update query
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNumb)
				   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000, trim(Request.Form(Item)))
				   .CommandType = adCmdStoredProc
				End With
			
				Set rs3 = oCmd4.Execute
				
			end if
		Next
		
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if

		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPDFAudit.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsirTemp_" & reportID & ".pdf"), true )
		Set Pdf = Nothing
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPDFAuditPage2.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf"), true )
		Set Pdf = Nothing
		
		'stitch the pdfs together
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTemp_" & reportID & ".pdf") )
		Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf") )
		Doc1.AppendDocument Doc2
		Filename = Doc1.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		Set Pdf = Nothing

		'send auto email if true for project
		If autoEmail(project) = true then
			'response.Write reportID
			sendAutoEmail reportID,project,division
		end if

		response.Redirect "reportList.asp?findReport=" & reportID
	
	case "addToolboxSession"
		topicID = request("topicID")	
		userID = request("userID")
		iParticipants = split(Request("participants"), ",")
		
		'add the session to the database and return the sessionid 
		On Error Resume Next		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddToolboxSession"
		   .parameters.Append .CreateParameter("@topicID", adInteger, adParamInput, 8, topicID)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		toolboxSessionID = rs("Identity")
		
		
		
		for i = 0 to ubound(iParticipants)	
		
			Set oCmd = Server.CreateObject("ADODB.Command")			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddToolboxSessionUser"
			   .parameters.Append .CreateParameter("@toolboxSessionID", adInteger, adParamInput, 8, toolboxSessionID)
			   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, trim(iParticipants(i)))
			   .CommandType = adCmdStoredProc
			End With					
			Set rs = oCmd.Execute
		next		
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'return the user to the toolbox session list
		response.Redirect "toolboxSessionList.asp"
	
	case "addTopic"
	
		topic = request("topic")
		document = request("document")
		helpText = request("helpText")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddTopic"
		   .parameters.Append .CreateParameter("@topic", adVarChar, adParamInput, 150, topic)
		   .parameters.Append .CreateParameter("@document", adVarChar, adParamInput, 100, document)
		   .parameters.Append .CreateParameter("@helpText", adVarChar, adParamInput, 8000, helpText)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
		
		response.Redirect "toolboxTopics.asp"
		
	case "editTopic"
		topicID = request("topicID")
		topic = request("topic")
		document = request("document")
		helpText = request("helpText")
		

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditTopic"
		   .parameters.Append .CreateParameter("@topicID", adInteger, adParamInput, 8, topicID)
		   .parameters.Append .CreateParameter("@topic", adVarChar, adParamInput, 150, topic)
		   .parameters.Append .CreateParameter("@document", adVarChar, adParamInput, 100, document)
		   .parameters.Append .CreateParameter("@helpText", adVarChar, adParamInput, 8000, helpText)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
		
		response.Redirect "toolboxTopics.asp"

	case "deleteTopic"
		topicID = Request("id")	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteTopic"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, topicID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "toolboxTopics.asp"
	
	case "addCrane"
	
		craneTypeID = request("craneTypeID")
		craneName = request("craneName")
		craneNumber = request("craneNumber")
		projectID = request("projectID")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddCrane"
		   .parameters.Append .CreateParameter("@craneTypeID", adInteger, adParamInput, 8, craneTypeID)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@craneName", adVarChar, adParamInput, 50, craneName)
		   .parameters.Append .CreateParameter("@craneNumber", adVarChar, adParamInput, 50, craneNumber)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "craneList.asp?divisionID=" & divisionID & "&customerID=" & customerID & "&projectID=" & projectID
		
	case "editCrane"
	
		craneID = request("craneID")
		craneTypeID = request("craneTypeID")
		craneName = request("craneName")
		craneNumber = request("craneNumber")
		projectID = request("projectID")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditCrane"
		   .parameters.Append .CreateParameter("@craneID", adInteger, adParamInput, 8, craneID)
		   .parameters.Append .CreateParameter("@craneTypeID", adInteger, adParamInput, 8, craneTypeID)
		   .parameters.Append .CreateParameter("@craneName", adVarChar, adParamInput, 50, craneName)
		   .parameters.Append .CreateParameter("@craneNumber", adVarChar, adParamInput, 50, craneNumber)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "craneList.asp?divisionID=" & divisionID & "&customerID=" & customerID & "&projectID=" & projectID
		
	case "deleteCrane"
		craneID = Request("id")
		projectID = request("projectID")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteCrane"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, craneID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "craneList.asp?divisionID=" & divisionID & "&customerID=" & customerID & "&projectID=" & projectID
	
	case "editCraneQuestion"
	
		questionID = request("questionID")
		question = request("question")
		sortNumber = request("sortNumber")
		categoryID = request("categoryID")
		customerID = request("customerID")
		towerCrane =  request("towerCrane")
		divisionID = request("divisionID")
		questionType = request("questionType")
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditCraneQuestion"
		   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, questionID)
		   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
		   .parameters.Append .CreateParameter("@question", adVarChar, adParamInput, 1000, question)
		   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, sortNumber) 
		   .parameters.Append .CreateParameter("@towerCrane", adBoolean, adParamInput, 1, isOn(towerCrane)) 
		   .parameters.Append .CreateParameter("@questionType", adInteger, adParamInput, 8, questionType)    
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing		
		
		rs.close
		set rs = nothing
		
		response.Redirect "craneQuestionList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "addCraneQuestion"
	
		question = request("question")
		sortNumber = request("sortNumber")
		categoryID = request("categoryID")
		customerID = request("customerID")
		towerCrane =  request("towerCrane")
		divisionID = request("divisionID")
		questionType = request("questionType")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddCraneQuestion"
		   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
		   .parameters.Append .CreateParameter("@question", adVarChar, adParamInput, 1000, question)
		   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, sortNumber)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)	 
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID) 
		   .parameters.Append .CreateParameter("@towerCrane", adBoolean, adParamInput, 1, isOn(towerCrane)) 
		   .parameters.Append .CreateParameter("@questionType", adInteger, adParamInput, 8, questionType)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "craneQuestionList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "deleteCraneQuestion"
		questionID = Request("id")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteCraneQuestion"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, questionID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "craneQuestionList.asp?id=" & divisionID & "&customerID=" & customerID
	
	case "editCraneCategory"
	
		categoryID = request("categoryID")
		category = request("category")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditCraneCategory"
		   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
		   .parameters.Append .CreateParameter("@category", adVarChar, adParamInput, 200, category)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "craneCategoryList.asp?id=" & customerID & "&divisionID=" & divisionID
		
	case "addCraneCategory"
	
		category = request("category")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddCraneCategory"
		   .parameters.Append .CreateParameter("@category", adVarChar, adParamInput, 200, category)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "craneCategoryList.asp?id=" & customerID & "&divisionID=" & divisionID
		
	case "deleteCraneCategory"
		categoryID = Request("id")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteCraneCategory"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, categoryID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "craneCategoryList.asp?id=" & customerID & "&divisionID=" & divisionID
	
	case "addSubVendor"
	
		userID = checknull(request("userID"))
		'dateAdded = checknull(request(""))
		companyName = checknull(request("companyName"))
		companyAddress = checknull(request("companyAddress"))
		companyCity = checknull(request("companyCity"))
		companyState = checknull(request("companyState"))
		companyZip = checknull(request("companyZip"))
		companyPhone = checknull(request("companyPhone"))
		serviceType = null'checknull(request("serviceType"))
		taxID = checknull(request("taxID"))
		yearsOperating = checknull(request("yearsOperating"))
		previousCompanyName = checknull(request("previousCompanyName"))
		orgTypeID = checknull(request("orgTypeID"))
		otherOrgType = checknull(request("otherOrgType"))
		servicesProvided = checknull(request("servicesProvided"))		
		servicesProvided2 = checknull(request("servicesProvided2"))
		servicesProvided3 = checknull(request("servicesProvided3"))
		servicesProvided4 = checknull(request("servicesProvided4"))
		servicesProvided5 = checknull(request("servicesProvided5"))
		servicesProvided6 = checknull(request("servicesProvided6"))
		servicesProvided7 = checknull(request("servicesProvided7"))
		servicesProvided8 = checknull(request("servicesProvided8"))
		servicesProvided9 = checknull(request("servicesProvided9"))
		servicesProvided10 = checknull(request("servicesProvided10"))
		
		EEOAAPStatement = checknull(request("EEOAAPStatement"))
		I9Acknowledgement = checknull(request("I9Acknowledgement"))
		nonDiscriminationPolicy = checknull(request("nonDiscriminationPolicy"))
		nonSegregationPolicy = checknull(request("nonSegregationPolicy"))
		NAICSCode = checknull(request("NAICSCode"))
		
		NAICSCode2 = checknull(request("NAICSCode2"))
		NAICSCode3 = checknull(request("NAICSCode3"))
		NAICSCode4 = checknull(request("NAICSCode4"))
		NAICSCode5 = checknull(request("NAICSCode5"))
		NAICSCode6 = checknull(request("NAICSCode6"))
		
		businessClassification = checknull(request("businessClassification"))

		smallBusiness = checknull(request("smallBusiness"))
		womanOwnedSmallBusiness = checknull(request("womanOwnedSmallBusiness"))
		HUBZone = checknull(request("HUBZone"))
		VOSB = checknull(request("VOSB"))
		SDVOSB = checknull(request("SDVOSB"))
		SDB = checknull(request("SDB"))
		DBE = checknull(request("DBE"))
		otherClassification = checknull(request("otherClassification"))
		 
		otherClassificationExplain = checknull(request("otherClassificationExplain"))
		name = checknull(request("name"))
		title = checknull(request("title"))
		phoneNumber = checknull(request("phoneNumber"))
		dateSigned = checknull(request("dateSigned"))

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddSubVendor"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@companyName", adVarChar, adParamInput, 50, companyName)
		   .parameters.Append .CreateParameter("@companyAddress", adVarChar, adParamInput, 50, companyAddress)
		   .parameters.Append .CreateParameter("@companyPhone", adVarChar, adParamInput, 50, companyPhone)
		   .parameters.Append .CreateParameter("@serviceType", adVarChar, adParamInput, 50, serviceType)
		   .parameters.Append .CreateParameter("@taxID", adVarChar, adParamInput, 50, taxID)
		   .parameters.Append .CreateParameter("@yearsOperating", adVarChar, adParamInput, 50, yearsOperating)
		   .parameters.Append .CreateParameter("@previousCompanyName", adVarChar, adParamInput, 50, previousCompanyName)
		   .parameters.Append .CreateParameter("@orgTypeID", adInteger, adParamInput, 8, orgTypeID)
		   .parameters.Append .CreateParameter("@otherOrgType", adVarChar, adParamInput, 50, otherOrgType)
		   .parameters.Append .CreateParameter("@servicesProvided", adVarChar, adParamInput, 50, servicesProvided)		   
		   .parameters.Append .CreateParameter("@servicesProvided2", adVarChar, adParamInput, 50, servicesProvided2)
		   .parameters.Append .CreateParameter("@servicesProvided3", adVarChar, adParamInput, 50, servicesProvided3)
		   .parameters.Append .CreateParameter("@servicesProvided4", adVarChar, adParamInput, 50, servicesProvided4)
		   .parameters.Append .CreateParameter("@servicesProvided5", adVarChar, adParamInput, 50, servicesProvided5)
		   .parameters.Append .CreateParameter("@servicesProvided6", adVarChar, adParamInput, 50, servicesProvided6)
		   .parameters.Append .CreateParameter("@servicesProvided7", adVarChar, adParamInput, 50, servicesProvided7)
		   .parameters.Append .CreateParameter("@servicesProvided8", adVarChar, adParamInput, 50, servicesProvided8)
		   .parameters.Append .CreateParameter("@servicesProvided9", adVarChar, adParamInput, 50, servicesProvided9)
		   .parameters.Append .CreateParameter("@servicesProvided10", adVarChar, adParamInput, 50, servicesProvided10)		   		   
		   .parameters.Append .CreateParameter("@EEOAAPStatement", adVarChar, adParamInput, 10, EEOAAPStatement)
		   .parameters.Append .CreateParameter("@I9Acknowledgement", adVarChar, adParamInput, 10, I9Acknowledgement)
		   .parameters.Append .CreateParameter("@nonDiscriminationPolicy", adVarChar, adParamInput, 10, nonDiscriminationPolicy)
		   .parameters.Append .CreateParameter("@nonSegregationPolicy", adVarChar, adParamInput, 10, nonSegregationPolicy)		   
		   .parameters.Append .CreateParameter("@NAICSCode", adVarChar, adParamInput, 50, NAICSCode)
		   .parameters.Append .CreateParameter("@NAICSCode2", adVarChar, adParamInput, 50, NAICSCode2)
		   .parameters.Append .CreateParameter("@NAICSCode3", adVarChar, adParamInput, 50, NAICSCode3)
		   .parameters.Append .CreateParameter("@NAICSCode4", adVarChar, adParamInput, 50, NAICSCode4)
		   .parameters.Append .CreateParameter("@NAICSCode5", adVarChar, adParamInput, 50, NAICSCode5)
		   .parameters.Append .CreateParameter("@NAICSCode6", adVarChar, adParamInput, 50, NAICSCode6)
		   .parameters.Append .CreateParameter("@businessClassification", adVarChar, adParamInput, 50, businessClassification)		   
		   .parameters.Append .CreateParameter("@smallBusiness", adBoolean, adParamInput, 1, isOn(smallBusiness))
		   .parameters.Append .CreateParameter("@womanOwnedSmallBusiness", adBoolean, adParamInput, 1, isOn(womanOwnedSmallBusiness))
		   .parameters.Append .CreateParameter("@HUBZone", adBoolean, adParamInput, 1, isOn(HUBZone))
		   .parameters.Append .CreateParameter("@VOSB", adBoolean, adParamInput, 1, isOn(VOSB))
		   .parameters.Append .CreateParameter("@SDVOSB", adBoolean, adParamInput, 1, isOn(SDVOSB))
		   .parameters.Append .CreateParameter("@SDB", adBoolean, adParamInput, 1, isOn(SDB))
		   .parameters.Append .CreateParameter("@DBE", adBoolean, adParamInput, 1, isOn(DBE))
		   .parameters.Append .CreateParameter("@otherClassification", adBoolean, adParamInput, 1, isOn(otherClassification))
		   .parameters.Append .CreateParameter("@otherClassificationExplain", adVarChar, adParamInput, 1000, otherClassificationExplain)		   
		   .parameters.Append .CreateParameter("@companyCity", adVarChar, adParamInput, 50, companyCity)
		   .parameters.Append .CreateParameter("@companyState", adVarChar, adParamInput, 50, companyState)
		   .parameters.Append .CreateParameter("@companyZip", adVarChar, adParamInput, 50, companyZip)
		   .parameters.Append .CreateParameter("@name", adVarChar, adParamInput, 50, name)
		   .parameters.Append .CreateParameter("@title", adVarChar, adParamInput, 50, title)
		   .parameters.Append .CreateParameter("@phoneNumber", adVarChar, adParamInput, 50, phoneNumber)
		   .parameters.Append .CreateParameter("@dateSigned", adVarChar, adParamInput, 50, dateSigned)
		   .CommandType = adCmdStoredProc
		End With
		
				
		Set rs = oCmd.Execute
		
		subVendorID = rs("Identity")
				
		Set oCmd = nothing
		
		'build the PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildCompliancePDF.asp?subVendorID=" & subVendorID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/compliance_" & subVendorID & ".pdf"), true )
		
		rs.close
		set rs = nothing
		
		response.Redirect "subVendorList.asp"
	
	case "preCertifyReport"
		reportID = Request("reportID")
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")		
		
		'delete the workOrder items
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spPreCertifyReport"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		'route to the administrators
		notifyAdmintoCertify reportID
		
		response.Redirect "OSHAReportList.asp?findReport=" & reportID
		
	case "certifyReport"
		reportID = Request("reportID")
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")		
		
		'delete the workOrder items
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spCertifyReport"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "OSHAReportList.asp?findReport=" & reportID

	Case "editOSHASub"
		subID = request("subID")
		reportID = request("reportID")
		clientID = Session("clientID")
		
		'if add new sub is checked, re-direct back to the sub form
		addNewSub = request("addNewSub")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")

		if request("subName") <> "" then
		
			'add the sub and return the ID
			Set oCmd = Server.CreateObject("ADODB.Command")		
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spEditOSHASub"
			   .parameters.Append .CreateParameter("@subID", adInteger, adParamInput, 8, subID)
			   .parameters.Append .CreateParameter("@subName", adVarchar, adParamInput, 100, request("subName"))
			   .CommandType = adCmdStoredProc
			End With
					
			Set rs = oCmd.Execute											
			Set oCmd = nothing

			'add the questions for this sub based on the sub ID and reportID
			for each item in Request.Form
				if Left(item,15) = "sub_question_ID" then
					ii = cint(replace(Item,"sub_question_ID",""))
					
					Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spEditOSHASubQuestion"
					   .parameters.Append .CreateParameter("@OSHASubID", adInteger, adParamInput, 8, ii)
					   .parameters.Append .CreateParameter("@answer", adInteger, adParamInput, 8, request("sub_question_answer_" & ii))
					   .parameters.Append .CreateParameter("@comment", adVarchar, adParamInput, 1000, request("sub_question_comment_" & ii))
					   .CommandType = adCmdStoredProc
					End With
							
					Set rs = oCmd.Execute												
					Set oCmd = nothing						
				end if
			next
			
		end if
		
		'edit the extra sub questions and answers
		' the extra sub questions that are added
		for each item in Request.Form
			if Left(item,23) = "sub_question@answerEdit" then
				i = cint(replace(Item,"sub_question@answerEdit",""))	
						
				
				'response.Write request("customer_question@answer" & i) & "<br><br>"
				Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spEditExtraOSHASubQuestion"
					   .parameters.Append .CreateParameter("@OSHASubID", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@question", adVarchar, adParamInput, 1000, trim(request("sub_question@answerEdit" & i)))
					   .CommandType = adCmdStoredProc
					End With
						
					Set rs = oCmd.Execute					
					Set oCmd = nothing

			end if
		next
			
			
		'add the extra sub-contractor questions that are added
		'may need to revisit this if bugs occur
		if request("subName") <> "" then
			for each item in Request.Form
				iNum = 20
				if Left(item,iNum) = "sub_question@answer_" then
					iID = replace(Item,"sub_question@answer_","")
					if request("sub_question@answer_" & iID) <> "" then
						Set oCmd = Server.CreateObject("ADODB.Command")		
						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spAddOSHASubQuestion"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
						   .parameters.Append .CreateParameter("@subID", adInteger, adParamInput, 8, subID)
						   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, null)
						   .parameters.Append .CreateParameter("@question", adVarchar, adParamInput, 1000, request("sub_question@answer_" & iID))
						   .parameters.Append .CreateParameter("@answer", adInteger, adParamInput, 8, null)
						   .parameters.Append .CreateParameter("@comment", adVarchar, adParamInput, 1000, null)
						   .parameters.Append .CreateParameter("@isExtraAnswer", adBoolean, adParamInput, 1, true)
						   .CommandType = adCmdStoredProc
						End With
								
						Set rs = oCmd.Execute												
						Set oCmd = nothing	
					end if
				end if
			next
		end if	
	
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If			
		
		'build the unbroken report PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMockOSHAPDFFull.asp?reportID=" & reportID & "&clientID=" & clientID', "landscape=true"	
		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "Ent.pdf"), true )			
		Set Pdf = Nothing
	
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMockOSHAPDF.asp?reportID=" & reportID & "&clientID=" & clientID', "landscape=true"	
		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "Gen.pdf"), true )			
		Set Pdf = Nothing
		
		
		'check to see if there are subs
		'attach the subs to the master
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spCheckForSubs"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsSubs = oCmd.Execute
		Set oCmd = nothing	
		
		if not rsSubs.eof then
			'response.Write "yes"
			iSubs = 0
			do until rsSubs.eof
				iSubs = iSubs + 1
				'create the pdf for each sub here
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Pdf.RegKey = sRegKey
				Set Doc = Pdf.CreateDocument
				Doc.ImportFromUrl sPDFPath & "buildMockOSHASubPDF.asp?reportID=" & reportID & "&clientID=" & clientID & "&subID=" & rsSubs("subID")', "landscape=true"	
				Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "_" & rsSubs("subID") & ".pdf"), true )			
				Set Pdf = Nothing		
		
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "gen.pdf") )
				Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "_" & rsSubs("subID") & ".pdf") )
				Doc1.AppendDocument Doc2
				Filename = Doc1.Save( Server.MapPath("downloads/Report_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
				
			rsSubs.movenext
			loop			
			
		end if
		
		
		
'		Set Pdf = Server.CreateObject("Persits.Pdf")
'		Pdf.RegKey = sRegKey
'		Set Doc = Pdf.CreateDocument
'		Doc.ImportFromUrl sPDFPath & "buildMockOSHASubPDF.asp?reportID=" & reportID & "&clientID=" & clientID & "&subID=" & subID', "landscape=true"	
'		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "_" & subID & ".pdf"), true )			
'		Set Pdf = Nothing		
'		
'		Set Pdf = Server.CreateObject("Persits.Pdf")
'		Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "Gen.pdf") )
'		Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "_" & subID & ".pdf") )
'		Doc1.AppendDocument Doc2
'		Filename = Doc1.Save( Server.MapPath("downloads/Report_" & reportID & ".pdf"), true )
'		Set Pdf = Nothing
		
		'route the report to the selected people
		'separate the user id's
		routeUsers = split(Request.Form ("routeUsers"), ",")
		if routeUsers <> "" then
			for i = 0 to ubound(routeUsers)
					routeReport reportID,routeUsers(i)
			next
		end if	
		
	
		're-direct to add new if flag is set
		if addNewSub = "on" then
			response.Redirect "form.asp?formType=addOSHASub&reportID=" & reportID
		else
			'redirect to the report list
			response.Redirect "subReportList.asp?reportID=" & reportID
		end if

	Case "addOSHASub"
	
		reportID = request("reportID")
		clientID = Session("clientID")
		
		'if add new sub is checked, re-direct back to the sub form
		addNewSub = request("addNewSub")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		

		'add the sub-contractor info
		'this will be up to 10 sub-contractors
	'	for i = 1 to 10
			if request("subName") <> "" then
			
				'add the sub and return the ID
				Set oCmd = Server.CreateObject("ADODB.Command")		
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spAddOSHASub"
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@subName", adVarchar, adParamInput, 100, request("subName"))
				   .CommandType = adCmdStoredProc
				End With
						
				Set rs = oCmd.Execute				
				subID = rs("Identity")								
				Set oCmd = nothing

				'add the questions for this sub based on the sub ID and reportID
				for each item in Request.Form
					if Left(item,15) = "sub_question_ID" then
						ii = cint(replace(Item,"sub_question_ID",""))
						
						Set oCmd = Server.CreateObject("ADODB.Command")		
						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spAddOSHASubQuestion"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
						   .parameters.Append .CreateParameter("@subID", adInteger, adParamInput, 8, subID)
						   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, ii)
						   .parameters.Append .CreateParameter("@question", adVarchar, adParamInput, 1000, request("sub_question_question_" & ii))
						   .parameters.Append .CreateParameter("@answer", adInteger, adParamInput, 8, request("sub_question_answer_" & ii))
						   .parameters.Append .CreateParameter("@comment", adVarchar, adParamInput, 1000, request("sub_question_comment_" & ii))
						   .parameters.Append .CreateParameter("@isExtraAnswer", adBoolean, adParamInput, 1, false)
						   .CommandType = adCmdStoredProc
						End With
								
						Set rs = oCmd.Execute												
						Set oCmd = nothing						
					end if
				next
				
			end if
			
			
			'add the extra sub-contractor questions that are added
			'may need to revisit this if bugs occur
			if request("subName") <> "" then
				for each item in Request.Form
					iNum = 20
					if Left(item,iNum) = "sub_question@answer_" then
						iID = replace(Item,"sub_question@answer_","")
						if request("sub_question@answer_" & iID) <> "" then
							Set oCmd = Server.CreateObject("ADODB.Command")		
							With oCmd
							   .ActiveConnection = DataConn
							   .CommandText = "spAddOSHASubQuestion"
							   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
							   .parameters.Append .CreateParameter("@subID", adInteger, adParamInput, 8, subID)
							   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, null)
							   .parameters.Append .CreateParameter("@question", adVarchar, adParamInput, 1000, request("sub_question@answer_" & iID))
							   .parameters.Append .CreateParameter("@answer", adInteger, adParamInput, 8, null)
							   .parameters.Append .CreateParameter("@comment", adVarchar, adParamInput, 1000, null)
							   .parameters.Append .CreateParameter("@isExtraAnswer", adBoolean, adParamInput, 1, true)
							   .CommandType = adCmdStoredProc
							End With
									
							Set rs = oCmd.Execute												
							Set oCmd = nothing	
						'	response.Write iID & "<br>"
						'	response.Write request("sub_question@answer" & i & "_" & iID) & "<br><br>"
						end if
					end if
				next
			end if
						
	'	next	
	
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If		
		
		'build the unbroken report PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMockOSHAPDFFull.asp?reportID=" & reportID & "&clientID=" & clientID', "landscape=true"	
		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "Ent.pdf"), true )			
		Set Pdf = Nothing		
	
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMockOSHAPDF.asp?reportID=" & reportID & "&clientID=" & clientID', "landscape=true"	
		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "Gen.pdf"), true )			
		Set Pdf = Nothing
		
		'check to see if there are subs
		'attach the subs to the master
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spCheckForSubs"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsSubs = oCmd.Execute
		Set oCmd = nothing	
		
		if not rsSubs.eof then
			'response.Write "yes"
			iSubs = 0
			do until rsSubs.eof
				iSubs = iSubs + 1
				'create the pdf for each sub here
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Pdf.RegKey = sRegKey
				Set Doc = Pdf.CreateDocument
				Doc.ImportFromUrl sPDFPath & "buildMockOSHASubPDF.asp?reportID=" & reportID & "&clientID=" & clientID & "&subID=" & rsSubs("subID")', "landscape=true"	
				Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "_" & rsSubs("subID") & ".pdf"), true )			
				Set Pdf = Nothing		
		
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "gen.pdf") )
				Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "_" & rsSubs("subID") & ".pdf") )
				Doc1.AppendDocument Doc2
				Filename = Doc1.Save( Server.MapPath("downloads/Report_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
				
			rsSubs.movenext
			loop			
			
		end if
		
	'	Set Pdf = Server.CreateObject("Persits.Pdf")
	'	Pdf.RegKey = sRegKey
	'	Set Doc = Pdf.CreateDocument
	'	Doc.ImportFromUrl sPDFPath & "buildMockOSHASubPDF.asp?reportID=" & reportID & "&clientID=" & clientID & "&subID=" & subID', "landscape=true"	
	'	Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "_" & subID & ".pdf"), true )			
	'	Set Pdf = Nothing		
	'	
	'	Set Pdf = Server.CreateObject("Persits.Pdf")
	'	Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "Gen.pdf") )
	'	Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "_" & subID & ".pdf") )
	'	Doc1.AppendDocument Doc2
	'	Filename = Doc1.Save( Server.MapPath("downloads/Report_" & reportID & ".pdf"), true )
	'	Set Pdf = Nothing
		
		'route the report to the selected people
		'separate the user id's
		routeUsers = split(Request.Form ("routeUsers"), ",")
		if routeUsers <> "" then
			for i = 0 to ubound(routeUsers)
					routeReport reportID,routeUsers(i)
			next
		end if	
	
		're-direct to add new if flag is set
		if addNewSub = "on" then
			response.Redirect "form.asp?formType=addOSHASub&reportID=" & reportID
		else
			'redirect to the report list
			response.Redirect "subReportList.asp?reportID=" & reportID
		end if
	
	Case "addOSHAReport"
	
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		customerID = request("customerID")
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		reportDate = request("reportDate")
		inspectionDate = request("inspectionDate")
		jobsiteStatus = request("jobsiteStatus")
		attendWalkAround = request("attendWalkAround")
		mainIssues = request("rte1")
		reportType = request("reportType")
		
		'response.Write reportType
			
		clientID = Session("clientID")
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddOSHAReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@reportDate", adDBTimeStamp, adParamInput, 8, reportDate)		
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@jobsiteStatus", adVarChar, adParamInput, 2000, jobsiteStatus)
		   .parameters.Append .CreateParameter("@attendWalkAround", adVarChar, adParamInput, 100, attendWalkAround)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@mainIssues", adVarChar, adParamInput, 8000, mainIssues)
		   .parameters.Append .CreateParameter("@reportType", adInteger, adParamInput, 8, reportType)
		   .parameters.Append .CreateParameter("@preCertified", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@Certified", adBoolean, adParamInput, 1, False)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
	
		Set oCmd = nothing
		rs.close
		set rs = nothing

		'if add general items is checked
		'add the general items and the added items
		'to the generalOSHA table
		addGeneralToReport = request("addGeneralToReport")
		if addGeneralToReport = "on" then
			For Each Item in Request.Form
				if Left(item,11) = "genquestion" then
					'get the question and add to the database along with the reportID					
					Set oCmd = Server.CreateObject("ADODB.Command")
		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spAddOSHAGeneralReminder"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@reminderQuestion", adVarchar, adParamInput, 1000, request(item))
					   .CommandType = adCmdStoredProc
					End With
						
					Set rs = oCmd.Execute					
					Set oCmd = nothing
				
				end if
			next
		end if
		
		for each item in Request.Form
		'get the id of the question
			if Left(item,17) = "customer_question" then
				if Left(item,26) = "customer_question_question" then
					i = cint(replace(Item,"customer_question_question",""))					
					Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spAddOSHACustomerQuestion"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@question", adVarchar, adParamInput, 1000, request("customer_question_question" & i))
					   .parameters.Append .CreateParameter("@answer", adInteger, adParamInput, 8, request("customer_question_answer" & i))
					   .parameters.Append .CreateParameter("@comment", adVarchar, adParamInput, 1000, request("customer_question_comment" & i))
					   .parameters.Append .CreateParameter("@isExtraAnswer", adBoolean, adParamInput, 1, false)
					   .CommandType = adCmdStoredProc
					End With
							
					Set rs = oCmd.Execute					
					Set oCmd = nothing
				end if
			end if
		next
		
		'add the extra customer questions that are added
		for each item in Request.Form
			if Left(item,24) = "customer_question@answer" then
				i = cint(replace(Item,"customer_question@answer",""))				
				
				'response.Write request("customer_question@answer" & i) & "<br><br>"
				Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spAddOSHACustomerQuestion"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, null)
					   .parameters.Append .CreateParameter("@question", adVarchar, adParamInput, 1000, trim(request("customer_question@answer" & i)))
					   .parameters.Append .CreateParameter("@answer", adInteger, adParamInput, 8, null)
					   .parameters.Append .CreateParameter("@comment", adVarchar, adParamInput, 1000, null)
					   .parameters.Append .CreateParameter("@isExtraAnswer", adBoolean, adParamInput, 1, true)
					   .CommandType = adCmdStoredProc
					End With
						
					Set rs = oCmd.Execute					
					Set oCmd = nothing

			end if
		next


		logDetails Session("ID"),reportID,"Report Created","OSHA Report and PDF document Created"		
		
		'build the report PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMockOSHAPDF.asp?reportID=" & reportID & "&clientID=" & clientID', "landscape=true"	
		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & ".pdf"), true )			
		Set Pdf = Nothing
		
		'build the unbroken report PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMockOSHAPDFFull.asp?reportID=" & reportID & "&clientID=" & clientID', "landscape=true"	
		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "Ent.pdf"), true )			
		Set Pdf = Nothing
		
		'route the report to the selected people
		'separate the user id's
		routeUsers = split(Request.Form ("routeUsers"), ",")
		if routeUsers <> "" then
			for i = 0 to ubound(routeUsers)
					routeReport reportID,routeUsers(i)
			next
		end if	
		
		
		

		response.Redirect "OSHAReportList.asp?findReport=" & reportID
		
	Case "editOSHAReport"
	
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		reportID = request("reportID")
		jobsiteStatus = request("jobsiteStatus")
		attendWalkAround = request("attendWalkAround")
		mainIssues = request("rte1")
			
		clientID = Session("clientID")
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditOSHAReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@jobsiteStatus", adVarChar, adParamInput, 2000, jobsiteStatus)
		   .parameters.Append .CreateParameter("@attendWalkAround", adVarChar, adParamInput, 100, attendWalkAround)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@mainIssues", adVarChar, adParamInput, 8000, mainIssues)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
	
		Set oCmd = nothing
	'	rs.close
	'	set rs = nothing
		
		If Err Then
		%>
		12	<!--#include file="includes/FatalError.inc"-->
		<%
		End If

		'if add general items is checked
		'add the general items and the added items
		'to the generalOSHA table
		addGeneralToReport = request("addGeneralToReport")
		'delete all of the general questions
		Set oCmd = Server.CreateObject("ADODB.Command")		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteGeneralQuestions"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsDelGen = oCmd.Execute
		Set oCmd = nothing

		
		if addGeneralToReport = "on" then
			For Each Item in Request.Form
				if Left(item,11) = "genquestion" then
					'get the question and add to the database along with the reportID					
					Set oCmd = Server.CreateObject("ADODB.Command")
		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spAddOSHAGeneralReminder"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@reminderQuestion", adVarchar, adParamInput, 1000, request(item))
					   .CommandType = adCmdStoredProc
					End With
						
					Set rs = oCmd.Execute					
					Set oCmd = nothing
				
				end if
			next
		end if
		
		
		for each item in Request.Form
		'get the id of the question
		'edit the questions
			if Left(item,17) = "customer_question" then
				if Left(item,26) = "customer_question_question" then
					i = cint(replace(Item,"customer_question_question",""))					
					Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spEditOSHACustomerQuestion"
					   .parameters.Append .CreateParameter("@OSHACustomerID", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@question", adVarchar, adParamInput, 1000, request("customer_question_question" & i))
					   .parameters.Append .CreateParameter("@answer", adInteger, adParamInput, 8, request("customer_question_answer" & i))
					   .parameters.Append .CreateParameter("@comment", adVarchar, adParamInput, 1000, request("customer_question_comment" & i))
					   .CommandType = adCmdStoredProc
					End With
							
					Set rs = oCmd.Execute					
					Set oCmd = nothing
				end if
			end if
		next
		
		'edit the extra customer questions and answers
		' the extra customer questions that are added
		for each item in Request.Form
			if Left(item,28) = "customer_question@answerEdit" then
				i = cint(replace(Item,"customer_question@answerEdit",""))				
				
				'response.Write request("customer_question@answer" & i) & "<br><br>"
				Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spEditExtraOSHACustomerQuestion"
					   .parameters.Append .CreateParameter("@OSHACustomerID", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@question", adVarchar, adParamInput, 1000, trim(request("customer_question@answerEdit" & i)))
					   .CommandType = adCmdStoredProc
					End With
						
					Set rs = oCmd.Execute					
					Set oCmd = nothing

			end if
		next
		
		' the extra customer questions that are added
		for each item in Request.Form
			if Left(item,24) = "customer_question@answer" then
				i = cint(replace(Item,"customer_question@answer",""))				
				
				'response.Write request("customer_question@answer" & i) & "<br><br>"
				Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spAddOSHACustomerQuestion"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, null)
					   .parameters.Append .CreateParameter("@question", adVarchar, adParamInput, 1000, trim(request("customer_question@answer" & i)))
					   .parameters.Append .CreateParameter("@answer", adInteger, adParamInput, 8, null)
					   .parameters.Append .CreateParameter("@comment", adVarchar, adParamInput, 1000, null)
					   .parameters.Append .CreateParameter("@isExtraAnswer", adBoolean, adParamInput, 1, true)
					   .CommandType = adCmdStoredProc
					End With
						
					Set rs = oCmd.Execute					
					Set oCmd = nothing

			end if
		next
		

		logDetails Session("ID"),reportID,"Report Edited","OSHA Report and PDF document Edited"
		
		'build the unbroken report PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMockOSHAPDFFull.asp?reportID=" & reportID & "&clientID=" & clientID', "landscape=true"	
		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "Ent.pdf"), true )			
		Set Pdf = Nothing		
		
		'build the report PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMockOSHAPDF.asp?reportID=" & reportID & "&clientID=" & clientID', "landscape=true"	
		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "gen.pdf"), true )			
		Set Pdf = Nothing
		
		'build the report PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMockOSHAPDF.asp?reportID=" & reportID & "&clientID=" & clientID', "landscape=true"	
		Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & ".pdf"), true )			
		Set Pdf = Nothing
		
		
		'check to see if there are subs
		'attach the subs to the master
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spCheckForSubs"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsSubs = oCmd.Execute
		Set oCmd = nothing	
		
		if not rsSubs.eof then
			'response.Write "yes"
			iSubs = 0
			do until rsSubs.eof
				iSubs = iSubs + 1
				'create the pdf for each sub here
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Pdf.RegKey = sRegKey
				Set Doc = Pdf.CreateDocument
				Doc.ImportFromUrl sPDFPath & "buildMockOSHASubPDF.asp?reportID=" & reportID & "&clientID=" & clientID & "&subID=" & rsSubs("subID")', "landscape=true"	
				Filename = Doc.Save( Server.MapPath("downloads/Report_" & reportID & "_" & rsSubs("subID") & ".pdf"), true )			
				Set Pdf = Nothing		
		
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "gen.pdf") )
				Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/Report_" & reportID & "_" & rsSubs("subID") & ".pdf") )
				Doc1.AppendDocument Doc2
				Filename = Doc1.Save( Server.MapPath("downloads/Report_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
				
			rsSubs.movenext
			loop			
			
		end if
		
		routeUsers = split(Request.Form ("routeUsers"), ",")
		if routeUsers <> "" then
			for i = 0 to ubound(routeUsers)
					routeReport reportID,routeUsers(i)
			next
		end if	

		response.Redirect "OSHAReportList.asp?findReport=" & reportID
	
	case "addSub"
		customerID = request("customerID")
		subContractorName = request("subContractorName")
		subContractorType = request("subContractorType")
		contactFirstName = request("contactFirstName")
		contactLastName = request("contactLastName")
		contactEmail = request("contactEmail")
		address1 = request("address1")
		address2 = request("address2")
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		phone = request("phone")
		fax = request("fax")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddSub"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@subContractorName", adVarChar, adParamInput, 200, subContractorName)
		   .parameters.Append .CreateParameter("@subContractorType", adVarChar, adParamInput, 50, subContractorType)
		   .parameters.Append .CreateParameter("@contactFirstName", adVarChar, adParamInput, 100, contactFirstName)
		   .parameters.Append .CreateParameter("@contactLastName", adVarChar, adParamInput, 100, contactLastName)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 150, contactEmail)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 200, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 200, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@phone", adVarChar, adParamInput, 50, phone)
		   .parameters.Append .CreateParameter("@fax", adVarChar, adParamInput, 50, fax)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
			
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "subList.asp?id=" & customerID
		
	case "editSub"
		customerID = request("customerID")
		subContractorID = request("subContractorID")
		subContractorName = request("subContractorName")
		subContractorType = request("subContractorType")
		contactFirstName = request("contactFirstName")
		contactLastName = request("contactLastName")
		contactEmail = request("contactEmail")
		address1 = request("address1")
		address2 = request("address2")
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		phone = request("phone")
		fax = request("fax")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditSub"
		   .parameters.Append .CreateParameter("@subContractorID", adInteger, adParamInput, 8, subContractorID)
		   .parameters.Append .CreateParameter("@subContractorName", adVarChar, adParamInput, 200, subContractorName)
		   .parameters.Append .CreateParameter("@subContractorType", adVarChar, adParamInput, 50, subContractorType)
		   .parameters.Append .CreateParameter("@contactFirstName", adVarChar, adParamInput, 100, contactFirstName)
		   .parameters.Append .CreateParameter("@contactLastName", adVarChar, adParamInput, 100, contactLastName)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 150, contactEmail)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 200, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 200, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@phone", adVarChar, adParamInput, 50, phone)
		   .parameters.Append .CreateParameter("@fax", adVarChar, adParamInput, 50, fax)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
			
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "subList.asp?id=" & customerID
		
	case "deleteSub"
		subContractorID = Request("id")
		customerID = request("customerID")
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")		
		
		'delete the workOrder items
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteSub"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, subContractorID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "subList.asp?id=" & customerID
	
	case "addOSHAQuestion"
		question = request("question")
		YesAnswer = request("YesAnswer")
		NoAnswer = request("NoAnswer")
		NAAnswer = request("NAAnswer")
		NotInspAnswer = request("NotInspAnswer")		
		sortNumber = request("sortNumber")
		GeneralReminder = request("GeneralReminder")
		subQuestion = request("subQuestion")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddOSHAQuestion"
		   .parameters.Append .CreateParameter("@question", adVarChar, adParamInput, 1000, question)
		   .parameters.Append .CreateParameter("@YesAnswer", adVarChar, adParamInput, 1000, YesAnswer)
		   .parameters.Append .CreateParameter("@NoAnswer", adVarChar, adParamInput, 1000, NoAnswer)
		   .parameters.Append .CreateParameter("@NAAnswer", adVarChar, adParamInput, 1000, NAAnswer)
		   .parameters.Append .CreateParameter("@NotInspAnswer", adVarChar, adParamInput, 1000, NotInspAnswer)		   
		   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, sortNumber)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)	
		   .parameters.Append .CreateParameter("@GeneralReminder", adBoolean, adParamInput, 1, isOn(GeneralReminder))
		   .parameters.Append .CreateParameter("@subQuestion", adBoolean, adParamInput, 1, isOn(subQuestion)) 
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID) 
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "OSHAquestionList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "editOSHAQuestion"
		questionID = request("questionID")
		question = request("question")
		YesAnswer = request("YesAnswer")
		NoAnswer = request("NoAnswer")
		NAAnswer = request("NAAnswer")
		NotInspAnswer = request("NotInspAnswer")		
		sortNumber = request("sortNumber")
		GeneralReminder = request("GeneralReminder")
		subQuestion = request("subQuestion")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditOSHAQuestion"
		   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, questionID)
		   .parameters.Append .CreateParameter("@question", adVarChar, adParamInput, 1000, question)
		   .parameters.Append .CreateParameter("@YesAnswer", adVarChar, adParamInput, 1000, YesAnswer)
		   .parameters.Append .CreateParameter("@NoAnswer", adVarChar, adParamInput, 1000, NoAnswer)
		   .parameters.Append .CreateParameter("@NAAnswer", adVarChar, adParamInput, 1000, NAAnswer)
		   .parameters.Append .CreateParameter("@NotInspAnswer", adVarChar, adParamInput, 1000, NotInspAnswer)		   
		   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, sortNumber)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)	
		   .parameters.Append .CreateParameter("@GeneralReminder", adBoolean, adParamInput, 1, isOn(GeneralReminder))
		   .parameters.Append .CreateParameter("@subQuestion", adBoolean, adParamInput, 1, isOn(subQuestion)) 
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID) 
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "OSHAquestionList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "deleteOSHAQuestion"
		questionID = Request("id")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")				
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteOSHAQuestion"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, questionID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "OSHAquestionList.asp?id=" & divisionID & "&customerID=" & customerID
	
	Case "restoreOSHADefaults"
		customerID = request("customerID")
		divisionID = request("divisionID")		
	
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'delete the current questions
		Set oCmd = Server.CreateObject("ADODB.Command")		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteOSHAQuestionByDivision"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc	   
		End With					
		Set rsDelQuestions = oCmd.Execute
		Set oCmd = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetDefaultOSHAQuestions"
		   .CommandType = adCmdStoredProc	   
		End With
					
		Set rsDefaultOSHA = oCmd.Execute
		Set oCmd = nothing
		
		Do until rsDefaultOSHA.eof
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddOSHAQuestion"
			   .parameters.Append .CreateParameter("@question", adVarChar, adParamInput, 1000, rsDefaultOSHA("question"))
			   .parameters.Append .CreateParameter("@YesAnswer", adVarChar, adParamInput, 1000, rsDefaultOSHA("YesAnswer"))
			   .parameters.Append .CreateParameter("@NoAnswer", adVarChar, adParamInput, 1000, rsDefaultOSHA("NoAnswer"))
			   .parameters.Append .CreateParameter("@NAAnswer", adVarChar, adParamInput, 1000, rsDefaultOSHA("NAAnswer"))
			   .parameters.Append .CreateParameter("@NotInspAnswer", adVarChar, adParamInput, 1000, rsDefaultOSHA("NotInspAnswer"))		   
			   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, rsDefaultOSHA("sortNumber"))
			   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)	
			   .parameters.Append .CreateParameter("@GeneralReminder", adBoolean, adParamInput, 1, rsDefaultOSHA("GeneralReminder"))
			   .parameters.Append .CreateParameter("@subQuestion", adBoolean, adParamInput, 1, rsDefaultOSHA("subQuestion")) 
			   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID) 
			   .CommandType = adCmdStoredProc
			End With
					
			Set rsOSHA = oCmd.Execute
		rsDefaultOSHA.movenext
		loop

		
		response.Redirect "OSHAquestionList.asp?id=" & divisionID & "&customerID=" & customerID
	
	Case "editOSHA301"
	
		customerID = request("customerID")
		reportID = trim(request("reportID"))
		userID = trim(request("userID"))
		sname = trim(request("name"))
		jobTitle = trim(request("jobTitle"))
		street = trim(request("street"))
		city = trim(request("city"))
		state = trim(request("state"))
		zip = trim(request("zip"))
		dateOfBirthMonth = trim(request("dateOfBirthMonth"))
		dateOfBirthDay = trim(request("dateOfBirthDay"))
		dateOfBirthYear = trim(request("dateOfBirthYear"))
		dateHiredMonth = trim(request("dateHiredMonth"))
		dateHiredDay = trim(request("dateHiredDay"))
		dateHiredYear = trim(request("dateHiredYear"))
		sex = trim(request("sex"))
		physicianName = trim(request("physicianName"))
		facility = trim(request("facility"))
		facilityStreet = trim(request("facilityStreet"))
		facilityCity = trim(request("facilityCity"))
		facilityState = trim(request("facilityState"))
		facilityZip = trim(request("facilityZip"))
		emergencyRoom = trim(request("emergencyRoom"))
		hospOvernight = trim(request("hospOvernight"))
		caseNumber = trim(request("caseNumber"))
		dateOfInjuryMonth = trim(request("dateOfInjuryMonth"))
		dateOfInjuryDay = trim(request("dateOfInjuryDay"))
		dateOfInjuryYear = trim(request("dateOfInjuryYear"))
		beganWork = trim(request("beganWork"))
		eventTime = trim(request("eventTime"))
		timeNotDetermined = request("timeNotDetermined")
		doingBefore = trim(request("doingBefore"))
		whatHappened = trim(request("whatHappened"))
		whatWasInjury = trim(request("whatWasInjury"))
		whatObject = trim(request("whatObject"))
		dateOfDeathMonth = trim(request("dateOfDeathMonth"))
		dateOfDeathDay = trim(request("dateOfDeathDay"))
		dateOfDeathYear = trim(request("dateOfDeathYear"))
		
		classifyCase = checkNull(trim(request("classifyCase")))
		daysAwayFromWork = checkNull(trim(request("daysAwayFromWork")))
		daysOnJobTransfer = checkNull(trim(request("daysOnJobTransfer")))
		injury = checkNull(trim(request("injury")))
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditOSHA301"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@name", adVarchar, adParamInput, 50, sname)	
		   .parameters.Append .CreateParameter("@jobTitle", adVarchar, adParamInput, 50, jobTitle)	   
		   .parameters.Append .CreateParameter("@street", adVarchar, adParamInput, 50, street)
		   .parameters.Append .CreateParameter("@city", adVarchar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarchar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@zip", adVarchar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@dateOfBirthMonth", adVarchar, adParamInput, 2, dateOfBirthMonth)
		   .parameters.Append .CreateParameter("@dateOfBirthDay", adVarchar, adParamInput, 2, dateOfBirthDay)
		   .parameters.Append .CreateParameter("@dateOfBirthYear", adVarchar, adParamInput, 4, dateOfBirthYear)
		   .parameters.Append .CreateParameter("@dateHiredMonth", adVarchar, adParamInput, 2, dateHiredMonth)
		   .parameters.Append .CreateParameter("@dateHiredDay", adVarchar, adParamInput, 2, dateHiredDay)
		   .parameters.Append .CreateParameter("@dateHiredYear", adVarchar, adParamInput, 4, dateHiredYear)
		   .parameters.Append .CreateParameter("@sex", adVarchar, adParamInput, 50, sex)
		   .parameters.Append .CreateParameter("@physicianName", adVarchar, adParamInput, 50, physicianName)		   
		   .parameters.Append .CreateParameter("@facility", adVarchar, adParamInput, 50, facility)
		   .parameters.Append .CreateParameter("@facilityStreet", adVarchar, adParamInput, 50, facilityStreet)
		   .parameters.Append .CreateParameter("@facilityCity", adVarchar, adParamInput, 50, facilityCity)
		   .parameters.Append .CreateParameter("@facilityState", adVarchar, adParamInput, 50, facilityState)
		   .parameters.Append .CreateParameter("@facilityZip", adVarchar, adParamInput, 50, facilityZip)
		   .parameters.Append .CreateParameter("@emergencyRoom", adVarchar, adParamInput, 50, emergencyRoom)
		   .parameters.Append .CreateParameter("@hospOvernight", adVarchar, adParamInput, 50, hospOvernight)		   
		   .parameters.Append .CreateParameter("@caseNumber", adVarchar, adParamInput, 50, caseNumber)
		   .parameters.Append .CreateParameter("@dateOfInjuryMonth", adVarchar, adParamInput, 2, dateOfInjuryMonth)
		   .parameters.Append .CreateParameter("@dateOfInjuryDay", adVarchar, adParamInput, 2, dateOfInjuryDay)
		   .parameters.Append .CreateParameter("@dateOfInjuryYear", adVarchar, adParamInput, 4, dateOfInjuryYear)
		   .parameters.Append .CreateParameter("@beganWork", adVarchar, adParamInput, 50, beganWork)
		   .parameters.Append .CreateParameter("@eventTime", adVarchar, adParamInput, 50, eventTime)
		   .parameters.Append .CreateParameter("@timeNotDetermined", adBoolean, adParamInput, 1, isOn(timeNotDetermined))
		   .parameters.Append .CreateParameter("@doingBefore", adVarchar, adParamInput, 500, doingBefore)
		   .parameters.Append .CreateParameter("@whatHappened", adVarchar, adParamInput, 500, whatHappened)
		   .parameters.Append .CreateParameter("@whatWasInjury", adVarchar, adParamInput, 500, whatWasInjury)
		   .parameters.Append .CreateParameter("@whatObject", adVarchar, adParamInput, 500, whatObject)
		   .parameters.Append .CreateParameter("@dateOfDeathMonth", adVarchar, adParamInput, 2, dateOfDeathMonth)
		   .parameters.Append .CreateParameter("@dateOfDeathDay", adVarchar, adParamInput, 2, dateOfDeathDay)
		   .parameters.Append .CreateParameter("@dateOfDeathYear", adVarchar, adParamInput, 4, dateOfDeathYear)	
		   .parameters.Append .CreateParameter("@classifyCase", adInteger, adParamInput, 8, classifyCase)
		   .parameters.Append .CreateParameter("@daysAwayFromWork", adVarchar, adParamInput, 50, daysAwayFromWork)
		   .parameters.Append .CreateParameter("@daysOnJobTransfer", adVarchar, adParamInput, 50, daysOnJobTransfer)
		   .parameters.Append .CreateParameter("@injury", adInteger, adParamInput, 8, injury)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute													
		Set oCmd = nothing	
		
		If Err Then
		%>
		1	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'create the pdf

		Set PDF = Server.CreateObject("Persits.PdfManager")
		PDF.RegKey = sRegKey
		Set Doc = PDF.OpenDocument( Server.MapPath( "pdfTemplates/OSHA301.pdf" ) )
		Set Page = Doc.Pages(1)	
		Set Param = PDF.CreateParam
		Set Font = Doc.Fonts("Helvetica-Bold")

		'y = larger the number the higher on page
		'x = larger the n
		
		' Name
		Param.Add("x=340, y=481, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText sname, Param, Font
		Page.Canvas.RestoreState
		
		' street
		Param.Add("x=326, y=458, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText street, Param, Font
		Page.Canvas.RestoreState
		
		' city
		Param.Add("x=323, y=435, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText city, Param, Font
		Page.Canvas.RestoreState
		
		' state
		Param.Add("x=473, y=435, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText state, Param, Font
		Page.Canvas.RestoreState
		
		' zip
		Param.Add("x=519, y=435, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText zip, Param, Font
		Page.Canvas.RestoreState
		
		' date of birth month
		Param.Add("x=352, y=413, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfBirthMonth, Param, Font
		Page.Canvas.RestoreState
		
		' date of birth day
		Param.Add("x=376, y=413, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfBirthDay, Param, Font
		Page.Canvas.RestoreState
		
		' date of birth year
		Param.Add("x=397, y=413, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfBirthYear, Param, Font
		Page.Canvas.RestoreState
		
		' date hired month
		Param.Add("x=352, y=399, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateHiredMonth, Param, Font
		Page.Canvas.RestoreState
		
		' date hired day
		Param.Add("x=376, y=399, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateHiredDay, Param, Font
		Page.Canvas.RestoreState
		
		' date hired year
		Param.Add("x=397, y=399, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateHiredYear, Param, Font
		Page.Canvas.RestoreState
		
		'sex
		select case sex
			case "Male"
				Param.Add("x=305, y=383, size=9")
			Case "Female"
				Param.Add("x=305, y=371, size=9")
		end select		
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText "x", Param, Font
		Page.Canvas.RestoreState
		
		' physician Name
		Param.Add("x=305, y=268, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText physicianName, Param, Font
		Page.Canvas.RestoreState
		
		' facility
		Param.Add("x=331, y=228, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facility, Param, Font
		Page.Canvas.RestoreState
		
		' facility street
		Param.Add("x=336, y=205, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facilityStreet, Param, Font
		Page.Canvas.RestoreState
		
		' facility city
		Param.Add("x=324, y=182, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facilityCity, Param, Font
		Page.Canvas.RestoreState
		
		' facility state
		Param.Add("x=473, y=182, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facilityState, Param, Font
		Page.Canvas.RestoreState
		
		' facility zip
		Param.Add("x=519, y=182, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facilityZip, Param, Font
		Page.Canvas.RestoreState
		
		'emergency room
		select case emergencyRoom
			case "Yes"
				Param.Add("x=305, y=152, size=9")
			Case "No"
				Param.Add("x=305, y=140, size=9")
		end select		
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText "x", Param, Font
		Page.Canvas.RestoreState
		
		'hospital overnight
		select case hospOvernight
			case "Yes"
				Param.Add("x=305, y=107, size=9")
			Case "No"
				Param.Add("x=305, y=95, size=9")
		end select		
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText "x", Param, Font
		Page.Canvas.RestoreState
		
		' case number
		Param.Add("x=700, y=481, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText caseNumber, Param, Font
		Page.Canvas.RestoreState
		
		' date of injury month
		Param.Add("x=705, y=466, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfInjuryMonth, Param, Font
		Page.Canvas.RestoreState
		
		' date of injury Day
		Param.Add("x=730, y=466, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfInjuryDay, Param, Font
		Page.Canvas.RestoreState
		
		' date of injury Year
		Param.Add("x=754, y=466, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfInjuryYear, Param, Font
		Page.Canvas.RestoreState
		
		' began work
		Param.Add("x=700, y=452, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText beganWork, Param, Font
		Page.Canvas.RestoreState
		
		' time of event
		Param.Add("x=700, y=432, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText eventTime, Param, Font
		Page.Canvas.RestoreState
		
		' time Not Determined
		if timeNotDetermined = "on" then
			Param.Add("x=816, y=434, size=9, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText "x", Param, Font
			Page.Canvas.RestoreState
		end if
		
		
		' doing before
		doingBefore = "<u>" & doingBefore & "</u>"
		Param.Add("x=608, y=370, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText doingBefore, Param, Font
		Page.Canvas.RestoreState
		
		' what happened
		whatHappened = "<u>" & whatHappened & "</u>"
		Param.Add("x=608, y=290, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText whatHappened, Param, Font
		Page.Canvas.RestoreState
		
		' what was injury
		whatWasInjury = "<u>" & whatWasInjury & "</u>"
		Param.Add("x=608, y=210, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText whatWasInjury, Param, Font
		Page.Canvas.RestoreState
		
		' what object
		whatObject = "<u>" & whatObject & "</u>"
		Param.Add("x=608, y=140, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText whatObject, Param, Font
		Page.Canvas.RestoreState
		
		' date of death month
		Param.Add("x=833, y=89, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfDeathMonth, Param, Font
		Page.Canvas.RestoreState
		
		' date of death day
		Param.Add("x=858, y=89, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfDeathDay, Param, Font
		Page.Canvas.RestoreState
		
		' date of death year
		Param.Add("x=880, y=89, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfDeathYear, Param, Font
		Page.Canvas.RestoreState
		
		'get the user that filled it out
		Set oCmd = Server.CreateObject("ADODB.Command")		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUser"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		end with
		
		Set rs = oCmd.Execute
		
		' completed by
		Param.Add("x=80, y=144, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rs("firstName") & " " & rs("lastName"), Param, Font
		Page.Canvas.RestoreState
		
		' title
		Param.Add("x=42, y=117, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rs("jobTitle"), Param, Font
		Page.Canvas.RestoreState
		
		' phone
		Param.Add("x=49, y=91, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rs("cellPhone"), Param, Font
		Page.Canvas.RestoreState
		
		' date month
		Param.Add("x=208, y=91, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText month(now()), Param, Font
		Page.Canvas.RestoreState
		
		' date day
		Param.Add("x=230, y=91, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText day(now()), Param, Font
		Page.Canvas.RestoreState
		
		' date year
		Param.Add("x=250, y=91, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText year(now()), Param, Font
		Page.Canvas.RestoreState
		

		FileName = Doc.Save( Server.MapPath("downloads/OSHA301_" & reportID & ".pdf"), True)

		Set Page = Nothing
		Set Doc = Nothing
		Set Pdf = Nothing
		
				
		
		'*******add the pdf log*****************************
		'get the customer
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetCustomer"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
		   .CommandType = adCmdStoredProc
		   
		End With		
		Set rsCustomer = oCmd.Execute
		Set oCmd = nothing
	
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetOSHA301ByYearAndCustomer"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@Year", adVarchar, adParamInput, 50, dateOfInjuryYear)
		   .CommandType = adCmdStoredProc   
		End With
		
		Set rsOSHA = oCmd.Execute
		Set oCmd = nothing
		
		
		
		Set PDF = Server.CreateObject("Persits.PdfManager")
		PDF.RegKey = sRegKey
		Set Doc = PDF.OpenDocument( Server.MapPath( "pdfTemplates/OSHA300Log.pdf" ) )
		Set Page = Doc.Pages(1)	
		Set Param = PDF.CreateParam
		Set Font = Doc.Fonts("Helvetica-Bold")
		
		'year
		Param.Add("x=914, y=577, size=13, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText right(dateOfInjuryYear,2), Param, Font
		Page.Canvas.RestoreState
		
		'customer name
		Param.Add("x=850, y=508, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("customerName"), Param, Font
		Page.Canvas.RestoreState
		
		'customer city
		Param.Add("x=803, y=490, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("city"), Param, Font
		Page.Canvas.RestoreState
		
		'customer state
		Param.Add("x=930, y=490, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("state"), Param, Font
		Page.Canvas.RestoreState
		
		
		'this is where we loop the recordset and
		'write on the pdf the data
		'get the list of osha 301 reports	
		y = "366"
		
		i = 1
		
		do until rsOSHA.eof
		
			
	
			' case number
			Param.Add("x=22, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("caseNumber"), Param, Font
			Page.Canvas.RestoreState
			
			' employee name
			Param.Add("x=55, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("name"), Param, Font
			Page.Canvas.RestoreState
			
			' job title
			Param.Add("x=178, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("jobTitle"), Param, Font
			Page.Canvas.RestoreState
			
			' date of injury month
			Param.Add("x=246, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("dateOfInjuryMonth"), Param, Font
			Page.Canvas.RestoreState
			
			' date of injury day
			Param.Add("x=262, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("dateOfInjuryDay"), Param, Font
			Page.Canvas.RestoreState
			
			' event occured
			Param.Add("x=302, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("doingBefore"), Param, Font
			Page.Canvas.RestoreState
			
			' describe injury
			Param.Add("x=403, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("whatWasInjury"), Param, Font
			Page.Canvas.RestoreState
			
			'classify case
			select case rsOSHA("classifyCase")
				case "1"
					Param.Add("x=617, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
								
				Case "2"
					Param.Add("x=653, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
					
				Case "3"
					Param.Add("x=697, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
				
				Case "4"
					Param.Add("x=743, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
				
				Case else
				
			end select
			
			'num days away from work
			daysAwayFromWork = rsOSHA("daysAwayFromWork")
			if daysAwayFromWork = "" then
				daysAwayFromWork = 0
			end if
			if isnull(daysAwayFromWork) then
				daysAwayFromWork = 0
			end if
			Param.Add("x=788, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText daysAwayFromWork, Param, Font
			Page.Canvas.RestoreState
			
			'num on job transfer
			daysOnJobTransfer = rsOSHA("daysOnJobTransfer")
			if daysOnJobTransfer = "" then
				daysOnJobTransfer = 0
			end if
			if isnull(daysOnJobTransfer) then
				daysOnJobTransfer = 0
			end if
			Param.Add("x=828, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText daysOnJobTransfer, Param, Font
			Page.Canvas.RestoreState
			
			
			'injury
			select case rsOSHA("injury")
				case "1"
					Param.Add("x=881, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
								
				Case "2"
					Param.Add("x=901, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
					
				Case "3"
					Param.Add("x=921, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
				
				Case "4"
					Param.Add("x=941, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
					
				Case "5"
					Param.Add("x=961, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
					
				Case "6"
					Param.Add("x=981, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
				
				Case else
				
			end select
		
		rsOSHA.movenext
		
			'decrease the Y corordinate 
			y = y - 21
			
		loop
		
		FileName = Doc.Save( Server.MapPath("downloads/OSHA300Log_" & dateOfInjuryYear & "_" & customerID & ".pdf"), True)
		
		'******end of the log***************************
		
		'*******add the 300A form*****************************
		
		Set PDF = Server.CreateObject("Persits.PdfManager")
		PDF.RegKey = sRegKey
		Set Doc = PDF.OpenDocument( Server.MapPath( "pdfTemplates/OSHA300A.pdf" ) )
		Set Page = Doc.Pages(1)	
		Set Param = PDF.CreateParam
		Set Font = Doc.Fonts("Helvetica-Bold")
		
		'year
		Param.Add("x=919, y=577, size=13, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText right(sYear,2), Param, Font
		Page.Canvas.RestoreState
		
		'customer/establishment name
		Param.Add("x=745, y=470, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("customerName"), Param, Font
		Page.Canvas.RestoreState
		
		'street
		Param.Add("x=690, y=449, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("address1"), Param, Font
		Page.Canvas.RestoreState
		
		'customer city
		Param.Add("x=690, y=432, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("city"), Param, Font
		Page.Canvas.RestoreState
		
		'customer state
		Param.Add("x=826, y=432, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("state"), Param, Font
		Page.Canvas.RestoreState
		
		'customer zip
		Param.Add("x=869, y=432, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("zip"), Param, Font
		Page.Canvas.RestoreState
		
		'customer/industry type
		Param.Add("x=690, y=384, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("customerType"), Param, Font
		Page.Canvas.RestoreState
		
		'SIC 1
		Param.Add("x=697, y=356, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("SIC1"), Param, Font
		Page.Canvas.RestoreState
	
		'SIC 2
		Param.Add("x=716, y=356, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("SIC2"), Param, Font
		Page.Canvas.RestoreState
		
		'SIC 3
		Param.Add("x=736, y=356, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("SIC3"), Param, Font
		Page.Canvas.RestoreState
		
		'SIC 4
		Param.Add("x=756, y=356, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("SIC4"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 1
		Param.Add("x=697, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS1"), Param, Font
		Page.Canvas.RestoreState
	
		'NAICS 2
		Param.Add("x=716, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS2"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 3
		Param.Add("x=736, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS3"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 4
		Param.Add("x=756, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS4"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 5
		Param.Add("x=776, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS5"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 6
		Param.Add("x=796, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS6"), Param, Font
		Page.Canvas.RestoreState
		
		'Annual num employees
		Param.Add("x=826, y=252, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("annualAverageEmployees"), Param, Font
		Page.Canvas.RestoreState
		
		'total hours
		Param.Add("x=826, y=231, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("totalHours"), Param, Font
		Page.Canvas.RestoreState
		
		'********enter totals here*****************
		'will need to get totals from the database
		iTotDeaths = 0
		iTotCasesAway = 0
		iTotTransfer = 0
		iTotOtherCases = 0
		daysAwayFromWork = 0
		daysOnJobTransfer = 0
		iTotInjury = 0
		iTotSkinDisorders = 0
		iTotRespitory = 0
		iTotPoison = 0
		iTotHearing = 0
		iTotOtherIllness = 0
		
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetOSHA301ByYearAndCustomer"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@Year", adVarchar, adParamInput, 50, sYear)
		   .CommandType = adCmdStoredProc   
		End With
		
		Set rsOSHA = oCmd.Execute
		Set oCmd = nothing
		
		do until rsOSHA.eof
			select case rsOSHA("classifyCase")
				Case "1"
					iTotDeaths = iTotDeaths + 1
				Case "2"
					iTotCasesAway = iTotCasesAway + 1
				Case "3"
					iTotTransfer = iTotTransfer + 1
				Case "4"
					iTotOtherCases = iTotOtherCases + 1
			end select
			
			daysAwayFromWork = daysAwayFromWork + cint(rsOSHA("daysAwayFromWork"))
			daysOnJobTransfer = daysOnJobTransfer + cint(rsOSHA("daysOnJobTransfer"))
			
			select case rsOSHA("injury")
				Case "1"
					iTotInjury = iTotInjury + 1
				Case "2"
					iTotSkinDisorders = iTotSkinDisorders + 1
				Case "3"
					iTotRespitory = iTotRespitory + 1
				Case "4"
					iTotPoison = iTotPoison + 1
				Case "5"
					iTotHearing = iTotHearing + 1
				Case "6"
					iTotOtherIllness = iTotOtherIllness + 1
			end select
		rsOSHA.movenext
		loop
		
		'total deaths
		Param.Add("x=57, y=353, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotDeaths, Param, Font
		Page.Canvas.RestoreState
		
		'total cases away
		Param.Add("x=151, y=353, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotCasesAway, Param, Font
		Page.Canvas.RestoreState
		
		'total transfer
		Param.Add("x=241, y=353, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotTransfer, Param, Font
		Page.Canvas.RestoreState
		
		'total other cases
		Param.Add("x=351, y=353, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotOtherCases, Param, Font
		Page.Canvas.RestoreState
		
		'numdays away from work
		Param.Add("x=60, y=244, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText daysAwayFromWork, Param, Font
		Page.Canvas.RestoreState
		
		'numdays on job transfer
		Param.Add("x=220, y=244, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText daysOnJobTransfer, Param, Font
		Page.Canvas.RestoreState
		
		
		'injury
		Param.Add("x=168, y=157, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotInjury, Param, Font
		Page.Canvas.RestoreState
		
		'skin disorders
		Param.Add("x=168, y=129, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotSkinDisorders, Param, Font
		Page.Canvas.RestoreState
		
		'respitory
		Param.Add("x=168, y=112, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotRespitory, Param, Font
		Page.Canvas.RestoreState
		
		'poison
		Param.Add("x=376, y=157, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotPoison, Param, Font
		Page.Canvas.RestoreState
		
		'hearing
		Param.Add("x=376, y=141, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotHearing, Param, Font
		Page.Canvas.RestoreState
		
		'other illnesses
		Param.Add("x=376, y=125, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotOtherIllness, Param, Font
		Page.Canvas.RestoreState
		
	
		
		FileName = Doc.Save( Server.MapPath("downloads/OSHA300A_" & sYear & "_" & customerID & ".pdf"), True)
		
		Set PDF = nothing
		
		'******end of 300A form***************************
		
		
		'send to the log
		response.Redirect "OSHA301Log.asp?customerID=" & customerID & "&year=" & year(now())
		
	Case "addOSHA301"
		customerID = trim(request("customerID"))
		userID = trim(request("userID"))
		sname = trim(request("name"))
		jobTitle = trim(request("jobTitle"))
		street = trim(request("street"))
		city = trim(request("city"))
		state = trim(request("state"))
		zip = trim(request("zip"))
		dateOfBirthMonth = trim(request("dateOfBirthMonth"))
		dateOfBirthDay = trim(request("dateOfBirthDay"))
		dateOfBirthYear = trim(request("dateOfBirthYear"))
		dateHiredMonth = trim(request("dateHiredMonth"))
		dateHiredDay = trim(request("dateHiredDay"))
		dateHiredYear = trim(request("dateHiredYear"))
		sex = trim(request("sex"))
		physicianName = trim(request("physicianName"))
		facility = trim(request("facility"))
		facilityStreet = trim(request("facilityStreet"))
		facilityCity = trim(request("facilityCity"))
		facilityState = trim(request("facilityState"))
		facilityZip = trim(request("facilityZip"))
		emergencyRoom = trim(request("emergencyRoom"))
		hospOvernight = trim(request("hospOvernight"))
		caseNumber = trim(request("caseNumber"))
		dateOfInjuryMonth = trim(request("dateOfInjuryMonth"))
		dateOfInjuryDay = trim(request("dateOfInjuryDay"))
		dateOfInjuryYear = trim(request("dateOfInjuryYear"))
		beganWork = trim(request("beganWork"))
		eventTime = trim(request("eventTime"))
		timeNotDetermined = request("timeNotDetermined")
		doingBefore = trim(request("doingBefore"))
		whatHappened = trim(request("whatHappened"))
		whatWasInjury = trim(request("whatWasInjury"))
		whatObject = trim(request("whatObject"))
		dateOfDeathMonth = trim(request("dateOfDeathMonth"))
		dateOfDeathDay = trim(request("dateOfDeathDay"))
		dateOfDeathYear = trim(request("dateOfDeathYear"))
		
		classifyCase = checkNull(trim(request("classifyCase")))
		daysAwayFromWork = checkNull(trim(request("daysAwayFromWork")))
		daysOnJobTransfer = checkNull(trim(request("daysOnJobTransfer")))
		injury = checkNull(trim(request("injury")))
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddOSHA301"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@name", adVarchar, adParamInput, 50, sname)	
		   .parameters.Append .CreateParameter("@jobTitle", adVarchar, adParamInput, 50, jobTitle)	   
		   .parameters.Append .CreateParameter("@street", adVarchar, adParamInput, 50, street)
		   .parameters.Append .CreateParameter("@city", adVarchar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarchar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@zip", adVarchar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@dateOfBirthMonth", adVarchar, adParamInput, 2, dateOfBirthMonth)
		   .parameters.Append .CreateParameter("@dateOfBirthDay", adVarchar, adParamInput, 2, dateOfBirthDay)
		   .parameters.Append .CreateParameter("@dateOfBirthYear", adVarchar, adParamInput, 4, dateOfBirthYear)
		   .parameters.Append .CreateParameter("@dateHiredMonth", adVarchar, adParamInput, 2, dateHiredMonth)
		   .parameters.Append .CreateParameter("@dateHiredDay", adVarchar, adParamInput, 2, dateHiredDay)
		   .parameters.Append .CreateParameter("@dateHiredYear", adVarchar, adParamInput, 4, dateHiredYear)
		   .parameters.Append .CreateParameter("@sex", adVarchar, adParamInput, 50, sex)
		   .parameters.Append .CreateParameter("@physicianName", adVarchar, adParamInput, 50, physicianName)		   
		   .parameters.Append .CreateParameter("@facility", adVarchar, adParamInput, 50, facility)
		   .parameters.Append .CreateParameter("@facilityStreet", adVarchar, adParamInput, 50, facilityStreet)
		   .parameters.Append .CreateParameter("@facilityCity", adVarchar, adParamInput, 50, facilityCity)
		   .parameters.Append .CreateParameter("@facilityState", adVarchar, adParamInput, 50, facilityState)
		   .parameters.Append .CreateParameter("@facilityZip", adVarchar, adParamInput, 50, facilityZip)
		   .parameters.Append .CreateParameter("@emergencyRoom", adVarchar, adParamInput, 50, emergencyRoom)
		   .parameters.Append .CreateParameter("@hospOvernight", adVarchar, adParamInput, 50, hospOvernight)		   
		   .parameters.Append .CreateParameter("@caseNumber", adVarchar, adParamInput, 50, caseNumber)
		   .parameters.Append .CreateParameter("@dateOfInjuryMonth", adVarchar, adParamInput, 2, dateOfInjuryMonth)
		   .parameters.Append .CreateParameter("@dateOfInjuryDay", adVarchar, adParamInput, 2, dateOfInjuryDay)
		   .parameters.Append .CreateParameter("@dateOfInjuryYear", adVarchar, adParamInput, 4, dateOfInjuryYear)
		   .parameters.Append .CreateParameter("@beganWork", adVarchar, adParamInput, 50, beganWork)
		   .parameters.Append .CreateParameter("@eventTime", adVarchar, adParamInput, 50, eventTime)
		   .parameters.Append .CreateParameter("@timeNotDetermined", adBoolean, adParamInput, 1, isOn(timeNotDetermined))
		   .parameters.Append .CreateParameter("@doingBefore", adVarchar, adParamInput, 500, doingBefore)
		   .parameters.Append .CreateParameter("@whatHappened", adVarchar, adParamInput, 500, whatHappened)
		   .parameters.Append .CreateParameter("@whatWasInjury", adVarchar, adParamInput, 500, whatWasInjury)
		   .parameters.Append .CreateParameter("@whatObject", adVarchar, adParamInput, 500, whatObject)
		   .parameters.Append .CreateParameter("@dateOfDeathMonth", adVarchar, adParamInput, 2, dateOfDeathMonth)
		   .parameters.Append .CreateParameter("@dateOfDeathDay", adVarchar, adParamInput, 2, dateOfDeathDay)
		   .parameters.Append .CreateParameter("@dateOfDeathYear", adVarchar, adParamInput, 4, dateOfDeathYear)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())	
		   .parameters.Append .CreateParameter("@classifyCase", adInteger, adParamInput, 8, classifyCase)
		   .parameters.Append .CreateParameter("@daysAwayFromWork", adVarchar, adParamInput, 50, daysAwayFromWork)
		   .parameters.Append .CreateParameter("@daysOnJobTransfer", adVarchar, adParamInput, 50, daysOnJobTransfer)
		   .parameters.Append .CreateParameter("@injury", adInteger, adParamInput, 8, injury)
		   .CommandType = adCmdStoredProc
		End With
		
				
		Set rs = oCmd.Execute	
		reportID = rs("Identity")												
		Set oCmd = nothing
	
		response.Write reportID
		
		
		If Err Then
		%>
		1	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'create the pdf

		Set PDF = Server.CreateObject("Persits.PdfManager")
		PDF.RegKey = sRegKey
		Set Doc = PDF.OpenDocument( Server.MapPath( "pdfTemplates/OSHA301.pdf" ) )
		Set Page = Doc.Pages(1)	
		Set Param = PDF.CreateParam
		Set Font = Doc.Fonts("Helvetica-Bold")

		'y = larger the number the higher on page
		'x = larger the n
		
		' Name
		Param.Add("x=340, y=481, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText sname, Param, Font
		Page.Canvas.RestoreState
		
		' street
		Param.Add("x=326, y=458, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText street, Param, Font
		Page.Canvas.RestoreState
		
		' city
		Param.Add("x=323, y=435, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText city, Param, Font
		Page.Canvas.RestoreState
		
		' state
		Param.Add("x=473, y=435, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText state, Param, Font
		Page.Canvas.RestoreState
		
		' zip
		Param.Add("x=519, y=435, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText zip, Param, Font
		Page.Canvas.RestoreState
		
		' date of birth month
		Param.Add("x=352, y=413, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfBirthMonth, Param, Font
		Page.Canvas.RestoreState
		
		' date of birth day
		Param.Add("x=376, y=413, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfBirthDay, Param, Font
		Page.Canvas.RestoreState
		
		' date of birth year
		Param.Add("x=397, y=413, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfBirthYear, Param, Font
		Page.Canvas.RestoreState
		
		' date hired month
		Param.Add("x=352, y=399, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateHiredMonth, Param, Font
		Page.Canvas.RestoreState
		
		' date hired day
		Param.Add("x=376, y=399, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateHiredDay, Param, Font
		Page.Canvas.RestoreState
		
		' date hired year
		Param.Add("x=397, y=399, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateHiredYear, Param, Font
		Page.Canvas.RestoreState
		
		'sex
		select case sex
			case "Male"
				Param.Add("x=305, y=383, size=9")
			Case "Female"
				Param.Add("x=305, y=371, size=9")
		end select		
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText "x", Param, Font
		Page.Canvas.RestoreState
		
		' physician Name
		Param.Add("x=305, y=268, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText physicianName, Param, Font
		Page.Canvas.RestoreState
		
		' facility
		Param.Add("x=331, y=228, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facility, Param, Font
		Page.Canvas.RestoreState
		
		' facility street
		Param.Add("x=336, y=205, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facilityStreet, Param, Font
		Page.Canvas.RestoreState
		
		' facility city
		Param.Add("x=324, y=182, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facilityCity, Param, Font
		Page.Canvas.RestoreState
		
		' facility state
		Param.Add("x=473, y=182, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facilityState, Param, Font
		Page.Canvas.RestoreState
		
		' facility zip
		Param.Add("x=519, y=182, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText facilityZip, Param, Font
		Page.Canvas.RestoreState
		
		'emergency room
		select case emergencyRoom
			case "Yes"
				Param.Add("x=305, y=152, size=9")
			Case "No"
				Param.Add("x=305, y=140, size=9")
		end select		
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText "x", Param, Font
		Page.Canvas.RestoreState
		
		'hospital overnight
		select case hospOvernight
			case "Yes"
				Param.Add("x=305, y=107, size=9")
			Case "No"
				Param.Add("x=305, y=95, size=9")
		end select		
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText "x", Param, Font
		Page.Canvas.RestoreState
		
		' case number
		Param.Add("x=700, y=481, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText caseNumber, Param, Font
		Page.Canvas.RestoreState
		
		' date of injury month
		Param.Add("x=705, y=466, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfInjuryMonth, Param, Font
		Page.Canvas.RestoreState
		
		' date of injury Day

		Param.Add("x=730, y=466, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfInjuryDay, Param, Font
		Page.Canvas.RestoreState
		
		' date of injury Year
		Param.Add("x=754, y=466, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfInjuryYear, Param, Font
		Page.Canvas.RestoreState
		
		' began work
		Param.Add("x=700, y=452, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText beganWork, Param, Font
		Page.Canvas.RestoreState
		
		' time of event
		Param.Add("x=700, y=432, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText eventTime, Param, Font
		Page.Canvas.RestoreState
		
		' time Not Determined
		if timeNotDetermined = "on" then
			Param.Add("x=816, y=434, size=9, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText "x", Param, Font
			Page.Canvas.RestoreState
		end if
		
		
		' doing before
		doingBefore = "<u>" & doingBefore & "</u>"
		Param.Add("x=608, y=370, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText doingBefore, Param, Font
		Page.Canvas.RestoreState
		
		' what happened
		whatHappened = "<u>" & whatHappened & "</u>"
		Param.Add("x=608, y=290, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText whatHappened, Param, Font
		Page.Canvas.RestoreState
		
		' what was injury
		whatWasInjury = "<u>" & whatWasInjury & "</u>"
		Param.Add("x=608, y=210, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText whatWasInjury, Param, Font
		Page.Canvas.RestoreState
		
		' what object
		whatObject = "<u>" & whatObject & "</u>"
		Param.Add("x=608, y=140, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText whatObject, Param, Font
		Page.Canvas.RestoreState
		
		' date of death month
		Param.Add("x=833, y=89, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfDeathMonth, Param, Font
		Page.Canvas.RestoreState
		
		' date of death day
		Param.Add("x=858, y=89, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfDeathDay, Param, Font
		Page.Canvas.RestoreState
		
		' date of death year
		Param.Add("x=880, y=89, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText dateOfDeathYear, Param, Font
		Page.Canvas.RestoreState
		
		'get the user that filled it out
		Set oCmd = Server.CreateObject("ADODB.Command")		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUser"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		end with
		
		Set rs = oCmd.Execute
		
		' completed by
		Param.Add("x=80, y=144, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rs("firstName") & " " & rs("lastName"), Param, Font
		Page.Canvas.RestoreState
		
		' title
		Param.Add("x=42, y=117, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rs("jobTitle"), Param, Font
		Page.Canvas.RestoreState
		
		' phone
		Param.Add("x=49, y=91, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rs("cellPhone"), Param, Font
		Page.Canvas.RestoreState
		
		' date month
		Param.Add("x=208, y=91, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText month(now()), Param, Font
		Page.Canvas.RestoreState
		
		' date day
		Param.Add("x=230, y=91, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText day(now()), Param, Font
		Page.Canvas.RestoreState
		
		' date year
		Param.Add("x=250, y=91, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText year(now()), Param, Font
		Page.Canvas.RestoreState
		

		FileName = Doc.Save( Server.MapPath("downloads/OSHA301_" & reportID & ".pdf"), True)

		Set Page = Nothing
		Set Doc = Nothing
		Set Pdf = Nothing
		
				
		
		'*******add the pdf log*****************************
		'get the customer
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetCustomer"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
		   .CommandType = adCmdStoredProc
		   
		End With		
		Set rsCustomer = oCmd.Execute
		Set oCmd = nothing
	
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetOSHA301ByYearAndCustomer"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@Year", adVarchar, adParamInput, 50, dateOfInjuryYear)
		   .CommandType = adCmdStoredProc   
		End With
		
		Set rsOSHA = oCmd.Execute
		Set oCmd = nothing
		
		
		
		Set PDF = Server.CreateObject("Persits.PdfManager")
		PDF.RegKey = sRegKey
		Set Doc = PDF.OpenDocument( Server.MapPath( "pdfTemplates/OSHA300Log.pdf" ) )
		Set Page = Doc.Pages(1)	
		Set Param = PDF.CreateParam
		Set Font = Doc.Fonts("Helvetica-Bold")
		
		'year
		Param.Add("x=914, y=577, size=13, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText right(dateOfInjuryYear,2), Param, Font
		Page.Canvas.RestoreState
		
		'customer name
		Param.Add("x=850, y=508, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("customerName"), Param, Font
		Page.Canvas.RestoreState
		
		'customer city
		Param.Add("x=803, y=490, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("city"), Param, Font
		Page.Canvas.RestoreState
		
		'customer state
		Param.Add("x=930, y=490, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("state"), Param, Font
		Page.Canvas.RestoreState
		
		
		'this is where we loop the recordset and
		'write on the pdf the data
		'get the list of osha 301 reports	
		y = "366"
		
		i = 1
		
		do until rsOSHA.eof
		
			
	
			' case number
			Param.Add("x=22, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("caseNumber"), Param, Font
			Page.Canvas.RestoreState
			
			' employee name
			Param.Add("x=55, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("name"), Param, Font
			Page.Canvas.RestoreState
			
			' job title
			Param.Add("x=178, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("jobTitle"), Param, Font
			Page.Canvas.RestoreState
			
			' date of injury month
			Param.Add("x=246, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("dateOfInjuryMonth"), Param, Font
			Page.Canvas.RestoreState
			
			' date of injury day
			Param.Add("x=262, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("dateOfInjuryDay"), Param, Font
			Page.Canvas.RestoreState
			
			' event occured
			Param.Add("x=302, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("doingBefore"), Param, Font
			Page.Canvas.RestoreState
			
			' describe injury
			Param.Add("x=403, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText rsOSHA("whatWasInjury"), Param, Font
			Page.Canvas.RestoreState
			
			'classify case
			select case rsOSHA("classifyCase")
				case "1"
					Param.Add("x=617, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
								
				Case "2"
					Param.Add("x=653, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
					
				Case "3"
					Param.Add("x=697, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
				
				Case "4"
					Param.Add("x=743, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
				
				Case else
				
			end select
			
			'num days away from work
			daysAwayFromWork = rsOSHA("daysAwayFromWork")
			if daysAwayFromWork = "" then
				daysAwayFromWork = 0
			end if
			if isnull(daysAwayFromWork) then
				daysAwayFromWork = 0
			end if
			Param.Add("x=788, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText daysAwayFromWork, Param, Font
			Page.Canvas.RestoreState
			
			'num on job transfer
			daysOnJobTransfer = rsOSHA("daysOnJobTransfer")
			if daysOnJobTransfer = "" then
				daysOnJobTransfer = 0
			end if
			if isnull(daysOnJobTransfer) then
				daysOnJobTransfer = 0
			end if
			Param.Add("x=828, y=" & y & ", size=7, html=true")
			Page.Canvas.SaveState
			Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
			Page.Canvas.DrawText daysOnJobTransfer, Param, Font
			Page.Canvas.RestoreState
			
			
			'injury
			select case rsOSHA("injury")
				case "1"
					Param.Add("x=881, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
								
				Case "2"
					Param.Add("x=901, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
					
				Case "3"
					Param.Add("x=921, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
				
				Case "4"
					Param.Add("x=941, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
					
				Case "5"
					Param.Add("x=961, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
					
				Case "6"
					Param.Add("x=981, y=" & y & ", size=7, html=true")
					Page.Canvas.SaveState
					Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
					Page.Canvas.DrawText "X", Param, Font
					Page.Canvas.RestoreState
				
				Case else
				
			end select
		
		rsOSHA.movenext
		
			'decrease the Y corordinate 
			y = y - 21
			
		loop
		
		FileName = Doc.Save( Server.MapPath("downloads/OSHA300Log_" & dateOfInjuryYear & "_" & customerID & ".pdf"), True)
		
		'******end of the log***************************
		
		'*******add the 300A form*****************************
		
		Set PDF = Server.CreateObject("Persits.PdfManager")
		PDF.RegKey = sRegKey
		Set Doc = PDF.OpenDocument( Server.MapPath( "pdfTemplates/OSHA300A.pdf" ) )
		Set Page = Doc.Pages(1)	
		Set Param = PDF.CreateParam
		Set Font = Doc.Fonts("Helvetica-Bold")
		
		'year
		Param.Add("x=919, y=577, size=13, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText right(sYear,2), Param, Font
		Page.Canvas.RestoreState
		
		'customer/establishment name
		Param.Add("x=745, y=470, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("customerName"), Param, Font
		Page.Canvas.RestoreState
		
		'street
		Param.Add("x=690, y=449, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("address1"), Param, Font
		Page.Canvas.RestoreState
		
		'customer city
		Param.Add("x=690, y=432, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("city"), Param, Font
		Page.Canvas.RestoreState
		
		'customer state
		Param.Add("x=826, y=432, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("state"), Param, Font
		Page.Canvas.RestoreState
		
		'customer zip
		Param.Add("x=869, y=432, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("zip"), Param, Font
		Page.Canvas.RestoreState
		
		'customer/industry type
		Param.Add("x=690, y=384, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("customerType"), Param, Font
		Page.Canvas.RestoreState
		
		'SIC 1
		Param.Add("x=697, y=356, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("SIC1"), Param, Font
		Page.Canvas.RestoreState
	
		'SIC 2
		Param.Add("x=716, y=356, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("SIC2"), Param, Font
		Page.Canvas.RestoreState
		
		'SIC 3
		Param.Add("x=736, y=356, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("SIC3"), Param, Font
		Page.Canvas.RestoreState
		
		'SIC 4
		Param.Add("x=756, y=356, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("SIC4"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 1
		Param.Add("x=697, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS1"), Param, Font
		Page.Canvas.RestoreState
	
		'NAICS 2
		Param.Add("x=716, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS2"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 3
		Param.Add("x=736, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS3"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 4
		Param.Add("x=756, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS4"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 5
		Param.Add("x=776, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS5"), Param, Font
		Page.Canvas.RestoreState
		
		'NAICS 6
		Param.Add("x=796, y=308, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("NAICS6"), Param, Font
		Page.Canvas.RestoreState
		
		'Annual num employees
		Param.Add("x=826, y=252, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("annualAverageEmployees"), Param, Font
		Page.Canvas.RestoreState
		
		'total hours
		Param.Add("x=826, y=231, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsCustomer("totalHours"), Param, Font
		Page.Canvas.RestoreState
		
		'********enter totals here*****************
		'will need to get totals from the database
		iTotDeaths = 0
		iTotCasesAway = 0
		iTotTransfer = 0
		iTotOtherCases = 0
		daysAwayFromWork = 0
		daysOnJobTransfer = 0
		iTotInjury = 0
		iTotSkinDisorders = 0
		iTotRespitory = 0
		iTotPoison = 0
		iTotHearing = 0
		iTotOtherIllness = 0
		
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetOSHA301ByYearAndCustomer"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@Year", adVarchar, adParamInput, 50, sYear)
		   .CommandType = adCmdStoredProc   
		End With
		
		Set rsOSHA = oCmd.Execute
		Set oCmd = nothing
		
		do until rsOSHA.eof
			select case rsOSHA("classifyCase")
				Case "1"
					iTotDeaths = iTotDeaths + 1
				Case "2"
					iTotCasesAway = iTotCasesAway + 1
				Case "3"
					iTotTransfer = iTotTransfer + 1
				Case "4"
					iTotOtherCases = iTotOtherCases + 1
			end select
			
			daysAwayFromWork = daysAwayFromWork + cint(rsOSHA("daysAwayFromWork"))
			daysOnJobTransfer = daysOnJobTransfer + cint(rsOSHA("daysOnJobTransfer"))
			
			select case rsOSHA("injury")
				Case "1"
					iTotInjury = iTotInjury + 1
				Case "2"
					iTotSkinDisorders = iTotSkinDisorders + 1
				Case "3"
					iTotRespitory = iTotRespitory + 1
				Case "4"
					iTotPoison = iTotPoison + 1
				Case "5"
					iTotHearing = iTotHearing + 1
				Case "6"
					iTotOtherIllness = iTotOtherIllness + 1
			end select
		rsOSHA.movenext
		loop
		
		'total deaths
		Param.Add("x=57, y=353, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotDeaths, Param, Font
		Page.Canvas.RestoreState
		
		'total cases away
		Param.Add("x=151, y=353, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotCasesAway, Param, Font
		Page.Canvas.RestoreState
		
		'total transfer
		Param.Add("x=241, y=353, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotTransfer, Param, Font
		Page.Canvas.RestoreState
		
		'total other cases
		Param.Add("x=351, y=353, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotOtherCases, Param, Font
		Page.Canvas.RestoreState
		
		'numdays away from work
		Param.Add("x=60, y=244, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText daysAwayFromWork, Param, Font
		Page.Canvas.RestoreState
		
		'numdays on job transfer
		Param.Add("x=220, y=244, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText daysOnJobTransfer, Param, Font
		Page.Canvas.RestoreState
		
		
		'injury
		Param.Add("x=168, y=157, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotInjury, Param, Font
		Page.Canvas.RestoreState
		
		'skin disorders
		Param.Add("x=168, y=129, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotSkinDisorders, Param, Font
		Page.Canvas.RestoreState
		
		'respitory
		Param.Add("x=168, y=112, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotRespitory, Param, Font
		Page.Canvas.RestoreState
		
		'poison
		Param.Add("x=376, y=157, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotPoison, Param, Font
		Page.Canvas.RestoreState
		
		'hearing
		Param.Add("x=376, y=141, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotHearing, Param, Font
		Page.Canvas.RestoreState
		
		'other illnesses
		Param.Add("x=376, y=125, size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText iTotOtherIllness, Param, Font
		Page.Canvas.RestoreState
		
	
		
		FileName = Doc.Save( Server.MapPath("downloads/OSHA300A_" & sYear & "_" & customerID & ".pdf"), True)
		
		Set PDF = nothing
		
		'******end of 300A form***************************
		
		
		'send to the log
		response.Redirect "OSHA301Log.asp?customerID=" & customerID & "&year=" & year(now())
	
	case "addJobHistory"
		company = request("company")
		title = request("title")
		responsibilities = request("responsibilities")
		startDate = request("startDate")
		endDate = request("endDate")
		userID = request("userID")
		isUser = request("isUser")
		
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddJobHistory"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@company", adVarChar, adParamInput, 150, company)
		   .parameters.Append .CreateParameter("@title", adVarChar, adParamInput, 150, title)
		   .parameters.Append .CreateParameter("@responsibilities", adVarChar, adParamInput, 500, responsibilities)  
		   .parameters.Append .CreateParameter("@startDate", adVarChar, adParamInput, 50, startDate)  
		   .parameters.Append .CreateParameter("@endDate", adVarChar, adParamInput, 50, endDate)  
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		'take user back to the edit user page and the resume tab
		if isUser = "False" then
			response.Redirect "form.asp?id=" & userID & "&formType=editUser&sMsgJobhistory=True&sec=jobhistory"
		else
			'response.Redirect "form.asp?formType=editUser"
			response.Redirect "form.asp?formType=editUser&sMsgJobhistory=True&sec=jobhistory"
		end if
		
	case "deleteJobHistory"
		jobID = request("id")
		isUser = request("isUser")
		userID = request("userID")
		
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")		
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteJobHistory"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, jobID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		if isUser = "False" then
			response.Redirect "form.asp?id=" & userID & "&formType=editUser&sMsgJobhistory=True&sec=jobhistory"
		else
			'response.Redirect "form.asp?formType=editUser"
			response.Redirect "form.asp?formType=editUser&sMsgJobhistory=True&sec=jobhistory"
		end if
	
	
	case "addEducation"
		school = request("school")
		startDate = request("startDateEdu")
		endDate = request("endDateEdu")
		userID = request("userID")
		isUser = request("isUser")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddEducation"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@school", adVarChar, adParamInput, 150, school)  
		   .parameters.Append .CreateParameter("@startDate", adVarChar, adParamInput, 50, startDate)  
		   .parameters.Append .CreateParameter("@endDate", adVarChar, adParamInput, 50, endDate)  
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		'take user back to the edit user page and the resume tab
		if isUser = "False" then
			response.Redirect "form.asp?id=" & userID & "&formType=editUser&sMsgEducation=True&sec=education"
		else
			'response.Redirect "form.asp?formType=editUser"
			response.Redirect "form.asp?formType=editUser&sMsgEducation=True&sec=education"
		end if
		
	case "deleteEducation"
		educationID = request("id")
		isUser = request("isUser")
		userID = request("userID")
		
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")		
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteEducation"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, educationID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		if isUser = "False" then
			response.Redirect "form.asp?id=" & userID & "&formType=editUser&sMsgEducation=True&sec=education"
		else
			'response.Redirect "form.asp?formType=editUser"
			response.Redirect "form.asp?formType=editUser&sMsgEducation=True&sec=education"
		end if

	case "addCertification"
		certification = request("certification")
		certificationNumber = request("certificationNumber")
		datePassed = request("datePassed")
		expirationDate = request("expirationDate")
		userID = request("userID")
		isUser = request("isUser")
		
		if datePassed = "" then
			datePassed = null
		end if
		
		if expirationDate = "" then
			expirationDate = null
		end if
		
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddCert"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@certification", adVarChar, adParamInput, 150, certification)  
		   .parameters.Append .CreateParameter("@certificationNumber", adVarChar, adParamInput, 150, certificationNumber)  
		   .parameters.Append .CreateParameter("@datePassed", adDBTimeStamp, adParamInput, 8, datePassed)
		   .parameters.Append .CreateParameter("@expirationDate", adDBTimeStamp, adParamInput, 8, expirationDate) 
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		'take user back to the edit user page and the resume tab
		if isUser = "False" then
			response.Redirect "form.asp?id=" & userID & "&formType=editUser&sMsgCerts=True&sec=certs"
		else
			'response.Redirect "form.asp?formType=editUser"
			response.Redirect "form.asp?formType=editUser&sMsgCerts=True&sec=certs"
		end if
		
	case "deleteCertification"
		certificationID = request("id")
		isUser = request("isUser")
		userID = request("userID")
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")		
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteCert"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, certificationID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		if isUser = "False" then
			response.Redirect "form.asp?id=" & userID & "&formType=editUser&sMsgCerts=True&sec=certs"
		else
			'response.Redirect "form.asp?formType=editUser"
			response.Redirect "form.asp?formType=editUser&sMsgCerts=True&sec=certs"
		end if
		
	case "editResume"
		sresume = request("txtresume")
		userID = request("userID")
		isUser = request("isUser")
		
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditResume"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   '.parameters.Append .CreateParameter("@resume", adVarChar, adParamInput, 8000, sresume)
		   .Parameters.Append .CreateParameter("@resume", adLongVarChar, adParamInput, Len(sresume))
		   .Parameters.Item("@resume").AppendChunk sresume   
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		'take user back to the edit user page and the resume tab
		if isUser = "False" then
			response.Redirect "form.asp?id=" & userID & "&formType=editUser&sMsgResume=True&sec=resume"
		else
			'response.Redirect "form.asp?formType=editUser"
			response.Redirect "form.asp?formType=editUser&sMsgResume=True&sec=resume"
		end if
	
	Case "restoreDefaults"
		customerID = request("customerID")
		divisionID = request("divisionID")
		clientID = request("clientID")
		
	
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'delete the current questions
		Set oCmd = Server.CreateObject("ADODB.Command")		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteQuestionsByDivision"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc	   
		End With					
		Set rsDelQuestions = oCmd.Execute
		Set oCmd = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'delete the current categories
		Set oCmd = Server.CreateObject("ADODB.Command")		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteCategoriesByDivision"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc	   
		End With					
		Set rsDelCategories = oCmd.Execute
		Set oCmd = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the question categories from the default categories table
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetDefaultCategories"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .CommandType = adCmdStoredProc	   
		End With
					
		Set rsDefaultCategories = oCmd.Execute
		Set oCmd = nothing
		
		i=1
		do until rsDefaultCategories.eof
			'create new question categories for this new project
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddCategory"
			   .parameters.Append .CreateParameter("@category", adVarChar, adParamInput, 200, rsDefaultCategories("category"))
			   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
			   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rsCategory = oCmd.Execute
			categoryID = rsCategory("Identity")
			
			Set oCmd = nothing
			
			'get all of the default questions for this category
			Set oCmd = Server.CreateObject("ADODB.Command")							
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetDefaultQuestion"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsDefaultCategories("categoryID"))
			   .CommandType = adCmdStoredProc		   
			End With
			Set rsDefaultQuestion = oCmd.Execute
			Set oCmd = nothing
			
			'loop through the questions and insert them into the database
			do until rsDefaultQuestion.eof
			
				Set oCmd = Server.CreateObject("ADODB.Command")
				
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spAddQuestion"
				   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
				   .parameters.Append .CreateParameter("@question", adVarChar, adParamInput, 1000, rsDefaultQuestion("question"))
				   .parameters.Append .CreateParameter("@secondLanguage", adVarChar, adParamInput, 1000, "")
				   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, rsDefaultQuestion("sortNumber"))
				   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)	
				   .parameters.Append .CreateParameter("@weekly", adBoolean, adParamInput, 1, rsDefaultQuestion("weekly"))
				   .parameters.Append .CreateParameter("@monthly", adBoolean, adParamInput, 1, rsDefaultQuestion("monthly"))	
				   .parameters.Append .CreateParameter("@postRain", adBoolean, adParamInput, 1, rsDefaultQuestion("postRain"))	 
				   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID) 
				   .parameters.Append .CreateParameter("@annual", adBoolean, adParamInput, 1, False)
				   .parameters.Append .CreateParameter("@questionType", adInteger, adParamInput, 8, rsDefaultQuestion("questionType"))  
				   .parameters.Append .CreateParameter("@petroleum", adBoolean, adParamInput, 1, False)
				   .parameters.Append .CreateParameter("@UtilBWPR", adBoolean, adParamInput, 1, False)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rsQuestion = oCmd.Execute
				Set oCmd = Nothing
				
			rsDefaultQuestion.movenext
			loop
			
		rsDefaultCategories.movenext
		i=i+1
		loop

		
		response.Redirect "questionList.asp?id=" & divisionID & "&customerID=" & customerID

	
	
	case "addInfoPack"
		userID= request("userID")
		customerID = request("customerID")
		clientID = request("clientID")
		contactID= request("contactID")
		openingParagraph= request("openingParagraph")
		sdate= request("date")
		emailPack = request("emailPack")		

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddInfoPack"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@contactID", adInteger, adParamInput, 8, contactID)
		   .parameters.Append .CreateParameter("@openingParagraph", adVarChar, adParamInput, 8000, openingParagraph)
		   .parameters.Append .CreateParameter("@date", adDBTimeStamp, adParamInput, 8, sdate)
		   .parameters.Append .CreateParameter("@isVerified", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@dateVerified", adDBTimeStamp, adParamInput, 8, null)
		   .parameters.Append .CreateParameter("@verifiedBy", adInteger, adParamInput, 8, null)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		infoPackID = rs("Identity")
		
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'response.Write "info " & infoPackID
		
		'build the PDF for the info pack
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildInfoPackPDF.asp?infoPackID=" & infoPackID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/InfoPack_" & infoPackID & ".pdf"), true )
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
	
		emailInfoPack infoPackID,userID,contactID,"False"
		
		response.Redirect "infoPackList.asp?customerID=" & customerID & "&clientID=" & clientID
		
	case "verifyInfoPack"
		infoPackID = request("infoPackID")
		verifiedBy = request("verifiedBy")
		userID= request("userID")
		contactID= request("contactID")
		openingParagraph= request("openingParagraph")
		emailPack = request("emailPack")
		clientID = request("clientID")
		customerID = request("customerID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditInfoPack"
		   .parameters.Append .CreateParameter("@infoPackID", adInteger, adParamInput, 8, infoPackID)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@contactID", adInteger, adParamInput, 8, contactID)
		   .parameters.Append .CreateParameter("@openingParagraph", adVarChar, adParamInput, 8000, openingParagraph)
		   .parameters.Append .CreateParameter("@isVerified", adBoolean, adParamInput, 1, True)
		   .parameters.Append .CreateParameter("@dateVerified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@verifiedBy", adInteger, adParamInput, 8, verifiedBy)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'build the PDF for the info pack
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildInfoPackPDF.asp?infoPackID=" & infoPackID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/InfoPack_" & infoPackID & ".pdf"), true )
		
		'email or fax the info pack
		if emailPack = "emailPack" then
			emailInfoPack infoPackID,userID,contactID,"True"
		else
		
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetCustomerContact"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, contactID)
			   .CommandType = adCmdStoredProc   
			End With
			Set rsContact = oCmd.Execute
			Set oCmd = nothing
			
			response.Redirect "form.asp?formType=sendFaxInfo&infoPackID=" & infoPackID & "&customerID=" & customerID & "&customerContactID=" & contactID & "&fax=" & rsContact("fax")
		end if

		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		response.Redirect "infoPackList.asp?customerID=" & customerID & "&clientID=" & clientID

	case "addTime"
		userID= request("userID")
		sdate = request("date")
		projectID = request("projectID")
		costCodeID= request("costCodeID")
		hours= request("hours")
		billable= request("billable")		
		comments= request("comments")
		catID = request("catID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddTime"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@date", adDBTimeStamp, adParamInput, 8, sdate)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@costCodeID", adInteger, adParamInput, 8, costCodeID)
		   .parameters.Append .CreateParameter("@hours", adDouble, adParamInput, 8, hours)
		   .parameters.Append .CreateParameter("@billable", adBoolean, adParamInput, 1, isOn(billable))
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 500, comments)
		   .parameters.Append .CreateParameter("@catID", adInteger, adParamInput, 8, catID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		response.Redirect "timeSheet.asp"
		
	case "editTime"
		timeID = request("timeID")
		userID= request("userID")
		sdate = request("date")
		projectID = request("projectID")
		costCodeID= request("costCodeID")
		hours= request("hours")
		billable= request("billable")		
		comments= request("comments")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditTime"
		   .parameters.Append .CreateParameter("@timeID", adInteger, adParamInput, 8, timeID)
		   .parameters.Append .CreateParameter("@date", adDBTimeStamp, adParamInput, 8, sdate)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		  ' .parameters.Append .CreateParameter("@costCodeID", adInteger, adParamInput, 8, costCodeID)
		   .parameters.Append .CreateParameter("@hours", adVarChar, adParamInput, 50, hours)
		   .parameters.Append .CreateParameter("@billable", adBoolean, adParamInput, 1, isOn(billable))
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 500, comments)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		response.Redirect "timeSheet.asp"
		
	case "deleteTime"
		timeID = Request("id")	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")			
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteTime"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, timeID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "timeSheet.asp"
	
	Case "assignSupervisor"

		userID = request("userID")
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		'unassign and reassign customer to the client
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spUnassignSupervisors"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, userID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
	'	Set oCmd = nothing
	'	rs.close
	'	set rs = nothing
		
		
		'reassign the customers to the client
		for i=1 to Request("sharedUsers").count
			listboxvalue=Request.form("sharedUsers").item(i)
			
			'response.Write userID & "<br>"
			'response.Write listboxvalue & "<br>"
			
			'insert the data into the userCustomer table
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			DataConn.Open Session("Connection"), Session("UserID")
			'Create command
			Set oCmd2 = Server.CreateObject("ADODB.Command")
			
			With oCmd2
			   .ActiveConnection = DataConn
			   .CommandText = "spAssignSupervisor"
			   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
			   .parameters.Append .CreateParameter("@supervisorID", adInteger, adParamInput, 8, listboxvalue)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rsAssign = oCmd2.Execute
			
		next
		
		Set oCmd2 = nothing
		rsAssign.close
		set rsAssign = nothing
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		response.Redirect "userList.asp"
	
	
	Case "deleteSignature"
		userID = request("id")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteSignature"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rs = oCmd.Execute
		
		response.Redirect "form.asp?id=" & userID & "&formType=editUser&sec=acctInfo"
	
	Case "archiveUser"
		userID = request("userID")
		sLetter = request("sLetter")
		archive = request("archive")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spArchiveUser"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@isArchived", adBoolean, adParamInput, 1, archive)
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rs = oCmd.Execute
		
		response.Redirect "userList.asp?id=" & sLetter
	
	Case "suggestCustomers"
		On Error Resume Next
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
		
		quoteID = request("quoteID")
		clientID = request("clientID")
		numManagers = request("numManagers")
		red = request("red")
		
		'get the quote info for the email
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetQuote"
		   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsQuote = oCmd.Execute
		Set oCmd = nothing
		
		sSubject = "New project quote"
		
		for iNum = 1 to numManagers 
			iNumCust = 0
			sBody = "<font face=Arial>The following project has been created in the quote system.<br><br>"
			sBody = sBody & "<b>Quote Number:</b> " & rsQuote("quoteID") & "<br>"
			sBody = sBody & "<b>Project Name:</b> " & rsQuote("projectName") & "<br>"
			sBody = sBody & "<b>Address:</b> " & rsQuote("projectAddress") & "<br>"
			sBody = sBody & "<b>City:</b> " & rsQuote("projectCity") & "<br>"
			sBody = sBody & "<b>State/Zip:</b> " & rsQuote("projectState") & "," & rsQuote("projectZip") & "<br>"
			sBody = sBody & "<b>County:</b> " & rsQuote("countyName") & "<br><br>"
			sBody = sBody & "The following customer(s) have been recommended to recieve quotes:<br><br>"
		
			sTo = request("email" & iNum)
			arrAll = split(Request.Form ("allBoxes" & iNum), ",")
			arrVals = split(Request.Form ("customer" & iNum), ",")
			
		'	response.Write email & "<br>"
			for i = 0 to ubound(arrVals)
				iNumCust = iNumCust + 1
				'get the customer info and add it to the string for the email	
				Set oCmd = Server.CreateObject("ADODB.Command")	
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetCustomer"
				   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, arrVals(i))
				   .CommandType = adCmdStoredProc   
				End With
							
				Set rsCust = oCmd.Execute
				Set oCmd = nothing	

				sBody = sBody & rsCust("customerName") & "<br>"
			next
			
			sBody = sBody & "<br>Please quote these customers for the project above.<br><br></font>"
			'send an email to this area manager with the list of recommended customers
			if iNumCust > 0 then
				'response.Write email & "<br>"
				'response.Write sBody
				
				'############################################################################################################
				Set oCdoMail = Server.CreateObject("CDO.Message")
				Set oCdoConf = Server.CreateObject("CDO.Configuration")				
				sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
				with oCdoConf
				  .Fields.Item(sConfURL & "sendusing") = 2
				  .Fields.Item(sConfURL & "smtpserver") = "localhost"
				  .Fields.Item(sConfURL & "smtpserverport") = 25
				  .Fields.Update
				end with
				
				with oCdoMail
				  .From = request("sEmail")
				  .To = sTo'"rreynolds@hbnext.com"
				  .CC = request("sEmail")
				  .bcc = "rreynolds@hbnext.com"
				  .Subject = sSubject
				  .TextBody = sBody
				  .HTMLBody = sBody
				end with
			
				oCdoMail.Configuration = oCdoConf
				oCdoMail.Send
				Set oCdoConf = Nothing
				Set oCdoMail = Nothing
		
				bSent = True
		
				'############################################################################################################
				
				
			end if

		next
		
		
		if red = "custQuoteList" then
			response.Redirect "quoteCustomerList.asp?clientID=" & clientID & "&quoteID=" & quoteID
		else
			response.Redirect "quoteList.asp?clientID=" & clientID
		end if
	
	Case "closeSupportTicket"
		supportTicketID = request("supportTicketID")
		smessage = request("smessage")
		closeTicket = request("closeTicket")
		
		'get the original support request
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetSupportTicket"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, supportTicketID)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		sSubject = "Support Ticket resolved"
		'sBody = sBody & "<b>Support Ticket resolved</b><br><br>"
		sBody = sBody & "<font style=font-family:Arial, Helvetica, sans-serif;font-size:10px><b>Comments from support team (" & session("name") & "):</b><br>" & smessage & "<br><br>"
		sBody = sBody & "This is in response to the support ticket below.<br><br>"
		sBody = sBody & "**************************************************************<br>"
		sBody = sBody & "<b>Support Ticket #:</b> " & supportTicketID & "<br>"
		sBody = sBody & "<b>Date of Support Ticket:</b> " & formatdatetime(rs("dateAdded"),2) & "<br>"
		sBody = sBody & "<b>Requestor:</b> " & rs("firstName") & " " & rs("lastName") & "<br>"
		sBody = sBody & "<b>Importance:</b> " & rs("importance") & "<br>"
		sBody = sBody & "<b>Manager:</b> " & rs("manager") & "<br>"
		sBody = sBody & "<b>Category:</b> " & rs("category") & "<br>"
		sBody = sBody & "<b>Message:</b> " & rs("message") & "<br>"
		sBody = sBody & "**************************************************************<br><br>"
		sBody = sBody & "<i>Please let us know if we can be of more assistance.<br>HB NEXT Support Team</i>"
		sBody = sBody & "</font>"
		
		
		'send the email to the user
		'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
 		Set oCdoConf = Server.CreateObject("CDO.Configuration")
	
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
	    with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
	    end with
		
		with oCdoMail
		  .From = session("email")
		  .To = rs("email")
		  .BCC = "rreynolds@hbnext.com"
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
	    end with
	
		oCdoMail.Configuration = oCdoConf
	    oCdoMail.Send
	    Set oCdoConf = Nothing
	    Set oCdoMail = Nothing

		bSent = True

		'############################################################################################################
		
		'close the support ticket
		if closeTicket = "on" then
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spCloseSupportTicket"
			   .parameters.Append .CreateParameter("@supportTicketID", adInteger, adParamInput, 8, supportTicketID)
			   .parameters.Append .CreateParameter("@dateClosed", adDBTimeStamp, adParamInput, 8, now())
			   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, isOn(closeTicket))
			   .parameters.Append .CreateParameter("@closedBy", adInteger, adParamInput, 8, session("ID"))
			   .CommandType = adCmdStoredProc
			End With
					
			Set rsSupport = oCmd.Execute
			Set oCmd = nothing
		end if
		
		'add the comments that were sent		
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddSupportTicketComments"
		   .parameters.Append .CreateParameter("@supportTicketID", adInteger, adParamInput, 8, supportTicketID)
		   .parameters.Append .CreateParameter("@supportComments", adVarChar, adParamInput, 500, smessage)
		   .parameters.Append .CreateParameter("@commentDate", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsSupport = oCmd.Execute
		Set oCmd = nothing
		
		'send the user to support ticket list page
		response.Redirect "supportTicketList.asp"
	
	case "activatePLS"
		'general info
		quoteID = request("quoteID")
		customerQuoteID = request("customerQuoteID")
		userID = request("userID")
		clientID = request("clientID")
		sname = checknull(request("name"))
		email = checknull(request("email"))
		customerName = checknull(request("customerName"))
	
		'project info
		projectName = checknull(request("projectName"))
		projectNumber = ""
		projectAddress = checknull(request("projectAddress"))
		city = checknull(request("city"))
		sstate = checknull(request("state"))
		zip = checknull(request("zip"))
		countyID = checknull(request("countyID"))
		latitude = checknull(request("latitude"))
		longitude = checknull(request("longitude"))
		directions = checknull(request("directions"))
		acresPhases = checknull(request("acresPhases"))
		stage = checknull(request("stage"))
		contactName = checknull(request("contactName"))
		contactPhone = checknull(request("contactPhone"))
		contactCell = checknull(request("contactCell"))
		contactFax = checknull(request("contactFax"))
		contactEmail = checknull(request("contactEmail"))
		'creationDate = request("creationDate")
		startDate = checknull(request("startDate"))
		primaryInspector = checknull(request("primaryInspector"))
		autoEmails = checknull(request("autoEmail"))
		endDate = null
		
		'customer info
		customerID = checknull(request("customerID"))
		divisionID = checknull(request("divisionID"))
		
		'rate info
		newStartBillingDays = checknull(request("newStartBillingDays"))
		activationFee = checknull(request("activationFee"))
		billRate = checknull(request("billRate"))
		rateType = checknull(request("rateType"))
		waterSamplingRate = checknull(request("waterSamplingRate"))
		wprRate = checknull(request("wprRate"))
		dRate = checknull(request("dRate"))
		
		'alert info
		sevenDayAlert = request("sevenDayAlert")
		fourteenDayAlert = request("fourteenDayAlert")
		openItemAlert = request("openItemAlert")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		'add the new project, alerts, and rates and set the division
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddProjectFromPLS"
		   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 200, projectName)
		   .parameters.Append .CreateParameter("@projectNumber", adVarChar, adParamInput, 50, projectNumber)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@countyID", adInteger, adParamInput, 8, countyID)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@isActive", adBoolean, adParamInput, 1, true)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@division", adInteger, adParamInput, 8, divisionID)
		   .parameters.Append .CreateParameter("@autoEmail", adBoolean, adParamInput, 1, isOn(autoEmails))
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@endDate", adDBTimeStamp, adParamInput, 8, endDate)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@latitude", adVarChar, adParamInput, 50, latitude)
		   .parameters.Append .CreateParameter("@longitude", adVarChar, adParamInput, 50, longitude)
		   'new fields
		   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@Zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@directions", adVarChar, adParamInput, 500, directions)
		   .parameters.Append .CreateParameter("@acresPhases", adVarChar, adParamInput, 50, acresPhases)
		   .parameters.Append .CreateParameter("@stage", adVarChar, adParamInput, 50, stage)
		   .parameters.Append .CreateParameter("@contactName", adVarChar, adParamInput, 50, contactName)
		   .parameters.Append .CreateParameter("@contactPhone", adVarChar, adParamInput, 50, contactPhone)
		   .parameters.Append .CreateParameter("@contactCell", adVarChar, adParamInput, 50, contactCell)
		   .parameters.Append .CreateParameter("@contactFax", adVarChar, adParamInput, 50, contactFax)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 50, contactEmail)
		   'alerts
		   .parameters.Append .CreateParameter("@sevenDayAlert", adBoolean, adParamInput, 1, isOn(sevenDayAlert))
		   .parameters.Append .CreateParameter("@fourteenDayAlert", adBoolean, adParamInput, 1, isOn(fourteenDayAlert))
		   .parameters.Append .CreateParameter("@openItemAlert", adBoolean, adParamInput, 1, isOn(openItemAlert))		   
		   'rates
		   .parameters.Append .CreateParameter("@wprRate", adCurrency, adParamInput, 8, wprRate)
		   .parameters.Append .CreateParameter("@dRate", adCurrency, adParamInput, 8, dRate)
		   .parameters.Append .CreateParameter("@billRate", adCurrency, adParamInput, 8, billRate)
		   .parameters.Append .CreateParameter("@activationFee", adCurrency, adParamInput, 8, activationFee)
		   .parameters.Append .CreateParameter("@waterSamplingRate", adCurrency, adParamInput, 8, waterSamplingRate)
		   .parameters.Append .CreateParameter("@rateType", adInteger, adParamInput, 8, rateType)
		   .parameters.Append .CreateParameter("@newStartBillingDays", adInteger, adParamInput, 8, newStartBillingDays)
		   .parameters.Append .CreateParameter("@primaryInspector", adInteger, adParamInput, 8, primaryInspector)
		   .parameters.Append .CreateParameter("@PLSGenerated", adBoolean, adParamInput, 1, true)
		   
		   
		   .CommandType = adCmdStoredProc
		End With
		
		
		Set rs = oCmd.Execute
		projectID = rs("Identity")		
		Set oCmd = nothing
		
		'activate the customer if not active
		Set oCmd = Server.CreateObject("ADODB.Command")				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spActivateCustomer"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@isActive", adInteger, adParamInput, 8, true)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsCustomer = oCmd.Execute
		Set oCmd = nothing
	
		'assign the primary inspector to the project
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddUserProject"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, primaryInspector)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .CommandType = adCmdStoredProc
		End With
		
		Set rsAssign = oCmd.Execute
		Set oCmd = nothing
		
		'update customerquote to let user know that the PDF was generated and project was created
		Set oCmd = Server.CreateObject("ADODB.Command")				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spProjectCreated"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerQuoteID)
		   '.parameters.Append .CreateParameter("@PDFProjectCreated", adInteger, adParamInput, 8, true)
		   .CommandType = adCmdStoredProc
		End With
		
		Set rsCreated = oCmd.Execute
		Set oCmd = nothing
		
		'create the PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPLSPDF.asp?projectID=" & projectID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/PLS_" & projectID & ".pdf"), true )
		
		'send the email to the primary inspector to let them know when to start
		'send an email to the admins based on the clientID
		'send email to admin for them to activate membership
		sBody = "The following project has been set up in the system and you have been chosen as the primary inspector." & vbcrlf & vbcrlf
		sBody = sBody & "Customer Name: " & customerName & vbcrlf
		sBody = sBody & "Project Name: " & projectName & vbcrlf
		'sBody = sBody & "Quote ID: " & quoteID & vbcrlf
		sBody = sBody & "Created By: " & sname & vbcrlf
		sBody = sBody & "As the primary inspector, you have been automatically assigned to this project."
		'response.Write sBody
		
		'####################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		sTo = sTo & getPrimaryInspectorEmail(primaryInspector)
		'sTo = sTo & "rreynolds@hbnext.com"
		
		with oCdoMail
		  .From = email
		  .To = sTo
		  .Subject = "New " & sPageTitle & " Project"
		  .TextBody = sBody
		end with
		
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing
		'######################################################################################
		
		response.Redirect "quoteCustomerList.asp?clientID=" & clientID & "&quoteID=" & quoteID
		'send user back to the customerquotelist page
	
	case "addPLS"
	
		projectName = request("projectName")
		projectAddress = request("projectAddress")
		projectCity = request("projectCity")
		projectState = request("projectState")
		projectZip = request("projectZip")
		projectCounty = request("projectCounty")
		latitude = request("latitude")
		longitude = request("longitude")
		projectDirections = request("projectDirections")
		acresPhases = request("acresPhases")
		stage = request("stage")
		contactName = request("contactName")
		contactPhone = request("contactPhone")
		contactCell = request("contactCell")
		contactFax = request("contactFax")
		contactEmail = request("contactEmail")
		
		customerID = request("customerID")
		quoteID = request("quoteID")
		customerQuoteID = request("customerQuoteID")
		userID = request("userID")
		clientID = request("clientID")
		email = request("email")
		sName = request("name")
		customerName = request("customerName")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddPLS"
		   .parameters.Append .CreateParameter("@inspectionQuoteCustomerID", adInteger, adParamInput, 8, customerQuoteID)
		   .parameters.Append .CreateParameter("@PLSActivatedBy", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 100, projectName)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@projectState", adVarChar, adParamInput, 50, projectState)
		   .parameters.Append .CreateParameter("@projectZip", adVarChar, adParamInput, 50, projectZip)		   
		   .parameters.Append .CreateParameter("@projectCounty", adInteger, adParamInput, 8, projectCounty)
		   .parameters.Append .CreateParameter("@latitude", adVarChar, adParamInput, 50, latitude)
		   .parameters.Append .CreateParameter("@longitude", adVarChar, adParamInput, 50, longitude)
		   .parameters.Append .CreateParameter("@projectDirections", adVarChar, adParamInput, 500, projectDirections)
		   .parameters.Append .CreateParameter("@acresPhases", adVarChar, adParamInput, 50, acresPhases)
		   .parameters.Append .CreateParameter("@stage", adVarChar, adParamInput, 50, stage)		   
		   .parameters.Append .CreateParameter("@contactName", adVarChar, adParamInput, 50, contactName)
		   .parameters.Append .CreateParameter("@contactPhone", adVarChar, adParamInput, 50, contactPhone)
		   .parameters.Append .CreateParameter("@contactCell", adVarChar, adParamInput, 50, contactCell)
		   .parameters.Append .CreateParameter("@contactFax", adVarChar, adParamInput, 50, contactFax)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 50, contactEmail)		   
		   .parameters.Append .CreateParameter("@PLSActivated", adBoolean, adParamInput, 1, true)		   
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'generate the PDF of the PLS, it will be incomplete until the project has been activated??????
		
		
		'send an email to the admins based on the clientID
		'send email to admin for them to activate membership
		sBody = "A PLS has been activated in the system." & vbcrlf & vbcrlf
		sBody = sBody & "Customer Name: " & customerName & vbcrlf
		sBody = sBody & "Project Name: " & projectName & vbcrlf
		sBody = sBody & "Quote ID: " & quoteID & vbcrlf
		sBody = sBody & "Generated By: " & sName & vbcrlf
		sBody = sBody & "This PLS is not complete until the project has been activated."
		'response.Write sBody
		
		'####################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		sTo = sTo & getAdminEmail(clientID)
		sTo = sTo & "rreynolds@hbnext.com"
		
		with oCdoMail
		  .From = email
		  .To = sTo
		  .Subject = "New " & sPageTitle & " PLS Activated"
		  .TextBody = sBody
		end with
		
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing
		'######################################################################################
		
		response.Redirect "quoteCustomerList.asp?clientID=" & clientID & "&quoteID=" & quoteID
		
	case "editPLS"
	
		projectName = request("projectName")
		projectAddress = request("projectAddress")
		projectCity = request("projectCity")
		projectState = request("projectState")
		projectZip = request("projectZip")
		projectCounty = request("projectCounty")
		latitude = request("latitude")
		longitude = request("longitude")
		projectDirections = request("projectDirections")
		acresPhases = request("acresPhases")
		stage = request("stage")
		contactName = request("contactName")
		contactPhone = request("contactPhone")
		contactCell = request("contactCell")
		contactFax = request("contactFax")
		contactEmail = request("contactEmail")
		
		'customerID = request("customerID")
		quoteID = request("quoteID")
		customerQuoteID = request("customerQuoteID")
		userID = request("userID")
		clientID = request("clientID")
		'email = request("email")
		'sName = request("name")
		'customerName = request("customerName")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddPLS"
		   .parameters.Append .CreateParameter("@inspectionQuoteCustomerID", adInteger, adParamInput, 8, customerQuoteID)
		   .parameters.Append .CreateParameter("@PLSActivatedBy", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 50, projectName)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@projectState", adVarChar, adParamInput, 50, projectState)
		   .parameters.Append .CreateParameter("@projectZip", adVarChar, adParamInput, 50, projectZip)		   
		   .parameters.Append .CreateParameter("@projectCounty", adInteger, adParamInput, 8, projectCounty)
		   .parameters.Append .CreateParameter("@latitude", adVarChar, adParamInput, 50, latitude)
		   .parameters.Append .CreateParameter("@longitude", adVarChar, adParamInput, 50, longitude)
		   .parameters.Append .CreateParameter("@projectDirections", adVarChar, adParamInput, 500, projectDirections)
		   .parameters.Append .CreateParameter("@acresPhases", adVarChar, adParamInput, 50, acresPhases)
		   .parameters.Append .CreateParameter("@stage", adVarChar, adParamInput, 50, stage)		   
		   .parameters.Append .CreateParameter("@contactName", adVarChar, adParamInput, 50, contactName)
		   .parameters.Append .CreateParameter("@contactPhone", adVarChar, adParamInput, 50, contactPhone)
		   .parameters.Append .CreateParameter("@contactCell", adVarChar, adParamInput, 50, contactCell)
		   .parameters.Append .CreateParameter("@contactFax", adVarChar, adParamInput, 50, contactFax)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 50, contactEmail)		   
		   .parameters.Append .CreateParameter("@PLSActivated", adBoolean, adParamInput, 1, true)		   
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		'generate the PDF of the PLS, it will be incomplete until the project has been activated??????
		
		response.Redirect "quoteCustomerList.asp?clientID=" & clientID & "&quoteID=" & quoteID
	
	Case "addDefaultECDevices"
		'get the array with all of the checkbox id's
		arrAll = split(Request.Form ("allBoxes"), ",")
		customerID = request("customerID")
		divisionID = request("divisionID")
		
		'get the array with the id's that were ticked
		arrVals = split(Request.Form ("device"), ",")
		
		On Error Resume Next
			
		Set DataConn = Server.CreateObject("ADODB.Connection") 				
		DataConn.Open Session("Connection"), Session("UserID")
		iCount = 0
		for i = 0 to ubound(arrVals)
			iCount = iCount + 1
			'get the device based on it's id from the default
			'table and add it to the division
		
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetDefaultECDevice"
			   .parameters.Append .CreateParameter("@deviceID", adInteger, adParamInput, 8, arrVals(i))
			   .CommandType = adCmdStoredProc
			End With
					
			Set rs = oCmd.Execute
			Set oCmd = nothing
			
		'	response.Write "ID " & arrVals(i) & "<br>"
		'	response.Write "sort " & i & "<br>"
		'	response.Write "device " & rs("device") & "<br><br>"
			
			'add the device to the ECDevices table
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddECDevice"
			   .parameters.Append .CreateParameter("@device", adVarChar, adParamInput, 250, rs("device"))
			   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
			   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, iCount)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rsDevice = oCmd.Execute
			Set oCmd = nothing
			
			
			rs.close
			set rs = nothing		
			
			
		next
		
		response.Redirect "ecDeviceList.asp?id=" & divisionID & "&customerID=" & customerID
	
	Case "archiveQuote"
		clientID = request("clientID")
		quoteID = request("quoteID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spArchiveQuote"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, quoteID)
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rs = oCmd.Execute
		
		response.Redirect "quoteList.asp?clientID=" & clientID
		
	Case "archiveNoticeOfSeparation"
		clientID = request("clientID")
		noticeOfSeparationID = request("noticeOfSeparationID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spArchiveNoticeOfSeparation"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, noticeOfSeparationID)
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rs = oCmd.Execute
		
		response.Redirect "noticeOfSeparationList.asp?clientID=" & clientID
		
	Case "archiveWorkersCompReport"
		clientID = request("clientID")
		workersCompID = request("workersCompID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spArchiveWorkersComp"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, workersCompID)
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rs = oCmd.Execute
		
		response.Redirect "workersCompReportList.asp?clientID=" & clientID
		
	Case "addNoticeOfSeparation"
		clientID = checkNull(request("clientID"))
		empName = checkNull(request("empName"))
		dateOfHire = checkNull(request("dateOfHire"))
		lastPayRate = checkNull(request("lastPayRate"))
		rehireDate = checkNull(request("rehireDate"))
		lastDateWorked = checkNull(request("lastDateWorked"))
		employeeType = checkNull(request("employeeType"))
		reasonVoluntary = checkNull(request("reasonVoluntary"))
		reasonInvoluntary = checkNull(request("reasonInvoluntary"))
		warningsGiven = checkNull(request("warningsGiven"))
		remarks = checkNull(request("remarks"))
		eligibleForRehire = checkNull(request("eligibleForRehire"))
		compOfficial = checkNull(request("compOfficial"))
		compOfficialTitle = checkNull(request("compOfficialTitle"))	
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddNoticeOfSeparation"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@empName", adVarChar, adParamInput, 100, empName)
		   .parameters.Append .CreateParameter("@dateOfHire", adDBTimeStamp, adParamInput, 8, dateOfHire)
		   .parameters.Append .CreateParameter("@lastPayRate", adVarChar, adParamInput, 50, lastPayRate)
		   .parameters.Append .CreateParameter("@rehireDate", adDBTimeStamp, adParamInput, 8, rehireDate)
		   .parameters.Append .CreateParameter("@lastDateWorked", adDBTimeStamp, adParamInput, 8, lastDateWorked)
		   .parameters.Append .CreateParameter("@employeeType", adVarChar, adParamInput, 10, employeeType)
		   .parameters.Append .CreateParameter("@reasonVoluntary", adVarChar, adParamInput, 100, reasonVoluntary)
		   .parameters.Append .CreateParameter("@reasonInvoluntary", adVarChar, adParamInput, 100, reasonInvoluntary)
		   .parameters.Append .CreateParameter("@warningsGiven", adVarChar, adParamInput, 10, warningsGiven)		   
		   .parameters.Append .CreateParameter("@remarks", adVarChar, adParamInput, 1000, remarks)
		   .parameters.Append .CreateParameter("@eligibleForRehire", adVarChar, adParamInput, 10, eligibleForRehire)
		   .parameters.Append .CreateParameter("@compOfficial", adVarChar, adParamInput, 100, compOfficial)
		   .parameters.Append .CreateParameter("@compOfficialTitle", adVarChar, adParamInput, 50, compOfficialTitle)
		   .parameters.Append .CreateParameter("@dateCompleted", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rs = oCmd.Execute
		
		'get the workers comp id that we just created
		noticeOfSeparationID = rs("Identity")
		
		'build the pdf for the worker comp data
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildNoticeOfSeparationPDF.asp?noticeOfSeparationID=" & noticeOfSeparationID & "&clientID=" & clientID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/noticeOfSeparation_" & noticeOfSeparationID & ".pdf"), true )
		
		'email the form to the appropriate party
		sendNoticeOfSeparationEmail noticeOfSeparationID
		
		response.Redirect "main.asp"
	
	Case "addWorkComp"
		clientID = checkNull(request("clientID"))
		boardClaimNo = checkNull(request("boardClaimNo"))
		empLastName = checkNull(request("empLastName"))
		empFirstName = checkNull(request("empFirstName"))
		empMI = checkNull(request("empMI"))
		ssn = checkNull(request("ssn"))
		injuryDate = checkNull(request("injuryDate"))
		empSex = checkNull(request("empSex"))
		birthDate = checkNull(request("birthDate"))
		empPhone = checkNull(request("empPhone"))
		empEmail = checkNull(request("empEmail"))
		empAddress = checkNull(request("empAddress"))
		empCity = checkNull(request("empCity"))
		empState = checkNull(request("empState"))
		empZip = checkNull(request("empZip"))
		employerName = checkNull(request("employerName"))
		employerAddress = checkNull(request("employerAddress"))
		employerCity = checkNull(request("employerCity"))
		employerState = checkNull(request("employerState"))
		employerZip = checkNull(request("employerZip"))
		employerPhone = checkNull(request("employerPhone"))
		employerEmail = checkNull(request("employerEmail"))
		NAICSCode = checkNull(request("NAICSCode"))
		natureOfBusiness = checkNull(request("natureOfBusiness"))
		employerFEIN = checkNull(request("employerFEIN"))
		insurerName = checkNull(request("insurerName"))
		insurerFEIN = checkNull(request("insurerFEIN"))
		insurerFileNumber = checkNull(request("insurerFileNumber"))
		claimName = checkNull(request("claimName"))
		claimFEIN = checkNull(request("claimFEIN"))
		claimPhone = checkNull(request("claimPhone"))
		claimEmail = checkNull(request("claimEmail"))
		claimSBWC = checkNull(request("claimSBWC"))
		claimAddress = checkNull(request("claimAddress"))
		claimCity = checkNull(request("claimCity"))
		claimState = checkNull(request("claimState"))
		claimZip = checkNull(request("claimZip"))
		dateHired = checkNull(request("dateHired"))
		jobCodeNumber = checkNull(request("jobCodeNumber"))
		numberDaysWork = checkNull(request("numberDaysWork"))
		wageRate = checkNull(request("wageRate"))
		insurerCode = checkNull(request("insurerCode"))
		daysOff = checkNull(request("daysOff"))
		injuryTime = checkNull(request("injuryTime"))
		injuryCounty = checkNull(request("injuryCounty"))
		dateEmployerKnowledge = checkNull(request("dateEmployerKnowledge"))
		dateEmployeeFullDay = checkNull(request("dateEmployeeFullDay"))
		receiveFullPay = checkNull(request("receiveFullPay"))
		occurOnPremises = checkNull(request("occurOnPremises"))
		injuryType = checkNull(request("injuryType"))
		bodyPartAffected = checkNull(request("bodyPartAffected"))
		howInjuryOccurred = checkNull(request("howInjuryOccurred"))
		physician = checkNull(request("physician"))
		treatmentGiven = checkNull(request("treatmentGiven"))
		hospital = checkNull(request("hospital"))
		dateReturned = checkNull(request("dateReturned"))
		returnedWage = checkNull(request("returnedWage"))
		dateOfDeath = checkNull(request("dateOfDeath"))
		preparedBy = checkNull(request("preparedBy"))
		preparedByPhone = checkNull(request("preparedByPhone"))
		reportDate = checkNull(request("reportDate"))
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddWorkersComp"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@boardClaimNo", adVarChar, adParamInput, 50, boardClaimNo)
		   .parameters.Append .CreateParameter("@empLastName", adVarChar, adParamInput, 50, empLastName)
		   .parameters.Append .CreateParameter("@empFirstName", adVarChar, adParamInput, 50, empFirstName)
		   .parameters.Append .CreateParameter("@empMI", adVarChar, adParamInput, 10, empMI)
		   .parameters.Append .CreateParameter("@ssn", adVarChar, adParamInput, 50, ssn)		   
		   .parameters.Append .CreateParameter("@injuryDate", adDBTimeStamp, adParamInput, 8, injuryDate)
		   .parameters.Append .CreateParameter("@empSex", adVarChar, adParamInput, 10, empSex)
		   .parameters.Append .CreateParameter("@birthDate", adDBTimeStamp, adParamInput, 8, birthDate)
		   .parameters.Append .CreateParameter("@empPhone", adVarChar, adParamInput, 50, empPhone)
		   .parameters.Append .CreateParameter("@empEmail", adVarChar, adParamInput, 100, empEmail)
		   .parameters.Append .CreateParameter("@empAddress", adVarChar, adParamInput, 100, empAddress)
		   .parameters.Append .CreateParameter("@empCity", adVarChar, adParamInput, 50, empCity)
		   .parameters.Append .CreateParameter("@empState", adVarChar, adParamInput, 50, empState)		   
		   .parameters.Append .CreateParameter("@empZip", adVarChar, adParamInput, 10, empZip)
		   .parameters.Append .CreateParameter("@employerName", adVarChar, adParamInput, 100, employerName)
		   .parameters.Append .CreateParameter("@employerAddress", adVarChar, adParamInput, 100, employerAddress)
		   .parameters.Append .CreateParameter("@employerCity", adVarChar, adParamInput, 50, employerCity)
		   .parameters.Append .CreateParameter("@employerState", adVarChar, adParamInput, 50, employerState)
		   .parameters.Append .CreateParameter("@employerZip", adVarChar, adParamInput, 10, employerZip)
		   .parameters.Append .CreateParameter("@employerPhone", adVarChar, adParamInput, 50, employerPhone)		   
		   .parameters.Append .CreateParameter("@employerEmail", adVarChar, adParamInput, 100, employerEmail)
		   .parameters.Append .CreateParameter("@NAICSCode", adVarChar, adParamInput, 50, NAICSCode)
		   .parameters.Append .CreateParameter("@natureOfBusiness", adVarChar, adParamInput, 250, natureOfBusiness)
		   .parameters.Append .CreateParameter("@employerFEIN", adVarChar, adParamInput, 50, employerFEIN)
		   .parameters.Append .CreateParameter("@insurerName", adVarChar, adParamInput, 100, insurerName)
		   .parameters.Append .CreateParameter("@insurerFEIN", adVarChar, adParamInput, 50, insurerFEIN)
		   .parameters.Append .CreateParameter("@insurerFileNumber", adVarChar, adParamInput, 50, insurerFileNumber)
		   .parameters.Append .CreateParameter("@claimName", adVarChar, adParamInput, 50, claimName)		   
		   .parameters.Append .CreateParameter("@claimFEIN", adVarChar, adParamInput, 50, claimFEIN)
		   .parameters.Append .CreateParameter("@claimPhone", adVarChar, adParamInput, 50, claimPhone)
		   .parameters.Append .CreateParameter("@claimEmail", adVarChar, adParamInput, 100, claimEmail)
		   .parameters.Append .CreateParameter("@claimSBWC", adVarChar, adParamInput, 50, claimSBWC)
		   .parameters.Append .CreateParameter("@claimAddress", adVarChar, adParamInput, 100, claimAddress)
		   .parameters.Append .CreateParameter("@claimCity", adVarChar, adParamInput, 50, claimCity)
		   .parameters.Append .CreateParameter("@claimState", adVarChar, adParamInput, 50, claimState)
		   .parameters.Append .CreateParameter("@claimZip", adVarChar, adParamInput, 10, claimZip)
		   .parameters.Append .CreateParameter("@dateHired", adDBTimeStamp, adParamInput, 8, dateHired)		   
		   .parameters.Append .CreateParameter("@jobCodeNumber", adVarChar, adParamInput, 50, jobCodeNumber)		   
		   .parameters.Append .CreateParameter("@numberDaysWork", adVarChar, adParamInput, 50, numberDaysWork)
		   .parameters.Append .CreateParameter("@wageRate", adVarChar, adParamInput, 50, wageRate)
		   .parameters.Append .CreateParameter("@insurerCode", adVarChar, adParamInput, 50, insurerCode)
		   .parameters.Append .CreateParameter("@daysOff", adVarChar, adParamInput, 50, daysOff)
		   .parameters.Append .CreateParameter("@injuryTime", adVarChar, adParamInput, 50, injuryTime)
		   .parameters.Append .CreateParameter("@injuryCounty", adInteger, adParamInput, 8, injuryCounty)		   
		   .parameters.Append .CreateParameter("@dateEmployerKnowledge", adDBTimeStamp, adParamInput, 8, dateEmployerKnowledge)	
		   .parameters.Append .CreateParameter("@dateEmployeeFullDay", adDBTimeStamp, adParamInput, 8, dateEmployeeFullDay)			   
		   .parameters.Append .CreateParameter("@receiveFullPay", adVarChar, adParamInput, 50, receiveFullPay)
		   .parameters.Append .CreateParameter("@occurOnPremises", adVarChar, adParamInput, 50, occurOnPremises)
		   .parameters.Append .CreateParameter("@injuryType", adVarChar, adParamInput, 100, injuryType)		   
		   .parameters.Append .CreateParameter("@bodyPartAffected", adVarChar, adParamInput, 150, bodyPartAffected)
		   .parameters.Append .CreateParameter("@howInjuryOccurred", adVarChar, adParamInput, 500, howInjuryOccurred)
		   .parameters.Append .CreateParameter("@physician", adVarChar, adParamInput, 150, physician)
		   .parameters.Append .CreateParameter("@treatmentGiven", adVarChar, adParamInput, 150, treatmentGiven)
		   .parameters.Append .CreateParameter("@hospital", adVarChar, adParamInput, 150, hospital)		   
		   .parameters.Append .CreateParameter("@dateReturned", adDBTimeStamp, adParamInput, 8, dateReturned)		   
		   .parameters.Append .CreateParameter("@returnedWage", adVarChar, adParamInput, 50, returnedWage)
		   .parameters.Append .CreateParameter("@dateOfDeath", adDBTimeStamp, adParamInput, 8, dateOfDeath)	
		   .parameters.Append .CreateParameter("@preparedBy", adVarChar, adParamInput, 50, preparedBy)
		   .parameters.Append .CreateParameter("@preparedByPhone", adVarChar, adParamInput, 50, preparedByPhone)
		   .parameters.Append .CreateParameter("@reportDate", adDBTimeStamp, adParamInput, 8, reportDate)
		   .parameters.Append .CreateParameter("@archived", adBoolean, adParamInput, 1, "False")
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rs = oCmd.Execute
		
		'get the workers comp id that we just created
		workersCompID = rs("Identity")
		
		'build the pdf for the worker comp data
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildWorkersCompPDF.asp?workersCompID=" & workersCompID & "&clientID=" & clientID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/workersComp_" & workersCompID & ".pdf"), true )
		
		'email the form to the appropriate party
		sendWorkCompEmail workersCompID
		
		response.Redirect "main.asp"
		

	Case "addGDOTMonthly"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = request("projectStatus")
		rainfallAmount = request("rainfallAmount")
		reportNumber = request("reportNumber")
		
		if reportNumber = "" then
			reportNumber = 0
		end if
		
		addToJournal = request("addToJournal")
		
		'this is to determine which PDF to generate
		buildPDFType = request("buildPDFType")
		
		clientID = Session("clientID")
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		If Err Then
		%>
			15<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		If Err Then
		%>
			16<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if
		
		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReportEC"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@reportNumber", adVarChar, adParamInput, 50, reportNumber)
		   .parameters.Append .CreateParameter("@disturbedAcreage", adInteger, adParamInput, 8, null)
		   .parameters.Append .CreateParameter("@deadlineToCorrect", adVarChar, adParamInput, 50, null)
		   .parameters.Append .CreateParameter("@currentPhase", adVarChar, adParamInput, 50, null)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			13<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
		141	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
		14	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'add the answers to the questions

		For Each Item in Request.Form
			if Left(item,6) = "answer" then
				
				'if the is no underscore, then write the data
				if Left(item,8) = "answer_&" then
					'insert the answers into the report answers table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					If Err Then
					%>
					10	<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddReportAnswers"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, cint(replace(Item,"answer_&","")))
					   .parameters.Append .CreateParameter("@answer", adVarChar, adParamInput, 50, trim(Request.Form(Item)))
					   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, now())
					   .CommandType = adCmdStoredProc
					End With
					
					If Err Then
					%>
					9	<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					Set rs = oCmd2.Execute
					
					If Err Then
					%>
					8	<!--#include file="includes/FatalError.inc"-->
					<%
					End If
				end if
				
				
				'for each no answer, add the responsive action needed
				if 	Left(item,8) = "answer_R" then
					'reportID get from above
			
					'questionID get from srting
					iLen1 = len(item)
					iLen2 = InStr(item, "@")
					quesID = Right(item,iLen1 - iLen2)
					'response.Write "question " & quesID & "<br>"
					
					'reference number
					sText = Replace(item,"answer_R", "")
					iLen3 = InStr(sText, "@")
					iLen4 = len(sText)
					sText = Left(sText,iLen3)
					sRefNum = Replace(sText,"@", "")
					
					'action needed
					sActionNeeded = trim(Request.Form(Item))
					
					If 	sActionNeeded <> "" then			
						'insert the data into the database
						Set oCmd3 = Server.CreateObject("ADODB.Command")
						If Err Then
						%>
							18<!--#include file="includes/FatalError.inc"-->
						<%
						End If
						
						With oCmd3
						   .ActiveConnection = DataConn
						   .CommandText = "spAddResponsiveActions"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
						   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
						   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
						   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, sActionNeeded)
						   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
						   .CommandType = adCmdStoredProc
						End With
						
						If Err Then
						%>
					6		<!--#include file="includes/FatalError.inc"-->
						<%
						End If
						
						Set rs2 = oCmd3.Execute
						
						If Err Then
						%>
					7		<!--#include file="includes/FatalError.inc"-->
						<%
						End If
					end if
					
				
				end if
			end if
		Next
		
		'loop through the form fields and update the reportAnswers table with the location info.
		'need reportID and reference number
		'keep the item open by inserting 0 into isClosed field
		For Each Item in Request.Form
			if 	Left(item,9) = "location_" then
				sRefNumb = Replace(item,"location_", "")
				
				'insert the data into the database
				Set oCmd4 = Server.CreateObject("ADODB.Command")
			
				With oCmd4
				   .ActiveConnection = DataConn
				   .CommandText = "spAddLocation" 'is an update query
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNumb)
				   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000, trim(Request.Form(Item)))
				   .CommandType = adCmdStoredProc
				End With
			
				Set rs3 = oCmd4.Execute
				
			end if
		Next
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if

		
		'log the details of the report
		'now() - log the date
		'userID
		'reportid
		'Report Created
		'Report and PDF document created
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMonthlyDOTPDF.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
	
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if

		response.Redirect "reportList.asp?findReport=" & reportID

	Case "archiveAccidentReport"
		clientID = request("clientID")
		accidentID = request("accidentID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spArchiveAccidentReport"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rs = oCmd.Execute
		
		response.Redirect "accidentReportList.asp?clientID=" & clientID
		
	Case "importContacts"
	
		'get the array with all of the checkbox id's
		arrAll = split(Request.Form ("allBoxes"), ",")
		projectID = request("projectID")
		importFrom = request("importFrom")
		
		'get the array with the id's that were ticked
		arrVals = split(Request.Form ("import"), ",")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		for i = 0 to ubound(arrVals)
			'response.Write arrVals(i) & "<br>"
			'get the contact for the selected item

			Set oCmd = Server.CreateObject("ADODB.Command")
			if importFrom = "project" then	
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetContact"
				   .parameters.Append .CreateParameter("@contactID", adInteger, adParamInput, 8, arrVals(i))
				   .CommandType = adCmdStoredProc
				   
				End With
			else
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetUser"
				   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, arrVals(i))
				   .CommandType = adCmdStoredProc
				   
				End With
			end if
			Set rsContact = oCmd.Execute
			
			if importFrom = "project" then
				if rsContact("getAutoEmail") = "True" Then
					getAutoEmail = "on"
				else
					getAutoEmail = ""
				end if
			else
				getAutoEmail = "on"
			end if
			
			
			'response.Write "fn " & rsContact("firstName")
			
			select case importFrom
				case "project"
					contactTypeID = rsContact("contactTypeID")
					firstName = rsContact("firstName")
					lastName = rsContact("lastName")
					companyName = rsContact("companyName")
					email = rsContact("email")
					officeNumber1 = rsContact("officeNumber1")
					officeNumber2 = rsContact("officeNumber2")
					officeNumber3 = rsContact("officeNumber3")
					officeNumberExt = rsContact("officeNumberExt")
					cellNumber1 = rsContact("cellNumber1")
					cellNumber2 = rsContact("cellNumber2")
					cellNumber3 = rsContact("cellNumber3")
					address = rsContact("address")
					city = rsContact("city")
					state = rsContact("state")
					zip = rsContact("zip")
					
				case "userList"
					response.Write "userList"
					contactTypeID = null
					firstName = rsContact("firstName")
					lastName = rsContact("lastName")
					companyName = rsContact("company")
					email = rsContact("email")
					
					officePhone = rsContact("officePhone")
					'strip all of the characters
					officePhone = replace(officePhone,"-","")
					officePhone = replace(officePhone," ","")
					officePhone = replace(officePhone,".","")
					'get the second three numbers
					officePhone2 = right(officePhone,7)
					officePhone2 = left(officePhone2,3)

					
					officeNumber1 = left(officePhone,3)
					officeNumber2 = officePhone2
					officeNumber3 = right(officePhone,4)
					
					officeNumberExt = null
					
					cellPhone = rsContact("cellPhone")
					'strip all of the characters
					cellPhone = replace(cellPhone,"-","")
					cellPhone = replace(cellPhone," ","")
					cellPhone = replace(cellPhone,".","")
					'get the second three numbers
					cellPhone2 = right(cellPhone,7)
					cellPhone2 = left(cellPhone2,3)
					
					cellNumber1 = left(cellPhone,3)
					cellNumber2 = cellPhone2
					cellNumber3 = right(cellPhone,4)
					address = null
					city = null
					state = rsContact("state")
					zip = null
			end select
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			'add the contact for the intended project
			Set oCmdContact = Server.CreateObject("ADODB.Command")				
			With oCmdContact
			   .ActiveConnection = DataConn
			   .CommandText = "spAddContact"
			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
			   .parameters.Append .CreateParameter("@contactTypeID", adInteger, adParamInput, 8, contactTypeID)
			   .parameters.Append .CreateParameter("@firstName", adVarChar, adParamInput, 50, firstName)
			   .parameters.Append .CreateParameter("@lastName", adVarChar, adParamInput, 100, lastName)
			   .parameters.Append .CreateParameter("@companyName", adVarChar, adParamInput, 100, companyName)
			   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 100, email)
			   .parameters.Append .CreateParameter("@officeNumber1", adVarChar, adParamInput, 50, officeNumber1)
			   .parameters.Append .CreateParameter("@officeNumber2", adVarChar, adParamInput, 50, officeNumber2)
			   .parameters.Append .CreateParameter("@officeNumber3", adVarChar, adParamInput, 50, officeNumber3)
			   .parameters.Append .CreateParameter("@officeNumberExt", adVarChar, adParamInput, 50, officeNumberExt)
			   .parameters.Append .CreateParameter("@cellNumber1", adVarChar, adParamInput, 50, cellNumber1)
			   .parameters.Append .CreateParameter("@cellNumber2", adVarChar, adParamInput, 50, cellNumber2)
			   .parameters.Append .CreateParameter("@cellNumber3", adVarChar, adParamInput, 50, cellNumber3)
			   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 100, address)
			   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
			   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, state)
			   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
			   .parameters.Append .CreateParameter("@getAutoEmail", adBoolean, adParamInput, 1, isOn(getAutoEmail))
			   .CommandType = adCmdStoredProc
			End With
			Set rs = oCmdContact.Execute
			
		next
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		response.Redirect "contactList.asp?project=" & projectID & "&imported=True"
		

	Case "addGDOTDailyInspection"
	
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		'inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		'weatherType = 1'don't need weather type for daily reportrequest("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		'projectStatus = request("projectStatus")
		'reportNumber = request("reportNumber")
		rainfallAmount= request("rainfallAmount")
		
		addToJournal = request("addToJournal")
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
	
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReportEC"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, null)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, null)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, null)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@reportNumber", adVarChar, adParamInput, 50, null)
		   .parameters.Append .CreateParameter("@disturbedAcreage", adInteger, adParamInput, 8, null)
		   .parameters.Append .CreateParameter("@deadlineToCorrect", adVarChar, adParamInput, 50, null)
		   .parameters.Append .CreateParameter("@currentPhase", adVarChar, adParamInput, 50, null)
		   .CommandType = adCmdStoredProc
		End With
			
		Set rs = oCmd.Execute
	
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		materialOnRoadway = request("materialOnRoadway")
		stoneConsolidated = request("stoneConsolidated")
		maintenanceRequired = request("maintenanceRequired")
		exitLocations = request("exitLocations")
		spillsVehicle = request("spillsVehicle")
		spillsEquipment = request("spillsEquipment")
		spillsStorageTanks = request("spillsStorageTanks")
		howSpillHandled	 = request("howSpillHandled")

		'add the answers to the gdot daily inspection
		Set oCmd = Server.CreateObject("ADODB.Command")		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddGDOTDailyInspection"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@materialOnRoadway", adVarChar, adParamInput, 10, materialOnRoadway)
		   .parameters.Append .CreateParameter("@stoneConsolidated", adVarChar, adParamInput, 10, stoneConsolidated)
		   .parameters.Append .CreateParameter("@maintenanceRequired", adVarChar, adParamInput, 10, maintenanceRequired)
		   .parameters.Append .CreateParameter("@exitLocations", adVarChar, adParamInput, 1500, exitLocations)
		   .parameters.Append .CreateParameter("@spillsVehicle", adVarChar, adParamInput, 10, spillsVehicle)
		   .parameters.Append .CreateParameter("@spillsEquipment", adVarChar, adParamInput, 10, spillsEquipment)
		   .parameters.Append .CreateParameter("@spillsStorageTanks", adVarChar, adParamInput, 10, spillsStorageTanks)
		   .parameters.Append .CreateParameter("@howSpillHandled", adVarChar, adParamInput, 1500, howSpillHandled)
		   .CommandType = adCmdStoredProc
		End With
			
		Set rs = oCmd.Execute		
		Set oCmd = nothing
	'	rs.close
	'	set rs = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildGDOTDailyInspectionPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if
		
		response.Redirect "reportList.asp?findReport=" & reportID

	Case "addAccidentOther"
		accidentTypeID = request("accidentTypeID")
		project = request("project")
		clientID = request("clientID")
		generalSuper = request("generalSuper")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddAccidentReport"
		   .parameters.Append .CreateParameter("@accidentTypeID", adInteger, adParamInput, 8, accidentTypeID)
		   .parameters.Append .CreateParameter("@project", adVarchar, adParamInput, 100, project)
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@dateEntered", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
		   .parameters.Append .CreateParameter("@archived", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@isDenied", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@generalSuper", adInteger, adParamInput, 8, generalSuper)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		accidentID = rs("Identity")
	
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		'specific items to this report
		accidentDate = request("accidentDate")
		timeofAccident = request("timeofAccident")
		'location = request("location")
		unitInvolved = request("unitInvolved")
		accidentDescription = request("accidentDescription")
		accidentCause = request("accidentCause")
		injuredName1 = request("injuredName1")
		injury1 = request("injury1")
		injuredAddress1 = request("injuredAddress1")
		injuredHomePhone1 = request("injuredHomePhone1")
		injuredWorkPhone1 = request("injuredWorkPhone1")
		injuredName2 = request("injuredName2")
		injury2 = request("injury2")
		injuredAddress2 = request("injuredAddress2")
		injuredHomePhone2 = request("injuredHomePhone2")
		injuredWorkPhone2 = request("injuredWorkPhone2")
		witnessName1 = request("witnessName1")
		witnessAddress1 = request("witnessAddress1")
		witnessHomePhone1 = request("witnessHomePhone1")
		witnessWorkPhone1 = request("witnessWorkPhone1")
		witnessName2 = request("witnessName2")
		witnessAddress2 = request("witnessAddress2")
		witnessHomePhone2 = request("witnessHomePhone2")
		witnessWorkPhone2 = request("witnessWorkPhone2")
		reported = request("reported")
		policeDepartment = request("policeDepartment")
		dateAndNamePolice = request("dateAndNamePolice")
		lossAmount = request("lossAmount")
		damageOwner1 = request("damageOwner1")
		damageAddress1 = request("damageAddress1")
		damageHomePhone1 = request("damageHomePhone1")
		damageWorkPhone1 = request("damageWorkPhone1")
		damageOwner2 = request("damageOwner2")
		damageAddress2 = request("damageAddress2")
		damageHomePhone2 = request("damageHomePhone2")
		damageWorkPhone2 = request("damageWorkPhone2")
		remarks = request("remarks")
		userID = null
		foreman = request("foreman")
		operator = request("operator")
		injured = request("injured")
		witnesses = request("witnesses")
		reportedBy = request("reportedBy")
		
		
		
		'add data to the accidentOtherReport Table
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddAccidentReportOther"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .parameters.Append .CreateParameter("@accidentDate", adDBTimeStamp, adParamInput, 8, accidentDate)
		   .parameters.Append .CreateParameter("@timeofAccident", adVarChar, adParamInput, 50, timeofAccident)
		  ' .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 200, location)	
		   .parameters.Append .CreateParameter("@unitInvolved", adVarChar, adParamInput, 100, unitInvolved)	
		   .parameters.Append .CreateParameter("@accidentDescription", adVarChar, adParamInput, 8000, accidentDescription)	
		   .parameters.Append .CreateParameter("@accidentCause", adVarChar, adParamInput, 8000, accidentCause)	
		   .parameters.Append .CreateParameter("@injuredName1", adVarChar, adParamInput, 50, injuredName1)	
		   .parameters.Append .CreateParameter("@injury1", adVarChar, adParamInput, 100, injury1)	
		   .parameters.Append .CreateParameter("@injuredAddress1", adVarChar, adParamInput, 100, injuredAddress1)	
		   .parameters.Append .CreateParameter("@injuredHomePhone1", adVarChar, adParamInput, 20, injuredHomePhone1)	
		   .parameters.Append .CreateParameter("@injuredWorkPhone1", adVarChar, adParamInput, 20, injuredWorkPhone1)	
		   .parameters.Append .CreateParameter("@injuredName2", adVarChar, adParamInput, 50, injuredName2)	
		   .parameters.Append .CreateParameter("@injury2", adVarChar, adParamInput, 100, injury2)	
		   .parameters.Append .CreateParameter("@injuredAddress2", adVarChar, adParamInput, 100, injuredAddress2)		   	
		   .parameters.Append .CreateParameter("@injuredHomePhone2", adVarChar, adParamInput, 20, injuredHomePhone2)
		   .parameters.Append .CreateParameter("@injuredWorkPhone2", adVarChar, adParamInput, 20, injuredWorkPhone2)
		   .parameters.Append .CreateParameter("@witnessName1", adVarChar, adParamInput, 50, witnessName1)
		   .parameters.Append .CreateParameter("@witnessAddress1", adVarChar, adParamInput, 100, witnessAddress1)
		   .parameters.Append .CreateParameter("@witnessHomePhone1", adVarChar, adParamInput, 20, witnessHomePhone1)
		   .parameters.Append .CreateParameter("@witnessWorkPhone1", adVarChar, adParamInput, 20, witnessWorkPhone1)
		   .parameters.Append .CreateParameter("@witnessName2", adVarChar, adParamInput, 50, witnessName2)
		   .parameters.Append .CreateParameter("@witnessAddress2", adVarChar, adParamInput, 100, witnessAddress2)
		   .parameters.Append .CreateParameter("@witnessHomePhone2", adVarChar, adParamInput, 20, witnessHomePhone2)
		   .parameters.Append .CreateParameter("@witnessWorkPhone2", adVarChar, adParamInput, 20, witnessWorkPhone2)
		   .parameters.Append .CreateParameter("@reported", adVarChar, adParamInput, 3, reported)
		   .parameters.Append .CreateParameter("@policeDepartment", adVarChar, adParamInput, 100, policeDepartment)
		   .parameters.Append .CreateParameter("@dateAndNamePolice", adVarChar, adParamInput, 100, dateAndNamePolice)
		   .parameters.Append .CreateParameter("@lossAmount", adVarChar, adParamInput, 50, lossAmount)		   
		   .parameters.Append .CreateParameter("@damageOwner1", adVarChar, adParamInput, 50, damageOwner1)
		   .parameters.Append .CreateParameter("@damageAddress1", adVarChar, adParamInput, 100, damageAddress1)	
		   .parameters.Append .CreateParameter("@damageHomePhone1", adVarChar, adParamInput, 20, damageHomePhone1)	
		   .parameters.Append .CreateParameter("@damageWorkPhone1", adVarChar, adParamInput, 20, damageWorkPhone1)	
		   .parameters.Append .CreateParameter("@damageOwner2", adVarChar, adParamInput, 50, damageOwner2)	
		   .parameters.Append .CreateParameter("@damageAddress2", adVarChar, adParamInput, 100, damageAddress2)	
		   .parameters.Append .CreateParameter("@damageHomePhone2", adVarChar, adParamInput, 20, damageHomePhone2)	
		   .parameters.Append .CreateParameter("@damageWorkPhone2", adVarChar, adParamInput, 20, damageWorkPhone2)
		   .parameters.Append .CreateParameter("@remarks", adVarChar, adParamInput, 8000, remarks)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@foreman", adVarChar, adParamInput, 100, foreman)
		   .parameters.Append .CreateParameter("@operator", adVarChar, adParamInput, 100, operator)
		   .parameters.Append .CreateParameter("@injured", adVarChar, adParamInput, 3, injured)
		   .parameters.Append .CreateParameter("@witnesses", adVarChar, adParamInput, 3, witnesses)
		   .parameters.Append .CreateParameter("@reportedBy", adVarChar, adParamInput, 50, reportedBy)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
	
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		'build the PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildBuildAccidentOtherPDF.asp?accidentID=" & accidentID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/accidentReport_" & accidentID & ".pdf"), true )
		
		'email the form to the appropriate party
		'sendRiskEmail accidentID,accidentTypeID,generalSuper
		
		'redirect back to the home page
		response.Redirect "main.asp"
		
	
	Case "addAccidentUtility"
		accidentTypeID = request("accidentTypeID")
		project = request("project")
		clientID = request("clientID")
		generalSuper = request("generalSuper")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddAccidentReport"
		   .parameters.Append .CreateParameter("@accidentTypeID", adInteger, adParamInput, 8, accidentTypeID)
		   .parameters.Append .CreateParameter("@project", adVarchar, adParamInput, 100, project)
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@dateEntered", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
		   .parameters.Append .CreateParameter("@archived", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@isDenied", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@generalSuper", adInteger, adParamInput, 8, generalSuper)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		accidentID = rs("Identity")
	
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		'specific items to this report
		propertyOwnerName = request("propertyOwnerName")
		propertyOwnerAddress = request("propertyOwnerAddress")
		propertyOwnerPhone = request("propertyOwnerPhone")
		incidentDate = request("incidentDate")
		timeofIncident = request("timeofIncident")
		location = request("location")
		incidentDescription = request("incidentDescription")
		jobNumber = null 'removed
		damagedPropertyDescription = request("damagedPropertyDescription")
		crewPresent = request("crewPresent")
		officialsPresent = request("officialsPresent")
		otherWitnesses = request("otherWitnesses")
		sufficientDepthHeight = request("sufficientDepthHeight")
		utilitiesShown = request("utilitiesShown")
		utilityInformed = request("utilityInformed")
		byWhom = null'request("byWhom")
		locateTicketNumber = request("locateTicketNumber")
		describeConversation = request("describeConversation")
		describeDamageAndDelays = request("describeDamageAndDelays")
		userID = null'request("userID")
		utilityMarked = request("utilityMarked")
		projectNumber = request("projectNumber")
		foreman = request("foreman")
		operator = request("operator")
		reportedBy = request("reportedBy")
		
		
		
		'add data to the accidentOtherReport Table
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddAccidentReportUtility"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .parameters.Append .CreateParameter("@propertyOwnerName", adVarChar, adParamInput, 50, propertyOwnerName)
		   .parameters.Append .CreateParameter("@propertyOwnerAddress", adVarChar, adParamInput, 100, propertyOwnerAddress)
		   .parameters.Append .CreateParameter("@propertyOwnerPhone", adVarChar, adParamInput, 20, propertyOwnerPhone)
		   .parameters.Append .CreateParameter("@incidentDate", adDBTimeStamp, adParamInput, 8, incidentDate)
		   .parameters.Append .CreateParameter("@timeofIncident", adVarChar, adParamInput, 50, timeofIncident)
		   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 200, location)
		   .parameters.Append .CreateParameter("@incidentDescription", adVarChar, adParamInput, 8000, incidentDescription)
		   .parameters.Append .CreateParameter("@jobNumber", adVarChar, adParamInput, 50, jobNumber)
		   .parameters.Append .CreateParameter("@damagedPropertyDescription", adVarChar, adParamInput, 8000, damagedPropertyDescription)
		   .parameters.Append .CreateParameter("@crewPresent", adVarChar, adParamInput, 100, crewPresent)
		   .parameters.Append .CreateParameter("@officialsPresent", adVarChar, adParamInput, 100, officialsPresent)
		   .parameters.Append .CreateParameter("@otherWitnesses", adVarChar, adParamInput, 100, otherWitnesses)
		   .parameters.Append .CreateParameter("@sufficientDepthHeight", adVarChar, adParamInput, 3, sufficientDepthHeight)
		   .parameters.Append .CreateParameter("@utilitiesShown", adVarChar, adParamInput, 3, utilitiesShown)
		   .parameters.Append .CreateParameter("@utilityInformed", adVarChar, adParamInput, 3, utilityInformed)
		   .parameters.Append .CreateParameter("@byWhom", adVarChar, adParamInput, 100, byWhom)
		   .parameters.Append .CreateParameter("@locateTicketNumber", adVarChar, adParamInput, 100, locateTicketNumber)
		   .parameters.Append .CreateParameter("@describeConversation", adVarChar, adParamInput, 8000, describeConversation)
		   .parameters.Append .CreateParameter("@describeDamageAndDelays", adVarChar, adParamInput, 8000, describeDamageAndDelays)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@utilityMarked", adVarChar, adParamInput, 3, utilityMarked)
		   .parameters.Append .CreateParameter("@projectNumber", adVarChar, adParamInput, 50, projectNumber)
		   .parameters.Append .CreateParameter("@foreman", adVarChar, adParamInput, 50, foreman)
		   .parameters.Append .CreateParameter("@operator", adVarChar, adParamInput, 50, operator)
		   .parameters.Append .CreateParameter("@reportedBy", adVarChar, adParamInput, 50, reportedBy)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
	
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		'build the PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildBuildAccidentUtilityPDF.asp?accidentID=" & accidentID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/accidentReport_" & accidentID & ".pdf"), true )
		
		'email the form to the appropriate party
		'sendRiskEmail accidentID,accidentTypeID,generalSuper
		
		'redirect back to the home page
		response.Redirect "main.asp"

	Case "addAccidentVehicular"
		accidentTypeID = request("accidentTypeID")
		project = request("project")
		clientID = request("clientID")
		
		'get the superintendent's user ID
		generalSuper = request("generalSuper")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddAccidentReport"
		   .parameters.Append .CreateParameter("@accidentTypeID", adInteger, adParamInput, 8, accidentTypeID)
		   .parameters.Append .CreateParameter("@project", adVarchar, adParamInput, 100, project)
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@dateEntered", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
		   .parameters.Append .CreateParameter("@archived", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@isDenied", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@generalSuper", adInteger, adParamInput, 8, generalSuper)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		accidentID = rs("Identity")
	
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		'specific items to this report
		yearAndMake = request("yearAndMake")
		model = request("model")
		driversName = request("driversName")
		unitNumber = request("unitNumber")
		division = request("division")
		foreman = request("foreman")
		hoursOnDuty = request("hoursOnDuty")
		yearsEmployed = request("yearsEmployed")
		previousAccidentsEmploy = request("previousAccidentsEmploy")
		previousAccidentsYear = request("previousAccidentsYear")
		doingPriorToAccident = request("doingPriorToAccident")
		accidentDate = request("accidentDate")
		dayOfWeek = request("dayOfWeek")
		timeofAccident = request("timeofAccident")
		state = request("state")
		county = request("county")
		if county = "" then
			county = null
		end if
		streetNumber = request("streetNumber")
		intersectingStreets = request("intersectingStreets")
		vehicle1Owner = request("vehicle1Owner")
		userID = null'request("userID")
		vehicle1Address = request("vehicle1Address")
		vehicle1Age = request("vehicle1Age")
		vehicle1DLNo = request("vehicle1DLNo")
		vehicle1YearMakeModel = request("vehicle1YearMakeModel")
		vehicle1Damage = request("vehicle1Damage")
		vehicle1Drivable = request("vehicle1Drivable")
		vehicle1RepairCost = request("vehicle1RepairCost")
		vehicle2Owner = request("vehicle2Owner")
		vehicle2Driver = request("vehicle2Driver")
		vehicle2Address = request("vehicle2Address")
		vehicle2Age = request("vehicle2Age")
		vehicle2DLNo = request("vehicle2DLNo")
		vehicle2YearMakeModel = request("vehicle2YearMakeModel")
		vehicle2Damage = request("vehicle2Damage")
		vehicle2Drivable = request("vehicle2Drivable")
		vehicle2RepairCost = request("vehicle2RepairCost")
		vehicle2HomeNumber = request("vehicle2HomeNumber")
		vehicle2OfficeNumber = request("vehicle2OfficeNumber")
		vehicle2CellNumber = request("vehicle2CellNumber")
		vehicle2InsuranceCo = request("vehicle2InsuranceCo")
		vehicle2PolicyNo = request("vehicle2PolicyNo")
		otherPropertyDamage = request("otherPropertyDamage")
		otherPropertyRepairCost = request("otherPropertyRepairCost")
		injury1Name = request("injury1Name")
		injury1Address = request("injury1Address")
		injury1NatureOfInjury = request("injury1NatureOfInjury")
		injury1Ambulance = request("injury1Ambulance")
		injury2Name = request("injury2Name")
		injury2Address = request("injury2Address")
		injury2NatureOfInjury = request("injury2NatureOfInjury")
		injury2Ambulance = request("injury2Ambulance")
		otherDriverRemarks = request("otherDriverRemarks")
		driverRemarks = request("driverRemarks")
		roadCharacter = request("roadCharacter")
		roadService = request("roadService")
		roadEffect = request("roadEffect")
		light = request("light")
		weather = request("weather")
		trafficControl = request("trafficControl")
		witnessName1 = request("witnessName1")
		witnessPhone1 = request("witnessPhone1")
		witnessAddress1 = request("witnessAddress1")
		witnessName2 = request("witnessName2")
		witnessPhone2 = request("witnessPhone2")
		witnessAddress2 = request("witnessAddress2")
		investigatingOfficer = request("investigatingOfficer")
		department = request("department")
		caseNo = request("caseNo")
		describeWhatHappened = request("describeWhatHappened")
		reportedToPolice = request("reportedToPolice")
		reportedBy = request("reportedBy")
		vehicle1Driver = request("vehicle1Driver")
		
		
		'add data to the accidentVehicularReport Table
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddAccidentReportVehicular"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .parameters.Append .CreateParameter("@yearAndMake", adVarChar, adParamInput, 50, yearAndMake)		   
		   .parameters.Append .CreateParameter("@model", adVarChar, adParamInput, 50, model)
		   .parameters.Append .CreateParameter("@driversName", adVarChar, adParamInput, 50, driversName)
		   .parameters.Append .CreateParameter("@unitNumber", adVarChar, adParamInput, 50, unitNumber)
		   .parameters.Append .CreateParameter("@division", adVarChar, adParamInput, 50, division)
		   .parameters.Append .CreateParameter("@foreman", adVarChar, adParamInput, 50, foreman)
		   .parameters.Append .CreateParameter("@hoursOnDuty", adVarChar, adParamInput, 50, hoursOnDuty)
		   .parameters.Append .CreateParameter("@yearsEmployed", adVarChar, adParamInput, 50, yearsEmployed)
		   .parameters.Append .CreateParameter("@previousAccidentsEmploy", adVarChar, adParamInput, 50, previousAccidentsEmploy)
		   .parameters.Append .CreateParameter("@previousAccidentsYear", adVarChar, adParamInput, 50, previousAccidentsYear)
		   .parameters.Append .CreateParameter("@doingPriorToAccident", adVarChar, adParamInput, 2000, doingPriorToAccident)
		   .parameters.Append .CreateParameter("@accidentDate", adDBTimeStamp, adParamInput, 8, accidentDate)
		   .parameters.Append .CreateParameter("@dayOfWeek", adVarChar, adParamInput, 50, dayOfWeek)
		   .parameters.Append .CreateParameter("@timeofAccident", adVarChar, adParamInput, 50, timeofAccident)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@county", adInteger, adParamInput, 8, county)
		   .parameters.Append .CreateParameter("@streetNumber", adVarChar, adParamInput, 100, streetNumber)
		   .parameters.Append .CreateParameter("@intersectingStreets", adVarChar, adParamInput, 100, intersectingStreets)
		   .parameters.Append .CreateParameter("@vehicle1Owner", adVarChar, adParamInput, 50, vehicle1Owner)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@vehicle1Address", adVarChar, adParamInput, 100, vehicle1Address)
		   .parameters.Append .CreateParameter("@vehicle1Age", adVarChar, adParamInput, 50, vehicle1Age)		   
		   .parameters.Append .CreateParameter("@vehicle1DLNo", adVarChar, adParamInput, 50, vehicle1DLNo)
		   .parameters.Append .CreateParameter("@vehicle1YearMakeModel", adVarChar, adParamInput, 100, vehicle1YearMakeModel)
		   .parameters.Append .CreateParameter("@vehicle1Damage", adVarChar, adParamInput, 500, vehicle1Damage)
		   .parameters.Append .CreateParameter("@vehicle1Drivable", adVarChar, adParamInput, 50, vehicle1Drivable)
		   .parameters.Append .CreateParameter("@vehicle1RepairCost", adVarChar, adParamInput, 50, vehicle1RepairCost)
		   .parameters.Append .CreateParameter("@vehicle2Owner", adVarChar, adParamInput, 50, vehicle2Owner)
		   .parameters.Append .CreateParameter("@vehicle2Driver", adVarChar, adParamInput, 50, vehicle2Driver)
		   .parameters.Append .CreateParameter("@vehicle2Address", adVarChar, adParamInput, 100, vehicle2Address)
		   .parameters.Append .CreateParameter("@vehicle2Age", adVarChar, adParamInput, 50, vehicle2Age)
		   .parameters.Append .CreateParameter("@vehicle2DLNo", adVarChar, adParamInput, 50, vehicle2DLNo)
		   .parameters.Append .CreateParameter("@vehicle2YearMakeModel", adVarChar, adParamInput, 100, vehicle2YearMakeModel)
		   .parameters.Append .CreateParameter("@vehicle2Damage", adVarChar, adParamInput, 500, vehicle2Damage)		   
		   .parameters.Append .CreateParameter("@vehicle2Drivable", adVarChar, adParamInput, 50, vehicle2Drivable)
		   .parameters.Append .CreateParameter("@vehicle2RepairCost", adVarChar, adParamInput, 50, vehicle2RepairCost)
		   .parameters.Append .CreateParameter("@vehicle2HomeNumber", adVarChar, adParamInput, 50, vehicle2HomeNumber)
		   .parameters.Append .CreateParameter("@vehicle2OfficeNumber", adVarChar, adParamInput, 50, vehicle2OfficeNumber)
		   .parameters.Append .CreateParameter("@vehicle2CellNumber", adVarChar, adParamInput, 50, vehicle2CellNumber)
		   .parameters.Append .CreateParameter("@vehicle2InsuranceCo", adVarChar, adParamInput, 100, vehicle2InsuranceCo)
		   .parameters.Append .CreateParameter("@vehicle2PolicyNo", adVarChar, adParamInput, 50, vehicle2PolicyNo)		   
		   .parameters.Append .CreateParameter("@otherPropertyDamage", adVarChar, adParamInput, 200, otherPropertyDamage)
		   .parameters.Append .CreateParameter("@otherPropertyRepairCost", adVarChar, adParamInput, 50, otherPropertyRepairCost)
		   .parameters.Append .CreateParameter("@injury1Name", adVarChar, adParamInput, 50, injury1Name)
		   .parameters.Append .CreateParameter("@injury1Address", adVarChar, adParamInput, 100, injury1Address)
		   .parameters.Append .CreateParameter("@injury1NatureOfInjury", adVarChar, adParamInput, 100, injury1NatureOfInjury)
		   .parameters.Append .CreateParameter("@injury1Ambulance", adVarChar, adParamInput, 50, injury1Ambulance)
		   .parameters.Append .CreateParameter("@injury2Name", adVarChar, adParamInput, 50, injury2Name)
		   .parameters.Append .CreateParameter("@injury2Address", adVarChar, adParamInput, 100, injury2Address)
		   .parameters.Append .CreateParameter("@injury2NatureOfInjury", adVarChar, adParamInput, 100, injury2NatureOfInjury)
		   .parameters.Append .CreateParameter("@injury2Ambulance", adVarChar, adParamInput, 50, injury2Ambulance)
		   .parameters.Append .CreateParameter("@otherDriverRemarks", adVarChar, adParamInput, 2000, otherDriverRemarks)
		   .parameters.Append .CreateParameter("@driverRemarks", adVarChar, adParamInput, 2000, driverRemarks)
		   .parameters.Append .CreateParameter("@roadCharacter", adVarChar, adParamInput, 50, roadCharacter)
		   .parameters.Append .CreateParameter("@roadService", adVarChar, adParamInput, 50, roadService)		   
		   .parameters.Append .CreateParameter("@roadEffect", adVarChar, adParamInput, 50, roadEffect)
		   .parameters.Append .CreateParameter("@light", adVarChar, adParamInput, 50, light)
		   .parameters.Append .CreateParameter("@weather", adVarChar, adParamInput, 50, weather)
		   .parameters.Append .CreateParameter("@trafficControl", adVarChar, adParamInput, 50, trafficControl)
		   .parameters.Append .CreateParameter("@witnessName1", adVarChar, adParamInput, 50, witnessName1)
		   .parameters.Append .CreateParameter("@witnessPhone1", adVarChar, adParamInput, 50, witnessPhone1)
		   .parameters.Append .CreateParameter("@witnessAddress1", adVarChar, adParamInput, 100, witnessAddress1)
		   .parameters.Append .CreateParameter("@witnessName2", adVarChar, adParamInput, 50, witnessName2)
		   .parameters.Append .CreateParameter("@witnessPhone2", adVarChar, adParamInput, 50, witnessPhone2)
		   .parameters.Append .CreateParameter("@witnessAddress2", adVarChar, adParamInput, 100, witnessAddress2)
		   .parameters.Append .CreateParameter("@investigatingOfficer", adVarChar, adParamInput, 50, investigatingOfficer)
		   .parameters.Append .CreateParameter("@department", adVarChar, adParamInput, 50, department)
		   .parameters.Append .CreateParameter("@caseNo", adVarChar, adParamInput, 50, caseNo)
		   .parameters.Append .CreateParameter("@describeWhatHappened", adVarChar, adParamInput, 8000, describeWhatHappened)
		   .parameters.Append .CreateParameter("@reportedToPolice", adVarChar, adParamInput, 3, reportedToPolice)
		   .parameters.Append .CreateParameter("@reportedBy", adVarChar, adParamInput, 50, reportedBy)
		   .parameters.Append .CreateParameter("@vehicle1Driver", adVarChar, adParamInput, 50, vehicle1Driver)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
	
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'build the PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildBuildAccidentVehiclePDF.asp?accidentID=" & accidentID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/accidentReport_" & accidentID & ".pdf"), true )
		
		'email the form to the appropriate party
	'	sendRiskEmail accidentID,accidentTypeID,generalSuper
		
		'redirect back to the home page
		response.Redirect "main.asp"
		
	Case "sendAccidentReport"
	
		accidentID = request("accidentID")
		accidentTypeID = request("accidentTypeID")
		generalSuper = request("generalSuper")
		clientID = request("clientID")
		
		sendRiskEmail accidentID,accidentTypeID,generalSuper
		
		response.Redirect "accidentReportList.asp?clientID=" & clientID & "&msg=Your accident report has been sent to the selected general superintendent"
		
	case "editAccidentVehicular"

		'get the superintendent's user ID
		generalSuper = request("generalSuper")
		accidentID = request("accidentID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditAccidentReport"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .parameters.Append .CreateParameter("@generalSuper", adInteger, adParamInput, 8, generalSuper)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
	
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
	
		'specific items to this report
		
		yearAndMake = request("yearAndMake")
		model = request("model")
		driversName = request("driversName")
		unitNumber = request("unitNumber")
		division = request("division")
		foreman = request("foreman")
		hoursOnDuty = request("hoursOnDuty")
		yearsEmployed = request("yearsEmployed")
		previousAccidentsEmploy = request("previousAccidentsEmploy")
		previousAccidentsYear = request("previousAccidentsYear")
		doingPriorToAccident = request("doingPriorToAccident")
		accidentDate = request("accidentDate")
		dayOfWeek = request("dayOfWeek")
		timeofAccident = request("timeofAccident")
		state = request("state")
		county = request("county")
		if county = "" then
			county = null
		end if
		streetNumber = request("streetNumber")
		intersectingStreets = request("intersectingStreets")
		vehicle1Owner = request("vehicle1Owner")
		userID = null'request("userID")
		vehicle1Address = request("vehicle1Address")
		vehicle1Age = request("vehicle1Age")
		vehicle1DLNo = request("vehicle1DLNo")
		vehicle1YearMakeModel = request("vehicle1YearMakeModel")
		vehicle1Damage = request("vehicle1Damage")
		vehicle1Drivable = request("vehicle1Drivable")
		vehicle1RepairCost = request("vehicle1RepairCost")
		vehicle2Owner = request("vehicle2Owner")
		vehicle2Driver = request("vehicle2Driver")
		vehicle2Address = request("vehicle2Address")
		vehicle2Age = request("vehicle2Age")
		vehicle2DLNo = request("vehicle2DLNo")
		vehicle2YearMakeModel = request("vehicle2YearMakeModel")
		vehicle2Damage = request("vehicle2Damage")
		vehicle2Drivable = request("vehicle2Drivable")
		vehicle2RepairCost = request("vehicle2RepairCost")
		vehicle2HomeNumber = request("vehicle2HomeNumber")
		vehicle2OfficeNumber = request("vehicle2OfficeNumber")
		vehicle2CellNumber = request("vehicle2CellNumber")
		vehicle2InsuranceCo = request("vehicle2InsuranceCo")
		vehicle2PolicyNo = request("vehicle2PolicyNo")
		otherPropertyDamage = request("otherPropertyDamage")
		otherPropertyRepairCost = request("otherPropertyRepairCost")
		injury1Name = request("injury1Name")
		injury1Address = request("injury1Address")
		injury1NatureOfInjury = request("injury1NatureOfInjury")
		injury1Ambulance = request("injury1Ambulance")
		injury2Name = request("injury2Name")
		injury2Address = request("injury2Address")
		injury2NatureOfInjury = request("injury2NatureOfInjury")
		injury2Ambulance = request("injury2Ambulance")
		otherDriverRemarks = request("otherDriverRemarks")
		driverRemarks = request("driverRemarks")
		roadCharacter = request("roadCharacter")
		roadService = request("roadService")
		roadEffect = request("roadEffect")
		light = request("light")
		weather = request("weather")
		trafficControl = request("trafficControl")
		witnessName1 = request("witnessName1")
		witnessPhone1 = request("witnessPhone1")
		witnessAddress1 = request("witnessAddress1")
		witnessName2 = request("witnessName2")
		witnessPhone2 = request("witnessPhone2")
		witnessAddress2 = request("witnessAddress2")
		investigatingOfficer = request("investigatingOfficer")
		department = request("department")
		caseNo = request("caseNo")
		describeWhatHappened = request("describeWhatHappened")
		reportedToPolice = request("reportedToPolice")
		reportedBy = request("reportedBy")
		vehicle1Driver = request("vehicle1Driver")
		
		'get the superintendent's user ID
		generalSuper = request("generalSuper")
		
		'add data to the accidentVehicularReport Table
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditAccidentReportVehicular"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .parameters.Append .CreateParameter("@yearAndMake", adVarChar, adParamInput, 50, yearAndMake)		   
		   .parameters.Append .CreateParameter("@model", adVarChar, adParamInput, 50, model)
		   .parameters.Append .CreateParameter("@driversName", adVarChar, adParamInput, 50, driversName)
		   .parameters.Append .CreateParameter("@unitNumber", adVarChar, adParamInput, 50, unitNumber)
		   .parameters.Append .CreateParameter("@division", adVarChar, adParamInput, 50, division)
		   .parameters.Append .CreateParameter("@foreman", adVarChar, adParamInput, 50, foreman)
		   .parameters.Append .CreateParameter("@hoursOnDuty", adVarChar, adParamInput, 50, hoursOnDuty)
		   .parameters.Append .CreateParameter("@yearsEmployed", adVarChar, adParamInput, 50, yearsEmployed)
		   .parameters.Append .CreateParameter("@previousAccidentsEmploy", adVarChar, adParamInput, 50, previousAccidentsEmploy)
		   .parameters.Append .CreateParameter("@previousAccidentsYear", adVarChar, adParamInput, 50, previousAccidentsYear)
		   .parameters.Append .CreateParameter("@doingPriorToAccident", adVarChar, adParamInput, 2000, doingPriorToAccident)
		   .parameters.Append .CreateParameter("@accidentDate", adDBTimeStamp, adParamInput, 8, accidentDate)
		   .parameters.Append .CreateParameter("@dayOfWeek", adVarChar, adParamInput, 50, dayOfWeek)
		   .parameters.Append .CreateParameter("@timeofAccident", adVarChar, adParamInput, 50, timeofAccident)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@county", adInteger, adParamInput, 8, county)
		   .parameters.Append .CreateParameter("@streetNumber", adVarChar, adParamInput, 100, streetNumber)
		   .parameters.Append .CreateParameter("@intersectingStreets", adVarChar, adParamInput, 100, intersectingStreets)
		   .parameters.Append .CreateParameter("@vehicle1Owner", adVarChar, adParamInput, 50, vehicle1Owner)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@vehicle1Address", adVarChar, adParamInput, 100, vehicle1Address)
		   .parameters.Append .CreateParameter("@vehicle1Age", adVarChar, adParamInput, 50, vehicle1Age)		   
		   .parameters.Append .CreateParameter("@vehicle1DLNo", adVarChar, adParamInput, 50, vehicle1DLNo)
		   .parameters.Append .CreateParameter("@vehicle1YearMakeModel", adVarChar, adParamInput, 100, vehicle1YearMakeModel)
		   .parameters.Append .CreateParameter("@vehicle1Damage", adVarChar, adParamInput, 500, vehicle1Damage)
		   .parameters.Append .CreateParameter("@vehicle1Drivable", adVarChar, adParamInput, 50, vehicle1Drivable)
		   .parameters.Append .CreateParameter("@vehicle1RepairCost", adVarChar, adParamInput, 50, vehicle1RepairCost)
		   .parameters.Append .CreateParameter("@vehicle2Owner", adVarChar, adParamInput, 50, vehicle2Owner)
		   .parameters.Append .CreateParameter("@vehicle2Driver", adVarChar, adParamInput, 50, vehicle2Driver)
		   .parameters.Append .CreateParameter("@vehicle2Address", adVarChar, adParamInput, 100, vehicle2Address)
		   .parameters.Append .CreateParameter("@vehicle2Age", adVarChar, adParamInput, 50, vehicle2Age)
		   .parameters.Append .CreateParameter("@vehicle2DLNo", adVarChar, adParamInput, 50, vehicle2DLNo)
		   .parameters.Append .CreateParameter("@vehicle2YearMakeModel", adVarChar, adParamInput, 100, vehicle2YearMakeModel)
		   .parameters.Append .CreateParameter("@vehicle2Damage", adVarChar, adParamInput, 500, vehicle2Damage)		   
		   .parameters.Append .CreateParameter("@vehicle2Drivable", adVarChar, adParamInput, 50, vehicle2Drivable)
		   .parameters.Append .CreateParameter("@vehicle2RepairCost", adVarChar, adParamInput, 50, vehicle2RepairCost)
		   .parameters.Append .CreateParameter("@vehicle2HomeNumber", adVarChar, adParamInput, 50, vehicle2HomeNumber)
		   .parameters.Append .CreateParameter("@vehicle2OfficeNumber", adVarChar, adParamInput, 50, vehicle2OfficeNumber)
		   .parameters.Append .CreateParameter("@vehicle2CellNumber", adVarChar, adParamInput, 50, vehicle2CellNumber)
		   .parameters.Append .CreateParameter("@vehicle2InsuranceCo", adVarChar, adParamInput, 100, vehicle2InsuranceCo)
		   .parameters.Append .CreateParameter("@vehicle2PolicyNo", adVarChar, adParamInput, 50, vehicle2PolicyNo)		   
		   .parameters.Append .CreateParameter("@otherPropertyDamage", adVarChar, adParamInput, 200, otherPropertyDamage)
		   .parameters.Append .CreateParameter("@otherPropertyRepairCost", adVarChar, adParamInput, 50, otherPropertyRepairCost)
		   .parameters.Append .CreateParameter("@injury1Name", adVarChar, adParamInput, 50, injury1Name)
		   .parameters.Append .CreateParameter("@injury1Address", adVarChar, adParamInput, 100, injury1Address)
		   .parameters.Append .CreateParameter("@injury1NatureOfInjury", adVarChar, adParamInput, 100, injury1NatureOfInjury)
		   .parameters.Append .CreateParameter("@injury1Ambulance", adVarChar, adParamInput, 50, injury1Ambulance)
		   .parameters.Append .CreateParameter("@injury2Name", adVarChar, adParamInput, 50, injury2Name)
		   .parameters.Append .CreateParameter("@injury2Address", adVarChar, adParamInput, 100, injury2Address)
		   .parameters.Append .CreateParameter("@injury2NatureOfInjury", adVarChar, adParamInput, 100, injury2NatureOfInjury)
		   .parameters.Append .CreateParameter("@injury2Ambulance", adVarChar, adParamInput, 50, injury2Ambulance)
		   .parameters.Append .CreateParameter("@otherDriverRemarks", adVarChar, adParamInput, 2000, otherDriverRemarks)
		   .parameters.Append .CreateParameter("@driverRemarks", adVarChar, adParamInput, 2000, driverRemarks)
		   .parameters.Append .CreateParameter("@roadCharacter", adVarChar, adParamInput, 50, roadCharacter)
		   .parameters.Append .CreateParameter("@roadService", adVarChar, adParamInput, 50, roadService)		   
		   .parameters.Append .CreateParameter("@roadEffect", adVarChar, adParamInput, 50, roadEffect)
		   .parameters.Append .CreateParameter("@light", adVarChar, adParamInput, 50, light)
		   .parameters.Append .CreateParameter("@weather", adVarChar, adParamInput, 50, weather)
		   .parameters.Append .CreateParameter("@trafficControl", adVarChar, adParamInput, 50, trafficControl)
		   .parameters.Append .CreateParameter("@witnessName1", adVarChar, adParamInput, 50, witnessName1)
		   .parameters.Append .CreateParameter("@witnessPhone1", adVarChar, adParamInput, 50, witnessPhone1)
		   .parameters.Append .CreateParameter("@witnessAddress1", adVarChar, adParamInput, 100, witnessAddress1)
		   .parameters.Append .CreateParameter("@witnessName2", adVarChar, adParamInput, 50, witnessName2)
		   .parameters.Append .CreateParameter("@witnessPhone2", adVarChar, adParamInput, 50, witnessPhone2)
		   .parameters.Append .CreateParameter("@witnessAddress2", adVarChar, adParamInput, 100, witnessAddress2)
		   .parameters.Append .CreateParameter("@investigatingOfficer", adVarChar, adParamInput, 50, investigatingOfficer)
		   .parameters.Append .CreateParameter("@department", adVarChar, adParamInput, 50, department)
		   .parameters.Append .CreateParameter("@caseNo", adVarChar, adParamInput, 50, caseNo)
		   .parameters.Append .CreateParameter("@describeWhatHappened", adVarChar, adParamInput, 8000, describeWhatHappened)
		   .parameters.Append .CreateParameter("@reportedToPolice", adVarChar, adParamInput, 3, reportedToPolice)
		   .parameters.Append .CreateParameter("@reportedBy", adVarChar, adParamInput, 50, reportedBy)
		   .parameters.Append .CreateParameter("@vehicle1Driver", adVarChar, adParamInput, 50, vehicle1Driver)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
	
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		'build the PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildBuildAccidentVehiclePDF.asp?accidentID=" & accidentID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/accidentReport_" & accidentID & ".pdf"), true )
		
		'email the form to the appropriate party
		'sendRiskEmail accidentID,accidentTypeID,generalSuper
		
		'redirect back to the home page
		response.Redirect "main.asp"
	
	case "editAccidentOther"
	
		'get the superintendent's user ID
		generalSuper = request("generalSuper")
		accidentID = request("accidentID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditAccidentReport"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .parameters.Append .CreateParameter("@generalSuper", adInteger, adParamInput, 8, generalSuper)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
	
		Set oCmd = nothing
	'	rs.close
	'	set rs = nothing
			
		'specific items to this report
		accidentDate = request("accidentDate")
		timeofAccident = request("timeofAccident")
		'location = request("location")
		unitInvolved = request("unitInvolved")
		accidentDescription = request("accidentDescription")
		accidentCause = request("accidentCause")
		injuredName1 = request("injuredName1")
		injury1 = request("injury1")
		injuredAddress1 = request("injuredAddress1")
		injuredHomePhone1 = request("injuredHomePhone1")
		injuredWorkPhone1 = request("injuredWorkPhone1")
		injuredName2 = request("injuredName2")
		injury2 = request("injury2")
		injuredAddress2 = request("injuredAddress2")
		injuredHomePhone2 = request("injuredHomePhone2")
		injuredWorkPhone2 = request("injuredWorkPhone2")
		witnessName1 = request("witnessName1")
		witnessAddress1 = request("witnessAddress1")
		witnessHomePhone1 = request("witnessHomePhone1")
		witnessWorkPhone1 = request("witnessWorkPhone1")
		witnessName2 = request("witnessName2")
		witnessAddress2 = request("witnessAddress2")
		witnessHomePhone2 = request("witnessHomePhone2")
		witnessWorkPhone2 = request("witnessWorkPhone2")
		reported = request("reported")
		policeDepartment = request("policeDepartment")
		dateAndNamePolice = request("dateAndNamePolice")
		lossAmount = request("lossAmount")
		damageOwner1 = request("damageOwner1")
		damageAddress1 = request("damageAddress1")
		damageHomePhone1 = request("damageHomePhone1")
		damageWorkPhone1 = request("damageWorkPhone1")
		damageOwner2 = request("damageOwner2")
		damageAddress2 = request("damageAddress2")
		damageHomePhone2 = request("damageHomePhone2")
		damageWorkPhone2 = request("damageWorkPhone2")
		remarks = request("remarks")
		userID = null
		foreman = request("foreman")
		operator = request("operator")
		injured = request("injured")
		witnesses = request("witnesses")
		reportedBy = request("reportedBy")
		
		generalSuper = request("generalSuper")
		
		'add data to the accidentOtherReport Table
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditAccidentReportOther"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .parameters.Append .CreateParameter("@accidentDate", adDBTimeStamp, adParamInput, 8, accidentDate)
		   .parameters.Append .CreateParameter("@timeofAccident", adVarChar, adParamInput, 50, timeofAccident)
		  ' .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 200, location)	
		   .parameters.Append .CreateParameter("@unitInvolved", adVarChar, adParamInput, 100, unitInvolved)	
		   .parameters.Append .CreateParameter("@accidentDescription", adVarChar, adParamInput, 8000, accidentDescription)	
		   .parameters.Append .CreateParameter("@accidentCause", adVarChar, adParamInput, 8000, accidentCause)	
		   .parameters.Append .CreateParameter("@injuredName1", adVarChar, adParamInput, 50, injuredName1)	
		   .parameters.Append .CreateParameter("@injury1", adVarChar, adParamInput, 100, injury1)	
		   .parameters.Append .CreateParameter("@injuredAddress1", adVarChar, adParamInput, 100, injuredAddress1)	
		   .parameters.Append .CreateParameter("@injuredHomePhone1", adVarChar, adParamInput, 20, injuredHomePhone1)	
		   .parameters.Append .CreateParameter("@injuredWorkPhone1", adVarChar, adParamInput, 20, injuredWorkPhone1)	
		   .parameters.Append .CreateParameter("@injuredName2", adVarChar, adParamInput, 50, injuredName2)	
		   .parameters.Append .CreateParameter("@injury2", adVarChar, adParamInput, 100, injury2)	
		   .parameters.Append .CreateParameter("@injuredAddress2", adVarChar, adParamInput, 100, injuredAddress2)		   	
		   .parameters.Append .CreateParameter("@injuredHomePhone2", adVarChar, adParamInput, 20, injuredHomePhone2)
		   .parameters.Append .CreateParameter("@injuredWorkPhone2", adVarChar, adParamInput, 20, injuredWorkPhone2)
		   .parameters.Append .CreateParameter("@witnessName1", adVarChar, adParamInput, 50, witnessName1)
		   .parameters.Append .CreateParameter("@witnessAddress1", adVarChar, adParamInput, 100, witnessAddress1)
		   .parameters.Append .CreateParameter("@witnessHomePhone1", adVarChar, adParamInput, 20, witnessHomePhone1)
		   .parameters.Append .CreateParameter("@witnessWorkPhone1", adVarChar, adParamInput, 20, witnessWorkPhone1)
		   .parameters.Append .CreateParameter("@witnessName2", adVarChar, adParamInput, 50, witnessName2)
		   .parameters.Append .CreateParameter("@witnessAddress2", adVarChar, adParamInput, 100, witnessAddress2)
		   .parameters.Append .CreateParameter("@witnessHomePhone2", adVarChar, adParamInput, 20, witnessHomePhone2)
		   .parameters.Append .CreateParameter("@witnessWorkPhone2", adVarChar, adParamInput, 20, witnessWorkPhone2)
		   .parameters.Append .CreateParameter("@reported", adVarChar, adParamInput, 3, reported)
		   .parameters.Append .CreateParameter("@policeDepartment", adVarChar, adParamInput, 100, policeDepartment)
		   .parameters.Append .CreateParameter("@dateAndNamePolice", adVarChar, adParamInput, 100, dateAndNamePolice)
		   .parameters.Append .CreateParameter("@lossAmount", adVarChar, adParamInput, 50, lossAmount)		   
		   .parameters.Append .CreateParameter("@damageOwner1", adVarChar, adParamInput, 50, damageOwner1)
		   .parameters.Append .CreateParameter("@damageAddress1", adVarChar, adParamInput, 100, damageAddress1)	
		   .parameters.Append .CreateParameter("@damageHomePhone1", adVarChar, adParamInput, 20, damageHomePhone1)	
		   .parameters.Append .CreateParameter("@damageWorkPhone1", adVarChar, adParamInput, 20, damageWorkPhone1)	
		   .parameters.Append .CreateParameter("@damageOwner2", adVarChar, adParamInput, 50, damageOwner2)	
		   .parameters.Append .CreateParameter("@damageAddress2", adVarChar, adParamInput, 100, damageAddress2)	
		   .parameters.Append .CreateParameter("@damageHomePhone2", adVarChar, adParamInput, 20, damageHomePhone2)	
		   .parameters.Append .CreateParameter("@damageWorkPhone2", adVarChar, adParamInput, 20, damageWorkPhone2)
		   .parameters.Append .CreateParameter("@remarks", adVarChar, adParamInput, 8000, remarks)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@foreman", adVarChar, adParamInput, 100, foreman)
		   .parameters.Append .CreateParameter("@operator", adVarChar, adParamInput, 100, operator)
		   .parameters.Append .CreateParameter("@injured", adVarChar, adParamInput, 3, injured)
		   .parameters.Append .CreateParameter("@witnesses", adVarChar, adParamInput, 3, witnesses)
		   .parameters.Append .CreateParameter("@reportedBy", adVarChar, adParamInput, 50, reportedBy)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
	
		Set oCmd = nothing
	'	rs.close
	'	set rs = nothing
		
		'build the PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildBuildAccidentOtherPDF.asp?accidentID=" & accidentID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/accidentReport_" & accidentID & ".pdf"), true )
		
		'email the form to the appropriate party
		'sendRiskEmail accidentID,accidentTypeID,generalSuper
		
		'redirect back to the home page
		response.Redirect "main.asp"
	
	case "editAccidentUtility"
	
		'get the superintendent's user ID
		generalSuper = request("generalSuper")
		accidentID = request("accidentID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditAccidentReport"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .parameters.Append .CreateParameter("@generalSuper", adInteger, adParamInput, 8, generalSuper)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
	
		Set oCmd = nothing
	'	rs.close
	'	set rs = nothing
	
		'edit the accident report	
		'specific items to this report
		propertyOwnerName = request("propertyOwnerName")
		propertyOwnerAddress = request("propertyOwnerAddress")
		propertyOwnerPhone = request("propertyOwnerPhone")
		incidentDate = request("incidentDate")
		timeofIncident = request("timeofIncident")
		location = request("location")
		incidentDescription = request("incidentDescription")
		jobNumber = null 'removed
		damagedPropertyDescription = request("damagedPropertyDescription")
		crewPresent = request("crewPresent")
		officialsPresent = request("officialsPresent")
		otherWitnesses = request("otherWitnesses")
		sufficientDepthHeight = request("sufficientDepthHeight")
		utilitiesShown = request("utilitiesShown")
		utilityInformed = request("utilityInformed")
		byWhom = null'request("byWhom")
		locateTicketNumber = request("locateTicketNumber")
		describeConversation = request("describeConversation")
		describeDamageAndDelays = request("describeDamageAndDelays")
		userID = null'request("userID")
		utilityMarked = request("utilityMarked")
		projectNumber = request("projectNumber")
		foreman = request("foreman")
		operator = request("operator")
		reportedBy = request("reportedBy")
		
		generalSuper = request("generalSuper")
		
		'add data to the accidentOtherReport Table
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "speditAccidentReportUtility"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .parameters.Append .CreateParameter("@propertyOwnerName", adVarChar, adParamInput, 50, propertyOwnerName)
		   .parameters.Append .CreateParameter("@propertyOwnerAddress", adVarChar, adParamInput, 100, propertyOwnerAddress)
		   .parameters.Append .CreateParameter("@propertyOwnerPhone", adVarChar, adParamInput, 20, propertyOwnerPhone)
		   .parameters.Append .CreateParameter("@incidentDate", adDBTimeStamp, adParamInput, 8, incidentDate)
		   .parameters.Append .CreateParameter("@timeofIncident", adVarChar, adParamInput, 50, timeofIncident)
		   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 200, location)
		   .parameters.Append .CreateParameter("@incidentDescription", adVarChar, adParamInput, 8000, incidentDescription)
		   .parameters.Append .CreateParameter("@jobNumber", adVarChar, adParamInput, 50, jobNumber)
		   .parameters.Append .CreateParameter("@damagedPropertyDescription", adVarChar, adParamInput, 8000, damagedPropertyDescription)
		   .parameters.Append .CreateParameter("@crewPresent", adVarChar, adParamInput, 100, crewPresent)
		   .parameters.Append .CreateParameter("@officialsPresent", adVarChar, adParamInput, 100, officialsPresent)
		   .parameters.Append .CreateParameter("@otherWitnesses", adVarChar, adParamInput, 100, otherWitnesses)
		   .parameters.Append .CreateParameter("@sufficientDepthHeight", adVarChar, adParamInput, 3, sufficientDepthHeight)
		   .parameters.Append .CreateParameter("@utilitiesShown", adVarChar, adParamInput, 3, utilitiesShown)
		   .parameters.Append .CreateParameter("@utilityInformed", adVarChar, adParamInput, 3, utilityInformed)
		   .parameters.Append .CreateParameter("@byWhom", adVarChar, adParamInput, 100, byWhom)
		   .parameters.Append .CreateParameter("@locateTicketNumber", adVarChar, adParamInput, 100, locateTicketNumber)
		   .parameters.Append .CreateParameter("@describeConversation", adVarChar, adParamInput, 8000, describeConversation)
		   .parameters.Append .CreateParameter("@describeDamageAndDelays", adVarChar, adParamInput, 8000, describeDamageAndDelays)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@utilityMarked", adVarChar, adParamInput, 3, utilityMarked)
		   .parameters.Append .CreateParameter("@projectNumber", adVarChar, adParamInput, 50, projectNumber)
		   .parameters.Append .CreateParameter("@foreman", adVarChar, adParamInput, 50, foreman)
		   .parameters.Append .CreateParameter("@operator", adVarChar, adParamInput, 50, operator)
		   .parameters.Append .CreateParameter("@reportedBy", adVarChar, adParamInput, 50, reportedBy)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
	
		Set oCmd = nothing
	'	rs.close
	'	set rs = nothing
		
		'build the PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildBuildAccidentUtilityPDF.asp?accidentID=" & accidentID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/accidentReport_" & accidentID & ".pdf"), true )
		
		'email the form to the appropriate party
		'sendRiskEmail accidentID,accidentTypeID,generalSuper
		
		'redirect back to the home page
		response.Redirect "main.asp"
		
	case "approveAccidentReport"
		clientID = request("clientID")
		accidentID = request("accidentID")
		userID = request("userID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spApproveAccidentReport"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'send email to the HR dept and the supervisors
		sendRiskEmailApprove accidentID,userID,clientID
		
		response.Redirect "accidentReportList.asp?clientID=" & clientID & "&msg=This accident report has been approved and sent to the appropriate recipients"
		
	case "denyAccidentReport"
		clientID = request("clientID")
		accidentID = request("accidentID")
		userID = request("userID")
		
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDenyAccidentReport"
		   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'send email to the HR to edit, re-route, etc.
		sendRiskEmailDeny accidentID,userID,clientID
		
		response.Redirect "accidentReportList.asp?clientID=" & clientID & "&msg=This accident report has been denied and sent to the appropriate recipients"
		
	

	case "addWeeklySummary"
	
		summary = request("summary")
		projectID= request("project")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddWeeklySummary"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
		   .parameters.Append .CreateParameter("@summary", adVarChar, adParamInput, 8000, summary)
		   .parameters.Append .CreateParameter("@dateEntered", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
	
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "weeklyProjSummary.asp?project=" & projectID

	case "addQuote"
		clientID = request("clientID")
		projectName = request("projectName")
		projectAddress = request("projectAddress")
		projectCity = request("projectCity")
		projectState = request("projectState")
		projectZip = request("projectZip")
		bidDate = request("bidDate")
		'invitationNumber = setZero(request("invitationNumber"))
		'reedConnect = setZero(request("reedConnect"))
		countyID = request("county")
		projectType = request("projectType")
		projectSize = request("projectSize")
		leadGeneratedBy = request("leadGeneratedBy")
		leadSource = request("leadSource")
		TFHourName = checknull(request("TFHourName"))
		TFHourPhone = checknull(request("TFHourPhone"))
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")

		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddQuote"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 100, projectName)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 50, projectCity)
		   .parameters.Append .CreateParameter("@projectState", adVarChar, adParamInput, 50, projectState)
		   .parameters.Append .CreateParameter("@projectZip", adVarChar, adParamInput, 50, projectZip)
		   .parameters.Append .CreateParameter("@isPending", adBoolean, adParamInput, 1, True)
		   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@createdBy", adVarChar, adParamInput, 50, Session("Name"))
		   .parameters.Append .CreateParameter("@dateCreated", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@bidDate", adDBTimeStamp, adParamInput, 8, bidDate)
		   '.parameters.Append .CreateParameter("@invitationNumber", adInteger, adParamInput, 8, invitationNumber)
		   '.parameters.Append .CreateParameter("@reedConnect", adInteger, adParamInput, 8, reedConnect)
		   .parameters.Append .CreateParameter("@countyID", adInteger, adParamInput, 8, countyID)
		   .parameters.Append .CreateParameter("@awarded", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@archived", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@plsGenerated", adBoolean, adParamInput, 1, False)		   
		   .parameters.Append .CreateParameter("@projectType", adVarChar, adParamInput, 50, projectType)
		   .parameters.Append .CreateParameter("@projectSize", adVarChar, adParamInput, 50, projectSize)
		   .parameters.Append .CreateParameter("@leadGeneratedBy", adVarChar, adParamInput, 50, leadGeneratedBy)
		   .parameters.Append .CreateParameter("@isNewQuote", adBoolean, adParamInput, 1, True)	
		   .parameters.Append .CreateParameter("@leadSource", adVarChar, adParamInput, 50, leadSource)
		   .parameters.Append .CreateParameter("@TFHourName", adVarChar, adParamInput, 50, TFHourName)
		   .parameters.Append .CreateParameter("@TFHourPhone", adVarChar, adParamInput, 50, TFHourPhone)	   
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		quoteID = rs("identity")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'redirect to the list where the user will choose the area managers to send an email to
		response.Redirect "areaManagerList.asp?clientID=" & clientID & "&quoteID=" & quoteID
		
	case "editQuote"
		quoteID = request("quoteID")
		clientID = request("clientID")
		projectName = request("projectName")
		projectAddress = request("projectAddress")
		projectCity = request("projectCity")
		projectState = request("projectState")
		projectZip = request("projectZip")
		bidDate = request("bidDate")
		countyID = request("county")
		projectType = request("projectType")
		projectSize = request("projectSize")
		leadGeneratedBy = request("leadGeneratedBy")
		leadSource = request("leadSource")
		TFHourName = checknull(request("TFHourName"))
		TFHourPhone = checknull(request("TFHourPhone"))
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")

		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditQuote"
		   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
		   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 100, projectName)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 50, projectCity)
		   .parameters.Append .CreateParameter("@projectState", adVarChar, adParamInput, 50, projectState)
		   .parameters.Append .CreateParameter("@projectZip", adVarChar, adParamInput, 50, projectZip)
		   .parameters.Append .CreateParameter("@bidDate", adDBTimeStamp, adParamInput, 8, bidDate)
		   .parameters.Append .CreateParameter("@countyID", adInteger, adParamInput, 8, countyID)		   
		   .parameters.Append .CreateParameter("@projectType", adVarChar, adParamInput, 50, projectType)
		   .parameters.Append .CreateParameter("@projectSize", adVarChar, adParamInput, 50, projectSize)
		   .parameters.Append .CreateParameter("@leadGeneratedBy", adVarChar, adParamInput, 50, leadGeneratedBy)
		   .parameters.Append .CreateParameter("@leadSource", adVarChar, adParamInput, 50, leadSource)
		   .parameters.Append .CreateParameter("@TFHourName", adVarChar, adParamInput, 50, TFHourName)
		   .parameters.Append .CreateParameter("@TFHourPhone", adVarChar, adParamInput, 50, TFHourPhone)	   
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing

		response.Redirect "quoteList.asp?clientID=" & clientID & "&init=true"
		
	case "addQuoteComment"
		clientID = request("clientID")
		quoteID = request("quoteID")
		comment = request("comment")
		addedBy = session("ID")
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddQuoteComment"
		   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
		   .parameters.Append .CreateParameter("@comment", adVarChar, adParamInput, 2000, comment)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@addedBy", adInteger, adParamInput, 8, addedBy)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "form.asp?formType=addQuoteComments&quoteID=" & quoteID & "&clientID=" & clientID
		
	case "awardQuote"
		clientID = request("clientID")
		quoteID = request("quoteID")
		awarded = request("awarded")
		wonContract = request("wonContract")
		lostTo = request("lostTo")
		percentageToClose = checkNull(request("percentageToClose"))
		jobStartDate = checkNull(request("jobStartDate"))
		
			
		'response.Write awarded
		'Open connection
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")

		if trim(wonContract) = "" then
		
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAwardCustomerQuote"
			   .parameters.Append .CreateParameter("@inspectionQuoteCustomerID", adInteger, adParamInput, 8, awarded)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rs = oCmd.Execute
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAwardQuote"
			   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rs = oCmd.Execute
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
		else 'update the quote to see if client won the contract. if lost, then who to.
		
			if wonContract = "" then
				wonContract = null
			end if
			
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spWonLostQoute"
			   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
			   .parameters.Append .CreateParameter("@wonContract", adBoolean, adParamInput, 1, wonContract)
			   .parameters.Append .CreateParameter("@lostTo", adVarChar, adParamInput, 100, lostTo)
			   .parameters.Append .CreateParameter("@percentageToClose", adInteger, adParamInput, 8, percentageToClose)
			   .parameters.Append .CreateParameter("@jobStartDate", adVarChar, adParamInput, 50, jobStartDate)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rs = oCmd.Execute
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
		end if
		
		response.Redirect "quoteCustomerList.asp?clientID=" & clientID & "&quoteID=" & quoteID
		
	case "editAssignCustomerQuote"
	
		customerQuoteID = request("inspectionQuoteCustomerID")
		customerID = request("customerID")
		clientID = request("clientID")
		quoteID = request("quoteID")
		
		address = request("address")
		city = request("city")
		state = request("state")
		zip = request("zip")
		contactName = request("contactName")
		contactPhone = request("contactPhone")
		contactCell = request("contactCell")
		contactFax = request("contactFax")
		contactEmail = request("contactEmail")
		quotePref = request("quotePref")		
		
		'new fields
		wprRate = setZero(request("wprRate"))
		dRate = setZero(request("dRate"))
		siteActivationFee = setZero(request("siteActivationFee"))
		waterSampRate = setZero(request("waterSampRate"))
		wRate = setZero(request("wRate"))
		wFreq = request("wFreq")
		prRate = setZero(request("prRate"))
		prFreq = request("prFreq")
		wprFreq = request("wprFreq")
		wprdRate = setZero(request("wprdRate"))
		wprdFreq = request("wprdFreq")
		dFreq = request("dFreq")
		bmpPerfAudit = null'setZero(request("bmpPerfAudit"))
		licSingle = null'setZero(request("licSingle"))
		licSingleFreq = null'request("licSingleFreq")
		licMultiple = null'setZero(request("licMultiple"))
		licMultipleFreq = null'request("licMultipleFreq")
		
		fourteenwprRate = setZero(request("fourteenwprRate"))
		fourteenwprFreq = request("fourteenwprFreq")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditCustomerQuote"
		   '.parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
		   .parameters.Append .CreateParameter("@customerQuoteID", adInteger, adParamInput, 8, customerQuoteID)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 100, address)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@contactName", adVarChar, adParamInput, 50, contactName)
		   .parameters.Append .CreateParameter("@contactPhone", adVarChar, adParamInput, 50, contactPhone)
		   .parameters.Append .CreateParameter("@contactCell", adVarChar, adParamInput, 50, contactCell)
		   .parameters.Append .CreateParameter("@contactFax", adVarChar, adParamInput, 50, contactFax)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 100, contactEmail)
		   .parameters.Append .CreateParameter("@quotePref", adVarChar, adParamInput, 50, quotePref)		   
		   .parameters.Append .CreateParameter("@createdBy", adVarChar, adParamInput, 50, Session("Name"))
		   .parameters.Append .CreateParameter("@dateCreated", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@awarded", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@wprRate", adCurrency, adParamInput, 8, wprRate)
		   .parameters.Append .CreateParameter("@dRate", adCurrency, adParamInput, 8, dRate)
		   .parameters.Append .CreateParameter("@siteActivationFee", adCurrency, adParamInput, 8, siteActivationFee)
		   .parameters.Append .CreateParameter("@waterSampRate", adCurrency, adParamInput, 8, waterSampRate)
		   .parameters.Append .CreateParameter("@wRate", adCurrency, adParamInput, 8, wRate)
		   .parameters.Append .CreateParameter("@wFreq", adVarChar, adParamInput, 10, wFreq)
		   .parameters.Append .CreateParameter("@prRate", adCurrency, adParamInput, 8, prRate)
		   .parameters.Append .CreateParameter("@prFreq", adVarChar, adParamInput, 10, prFreq)
		   .parameters.Append .CreateParameter("@wprFreq", adVarChar, adParamInput, 10, wprFreq)
		   .parameters.Append .CreateParameter("@wprdRate", adCurrency, adParamInput, 8, wprdRate)
		   .parameters.Append .CreateParameter("@wprdFreq", adVarChar, adParamInput, 10, wprdFreq)
		   .parameters.Append .CreateParameter("@dFreq", adVarChar, adParamInput, 10, dFreq)
		   .parameters.Append .CreateParameter("@bmpPerfAudit", adCurrency, adParamInput, 8, bmpPerfAudit)
		   .parameters.Append .CreateParameter("@licSingle", adCurrency, adParamInput, 8, licSingle)
		   .parameters.Append .CreateParameter("@licSingleFreq", adVarChar, adParamInput, 10, licSingleFreq)
		   .parameters.Append .CreateParameter("@licMultiple", adCurrency, adParamInput, 8, licMultiple)
		   .parameters.Append .CreateParameter("@licMultipleFreq", adVarChar, adParamInput, 10, licMultipleFreq)
		   .parameters.Append .CreateParameter("@fourteenwprRate", adCurrency, adParamInput, 8, fourteenwprRate)
		   .parameters.Append .CreateParameter("@fourteenwprFreq", adVarChar, adParamInput, 10, fourteenwprFreq)
		   .CommandType = adCmdStoredProc
		End With

		Set rs = oCmd.Execute
		
	'	customerQuoteID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		
		'create the quote PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildQuotePDF.asp?quoteID=" & quoteID & "&customerQuoteID=" & customerQuoteID & "&userID=" & Session("ID")', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/quote_" & customerQuoteID & ".pdf"), true )		
		set Pdf = nothing
		
		'create the printed version pdf
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "printQuote.asp?quoteID=" & quoteID & "&customerQuoteID=" & customerQuoteID & "&userID=" & Session("ID")', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/quotePrint_" & customerQuoteID & ".pdf"), true )		
		set Pdf = nothing

		response.Redirect "quoteCustomerList.asp?clientID=" & clientID & "&quoteID=" & quoteID
		
	case "assignCustomerQuote"
		customerID = request("customerID")
		clientID = request("clientID")
		quoteID = request("quoteID")
		address = request("address")
		city = request("city")
		state = request("state")
		zip = request("zip")
		contactName = request("contactName")
		contactPhone = request("contactPhone")
		contactCell = request("contactCell")
		contactFax = request("contactFax")
		contactEmail = request("contactEmail")
		quotePref = request("quotePref")		
		
		'new fields
		wprRate = setZero(request("wprRate"))
		dRate = setZero(request("dRate"))
		siteActivationFee = setZero(request("siteActivationFee"))
		waterSampRate = setZero(request("waterSampRate"))
		wRate = setZero(request("wRate"))
		wFreq = request("wFreq")
		prRate = setZero(request("prRate"))
		prFreq = request("prFreq")
		wprFreq = request("wprFreq")
		wprdRate = setZero(request("wprdRate"))
		wprdFreq = request("wprdFreq")
		dFreq = request("dFreq")
		bmpPerfAudit = null'setZero(request("bmpPerfAudit"))
		licSingle = null'setZero(request("licSingle"))
		licSingleFreq = null'request("licSingleFreq")
		licMultiple = null'setZero(request("licMultiple"))
		licMultipleFreq = null'request("licMultipleFreq")
		
		fourteenwprRate = setZero(request("fourteenwprRate"))
		fourteenwprFreq = request("fourteenwprFreq")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAssignCustomerQuote"
		   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 100, address)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, state)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@contactName", adVarChar, adParamInput, 50, contactName)
		   .parameters.Append .CreateParameter("@contactPhone", adVarChar, adParamInput, 50, contactPhone)
		   .parameters.Append .CreateParameter("@contactCell", adVarChar, adParamInput, 50, contactCell)
		   .parameters.Append .CreateParameter("@contactFax", adVarChar, adParamInput, 50, contactFax)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 100, contactEmail)
		   .parameters.Append .CreateParameter("@quotePref", adVarChar, adParamInput, 50, quotePref)		   
		   .parameters.Append .CreateParameter("@createdBy", adVarChar, adParamInput, 50, Session("Name"))
		   .parameters.Append .CreateParameter("@dateCreated", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@awarded", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@wprRate", adCurrency, adParamInput, 8, wprRate)
		   .parameters.Append .CreateParameter("@dRate", adCurrency, adParamInput, 8, dRate)
		   .parameters.Append .CreateParameter("@siteActivationFee", adCurrency, adParamInput, 8, siteActivationFee)
		   .parameters.Append .CreateParameter("@waterSampRate", adCurrency, adParamInput, 8, waterSampRate)
		   .parameters.Append .CreateParameter("@wRate", adCurrency, adParamInput, 8, wRate)
		   .parameters.Append .CreateParameter("@wFreq", adVarChar, adParamInput, 10, wFreq)
		   .parameters.Append .CreateParameter("@prRate", adCurrency, adParamInput, 8, prRate)
		   .parameters.Append .CreateParameter("@prFreq", adVarChar, adParamInput, 10, prFreq)
		   .parameters.Append .CreateParameter("@wprFreq", adVarChar, adParamInput, 10, wprFreq)
		   .parameters.Append .CreateParameter("@wprdRate", adCurrency, adParamInput, 8, wprdRate)
		   .parameters.Append .CreateParameter("@wprdFreq", adVarChar, adParamInput, 10, wprdFreq)
		   .parameters.Append .CreateParameter("@dFreq", adVarChar, adParamInput, 10, dFreq)
		   .parameters.Append .CreateParameter("@bmpPerfAudit", adCurrency, adParamInput, 8, bmpPerfAudit)
		   .parameters.Append .CreateParameter("@licSingle", adCurrency, adParamInput, 8, licSingle)
		   .parameters.Append .CreateParameter("@licSingleFreq", adVarChar, adParamInput, 10, licSingleFreq)
		   .parameters.Append .CreateParameter("@licMultiple", adCurrency, adParamInput, 8, licMultiple)
		   .parameters.Append .CreateParameter("@licMultipleFreq", adVarChar, adParamInput, 10, licMultipleFreq)
		   .parameters.Append .CreateParameter("@fourteenwprRate", adCurrency, adParamInput, 8, fourteenwprRate)
		   .parameters.Append .CreateParameter("@fourteenwprFreq", adVarChar, adParamInput, 10, fourteenwprFreq)
		   .CommandType = adCmdStoredProc
		End With

		Set rs = oCmd.Execute
		
		customerQuoteID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		
		'create the quote PDF
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildQuotePDF.asp?quoteID=" & quoteID & "&customerQuoteID=" & customerQuoteID & "&userID=" & Session("ID")', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/quote_" & customerQuoteID & ".pdf"), true )		
		set Pdf = nothing
		
		'create the printed version pdf
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "printQuote.asp?quoteID=" & quoteID & "&customerQuoteID=" & customerQuoteID & "&userID=" & Session("ID")', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/quotePrint_" & customerQuoteID & ".pdf"), true )		
		set Pdf = nothing

		response.Redirect "quoteCustomerList.asp?clientID=" & clientID & "&quoteID=" & quoteID

	Case "addEquipReport"
		reportType = request("reportType")
		projectID = request("job") 'projectID
		imonth = request("month")
		iyear = request("year")
		serialNumber = request("serialNumber")
		userID = request("operator") 'userID
		makeModel = request("makeModel")
		address = request("address")
		
		'insert this into the equipReport table and return the reportID
	'	response.Write "reportType " & reportType & "<br>"
	'	response.Write "job " & projectID & "<br>"
	'	response.Write "imonth " & imonth & "<br>"
	'	response.Write "iyear " & iyear & "<br>"
	'	response.Write "serialNumber " & serialNumber & "<br>"
	'	response.Write "operator " & userID  & "<br>"
	'	response.Write "makeModel " & makeModel & "<br>"
	'	response.Write "address " & address & "<br>"
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddEquipReport"
		   .parameters.Append .CreateParameter("@reportType", adInteger, adParamInput, 8, reportType)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@imonth", adInteger, adParamInput, 8, imonth)
		   .parameters.Append .CreateParameter("@iyear", adInteger, adParamInput, 8, iyear)
		   .parameters.Append .CreateParameter("@serialNumber", adVarChar, adParamInput, 100, serialNumber)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@makeModel", adVarChar, adParamInput, 100, makeModel)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 100, address)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		'get the report id that we just created
		reportID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'get the daily answers
		For Each Item in Request.Form
			if Left(item,5) = "daily" then
				'response.Write item & "<br>"
				'get the id out of the form fiels
				sString = replace(item,"daily_","")
				iLen = instr(sString,"_")
				iLen2 = len(sString)
				iLen3 = iLen2 - iLen
				iday = right(sString, iLen3)
				quesID = left(sString,iLen - 1)
				iWeek = null
				'response.Write iLen & "<br>"
				'quesID = replace(quesID,i & "_","")
				
				'response.Write "day " & iday & "<br>" '"question " & quesID & "<br>"
				'response.Write "quesID " & quesID & "<br><br>"
				
				'insert this into db				
				'Create command
				Set oCmd = Server.CreateObject("ADODB.Command")
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spAddEquipReportAnswers"
				   .parameters.Append .CreateParameter("@equipReportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@equipQuestionID", adInteger, adParamInput, 8, quesID)
				   .parameters.Append .CreateParameter("@iDay", adInteger, adParamInput, 8, iday)
				   .parameters.Append .CreateParameter("@iWeek", adInteger, adParamInput, 8, iWeek)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rs = oCmd.Execute

			end if
		next
		
		
		response.Write "weekly<br><br><br>"
		'get the weekly answers
		For Each Item in Request.Form
			if Left(item,6) = "weekly" then
				'response.Write item & "<br>"
				'get the id out of the form fiels
				sString = replace(item,"weekly_","")
				iLen = instr(sString,"_")
				iLen2 = len(sString)
				iLen3 = iLen2 - iLen
				quesID = right(sString, iLen3)
				iWeek = left(sString,iLen - 1)
				iDay = null
				'response.Write iLen & "<br>"
				'response.Write iLen2 & "<br>"
				'quesID = replace(quesID,i & "_","")
				
				'response.Write "week " & iWeek & "<br>" '"question " & quesID & "<br>"
				'response.Write "quesID " & quesID & "<br><br>"
				
				'insert this into db
				'insert this into db				
				'Create command
				Set oCmd = Server.CreateObject("ADODB.Command")
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spAddEquipReportAnswers"
				   .parameters.Append .CreateParameter("@equipReportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@equipQuestionID", adInteger, adParamInput, 8, quesID)
				   .parameters.Append .CreateParameter("@iDay", adInteger, adParamInput, 8, iday)
				   .parameters.Append .CreateParameter("@iWeek", adInteger, adParamInput, 8, iWeek)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rs = oCmd.Execute

			end if
		next
		
		response.Write "monthly<br><br><br>"
		'get the monthly answers
		For Each Item in Request.Form
			if Left(item,7) = "monthly" then
				'response.Write item & "<br>"
				'get the id out of the form fiels
				sString = replace(item,"monthly_","")
				iLen = instr(sString,"_")
				iLen2 = len(sString)
				iLen3 = iLen2 - iLen
				quesID = right(sString, iLen3)
				iWeek null
				iDay = null
				'response.Write iLen & "<br>"
				'response.Write iLen2 & "<br>"
				'quesID = replace(quesID,i & "_","")
				
		'		response.Write "week " & iWeek & "<br>" '"question " & quesID & "<br>"
		'		response.Write "quesID " & quesID & "<br><br>"
				
				Set oCmd = Server.CreateObject("ADODB.Command")
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spAddEquipReportAnswers"
				   .parameters.Append .CreateParameter("@equipReportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@equipQuestionID", adInteger, adParamInput, 8, quesID)
				   .parameters.Append .CreateParameter("@iDay", adInteger, adParamInput, 8, iday)
				   .parameters.Append .CreateParameter("@iWeek", adInteger, adParamInput, 8, iWeek)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rs = oCmd.Execute

			end if
		next
		

	case "addSiteNews"
	
		headline = request("headline")
		story = request("story")
		isPublished = request("isPublished")
		isPublic = request("isPublic")		
		clientID = request("clientID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddSiteNews"
		   .parameters.Append .CreateParameter("@headline", adVarChar, adParamInput, 100, headline)
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@story", adVarChar, adParamInput, 5000, story)
		   .parameters.Append .CreateParameter("@addedBy", adVarChar, adParamInput, 100, Session("Name"))
		   .parameters.Append .CreateParameter("@isPublished", adBoolean, adParamInput, 1, isOn(isPublished))
		   .parameters.Append .CreateParameter("@isPublic", adBoolean, adParamInput, 1, isOn(isPublic))
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "newsList.asp?clientID=" & clientID
		
	case "editSiteNews"
	
		siteNewsID = request("siteNewsID")
		headline = request("headline")
		story = request("story")
		isPublished = request("isPublished")
		isPublic = request("isPublic")		
		clientID = request("clientID")

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditSiteNews"
		   .parameters.Append .CreateParameter("@siteNewsID", adInteger, adParamInput, 8, siteNewsID)
		   .parameters.Append .CreateParameter("@headline", adVarChar, adParamInput, 100, headline)
		   .parameters.Append .CreateParameter("@story", adVarChar, adParamInput, 5000, story)
		   .parameters.Append .CreateParameter("@isPublished", adBoolean, adParamInput, 1, isOn(isPublished))
		   .parameters.Append .CreateParameter("@isPublic", adBoolean, adParamInput, 1, isOn(isPublic))
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "newsList.asp?clientID=" & clientID
		
	case "deleteSiteNewsItem"
		siteNewsID = Request("id")
		clientID = request("clientID")
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")		
		
		'delete the workOrder items
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteSiteNewsItem"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, siteNewsID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "newsList.asp?clientID=" & clientID

	case "deleteWorkOrder"
		workOrderID = Request("id")
		projectID = request("projectID")
		clientID = request("clientID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'delete the workOrder items
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteWorkOrderItems"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, workOrderID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		'delete the workOrder
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteWorkOrder"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, workOrderID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "workOrdersList.asp?projectID=" & projectID & "&clientID=" & clientID
		
	Case "approveWorkOrderItems"
		'approve the individual items and create the PDF
		'get the array with all of the checkbox id's
		arrAll = split(Request.Form ("allBoxes"), ",")
		sSend = False
		
		'get the array with the id's that were ticked
		arrVals = split(Request.Form ("approveWorkOrderItem"), ",")
		'arrVals2 = split(Request.Form ("approveWorkOrder"), ",")
		
		'addressedBy = session("Name")
		projectID = request("projectID")
		clientID = request("clientID")
		workOrderID = request("workOrderID")
		'questionID = request("questionID")
		
		'response.Write projectID & "<br>"
		'response.Write clientID & "<br>"
		'response.Write workOrderID & "<br><br>"
		
		On Error Resume Next
			
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")

		'approve items that have been checked
		for i = 0 to ubound(arrVals)
		
			'response.Write "itemID: " & arrVals(i) & "<br>"
			'response.Write "approvedBy: " & request("approvedBy" & trim(arrVals(i))) & "<br>"
			'response.Write "date approved: " & request("dateApproved" & trim(arrVals(i))) & "<br>"
			'response.Write "customerComments: " & request("customerComments" & trim(arrVals(i))) & "<br><br>"
		
	
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spApproveWorkOrderItem"
			   .parameters.Append .CreateParameter("@workOrderItemID", adInteger, adParamInput, 8, arrVals(i))
			   .parameters.Append .CreateParameter("@approvedBy", adVarChar, adParamInput, 100, request("approvedBy" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@dateApproved", adDBTimeStamp, adParamInput, 8, request("dateApproved" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, true)
			   .parameters.Append .CreateParameter("@customerComments", adVarChar, adParamInput, 500, request("customerComments" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@activityCode", adVarChar, adParamInput, 100, request("activityCode" & trim(arrVals(i))))
			   .CommandType = adCmdStoredProc
			End With
			
			Set rs = oCmd.Execute
			Set oCmd = nothing
			'rs.close
			'set rs = nothing
			
			'set the items
			sApprovedBy = request("approvedBy" & trim(arrVals(i)))
			dtDateApproved = request("dateApproved" & trim(arrVals(i)))
			'sApprovalComments = ""
		
		next
		
		'approve the work order
		'response.Write "workOrderID: " & workOrderID & "<br>"
		'response.Write "approvedBy: " & sApprovedBy & "<br>"
		'response.Write "dateApproved: " & dtDateApproved & "<br>"
		
		Set oCmd2 = Server.CreateObject("ADODB.Command")
		With oCmd2
		   .ActiveConnection = DataConn
		   .CommandText = "spApproveWorkOrder"
		   .parameters.Append .CreateParameter("@workOrderID", adInteger, adParamInput, 8, workOrderID)
		   .parameters.Append .CreateParameter("@approvedBy", adVarChar, adParamInput, 100, sApprovedBy)
		   .parameters.Append .CreateParameter("@dateApproved", adDBTimeStamp, adParamInput, 8, dtDateApproved)
		   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, true)
		   '.parameters.Append .CreateParameter("@approvalComments", adVarChar, adParamInput, 250, sApprovalComments)
		   .CommandType = adCmdStoredProc
		End With
	
		Set rs2 = oCmd2.Execute
		Set oCmd2 = nothing
		'rs2.close
		'set rs2 = nothing
	
		
		'create the PDF for the work order
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildWorkOrderPDF.asp?workOrderID=" & workOrderID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/workOrder_" & workOrderID & ".pdf"), true )		
		set Pdf = nothing
		
		
		'send auto email if true for project
		If autoEmail(projectID) = true then
			sendAutoEmailWO workOrderID,projectID,"True" 'workorderID
		end if

		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		

		response.Redirect "workOrdersList.asp?projectID=" & projectID & "&clientID=" & clientID
		
	Case "processWorkOrders"
	
		'get the array with all of the checkbox id's
		arrAll = split(Request.Form ("allBoxes"), ",")
		
		'get the array with the id's that were ticked
		arrVals = split(Request.Form ("processWorkOrder"), ",")
		
		projectID = request("projectID")
		clientID = request("clientID")
		
		On Error Resume Next
			
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'close items that have been approved
		for i = 0 to ubound(arrVals)
		
			'response.Write "process item<br>"
			'response.Write "work order id " & arrVals(i) & "<br>"
			'response.Write "date " & request("dateProcessed" & trim(arrVals(i))) & "<br>"
			'response.Write "init " & request("processedBy" & trim(arrVals(i))) & "<br><br>"

		
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spProcessWorkOrder"
			   .parameters.Append .CreateParameter("@workOrderID", adInteger, adParamInput, 8, arrVals(i))
			   .parameters.Append .CreateParameter("@processedBy", adVarChar, adParamInput, 100, request("processedBy" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@dateProcessed", adDBTimeStamp, adParamInput, 8, request("dateProcessed" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@isProcessed", adBoolean, adParamInput, 1, true)
			   .CommandType = adCmdStoredProc
			End With
			
	
			Set rs = oCmd.Execute
			Set oCmd = nothing
			rs.close
			set rs = nothing
	
		next

		response.Redirect "closedWorkOrdersList.asp?projectID=" & projectID & "&clientID=" & clientID

	Case "approveCloseWorkOrders"
		
		'if approving the work order, create the PDF
	
		'get the array with all of the checkbox id's
		arrAll = split(Request.Form ("allBoxes"), ",")
		sSend = False
		
		'get the array with the id's that were ticked
		arrVals = split(Request.Form ("closeWorkOrder"), ",")
	'	arrVals2 = split(Request.Form ("approveWorkOrder"), ",")
		
		'addressedBy = session("Name")
		projectID = request("projectID")
		clientID = request("clientID")
		'questionID = request("questionID")
		
		On Error Resume Next
			
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'close items that have been approved
		for i = 0 to ubound(arrVals)
		
			'response.Write "close item<br>"
			'response.Write "ref# " & arrVals(i) & "<br>"
			'response.Write "date " & request("addressedByDate" & trim(arrVals(i))) & "<br>"
			'response.Write "init " & request("addressedBy" & trim(arrVals(i))) & "<br><br>"
			
			'update the responsiveAction table and close the open item
			'Open connection and insert into the database
			
		
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spCloseWorkOrder"
			   .parameters.Append .CreateParameter("@workOrderID", adInteger, adParamInput, 8, arrVals(i))
			   .parameters.Append .CreateParameter("@closedBy", adVarChar, adParamInput, 100, request("closedBy" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@dateClosed", adDBTimeStamp, adParamInput, 8, request("dateClosed" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, true)
			   .CommandType = adCmdStoredProc
			End With
			
	
			Set rs = oCmd.Execute
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
			'close the item in the responsiveaction table
			'get the responsive action ids from the workorderitems table based on the work order id			
			'Create command
			Set oCmd3 = Server.CreateObject("ADODB.Command")
				
			With oCmd3
			   .ActiveConnection = DataConn
			   .CommandText = "spGetApprovedWorkOrderItems"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, arrVals(i))
			   .CommandType = adCmdStoredProc   
			End With
						
			Set rsWO = oCmd3.Execute
			Set oCmd3 = nothing
			
			'close the responsive items out
			do until rsWO.eof
				
				Set oCmd4 = Server.CreateObject("ADODB.Command")
			
				With oCmd4
				   .ActiveConnection = DataConn
				   .CommandText = "spCloseItem"
				   .parameters.Append .CreateParameter("@responsiveActionID", adInteger, adParamInput, 8, rsWO("responsiveActionID"))
				   .parameters.Append .CreateParameter("@addressedBy", adVarChar, adParamInput, 100, request("closedBy" & trim(arrVals(i))))
				   .parameters.Append .CreateParameter("@addressedByDate", adDBTimeStamp, adParamInput, 8, request("dateClosed" & trim(arrVals(i))))
				   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, true)
				   .parameters.Append .CreateParameter("@closeComments", adVarChar, adParamInput, 200, null)
				   .CommandType = adCmdStoredProc
				End With
					
				Set rsCI = oCmd4.Execute

				
			rsWO.movenext
			loop
			
		next

		
		'approve items that have been checked
'		for i = 0 to ubound(arrVals2)
		
			'response.Write "approve item<br>"
			'response.Write "workOrderID " & arrVals2(i) & "<br>"
			'response.Write "approvedBy " & request("approvedBy" & trim(arrVals2(i))) & "<br>"
			'response.Write "dateApproved " & request("dateApproved" & trim(arrVals2(i))) & "<br><br>"
	
			'Create command
'			Set oCmd2 = Server.CreateObject("ADODB.Command")
'			With oCmd2
'			   .ActiveConnection = DataConn
'			   .CommandText = "spApproveWorkOrder"
'			   .parameters.Append .CreateParameter("@workOrderID", adInteger, adParamInput, 8, arrVals2(i))
'			   .parameters.Append .CreateParameter("@approvedBy", adVarChar, adParamInput, 100, request("approvedBy" & trim(arrVals2(i))))
'			   .parameters.Append .CreateParameter("@dateApproved", adDBTimeStamp, adParamInput, 8, request("dateApproved" & trim(arrVals2(i))))
'			   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, true)
			   '.parameters.Append .CreateParameter("@approvalComments", adVarChar, adParamInput, 250, approvalComments)
'			   .CommandType = adCmdStoredProc
'			End With
			
			
	
'			Set rs2 = oCmd2.Execute
'			Set oCmd2 = nothing
'			rs2.close
'			set rs2 = nothing
			
			'customerComments = ""
			
			'for each item in the work order, approve all of the items
'			Set oCmd2 = Server.CreateObject("ADODB.Command")
'			With oCmd2
'			   .ActiveConnection = DataConn
'			   .CommandText = "spApproveWorkOrderItems"
'			   .parameters.Append .CreateParameter("@workOrderID", adInteger, adParamInput, 8, arrVals2(i))
'			   .parameters.Append .CreateParameter("@approvedBy", adVarChar, adParamInput, 100, request("approvedBy" & trim(arrVals2(i))))
'			   .parameters.Append .CreateParameter("@dateApproved", adDBTimeStamp, adParamInput, 8, request("dateApproved" & trim(arrVals2(i))))
'			   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, true)
			   '.parameters.Append .CreateParameter("@customerComments", adVarChar, adParamInput, 500, customerComments)
'			   .CommandType = adCmdStoredProc
'			End With
			
			
	
'			Set rs2 = oCmd2.Execute
'			Set oCmd2 = nothing
'			rs2.close
'			set rs2 = nothing
			
			
	'		sSend = True
			
			'create the PDF for the work order
'			Set Pdf = Server.CreateObject("Persits.Pdf")
'			Pdf.RegKey = sRegKey
'			Set Doc = Pdf.CreateDocument
'			Doc.ImportFromUrl sPDFPath & "buildWorkOrderPDF.asp?workOrderID=" & arrVals2(i)', "landscape=true"
'			Filename = Doc.Save( Server.MapPath("downloads/workOrder_" & arrVals2(i) & ".pdf"), true )		
'			set Pdf = nothing
			
			
			'send auto email if true for project
'			If autoEmail(projectID) = true then
'				sendAutoEmailWO arrVals2(i),projectID,"True" 'workorderID
'			end if
			
'		next
		
		

		response.Redirect "workOrdersList.asp?projectID=" & projectID & "&clientID=" & clientID

	Case "addWorkOrder"

		projectID = request("projectID")
		clientID = request("clientID")
		generalComments = request("generalComments")
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")
			
		'write the dat to the work order table and return the workorderID
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddWorkOrder"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
		   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, false)
		   .parameters.Append .CreateParameter("@generalComments", adVarChar, adParamInput, 500, generalComments)
		   .parameters.Append .CreateParameter("@isProcessed", adBoolean, adParamInput, 1, false)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		'get the report id that we just created
		workOrderID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing

		
		'add the action for the report
		'if they have "item" in the name
		For Each Item in Request.Form
			if Left(item,5) = "item&" then
				sText = replace(Item,"item&","")'item'
				If Left(sText,8) = "material" then
					
					iLen1 = len(sText)
					iLen2 = InStr(sText, "_")
					iLen3 = len(Right(sText,iLen1 - iLen2))	
					responsiveActionID = Right(sText,iLen3)
					
					sText2 = Replace(sText,"material", "")
					iLen4 = len(sText2)
					iLen5 = InStr(sText2, "_") - 1
					iLen6 = iLen4 + iLen5
					itemnum = left(sText2,iLen6 - iLen4)
					
					iAmt = request("item&amount" & itemnum & "_" & responsiveActionID)
					
					if iAmt = "" then
						iAmt = 0
					end if
					
					
				'	response.Write responsiveActionID & "<br>"
				'	response.Write request("item&material" & itemnum & "_" & responsiveActionID) & "<br>"
				'	response.Write iAmt & "<br>"
				'	response.Write request("item&comments" & itemnum & "_" & responsiveActionID) & "<br>"
				'	response.Write isOn(request("item&warrantyNoCharge" & itemnum & "_" & responsiveActionID)) & "<br>"
					
					'insert the items into the report items table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddWorkOrderItem"
					   .parameters.Append .CreateParameter("@workOrderID", adInteger, adParamInput, 8, workOrderID)
					   .parameters.Append .CreateParameter("@responsiveActionID", adInteger, adParamInput, 8, responsiveActionID)
					   .parameters.Append .CreateParameter("@materialID", adInteger, adParamInput, 8, request("item&material" & itemnum & "_" & responsiveActionID))
					   .parameters.Append .CreateParameter("@quantity", adDouble, adParamInput, 8, iAmt)
					   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 500, request("item&comments" & itemnum & "_" & responsiveActionID))
					   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
					   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, false)
					   .parameters.Append .CreateParameter("@warrantyNoCharge", adBoolean, adParamInput, 1, isOn(request("item&warrantyNoCharge" & itemnum & "_" & responsiveActionID)))
					   .CommandType = adCmdStoredProc
					End With
					
					Set rs = oCmd2.Execute
					
				end if				
			end if
			
		next


		'add the general items
		For Each Item in Request.Form
			if Left(item,6) = "gItem&" then
				sText = replace(Item,"gItem&","")'item'
				If Left(sText,8) = "material" then
					sText = replace(sText,"material","")
					itemnum = sText
				'	response.Write itemnum & "<br>"
				'	response.Write "material " & request("gItem&material" & itemNum) & "<br>"
				'	response.Write "amount " & request("gItem&amount" & itemNum) & "<br>"
				'	response.Write "comments " & request("gItem&comments" & itemNum) & "<br><br>"
					
					'insert the items into the report items table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddWorkOrderItem"
					   .parameters.Append .CreateParameter("@workOrderID", adInteger, adParamInput, 8, workOrderID)
					   .parameters.Append .CreateParameter("@responsiveActionID", adInteger, adParamInput, 8, 0)
					   .parameters.Append .CreateParameter("@materialID", adInteger, adParamInput, 8, request("gItem&material" & itemNum))
					   .parameters.Append .CreateParameter("@quantity", adDouble, adParamInput, 8, request("gItem&amount" & itemNum))
					   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 500, request("gItem&comments" & itemNum))
					   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
					   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, false)
					   .parameters.Append .CreateParameter("@warrantyNoCharge", adBoolean, adParamInput, 1, isOn(request("gItem&warrantyNoCharge" & itemNum)))
					   .CommandType = adCmdStoredProc
					End With
					
					Set rs = oCmd2.Execute
					
				end if				
			end if
			
		next
		
		If Err Then%>
				<!--#include file="includes/FatalError.inc"-->
		<%End If
		
		'don't create the PDF until it has been approved
		'send an email to say the work ordeer has been created
		If autoEmail(projectID) = true then
			sendAutoEmailWO workOrderID,projectID,"False" 'workorderID
		end if

		response.Redirect "workOrdersList.asp?projectID=" & projectID & "&clientID=" & clientID

	case "addMaterial"
		clientID = request("clientID")
		material = request("material")
		unit = request("unit")
		cost = request("cost")
		clientCost = request("clientCost")
		
		if cost = "" then
			cost = null
		end if
		
		if clientCost = "" then
			clientCost = null
		end if


		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddMaterial"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@material", adVarChar, adParamInput, 100, material)
		   .parameters.Append .CreateParameter("@unit", adVarChar, adParamInput, 50, unit)
		   .parameters.Append .CreateParameter("@cost", adCurrency, adParamInput, 8, cost)
		   .parameters.Append .CreateParameter("@clientCost", adCurrency, adParamInput, 8, clientCost)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "materialsList.asp?clientID=" & clientID
		
	case "editMaterial"
		materialID = request("materialID")
		clientID = request("clientID")
		material = request("material")
		unit = request("unit")
		cost = request("cost")
		clientCost = request("clientCost")
		
		if cost = "" then
			cost = null
		end if
		
		if clientCost = "" then
			clientCost = null
		end if

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditMaterial"
		   .parameters.Append .CreateParameter("@materialID", adInteger, adParamInput, 8, materialID)
		   .parameters.Append .CreateParameter("@material", adVarChar, adParamInput, 100, material)
		   .parameters.Append .CreateParameter("@unit", adVarChar, adParamInput, 50, unit)
		   .parameters.Append .CreateParameter("@cost", adCurrency, adParamInput, 8, cost)
		   .parameters.Append .CreateParameter("@clientCost", adCurrency, adParamInput, 8, clientCost)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "materialsList.asp?clientID=" & clientID
		
	Case "editPetroleum"
	
		'report header
		reportID = request("reportID")
		comments = request("comments")
		division = request("division")
				
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditDaily"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing

		'add the monthly report data here
		For Each Item in Request.Form
			if Left(item,6) = "answer" then
				
				'get the questionID
				iQuestionID = Replace(item,"answer", "")
				sAnswer = (Request.Form(Item))
					
				'response.Write iQuestionID & "<br>"
				'response.Write sAnswer & "<br>"
				
				'response.Write Left(item,9) & "<br>"
				
				'Create command
				Set oCmd = Server.CreateObject("ADODB.Command")
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spEditPetroleumReport"
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, iQuestionID)
				   .parameters.Append .CreateParameter("@answer", adVarChar, adParamInput, 150, sAnswer)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rs = oCmd.Execute
				
				Set oCmd = nothing
				rs.close
				set rs = nothing
			end if	
		next
		
		For Each Item in Request.Form
			if Left(item,6) = "remark" then
				iQuestionID = Replace(item,"remark", "")
				sRemark = (Request.Form(Item))
					
				'response.Write iQuestionID & "<br>"
				'response.Write sRemark & "<br>"
				'Create command
				If sRemark <> "" then
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddPetroleumRemark"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, iQuestionID)
					   .parameters.Append .CreateParameter("@remark", adVarChar, adParamInput, 500, sRemark)
					   .CommandType = adCmdStoredProc
					End With
							
					Set rs = oCmd2.Execute
					
					Set oCmd2 = nothing
					rs.close
					set rs = nothing
				end if
			end if
		next
		
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Edited","Report and PDF document Edited"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPetroleumPDF.asp?divisionID=" & division & "&reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "addPetroleum"
	
		'report header
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = 1'don't need weather type for monthly report request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		
		addToJournal = request("addToJournal")
		
		soilStabilizationComments = checknull(request("soilStabilizationComments"))
				
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType)
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, 0)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing

		'add the monthly report data here
		For Each Item in Request.Form
			if Left(item,6) = "answer" then
				
				'get the questionID
				iQuestionID = Replace(item,"answer", "")
				sAnswer = (Request.Form(Item))
					
				'response.Write iQuestionID & "<br>"
				'response.Write sAnswer & "<br>"
				
				'response.Write Left(item,9) & "<br>"
				
				'Create command
				Set oCmd = Server.CreateObject("ADODB.Command")
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spAddPetroleumReport"
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, iQuestionID)
				   .parameters.Append .CreateParameter("@answer", adVarChar, adParamInput, 150, sAnswer)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rs = oCmd.Execute
				
				Set oCmd = nothing
				rs.close
				set rs = nothing
			end if	
		next
		
		For Each Item in Request.Form
			if Left(item,6) = "remark" then
				iQuestionID = Replace(item,"remark", "")
				sRemark = (Request.Form(Item))
					
				'response.Write iQuestionID & "<br>"
				'response.Write sRemark & "<br>"
				'Create command
				If sRemark <> "" then
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddPetroleumRemark"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, iQuestionID)
					   .parameters.Append .CreateParameter("@remark", adVarChar, adParamInput, 500, sRemark)
					   .CommandType = adCmdStoredProc
					End With
							
					Set rs = oCmd2.Execute
					
					Set oCmd2 = nothing
					rs.close
					set rs = nothing
				end if
			end if
		next
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
	
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPetroleumPDF.asp?divisionID=" & division & "&reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if
		
		response.Redirect "reportList.asp?findReport=" & reportID

	Case "addMonthly"
	
		'report header
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = null'request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = null'don't need weather type for monthly report request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		soilStabilizationComments = null'checknull(request("soilStabilizationComments"))
		addToJournal = request("addToJournal")
		
		'check to see if initial report has been done on this project
		'checkInitialInspection project


		LDADate = checknull(request("LDADate"))
		
	
		updateLDADate project,LDADate

				
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, null)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType)
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, 0)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		'get the report id that we just created
		reportID = rs("Identity")
		
		Set oCmd = nothing

		'add the data to the monthly rain report table
		
		tlNumConstructionDays = checknull(request("tlNumConstructionDays"))
		tlDailyReportsAllDays = checknull(request("tlDailyReportsAllDays"))
		ssNumConstructionDays = null'checknull(request("ssNumConstructionDays"))
		ssDailyReportsAllDays = null'checknull(request("ssDailyReportsAllDays"))
		complianceGTCPolicy = checknull(request("complianceGTCPolicy"))
		complianceGTCPolicyWhyNot = checknull(request("complianceGTCPolicyWhyNot"))
		inspectionOccurEvery14Days = checknull(request("inspectionOccurEvery14Days"))
		inspectionOccurEvery14DaysWhyNot = checknull(request("inspectionOccurEvery14DaysWhyNot"))
		inspectionOccur24HourRain = checknull(request("inspectionOccur24HourRain"))
		inspectionOccur24HourRainWhyNot = checknull(request("inspectionOccur24HourRainWhyNot"))
		bmpComplianceGTCPolicy = checknull(request("bmpComplianceGTCPolicy"))
		bmpComplianceGTCPolicyWhyNot = checknull(request("bmpComplianceGTCPolicyWhyNot"))		
		samplesCollectedInMonth = checknull(request("samplesCollectedInMonth"))
		exceedNTULimit = checknull(request("exceedNTULimit"))
		waterQualityMonitoringCompliance = checknull(request("waterQualityMonitoringCompliance"))
		waterQualityMonitoringComplianceWhyNot = checknull(request("waterQualityMonitoringComplianceWhyNot"))
		hobo = checknull(request("hobo"))
		dataTable = checknull(request("dataTable"))
		rainfallCollectedDaily = checknull(request("rainfallCollectedDaily"))
		BMPInstalledMaintRetrieved = checknull(request("BMPInstalledMaintRetrieved"))
		
		projectDrainUpstream = checknull(request("projectDrainUpstream"))
		projectDrainUpstreamExplain = checknull(request("projectDrainUpstreamExplain"))
		projectDrainUpstreamYesOutlined = checknull(request("projectDrainUpstreamYesOutlined"))
		
		siteStabilizationDate = checknull(request("siteStabilizationDate"))
		
		perimControlBMPInstallDate = checknull(request("perimControlBMPInstallDate"))
		initSedStorageInstallDate = checknull(request("initSedStorageInstallDate"))
		sedStorageBMPInstallDate = checknull(request("sedStorageBMPInstallDate"))
		
		monthlyInspectionOccur = checknull(request("monthlyInspectionOccur"))
		stabilizationAchieved = checknull(request("stabilizationAchieved"))
		explainStabilizationAchieved = checknull(request("explainStabilizationAchieved"))
		
		landDisturbanceWeekendHoliday = checknull(request("landDisturbanceWeekendHoliday"))
		landDisturbanceWeekendHolidayListDates = checknull(request("landDisturbanceWeekendHolidayListDates"))
		
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddMonthlyRainReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@tlNumConstructionDays", adVarChar, adParamInput, 50, tlNumConstructionDays)
		   .parameters.Append .CreateParameter("@tlDailyReportsAllDays", adVarChar, adParamInput, 50, tlDailyReportsAllDays)
		   .parameters.Append .CreateParameter("@ssNumConstructionDays", adVarChar, adParamInput, 50, ssNumConstructionDays)
		   .parameters.Append .CreateParameter("@ssDailyReportsAllDays", adVarChar, adParamInput, 50, ssDailyReportsAllDays)
		   .parameters.Append .CreateParameter("@complianceGTCPolicy", adVarChar, adParamInput, 50, complianceGTCPolicy)
		   .parameters.Append .CreateParameter("@complianceGTCPolicyWhyNot", adVarChar, adParamInput, 2500, complianceGTCPolicyWhyNot)
		   .parameters.Append .CreateParameter("@inspectionOccurEvery14Days", adVarChar, adParamInput, 50, inspectionOccurEvery14Days)
		   .parameters.Append .CreateParameter("@inspectionOccurEvery14DaysWhyNot", adVarChar, adParamInput, 2500, inspectionOccurEvery14DaysWhyNot)
		   .parameters.Append .CreateParameter("@inspectionOccur24HourRain", adVarChar, adParamInput, 50, inspectionOccur24HourRain)
		   .parameters.Append .CreateParameter("@inspectionOccur24HourRainWhyNot", adVarChar, adParamInput, 2500, inspectionOccur24HourRainWhyNot)
		   .parameters.Append .CreateParameter("@bmpComplianceGTCPolicy", adVarChar, adParamInput, 50, bmpComplianceGTCPolicy)
		   .parameters.Append .CreateParameter("@bmpComplianceGTCPolicyWhyNot", adVarChar, adParamInput, 2500, bmpComplianceGTCPolicyWhyNot)
		   .parameters.Append .CreateParameter("@samplesCollectedInMonth", adVarChar, adParamInput, 50, samplesCollectedInMonth)
		   .parameters.Append .CreateParameter("@exceedNTULimit", adVarChar, adParamInput, 50, exceedNTULimit)
		   .parameters.Append .CreateParameter("@waterQualityMonitoringCompliance", adVarChar, adParamInput, 50, waterQualityMonitoringCompliance)
		   .parameters.Append .CreateParameter("@waterQualityMonitoringComplianceWhyNot", adVarChar, adParamInput, 2500, waterQualityMonitoringComplianceWhyNot)
		   .parameters.Append .CreateParameter("@hobo", adVarChar, adParamInput, 50, hobo)
		   .parameters.Append .CreateParameter("@dataTable", adVarChar, adParamInput, 50, dataTable)
		   .parameters.Append .CreateParameter("@rainfallCollectedDaily", adVarChar, adParamInput, 50, rainfallCollectedDaily)
		   .parameters.Append .CreateParameter("@BMPInstalledMaintRetrieved", adVarChar, adParamInput, 50, BMPInstalledMaintRetrieved)
		   .parameters.Append .CreateParameter("@projectDrainUpstream", adVarChar, adParamInput, 50, projectDrainUpstream)
		   .parameters.Append .CreateParameter("@projectDrainUpstreamExplain", adVarChar, adParamInput, 2500, projectDrainUpstreamExplain)
		   .parameters.Append .CreateParameter("@projectDrainUpstreamYesOutlined", adVarChar, adParamInput, 50, projectDrainUpstreamYesOutlined)
		   .parameters.Append .CreateParameter("@siteStabilizationDate", adDBTimeStamp, adParamInput, 8, siteStabilizationDate)
		   .parameters.Append .CreateParameter("@perimControlBMPInstallDate", adVarChar, adParamInput, 50, perimControlBMPInstallDate)
		   .parameters.Append .CreateParameter("@initSedStorageInstallDate", adVarChar, adParamInput, 50, initSedStorageInstallDate)
		   .parameters.Append .CreateParameter("@sedStorageBMPInstallDate", adVarChar, adParamInput, 50, sedStorageBMPInstallDate)
		   .parameters.Append .CreateParameter("@monthlyInspectionOccur", adVarChar, adParamInput, 50, monthlyInspectionOccur)		   
		   .parameters.Append .CreateParameter("@stabilizationAchieved", adVarChar, adParamInput, 50, stabilizationAchieved)
		   .parameters.Append .CreateParameter("@explainStabilizationAchieved", adVarChar, adParamInput, 2500, explainStabilizationAchieved)
		   .parameters.Append .CreateParameter("@landDisturbanceWeekendHoliday", adVarChar, adParamInput, 50, landDisturbanceWeekendHoliday)
		   .parameters.Append .CreateParameter("@landDisturbanceWeekendHolidayListDates", adVarChar, adParamInput, 2500, landDisturbanceWeekendHolidayListDates)
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		'add the data to the water quality monitoring table
		For i = 1 to 10
		
		'response.Write i & "<br>"
		
			samplerID = checknull(request("samplerID" & i))
			GCSamplerID = null'checknull(request("GCSamplerID" & i))
			samplerType = null'checknull(request("samplerType" & i))
			dateEndClearingGrubbing = checknull(request("dateEndClearingGrubbing" & i))
			dateSamplerInstalled = null'checknull(request("dateSamplerInstalled" & i))
			dateSampleTakenAfterClearGrub = checknull(request("dateSampleTakenAfterClearGrub" & i))
			dateEndGrading = checknull(request("dateEndGrading" & i))
			dateAfterEndGradingorNinety = checknull(request("dateAfterEndGradingorNinety" & i))
			'BMPInstalledMaintRetrieved = checknull(request("BMPInstalledMaintRetrieved" & i))
			dateSamplerRemoved = null'checknull(request("dateSamplerRemoved" & i))
			rainDateEvent = checknull(request("rainDateEvent" & i))
			rainAmount = checknull(request("rainAmount" & i))
			codeCol1 = checknull(request("codeCol1" & i))
			codeCol2 = checknull(request("codeCol2" & i))
			codeCol3 = checknull(request("codeCol3" & i))
			codeCol4 = checknull(request("codeCol4" & i))
			codeCol5 = checknull(request("codeCol5" & i))
			codeCol6 = checknull(request("codeCol6" & i))
			codeCol7 = checknull(request("codeCol7" & i))
			codeCol8 = checknull(request("codeCol8" & i))
			codeCol9 = checknull(request("codeCol9" & i))
			codeCol10 = checknull(request("codeCol10" & i))

			samplingRequired = checknull(request("samplingRequired" & i))
			samplingComplete = checknull(request("samplingComplete" & i))
			
						
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddMonthlyRainData"
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
			   .parameters.Append .CreateParameter("@samplerID", adVarChar, adParamInput, 50, samplerID)
			   .parameters.Append .CreateParameter("@GCSamplerID", adVarChar, adParamInput, 50, GCSamplerID)
			   .parameters.Append .CreateParameter("@samplerType", adVarChar, adParamInput, 50, samplerType)
			   .parameters.Append .CreateParameter("@dateEndClearingGrubbing", adVarChar, adParamInput, 50, dateEndClearingGrubbing)			   
			   .parameters.Append .CreateParameter("@dateSamplerInstalled", adVarChar, adParamInput, 50, dateSamplerInstalled)
			   .parameters.Append .CreateParameter("@dateSampleTakenAfterClearGrub", adVarChar, adParamInput, 50, dateSampleTakenAfterClearGrub)
			   .parameters.Append .CreateParameter("@dateEndGrading", adVarChar, adParamInput, 50, dateEndGrading)
			   .parameters.Append .CreateParameter("@dateAfterEndGradingorNinety", adVarChar, adParamInput, 50, dateAfterEndGradingorNinety)
			   .parameters.Append .CreateParameter("@BMPInstalledMaintRetrieved", adVarChar, adParamInput, 50, null)
			   .parameters.Append .CreateParameter("@dateSamplerRemoved", adVarChar, adParamInput, 50, dateSamplerRemoved)
			   .parameters.Append .CreateParameter("@rainDateEvent", adVarChar, adParamInput, 50, rainDateEvent)
			   .parameters.Append .CreateParameter("@rainAmount", adVarChar, adParamInput, 50, rainAmount)
			   .parameters.Append .CreateParameter("@codeCol1", adVarChar, adParamInput, 50, codeCol1)
			   .parameters.Append .CreateParameter("@codeCol2", adVarChar, adParamInput, 50, codeCol2)
			   .parameters.Append .CreateParameter("@codeCol3", adVarChar, adParamInput, 50, codeCol3)
			   .parameters.Append .CreateParameter("@codeCol4", adVarChar, adParamInput, 50, codeCol4)
			   .parameters.Append .CreateParameter("@codeCol5", adVarChar, adParamInput, 50, codeCol5)
			   .parameters.Append .CreateParameter("@codeCol6", adVarChar, adParamInput, 50, codeCol6)
			   .parameters.Append .CreateParameter("@codeCol7", adVarChar, adParamInput, 50, codeCol7)
			   .parameters.Append .CreateParameter("@codeCol8", adVarChar, adParamInput, 50, codeCol8)
			   .parameters.Append .CreateParameter("@codeCol9", adVarChar, adParamInput, 50, codeCol9)
			   .parameters.Append .CreateParameter("@codeCol10", adVarChar, adParamInput, 50, codeCol10)
			   .parameters.Append .CreateParameter("@samplingRequired ", adVarChar, adParamInput, 50, samplingRequired)
			   .parameters.Append .CreateParameter("@samplingComplete", adVarChar, adParamInput, 50, samplingComplete)
			   .CommandType = adCmdStoredProc
			End With
			
			Set rs = oCmd.Execute
			Set oCmd = nothing
		
		next
		
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildMonthlyPDF.asp?reportID=" & reportID,  "LeftMargin=30, RightMargin=10, TopMargin=30, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
	'	If autoEmail(project) = true then
	'		sendAutoEmail reportID,project,division
	'	end if
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
'	Case "editMonthly"
	
		'report header
'		reportID = request("reportID")
'		comments = request("comments")
'		division = request("division")
'		inspectionDate = checknull(request("inspectionDate"))
				
		'Open connection and insert into the database
'		On Error Resume Next
			
'		DataConn.Open Session("Connection"), Session("UserID")
			
	
		'Create command
'		Set oCmd = Server.CreateObject("ADODB.Command")
		
'		With oCmd
'		   .ActiveConnection = DataConn
'		   .CommandText = "spEditDaily"
'		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
'		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
'		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
'		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
'		   .CommandType = adCmdStoredProc
'		End With
'		Set rs = oCmd.Execute
		
'		Set oCmd = nothing
'		rs.close
'		set rs = nothing

		'add the monthly report data here
'		For Each Item in Request.Form
'			if Left(item,6) = "answer" then
				
				'get the questionID
'				iQuestionID = Replace(item,"answer", "")
'				sAnswer = (Request.Form(Item))
					
				'response.Write iQuestionID & "<br>"
				'response.Write sAnswer & "<br>"
				
				'response.Write Left(item,9) & "<br>"
				
				'Create command
'				Set oCmd = Server.CreateObject("ADODB.Command")
'				With oCmd
'				   .ActiveConnection = DataConn
'				   .CommandText = "spEditMonthlyReport"
'				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
'				   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, iQuestionID)
'				   .parameters.Append .CreateParameter("@answer", adVarChar, adParamInput, 150, sAnswer)
'				   .CommandType = adCmdStoredProc
'				End With
'						
'				Set rs = oCmd.Execute
'				
'				Set oCmd = nothing
'				rs.close
'				set rs = nothing
'			end if	
'		next
		
'		For Each Item in Request.Form
'			if Left(item,6) = "remark" then
'				iQuestionID = Replace(item,"remark", "")
'				sRemark = (Request.Form(Item))
'					
'				'response.Write iQuestionID & "<br>"
'				'response.Write sRemark & "<br>"
'				'Create command
'				If sRemark <> "" then
'					Set oCmd2 = Server.CreateObject("ADODB.Command")
'					With oCmd2
'					   .ActiveConnection = DataConn
'					   .CommandText = "spAddMonthlyRemark"
'					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
'					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, iQuestionID)
'					   .parameters.Append .CreateParameter("@remark", adVarChar, adParamInput, 500, sRemark)
'					   .CommandType = adCmdStoredProc
'					End With
'							
'					Set rs = oCmd2.Execute
'					
'					Set oCmd2 = nothing
'					rs.close
'					set rs = nothing
'				end if
'			end if
'		next
		
		
		'log the details of the report
'		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
'		Set Pdf = Server.CreateObject("Persits.Pdf")
'		Pdf.RegKey = sRegKey
'		Set Doc = Pdf.CreateDocument
'		Doc.ImportFromUrl sPDFPath & "buildMonthlyPDF.asp?divisionID=" & division & "&reportID=" & reportID', "landscape=true"
'		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
'		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "addBiWeekly"
	
		'report header
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		rainfallAmount = request("rainfallAmount")
		
		districtOffice = request("districtOffice")
		EPDDivision = request("EPDDivision")
		address = request("address")
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		reportCovering = request("reportCovering")
		'project see above
		county = request("county")
		observation1 = request("observation1")
		actionTaken1 = request("actionTaken1")
		observation2 = request("observation2")
		actionTaken2 = request("actionTaken2")
		
		addToJournal = request("addToJournal")
		
		soilStabilizationComments = checknull(request("soilStabilizationComments"))
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'add the bi weekly data here
		'Create command
		Set oCmd2 = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd2
		   .ActiveConnection = DataConn
		   .CommandText = "spAddBiWeekly"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@reportCovering", adVarChar, adParamInput, 150, reportCovering)		   
		   .parameters.Append .CreateParameter("@districtOffice", adVarChar, adParamInput, 150, districtOffice)
		   .parameters.Append .CreateParameter("@EPDDivision", adVarChar, adParamInput, 150, EPDDivision)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 150, address)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 150, city)		   
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@county", adVarChar, adParamInput, 150, county)		   
		   .parameters.Append .CreateParameter("@observation1", adVarChar, adParamInput, 500, observation1)
		   .parameters.Append .CreateParameter("@actionTaken1", adVarChar, adParamInput, 500, actionTaken1)
		   .parameters.Append .CreateParameter("@observation2", adVarChar, adParamInput, 500, observation2)
		   .parameters.Append .CreateParameter("@actionTaken2", adVarChar, adParamInput, 500, actionTaken2)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs2 = oCmd2.Execute

		Set oCmd2 = nothing
		rs2.close
		set rs2 = nothing
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildBiWeeklyPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		'If autoEmail(project) = true then
		'	sendAutoEmail reportID,project,division
		'end if
	
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "editBiWeekly"
	
		'report header
		biWeeklyReportID = request("biWeeklyReportID")
		reportID = request("reportID")
		comments = request("comments")
		project = request("project")
		rainfallAmount = request("rainfallAmount")
		
		districtOffice = request("districtOffice")
		EPDDivision = request("EPDDivision")
		address = request("address")
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		reportCovering = request("reportCovering")
		'project see above
		county = request("county")
		observation1 = request("observation1")
		actionTaken1 = request("actionTaken1")
		observation2 = request("observation2")
		actionTaken2 = request("actionTaken2")
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		
		'edit the bi weekly data here
		'Create command
		Set oCmd2 = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd2
		   .ActiveConnection = DataConn
		   .CommandText = "spEditBiWeekly"
		   .parameters.Append .CreateParameter("@biWeeklyReportID", adInteger, adParamInput, 8, biWeeklyReportID)
		   .parameters.Append .CreateParameter("@reportCovering", adVarChar, adParamInput, 150, reportCovering)		   
		   .parameters.Append .CreateParameter("@districtOffice", adVarChar, adParamInput, 150, districtOffice)
		   .parameters.Append .CreateParameter("@EPDDivision", adVarChar, adParamInput, 150, EPDDivision)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 150, address)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 150, city)		   
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@county", adVarChar, adParamInput, 150, county)		   
		   .parameters.Append .CreateParameter("@observation1", adVarChar, adParamInput, 500, observation1)
		   .parameters.Append .CreateParameter("@actionTaken1", adVarChar, adParamInput, 500, actionTaken1)
		   .parameters.Append .CreateParameter("@observation2", adVarChar, adParamInput, 500, observation2)
		   .parameters.Append .CreateParameter("@actionTaken2", adVarChar, adParamInput, 500, actionTaken2)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs2 = oCmd2.Execute

		Set oCmd = nothing
		rs.close
		set rs = nothing
		Set oCmd2 = nothing
		rs2.close
		set rs2 = nothing
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document edited"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildBiWeeklyPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
	
		response.Redirect "reportList.asp?findReport=" & reportID
	
	Case "addWaterSampling"
	
		'report header
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = null'request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = 1'don't need weather type for water sampling report request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		rainfallAmount = request("rainfallAmount")
		
		addToJournal = request("addToJournal")
		isBillable = request("isBillable")
		
		rainfallSource = checknull(request("rainfallSource"))
		soilStabilizationComments = checknull(request("soilStabilizationComments"))
		
		'check to see if initial report has been done on this project
		'checkInitialInspection project
		
			
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReportWS"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@isBillable", adBoolean, adParamInput, 1, isOn(isBillable))
		   .parameters.Append .CreateParameter("@rainfallSource", adVarChar, adParamInput, 50, rainfallSource)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		'get the number of rows
		dataCount = request("dataCount")
		'need to add the weekly and hourly data to report
		'add the data to the ECChecklist table for this report
		For i = 1 to dataCount
		
			'data for the water sampling report
			sampleNumber = "sampleNumber" & i
			dateSampleTaken = "dateSampleTaken" & i
			exactLocation = "exactLocation" & i
			city = "city" & i
			county = "county" & i
			timeSampled = "timeSampled" & i
			
			sampleMethod = "sampleMethod" & i
			
			'isAutomatic = "isAutomatic" & i			
			'isManual = "isManual" & i
			sampledBy = "sampledBy" & i
			analysisDate = "analysisDate" & i
			analysisTime = "analysisTime" & i			
			analysisBy = "analysisBy" & i
			analysisMethod = "analysisMethod" & i
			calibrationDate = "calibrationDate" & i
			calibrationTime = "calibrationTime" & i
			resultsNTU = "resultsNTU" & i
			rain = "rain" & i
			
			sampleNumber = request(sampleNumber)
			dateSampleTaken = request(dateSampleTaken)
			exactLocation = request(exactLocation)
			city = null'request(city)
			county = null'request(county)
			timeSampled = request(timeSampled)
			sampleMethod = request(sampleMethod)
			select case sampleMethod
				case 0
					isAutomatic = "on"	
					isManual = ""	
				case 1
					isManual = "on"
					isAutomatic = ""	
				case else
					isManual = ""
					isAutomatic = ""
				
			end select
		
			
			sampledBy = request(sampledBy)
			analysisDate = request(analysisDate)
			analysisTime = request(analysisTime)
			analysisBy = request(analysisBy)
			analysisMethod = request(analysisMethod)
			calibrationDate = request(calibrationDate)
			calibrationTime = request(calibrationTime)
			resultsNTU = request(resultsNTU)
			rain = request(rain)

			if dateSampleTaken = "" then
				dateSampleTaken = null
			end if

			if analysisDate = "" then
				analysisDate = null
			end if

			if calibrationDate = "" then
				calibrationDate = null
			end if

	
			'add the water sampling data here
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddWaterSampling"
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
			   .parameters.Append .CreateParameter("@sampleNumber", adInteger, adParamInput, 8, sampleNumber)
			   .parameters.Append .CreateParameter("@dateSampleTaken", adDBTimeStamp, adParamInput, 8, dateSampleTaken)
			   .parameters.Append .CreateParameter("@exactLocation", adVarChar, adParamInput, 150, exactLocation)
			   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, null)
			   .parameters.Append .CreateParameter("@county", adVarChar, adParamInput, 100, null)
			   .parameters.Append .CreateParameter("@timeSampled", adVarChar, adParamInput, 50, timeSampled)
			   .parameters.Append .CreateParameter("@isAutomatic", adBoolean, adParamInput, 1, isOn(isAutomatic))
			   .parameters.Append .CreateParameter("@isManual", adBoolean, adParamInput, 1, isOn(isManual))
			   .parameters.Append .CreateParameter("@sampledBy", adVarChar, adParamInput, 150, sampledBy)
			   .parameters.Append .CreateParameter("@analysisDate", adDBTimeStamp, adParamInput, 8, analysisDate)
			   .parameters.Append .CreateParameter("@analysisTime", adVarChar, adParamInput, 50, analysisTime)
			   .parameters.Append .CreateParameter("@analysisBy", adVarChar, adParamInput, 150, analysisBy)
			   .parameters.Append .CreateParameter("@analysisMethod", adVarChar, adParamInput, 150, analysisMethod)
			   .parameters.Append .CreateParameter("@calibrationDate", adDBTimeStamp, adParamInput, 8, null)
			   .parameters.Append .CreateParameter("@calibrationTime", adVarChar, adParamInput, 50, null)
			   .parameters.Append .CreateParameter("@resultsNTU", adVarChar, adParamInput, 50, resultsNTU)
			   .parameters.Append .CreateParameter("@rain", adDouble, adParamInput, 8, rain)
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
		
		next
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildWaterSamplingPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "editWaterSampling"
		
		waterSamplingReportID = request("waterSamplingReportID")
		reportID = request("reportID")
		comments = request("comments")
		rainfallAmount = request("rainfallAmount")
		isBillable = request("isBillable")
		inspectionDate = checknull(request("isBillable"))
		rainfallSource = checknull(request("rainfallSource"))
		soilStabilizationComments = checknull(request("soilStabilizationComments"))
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditReportWS"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@isBillable", adBoolean, adParamInput, 1, isOn(isBillable))
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@rainfallSource", adVarChar, adParamInput, 50, rainfallSource)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd.Execute
		
		
		'delete the current rows in the watersampling report table
		'Create command
		Set oCmdDel = Server.CreateObject("ADODB.Command")
		
		With oCmdDel
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteWaterSampling"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
		
		Set rsDel = oCmdDel.Execute
		
		'get the number of rows
		dataCount = request("dataCount")
		'need to add the weekly and hourly data to report
		'add the data to the ECChecklist table for this report
		For i = 1 to dataCount
		
			'data for the water sampling report
			sampleNumber = "sampleNumber" & i
			dateSampleTaken = "dateSampleTaken" & i
			exactLocation = "exactLocation" & i
			city = "city" & i
			county = "county" & i
			timeSampled = "timeSampled" & i
			rain = "rain" & i
			sampledBy = "sampledBy" & i
			analysisDate = "analysisDate" & i
			analysisTime = "analysisTime" & i			
			analysisBy = "analysisBy" & i
			analysisMethod = "analysisMethod" & i
			calibrationDate = "calibrationDate" & i
			calibrationTime = "calibrationTime" & i
			resultsNTU = "resultsNTU" & i
			sampleMethod = "sampleMethod" & i
			
			sampleNumber = request(sampleNumber)
			dateSampleTaken = request(dateSampleTaken)
			exactLocation = request(exactLocation)
			city = null'request(city)
			county = null'request(county)
			timeSampled = request(timeSampled)
			sampleMethod = request(sampleMethod)
			select case sampleMethod
				case 0
					isAutomatic = "on"	
					isManual = ""	
				case 1
					isManual = "on"
					isAutomatic = ""	
				case else
					isManual = ""
					isAutomatic = ""
				
			end select
	'response.Write "auto " & isAutomatic & "<br>"
	'response.Write "man " & isManual & "<br>"
			sampledBy = request(sampledBy)
			analysisDate = request(analysisDate)
			analysisTime = request(analysisTime)
			analysisBy = request(analysisBy)
			analysisMethod = request(analysisMethod)
			calibrationDate = request(calibrationDate)
			calibrationTime = request(calibrationTime)
			resultsNTU = request(resultsNTU)
			rain = request(rain)

			if dateSampleTaken = "" then
				dateSampleTaken = null
			end if

			if analysisDate = "" then
				analysisDate = null
			end if

			if calibrationDate = "" then
				calibrationDate = null
			end if

	
			'add the water sampling data here
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddWaterSampling"
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
			   .parameters.Append .CreateParameter("@sampleNumber", adInteger, adParamInput, 8, sampleNumber)
			   .parameters.Append .CreateParameter("@dateSampleTaken", adDBTimeStamp, adParamInput, 8, dateSampleTaken)
			   .parameters.Append .CreateParameter("@exactLocation", adVarChar, adParamInput, 150, exactLocation)
			   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, null)
			   .parameters.Append .CreateParameter("@county", adVarChar, adParamInput, 100, null)
			   .parameters.Append .CreateParameter("@timeSampled", adVarChar, adParamInput, 50, timeSampled)
			   .parameters.Append .CreateParameter("@isAutomatic", adBoolean, adParamInput, 1, isOn(isAutomatic))
			   .parameters.Append .CreateParameter("@isManual", adBoolean, adParamInput, 1, isOn(isManual))
			   .parameters.Append .CreateParameter("@sampledBy", adVarChar, adParamInput, 150, sampledBy)
			   .parameters.Append .CreateParameter("@analysisDate", adDBTimeStamp, adParamInput, 8, analysisDate)
			   .parameters.Append .CreateParameter("@analysisTime", adVarChar, adParamInput, 50, analysisTime)
			   .parameters.Append .CreateParameter("@analysisBy", adVarChar, adParamInput, 150, analysisBy)
			   .parameters.Append .CreateParameter("@analysisMethod", adVarChar, adParamInput, 150, analysisMethod)
			   .parameters.Append .CreateParameter("@calibrationDate", adDBTimeStamp, adParamInput, 8, null)
			   .parameters.Append .CreateParameter("@calibrationTime", adVarChar, adParamInput, 50, null)
			   .parameters.Append .CreateParameter("@resultsNTU", adVarChar, adParamInput, 50, resultsNTU)
			   .parameters.Append .CreateParameter("@rain", adDouble, adParamInput, 8, rain)
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
		
		next
	
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Edited","Report and PDF document Edited"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildWaterSamplingPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		response.Redirect "reportList.asp?findReport=" & reportID

	Case "addUtility"
		lotID = request("lotID")
		lotNumber = request("lotNumber")
		address = request("address")
		confirmationNumber = request("confirmationNumber")
		contractor = request("contractor")
		dateBegins = request("dateBegins")
		if dateBegins = "" then
			dateBegins = null
		end if
		renewBy = request("renewBy")
		if renewBy = "" then
			renewBy = null
		end if
		dateEnds = request("dateEnds")
		if dateEnds = "" then
			dateEnds = null
		end if
		
		customerID = request("customerID")
		divisionID = request("divisionID")
		projectID = request("projectID")
		sPg = request("pg")
	
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddUtility"
		   .parameters.Append .CreateParameter("@lotID", adInteger, adParamInput, 8, lotID)
		   .parameters.Append .CreateParameter("@lotNumber", adInteger, adParamInput, 8, lotNumber)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 150, address)
		   .parameters.Append .CreateParameter("@confirmationNumber", adVarChar, adParamInput, 100, confirmationNumber)
		   .parameters.Append .CreateParameter("@contractor", adVarChar, adParamInput, 100, contractor)
		   .parameters.Append .CreateParameter("@dateBegins", adDBTimeStamp, adParamInput, 8, dateBegins)
		   .parameters.Append .CreateParameter("@renewBy", adDBTimeStamp, adParamInput, 8, renewBy)
		   .parameters.Append .CreateParameter("@dateEnds", adDBTimeStamp, adParamInput, 8, dateEnds)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
		if sPg = "ol" then
			response.Redirect "openLots.asp?id=" & projectID & "&customerID=" & customerID & "&divisionID=" & divisionID
		else
			response.Redirect "closedLots.asp?id=" & projectID & "&customerID=" & customerID & "&divisionID=" & divisionID
		end if
		
	Case "editUtility"
		locateLogID = request("locateLogID")
		lotID = request("lotID")
		lotNumber = request("lotNumber")
		address = request("address")
		confirmationNumber = request("confirmationNumber")
		contractor = request("contractor")
		dateBegins = request("dateBegins")
		if dateBegins = "" then
			dateBegins = null
		end if
		renewBy = request("renewBy")
		if renewBy = "" then
			renewBy = null
		end if
		dateEnds = request("dateEnds")
		if dateEnds = "" then
			dateEnds = null
		end if
		
		customerID = request("customerID")
		divisionID = request("divisionID")
		projectID = request("projectID")
		sPg = request("pg")
	
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditUtility"
		   .parameters.Append .CreateParameter("@locateLogID", adInteger, adParamInput, 8, locateLogID)
		  ' .parameters.Append .CreateParameter("@lotID", adInteger, adParamInput, 8, lotID)
		   '.parameters.Append .CreateParameter("@lotNumber", adInteger, adParamInput, 8, lotNumber)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 150, address)
		   .parameters.Append .CreateParameter("@confirmationNumber", adVarChar, adParamInput, 100, confirmationNumber)
		   .parameters.Append .CreateParameter("@contractor", adVarChar, adParamInput, 100, contractor)
		   .parameters.Append .CreateParameter("@dateBegins", adDBTimeStamp, adParamInput, 8, dateBegins)
		   .parameters.Append .CreateParameter("@renewBy", adDBTimeStamp, adParamInput, 8, renewBy)
		   .parameters.Append .CreateParameter("@dateEnds", adDBTimeStamp, adParamInput, 8, dateEnds)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
		if sPg = "ol" then
			response.Redirect "openLots.asp?id=" & projectID & "&customerID=" & customerID & "&divisionID=" & divisionID
		else
			response.Redirect "closedLots.asp?id=" & projectID & "&customerID=" & customerID & "&divisionID=" & divisionID
		end if
		
	Case "addECChecklistMonthly"

		'add the report details to the report page
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = request("projectStatus")
		rainfallAmount = request("rainfallAmount")
		reportNumber = request("reportNumber")
		disturbedAcreage = request("disturbedAcreage")
		
		if reportNumber = "" then
			reportNumber = 0
		end if
		
		if disturbedAcreage = "" then
			disturbedAcreage = 0
		end if
		
		addToJournal = request("addToJournal")
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
	
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if	
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReportEC"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@reportNumber", adInteger, adParamInput, 8, reportNumber)
		   .parameters.Append .CreateParameter("@disturbedAcreage", adInteger, adParamInput, 8, disturbedAcreage)
		   .parameters.Append .CreateParameter("@deadlineToCorrect", adVarChar, adParamInput, 50, null)
		   .parameters.Append .CreateParameter("@currentPhase", adVarChar, adParamInput, 50, null)
		   .CommandType = adCmdStoredProc
		End With
			
		Set rs = oCmd.Execute
	
		'get the report id that we just created
		reportID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
			
		'get the number of rows
		dataCount = request("dataCount")
		'need to add the weekly and hourly data to report
		'add the data to the ECChecklist table for this report
		For i = 1 to dataCount	
			'if there is an ECChecklist ID, then update. else add the new record
			
			'get the ECChecklistID
			ECChecklistID = "ECChecklistID" & i			
			device = "device" & i
			location = "location" & i
			installDate = "installDate" & i
			percentFilled = "percentFilled" & i
			maintReq = "maintReq" & i
			maintenanceRequired = "maintenanceRequired" & i
			comments = "comments" & i
			dateFixed = "dateFixed" & i
			addRemBMP = "addRemBMP" & i
			
			ECChecklistID = request(ECChecklistID)
			periodWeekly = request("periodWeekly")
			periodHour = request("periodHour")
			device = request(device)
			location = request(location)
			installDate = request(installDate)
			percentFilled = request(percentFilled)
			maintReq = request(maintReq)
			maintenanceRequired = request(maintenanceRequired)
			comments = request(comments)
			dateFixed = request(dateFixed)
			addRemBMP = request(addRemBMP)
			checklistType = request("checklistType")
			
			if installDate = "" then
				installDate = null
			end if
			
			if dateFixed = "" then
				dateFixed = null
			end if
			
			if percentFilled = "" then
				percentFilled = null
			end if
			
			If device <> "" then
				'add the data to the ecchecklist table
				Set oCmd2 = Server.CreateObject("ADODB.Command")
				If Err Then
				%>
					<!--#include file="includes/FatalError.inc"-->
				<%
				End If
				
				With oCmd2
				   .ActiveConnection = DataConn
				   .CommandText = "spAddECChecklist"
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@deviceID", adInteger, adParamInput, 8, device)
				   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 250, location)
				   .parameters.Append .CreateParameter("@installDate", adDBTimeStamp, adParamInput, 8, installDate)
				   .parameters.Append .CreateParameter("@percentFilled", adDouble, adParamInput, 8, percentFilled)
				   .parameters.Append .CreateParameter("@maintReq", adVarChar, adParamInput, 50, maintReq)
				   .parameters.Append .CreateParameter("@maintenanceRequired", adVarChar, adParamInput, 50, maintenanceRequired)
				   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 500, comments)
				   .parameters.Append .CreateParameter("@dateFixed", adDBTimeStamp, adParamInput, 8, dateFixed)
				   .parameters.Append .CreateParameter("@addRemBMP", adVarChar, adParamInput, 100, addRemBMP)
				   .parameters.Append .CreateParameter("@checklistType", adVarChar, adParamInput, 50, checklistType)
				   .CommandType = adCmdStoredProc
				End With
				
				If Err Then
				%>
					<!--#include file="includes/FatalError.inc"-->
				<%
				End If
			
				Set rs2 = oCmd2.Execute
				
				ecChecklistID = rs2("Identity")
				
				'if the maint req select box says yes, then add it to the corrective action table
				if maintReq = "Yes" then
					'add the item to the responsiveAction table
					Set oCmd3 = Server.CreateObject("ADODB.Command")					
					With oCmd3
					   .ActiveConnection = DataConn
					   .CommandText = "spAddResponsiveActionsEC1"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, 0) 'make it zero because we don't have a questionID
					   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, "EC1-" & ecChecklistID)
					   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, maintenanceRequired)
					   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000, location)
					   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
					   .CommandType = adCmdStoredProc
					   
					End With
							
					Set rs2 = oCmd3.Execute
				end if
				
			End if			
		next
		
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		'log the details of the report
		'logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildECChecklistMonthlyPDF.asp?reportID=" & reportID, "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "addECChecklist"

		'add the report details to the report page
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = request("projectStatus")
		rainfallAmount = checknull(request("rainfallAmount"))
		reportNumber = request("reportNumber")
		disturbedAcreage = request("disturbedAcreage")
		deadlineToCorrect= checknull(request("deadlineToCorrect"))
		currentPhase= checknull(request("currentPhase"))
		
		if reportNumber = "" then
			reportNumber = 0
		end if
		
		if disturbedAcreage = "" then
			disturbedAcreage = 0
		end if
		
		addToJournal = request("addToJournal")
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
	
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if	
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReportEC"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@reportNumber", adVarChar, adParamInput, 50, reportNumber)
		   .parameters.Append .CreateParameter("@disturbedAcreage", adDouble, adParamInput, 8, disturbedAcreage)
		   .parameters.Append .CreateParameter("@deadlineToCorrect", adVarChar, adParamInput, 50, deadlineToCorrect)
		   .parameters.Append .CreateParameter("@currentPhase", adVarChar, adParamInput, 50, currentPhase)
		   .CommandType = adCmdStoredProc
		End With
			
		Set rs = oCmd.Execute
	
		'get the report id that we just created
		reportID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
			
		'get the number of rows
		dataCount = request("dataCount")
		'need to add the weekly and hourly data to report
		'add the data to the ECChecklist table for this report
		For i = 1 to dataCount	
			'if there is an ECChecklist ID, then update. else add the new record
			
			'get the ECChecklistID
			removeBMP = "remove" & i
			ECChecklistID = "ECChecklistID" & i			
			device = "device" & i
			location = "location" & i
			installDate = "installDate" & i
			percentFilled = "percentFilled" & i
			maintReq = "maintReq" & i
			maintenanceRequired = "maintenanceRequired" & i
			comments = "comments" & i
			dateFixed = "dateFixed" & i
			addRemBMP = "addRemBMP" & i
			
			removeBMP = request(removeBMP)
			ECChecklistID = request(ECChecklistID)
			periodWeekly = checknull(request("periodWeekly"))
			periodHour = checknull(request("periodHour"))
			device = checknull(request(device))
			location = checknull(request(location))
			installDate = checknull(request(installDate))
			percentFilled = checknull(request(percentFilled))
			maintReq = checknull(request(maintReq))
			maintenanceRequired = checknull(request(maintenanceRequired))
			comments = checknull(request(comments))
			dateFixed = checknull(request(dateFixed))
			addRemBMP = checknull(request(addRemBMP))
			checklistType = checknull(request("checklistType"))
			
			'if installDate = "" then
			'	installDate = null
			'end if
			
			'if dateFixed = "" then
			'	dateFixed = null
			'end if
			
			'if percentFilled = "" then
		'		percentFilled = null
		'	end if
			
			If device <> "" then
				'if remove then don't add
				If removeBMP = "" then
					'add the data to the ecchecklist table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddECChecklist"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@deviceID", adInteger, adParamInput, 8, device)
					   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 250, location)
					   .parameters.Append .CreateParameter("@installDate", adVarChar, adParamInput, 50, installDate)
					   .parameters.Append .CreateParameter("@percentFilled", adDouble, adParamInput, 8, percentFilled)
					   .parameters.Append .CreateParameter("@maintReq", adVarChar, adParamInput, 50, maintReq)
					   .parameters.Append .CreateParameter("@maintenanceRequired", adVarChar, adParamInput, 50, maintenanceRequired)
					   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 500, comments)
					   .parameters.Append .CreateParameter("@dateFixed", adVarChar, adParamInput, 50, dateFixed)
					   .parameters.Append .CreateParameter("@addRemBMP", adVarChar, adParamInput, 100, addRemBMP)
					   .parameters.Append .CreateParameter("@checklistType", adVarChar, adParamInput, 50, checklistType)
					   .CommandType = adCmdStoredProc
					End With
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
				
					Set rs2 = oCmd2.Execute
					
					ecChecklistID = rs2("Identity")
					
					'if the maint req select box says yes, then add it to the corrective action table
					if maintReq = "Yes" then
						'add the item to the responsiveAction table
						Set oCmd3 = Server.CreateObject("ADODB.Command")					
						With oCmd3
						   .ActiveConnection = DataConn
						   .CommandText = "spAddResponsiveActionsEC1"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
						   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, 0) 'make it zero because we don't have a questionID
						   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, "EC1-" & ecChecklistID)
						   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, maintenanceRequired)
						   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000, location)
						   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
						   .CommandType = adCmdStoredProc
						   
						End With
								
						Set rs2 = oCmd3.Execute
					end if
					
				end if
				
			End if
		
		next
		
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildECChecklistPDF.asp?reportID=" & reportID, "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "editECChecklist"
		'edit the header
		division = request("division")
		reportID = request("reportID")
		comments = request("comments")
		rainfallAmount = request("rainfallAmount")
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd.Execute
		
'		Set oCmd = nothing
'		rs.close
'		set rs = nothing

		'delete the checklist items from the ecchecklist table
		Set oCmdDel = Server.CreateObject("ADODB.Command")
		With oCmdDel
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteECChecklistItems"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
		Set rsDel = oCmdDel.Execute
		
		
		
'		Set oCmdDel = nothing
'		rsDel.close
'		set rsDel = nothing
			
		'get the number of rows
		dataCount = request("dataCount")
		'need to add the weekly and hourly data to report
		'add the data to the ECChecklist table for this report
		For i = 1 to dataCount		
			

			device = "device" & i
			location = "location" & i
			installDate = "installDate" & i
			percentFilled = "percentFilled" & i
			maintenanceRequired = "maintenanceRequired" & i
			comments = "comments" & i
			dateFixed = "dateFixed" & i
			
			periodWeekly = request("periodWeekly")
			periodHour = request("periodHour")
			device = request(device)
			location = request(location)
			installDate = request(installDate)
			percentFilled = request(percentFilled)
			maintenanceRequired = request(maintenanceRequired)
			comments = request(comments)
			dateFixed = request(dateFixed)
			
			if installDate = "" then
				installDate = null
			end if
			
			if dateFixed = "" then
				dateFixed = null
			end if
			
			if percentFilled = "" then
				percentFilled = null
			end if
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			If device <> "" then
				'add the data to the ecchecklist table
				Set oCmd2 = Server.CreateObject("ADODB.Command")
				If Err Then
				%>
				2	<!--#include file="includes/FatalError.inc"-->
				<%
				End If
				
				With oCmd2
				   .ActiveConnection = DataConn
				   .CommandText = "spAddECChecklist"
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@deviceID", adInteger, adParamInput, 8, device)
				   .parameters.Append .CreateParameter("@periodWeekly", adBoolean, adParamInput, 1, isOn(periodWeekly))
				   .parameters.Append .CreateParameter("@periodHour", adBoolean, adParamInput, 1, isOn(periodHour))
				   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 250, location)
				   .parameters.Append .CreateParameter("@installDate", adDBTimeStamp, adParamInput, 8, installDate)
				   .parameters.Append .CreateParameter("@percentFilled", adInteger, adParamInput, 8, percentFilled)
				   .parameters.Append .CreateParameter("@maintenanceRequired", adVarChar, adParamInput, 50, maintenanceRequired)
				   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 500, comments)
				   .parameters.Append .CreateParameter("@dateFixed", adDBTimeStamp, adParamInput, 8, dateFixed)
				   .CommandType = adCmdStoredProc
				End With
				
				If Err Then
				%>
				3	<!--#include file="includes/FatalError.inc"-->
				<%
				End If
				
				Set rs2 = oCmd2.Execute
				
			End if
			
		next
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Edited","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildECChecklistPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "editGDOTDaily"
		
		reportID = request("reportID")
		comments = request("comments")
		reportNumber = request("reportNumber")
		
		response.Write reportID & "<br>"
		response.Write comments & "<br>"
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditECDaily"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@reportNumber", adVarChar, adParamInput, 50, reportNumber)
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		'delete all the daily entries before we add the new ones
		'Create command
		Set oCmd3 = Server.CreateObject("ADODB.Command")
		
		With oCmd3
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteGDOTDaily"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsDel = oCmd3.Execute
		
		Set oCmd3 = nothing
		rsDel.close
		set rsDel = nothing
		

		'loop through for 31 days
		For i = 1 to 31
				iRain = "rainfall" & i			
				iRain = request.Form(iRain)

				If iRain <> "" then
					'add the data to the daily table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
				
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddGDOTDailyReport"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@dayOfMonth", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, iRain)
					   .CommandType = adCmdStoredProc
					End With
				
					Set rs = oCmd2.Execute
	
				End if
		Next
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Edited","Report and PDF document Edited"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildGDOTDailyPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "addGDOTDaily"
	
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = 1'don't need weather type for daily reportrequest("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = request("projectStatus")
		reportNumber = request("reportNumber")
		
		addToJournal = request("addToJournal")
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReportEC"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, 0)
		   .parameters.Append .CreateParameter("@reportNumber", adVarChar, adParamInput, 50, reportNumber)
		   .parameters.Append .CreateParameter("@disturbedAcreage", adInteger, adParamInput, 8, 0)
		   .parameters.Append .CreateParameter("@deadlineToCorrect", adVarChar, adParamInput, 50, null)
		   .parameters.Append .CreateParameter("@currentPhase", adVarChar, adParamInput, 50, null)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		

		'Dim i
		'loop through for 31 days
		For i = 1 to 31
				iRain = "rainfall" & i
				iRain = request.Form(iRain)


				If iRain <> "" then
					'add the data to the daily table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddGDOTDailyReport"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@dayOfMonth", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, iRain)
					   .CommandType = adCmdStoredProc
					End With
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					Set rs = oCmd2.Execute
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If		
				End if
		Next
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildGDOTDailyPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "addUtilDaily"
	
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = null'request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = 1'don't need weather type for daily reportrequest("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		
		addToJournal = request("addToJournal")
		
		soilStabilizationComments = checknull(request("soilStabilizationComments"))
		
		inCompliance = checknull(request("inCompliance"))
		
		'check to see if initial report has been done on this project
		'checkInitialInspection project
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReportUtilDaily"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, 0)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .parameters.Append .CreateParameter("@inCompliance", adBoolean, adParamInput, 1, isOn(inCompliance))
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		

		'Dim i
		'loop through for 31 days
		For i = 1 to 31
				iRain = "rainfall" & i
				sQues1 = "question1_" & i
				sQues2 = "question2_" & i
				sInspector = "inspector" & i
				sTime = "time" & i
				sComments = "comments" & i
				
				iRain = request.Form(iRain)
				sQues1 = request.Form(sQues1)
				if sQues1 = "No" then
					sQues1 = ""
				else
					sQues1 = "on"
				end if
				sQues2 = request.Form(sQues2)
				if sQues2 = "No" then
					sQues2 = ""
				else
					sQues2 = "on"
				end if
				sInspector = request.Form(sInspector)
				sTime = request.Form(sTime)
				sComments = request.Form(sComments)
				
			'	if sCompliant = "No" then
			'		sCompliant = ""
			'	else
			'		sCompliant = "on"
			'	end if

				If iRain <> "" then
					'add the data to the daily table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddDailyUtilReport"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@dayOfMonth", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, iRain)
					   .parameters.Append .CreateParameter("@question1Response", adBoolean, adParamInput, 1, isOn(sQues1))
					   .parameters.Append .CreateParameter("@question2Response", adBoolean, adParamInput, 1, isOn(sQues2))
					   .parameters.Append .CreateParameter("@inspector", adInteger, adParamInput, 8, sInspector)
					   .parameters.Append .CreateParameter("@time", adVarChar, adParamInput, 50, sTime)
					   .parameters.Append .CreateParameter("@sComments", adVarChar, adParamInput, 250, sComments)
					   .CommandType = adCmdStoredProc
					End With
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					Set rs = oCmd2.Execute
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If		
				End if
		Next
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildDailyUtilPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "editUtilDaily"
		
		reportID = request("reportID")
		comments = request("comments")
		inspectionDate = checknull(request("inspectionDate"))
		
		response.Write reportID & "<br>"
		response.Write comments & "<br>"
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditDaily"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		'delete all the daily entries before we add the new ones
		'Create command
		Set oCmd3 = Server.CreateObject("ADODB.Command")
		
		With oCmd3
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteDaily"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsDel = oCmd3.Execute
		
		Set oCmd3 = nothing
		rsDel.close
		set rsDel = nothing
		

		'loop through for 31 days
		For i = 1 to 31
				iRain = "rainfall" & i
				sQues1 = "question1_" & i
				sQues2 = "question2_" & i
				sInspector = "inspector" & i
				sTime = "time" & i
				sComments = "comments" & i
				
				iRain = request.Form(iRain)
				sQues1 = request.Form(sQues1)
				if sQues1 = "No" then
					sQues1 = ""
				else
					sQues1 = "on"
				end if
				sQues2 = request.Form(sQues2)
				if sQues2 = "No" then
					sQues2 = ""
				else
					sQues2 = "on"
				end if
				sInspector = request.Form(sInspector)
				sTime = request.Form(sTime)
				sComments = request.Form(sComments)

				If iRain <> "" then
					'add the data to the daily table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
				
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddDailyUtilReport"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@dayOfMonth", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, iRain)
					   .parameters.Append .CreateParameter("@question1Response", adBoolean, adParamInput, 1, isOn(sQues1))
					   .parameters.Append .CreateParameter("@question2Response", adBoolean, adParamInput, 1, isOn(sQues2))
					   .parameters.Append .CreateParameter("@inspector", adInteger, adParamInput, 8, sInspector)
					   .parameters.Append .CreateParameter("@time", adVarChar, adParamInput, 50, sTime)
					   .parameters.Append .CreateParameter("@sComments", adVarChar, adParamInput, 250, sComments)
					   .CommandType = adCmdStoredProc
					End With
				
					Set rs = oCmd2.Execute
	
				End if
		Next
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Edited","Report and PDF document Edited"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildDailyUtilPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		response.Redirect "reportList.asp?findReport=" & reportID

	Case "addDaily"
	
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = 1'don't need weather type for daily reportrequest("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		
		addToJournal = request("addToJournal")
		
		soilStabilizationComments = checknull(request("soilStabilizationComments"))
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, 0)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		

		Dim i
		'loop through for 31 days
		For i = 1 to 31
				iRain = "rainfall" & i
				sQues1 = "question1_" & i
				sQues2 = "question2_" & i
				sInspector = "inspector" & i
				sCompliant = "compliant_" & i
				
				iRain = request.Form(iRain)
				sQues1 = request.Form(sQues1)
				if sQues1 = "No" then
					sQues1 = ""
				else
					sQues1 = "on"
				end if
				sQues2 = request.Form(sQues2)
				if sQues2 = "No" then
					sQues2 = ""
				else
					sQues2 = "on"
				end if
				sInspector = request.Form(sInspector)
				
				sCompliant = request.Form(sCompliant)
				if sCompliant = "No" then
					sCompliant = ""
				else
					sCompliant = "on"
				end if

				If iRain <> "" then
					'add the data to the daily table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddDailyReport"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@dayOfMonth", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, iRain)
					   .parameters.Append .CreateParameter("@question1Response", adBoolean, adParamInput, 1, isOn(sQues1))
					   .parameters.Append .CreateParameter("@question2Response", adBoolean, adParamInput, 1, isOn(sQues2))
					   .parameters.Append .CreateParameter("@inspector", adInteger, adParamInput, 8, sInspector)
					   .parameters.Append .CreateParameter("@compliant", adBoolean, adParamInput, 1, isOn(sCompliant))
					   .CommandType = adCmdStoredProc
					End With
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					Set rs = oCmd2.Execute
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If		
				End if
		Next
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildDailyPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		'send auto email if true for project
		'If autoEmail(project) = true then
		'	sendAutoEmail reportID,project,division
		'end if
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "editDaily"
		
		reportID = request("reportID")
		comments = request("comments")
		
		response.Write reportID & "<br>"
		response.Write comments & "<br>"
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditDaily"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
		
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		'delete all the daily entries before we add the new ones
		'Create command
		Set oCmd3 = Server.CreateObject("ADODB.Command")
		
		With oCmd3
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteDaily"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsDel = oCmd3.Execute
		
		Set oCmd3 = nothing
		rsDel.close
		set rsDel = nothing
		

		'loop through for 31 days
		For i = 1 to 31
				iRain = "rainfall" & i
				sQues1 = "question1_" & i
				sQues2 = "question2_" & i
				sInspector = "inspector" & i
				sCompliant = "compliant_" & i
				
				iRain = request.Form(iRain)
				sQues1 = request.Form(sQues1)
				if sQues1 = "No" then
					sQues1 = ""
				else
					sQues1 = "on"
				end if
				sQues2 = request.Form(sQues2)
				if sQues2 = "No" then
					sQues2 = ""
				else
					sQues2 = "on"
				end if
				sInspector = request.Form(sInspector)
				
				sCompliant = request.Form(sCompliant)
				if sCompliant = "Yes" then
					sCompliant = "on"
				else
					sCompliant = ""
				end if
				
				'response.Write iRain & "<br>"
				'response.Write sQues1 & "<br>"
				'response.Write sQues2 & "<br>"
				'response.Write sInspector & "<br><br>"

				If iRain <> "" then
					'add the data to the daily table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
				
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddDailyReport"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@dayOfMonth", adInteger, adParamInput, 8, i)
					   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, iRain)
					   .parameters.Append .CreateParameter("@question1Response", adBoolean, adParamInput, 1, isOn(sQues1))
					   .parameters.Append .CreateParameter("@question2Response", adBoolean, adParamInput, 1, isOn(sQues2))
					   .parameters.Append .CreateParameter("@inspector", adInteger, adParamInput, 8, sInspector)
					   .parameters.Append .CreateParameter("@compliant", adBoolean, adParamInput, 1, isOn(sCompliant))
					   .CommandType = adCmdStoredProc
					End With
				
					Set rs = oCmd2.Execute
	
				End if
		Next
		
		'log the details of the report
		logDetails Session("ID"),reportID,"Report Edited","Report and PDF document Edited"		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildDailyPDF.asp?reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		
		response.Redirect "reportList.asp?findReport=" & reportID
		
	Case "addUtilBWPR"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		division = checknull(request("division"))
		project = checknull(request("project"))
		inspector = checknull(request("inspector"))
		contactNumber = null'checknull(request("contactNumber"))
		inspectionType = checknull(request("inspectionType"))
		inspectionDate = checknull(request("inspectionDate"))
		weatherType = checknull(request("weatherType"))
		comments = checknull(request("comments"))
		reportType = checknull(request("reportType"))
		projectStatus = checknull(request("projectStatus"))
		rainfallAmount = checknull(request("rainfallAmount"))
		rainfallSource = checknull(request("rainfallSource"))
		phase = checknull(request("phase"))
		
		addToJournal = checknull(request("addToJournal"))
		personnelPresent = null'request("personnelPresent")
		
		LDADate = checknull(request("LDADate"))
		
		inCompliance = checknull(request("inCompliance"))
		
		updateLDADate project,LDADate
		
		'this is to determine which PDF to generate
		'buildPDFType = request("buildPDFType")
		
		
		'check to see if initial report has been done on this project
		'checkInitialInspection project
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
			
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if
		
		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReportUtil"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@personnelPresent", adVarChar, adParamInput, 50, personnelPresent)
		   .parameters.Append .CreateParameter("@rainfallSource", adVarChar, adParamInput, 50, rainfallSource)
		   .parameters.Append .CreateParameter("@inCompliance", adBoolean, adParamInput, 1, isOn(inCompliance))
		   .parameters.Append .CreateParameter("@phase", adVarChar, adParamInput, 50, phase)
		   
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute

		'get the report id that we just created
		reportID = rs("Identity")

	
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'add the answers to the questions

		For Each Item in Request.Form
			if Left(item,6) = "answer" then				
				
				'for each no answer, add the responsive action needed
				if 	Left(item,8) = "answer_R" then
					'reportID get from above
			
					'questionID get from srting
					iLen1 = len(item)
					iLen2 = InStr(item, "@")
					quesID = Right(item,iLen1 - iLen2)
					'response.Write "question " & quesID & "<br>"
					
					'reference number
					sText = Replace(item,"answer_R", "")
					iLen3 = InStr(sText, "@")
					iLen4 = len(sText)
					sText = Left(sText,iLen3)
					sRefNum = Replace(sText,"@", "")
					
					'action needed
					sActionNeeded = trim(Request.Form(Item))
					
					installedCorrectly = isOn(request("installed" & sRefNum))
					majorObservations = checknull(request("observations" & sRefNum))
					proposedActivity = checknull(request("proposedActivity" & sRefNum))
					dateInstalled = checknull(request("dateInstalled" & sRefNum))
					
					inspected = isOn(request("inspected" & sRefNum))
					
					locationID = checknull(request("locationID" & sRefNum))
				
					If Err Then
				%>
				1		<!--#include file="includes/FatalError.inc"-->
				<%
					End If
					
					'if the dateInstalled <> "" then update the location
					
					if dateInstalled <> "" then
					
						
						
						
					
						Set oCmd = Server.CreateObject("ADODB.Command")
							
						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spEditDateInstalled"
						   .parameters.Append .CreateParameter("@locationID", adInteger, adParamInput, 8, locationID)
						   .parameters.Append .CreateParameter("@dateInstalled", adDBTimeStamp, adParamInput, 8, dateInstalled)
						   .CommandType = adCmdStoredProc
						End With
						
						Set rsDateIns = oCmd.Execute
					
					end if
					
					If Err Then
				%>
				2		<!--#include file="includes/FatalError.inc"-->
				<%
					End If
					
				'	response.Write sActionNeeded & "<br>"
				'	response.Write sRefNum & "<br>"
				'	response.Write installedCorrectly & "<br>"
					
					If 	sActionNeeded <> "" then
'						if 	installedCorrectly = "True" then
						
							'get the insspector name
'							Set oCmd7 = Server.CreateObject("ADODB.Command")							
'							With oCmd7
'							   .ActiveConnection = DataConn
'							   .CommandText = "spGetUser"
'							   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
'							   .CommandType = adCmdStoredProc
'							End With
'							
'							Set rsInspector = oCmd7.Execute
'							
'							addressedBy = rsInspector("firstName") & " " & rsInspector("lastName")
'				
'							if installedCorrectly = "True" then
'								isClosed = "True"
'							else
'								isClosed = "False"
'							end if
'							
'							'insert the data into the database
'							Set oCmd3 = Server.CreateObject("ADODB.Command")
'							
'							With oCmd3
'							   .ActiveConnection = DataConn
'							   .CommandText = "spAddResponsiveActionsInstalled"
'							   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
'							   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
'							   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
'							   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, sActionNeeded)
'							   .parameters.Append .CreateParameter("@majorObservations", adVarChar, adParamInput, 1000, majorObservations)
'							   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, isClosed)
'							   .parameters.Append .CreateParameter("@installedCorrectly", adBoolean, adParamInput, 1, installedCorrectly)
'							   .parameters.Append .CreateParameter("@addressedBy", adVarChar, adParamInput, 50, addressedBy)'addressedBy
'							   .parameters.Append .CreateParameter("@addressedByDate", adDBTimeStamp, adParamInput, 8, now())'now()
'							   .parameters.Append .CreateParameter("@proposedActivity", adVarChar, adParamInput, 500, proposedActivity)
'							   .parameters.Append .CreateParameter("@dateInstalled", adDBTimeStamp, adParamInput, 8, dateInstalled)
'							   .CommandType = adCmdStoredProc
'							End With
'							
'							Set rs2 = oCmd3.Execute
							
'						else	
							'insert the data into the database
							Set oCmd3 = Server.CreateObject("ADODB.Command")
							
							With oCmd3
							   .ActiveConnection = DataConn
							   .CommandText = "spAddResponsiveActionsUtil"
							   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
							   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
							   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
							   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, sActionNeeded)
							   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
							   .parameters.Append .CreateParameter("@majorObservations", adVarChar, adParamInput, 5000, majorObservations)
							   .parameters.Append .CreateParameter("@proposedActivity", adVarChar, adParamInput, 500, proposedActivity)
							   .parameters.Append .CreateParameter("@dateInstalled", adDBTimeStamp, adParamInput, 8, dateInstalled)
							   .CommandType = adCmdStoredProc
							End With
							
							Set rs2 = oCmd3.Execute
							
				'	else
						'add the comments only to new table
						
				'		if majorObservations <> "" then
				'			loc = checknull(request("location_" & sRefNum))
				'			
				'			Set oCmd4 = Server.CreateObject("ADODB.Command")
				'			
				'			With oCmd4
				'			   .ActiveConnection = DataConn
				'			   .CommandText = "spAddMajorObservations"
				'			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				'			   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
				'			   .parameters.Append .CreateParameter("@majorObservations", adVarChar, adParamInput, 1000, majorObservations)
				'			   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000,loc)
				'			   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
				'			   .CommandType = adCmdStoredProc
				'			End With
				'			
				'			Set rs3 = oCmd4.Execute
				'			
				'		end if
						
'						end if
					end if
					
					'update the location table with the new comments/majorObservations
					Set oCmd = Server.CreateObject("ADODB.Command")
							
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spEditMajorObservations"
					   .parameters.Append .CreateParameter("@locationID", adInteger, adParamInput, 8, locationID)
					   .parameters.Append .CreateParameter("@majorObservations", adVarChar, adParamInput, 5000, majorObservations)
					   .parameters.Append .CreateParameter("@inspected", adBoolean, adParamInput, 1, inspected)
					   
					   .CommandType = adCmdStoredProc
					End With
					
					Set rsDateIns = oCmd.Execute
				
				
				end if
			end if
		Next
		
		'loop through the form fields and update the reportAnswers table with the location info.
		'need reportID and reference number
		'keep the item open by inserting 0 into isClosed field
		For Each Item in Request.Form
			if 	Left(item,9) = "location_" then
				sRefNumb = Replace(item,"location_", "")
				
			'	response.Write trim(Request.Form(Item)) & "<br>"
				
				'insert the data into the database
				Set oCmd4 = Server.CreateObject("ADODB.Command")
		
				With oCmd4
				   .ActiveConnection = DataConn
				   .CommandText = "spAddLocation" 'is an update query
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNumb)
			   	   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000, trim(Request.Form(Item)))
				   .CommandType = adCmdStoredProc
				End With
			
				Set rs3 = oCmd4.Execute
			
			end if
		Next
		
		
		
		
		
		'add the data to the water quality monitoring table
'		For i = 1 to 1
		
		'response.Write i & "<br>"
		
'			samplerID = checknull(request("samplerID" & i))
'			GCSamplerID = null'checknull(request("GCSamplerID" & i))
'			samplerType = null'checknull(request("samplerType" & i))
'			dateEndClearingGrubbing = checknull(request("dateEndClearingGrubbing" & i))
'			dateSamplerInstalled = null'checknull(request("dateSamplerInstalled" & i))
'			dateSampleTakenAfterClearGrub = checknull(request("dateSampleTakenAfterClearGrub" & i))
'			dateEndGrading = checknull(request("dateEndGrading" & i))
'			dateAfterEndGradingorNinety = checknull(request("dateAfterEndGradingorNinety" & i))
			'BMPInstalledMaintRetrieved = checknull(request("BMPInstalledMaintRetrieved" & i))
'			dateSamplerRemoved = null'checknull(request("dateSamplerRemoved" & i))
'			rainDateEvent = checknull(request("rainDateEvent" & i))
'			rainAmount = checknull(request("rainAmount" & i))
'			codeCol1 = checknull(request("codeCol1" & i))
'			codeCol2 = checknull(request("codeCol2" & i))
'			codeCol3 = checknull(request("codeCol3" & i))
'			codeCol4 = checknull(request("codeCol4" & i))
'			codeCol5 = checknull(request("codeCol5" & i))
'			codeCol6 = checknull(request("codeCol6" & i))
'			codeCol7 = checknull(request("codeCol7" & i))
'			codeCol8 = checknull(request("codeCol8" & i))
'			codeCol9 = checknull(request("codeCol9" & i))
'			codeCol10 = checknull(request("codeCol10" & i))
'
'			samplingRequired = checknull(request("samplingRequired" & i))
'			samplingComplete = checknull(request("samplingComplete" & i))
			
						
'			Set oCmd = Server.CreateObject("ADODB.Command")
'			With oCmd
'			   .ActiveConnection = DataConn
'			   .CommandText = "spAddMonthlyRainData"
'			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
'			   .parameters.Append .CreateParameter("@samplerID", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@GCSamplerID", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@samplerType", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateEndClearingGrubbing", adVarChar, adParamInput, 50, null)			   
'			   .parameters.Append .CreateParameter("@dateSamplerInstalled", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateSampleTakenAfterClearGrub", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateEndGrading", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateAfterEndGradingorNinety", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@BMPInstalledMaintRetrieved", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateSamplerRemoved", adVarChar, adParamInput, 50, null)
'			   
'			   .parameters.Append .CreateParameter("@rainDateEvent", adVarChar, adParamInput, 50, rainDateEvent)
'			   .parameters.Append .CreateParameter("@rainAmount", adVarChar, adParamInput, 50, rainAmount)
'			   .parameters.Append .CreateParameter("@codeCol1", adVarChar, adParamInput, 50, codeCol1)
'			   .parameters.Append .CreateParameter("@codeCol2", adVarChar, adParamInput, 50, codeCol2)
'			   .parameters.Append .CreateParameter("@codeCol3", adVarChar, adParamInput, 50, codeCol3)
'			   .parameters.Append .CreateParameter("@codeCol4", adVarChar, adParamInput, 50, codeCol4)
'			   .parameters.Append .CreateParameter("@codeCol5", adVarChar, adParamInput, 50, codeCol5)
'			   .parameters.Append .CreateParameter("@codeCol6", adVarChar, adParamInput, 50, codeCol6)
'			   .parameters.Append .CreateParameter("@codeCol7", adVarChar, adParamInput, 50, codeCol7)
'			   .parameters.Append .CreateParameter("@codeCol8", adVarChar, adParamInput, 50, codeCol8)
'			   .parameters.Append .CreateParameter("@codeCol9", adVarChar, adParamInput, 50, codeCol9)
'			   .parameters.Append .CreateParameter("@codeCol10", adVarChar, adParamInput, 50, codeCol10)
'			   .parameters.Append .CreateParameter("@samplingRequired ", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@samplingComplete", adVarChar, adParamInput, 50, null)
'			   .CommandType = adCmdStoredProc
'			End With
			
'			Set rs = oCmd.Execute
'			Set oCmd = nothing
'		
'		next

		
		
		
		
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if

		'Report and PDF document created
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"
	
	
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildUtilBWPRPDF.asp?divisionID=" & division & "&reportID=" & reportID', "landscape=true"				
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
	
		'send auto email if true for project
		If autoEmail(project) = true then
			sendAutoEmail reportID,project,division
		end if

		response.Redirect "reportList.asp?findReport=" & reportID
	
	Case "editUtilBWPR"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		division = request("division")
		reportID = request("reportID")
		comments = request("comments")
		rainfallAmount = request("rainfallAmount")
		inspectionDate = checknull(request("inspectionDate"))
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If

		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		'***delete the previous entries in the reportanswers and responsiveaction tables
	'	Set oCmd = Server.CreateObject("ADODB.Command")
	'	With oCmd
	'	   .ActiveConnection = DataConn
	'	   .CommandText = "spDeleteReportAnswers"
	'	   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
	'	   .CommandType = adCmdStoredProc
	'	End With					
	'	Set rs = oCmd.Execute
	'	Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteResponsiveActions"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With					
		Set rs = oCmd.Execute
		Set oCmd = nothing
		'rs.close
		'set rs = nothing		
		
		
		'***add the answers to the questions

		For Each Item in Request.Form
			if Left(item,6) = "answer" then				
				
				'for each no answer, add the responsive action needed
				if 	Left(item,8) = "answer_R" then
					'reportID get from above
			
					'questionID get from srting
					iLen1 = len(item)
					iLen2 = InStr(item, "@")
					quesID = Right(item,iLen1 - iLen2)
					'response.Write "question " & quesID & "<br>"
					
					'reference number
					sText = Replace(item,"answer_R", "")
					iLen3 = InStr(sText, "@")
					iLen4 = len(sText)
					sText = Left(sText,iLen3)
					sRefNum = Replace(sText,"@", "")
					
					'action needed
					sActionNeeded = trim(Request.Form(Item))
					
					installedCorrectly = isOn(request("installed" & sRefNum))
					majorObservations = checknull(request("observations" & sRefNum))
					proposedActivity = checknull(request("proposedActivity" & sRefNum))
					dateInstalled = checknull(request("dateInstalled" & sRefNum))
					
					inspected = isOn(request("inspected" & sRefNum))
					
					locationID = checknull(request("locationID" & sRefNum))
				
					If Err Then
				%>
				1		<!--#include file="includes/FatalError.inc"-->
				<%
					End If
					
					'if the dateInstalled <> "" then update the location
					
					if dateInstalled <> "" then
					
						
						
						
					
						Set oCmd = Server.CreateObject("ADODB.Command")
							
						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spEditDateInstalled"
						   .parameters.Append .CreateParameter("@locationID", adInteger, adParamInput, 8, locationID)
						   .parameters.Append .CreateParameter("@dateInstalled", adDBTimeStamp, adParamInput, 8, dateInstalled)
						   .CommandType = adCmdStoredProc
						End With
						
						Set rsDateIns = oCmd.Execute
					
					end if
					
					If Err Then
				%>
				2		<!--#include file="includes/FatalError.inc"-->
				<%
					End If
					
				'	response.Write sActionNeeded & "<br>"
				'	response.Write sRefNum & "<br>"
				'	response.Write installedCorrectly & "<br>"
					
					If 	sActionNeeded <> "" then
'						if 	installedCorrectly = "True" then
						
							'get the insspector name
'							Set oCmd7 = Server.CreateObject("ADODB.Command")							
'							With oCmd7
'							   .ActiveConnection = DataConn
'							   .CommandText = "spGetUser"
'							   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
'							   .CommandType = adCmdStoredProc
'							End With
'							
'							Set rsInspector = oCmd7.Execute
'							
'							addressedBy = rsInspector("firstName") & " " & rsInspector("lastName")
'				
'							if installedCorrectly = "True" then
'								isClosed = "True"
'							else
'								isClosed = "False"
'							end if
'							
'							'insert the data into the database
'							Set oCmd3 = Server.CreateObject("ADODB.Command")
'							
'							With oCmd3
'							   .ActiveConnection = DataConn
'							   .CommandText = "spAddResponsiveActionsInstalled"
'							   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
'							   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
'							   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
'							   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, sActionNeeded)
'							   .parameters.Append .CreateParameter("@majorObservations", adVarChar, adParamInput, 1000, majorObservations)
'							   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, isClosed)
'							   .parameters.Append .CreateParameter("@installedCorrectly", adBoolean, adParamInput, 1, installedCorrectly)
'							   .parameters.Append .CreateParameter("@addressedBy", adVarChar, adParamInput, 50, addressedBy)'addressedBy
'							   .parameters.Append .CreateParameter("@addressedByDate", adDBTimeStamp, adParamInput, 8, now())'now()
'							   .parameters.Append .CreateParameter("@proposedActivity", adVarChar, adParamInput, 500, proposedActivity)
'							   .parameters.Append .CreateParameter("@dateInstalled", adDBTimeStamp, adParamInput, 8, dateInstalled)
'							   .CommandType = adCmdStoredProc
'							End With
'							
'							Set rs2 = oCmd3.Execute
							
'						else	
							'insert the data into the database
							Set oCmd3 = Server.CreateObject("ADODB.Command")
							
							With oCmd3
							   .ActiveConnection = DataConn
							   .CommandText = "spAddResponsiveActionsUtil"
							   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
							   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
							   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
							   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, sActionNeeded)
							   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
							   .parameters.Append .CreateParameter("@majorObservations", adVarChar, adParamInput, 5000, majorObservations)
							   .parameters.Append .CreateParameter("@proposedActivity", adVarChar, adParamInput, 500, proposedActivity)
							   .parameters.Append .CreateParameter("@dateInstalled", adDBTimeStamp, adParamInput, 8, dateInstalled)
							   .CommandType = adCmdStoredProc
							End With
							
							Set rs2 = oCmd3.Execute
							
				'	else
						'add the comments only to new table
						
				'		if majorObservations <> "" then
				'			loc = checknull(request("location_" & sRefNum))
				'			
				'			Set oCmd4 = Server.CreateObject("ADODB.Command")
				'			
				'			With oCmd4
				'			   .ActiveConnection = DataConn
				'			   .CommandText = "spAddMajorObservations"
				'			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				'			   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
				'			   .parameters.Append .CreateParameter("@majorObservations", adVarChar, adParamInput, 1000, majorObservations)
				'			   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000,loc)
				'			   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
				'			   .CommandType = adCmdStoredProc
				'			End With
				'			
				'			Set rs3 = oCmd4.Execute
				'			
				'		end if
						
'						end if
					end if
					
					'update the location table with the new comments/majorObservations
					Set oCmd = Server.CreateObject("ADODB.Command")
							
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spEditMajorObservations"
					   .parameters.Append .CreateParameter("@locationID", adInteger, adParamInput, 8, locationID)
					   .parameters.Append .CreateParameter("@majorObservations", adVarChar, adParamInput, 5000, majorObservations)
					   .parameters.Append .CreateParameter("@inspected", adBoolean, adParamInput, 1, inspected)
					   
					   .CommandType = adCmdStoredProc
					End With
					
					Set rsDateIns = oCmd.Execute
				
				
				end if
			end if
		Next
		
		'loop through the form fields and update the reportAnswers table with the location info.
		'need reportID and reference number
		'keep the item open by inserting 0 into isClosed field
		For Each Item in Request.Form
			if 	Left(item,9) = "location_" then
				sRefNumb = Replace(item,"location_", "")
				
			'	response.Write trim(Request.Form(Item)) & "<br>"
				
				'insert the data into the database
				Set oCmd4 = Server.CreateObject("ADODB.Command")
		
				With oCmd4
				   .ActiveConnection = DataConn
				   .CommandText = "spAddLocation" 'is an update query
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNumb)
			   	   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000, trim(Request.Form(Item)))
				   .CommandType = adCmdStoredProc
				End With
			
				Set rs3 = oCmd4.Execute
			
			end if
		Next
		
		
		
		
		'delete the data from the monthly rain data table
'		Set oCmd = Server.CreateObject("ADODB.Command")
		
'		With oCmd
'		   .ActiveConnection = DataConn
'		   .CommandText = "spDeleteMonthlyRainData"
'		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
'		   .CommandType = adCmdStoredProc
'		End With
'				
'		Set rsDeleteRainData = oCmd.Execute
'		Set oCmd = nothing
'		
'		
'		'add the data to the water quality monitoring table
'		For i = 1 to 10
'		
'		'response.Write i & "<br>"
'		
'			samplerID = checknull(request("samplerID" & i))
'			GCSamplerID = null'checknull(request("GCSamplerID" & i))
'			samplerType = null'checknull(request("samplerType" & i))
'			dateEndClearingGrubbing = checknull(request("dateEndClearingGrubbing" & i))
'			dateSamplerInstalled = null'checknull(request("dateSamplerInstalled" & i))
'			dateSampleTakenAfterClearGrub = checknull(request("dateSampleTakenAfterClearGrub" & i))
'			dateEndGrading = checknull(request("dateEndGrading" & i))
'			dateAfterEndGradingorNinety = checknull(request("dateAfterEndGradingorNinety" & i))
'			'BMPInstalledMaintRetrieved = checknull(request("BMPInstalledMaintRetrieved" & i))
'			dateSamplerRemoved = null'checknull(request("dateSamplerRemoved" & i))
'			rainDateEvent = checknull(request("rainDateEvent" & i))
'			rainAmount = checknull(request("rainAmount" & i))
'			codeCol1 = checknull(request("codeCol1" & i))
'			codeCol2 = checknull(request("codeCol2" & i))
'			codeCol3 = checknull(request("codeCol3" & i))
'			codeCol4 = checknull(request("codeCol4" & i))
'			codeCol5 = checknull(request("codeCol5" & i))
'			codeCol6 = checknull(request("codeCol6" & i))
'			codeCol7 = checknull(request("codeCol7" & i))
'			codeCol8 = checknull(request("codeCol8" & i))
'			codeCol9 = checknull(request("codeCol9" & i))
'			codeCol10 = checknull(request("codeCol10" & i))
'
'			samplingRequired = checknull(request("samplingRequired" & i))
'			samplingComplete = checknull(request("samplingComplete" & i))
'
'			
'			
'						
'			Set oCmd = Server.CreateObject("ADODB.Command")
'			With oCmd
'			   .ActiveConnection = DataConn
'			   .CommandText = "spAddMonthlyRainData"
'			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
'			   .parameters.Append .CreateParameter("@samplerID", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@GCSamplerID", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@samplerType", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateEndClearingGrubbing", adVarChar, adParamInput, 50, null)			   
'			   .parameters.Append .CreateParameter("@dateSamplerInstalled", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateSampleTakenAfterClearGrub", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateEndGrading", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateAfterEndGradingorNinety", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@BMPInstalledMaintRetrieved", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@dateSamplerRemoved", adVarChar, adParamInput, 50, null)
'			   
'			   .parameters.Append .CreateParameter("@rainDateEvent", adVarChar, adParamInput, 50, rainDateEvent)
'			   .parameters.Append .CreateParameter("@rainAmount", adVarChar, adParamInput, 50, rainAmount)
'			   .parameters.Append .CreateParameter("@codeCol1", adVarChar, adParamInput, 50, codeCol1)
'			   .parameters.Append .CreateParameter("@codeCol2", adVarChar, adParamInput, 50, codeCol2)
'			   .parameters.Append .CreateParameter("@codeCol3", adVarChar, adParamInput, 50, codeCol3)
'			   .parameters.Append .CreateParameter("@codeCol4", adVarChar, adParamInput, 50, codeCol4)
'			   .parameters.Append .CreateParameter("@codeCol5", adVarChar, adParamInput, 50, codeCol5)
'			   .parameters.Append .CreateParameter("@codeCol6", adVarChar, adParamInput, 50, codeCol6)
'			   .parameters.Append .CreateParameter("@codeCol7", adVarChar, adParamInput, 50, codeCol7)
'			   .parameters.Append .CreateParameter("@codeCol8", adVarChar, adParamInput, 50, codeCol8)
'			   .parameters.Append .CreateParameter("@codeCol9", adVarChar, adParamInput, 50, codeCol9)
'			   .parameters.Append .CreateParameter("@codeCol10", adVarChar, adParamInput, 50, codeCol10)
'			   .parameters.Append .CreateParameter("@samplingRequired", adVarChar, adParamInput, 50, null)
'			   .parameters.Append .CreateParameter("@samplingComplete", adVarChar, adParamInput, 50, null)
'			   .CommandType = adCmdStoredProc
'			End With
'			
'			Set rs = oCmd.Execute
'			Set oCmd = nothing
'		
'		next
		
		
		
		
		
		
		
		
		

		'Report and PDF document created
		logDetails Session("ID"),reportID,"Report Edited","Report and PDF document Edited"
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildUtilBWPRPDF.asp?divisionID=" & division & "&reportID=" & reportID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )

		response.Redirect "reportList.asp?findReport=" & reportID
	

	Case "addReport"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		division = request("division")
		project = request("project")
		inspector = request("inspector")
		contactNumber = request("contactNumber")
		inspectionType = request("inspectionType")
		inspectionDate = request("inspectionDate")
		weatherType = request("weatherType")
		comments = request("comments")
		reportType = request("reportType")
		projectStatus = null'request("projectStatus")
		rainfallAmount = request("rainfallAmount")
		
		addToJournal = request("addToJournal")
		
		'this is to determine which PDF to generate
		buildPDFType = request("buildPDFType")
		
		clientID = Session("clientID")
		
		soilStabilizationComments = checknull(request("soilStabilizationComments"))
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")

			
			
		'get the last inspectionDate
		Set oCmd4 = Server.CreateObject("ADODB.Command")
	
		With oCmd4
		   .ActiveConnection = DataConn
		   .CommandText = "spGetLastInspectionDate"
			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, project)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		If Err Then
		%>
			15<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set rsLastDate = oCmd4.Execute
		Set oCmd4 = nothing
		
		If Err Then
		%>
			16<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		if not rsLastDate.eof then
			If isnull(rsLastDate("lastInspectionDate")) then				
				lastInspectionDate = null
				'response.Write "d " & lastInspectionDate
			else
				lastInspectionDate = rsLastDate("lastInspectionDate")
			end if
		end if
		
		
		
		rsLastDate.close
		set rsLastDate = nothing
		Set oCmd4 = nothing
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReport"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, project)
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
		   .parameters.Append .CreateParameter("@contactNumber", adVarChar, adParamInput, 50, contactNumber)
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionType)
		   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, inspectionDate)
		   .parameters.Append .CreateParameter("@weatherTypeID", adInteger, adParamInput, 8, weatherType)
		   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, lastInspectionDate)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportType) 'add later
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatus)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .parameters.Append .CreateParameter("@soilStabilizationComments", adVarChar, adParamInput, 8000, soilStabilizationComments)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			13<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
		141	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the report id that we just created
		reportID = rs("Identity")
		
		If Err Then
		%>
		14	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'add the answers to the questions

		For Each Item in Request.Form
			if Left(item,6) = "answer" then
				
				'if the is no underscore, then write the data
				if Left(item,8) = "answer_&" then
					'insert the answers into the report answers table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					If Err Then
					%>
					10	<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddReportAnswers"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, cint(replace(Item,"answer_&","")))
					   .parameters.Append .CreateParameter("@answer", adVarChar, adParamInput, 50, trim(Request.Form(Item)))
					   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, now())
					   .CommandType = adCmdStoredProc
					End With
					
					If Err Then
					%>
					9	<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					Set rs = oCmd2.Execute
					
					If Err Then
					%>
					8	<!--#include file="includes/FatalError.inc"-->
					<%
					End If
				end if
				
				
				'for each no answer, add the responsive action needed
				if 	Left(item,8) = "answer_R" then
					'reportID get from above
			
					'questionID get from srting
					iLen1 = len(item)
					iLen2 = InStr(item, "@")
					quesID = Right(item,iLen1 - iLen2)
					'response.Write "question " & quesID & "<br>"
					
					'reference number
					sText = Replace(item,"answer_R", "")
					iLen3 = InStr(sText, "@")
					iLen4 = len(sText)
					sText = Left(sText,iLen3)
					sRefNum = Replace(sText,"@", "")
					
					'action needed
					sActionNeeded = trim(Request.Form(Item))
					
					If 	sActionNeeded <> "" then			
						'insert the data into the database
						Set oCmd3 = Server.CreateObject("ADODB.Command")
						If Err Then
						%>
							18<!--#include file="includes/FatalError.inc"-->
						<%
						End If
						
						With oCmd3
						   .ActiveConnection = DataConn
						   .CommandText = "spAddResponsiveActions"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
						   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
						   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
						   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, sActionNeeded)
						   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
						   .CommandType = adCmdStoredProc
						End With
						
						If Err Then
						%>
					6		<!--#include file="includes/FatalError.inc"-->
						<%
						End If
						
						Set rs2 = oCmd3.Execute
						
						If Err Then
						%>
					7		<!--#include file="includes/FatalError.inc"-->
						<%
						End If
					end if
					
				
				end if
			end if
		Next
		
		'loop through the form fields and update the reportAnswers table with the location info.
		'need reportID and reference number
		'keep the item open by inserting 0 into isClosed field
		For Each Item in Request.Form
			if 	Left(item,9) = "location_" then
				sRefNumb = Replace(item,"location_", "")
				
				'insert the data into the database
				Set oCmd4 = Server.CreateObject("ADODB.Command")
			
				With oCmd4
				   .ActiveConnection = DataConn
				   .CommandText = "spAddLocation" 'is an update query
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNumb)
				   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000, trim(Request.Form(Item)))
				   .CommandType = adCmdStoredProc
				End With
			
				Set rs3 = oCmd4.Execute
				
			end if
		Next
		
		
		'**********************************************
		'St Louis County Form
		countyID = request("countyID")
		addToReport = request("addToReport")
		
		if countyID = 1561 then 'st louis county			
			'if addToReport is checked then add this form
			if addToReport = "on" then
				
				'items on both forms
				attachAProjectName = checkNull(request("attachAProjectName"))
				attachAPermitNumber = checkNull(request("attachAPermitNumber"))
				attachAInspectionDate = checkNull(request("attachAInspectionDate"))
				attachAInspectedByCompanyName = checkNull(request("attachAInspectedByCompanyName"))
				attachAInspectedByIndividualName = checkNull(request("attachAInspectedByIndividualName"))
				attachAInspectedByPhoneNumber = checkNull(request("attachAInspectedByPhoneNumber"))
				attachAInspectedByPhoneEmergency = checkNull(request("attachAInspectedByPhoneEmergency"))
				attachAInspectedByEmail = checkNull(request("attachAInspectedByEmail"))
				attachAInspectedByFax = checkNull(request("attachAInspectedByFax"))
				
				furtherConcerns = request("furtherConcerns")
				weeklyPrintedName = checkNull(request("weeklyPrintedName"))
				weeklyCompany = checkNull(request("weeklyCompany"))
				weeklyDate = checkNull(request("weeklyDate"))
				
				'weekly report items
				itemSedimentLeaving = request("itemSedimentLeaving")
				itemSedimentLeavingSiteInCompliance = request("itemSedimentLeavingSiteInCompliance")
				itemSedimentLeavingCorrectionRequired = request("itemSedimentLeavingCorrectionRequired")
				itemSedimentLeavingDeficienciesCorrectedNotCorrected = request("itemSedimentLeavingDeficienciesCorrectedNotCorrected")
				itemSedimentLeavingDeficienciesCorrected = request("itemSedimentLeavingDeficienciesCorrected")
				itemSedimentLeavingDeficienciesNotCorrected = request("itemSedimentLeavingDeficienciesNotCorrected")
				itemOffSiteErosion = request("itemOffSiteErosion")
				itemOffSiteErosionInCompliance = request("itemOffSiteErosionInCompliance")
				itemOffSiteErosionCorrectionRequired = request("itemOffSiteErosionCorrectionRequired")
				itemOffSiteErosionDeficienciesCorrectedNotCorrected = request("itemOffSiteErosionDeficienciesCorrectedNotCorrected")
				itemOffSiteErosionDeficienciesCorrected = request("itemOffSiteErosionDeficienciesCorrected")
				itemOffSiteErosionDeficienciesNotCorrected = request("itemOffSiteErosionDeficienciesNotCorrected")
				itemProtection = request("itemProtection")
				itemProtectionInCompliance = request("itemProtectionInCompliance")
				itemProtectionCorrectionRequired = request("itemProtectionCorrectionRequired")
				itemProtectionDeficienciesCorrectedNotCorrected = request("itemProtectionDeficienciesCorrectedNotCorrected")
				itemProtectionDeficienciesCorrected = request("itemProtectionDeficienciesCorrected")
				itemProtectionDeficienciesNotCorrected = request("itemProtectionDeficienciesNotCorrected")
				itemMud = request("itemMud")
				itemMudInCompliance = request("itemMudInCompliance")
				itemMudCorrectionRequired = request("itemMudCorrectionRequired")
				itemMudDeficienciesCorrectedNotCorrected = request("itemMudDeficienciesCorrectedNotCorrected")
				itemMudDeficienciesCorrected = request("itemMudDeficienciesCorrected")
				itemMudDeficienciesNotCorrected = request("itemMudDeficienciesNotCorrected")
				itemInstallation = request("itemInstallation")
				itemInstallationInCompliance = request("itemInstallationInCompliance")
				itemInstallationCorrectionRequired = request("itemInstallationCorrectionRequired")
				itemInstallationDeficienciesCorrectedNotCorrected = request("itemInstallationDeficienciesCorrectedNotCorrected")
				itemInstallationDeficienciesCorrected = request("itemInstallationDeficienciesCorrected")
				itemInstallationDeficienciesNotCorrected = request("itemInstallationDeficienciesNotCorrected")
				
				
				'post-rain report items
				relatedToHeavyRain = request("relatedToHeavyRain")
				heavyRainDate = checkNull(request("heavyRainDate"))
				relatedToCompliant = request("relatedToCompliant")
				wereInCompliance = request("wereInCompliance")
				wereNotInCompliance = request("wereNotInCompliance")
				correctionDate = checkNull(request("correctionDate"))
				describeDeficiencies = checkNull(request("describeDeficiencies"))
				previousReportDate = checkNull(request("previousReportDate"))
				wereProperlyCorrected = request("wereProperlyCorrected")
				wereNotProperlyCorrected = request("wereNotProperlyCorrected")
				
				
				'add the items to the table based on the reporttype
				select case reportType
					case 1
						Set oCmdStl = Server.CreateObject("ADODB.Command")			
						With oCmdStl
						   .ActiveConnection = DataConn
						   .CommandText = "spAddSTLCountyWeekly"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
						   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
						   .parameters.Append .CreateParameter("@attachAProjectName", adVarChar, adParamInput, 50, attachAProjectName)
						   .parameters.Append .CreateParameter("@attachAPermitNumber", adVarChar, adParamInput, 50, attachAPermitNumber)
						   .parameters.Append .CreateParameter("@attachAInspectionDate", adDBTimeStamp, adParamInput, 8, attachAInspectionDate)
						   .parameters.Append .CreateParameter("@attachAInspectedByCompanyName", adVarChar, adParamInput, 50, attachAInspectedByCompanyName)
						   .parameters.Append .CreateParameter("@attachAInspectedByIndividualName", adVarChar, adParamInput, 50, attachAInspectedByIndividualName)
						   .parameters.Append .CreateParameter("@attachAInspectedByPhoneNumber", adVarChar, adParamInput, 50, attachAInspectedByPhoneNumber)
						   .parameters.Append .CreateParameter("@attachAInspectedByPhoneEmergency", adVarChar, adParamInput, 50, attachAInspectedByPhoneEmergency)
						   .parameters.Append .CreateParameter("@attachAInspectedByEmail", adVarChar, adParamInput, 100, attachAInspectedByEmail)
						   .parameters.Append .CreateParameter("@attachAInspectedByFax", adVarChar, adParamInput, 50, attachAInspectedByFax)						   
						   .parameters.Append .CreateParameter("@furtherConcerns", adBoolean, adParamInput, 1, isOn(furtherConcerns))
						   .parameters.Append .CreateParameter("@weeklyPrintedName", adVarChar, adParamInput, 50, weeklyPrintedName)
						   .parameters.Append .CreateParameter("@weeklyCompany", adVarChar, adParamInput, 50, weeklyCompany)
						   .parameters.Append .CreateParameter("@weeklyDate", adDBTimeStamp, adParamInput, 8, weeklyDate)
						   'the below are for the weekly report						   
						   .parameters.Append .CreateParameter("@itemSedimentLeaving", adBoolean, adParamInput, 1, isOn(itemSedimentLeaving))
						   .parameters.Append .CreateParameter("@itemSedimentLeavingSiteInCompliance", adBoolean, adParamInput, 1, isOn(itemSedimentLeavingSiteInCompliance))
						   .parameters.Append .CreateParameter("@itemSedimentLeavingCorrectionRequired", adBoolean, adParamInput, 1, isOn(itemSedimentLeavingCorrectionRequired))
						   .parameters.Append .CreateParameter("@itemSedimentLeavingDeficienciesCorrectedNotCorrected", adBoolean, adParamInput, 1, isOn(itemSedimentLeavingDeficienciesCorrectedNotCorrected))
						   .parameters.Append .CreateParameter("@itemSedimentLeavingDeficienciesCorrected", adBoolean, adParamInput, 1, isOn(itemSedimentLeavingDeficienciesCorrected))
						   .parameters.Append .CreateParameter("@itemSedimentLeavingDeficienciesNotCorrected", adBoolean, adParamInput, 1, isOn(itemSedimentLeavingDeficienciesNotCorrected))
						   .parameters.Append .CreateParameter("@itemOffSiteErosion", adBoolean, adParamInput, 1, isOn(itemOffSiteErosion))
						   .parameters.Append .CreateParameter("@itemOffSiteErosionInCompliance", adBoolean, adParamInput, 1, isOn(itemOffSiteErosionInCompliance))
						   .parameters.Append .CreateParameter("@itemOffSiteErosionCorrectionRequired", adBoolean, adParamInput, 1, isOn(itemOffSiteErosionCorrectionRequired))
						   .parameters.Append .CreateParameter("@itemOffSiteErosionDeficienciesCorrectedNotCorrected", adBoolean, adParamInput, 1, isOn(itemOffSiteErosionDeficienciesCorrectedNotCorrected))
						   .parameters.Append .CreateParameter("@itemOffSiteErosionDeficienciesCorrected", adBoolean, adParamInput, 1, isOn(itemOffSiteErosionDeficienciesCorrected))
						   .parameters.Append .CreateParameter("@itemOffSiteErosionDeficienciesNotCorrected", adBoolean, adParamInput, 1, isOn(itemOffSiteErosionDeficienciesNotCorrected))							
						   .parameters.Append .CreateParameter("@itemProtection", adBoolean, adParamInput, 1, isOn(itemProtection))
						   .parameters.Append .CreateParameter("@itemProtectionInCompliance", adBoolean, adParamInput, 1, isOn(itemProtectionInCompliance))
						   .parameters.Append .CreateParameter("@itemProtectionCorrectionRequired", adBoolean, adParamInput, 1, isOn(itemProtectionCorrectionRequired))
						   .parameters.Append .CreateParameter("@itemProtectionDeficienciesCorrectedNotCorrected", adBoolean, adParamInput, 1, isOn(itemProtectionDeficienciesCorrectedNotCorrected))
						   .parameters.Append .CreateParameter("@itemProtectionDeficienciesCorrected", adBoolean, adParamInput, 1, isOn(itemProtectionDeficienciesCorrected))
						   .parameters.Append .CreateParameter("@itemProtectionDeficienciesNotCorrected", adBoolean, adParamInput, 1, isOn(itemProtectionDeficienciesNotCorrected))
						   .parameters.Append .CreateParameter("@itemMud", adBoolean, adParamInput, 1, isOn(itemMud))
						   .parameters.Append .CreateParameter("@itemMudInCompliance", adBoolean, adParamInput, 1, isOn(itemMudInCompliance))
						   .parameters.Append .CreateParameter("@itemMudCorrectionRequired", adBoolean, adParamInput, 1, isOn(itemMudCorrectionRequired))
						   .parameters.Append .CreateParameter("@itemMudDeficienciesCorrectedNotCorrected", adBoolean, adParamInput, 1, isOn(itemMudDeficienciesCorrectedNotCorrected))
						   .parameters.Append .CreateParameter("@itemMudDeficienciesCorrected", adBoolean, adParamInput, 1, isOn(itemMudDeficienciesCorrected))
						   .parameters.Append .CreateParameter("@itemMudDeficienciesNotCorrected", adBoolean, adParamInput, 1, isOn(itemMudDeficienciesNotCorrected))
						   .parameters.Append .CreateParameter("@itemInstallation", adBoolean, adParamInput, 1, isOn(itemInstallation))
						   .parameters.Append .CreateParameter("@itemInstallationInCompliance", adBoolean, adParamInput, 1, isOn(itemInstallationInCompliance))
						   .parameters.Append .CreateParameter("@itemInstallationCorrectionRequired", adBoolean, adParamInput, 1, isOn(itemInstallationCorrectionRequired))						   
						   .parameters.Append .CreateParameter("@itemInstallationDeficienciesCorrectedNotCorrected", adBoolean, adParamInput, 1, isOn(itemInstallationDeficienciesCorrectedNotCorrected))
						   .parameters.Append .CreateParameter("@itemInstallationDeficienciesCorrected", adBoolean, adParamInput, 1, isOn(itemInstallationDeficienciesCorrected))
						   .parameters.Append .CreateParameter("@itemInstallationDeficienciesNotCorrected", adBoolean, adParamInput, 1, isOn(itemInstallationDeficienciesNotCorrected))						   
						   .CommandType = adCmdStoredProc
						End With
					
						Set rsStl = oCmdStl.Execute
					
					case 4
					
						Set oCmdStl = Server.CreateObject("ADODB.Command")			
						With oCmdStl
						   .ActiveConnection = DataConn
						   .CommandText = "spAddSTLCountyPostRain"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
						   .parameters.Append .CreateParameter("@attachAProjectName", adVarChar, adParamInput, 50, attachAProjectName)
						   .parameters.Append .CreateParameter("@attachAPermitNumber", adVarChar, adParamInput, 50, attachAPermitNumber)
						   .parameters.Append .CreateParameter("@attachAInspectionDate", adDBTimeStamp, adParamInput, 8, attachAInspectionDate)
						   .parameters.Append .CreateParameter("@attachAInspectedByCompanyName", adVarChar, adParamInput, 50, attachAInspectedByCompanyName)
						   .parameters.Append .CreateParameter("@attachAInspectedByIndividualName", adVarChar, adParamInput, 50, attachAInspectedByIndividualName)
						   .parameters.Append .CreateParameter("@attachAInspectedByPhoneNumber", adVarChar, adParamInput, 50, attachAInspectedByPhoneNumber)
						   .parameters.Append .CreateParameter("@attachAInspectedByPhoneEmergency", adVarChar, adParamInput, 50, attachAInspectedByPhoneEmergency)
						   .parameters.Append .CreateParameter("@attachAInspectedByEmail", adVarChar, adParamInput, 100, attachAInspectedByEmail)
						   .parameters.Append .CreateParameter("@attachAInspectedByFax", adVarChar, adParamInput, 50, attachAInspectedByFax)						   
						   .parameters.Append .CreateParameter("@furtherConcerns", adBoolean, adParamInput, 1, isOn(furtherConcerns))
						   .parameters.Append .CreateParameter("@weeklyPrintedName", adVarChar, adParamInput, 50, weeklyPrintedName)
						   .parameters.Append .CreateParameter("@weeklyCompany", adVarChar, adParamInput, 50, weeklyCompany)
						   .parameters.Append .CreateParameter("@weeklyDate", adDBTimeStamp, adParamInput, 8, weeklyDate)
						   'the below are for the post rain report						   	
							.parameters.Append .CreateParameter("@relatedToHeavyRain", adBoolean, adParamInput, 1, isOn(relatedToHeavyRain))
							.parameters.Append .CreateParameter("@heavyRainDate", adDBTimeStamp, adParamInput, 8, heavyRainDate)
							.parameters.Append .CreateParameter("@relatedToCompliant", adBoolean, adParamInput, 1, isOn(relatedToCompliant))
							.parameters.Append .CreateParameter("@wereInCompliance", adBoolean, adParamInput, 1, isOn(wereInCompliance))
							.parameters.Append .CreateParameter("@wereNotInCompliance", adBoolean, adParamInput, 1, isOn(wereNotInCompliance))
							.parameters.Append .CreateParameter("@correctionDate", adDBTimeStamp, adParamInput, 8, correctionDate)
							.parameters.Append .CreateParameter("@describeDeficiencies", adVarChar, adParamInput, 8000, describeDeficiencies)
							.parameters.Append .CreateParameter("@previousReportDate", adDBTimeStamp, adParamInput, 8, previousReportDate)
							.parameters.Append .CreateParameter("@wereProperlyCorrected", adBoolean, adParamInput, 1, isOn(wereProperlyCorrected))
							.parameters.Append .CreateParameter("@wereNotProperlyCorrected", adBoolean, adParamInput, 1, isOn(wereNotProperlyCorrected))						   					   
						   .CommandType = adCmdStoredProc
						End With
					
						Set rsStl = oCmdStl.Execute
					
				end select
				
			
			end if
		
		end if
		'**********************************************
		
		
		
		
		'if addtojournal is checked and the comments field is not blank
		'add the comments to the journal
		If addToJournal = "on" then
			if comments <> "" then
				
				userID = session("ID")
				itemName = "Report #" & reportID
				itemDay = day(date())
				itemMonth = month(date())
				itemYear = year(date())
				itemEntry = comments
				
				addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project

			end if
		end if

		
		'log the details of the report
		'now() - log the date
		'userID
		'reportid
		'Report Created
		'Report and PDF document created
		logDetails Session("ID"),reportID,"Report Created","Report and PDF document Created"
		
		
		if countyID = 1561 then 'st louis county			
			if addToReport = "on" then
			
				'create the PDF using the above report ID
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Pdf.RegKey = sRegKey
				Set Doc = Pdf.CreateDocument
				Doc.ImportFromUrl sPDFPath & "buildPDF.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
				Filename = Doc.Save( Server.MapPath("downloads/swsirTempPage1_" & reportID & ".pdf"), true )
				Set Pdf = Nothing				
				
				'create the PDF using the above report ID
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Pdf.RegKey = sRegKey
				Set Doc = Pdf.CreateDocument
				Doc.ImportFromUrl sPDFPath & "buildPDFPage2.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
				Filename = Doc.Save( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
				
				
				'get the appropriate form, weekly or post-rain
				select case reportType
					case 1
						sPDFName = "buildPDFStLouisWeekly"
					case 4
						sPDFName = "buildPDFStLouisPostRain"
				end select
				
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Pdf.RegKey = sRegKey
				Set Doc = Pdf.CreateDocument
				Doc.ImportFromUrl sPDFPath & sPDFName & ".asp?reportID=" & reportID', "landscape=true"
				Filename = Doc.Save( Server.MapPath("downloads/swsirTempPage3_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
			
				'stitch the PDF's together
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTempPage1_" & reportID & ".pdf") )
				Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf") )
				Set Doc3 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTempPage3_" & reportID & ".pdf") )
			'	Set Doc3 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTempPage3_" & reportID & ".pdf") )
				Doc1.AppendDocument Doc2
				Doc1.AppendDocument Doc3
				Filename = Doc1.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
				
			else
			
				'create the PDF using the above report ID
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Pdf.RegKey = sRegKey
				Set Doc = Pdf.CreateDocument
				Doc.ImportFromUrl sPDFPath & "buildPDF.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
				Filename = Doc.Save( Server.MapPath("downloads/swsirTemp_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
				
				'create the PDF using the above report ID
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Pdf.RegKey = sRegKey
				Set Doc = Pdf.CreateDocument
				Doc.ImportFromUrl sPDFPath & "buildPDFPage2.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
				Filename = Doc.Save( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
				
				'stitch the pdfs together
				Set Pdf = Server.CreateObject("Persits.Pdf")
				Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTemp_" & reportID & ".pdf") )
				Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf") )
				Doc1.AppendDocument Doc2
				Filename = Doc1.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
				Set Pdf = Nothing
			
			end if
		else
		
			'create the PDF using the above report ID
			Set Pdf = Server.CreateObject("Persits.Pdf")
			Pdf.RegKey = sRegKey
			Set Doc = Pdf.CreateDocument
			Doc.ImportFromUrl sPDFPath & "buildPDF.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
			Filename = Doc.Save( Server.MapPath("downloads/swsirTemp_" & reportID & ".pdf"), true )
			Set Pdf = Nothing
			
			'create the PDF using the above report ID
			Set Pdf = Server.CreateObject("Persits.Pdf")
			Pdf.RegKey = sRegKey
			Set Doc = Pdf.CreateDocument
			Doc.ImportFromUrl sPDFPath & "buildPDFPage2.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
			Filename = Doc.Save( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf"), true )
			Set Pdf = Nothing
			
			'stitch the pdfs together
			Set Pdf = Server.CreateObject("Persits.Pdf")
			Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTemp_" & reportID & ".pdf") )
			Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf") )
			Doc1.AppendDocument Doc2
			Filename = Doc1.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
			Set Pdf = Nothing
		
		end if

		'send auto email if true for project
		If autoEmail(project) = true then
			'response.Write reportID
			sendAutoEmail reportID,project,division
		end if

		response.Redirect "reportList.asp?findReport=" & reportID
	
	Case "editReport"
		'loop through all of the answers and write them to the database
		'add the report details to the report page
		division = request("division")
		reportID = request("reportID")
		comments = request("comments")
		rainfallAmount = request("rainfallAmount")
		projectID = request("projectID")
		
		clientID = session("clientID")
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If

		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditReport"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 800, comments)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@rainfallAmount", adDouble, adParamInput, 8, rainfallAmount)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		'***delete the previous entries in the reportanswers and responsiveaction tables
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteReportAnswers"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With					
		Set rs = oCmd.Execute
		Set oCmd = nothing
		'rs.close
		'set rs = nothing
		
		If Err Then
		%>
			ff<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteResponsiveActions"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
		   .CommandType = adCmdStoredProc
		End With					
		Set rs = oCmd.Execute
		Set oCmd = nothing
		'rs.close
		'set rs = nothing		
		
		
		'***add the answers to the questions

		For Each Item in Request.Form
			if Left(item,6) = "answer" then
				
				'if the is no underscore, then write the data
				if Left(item,8) = "answer_&" then
					'insert the answers into the report answers table				
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddReportAnswers"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, cint(replace(Item,"answer_&","")))
					   .parameters.Append .CreateParameter("@answer", adVarChar, adParamInput, 50, trim(Request.Form(Item)))
					   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, now())
					   .CommandType = adCmdStoredProc
					End With					
					Set rs = oCmd2.Execute

				end if
				
				
				'for each See Corrective Action Log answer, add the responsive action needed
				if 	Left(item,8) = "answer_R" then
					'reportID get from above
			
					'questionID get from srting
					iLen1 = len(item)
					iLen2 = InStr(item, "@")
					quesID = Right(item,iLen1 - iLen2)
					'response.Write "question " & quesID & "<br>"
					
					'reference number
					sText = Replace(item,"answer_R", "")
					iLen3 = InStr(sText, "@")
					iLen4 = len(sText)
					sText = Left(sText,iLen3)
					sRefNum = Replace(sText,"@", "")
					
					'action needed
					sActionNeeded = trim(Request.Form(Item))
									
					'insert the data into the database
					Set oCmd3 = Server.CreateObject("ADODB.Command")
					If Err Then
					%>
						18<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					With oCmd3
					   .ActiveConnection = DataConn
					   .CommandText = "spAddResponsiveActions"
					   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
					   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, quesID)
					   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNum)
					   .parameters.Append .CreateParameter("@actionNeeded", adVarChar, adParamInput, 1000, sActionNeeded)
					   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
					   .CommandType = adCmdStoredProc
					End With
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
					Set rs2 = oCmd3.Execute
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If
					
				
				end if
			end if
		Next
		
		'loop through the form fields and update the reportAnswers table with the location info.
		'need reportID and reference number
		'keep the item open by inserting 0 into isClosed field
		For Each Item in Request.Form
			if 	Left(item,9) = "location_" then
				sRefNumb = Replace(item,"location_", "")
				
				'insert the data into the database
				Set oCmd4 = Server.CreateObject("ADODB.Command")
			
				With oCmd4
				   .ActiveConnection = DataConn
				   .CommandText = "spAddLocation" 'is an update query
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
				   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNumb)
				   .parameters.Append .CreateParameter("@location", adVarChar, adParamInput, 1000, trim(Request.Form(Item)))
				   .CommandType = adCmdStoredProc
				End With
			
				Set rs3 = oCmd4.Execute
				
			end if
		Next
		
		
		'if there is a folder for this item, rename to the new id
'		For Each Item in Request.Form
'			if 	Left(item,4) = "RAID" then
'				sRefNumb = Replace(item,"RAID", "")
'				
'				'get the new id based on the reference number and the reportid
'				
'				'insert the data into the database
'				Set oCmdID = Server.CreateObject("ADODB.Command")
'			
'				With oCmdID
'				   .ActiveConnection = DataConn
'				   .CommandText = "spGetResponsiveActionByReportAndRefNum"
'				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
'				   .parameters.Append .CreateParameter("@referenceNumber", adVarChar, adParamInput, 50, sRefNumb)
'				   .CommandType = adCmdStoredProc
'				End With
			
'				Set rsID = oCmdID.Execute
'				OldRAID = trim(Request.Form(Item))
'				NewRAID = rsID("responsiveActionID")
'				sNewPath = "downloads/project_" & projectID & "/report_" & reportID & "/responsiveAction_" & NewRAID
				
'				Set fs = Server.CreateObject("Scripting.FileSystemObject")
'		
'				sPath = "downloads/project_" & projectID				
'				'if the old folder exists, rename to the new ID
'				If fs.FolderExists(server.mappath(sPath)) Then 
'					
'					sPath = "downloads/project_" & projectID & "/report_" & reportID '& "/responsiveAction_" & responsiveActionID
'					If fs.FolderExists(server.mappath(sPath)) Then 
'						
'						sPath = "downloads/project_" & projectID & "/report_" & reportID & "/responsiveAction_" & OldRAID
'						If fs.FolderExists(server.mappath(sPath)) Then 
'							
'							'rename the folder to the new ID
'							if OldRAID <> NewRAID then
'								fs.MoveFolder server.mappath(sPath), server.mappath(sNewPath)
'							end if
'							
'						end if
'						
'					end if
'					
'				end if
'				
'			end if
'		Next

		
		'log the details of the report
		'now() - log the date
		'userID
		'reportid
		'Report Created
		'Report and PDF document created
		logDetails Session("ID"),reportID,"Report Edited","Report and PDF document Edited"
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPDF.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsirTemp_" & reportID & ".pdf"), true )
		Set Pdf = Nothing
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPDFPage2.asp?divisionID=" & division & "&reportID=" & reportID & "&clientID=" & clientID, "LeftMargin=20, RightMargin=10, TopMargin=10, BottomMargin=10"
		Filename = Doc.Save( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf"), true )
		Set Pdf = Nothing
		
		'stitch the pdfs together
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Set Doc1 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTemp_" & reportID & ".pdf") )
		Set Doc2 = Pdf.OpenDocument( Server.MapPath("downloads/swsirTempPage2_" & reportID & ".pdf") )
		Doc1.AppendDocument Doc2
		Filename = Doc1.Save( Server.MapPath("downloads/swsir_" & reportID & ".pdf"), true )
		Set Pdf = Nothing

		response.Redirect "reportList.asp?findReport=" & reportID
	
	Case "closeItems"
		'get the array with all of the checkbox id's
		arrAll = split(Request.Form ("allBoxes"), ",")
		
		'get the array with the id's that were ticked
		arrVals = split(Request.Form ("closeItem"), ",")
		
		'addressedBy = session("Name")
		projectID = request("projectID")
		questionID = request("questionID")
		
		On Error Resume Next
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		
		for i = 0 to ubound(arrVals)
		
			'response.Write "ref# " & arrVals(i) & "<br>"
			'response.Write "date " & request("addressedByDate" & trim(arrVals(i))) & "<br>"
			'response.Write "init " & request("addressedBy" & trim(arrVals(i))) & "<br><br>"
			
			'update the responsiveAction table and close the open item
			'Open connection and insert into the database
				
			
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spCloseItem"
			   .parameters.Append .CreateParameter("@responsiveActionID", adInteger, adParamInput, 8, arrVals(i))
			   .parameters.Append .CreateParameter("@addressedBy", adVarChar, adParamInput, 100, request("addressedBy" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@addressedByDate", adDBTimeStamp, adParamInput, 8, request("addressedByDate" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, true)
			   .parameters.Append .CreateParameter("@closeComments", adVarChar, adParamInput, 200, request("closeComments" & trim(arrVals(i))))
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			Set oCmd = nothing
		'	rs.close
		'	set rs = nothing
			
		next
		
		'response.Write ubound(arrAll)
		for ii = 0 to ubound(arrAll)
		'	response.Write trim(arrAll(ii)) & "<br>"
		'	response.Write request("closeComments" & trim(arrAll(ii))) & "<br>"
			if request("closeComments" & trim(arrAll(ii))) <> "" then
				'update the responsive action table with the comments
				'Create command
				Set oCmd = Server.CreateObject("ADODB.Command")
					
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spUpdateCloseComments"
				   .parameters.Append .CreateParameter("@responsiveActionID", adInteger, adParamInput, 8, arrAll(ii))
				   .parameters.Append .CreateParameter("@closeComments", adVarChar, adParamInput, 200, request("closeComments" & trim(arrAll(ii))))
				   .CommandType = adCmdStoredProc
				End With
						
				Set rs = oCmd.Execute
				
				Set oCmd = nothing
			'	rs.close
			'	set rs = nothing
			end if
		next

		
		response.Redirect "openItems.asp?questionID=" & questionID & "&projectID=" & projectID
		
	Case "closeLots"
		'get the array with all of the checkbox id's
		arrAll = split(Request.Form ("allBoxes"), ",")
		
		'get the array with the id's that were ticked
		arrVals = split(Request.Form ("closeLot"), ",")
		
		'addressedBy = session("Name")
		projectID = request("projectID")
		customerID = request("customerID")
		divisionID = request("divisionID")
		
		
		for i = 0 to ubound(arrVals)
			'response.Write "ID " & trim(arrVals(i)) & "<br>"
			'response.Write "date " & request("closedByDate" & trim(arrVals(i))) & "<br>"
			'response.Write "closed by " & request("closedBy" & trim(arrVals(i))) & "<br><br>"
			
			'update the lotInventory table and close the open lot
			'Open connection and insert into the database
			On Error Resume Next
			
				Set DataConn = Server.CreateObject("ADODB.Connection") 
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If
				
				DataConn.Open Session("Connection"), Session("UserID")
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If	
			
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			'check the date
			If request("closedByDate" & trim(arrVals(i))) = "" then
				closeDate = now()
			else
				closeDate = request("closedByDate" & trim(arrVals(i)))
			end if
				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spCloseLot"
			   .parameters.Append .CreateParameter("@lotID", adInteger, adParamInput, 8, arrVals(i))
			   .parameters.Append .CreateParameter("@closedBy", adVarChar, adParamInput, 150, request("closedBy" & trim(arrVals(i))))
			   .parameters.Append .CreateParameter("@closedByDate", adDBTimeStamp, adParamInput, 8, closeDate)
			   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, true)
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
		next
		
		response.Redirect "openLots.asp?id=" & projectID & "&divisionID=" & divisionID & "&customerID=" & customerID
		
	case "addLots"
		
		projectID = request("projectID")
		customerID = request("customerID")
		divisionID = request("divisionID")
		numberOfLots = request("numberOfLots")
		phase = request("phase")
		startingLotNumber = request("startingLotNumber")
		
		endingLotNumber = cint(startingLotNumber) + cint(numberOfLots)
		
		On Error Resume Next
			
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
		
		'loop through and add the lots to the database for the selected project
		for i = startingLotNumber to endingLotNumber - 1
			
			response.Write i & "<br>"
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddLot"
			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
			   .parameters.Append .CreateParameter("@lotNumber", adInteger, adParamInput, 8, i)
			   .parameters.Append .CreateParameter("@phase", adVarChar, adParamInput, 50, phase)
			   .parameters.Append .CreateParameter("@openDate", adDBTimeStamp, adParamInput, 8, now())
			   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, false)
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
		next
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
	
		response.Redirect "openLots.asp?id=" & projectID & "&divisionID=" & divisionID & "&customerID=" & customerID
		
	case "editLot"
		lotID = request("lotID")
		customerID = request("customer")
		projectID = request("projectID")
		divisionID = request("divisionID")
		lotNumber = request("lotNumber")
		phase = request("phase")
		streetID = request("streetID")
		lotStatusID = request("lotStatusID")
		outFall = request("outFall")
		curbInlet = request("curbInlet")
		dropInlet = request("dropInlet")
		retentionPond = request("retentionPond")
		concreteWashout = request("concreteWashout")
		jobTrailer = request("jobTrailer")
		rate = request("rate")
		if rate = "" then
			rate = null
		end if

		

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditLot"
		   .parameters.Append .CreateParameter("@lotID", adInteger, adParamInput, 8, lotID)
		   .parameters.Append .CreateParameter("@lotNumber", adInteger, adParamInput, 8, lotNumber)
		   .parameters.Append .CreateParameter("@phase", adVarChar, adParamInput, 50, phase)
		   .parameters.Append .CreateParameter("@streetID", adInteger, adParamInput, 8, streetID)
		   .parameters.Append .CreateParameter("@lotStatusID", adInteger, adParamInput, 8, lotStatusID)
		   .parameters.Append .CreateParameter("@outFall", adBoolean, adParamInput, 1, isOn(outFall))		   
		   .parameters.Append .CreateParameter("@curbInlet", adBoolean, adParamInput, 1, isOn(curbInlet))
		   .parameters.Append .CreateParameter("@dropInlet", adBoolean, adParamInput, 1, isOn(dropInlet))
		   .parameters.Append .CreateParameter("@retentionPond", adBoolean, adParamInput, 1, isOn(retentionPond))
		   .parameters.Append .CreateParameter("@concreteWashout", adBoolean, adParamInput, 1, isOn(concreteWashout))
		   .parameters.Append .CreateParameter("@jobTrailer", adBoolean, adParamInput, 1, isOn(jobTrailer))
		   .parameters.Append .CreateParameter("@rate", adCurrency, adParamInput, 8, rate)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "openLots.asp?id=" & projectID & "&divisionID=" & divisionID & "&customerID=" & customerID
		
	case "deleteLot"
		lotID = Request("id")
		projectID = request("projectID")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteLot"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, lotID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "openLots.asp?id=" & projectID & "&divisionID=" & divisionID & "&customerID=" & customerID
		
	Case "addUser"
		firstName = checkNull(request("firstName"))
		lastName = checkNull(request("lastName"))
		company = checkNull(request("company"))
		jobtitle = checkNull(request("jobtitle"))
		sstate = checkNull(request("state"))
		officePhone = checkNull(request("officePhone"))
		cellPhone = checkNull(request("cellPhone"))
		email = checkNull(request("email"))
		'userName = request("userName")
		userName = checkNull(request("email"))
		password = checkNull(request("password"))
		passwordHint = checkNull(request("passwordHint"))
		userType = checkNull(request("industry"))
		interest = checkNull(request("interest"))
		associatedProjects = checkNull(request("associatedProjects"))		
		appLogo = ""
		refer = checkNull(request("refer"))
		clientID = request("clientID")
		WECSNo = checkNull(request("WECSNo"))
		WECSExpDate = checkNull(request("WECSExpDate"))
		otherCerts = checkNull(request("otherCerts"))
		GASWCCNo = checkNull(request("GASWCCNo"))
		GASWCCExpDate = checkNull(request("GASWCCExpDate"))
		
		if clientID = "" then
			clientID = 1 'means they signed up through the website. may need to be reassigned to correct client.
		end if	
		
	'	if associatedProjects = "" then
	'		associatedProjects = null
	'	end if
		
	'	if WECSExpDate = "" then
	'		WECSExpDate = null
	'	end if	
		
	'	if WECSNo = "" then
	'		WECSNo = null
	'	end if
		
	'	if GASWCCExpDate = "" then
	'		GASWCCExpDate = null
	'	end if
		
	'	if GASWCCNo = "" then
	'		GASWCCNo = null
	'	end if	
		
	'	if otherCerts = "" then
	'		otherCerts = null
	'	end if
		
		'NPDESManager = request("NPDESManager")
		'NOIManager = request("NOIManager")	
		
		
		if checkEmailAddress(email) = "false" then
		
			'Open connection and insert into the database
			On Error Resume Next
			
				Set DataConn = Server.CreateObject("ADODB.Connection") 
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If
				
				DataConn.Open Session("Connection"), Session("UserID")
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If	
			
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddUser"
			   .parameters.Append .CreateParameter("@firstName", adVarChar, adParamInput, 50, firstName)
			   .parameters.Append .CreateParameter("@lastName", adVarChar, adParamInput, 50, lastName)
			   .parameters.Append .CreateParameter("@company", adVarChar, adParamInput, 100, company)
			   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 100, email)
			   .parameters.Append .CreateParameter("@userName", adVarChar, adParamInput, 50, userName)
			   .parameters.Append .CreateParameter("@password", adVarChar, adParamInput, 50, password)
			   .parameters.Append .CreateParameter("@isActive", adBoolean, adParamInput, 1, false)
			   .parameters.Append .CreateParameter("@isAdmin", adBoolean, adParamInput, 1, false)
			   .parameters.Append .CreateParameter("@isSuperAdmin", adBoolean, adParamInput, 1, false)
			   .parameters.Append .CreateParameter("@jobtitle", adVarChar, adParamInput, 50, jobtitle)
			   .parameters.Append .CreateParameter("@officePhone", adVarChar, adParamInput, 50, officePhone)
			   .parameters.Append .CreateParameter("@cellPhone", adVarChar, adParamInput, 50, cellPhone)
			   '.parameters.Append .CreateParameter("@division", adInteger, adParamInput, 8, division)
			   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
			   .parameters.Append .CreateParameter("@appLogo", adVarChar, adParamInput, 150, appLogo)
			   .parameters.Append .CreateParameter("@signUpDate", adDBTimeStamp, adParamInput, 8, now())
			   .parameters.Append .CreateParameter("@userType", adVarChar, adParamInput, 50, userType)
			   .parameters.Append .CreateParameter("@associatedProjects", adVarChar, adParamInput, 250, associatedProjects)
			  ' .parameters.Append .CreateParameter("@NPDESManager", adBoolean, adParamInput, 1, isOn(NPDESManager))
			   '.parameters.Append .CreateParameter("@NOIManager", adBoolean, adParamInput, 1, isOn(NOIManager))
			   .parameters.Append .CreateParameter("@interest", adVarChar, adParamInput, 50, interest)
			   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
			   .parameters.Append .CreateParameter("@WECSNo", adVarChar, adParamInput, 50, WECSNo)
			   .parameters.Append .CreateParameter("@WECSExpDate", adDBTimeStamp, adParamInput, 8, WECSExpDate)
			   .parameters.Append .CreateParameter("@otherCerts", adVarChar, adParamInput, 500, otherCerts)
			   .parameters.Append .CreateParameter("@GASWCCNo", adVarChar, adParamInput, 50, GASWCCNo)
			   .parameters.Append .CreateParameter("@GASWCCExpDate", adDBTimeStamp, adParamInput, 8, GASWCCExpDate)
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
			'send email to admin for them to activate membership
			sBody = "A new user has signed up on the " & sPageTitle & "." & vbcrlf & vbcrlf
			sBody = sBody & "Name: " & firstName & " " & lastName & vbcrlf
			sBody = sBody & "Company: " & company & vbcrlf
			sBody = sBody & "Job Title: " & jobtitle & vbcrlf
			sBody = sBody & "Email: " & email & vbcrlf
			sBody = sBody & "Office Phone: " & officePhone & vbcrlf
			sBody = sBody & "Cell Phone: " & cellPhone & vbcrlf
			sBody = sBody & "User Type/Industry: " & userType & vbcrlf
			sBody = sBody & "Interest: " & interest & vbcrlf
			'If NPDESManager = "on" then
			'	sBody = sBody & "NPDES Manager: Yes" & vbcrlf
			'else
			'	sBody = sBody & "NPDES Manager: No" & vbcrlf
			'end if
			'If NOIManager = "on" then
			'	sBody = sBody & "NOI Manager: Yes" & vbcrlf
			'else
			'	sBody = sBody & "NOI Manager: No" & vbcrlf
			'end if
			sBody = sBody & "Projects/Customers: " & associatedProjects & vbcrlf
			sBody = sBody & "Country: " & country & vbcrlf
			sBody = sBody & "State: " & sstate & vbcrlf & vbcrlf
			sBody = sBody & "This user will need to be activated in the system before he/she can use it."
			'response.Write sBody
			
			'####################################################################################
			Set oCdoMail = Server.CreateObject("CDO.Message")
			Set oCdoConf = Server.CreateObject("CDO.Configuration")
			
			sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
			with oCdoConf
			  .Fields.Item(sConfURL & "sendusing") = 2
			  .Fields.Item(sConfURL & "smtpserver") = "localhost"
			  .Fields.Item(sConfURL & "smtpserverport") = 25
			  .Fields.Update
			end with
			
			sTo = sTo & getAdminEmail(clientID)
			
			with oCdoMail
			  .From = email
			  .To = sTo
			end with
			
			with oCdoMail
			  .Subject = "New " & sPageTitle & " User"
			  .TextBody = sBody
			end with
			
			oCdoMail.Configuration = oCdoConf
			oCdoMail.Send
			Set oCdoConf = Nothing
			Set oCdoMail = Nothing
			'######################################################################################
	
			response.Redirect "register_thanks.asp?refer=" & refer
			
		else
			
			If refer <> "" then
				sMsg = "<a href=" & refer & "?appLogo=" & Session("appLogo") & " class=redLink>The email address you entered is already in our system.<br> Please click here to log into your account.</a>"
			else
				sMsg = "<a href=index.asp?appLogo=" & Session("appLogo") & " class=redLink>The email address you entered is already in our system.<br> Please click here to log into your account.</a>"
			end if
			
			response.Redirect "form_addUser.asp?msg=" & sMsg & "&firstName=" & firstName & "&lastName=" & lastName & "&company=" & company & "&jobtitle=" & jobtitle & "&officePhone=" & officePhone & "&cellPhone=" & cellPhone & "&state=" & sstate
		end if
	
	
	
		
		
	Case "editUser"
		userID = request("userID")
		firstName = request("firstName")
		lastName = request("lastName")
		company = request("company")
		email = request("email")
		userName = request("userName")
		password = request("password")
		jobtitle = request("jobtitle")
		officePhone = request("officePhone")
		cellPhone = request("cellPhone")
		sstate = request("state")
		isUser = request("isUser")
		inspector = request("inspector")
		'maintenanceUser = request("maintenanceUser")
		customerAdmin = request("customerAdmin")
		isActive = request("isActive")
		isSingleProject = request("isSingleProject")
		'bmpContractor = request("bmpContractor")
		appLogo = request("appLogo")
		'division = request("division")
		'customer = request("customer")
		customerReports = null'request("customerReports")
		openCorrectedLog = null'request("openCorrectedLog")
		projectDocuments = null'request("projectDocuments")
		noiUser = null'request("noiUser")
		recieveOIAlerts = request("recieveOIAlerts")
		receiveSevenDayAlerts = request("receiveSevenDayAlerts")
		receiveFourteenDayAlerts = request("receiveFourteenDayAlerts")
		canApprove = null'request("canApprove")
		emailSignature = request("emailSignature")
		WECSNo = request("WECSNo")
		WECSExpDate = request("WECSExpDate")
		otherCerts = request("otherCerts")
		viewQuotes = request("viewQuotes")
		GASWCCNo = request("GASWCCNo")
		GASWCCExpDate = request("GASWCCExpDate")
		deleteWorkOrder = null'request("deleteWorkOrder")
		empType = null'request("empType")
		closeWorkOrder = null'request("closeWorkOrder")
		OSHAManager = null'request("OSHAManager")
		
		if WECSExpDate = "" then
			WECSExpDate = null
		end if	
		
		if GASWCCExpDate = "" then
			GASWCCExpDate = null
		end if	

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
		1		<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
		2		<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
		3	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		If isUser = "True" then
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spEditMe"
			   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
			   .parameters.Append .CreateParameter("@firstName", adVarChar, adParamInput, 50, firstName)
			   .parameters.Append .CreateParameter("@lastName", adVarChar, adParamInput, 50, lastName)
			   .parameters.Append .CreateParameter("@company", adVarChar, adParamInput, 100, company)
			   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 100, email)
			   .parameters.Append .CreateParameter("@userName", adVarChar, adParamInput, 50, userName)
			   .parameters.Append .CreateParameter("@password", adVarChar, adParamInput, 50, password)
			   .parameters.Append .CreateParameter("@jobtitle", adVarChar, adParamInput, 50, jobtitle)
			   .parameters.Append .CreateParameter("@officePhone", adVarChar, adParamInput, 50, officePhone)
			   .parameters.Append .CreateParameter("@cellPhone", adVarChar, adParamInput, 50, cellPhone)
			   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
			   .parameters.Append .CreateParameter("@appLogo", adVarChar, adParamInput, 150, appLogo)
			   .parameters.Append .CreateParameter("@emailSignature", adVarChar, adParamInput, 5000, emailSignature)
			   .parameters.Append .CreateParameter("@WECSNo", adVarChar, adParamInput, 50, WECSNo)
			   .parameters.Append .CreateParameter("@WECSExpDate", adDBTimeStamp, adParamInput, 8, WECSExpDate)
			   .parameters.Append .CreateParameter("@otherCerts", adVarChar, adParamInput, 500, otherCerts)
			   .parameters.Append .CreateParameter("@GASWCCNo", adVarChar, adParamInput, 50, GASWCCNo)
			   .parameters.Append .CreateParameter("@GASWCCExpDate", adDBTimeStamp, adParamInput, 8, GASWCCExpDate)
			   '.parameters.Append .CreateParameter("@division", adInteger, adParamInput, 8, division)
			   .CommandType = adCmdStoredProc
			End With
		else	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spEditUser"
			   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
			   .parameters.Append .CreateParameter("@firstName", adVarChar, adParamInput, 50, firstName)
			   .parameters.Append .CreateParameter("@lastName", adVarChar, adParamInput, 50, lastName)
			   .parameters.Append .CreateParameter("@company", adVarChar, adParamInput, 100, company)
			   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 100, email)
			   .parameters.Append .CreateParameter("@userName", adVarChar, adParamInput, 50, userName)
			   .parameters.Append .CreateParameter("@password", adVarChar, adParamInput, 50, password)
			   .parameters.Append .CreateParameter("@jobtitle", adVarChar, adParamInput, 50, jobtitle)
			   .parameters.Append .CreateParameter("@officePhone", adVarChar, adParamInput, 50, officePhone)
			   .parameters.Append .CreateParameter("@cellPhone", adVarChar, adParamInput, 50, cellPhone)
			   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
			   .parameters.Append .CreateParameter("@inspector", adBoolean, adParamInput, 1, isOn(inspector))
			 '  .parameters.Append .CreateParameter("@maintenanceUser", adBoolean, adParamInput, 1, isOn(maintenanceUser))
			   .parameters.Append .CreateParameter("@customerAdmin", adBoolean, adParamInput, 1, isOn(customerAdmin))
			   .parameters.Append .CreateParameter("@isActive", adBoolean, adParamInput, 1, isOn(isActive))
			   .parameters.Append .CreateParameter("@isSingleProject", adBoolean, adParamInput, 1, isOn(isSingleProject))
			  ' .parameters.Append .CreateParameter("@bmpContractor", adBoolean, adParamInput, 1, isOn(bmpContractor))
			   .parameters.Append .CreateParameter("@appLogo", adVarChar, adParamInput, 150, appLogo)
			   '.parameters.Append .CreateParameter("@division", adInteger, adParamInput, 8, division)
			   '.parameters.Append .CreateParameter("@customer", adInteger, adParamInput, 8, customer)
			   .parameters.Append .CreateParameter("@customerReports", adBoolean, adParamInput, 1, isOn(customerReports))
			   .parameters.Append .CreateParameter("@openCorrectedLog", adBoolean, adParamInput, 1, isOn(openCorrectedLog))
			   .parameters.Append .CreateParameter("@projectDocuments", adBoolean, adParamInput, 1, isOn(projectDocuments))
			   .parameters.Append .CreateParameter("@noiUser", adBoolean, adParamInput, 1, isOn(noiUser))
			   .parameters.Append .CreateParameter("@recieveOIAlerts", adBoolean, adParamInput, 1, isOn(recieveOIAlerts))
			   .parameters.Append .CreateParameter("@receiveSevenDayAlerts", adBoolean, adParamInput, 1, isOn(receiveSevenDayAlerts))
			   .parameters.Append .CreateParameter("@receiveFourteenDayAlerts", adBoolean, adParamInput, 1, isOn(receiveFourteenDayAlerts))
			   .parameters.Append .CreateParameter("@canApprove", adBoolean, adParamInput, 1, isOn(canApprove))
			   .parameters.Append .CreateParameter("@emailSignature", adVarChar, adParamInput, 5000, emailSignature)
			   .parameters.Append .CreateParameter("@WECSNo", adVarChar, adParamInput, 50, WECSNo)
			   .parameters.Append .CreateParameter("@WECSExpDate", adDBTimeStamp, adParamInput, 8, WECSExpDate)
			   .parameters.Append .CreateParameter("@otherCerts", adVarChar, adParamInput, 500, otherCerts)
			   .parameters.Append .CreateParameter("@viewQuotes", adBoolean, adParamInput, 1, isOn(viewQuotes))
			   .parameters.Append .CreateParameter("@GASWCCNo", adVarChar, adParamInput, 50, GASWCCNo)
			   .parameters.Append .CreateParameter("@GASWCCExpDate", adDBTimeStamp, adParamInput, 8, GASWCCExpDate)
			   .parameters.Append .CreateParameter("@deleteWorkOrder", adBoolean, adParamInput, 1, isOn(deleteWorkOrder))
			   .parameters.Append .CreateParameter("@empType", adInteger, adParamInput, 8, empType)
			   .parameters.Append .CreateParameter("@closeWorkOrder", adBoolean, adParamInput, 1, isOn(closeWorkOrder))
			   .parameters.Append .CreateParameter("@OSHAManager", adBoolean, adParamInput, 1, isOn(OSHAManager))
			   .CommandType = adCmdStoredProc
			End With
		end if
		
		
				
		Set rs = oCmd.Execute
		If Err Then
		%>
		5	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		If isUser = "False" then
			'unassign and reassign user to customers
			'delete all records in userCustomer table so re can reassign
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			DataConn.Open Session("Connection"), Session("UserID")
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spDeleteUserProject"
			   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, userID)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rs = oCmd.Execute
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
			'reassign the customers to the user
			for i=1 to Request("project").count
				listboxvalue=Request.form("project").item(i)
				'insert the data into the userCustomer table
				Set DataConn = Server.CreateObject("ADODB.Connection") 
				DataConn.Open Session("Connection"), Session("UserID")
				'Create command
				Set oCmd = Server.CreateObject("ADODB.Command")
				
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spAddUserProject"
				   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
				   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, listboxvalue)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rs = oCmd.Execute
				Set oCmd = nothing
				rs.close
				set rs = nothing
			next
			
			
			
			'unassign and reassign user to customers
			'delete all records in userReportType table so re can reassign
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			DataConn.Open Session("Connection"), Session("UserID")
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spDeleteUserReportTypes"
			   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, userID)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rs = oCmd.Execute
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
			'reassign the customers to the user
			for i=1 to Request("reports").count
				listboxvalue=Request.form("reports").item(i)
				'insert the data into the userCustomer table
				Set DataConn = Server.CreateObject("ADODB.Connection") 
				DataConn.Open Session("Connection"), Session("UserID")
				'Create command
				Set oCmd = Server.CreateObject("ADODB.Command")
				
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spAddUserReportType"
				   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
				   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, listboxvalue)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rs = oCmd.Execute
				Set oCmd = nothing
				rs.close
				set rs = nothing
			next
			
		end if
		
		if isUser = "False" then
			'response.Write "not user"
			response.Redirect "form.asp?id=" & userID & "&formType=editUser&sMsgAcctInfo=True&sec=acctInfo"
		else
			'response.Write "is user"
			response.Redirect "form.asp?formType=editUser&sMsgAcctInfo=True&sec=acctInfo"
		end if

	Case "activateUser"
		userID = request("id")
		email = request("email")

		'Open connection
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spActivateUser"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
				
		'Create command
		Set oCmd2 = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd2
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUser"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rsUser = oCmd2.Execute
		
		sUsername = rsUser("userName")
		sPassword = rsUser("password")
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'Set oCmd2 = nothing
		'rsUser.close
		'set rsUser = nothing
		
		
		'send an email to the user letting them know he/she is active in the system
		'Create the e-mail server object

	'	sBody = "Thanks for registering as a user of the " & sPageTitle & ". You have now been activated in the " & sPageTitle & " system. <br><br>Please follow the below link and log in with the username and password you supplied when you registered.<br><br>"
	'	sBody = sBody & "Username: " & sUsername & "<br>"
	'	sBody = sBody & "Password: " & sPassword & "<br><br>"
	'	sBody = sBody & "<a href=" & sServerURL & ">Log in</a><br><br>"
	'	sBody = sBody & "If the link above does not work, copy the link below into your web browser.<br>"
	'	sBody = sBody & sServerURL

		sName = rsUser("firstName") & " " & rsUser("lastName")
	
		sBody = "<span style=""font-family:Arial, Helvetica, sans-serif;color:#317d29"">Thanks " & sName & " for signing up as a user of Sequence! You have been activated.<br><br>"
		sBody = sBody & "Click on the following link to access your Sequence account.<br><br>"
		sBody = sBody & "<a href=" & sServerURL & ">Login</a><br><br>"
		sBody = sBody & "If the link above does not work, copy the link below into your web browser.<br><br>"
		sBody = sBody & sServerURL & "<br><br>"
		sBody = sBody & "Username: " & sUsername & "<br>"
		sBody = sBody & "Password: " & sPassword & "</span><br><br>"
		
		sBody = sBody & "<span style=""font-family:Arial, Helvetica, sans-serif""><b>""HB NEXT is the leader in internet based field technology solutions""</b><br><br>"
		sBody = sBody & "<b style=""color:#666666"">We want you to be successful in your business, so here are 3 different ways you can access our award winning support.</b><br><br>"
		sBody = sBody & "<b>#1 � Contact Us</b><br>"
		sBody = sBody & "The way to get answers to most support questions is by sending an email to our on-line customer care group. Go to <a href=http://www.hbnext.com>www.hbnext.com</a> and click on the Contact Us button at the top right of the website.<br><br>"
		sBody = sBody & "<b>#2 - Create a Support Ticket</b><br>"
		sBody = sBody & "If your question is not answered by the on-line customer care group, then the way to get help is to submit a support ticket. Once you are logged into your Sequence platform click on the Support Request in the Support or Resource Manager.<br><br>"
		sBody = sBody & "<b>#3 - Phone Support</b><br>"
		sBody = sBody & "We are also available by phone at 678-336-1357. Our phone support hours are 8AM-5PM EST, Monday-Friday.<br><br>"
		sBody = sBody & "Thank You,<br><br>"
		sBody = sBody & "Tony Middlebrooks<br>President<br>HB NEXT<br><a href=http://www.hbnext.com>www.hbnext.com</a>"
		
		'####################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
  		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
	    with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
	    end with
		
		with oCdoMail
		  .From = "support@hbnext.com"'change later to admin email acct
		  .To = email
		  '.To = "randyreynolds@bellsouth.net"
	    end with
		
		with oCdoMail
		  .Subject = sPageTitle & " Activation - Welcome"
		  .TextBody = sBody
		  .HTMLBody = sBody
	    end with
		
		oCdoMail.Configuration = oCdoConf
	    oCdoMail.Send
	    Set oCdoConf = Nothing
	    Set oCdoMail = Nothing
		'######################################################################################	
		
		Set oCmd = nothing
		rs.close
		set rs = nothing	
		
		response.Redirect "userList.asp"
	
	case "editQuestion"
	
		questionID = request("questionID")
		secondLanguage = request("secondLanguage")
		'referenceLetter = request("referenceLetter")
		question = request("question")
		sortNumber = request("sortNumber")
		categoryID = request("categoryID")
		customerID = request("customerID")
		weekly =  request("weekly")
		monthly =  request("monthly")
		postRain =  request("postRain")
		divisionID = request("divisionID")
		'biweekly =  request("biweekly")
		annual =  request("annual")
		petroleum =  request("petroleum")
		questionType = request("questionType")
		UtilBWPR = request("UtilBWPR")
		
		'arrAll = split(Request.Form ("allBoxes"), ",")
		'arrVals = split(Request.Form ("reportType"), ",")
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditQuestion"
		   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, questionID)
		   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
		   '.parameters.Append .CreateParameter("@referenceLetter", adChar, adParamInput, 10, referenceLetter)
		   .parameters.Append .CreateParameter("@question", adVarChar, adParamInput, 1000, question)
		   .parameters.Append .CreateParameter("@secondLanguage", adVarChar, adParamInput, 1000, secondLanguage)
		   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, sortNumber)
		   .parameters.Append .CreateParameter("@weekly", adBoolean, adParamInput, 1, isOn(weekly))
		   .parameters.Append .CreateParameter("@monthly", adBoolean, adParamInput, 1, isOn(monthly))	
		   .parameters.Append .CreateParameter("@postRain", adBoolean, adParamInput, 1, isOn(postRain))
		   '.parameters.Append .CreateParameter("@biweekly", adBoolean, adParamInput, 1, isOn(biweekly))	
		   .parameters.Append .CreateParameter("@annual", adBoolean, adParamInput, 1, isOn(annual))	
		   .parameters.Append .CreateParameter("@questionType", adInteger, adParamInput, 8, questionType)
		   .parameters.Append .CreateParameter("@petroleum", adBoolean, adParamInput, 1, isOn(petroleum))	
		   .parameters.Append .CreateParameter("@UtilBWPR", adBoolean, adParamInput, 1, isOn(UtilBWPR))    
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		
		'delete from the questionReportType table wher questionID = questionID
		'Create command
	'	Set oCmd = Server.CreateObject("ADODB.Command")
		
	'	With oCmd
	'	   .ActiveConnection = DataConn
	'	   .CommandText = "spDeleteQuestionReportType"
	'	   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, questionID)
	'	   .CommandType = adCmdStoredProc
	'	End With
				
	'	Set rs = oCmd.Execute
		
	'	Set oCmd = nothing
		
		'add to the questionReportTypeTable		
	'	for i = 0 to ubound(arrVals)
		
			'Create command
	'		Set oCmd = Server.CreateObject("ADODB.Command")
	'		With oCmd
	'		   .ActiveConnection = DataConn
	'		   .CommandText = "spAddQuestionReportType"
	'		   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, questionID)
	'		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, trim(arrVals(i)))
	'		   .CommandType = adCmdStoredProc
	'		End With
					
	'		Set rs = oCmd.Execute
	'		Set oCmd = nothing
			'response.Write trim(arrVals(i)) & "<br>"'request("reportType" & trim(arrVals(i))) & "<br>"
	'	next
		
		
		rs.close
		set rs = nothing
		
		response.Redirect "questionList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "addQuestion"
	
		'referenceLetter = request("referenceLetter")
		question = request("question")
		secondLanguage = request("secondLanguage")
		sortNumber = request("sortNumber")
		categoryID = request("categoryID")
		customerID = request("customerID")
		weekly =  request("weekly")
		monthly =  request("monthly")
		postRain =  request("postRain")
		divisionID = request("divisionID")
		'biweekly =  request("biweekly")
		annual =  request("annual")
		petroleum =  request("petroleum")
		questionType = request("questionType")
		UtilBWPR = request("UtilBWPR")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddQuestion"
		   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
		  ' .parameters.Append .CreateParameter("@referenceLetter", adChar, adParamInput, 10, referenceLetter)
		   .parameters.Append .CreateParameter("@question", adVarChar, adParamInput, 1000, question)
		   .parameters.Append .CreateParameter("@secondLanguage", adVarChar, adParamInput, 1000, secondLanguage)
		   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, sortNumber)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)	
		   .parameters.Append .CreateParameter("@weekly", adBoolean, adParamInput, 1, isOn(weekly))
		   .parameters.Append .CreateParameter("@monthly", adBoolean, adParamInput, 1, isOn(monthly))	
		   .parameters.Append .CreateParameter("@postRain", adBoolean, adParamInput, 1, isOn(postRain))	 
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID) 
		  ' .parameters.Append .CreateParameter("@biweekly", adBoolean, adParamInput, 1, isOn(biweekly))
		   .parameters.Append .CreateParameter("@annual", adBoolean, adParamInput, 1, isOn(annual)) 
		   .parameters.Append .CreateParameter("@questionType", adInteger, adParamInput, 8, questionType)
		   .parameters.Append .CreateParameter("@petroleum", adBoolean, adParamInput, 1, isOn(petroleum)) 
		   .parameters.Append .CreateParameter("@UtilBWPR", adBoolean, adParamInput, 1, isOn(UtilBWPR)) 
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "questionList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "deleteQuestion"
		questionID = Request("id")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteQuestion"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, questionID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "questionList.asp?id=" & divisionID & "&customerID=" & customerID
	
	case "editCategory"
	
		categoryID = request("categoryID")
		category = request("category")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditCategory"
		   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
		   .parameters.Append .CreateParameter("@category", adVarChar, adParamInput, 200, category)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "categoryList.asp?id=" & customerID & "&divisionID=" & divisionID
		
	case "addCategory"
	
		category = request("category")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddCategory"
		   .parameters.Append .CreateParameter("@category", adVarChar, adParamInput, 200, category)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "categoryList.asp?id=" & customerID & "&divisionID=" & divisionID
		
	case "deleteCategory"
		categoryID = Request("id")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteCategory"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, categoryID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "categoryList.asp?id=" & customerID & "&divisionID=" & divisionID
		
	case "addPerformance"
	
		userID = request("userID")
		projectID = request("project")
		clientID = request("clientID")
		email = request("email")
		phone = request("phone")
		performanceIssueID = request("issue")
		describe = request("describe")

		'Open connection and insert into the database
		On Error Resume Next
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'add the data to the database
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddPerformance"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 100, email)
		   .parameters.Append .CreateParameter("@phone", adVarChar, adParamInput, 50, phone)
		   .parameters.Append .CreateParameter("@performanceIssueID", adInteger, adParamInput, 8, performanceIssueID)
		   .parameters.Append .CreateParameter("@describe", adVarChar, adParamInput, 5000, describe)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		performanceID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
		
		'send an email with the info above to the correct administrators
		'get the performance info
		'spGetPerformanceItem
		'response.Write "perf ID " & performanceID
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetPerformanceItem"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, performanceID)
		   .CommandType = adCmdStoredProc
		   
		End With
		Set rsPerf = oCmd.Execute
		
		
		'send the email to the administrators
		sSubject = "BMP Performance for " & rsPerf("projectName")
		sBody = sBody & "A BMP performance entry has been submitted.<br><br>"
		sBody = sBody & "<b>Inspector:</b> " & rsPerf("firstName") & " " & rsPerf("lastName") & "<br>"
		sBody = sBody & "<b>Project Involved:</b> " & rsPerf("projectName") & "<br>"
		sBody = sBody & "<b>Inspector's Email Address:</b> <a href=mailto:" & rsPerf("email") & ">" & rsPerf("email") & "</a><br>"
		sBody = sBody & "<b>Inspector's Phone Number:</b> " & rsPerf("phone") & "<br>"
		sBody = sBody & "<b>Issue:</b> " & rsPerf("issue") & "<br>"
		sBody = sBody & "<b>Issue Description:</b> " & rsPerf("describe") & "<br>"
		
		sFrom = rsPerf("email")
		'get the list of admins based on the client ID supplied
		sTo = getAdminEmail(rsPerf("clientID"))
		
		'send the email
		'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
 		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
	    with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
	    end with
		
		with oCdoMail
		  .From = sFrom
		  .To = sTo'"rreynolds@nextnpdes.com;randy@crittgraham.com"'sTo
	    end with
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
	    end with
	
		oCdoMail.Configuration = oCdoConf
	    oCdoMail.Send
	    Set oCdoConf = Nothing
	    Set oCdoMail = Nothing

		bSent = True

		'############################################################################################################
		
		response.Redirect "form.asp?formType=addPerformance&msg=True"
		
	case "addProject"
	
		projectName = request("projectName")
		projectNumber = request("projectNumber")
		city = request("city")
		county = request("county")		
		customerID = request("customer")
		division = request("division")
		autoEmails = request("autoEmail")
		startDate = request("startDate")
		endDate = request("endDate")
		sstate = request("state")
		latitude = request("latitude")
		longitude = request("longitude")
		latitude2 = request("latitude2")
		longitude2 = request("longitude2")
		regionID = request("regionID")
		projectTypeID = request("projectTypeID")
		NTULimit = request("NTULimit")
		officeID = checknull(request("officeID"))
		
		response.Write county
		
		
		if dRate = "" then
			dRate = 0
		end if
		
		if wprRate = "" then
			wprRate = 0
		end if
		
		If startDate = "" then				
			startDate = null
		end if
		
		If endDate = "" then				
			endDate = null
		end if

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddProject"
		   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 200, projectName)
		   .parameters.Append .CreateParameter("@projectNumber", adVarChar, adParamInput, 50, projectNumber)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@county", adVarChar, adParamInput, 100, county)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@isActive", adBoolean, adParamInput, 1, true)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@division", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@autoEmail", adBoolean, adParamInput, 1, isOn(autoEmails))
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@endDate", adDBTimeStamp, adParamInput, 8, endDate)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@latitude", adVarChar, adParamInput, 50, latitude)
		   .parameters.Append .CreateParameter("@longitude", adVarChar, adParamInput, 50, longitude)
		   .parameters.Append .CreateParameter("@latitude2", adVarChar, adParamInput, 50, latitude2)
		   .parameters.Append .CreateParameter("@longitude2", adVarChar, adParamInput, 50, longitude2)
		   .parameters.Append .CreateParameter("@regionID", adInteger, adParamInput, 8, regionID)
		   .parameters.Append .CreateParameter("@projectTypeID", adInteger, adParamInput, 8, projectTypeID)
		   .parameters.Append .CreateParameter("@NTULimit", adVarChar, adParamInput, 50, NTULimit)
		   .parameters.Append .CreateParameter("@officeID", adInteger, adParamInput, 8, officeID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		projectID = rs("identity")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'assign the project to the super admins
		assignSuperAdmin projectID 
		
		response.Redirect "projectList.asp?id=" & division & "&customerID=" & customerID
		
	case "editProject"
	
		projectID = request("projectID")
		projectName = request("projectName")
		projectNumber = request("projectNumber")
		city = request("city")
		county = request("county")
		customerID = request("customer")
		isActive = request("isActive")
		division = request("division")
		autoEmails = request("autoEmail")
		startDate = request("startDate")
		endDate = request("endDate")
		primaryInspector = request("primaryInspector")
		sstate = request("state")
		latitude = request("latitude")
		longitude = request("longitude")
		latitude2 = request("latitude2")
		longitude2 = request("longitude2")
		
		projectActive = request("projectActive")
		clientID = request("clientID")
		customerName = request("customerName")
		userName = request("userName")
		regionID = request("regionID")
		projectTypeID = request("projectTypeID")
		NTULimit = request("NTULimit")
		officeID = checknull(request("officeID"))
		
		if dRate = "" then
			dRate = 0
		end if
		
		if wprRate = "" then
			wprRate = 0
		end if
		
		If startDate = "" then				
			startDate = null
		end if
		
		If endDate = "" then				
			endDate = null
		end if

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditProject"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 200, projectName)
		   .parameters.Append .CreateParameter("@projectNumber", adVarChar, adParamInput, 50, projectNumber)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@county", adVarChar, adParamInput, 100, county)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@isActive", adBoolean, adParamInput, 1, isOn(isActive))
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@autoEmail", adBoolean, adParamInput, 1, isOn(autoEmails))
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@endDate", adDBTimeStamp, adParamInput, 8, endDate)
		   .parameters.Append .CreateParameter("@primaryInspector", adInteger, adParamInput, 8, primaryInspector)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@latitude", adVarChar, adParamInput, 50, latitude)
		   .parameters.Append .CreateParameter("@longitude", adVarChar, adParamInput, 50, longitude)
		   .parameters.Append .CreateParameter("@latitude2", adVarChar, adParamInput, 50, latitude2)
		   .parameters.Append .CreateParameter("@longitude2", adVarChar, adParamInput, 50, longitude2)
		   .parameters.Append .CreateParameter("@regionID", adInteger, adParamInput, 8, regionID)
		   .parameters.Append .CreateParameter("@projectTypeID", adInteger, adParamInput, 8, projectTypeID)
		   .parameters.Append .CreateParameter("@NTULimit", adVarChar, adParamInput, 50, NTULimit)
		   .parameters.Append .CreateParameter("@officeID", adInteger, adParamInput, 8, officeID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		if projectActive = "True" then
			if isActive = "" then
			
				'disable the alerts
				Set oCmd = Server.CreateObject("ADODB.Command")	
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spDisableAlerts"
				   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rsAlerts = oCmd.Execute
			
				'send an email to let the users know that the project has been set to inactive
				'get the users to send the email
				Set oCmd = Server.CreateObject("ADODB.Command")	
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetAssignedUsersEmail"
				   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
				   .CommandType = adCmdStoredProc
				End With
						
				Set rsEmail = oCmd.Execute
				
				if endDate = "" then
					endDate = formatdatetime(now(),2)
				end if
				
				sSubject = "Project De-activation - " & projectName & " - " & customerName
				sBody = "This email is notification that the above project is no longer active in HB NEXT as of " & endDate & ".<br>"
				sBody = sBody & "The project was de-activated by " & userName & ".<br><br>"
				sBody = sBody & "If you have any questions about this project being de-activated please reply to this email.<br><br>"
				sBody = sBody & "Thanks<br>Customer Support<br>HB NEXT"
				
				do until rsEmail.eof
					'response.Write rsEmail("email") & "<br>"
					'send the email out
					'############################################################################################################
					Set oCdoMail = Server.CreateObject("CDO.Message")
					Set oCdoConf = Server.CreateObject("CDO.Configuration")
					
					sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
					with oCdoConf
					  .Fields.Item(sConfURL & "sendusing") = 2
					  .Fields.Item(sConfURL & "smtpserver") = "localhost"
					  .Fields.Item(sConfURL & "smtpserverport") = 25
					  .Fields.Update
					end with
					
					with oCdoMail
					  .From = "support@hbnext.com"
					  .To = rsEmail("email")
					end with
					
					with oCdoMail
					  .Subject = sSubject
					  .TextBody = sBody
					  .HTMLBody = sBody
					end with
				
					oCdoMail.Configuration = oCdoConf
					oCdoMail.Send
					Set oCdoConf = Nothing
					Set oCdoMail = Nothing
			
					bSent = True			
					'############################################################################################################
				rsEmail.movenext
				loop
				
			end if
		end if
		
		'response.Redirect "projectList.asp?id=" & division & "&customerID=" & customerID & "#projectInfo"
		response.Redirect "form.asp?id=" & projectID & "&formType=editProject&customerID=" & customerID & "&divisionID=" & division & "&smsgProjectInfo=True#projectInfo"
		
	case "editRates"
	
		projectID = request("projectID")
		customerID = request("customer")
		division = request("division")
		wprRate = request("wprRate")
		dRate = request("dRate")
		billRate = request("billRate")
		activationFee = request("activationFee")
		'perInspectionRate = request("perInspectionRate")
		waterSamplingRate = request("waterSamplingRate")
		rateType = request("rateType")
		newStartBillingDays = request("newStartBillingDays")
		
		if newStartBillingDays = "" then
			newStartBillingDays = null
		end if
		
		if dRate = "" then
			dRate = 0
		end if
		
		if wprRate = "" then
			wprRate = 0
		end if
		
		if billRate = "" then
			billRate = 0
		end if
		
		if activationFee = "" then
			activationFee = 0
		end if
		
		if perInspectionRate = "" then
			perInspectionRate = 0
		end if
		
		if waterSamplingRate = "" then
			waterSamplingRate = 0
		end if

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditRates"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@wprRate", adCurrency, adParamInput, 8, wprRate)
		   .parameters.Append .CreateParameter("@dRate", adCurrency, adParamInput, 8, dRate)
		   .parameters.Append .CreateParameter("@billRate", adCurrency, adParamInput, 8, billRate)
		   .parameters.Append .CreateParameter("@activationFee", adCurrency, adParamInput, 8, activationFee)
		  ' .parameters.Append .CreateParameter("@perInspectionRate", adCurrency, adParamInput, 8, perInspectionRate)
		   .parameters.Append .CreateParameter("@waterSamplingRate", adCurrency, adParamInput, 8, waterSamplingRate)
		   .parameters.Append .CreateParameter("@rateType", adInteger, adParamInput, 8, rateType)
		   .parameters.Append .CreateParameter("@newStartBillingDays", adInteger, adParamInput, 8, newStartBillingDays)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'response.Redirect "projectList.asp?id=" & division & "&customerID=" & customerID & "#projectInfo"
		response.Redirect "form.asp?id=" & projectID & "&formType=editProject&customerID=" & customerID & "&divisionID=" & division & "&sMsgRates=True#rates"
		
	case "editAlerts"
	
		projectID = request("projectID")
		customerID = request("customer")
		division = request("division")
		sevenDayAlert = request("sevenDayAlert")
		fourteenDayAlert = request("fourteenDayAlert")
		openItemAlert = request("openItemAlert")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditAlerts"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@sevenDayAlert", adBoolean, adParamInput, 1, isOn(sevenDayAlert))
		   .parameters.Append .CreateParameter("@fourteenDayAlert", adBoolean, adParamInput, 1, isOn(fourteenDayAlert))
		   .parameters.Append .CreateParameter("@openItemAlert", adBoolean, adParamInput, 1, isOn(openItemAlert))
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing

		'response.Redirect "projectList.asp?id=" & division & "&customerID=" & customerID & "#projectInfo"
		response.Redirect "form.asp?id=" & projectID & "&formType=editProject&customerID=" & customerID & "&divisionID=" & division & "&sMsgAlerts=True#alerts"
		
		
	case "deleteProject"
		projectID = request("id")
		customerID = request("customerID")
		division = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteProject"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "projectList.asp?id=" & division & "&customerID=" & customerID
		
	case "addCustomer"
	
		customerName = request("customerName")
		contactName = null'request("contactName")
		contactEmail = null'request("contactEmail")
		address1 = request("address1")
		address2 = request("address2")
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		phone1 = request("phone1")
		phone2 = request("phone2")
		phone3 = request("phone3")
		phone = phone1 & phone2 & phone3
		ext = request("ext")
		cellPhone1 = null'request("cellPhone1")
		cellPhone2 = null'request("cellPhone2")
		cellPhone3 = null'request("cellPhone3")
		cellPhone = null'cellPhone1 & cellPhone2 & cellPhone3
		fax1 = request("fax1")
		fax2 = request("fax2")
		fax3 = request("fax3")
		fax = fax1 & fax2 & fax3
		logo = request("logo")
		parentCustomerID = request("parentCustomerID")
		clientID = request("clientID")
		isActive = request("isActive")
		customerTypeID = request("customerTypeID")
		
		accountManagerID = request("accountManagerID")
		areaManager = request("areaManager")
		countyID = null'request("county")
		HBTCClient = request("HBTCClient")
		SIC1 = checknull(request("SIC1"))
		SIC2 = checknull(request("SIC2"))
		SIC3 = checknull(request("SIC3"))
		SIC4 = checknull(request("SIC4"))
		NAICS1 = checknull(request("NAICS1"))
		NAICS2 = checknull(request("NAICS2"))
		NAICS3 = checknull(request("NAICS3"))
		NAICS4 = checknull(request("NAICS4"))
		NAICS5 = checknull(request("NAICS5"))
		NAICS6 = checknull(request("NAICS6"))		
		annualAverageEmployees = checknull(request("annualAverageEmployees"))
		totalHours = checknull(request("totalHours"))
		acctEstBy= request("acctEstBy")
		
		HBSClient = request("HBSClient")
		website = checknull(request("website "))
		softwareClient = request("softwareClient")
		
		if clientID = "" then
			clientID = null
		end if
		
		if parentCustomerID = "" then
			parentCustomerID = null
		end if
		
		if accountManagerID = "" then
			accountManagerID = null
		end if
		
		if areaManager = "" then
			areaManager = null
		end if

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddCustomer"
		   .parameters.Append .CreateParameter("@customerName", adVarChar, adParamInput, 200, customerName)
		   .parameters.Append .CreateParameter("@contactName", adVarChar, adParamInput, 100, contactName)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 150, contactEmail)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 200, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 200, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@phone", adVarChar, adParamInput, 50, phone)
		   .parameters.Append .CreateParameter("@ext", adVarChar, adParamInput, 50, ext)
		   .parameters.Append .CreateParameter("@cellPhone", adVarChar, adParamInput, 50, cellPhone)
		   .parameters.Append .CreateParameter("@fax", adVarChar, adParamInput, 50, fax)
		   .parameters.Append .CreateParameter("@logo", adVarChar, adParamInput, 200, logo)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@parentCustomerID", adInteger, adParamInput, 8, parentCustomerID)
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .parameters.Append .CreateParameter("@isActive", adBoolean, adParamInput, 1, isOn(isActive))
		   .parameters.Append .CreateParameter("@customerTypeID", adInteger, adParamInput, 8, customerTypeID)
		   .parameters.Append .CreateParameter("@accountManagerID", adInteger, adParamInput, 8, accountManagerID)
		   .parameters.Append .CreateParameter("@areaManager", adInteger, adParamInput, 8, areaManager)
		   .parameters.Append .CreateParameter("@countyID", adInteger, adParamInput, 8, countyID)
		   .parameters.Append .CreateParameter("@HBTCClient", adBoolean, adParamInput, 1, isOn(HBTCClient))
		   .parameters.Append .CreateParameter("@SIC1", adInteger, adParamInput, 8, SIC1)
		   .parameters.Append .CreateParameter("@SIC2", adInteger, adParamInput, 8, SIC2)
		   .parameters.Append .CreateParameter("@SIC3", adInteger, adParamInput, 8, SIC3)
		   .parameters.Append .CreateParameter("@SIC4", adInteger, adParamInput, 8, SIC4)
		   .parameters.Append .CreateParameter("@NAICS1", adInteger, adParamInput, 8, NAICS1)
		   .parameters.Append .CreateParameter("@NAICS2", adInteger, adParamInput, 8, NAICS2)
		   .parameters.Append .CreateParameter("@NAICS3", adInteger, adParamInput, 8, NAICS3)
		   .parameters.Append .CreateParameter("@NAICS4", adInteger, adParamInput, 8, NAICS4)
		   .parameters.Append .CreateParameter("@NAICS5", adInteger, adParamInput, 8, NAICS5)
		   .parameters.Append .CreateParameter("@NAICS6", adInteger, adParamInput, 8, NAICS6)
		   .parameters.Append .CreateParameter("@annualAverageEmployees", adInteger, adParamInput, 8, annualAverageEmployees)
		   .parameters.Append .CreateParameter("@totalHours", adInteger, adParamInput, 8, totalHours)
		   .parameters.Append .CreateParameter("@acctEstBy", adInteger, adParamInput, 8, acctEstBy)
		   .parameters.Append .CreateParameter("@HBSClient", adBoolean, adParamInput, 1, isOn(HBSClient))
		   .parameters.Append .CreateParameter("@website", adVarChar, adParamInput, 150, website)
		   .parameters.Append .CreateParameter("@softwareClient", adBoolean, adParamInput, 1, isOn(softwareClient))
		   
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		'get the customer id that we just created
		customerID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'build the default billing info		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditCustomerBilling"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)		   
		   .parameters.Append .CreateParameter("@billStreet", adVarChar, adParamInput, 100, address1)
		   .parameters.Append .CreateParameter("@billSuite", adVarChar, adParamInput, 50, address2)
		   .parameters.Append .CreateParameter("@billCity", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@billState", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@billZip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@billName", adVarChar, adParamInput, 50, contactName)
		   .parameters.Append .CreateParameter("@billEmail", adVarChar, adParamInput, 100, contactEmail)
		   .parameters.Append .CreateParameter("@billPhone", adVarChar, adParamInput, 50, phone)
		   .parameters.Append .CreateParameter("@billMobile", adVarChar, adParamInput, 50, null)
		   .parameters.Append .CreateParameter("@billFax", adVarChar, adParamInput, 50, fax)
		   .parameters.Append .CreateParameter("@billOtherPhone", adVarChar, adParamInput, 50, null)
		   .parameters.Append .CreateParameter("@billMethodFax", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@billMethodEmail", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@billMethodMail", adBoolean, adParamInput, 1, False)
		   .parameters.Append .CreateParameter("@acctManager", adVarChar, adParamInput, 50, null)
		   .parameters.Append .CreateParameter("@advanceBilling", adBoolean, adParamInput, 1, True)
		   .CommandType = adCmdStoredProc
		End With
			
		Set rsBill = oCmd.Execute
		Set oCmd = nothing
		rsBill.close
		set rsBill = nothing
		
		
		'*******************************************************************************************************************************************
		'if parentCustomerID <> Null then
		'create a template division
		'create lists for - Report Questions, Inspection Types, project Status and weather conditions based on what the parent has. can delete later
		'If parentCustomerID <> null then			
		buildDefaultLists customerID,0, clientID
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		'end if
		
		
		'*******************************************************************************************************************************************
		'get the first letter of the customer name
		'sLetter = Left(customerName,1)
		
		
		Select Case Session("fp")
			Case "quoteList"
				response.Redirect "quoteList.asp?clientID=" & clientID
			Case else
				response.Redirect "customerList.asp?search=" & customerName
		End Select
		
		
	case "addCustomerContact"
	
		customerID = request("customerID")
		quoteID = request("quoteID")
		clientID = request("clientID")
		contactName = request("contactName")
		contactLastName = request("contactLastName")
		contactEmail = request("contactEmail")
		address1 = request("address1")
		address2 = request("address2")
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		isPrimary = request("isPrimary")		
		phone1 = request("phone1")
		phone2 = request("phone2")
		phone3 = request("phone3")
		phone = phone1 & phone2 & phone3
		cellPhone1 = request("cellPhone1")
		cellPhone2 = request("cellPhone2")
		cellPhone3 = request("cellPhone3")
		cell = cellPhone1 & cellPhone2 & cellPhone3
		fax1 = request("fax1")
		fax2 = request("fax2")
		fax3 = request("fax3")
		fax = fax1 & fax2 & fax3
		contactTitle = request("contactTitle")
		HBTCContact = request("HBTCContact")
		ext = request("ext")
		
		officePhone1 = request("officePhone1")
		officePhone2 = request("officePhone2")
		officePhone3 = request("officePhone3")
		officePhone = officePhone1 & officePhone2 & officePhone3
		extOffice = request("extOffice")
		
		red = request("red")


		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddCustomerContact"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@contactName", adVarChar, adParamInput, 100, contactName)
		   .parameters.Append .CreateParameter("@contactLastName", adVarChar, adParamInput, 100, contactLastName)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 150, contactEmail)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 200, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 200, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@phone", adVarChar, adParamInput, 50, phone)
		   .parameters.Append .CreateParameter("@fax", adVarChar, adParamInput, 50, fax)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@isPrimary", adBoolean, adParamInput, 1, 0)
		   .parameters.Append .CreateParameter("@cell", adVarChar, adParamInput, 50, cell)
		   .parameters.Append .CreateParameter("@contactTitle", adVarChar, adParamInput, 50, contactTitle)
		   .parameters.Append .CreateParameter("@HBTCContact", adBoolean, adParamInput, 1, isOn(HBTCContact))
		   .parameters.Append .CreateParameter("@ext", adVarChar, adParamInput, 50, ext)
		   .parameters.Append .CreateParameter("@officePhone", adVarChar, adParamInput, 50, officePhone)
		   .parameters.Append .CreateParameter("@extOffice", adVarChar, adParamInput, 50, extOffice)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
	
		'get the customer id that we just created
		customerContactID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'unassign all of the isPrimary for this customer
		If isPrimary = "on" then
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spUnassignPrimaryContact"
			   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
			   .CommandType = adCmdStoredProc
			End With					
			Set rs = oCmd.Execute
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAssignPrimaryContact"
			   .parameters.Append .CreateParameter("@customerContactID", adInteger, adParamInput, 8, customerContactID)
			   .CommandType = adCmdStoredProc
			End With					
			Set rs = oCmd.Execute
		end if	
		
		'unassign all of the HBTCContact for this customer
		If HBTCContact = "on" then
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spUnassignHBTCContact"
			   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
			   .CommandType = adCmdStoredProc
			End With					
			Set rs = oCmd.Execute
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAssignHBTCContact"
			   .parameters.Append .CreateParameter("@customerContactID", adInteger, adParamInput, 8, customerContactID)
			   .CommandType = adCmdStoredProc
			End With					
			Set rs = oCmd.Execute
		end if
		
		Select Case red
			Case "quote"
				response.Redirect "form.asp?formType=assignCustomer&customerID=" & customerID & "&quoteID=" & quoteID & "&clientID=" & clientID
			case "conv"
				response.Redirect "form.asp?formType=editConversations&id=" & customerID
			Case else
				response.Redirect "customerContactList.asp?id=" & customerID
		End Select		
		
		
	case "editCustomerContact"
	
		customerContactID = request("customerContactID")
		customerID = request("customerID")
		contactName = request("contactName")
		contactLastName = request("contactLastName")
		contactEmail = request("contactEmail")
		address1 = request("address1")
		address2 = request("address2")
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		isPrimary = request("isPrimary")
		phone1 = request("phone1")
		phone2 = request("phone2")
		phone3 = request("phone3")
		phone = phone1 & phone2 & phone3
		cellPhone1 = request("cellPhone1")
		cellPhone2 = request("cellPhone2")
		cellPhone3 = request("cellPhone3")
		cell = cellPhone1 & cellPhone2 & cellPhone3
		fax1 = request("fax1")
		fax2 = request("fax2")
		fax3 = request("fax3")
		fax = fax1 & fax2 & fax3
		contactTitle = request("contactTitle")
		HBTCContact = request("HBTCContact")
		ext = request("ext")
		officePhone1 = request("officePhone1")
		officePhone2 = request("officePhone2")
		officePhone3 = request("officePhone3")
		officePhone = officePhone1 & officePhone2 & officePhone3
		extOffice = request("extOffice")


		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditCustomerContact"
		   .parameters.Append .CreateParameter("@customerContactID", adInteger, adParamInput, 8, customerContactID)
		   .parameters.Append .CreateParameter("@contactName", adVarChar, adParamInput, 100, contactName)
		   .parameters.Append .CreateParameter("@contactLastName", adVarChar, adParamInput, 100, contactLastName)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 150, contactEmail)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 200, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 200, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@phone", adVarChar, adParamInput, 50, phone)
		   .parameters.Append .CreateParameter("@fax", adVarChar, adParamInput, 50, fax)
		   .parameters.Append .CreateParameter("@cell", adVarChar, adParamInput, 50, cell)
		   .parameters.Append .CreateParameter("@contactTitle", adVarChar, adParamInput, 50, contactTitle)
		   .parameters.Append .CreateParameter("@HBTCContact", adBoolean, adParamInput, 1, isOn(HBTCContact))
		   .parameters.Append .CreateParameter("@ext", adVarChar, adParamInput, 50, ext)
		   .parameters.Append .CreateParameter("@officePhone", adVarChar, adParamInput, 50, officePhone)
		   .parameters.Append .CreateParameter("@extOffice", adVarChar, adParamInput, 50, extOffice)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'unassign all of the isPrimary for this customer
		If isPrimary = "on" then
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spUnassignPrimaryContact"
			   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
			   .CommandType = adCmdStoredProc
			End With					
			Set rs = oCmd.Execute
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAssignPrimaryContact"
			   .parameters.Append .CreateParameter("@customerContactID", adInteger, adParamInput, 8, customerContactID)
			   .CommandType = adCmdStoredProc
			End With					
			Set rs = oCmd.Execute
		end if	
		
		'unassign all of the HBTCContact for this customer
		If HBTCContact = "on" then
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spUnassignHBTCContact"
			   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
			   .CommandType = adCmdStoredProc
			End With					
			Set rs = oCmd.Execute
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
			Set oCmd = Server.CreateObject("ADODB.Command")				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAssignHBTCContact"
			   .parameters.Append .CreateParameter("@customerContactID", adInteger, adParamInput, 8, customerContactID)
			   .CommandType = adCmdStoredProc
			End With					
			Set rs = oCmd.Execute
		end if
		
		response.Redirect "customerContactList.asp?id=" & customerID
		
	case "deleteCustomerContact"
		customerContactID = Request("id")
		customerID = request("customerID")
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			
		DataConn.Open Session("Connection"), Session("UserID")
	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteCustomerContact"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, customerContactID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "customerContactList.asp?id=" & customerID 
		
	case "addConversation"
	
		customerID = request("customerID")
		billComments = request("billComments")
		customerName = request("customerName")
		userID = request("userID")
		commentTypeID = request("commentTypeID")
		customerContactID = request("customerContactID")
		project = null
		

		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		itemName = "Billing comments for: " & customerName
		itemDay = day(date())
		itemMonth = month(date())
		itemYear = year(date())
		itemEntry = billComments
			
		addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project
		
		
		'add the comments to the billComments table
		Set oCmd = Server.CreateObject("ADODB.Command")

		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddBillComments"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)	
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)	   
		   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 5000, billComments)
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@commentTypeID", adInteger, adParamInput, 8, commentTypeID)
		   .parameters.Append .CreateParameter("@customerContactID", adInteger, adParamInput, 8, customerContactID)
		   .CommandType = adCmdStoredProc
		End With
	
		Set rs = oCmd.Execute
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'response.Redirect "customerList.asp?id=" & left(customerName,1)
		'
		response.Redirect "form.asp?formType=editConversations&id=" & customerID & "&sMsgConv=True"
		
		
	case "editCustomerBilling"
	
		customerID = request("customerID")
		billStreet = request("billStreet")
		billSuite = request("billSuite")
		billCity = request("billCity")
		billState = request("billState")
		billZip = request("billZip")
		billName = request("billName")
		billEmail = request("billEmail")
		billPhone = request("billPhone")		
		billMobile = request("billMobile")
		billFax = request("billFax")
		billOtherPhone = request("billOtherPhone")
		billMethodFax = request("billMethodFax")
		billMethodEmail = request("billMethodEmail")
		billMethodMail = request("billMethodMail")
		acctManager = request("acctManager")
		advanceBilling = request("advanceBilling")
		
		'added to the comments table and the journal/blog
		'for the user
		billComments = request("billComments")
		customerName = request("customerName")
		userID = request("userID")
		commentTypeID = request("commentTypeID")
		project = null
		

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditCustomerBilling"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)		   
		   .parameters.Append .CreateParameter("@billStreet", adVarChar, adParamInput, 100, billStreet)
		   .parameters.Append .CreateParameter("@billSuite", adVarChar, adParamInput, 50, billSuite)
		   .parameters.Append .CreateParameter("@billCity", adVarChar, adParamInput, 100, billCity)
		   .parameters.Append .CreateParameter("@billState", adVarChar, adParamInput, 50, billState)
		   .parameters.Append .CreateParameter("@billZip", adVarChar, adParamInput, 50, billZip)
		   .parameters.Append .CreateParameter("@billName", adVarChar, adParamInput, 50, billName)
		   .parameters.Append .CreateParameter("@billEmail", adVarChar, adParamInput, 100, billEmail)
		   .parameters.Append .CreateParameter("@billPhone", adVarChar, adParamInput, 50, billPhone)
		   .parameters.Append .CreateParameter("@billMobile", adVarChar, adParamInput, 50, billMobile)
		   .parameters.Append .CreateParameter("@billFax", adVarChar, adParamInput, 50, billFax)
		   .parameters.Append .CreateParameter("@billOtherPhone", adVarChar, adParamInput, 50, billOtherPhone)
		   .parameters.Append .CreateParameter("@billMethodFax", adBoolean, adParamInput, 1, isOn(billMethodFax))
		   .parameters.Append .CreateParameter("@billMethodEmail", adBoolean, adParamInput, 1, isOn(billMethodEmail))
		   .parameters.Append .CreateParameter("@billMethodMail", adBoolean, adParamInput, 1, isOn(billMethodMail))
		   .parameters.Append .CreateParameter("@acctManager", adVarChar, adParamInput, 50, acctManager)
		   .parameters.Append .CreateParameter("@advanceBilling", adBoolean, adParamInput, 1, isOn(advanceBilling))
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		'add the comments to the blog/journal and the billcomments table
		if billComments <> "" then
			
			userID = session("ID")
			itemName = "Billing comments for: " & customerName
			itemDay = day(date())
			itemMonth = month(date())
			itemYear = year(date())
			itemEntry = billComments
			
			addJournalEntry userID, itemName, itemDay, itemMonth, itemYear, itemEntry, project
		
		
			'add the comments to the billComments table
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddBillComments"
			   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)	
			   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)	   
			   .parameters.Append .CreateParameter("@comments", adVarChar, adParamInput, 5000, billComments)
			   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
			   .parameters.Append .CreateParameter("@commentTypeID", adInteger, adParamInput, 8, commentTypeID)
			   .parameters.Append .CreateParameter("@customerContactID", adInteger, adParamInput, 8, 0)
			   .CommandType = adCmdStoredProc
			End With
		
			Set rs = oCmd.Execute
			
			Set oCmd = nothing
			rs.close
			set rs = nothing
			
		end if	
		
		'response.Redirect "customerList.asp?id=" & left(customerName,1)
		'
		response.Redirect "form.asp?formType=editCustomerBilling&id=" & customerID & "&sMsgBill=True"
		
	case "editCustomer"
		customerID = request("id")
		customerName = request("customerName")
		contactName = null'request("contactName")
		contactEmail = null'request("contactEmail")
		address1 = request("address1")
		address2 = request("address2")
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		phone1 = request("phone1")
		phone2 = request("phone2")
		phone3 = request("phone3")
		phone = phone1 & phone2 & phone3
		ext = request("ext")
		cellPhone1 = null'request("cellPhone1")
		cellPhone2 = null'request("cellPhone2")
		cellPhone3 = null'request("cellPhone3")
		cellPhone = null'cellPhone1 & cellPhone2 & cellPhone3
		fax1 = request("fax1")
		fax2 = request("fax2")
		fax3 = request("fax3")
		fax = fax1 & fax2 & fax3
		logo = request("logo")		
		parentCustomerID = request("parentCustomerID")
		isActive = request("isActive")
		customerTypeID = request("customerTypeID")
		accountManagerID = request("accountManagerID")
		areaManager = request("areaManager")
		countyID = null'request("county")
		HBTCClient = request("HBTCClient")
		SIC1 = checknull(request("SIC1"))
		SIC2 = checknull(request("SIC2"))
		SIC3 = checknull(request("SIC3"))
		SIC4 = checknull(request("SIC4"))
		NAICS1 = checknull(request("NAICS1"))
		NAICS2 = checknull(request("NAICS2"))
		NAICS3 = checknull(request("NAICS3"))
		NAICS4 = checknull(request("NAICS4"))
		NAICS5 = checknull(request("NAICS5"))
		NAICS6 = checknull(request("NAICS6"))		
		annualAverageEmployees = checknull(request("annualAverageEmployees"))
		totalHours = checknull(request("totalHours"))
		acctEstBy = request("acctEstBy")
		HBSClient = request("HBSClient")
		website = checknull(request("website"))
		softwareClient = request("softwareClient")
		
		if parentCustomerID = "" then
			parentCustomerID = null
		end if
		
		if accountManagerID = "" then
			accountManagerID = null
		end if
		
		if areaManager = "" then
			areaManager = null
		end if

		if countyID = "" then
			countyID = null
		end if
		

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditCustomer"
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@customerName", adVarChar, adParamInput, 200, customerName)
		   .parameters.Append .CreateParameter("@contactName", adVarChar, adParamInput, 100, contactName)
		   .parameters.Append .CreateParameter("@contactEmail", adVarChar, adParamInput, 150, contactEmail)
		   .parameters.Append .CreateParameter("@address1", adVarChar, adParamInput, 200, address1)
		   .parameters.Append .CreateParameter("@address2", adVarChar, adParamInput, 200, address2)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 100, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@phone", adVarChar, adParamInput, 50, phone)
		   .parameters.Append .CreateParameter("@ext", adVarChar, adParamInput, 50, ext)
		   .parameters.Append .CreateParameter("@cellPhone", adVarChar, adParamInput, 50, cellPhone)
		   .parameters.Append .CreateParameter("@fax", adVarChar, adParamInput, 50, fax)
		   .parameters.Append .CreateParameter("@logo", adVarChar, adParamInput, 200, logo)
		   .parameters.Append .CreateParameter("@dateModified", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@parentCustomerID", adInteger, adParamInput, 8, parentCustomerID)
		   .parameters.Append .CreateParameter("@isActive", adBoolean, adParamInput, 1, isOn(isActive))
		   .parameters.Append .CreateParameter("@customerTypeID", adInteger, adParamInput, 8, customerTypeID)
		   .parameters.Append .CreateParameter("@accountManagerID", adInteger, adParamInput, 8, accountManagerID)
		   .parameters.Append .CreateParameter("@areaManager", adInteger, adParamInput, 8, areaManager)
		   .parameters.Append .CreateParameter("@countyID", adInteger, adParamInput, 8, countyID)
		   .parameters.Append .CreateParameter("@HBTCClient", adBoolean, adParamInput, 1, isOn(HBTCClient))
		   .parameters.Append .CreateParameter("@SIC1", adInteger, adParamInput, 8, SIC1)
		   .parameters.Append .CreateParameter("@SIC2", adInteger, adParamInput, 8, SIC2)
		   .parameters.Append .CreateParameter("@SIC3", adInteger, adParamInput, 8, SIC3)
		   .parameters.Append .CreateParameter("@SIC4", adInteger, adParamInput, 8, SIC4)
		   .parameters.Append .CreateParameter("@NAICS1", adInteger, adParamInput, 8, NAICS1)
		   .parameters.Append .CreateParameter("@NAICS2", adInteger, adParamInput, 8, NAICS2)
		   .parameters.Append .CreateParameter("@NAICS3", adInteger, adParamInput, 8, NAICS3)
		   .parameters.Append .CreateParameter("@NAICS4", adInteger, adParamInput, 8, NAICS4)
		   .parameters.Append .CreateParameter("@NAICS5", adInteger, adParamInput, 8, NAICS5)
		   .parameters.Append .CreateParameter("@NAICS6", adInteger, adParamInput, 8, NAICS6)
		   .parameters.Append .CreateParameter("@annualAverageEmployees", adInteger, adParamInput, 8, annualAverageEmployees)
		   .parameters.Append .CreateParameter("@totalHours", adInteger, adParamInput, 8, totalHours)
		   .parameters.Append .CreateParameter("@acctEstBy", adInteger, adParamInput, 8, acctEstBy)
		   .parameters.Append .CreateParameter("@HBSClient", adBoolean, adParamInput, 1, isOn(HBSClient))
		   .parameters.Append .CreateParameter("@website", adVarChar, adParamInput, 150, website)
		   .parameters.Append .CreateParameter("@softwareClient", adBoolean, adParamInput, 1, isOn(softwareClient))
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'sLetter = Left(customerName,1)

		response.Redirect "customerList.asp?search=" & customerName
		
	case "deleteCustomer"
		customerID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteCustomer"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, customerID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "customerList.asp" 
		
	case "addInspectionType"
	
		inspectionType = request("inspectionType")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddInspectionType"
		   .parameters.Append .CreateParameter("@inspectionType", adVarChar, adParamInput, 100, inspectionType)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "inspectionTypeList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "editInspectionType"
		inspectionTypeID = request("inspectionTypeID")
		inspectionType = request("inspectionType")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditInspectionType"
		   .parameters.Append .CreateParameter("@inspectionTypeID", adInteger, adParamInput, 8, inspectionTypeID)
		   .parameters.Append .CreateParameter("@inspectionType", adVarChar, adParamInput, 100, inspectionType)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "inspectionTypeList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "deleteInspectionType"
		inspectionTypeID = Request("id")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteInspectionType"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, inspectionTypeID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "inspectionTypeList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "addReportType"
	
		reportType = request("reportType")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddReportType"
		   .parameters.Append .CreateParameter("@reportType", adVarChar, adParamInput, 50, reportType)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "reportTypeList.asp"
		
	case "editReportType"
		reportTypeID = request("reportTypeID")
		reportType = request("reportType")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditReportType"
		   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, reportTypeID)
		   .parameters.Append .CreateParameter("@reportType", adVarChar, adParamInput, 50, reportType)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "reportTypeList.asp"
		
	case "deleteReportType"
		reportTypeID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteReportType"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportTypeID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "reportTypeList.asp" 
		
	case "addProjectStatus"
	
		projectStatus = request("projectStatus")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddProjectStatus"
		   .parameters.Append .CreateParameter("@projectStatus", adVarChar, adParamInput, 50, projectStatus)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "projectStatusList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "editProjectStatus"
		projectStatusID = request("projectStatusID")
		projectStatus = request("projectStatus")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditProjectStatus"
		   .parameters.Append .CreateParameter("@projectStatusID", adInteger, adParamInput, 8, projectStatusID)
		   .parameters.Append .CreateParameter("@projectStatus", adVarChar, adParamInput, 50, projectStatus)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "projectStatusList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "deleteProjectStatus"
		projectStatusID = Request("id")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteProjectStatus"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectStatusID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "projectStatusList.asp?id=" & divisionID & "&customerID=" & customerID 
		
	case "addWeatherType"
	
		weatherType = request("weatherType")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddWeatherType"
		   .parameters.Append .CreateParameter("@weatherType", adVarChar, adParamInput, 50, weatherType)
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "weatherList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "editWeatherType"
		weatherID = request("weatherID")
		weatherType = request("weatherType")
		customerID = request("customerID")
		divisionID = request("divisionID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditWeatherType"
		   .parameters.Append .CreateParameter("@weatherID", adInteger, adParamInput, 8, weatherID)
		   .parameters.Append .CreateParameter("@weatherType", adVarChar, adParamInput, 50, weatherType)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "weatherList.asp?id=" & divisionID & "&customerID=" & customerID
		
	case "deleteWeatherType"
		weatherID = Request("id")
		customerID = request("customerID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteWeatherType"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, weatherID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "weatherList.asp?id=" & divisionID & "&customerID=" & customerID 
		
	case "addContact"
		projectID = request("projectID")
		contactTypeID = request("contactType")
		firstName = request("firstName")		
		lastName = request("lastName")
		companyName = request("companyName")
		email = request("email")
		officeNumber1 = request("officeNumber1")
		officeNumber2 = request("officeNumber2")
		officeNumber3 = request("officeNumber3")
		officeNumberExt = request("officeNumberExt")
		cellNumber1 = request("cellNumber1")
		cellNumber2 = request("cellNumber2")
		cellNumber3 = request("cellNumber3")
		address = request("address")		
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		f = request("f")
		getAutoEmail = request("getAutoEmail")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddContact"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@contactTypeID", adInteger, adParamInput, 8, contactTypeID)
		   .parameters.Append .CreateParameter("@firstName", adVarChar, adParamInput, 50, firstName)
		   .parameters.Append .CreateParameter("@lastName", adVarChar, adParamInput, 100, lastName)
		   .parameters.Append .CreateParameter("@companyName", adVarChar, adParamInput, 100, companyName)
		   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 100, email)
		   .parameters.Append .CreateParameter("@officeNumber1", adVarChar, adParamInput, 50, officeNumber1)
		   .parameters.Append .CreateParameter("@officeNumber2", adVarChar, adParamInput, 50, officeNumber2)
		   .parameters.Append .CreateParameter("@officeNumber3", adVarChar, adParamInput, 50, officeNumber3)
		   .parameters.Append .CreateParameter("@officeNumberExt", adVarChar, adParamInput, 50, officeNumberExt)
		   .parameters.Append .CreateParameter("@cellNumber1", adVarChar, adParamInput, 50, cellNumber1)
		   .parameters.Append .CreateParameter("@cellNumber2", adVarChar, adParamInput, 50, cellNumber2)
		   .parameters.Append .CreateParameter("@cellNumber3", adVarChar, adParamInput, 50, cellNumber3)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 100, address)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@getAutoEmail", adBoolean, adParamInput, 1, isOn(getAutoEmail))
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		if f = "ab" then
			response.Redirect "addressBook.asp?project=" & projectID
		else
			response.Redirect "contactList.asp?project=" & projectID
		end if
		
	case "editContact"
		projectID = request("projectID")
		contactID = request("contactID")
		contactTypeID = request("contactType")
		firstName = request("firstName")		
		lastName = request("lastName")
		companyName = request("companyName")
		email = request("email")
		officeNumber1 = request("officeNumber1")
		officeNumber2 = request("officeNumber2")
		officeNumber3 = request("officeNumber3")
		officeNumberExt = request("officeNumberExt")
		cellNumber1 = request("cellNumber1")
		cellNumber2 = request("cellNumber2")
		cellNumber3 = request("cellNumber3")
		address = request("address")		
		city = request("city")
		sstate = request("state")
		zip = request("zip")
		getAutoEmail = request("getAutoEmail")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditContact"
		   .parameters.Append .CreateParameter("@contactID", adInteger, adParamInput, 8, contactID)
		   .parameters.Append .CreateParameter("@contactTypeID", adInteger, adParamInput, 8, contactTypeID)
		   .parameters.Append .CreateParameter("@firstName", adVarChar, adParamInput, 50, firstName)
		   .parameters.Append .CreateParameter("@lastName", adVarChar, adParamInput, 100, lastName)
		   .parameters.Append .CreateParameter("@companyName", adVarChar, adParamInput, 100, companyName)
		   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 100, email)
		   .parameters.Append .CreateParameter("@officeNumber1", adVarChar, adParamInput, 50, officeNumber1)
		   .parameters.Append .CreateParameter("@officeNumber2", adVarChar, adParamInput, 50, officeNumber2)
		   .parameters.Append .CreateParameter("@officeNumber3", adVarChar, adParamInput, 50, officeNumber3)
		   .parameters.Append .CreateParameter("@officeNumberExt", adVarChar, adParamInput, 50, officeNumberExt)
		   .parameters.Append .CreateParameter("@cellNumber1", adVarChar, adParamInput, 50, cellNumber1)
		   .parameters.Append .CreateParameter("@cellNumber2", adVarChar, adParamInput, 50, cellNumber2)
		   .parameters.Append .CreateParameter("@cellNumber3", adVarChar, adParamInput, 50, cellNumber3)
		   .parameters.Append .CreateParameter("@address", adVarChar, adParamInput, 100, address)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@state", adVarChar, adParamInput, 50, sstate)
		   .parameters.Append .CreateParameter("@zip", adVarChar, adParamInput, 50, zip)
		   .parameters.Append .CreateParameter("@getAutoEmail", adBoolean, adParamInput, 1, isOn(getAutoEmail))
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "contactList.asp?project=" & projectID
		
	case "deleteContact"
		contactID = Request("id")
		projectID = request("projectID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteContact"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, contactID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "contactList.asp?project=" & projectID
		
	case "addJournal"
	
		itemName = request("itemName")
		itemDate = request("itemDate")
		itemEntry= request("itemEntry")
		userID= request("userID")
	
		itemDay = cDate(day(itemDate))
		itemMonth = cDate(month(itemDate))
		itemYear = cDate(year(itemDate))

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddJournal"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@itemName", adVarChar, adParamInput, 100, itemName)
		   .parameters.Append .CreateParameter("@itemDay", adInteger, adParamInput, 8, itemDay)
		   .parameters.Append .CreateParameter("@itemMonth", adInteger, adParamInput, 8, itemMonth)
		   .parameters.Append .CreateParameter("@itemYear", adInteger, adParamInput, 8, itemYear)
		   .parameters.Append .CreateParameter("@itemEntry", adVarChar, adParamInput, 5000, itemEntry)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, null)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "journal.asp"
		
	case "editJournal"
	
		journalID= request("journalID")
		itemName = request("itemName")
		itemDate = request("itemDate")
		itemEntry= request("itemEntry")
		userID= request("userID")
	
		itemDay = cDate(day(itemDate))
		itemMonth = cDate(month(itemDate))
		itemYear = cDate(year(itemDate))

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditJournal"
		   .parameters.Append .CreateParameter("@journalID", adInteger, adParamInput, 8, journalID)
		   .parameters.Append .CreateParameter("@itemName", adVarChar, adParamInput, 100, itemName)
		   .parameters.Append .CreateParameter("@itemDay", adInteger, adParamInput, 8, itemDay)
		   .parameters.Append .CreateParameter("@itemMonth", adInteger, adParamInput, 8, itemMonth)
		   .parameters.Append .CreateParameter("@itemYear", adInteger, adParamInput, 8, itemYear)
		   .parameters.Append .CreateParameter("@itemEntry", adVarChar, adParamInput, 500, itemEntry)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "journal.asp"
		
	case "deleteJournal"
		journalID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteJournal"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, journalID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "journal.asp"
		
	Case "shareJournal"

		userID = request("userID")
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		'unassign and reassign customer to the client
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spUnshareJournal"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, userID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
	'	Set oCmd = nothing
	'	rs.close
	'	set rs = nothing
		
		
		'reassign the customers to the client
		for i=1 to Request("sharedUsers").count
			listboxvalue=Request.form("sharedUsers").item(i)
			
			'response.Write userID & "<br>"
			'response.Write listboxvalue & "<br>"
			
			'insert the data into the userCustomer table
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			DataConn.Open Session("Connection"), Session("UserID")
			'Create command
			Set oCmd2 = Server.CreateObject("ADODB.Command")
			
			With oCmd2
			   .ActiveConnection = DataConn
			   .CommandText = "spShareJournal"
			   .parameters.Append .CreateParameter("@sharerID", adInteger, adParamInput, 8, userID)
			   .parameters.Append .CreateParameter("@sharedID", adInteger, adParamInput, 8, listboxvalue)
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rsShare = oCmd2.Execute
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
		next
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd2 = nothing
		rsShare.close
		set rsShare = nothing
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		response.Redirect "journal.asp"
		
	Case "shareCalendar"

		userID = request("userID")
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		'unassign and reassign customer to the client
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spUnshareCalendar"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, userID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
	'	Set oCmd = nothing
	'	rs.close
	'	set rs = nothing
		
		
		'reassign the customers to the client
		for i=1 to Request("sharedUsers").count
			listboxvalue=Request.form("sharedUsers").item(i)
			
			'response.Write userID & "<br>"
			'response.Write listboxvalue & "<br>"
			
			'insert the data into the userCustomer table
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			DataConn.Open Session("Connection"), Session("UserID")
			'Create command
			Set oCmd2 = Server.CreateObject("ADODB.Command")
			
			With oCmd2
			   .ActiveConnection = DataConn
			   .CommandText = "spShareCalendar"
			   .parameters.Append .CreateParameter("@sharerID", adInteger, adParamInput, 8, userID)
			   .parameters.Append .CreateParameter("@sharedID", adInteger, adParamInput, 8, listboxvalue)
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rsShare = oCmd2.Execute
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
		next
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd2 = nothing
		rsShare.close
		set rsShare = nothing
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		
		response.Redirect "calendar.asp"
		
		
	case "addEvent"
	
		eventName = request("eventName")
		eventDate = request("eventDate")
		timeFrom= request("timeFrom")
		timeTo= request("timeTo")
		eventDescription= request("eventDescription")
		userID= request("userID")
		'clientID= request("clientID")
		
		eventDay = cDate(day(eventDate))
		eventMonth = cDate(month(eventDate))
		eventYear = cDate(year(eventDate))

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddEvent"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .parameters.Append .CreateParameter("@eventName", adVarChar, adParamInput, 100, eventName)
		   .parameters.Append .CreateParameter("@eventDay", adInteger, adParamInput, 8, eventDay)
		   .parameters.Append .CreateParameter("@eventMonth", adInteger, adParamInput, 8, eventMonth)
		   .parameters.Append .CreateParameter("@eventYear", adInteger, adParamInput, 8, eventYear)
		   .parameters.Append .CreateParameter("@timeFrom", adVarChar, adParamInput, 50, timeFrom)
		   .parameters.Append .CreateParameter("@timeTo", adVarChar, adParamInput, 50, timeTo)
		   .parameters.Append .CreateParameter("@eventDescription", adVarChar, adParamInput, 500, eventDescription)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "calendar.asp"
		
	case "editEvent"
	
		eventID= request("eventID")
		eventName = request("eventName")
		eventDate = request("eventDate")
		timeFrom= request("timeFrom")
		timeTo= request("timeTo")
		eventDescription= request("eventDescription")
		
		
		eventDay = cDate(day(eventDate))
		eventMonth = cDate(month(eventDate))
		eventYear = cDate(year(eventDate))

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditEvent"
		   .parameters.Append .CreateParameter("@eventID", adInteger, adParamInput, 8, eventID)
		   .parameters.Append .CreateParameter("@eventName", adVarChar, adParamInput, 100, eventName)
		   .parameters.Append .CreateParameter("@eventDay", adInteger, adParamInput, 8, eventDay)
		   .parameters.Append .CreateParameter("@eventMonth", adInteger, adParamInput, 8, eventMonth)
		   .parameters.Append .CreateParameter("@eventYear", adInteger, adParamInput, 8, eventYear)
		   .parameters.Append .CreateParameter("@timeFrom", adVarChar, adParamInput, 50, timeFrom)
		   .parameters.Append .CreateParameter("@timeTo", adVarChar, adParamInput, 50, timeTo)
		   .parameters.Append .CreateParameter("@eventDescription", adVarChar, adParamInput, 500, eventDescription)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "calendar.asp"
		
	case "deleteEvent"
		eventID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteEvent"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, eventID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "calendar.asp"
		
	case "addDivision"
	
		customer = request("customer")
		division = request("division")
		clientID = request("clientID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddDivision"
		   .parameters.Append .CreateParameter("@customer", adInteger, adParamInput, 8, customer)
		   .parameters.Append .CreateParameter("@division", adVarChar, adParamInput, 200, division)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		divisionID = rs("Identity")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		buildDefaultLists customer,divisionID, clientID
		
		response.Redirect "divisionList.asp?id=" & customer
		
	case "editDivision"
		divisionID = request("divisionID")
		customer = request("customer")
		division = request("division")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditDivision"
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .parameters.Append .CreateParameter("@division", adVarChar, adParamInput, 200, division)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "divisionList.asp?id=" & customer
		
	case "deleteDivision"
		divisionID = Request("id")
		customer = request("customerID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteDivision"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "divisionList.asp?id=" & customer
		
	Case "sendEmail"
		'need to change
		'sServerPath = "/test6/noi/downloads/"
		sTo = request("emailTo")
		sCC = request("emailCC")
		sBCC = request("emailBCC")
		sSubject = request("subject")
		sBody = request("smessage")
		sAttach = request("attachment1")
		sAttach2 = request("attachment2")
		sAttach3 = request("attachment3")
		reportID = request("reportID")
		quoteID = request("quoteID")
		emailType = request("emailType")
		'clientID = request("client")
		
		reportPDF = isOn(request("reportPDF"))
		openItemsPDF = isOn(request("openItemsPDF"))
		correctedItemsPDF = isOn(request("correctedItemsPDF"))
		workOrderPDF = isOn(request("workOrderPDF"))
		quotePDF = isOn(request("quotePDF"))
		
		'if the email is a CWMatthews account
		'use a dummy email address
		If session("clientID") = 3 then
			sFrom = "admin@nextnpdes.com"
			sBody = sBody & "<br><br>Sent by: " & Session("Email")
		else
			sFrom = Session("Email")
		end if
		
		
		If quotePDF = "True" Then
			insAttach = sAttach
		else
			insAttach = ""
		end if
		
		If workOrderPDF = "True" Then
			insAttach = sAttach
		else
			insAttach = ""
		end if
		
		If reportID <> "" then
			If reportPDF = "True" Then
				insAttach = sAttach
			else
				insAttach = ""
			end if
		End if
		
		If openItemsPDF = "True" Then
			insAttach2 = sAttach2
		else
			insAttach2 = ""
		end if
		
		If correctedItemsPDF = "True" Then
			insAttach3 = sAttach3
		else
			insAttach3 = ""
		end if
		
		'set for each form type
		If reportPDF = "True" Then
			if sAttach <> "" then
				sAttach = Server.MapPath(sServerPath & sAttach)
			end if
		End If
		
		If workOrderPDF = "True" Then
			if sAttach <> "" then
				sAttach = Server.MapPath(sServerPath & sAttach)
			end if
		End If
		
		If quotePDF = "True" Then
			if sAttach <> "" then
				sAttach = Server.MapPath(sServerPath & sAttach)
			end if
		End If

		'set for each form type
		If openItemsPDF = "True" Then
			if sAttach2 <> "" then
				sAttach2 = Server.MapPath(sServerOIPath & sAttach2)
			end if
		End If
		
		'set for each form type
		If correctedItemsPDF = "True" Then
			if sAttach3 <> "" then
				sAttach3 = Server.MapPath(sServerOIPath & sAttach3)
			end if
		End If

		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		

		
		'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
 		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
	    with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
	    end with
		
		with oCdoMail
		  .From = sFrom
		  .To = sTo
		  .CC = sCC
		  .BCC = sBCC
	    end with
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  If reportPDF = "True" Then
		  	.AddAttachment (sAttach)
		  end if
		  If openItemsPDF = "True" Then
		  	.AddAttachment (sAttach2)
		  end if
		  If correctedItemsPDF = "True" Then
		  	.AddAttachment (sAttach3)
		  end if
		  
		  If workOrderPDF = "True" Then
		  	.AddAttachment (sAttach)
		  end if
		  
		  If quotePDF = "True" Then
		  	.AddAttachment (sAttach)
		  end if
	    end with
	
		oCdoMail.Configuration = oCdoConf
	    oCdoMail.Send
	    Set oCdoConf = Nothing
	    Set oCdoMail = Nothing

		bSent = True

		'############################################################################################################
		
		'add the email details to the database		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddEmail"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
		   .parameters.Append .CreateParameter("@sendto", adVarChar, adParamInput, 8000, sTo)
		   .parameters.Append .CreateParameter("@cc", adVarChar, adParamInput, 8000, sCC)
		   .parameters.Append .CreateParameter("@bcc", adVarChar, adParamInput, 8000, sBCC)
		   .parameters.Append .CreateParameter("@subject", adVarChar, adParamInput, 8000, sSubject)
		   .parameters.Append .CreateParameter("@message", adVarChar, adParamInput, 8000, sBody)
		   .parameters.Append .CreateParameter("@attach1", adVarChar, adParamInput, 200, insAttach)
		   .parameters.Append .CreateParameter("@attach2", adVarChar, adParamInput, 200, insAttach2)
		   .parameters.Append .CreateParameter("@attach3", adVarChar, adParamInput, 200, insAttach3)
		   .parameters.Append .CreateParameter("@dateSent", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		
		'log the details
		If reportID <> "" then
			logDetails Session("ID"),reportID,"Report E-Mailed","Report e-mailed to: " & sTo
		end if
		
		'response.Write "sAttach " & sAttach & "<br>"
		'response.Write "insAttach " & insAttach
	
		'send the user to sent page
		response.Redirect "email_sent.asp?emailType=" & emailType & "&clientID=" & session("clientID")
		
	case "deleteEmail"
		emailID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteEmail"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, emailID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "emailList.asp"
		
	case "archiveEmail"
		emailID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spArchiveEmail"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, emailID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "emailList.asp"
		
	case "addProductCategory"
	
		projectID = request("projectID")
		category = request("category")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddProductCategory"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@category", adVarChar, adParamInput, 150, category)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "ecProductCategoryList.asp?projectID=" & projectID
		
	case "editProductCategory"
		
		categoryID = request("categoryID")
		projectID = request("projectID")
		category = request("category")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditProductCategory"
		   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
		   .parameters.Append .CreateParameter("@category", adVarChar, adParamInput, 150, category)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "ecProductCategoryList.asp?projectID=" & projectID
		
	case "deleteProductCategory"
		categoryID = request("id")
		projectID = request("projectID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteProductCategory"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, categoryID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "ecProductCategoryList.asp?projectID=" & projectID
		
	case "addProduct"
	
		product = request("product")
		projectID = request("projectID")
		unit = request("unit")
		pricePerUnit = request("pricePerUnit")
		categoryID = request("categoryID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddProduct"
		   .parameters.Append .CreateParameter("@product", adVarChar, adParamInput, 150, product)
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@unit", adVarChar, adParamInput, 150, unit)
		   .parameters.Append .CreateParameter("@pricePerUnit", adCurrency, adParamInput, 8, pricePerUnit)
		   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "ecPropertyList.asp?projectID=" & projectID
		
	case "editProduct"
	
		product = request("product")
		projectID = request("projectID")
		productID = request("productID")
		unit = request("unit")
		pricePerUnit = request("pricePerUnit")
		categoryID = request("categoryID")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditProduct"
		   .parameters.Append .CreateParameter("@productID", adInteger, adParamInput, 8, productID)
		   .parameters.Append .CreateParameter("@product", adVarChar, adParamInput, 150, product) 
		   .parameters.Append .CreateParameter("@unit", adVarChar, adParamInput, 150, unit)
		   .parameters.Append .CreateParameter("@pricePerUnit", adCurrency, adParamInput, 8, pricePerUnit)
		   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)  
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "ecPropertyList.asp?projectID=" & projectID
		
	case "deleteProduct"
		projectID = request("projectID")
		productID = request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteProduct"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, productID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "ecPropertyList.asp?projectID=" & projectID
		
	Case "addWorkOrder"
		projectID = request("projectID")
		itemNumber = request("itemNumber")
		generatedBy = request("generatedBy")
		
		'add the work order to the database and return the workOrderID
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddWorkOrder"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@generatedBy", adInteger, adParamInput, 8, generatedBy)
		   .parameters.Append .CreateParameter("@dateCreated", adDBTimeStamp, adParamInput, 8, now())   
		   .CommandType = adCmdStoredProc
		End With
			
		Set rs = oCmd.Execute
		
		workOrderID = rs("Identity")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		For i = 1 to itemNumber
				productID = "productID" & i
				quantity = "quantity" & i
				sdescription = "description" & i
				
				productID = request.Form(productID)
				quantity = request.Form(quantity)
				sdescription = request.Form(sdescription)
				
				'response.Write productID & "<br>"
				'response.Write quantity & "<br>"
				'response.Write sdescription & "<br><br>"

				If productID <> "" then
					'add the data to the daily table
					Set oCmd2 = Server.CreateObject("ADODB.Command")
					
					With oCmd2
					   .ActiveConnection = DataConn
					   .CommandText = "spAddWorkOrderItem"
					   .parameters.Append .CreateParameter("@workOrderID", adInteger, adParamInput, 8, workOrderID)
					   .parameters.Append .CreateParameter("@productID", adInteger, adParamInput, 8, productID)
					   .parameters.Append .CreateParameter("@quantity", adInteger, adParamInput, 8, quantity)
					   .parameters.Append .CreateParameter("@description", adVarChar, adParamInput, 200, sdescription)
					   .parameters.Append .CreateParameter("@isApproved", adBoolean, adParamInput, 1, 0)
					   .CommandType = adCmdStoredProc
					End With
					
					Set rs = oCmd2.Execute
					
					If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
					End If		
				End if
		Next

		'log the details
		'logDetails Session("ID"),reportID,"Report Edited","Report and PDF document Edited"		
		
		'generate the PDF of the work order
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildWorkOrderPDF.asp?workOrderID=" & workOrderID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/workOrder_" & workOrderID & ".pdf"), true )		
		
		response.Redirect "ecReportList.asp?findWorkOrder=" & workOrderID
		
	Case "emailUP"
		'need to change
		'sServerPath = "/test6/noi/downloads/"
		refer = request("refer")
		appLogo = request("appLogo")
		email = request("email")
		
		'retrieve the username and password
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
		DataConn.Open Session("Connection"), Session("UserID")
	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUsernamePassword"
		   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 100, email)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		If not rs.eof then
			
			sTo = email
			sSubject = "NPDES Login Info"
			sBody = "Please do not respond to this email.<br><br>"
			sBody = sBody & "Here is your login info that you requested:<br><br>"
			sBody = sBody & "Username: " & rs("username") & "<br>"
			sBody = sBody & "Password: " & rs("password") & "<br><br>"
			
			If refer <> "" then
				sBody = sBody & "<a href=" & refer & ">Please click here to login</a>"
			else
				sBody = sBody & "<a href=http://swsir.nextnpdes.com>Please click here to login</a>"
			end if
			
			'############################################################################################################
			Set oCdoMail = Server.CreateObject("CDO.Message")
			Set oCdoConf = Server.CreateObject("CDO.Configuration")
			
			If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
			with oCdoConf
			  .Fields.Item(sConfURL & "sendusing") = 2
			  .Fields.Item(sConfURL & "smtpserver") = "localhost"
			  .Fields.Item(sConfURL & "smtpserverport") = 25
			  .Fields.Update
			end with
			
			with oCdoMail
			  .From = "support@hbnext.com"
			  .To = sTo
			end with
			
			with oCdoMail
			  .Subject = sSubject
			  .TextBody = sBody
			  .HTMLBody = sBody
			end with
		
			oCdoMail.Configuration = oCdoConf
			oCdoMail.Send
			Set oCdoConf = Nothing
			Set oCdoMail = Nothing
	
			bSent = True
	
			'############################################################################################################
			
			response.Redirect "up_sent.asp?appLogo=" & appLogo & "&refer=" & refer
			
		else
			msg = "The email address you entered is not in our system.<br>"
			If refer <> "" then
				msg = msg & "<a href=form_addUser.asp?appLogo=" & appLogo & "&refer=" & refer & ">Please click here to create an account.</a>"
			else
				msg = msg & "<a href=form_addUser.asp>Please click here to create an account.</a>"
			end if
			
			response.Redirect "emailUP.asp?refer=" & refer & "&msg=" & msg
		end if

	case "addStreet"
	
		customer = request("customer")
		projectID = request("projectID")
		divisionID = request("divisionID")
		streetName = request("streetName")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddStreet"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@streetName", adVarChar, adParamInput, 250, streetName)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "streetList.asp?id=" & projectID & "&customerID=" & customer & "&divisionID=" & divisionID
		
	case "editStreet"
	
		streetID = request("streetID")
		customer = request("customer")
		projectID = request("projectID")
		divisionID = request("divisionID")
		streetName = request("streetName")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditStreet"
		   .parameters.Append .CreateParameter("@streetID", adInteger, adParamInput, 8, streetID)
		   .parameters.Append .CreateParameter("@streetName", adVarChar, adParamInput, 250, streetName)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "streetList.asp?id=" & projectID & "&customerID=" & customer & "&divisionID=" & divisionID
		
	case "deleteStreet"
		streetID = Request("id")
		customer = request("customer")
		projectID = request("projectID")
		divisionID = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteStreet"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, streetID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "streetList.asp?id=" & projectID & "&customerID=" & customer & "&divisionID=" & divisionID
		
	case "addECDevice"
	
		device = request("device")
		customerID = request("customerID")
		division = request("divisionID")
		sortNumber = request("sortNumber")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddECDevice"
		   .parameters.Append .CreateParameter("@device", adVarChar, adParamInput, 250, device)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, division)
		   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, sortNumber)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing

		response.Redirect "ecDeviceList.asp?id=" & division & "&customerID=" & customerID
		
	case "editECDevice"
	
		deviceID = request("deviceID")
		device = request("device")
		customerID = request("customerID")
		division = request("divisionID")
		sortNumber = request("sortNumber")

		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditECDevice"
		   .parameters.Append .CreateParameter("@deviceID", adInteger, adParamInput, 8, deviceID)
		   .parameters.Append .CreateParameter("@device", adVarChar, adParamInput, 250, device)
		   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, sortNumber)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing

		response.Redirect "ecDeviceList.asp?id=" & division & "&customerID=" & customerID
		
	case "deleteECDevice"
		deviceID = Request("id")
		customerID = request("customerID")
		division = request("divisionID")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteECDevice"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, deviceID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		response.Redirect "ecDeviceList.asp?id=" & division & "&customerID=" & customerID
		
	Case "sendSupportRequest"
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 			
		DataConn.Open Session("Connection"), Session("UserID")
		'add the support request to the database
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddSupportTicket"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
		   .parameters.Append .CreateParameter("@importance", adVarChar, adParamInput, 50, request("importance"))
		   .parameters.Append .CreateParameter("@manager", adVarChar, adParamInput, 50, request("manager"))
		   .parameters.Append .CreateParameter("@category", adVarChar, adParamInput, 50, request("category"))
		   .parameters.Append .CreateParameter("@message", adVarChar, adParamInput, 8000, request("smessage"))
		   .parameters.Append .CreateParameter("@dateAdded", adDBTimeStamp, adParamInput, 8, now())
		   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, False)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsSupport = oCmd.Execute
		Set oCmd = nothing
		
		ticketNumber = rsSupport("Identity")
		
		
		sTo = "rreynolds@hbnext.com;amiddlebrooks@hbnext.com"
		sCC = request("from")
		sSubject = "HB NEXT Support Request Ticket #" & ticketNumber & " (do not reply)"
		sBody = sBody & "<font style=font-family:Arial, Helvetica, sans-serif;font-size:10px><b>Importance:</b> " & request("importance") & "<br><br>"
		sBody = sBody & "<b>Manager:</b> " & request("manager") & "<br><br>"
		sBody = sBody & "<b>Category:</b> " & request("category") & "<br><br>"
		sBody = sBody & "<b>Message:</b> " & request("smessage") & "</font>"
		
		'send a message to the requestor
		sBodyThanks = sBodyThanks & "<font style=font-family:Arial, Helvetica, sans-serif;font-size:10px>Support Ticket #" & ticketNumber & "<br>"
		sBodyThanks = sBodyThanks & "Thank you for submitting your request for support.<br><br>"
		sBodyThanks = sBodyThanks & "We have received your request and will respond as soon as possible. We will do everything possible to handle your request within 24 hours.<br><br>Thank you from your HB NEXT support team.<br><br>"
		sBodyThanks = sBodyThanks & "Your message is as follows:<br><br>"
		sBodyThanks = sBodyThanks & "<b>" & sBody & "</b></font>"
		
		
		'send the support request
		'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
 		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
	    with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
	    end with
		
		with oCdoMail
		  .From = sCC
		  .To = sTo
		  '.CC = sCC
	    end with
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
	    end with
	
		oCdoMail.Configuration = oCdoConf
	    oCdoMail.Send
	    Set oCdoConf = Nothing
	    Set oCdoMail = Nothing

		bSent = True

		'############################################################################################################
		
		'send the thank you
		'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
 		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
	    with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
	    end with
		
		with oCdoMail
		  .From = "support@hbnext.com"
		  .To = sCC
	    end with
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBodyThanks
		  .HTMLBody = sBodyThanks
	    end with
	
		oCdoMail.Configuration = oCdoConf
	    oCdoMail.Send
	    Set oCdoConf = Nothing
	    Set oCdoMail = Nothing

		bSent = True

		'############################################################################################################
		
		'add the email details to the database	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddEmail"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
		   .parameters.Append .CreateParameter("@sendto", adVarChar, adParamInput, 8000, sTo)
		   .parameters.Append .CreateParameter("@cc", adVarChar, adParamInput, 8000, "")
		   .parameters.Append .CreateParameter("@bcc", adVarChar, adParamInput, 8000, "")
		   .parameters.Append .CreateParameter("@subject", adVarChar, adParamInput, 8000, sSubject)
		   .parameters.Append .CreateParameter("@message", adVarChar, adParamInput, 8000, sBody)
		   .parameters.Append .CreateParameter("@attach1", adVarChar, adParamInput, 200, "")
		   .parameters.Append .CreateParameter("@attach2", adVarChar, adParamInput, 200, "")
		   .parameters.Append .CreateParameter("@attach3", adVarChar, adParamInput, 200, "")
		   .parameters.Append .CreateParameter("@dateSent", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute
		Set oCmd = nothing		

		'send the user to sent page
		response.Redirect "support_sent.asp"
		
	Case "sendUsersEmails"
		'need to change
		'sServerPath = "/test6/noi/downloads/"
		from = request("from")
		userList = request("userList")
		sSubject = "Message from NEXT"
		sBody = request("smessage")
		
		'get the user list as defined by what the user chose
		Select Case userList
			Case "AllUsers" 'send to all active users
				sp = "spGetAllUsers"
			Case "All" 'send to all active users
				sp = "spGetActiveUsers"
			Case "Administrators" ' send to only the administrators
				sp = "spGetAdministrators"
			Case "Inspectors"
				sp = "spGetInspectors"
			Case "NOI"
				sp = "spGetNOIUsers"
		end select
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
		DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = sp
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
			
		'loop through the recordset and send an email to each user
		'build the list
		do until rs.eof
			sTo = sTo & rs("email") & ";"
		rs.movenext
		loop
		
		'response.Write sTo
		'############################################################################################################
		
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
	
	
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = Session("Email")
		  .BCC = sTo
		end with
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

		bSent = True
			
		

		'############################################################################################################
		
		rs.close
		set rs = nothing
	
		'send the user to sent page
		response.Redirect "email_sent.asp"
		
	'case "test"
	'	projectID=8
	'	workOrderID = 32

	'	If autoEmail(8) = true then
	'		response.Write "here<br>"
	'		sendAutoEmailWO workOrderID,projectID 'workorderID
	'	end if
	'	
	'	response.Write "done"
		
		
	Case else%>
		Your request could not be processed at this time.		  
	
<%end select


function getAdminEmail(clientID)

	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	DataConn.Open Session("Connection"), Session("UserID")
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAdminEmail"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	
	do until rs.eof
		sEmail = sEmail & rs("email") & ";"
	rs.movenext
	loop
	getAdminEmail = sEmail
	
	rs.close
	set rs = nothing
	Set oCmd = nothing

end function

function getPrimaryInspectorEmail(userID)
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 		
	DataConn.Open Session("Connection"), Session("UserID")
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetUser"
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
	   .CommandType = adCmdStoredProc	   
	End With				
	Set rs = oCmd.Execute
	
	'do until rs.eof
		sEmail = rs("email")' & ";"
	'rs.movenext
	'loop
	getPrimaryInspectorEmail = sEmail
	
	rs.close
	set rs = nothing
	Set oCmd = nothing
end function

Function logDetails(userID,repID,eventType,eventDetails)
	
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	DataConn.Open Session("Connection"), Session("UserID")
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spLogDetails"
	   .parameters.Append .CreateParameter("@activityDate", adDBTimeStamp, adParamInput, 8, now())
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
	   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, repID)
	   .parameters.Append .CreateParameter("@eventType", adVarChar, adParamInput, 100, eventType)
	   .parameters.Append .CreateParameter("@eventDetails", adVarChar, adParamInput, 1000, eventDetails)
	   .CommandType = adCmdStoredProc	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsLog = oCmd.Execute
	
	rsLog.close
	set rsLog = nothing
	Set oCmd = nothing
	
	logDetails = true
	
End Function

Function autoEmail(projectID)
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
		
	DataConn.Open Session("Connection"), Session("UserID")

	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spCheckAutoEmail"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc	   
	End With
		
		If Err Then
	%>
	3		<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsAutoEmail = oCmd.Execute
	
	bAuto = rsAutoEmail("autoEmail")
	
	rsAutoEmail.close
	set rsAutoEmail = nothing
	Set oCmd = nothing
	
	If bAuto = True then
		autoEmail = True
	else
		autoEmail = False
	end if
		
End Function

function sendAutoEmailWO(workOrderID,projectID,sendPDF)
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	'get the project info(name)
	Set oCmd = Server.CreateObject("ADODB.Command")		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProject"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsProject = oCmd.Execute
	set oCmd = nothing
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetContactsByProjectAuto"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsContact = oCmd.Execute
	set oCmd = nothing
	
	If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
	
	if rsContact.eof then
		'there isn't at least one recipient
		bSendEmail = false
	else
		'there is at least one recipient
		bSendEmail = true
		Do until rsContact.eof
			sTo = sTo & rsContact("email")
		rsContact.movenext
			if not rsContact.eof then
				sTo = sTo & ";"
			end if
		loop
	end if
	
	
	'build the recipient list
'	Do until rsContact.eof
'		sTo = sTo & rsContact("email")
'	rsContact.movenext
'	if not rsContact.eof then
'		sTo = sTo & ";"
'	end if
'	loop
	
	If bSendEmail = true then
	
		if sendPDF = "True" then
			'get the work order attachments
			sAttach = "workOrder_" & workOrderID & ".pdf"
			insAttach = sAttach
			sAttach = Server.MapPath(sServerPath & sAttach)
		end if
		
		'**************send the email************************************************
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = Session("Email")
		  .To = sTo
		end with
		
		sSubject = "Work Order for " & rsProject("projectName") & " - " & date()
		if sendPDF = "True" then
			sBody = "Work Order # " & workOrderID
		else
			sBody = "Work Order # " & workOrderID & " has been created and is awaiting approval."
		end if
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		if sendPDF = "True" then
		  .AddAttachment (sAttach)
		end if
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing
	
		bSent = True
		'*****************************************************************************************************
		sCC = ""
		sBCC = ""
		insAttach2 = ""
		insAttach3 = ""
		'add the email details to the database		
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddEmail"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
		   .parameters.Append .CreateParameter("@sendto", adVarChar, adParamInput, 8000, sTo)
		   .parameters.Append .CreateParameter("@cc", adVarChar, adParamInput, 8000, sCC)
		   .parameters.Append .CreateParameter("@bcc", adVarChar, adParamInput, 8000, sBCC)
		   .parameters.Append .CreateParameter("@subject", adVarChar, adParamInput, 8000, sSubject)
		   .parameters.Append .CreateParameter("@message", adVarChar, adParamInput, 8000, sBody)
		   .parameters.Append .CreateParameter("@attach1", adVarChar, adParamInput, 200, insAttach)
		   .parameters.Append .CreateParameter("@attach2", adVarChar, adParamInput, 200, insAttach2)
		   .parameters.Append .CreateParameter("@attach3", adVarChar, adParamInput, 200, insAttach3)
		   .parameters.Append .CreateParameter("@dateSent", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
	
		Set rsAdd = oCmd.Execute
		Set oCmd = nothing
		
		'response.Write "sent"
		
	end if
	

end function



 
function sendNoticeOfSeparationEmail (noticeOfSeparationID)
	
	On Error Resume Next
	
	'get the email address to send to
	sTo = "rayr@cwmatthews.com;susans@cwmatthews.com;kellyo@cwmatthews.com;mluke@cwmatthews.com"

	
	sFrom = session("email")
	
	'get the attachments
	sAttach = "noticeOfSeparation_" & noticeOfSeparationID & ".pdf"
	sAttach = Server.MapPath(sServerPath & sAttach)
		
	'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = sFrom
		  .To = sTo
		  .CC = ""
		  .BCC = ""
		end with
		sSubject = "Notice Of Separation - " & date()
		sBody = "Notice Of Separation # " & noticeOfSeparationID
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  .AddAttachment (sAttach)
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

		bSent = True
	
	sendNoticeOfSeparationEmail = True
end function

function sendWorkCompEmail (workersCompID)
	
	On Error Resume Next
	
	'get the email address to send to
	sTo = "lorit@cwmatthews.com"
		
'sTo = "rreynolds@nextnpdes.com"
	
	sFrom = session("email")
	
	'get the attachments
	sAttach = "workersComp_" & workersCompID & ".pdf"
	sAttach = Server.MapPath(sServerPath & sAttach)
		
	'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = sFrom
		  .To = sTo
		  .CC = ""
		  .BCC = ""
		end with
		sSubject = "Workers Compensation Report - " & date()
		sBody = "Workers Compensation # " & workersCompID
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  .AddAttachment (sAttach)
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

		bSent = True
	
	sendWorkCompEmail = True
end function


function sendRiskEmailApprove(accidentID,userID,clientID)
	On Error Resume Next

	
	'get the superintendent's email address to route the form to
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetRiskEmails"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsRisk = oCmd.Execute
	set oCmd = nothing
	
	do until rsRisk.eof
		sREmail = sREmail & rsRisk("email")
	rsRisk.movenext
	if not rsRisk.eof then
		sREmail = ";"
	end if
	loop
	
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetSupervisors"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsUser = oCmd.Execute
	set oCmd = nothing
	
	do until rsUser.eof
		sUEmail = sUEmail & rsUser("email") & ";"
	rsUser.movenext
	if not rsRisk.eof then
		sUEmail = ";"
	end if
	loop
	
	sTo = sREmail & ";" & sUEmail
	
	sFrom = session("email")
	
	'get the attachments
	sAttach = "accidentReport_" & accidentID & ".pdf"
	sAttach = Server.MapPath(sServerPath & sAttach)
		
	'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = sFrom
		  .To = sTo
		  .CC = ""
		  .BCC = ""
		end with
		sSubject = "Accident Report - " & date()
		sBody = "Accident # " & accidentID & "<br><br>"
		sBody = sBody & "This accident report has been approved."
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  .AddAttachment (sAttach)
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

		bSent = True
	
	sendRiskEmailApprove = True
end function

function sendRiskEmailDeny(accidentID,userID,clientID)
	On Error Resume Next
	
	'get the superintendent's email address to route the form to
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetRiskEmails"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsRisk = oCmd.Execute
	set oCmd = nothing
	
	do until rsRisk.eof
		sREmail = sREmail & rsRisk("email")
	rsRisk.movenext
	if not rsRisk.eof then
		sREmail = ";"
	end if
	loop
	
	sTo = sREmail
	
	sFrom = session("email")
	
	'get the attachments
	sAttach = "accidentReport_" & accidentID & ".pdf"
	sAttach = Server.MapPath(sServerPath & sAttach)
		
	'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = sFrom
		  .To = sTo
		  .CC = ""
		  .BCC = ""
		end with
		sSubject = "Accident Report - " & date()
		sBody = "Accident # " & accidentID & "<br><br>"
		sBody = sBody & "This accident report has been denied. Please log-in to Sequence, edit and re-route to the correct supervisor."
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  .AddAttachment (sAttach)
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

		bSent = True
	
	sendRiskEmailDeny = True
end function


function sendRiskEmail(accidentID,accidentTypeID,generalSuper)
	
	On Error Resume Next
	
	'get the email address to send to
'	select Case accidentTypeID
'		case 1 'Vehicular Accident Report
'			sTo = "sandyo@cwmatthews.com;lorit@cwmatthews.com;mking@cwmatthews.com"
'		case 2 'Equipment Damage Report
'			sTo = "lorit@cwmatthews.com"
'		Case 3 'Utility Damage Report
'			sTo = "sandyo@cwmatthews.com;mking@cwmatthews.com"
'	end select
	'sTo = "rreynolds@nextnpdes.com"
	
	'get the superintendent's email address to route the form to
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetUser"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, generalSuper)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsUser = oCmd.Execute
	set oCmd = nothing
	
	sTo = rsUser("email")
	
	sFrom = session("email")
	
	'get the attachments
	sAttach = "accidentReport_" & accidentID & ".pdf"
	sAttach = Server.MapPath(sServerPath & sAttach)
		
	'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = sFrom
		  .To = sTo
		  .CC = ""
		  .BCC = ""
		end with
		sSubject = "Accident Report - " & date()
		sBody = "Accident # " & accidentID & "<br><br>"
		sBody = sBody & "Please log into the Sequence application and approve or deny this accident report."
		
		'if the email is a CWMatthews account
		'add to the body of the email
	'	If session("clientID") = 3 then
	'		sBody = sBody & "<br><br>Sent by: " & Session("Email")
	'	end if
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  .AddAttachment (sAttach)
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

		bSent = True
	
	sendRiskEmail = True
end function

function emailInfoPack(infoPackID,userID,contactID,isVerified)
	
	if isVerified = "True" Then
	
		On Error Resume Next
		
		Set DataConn = Server.CreateObject("ADODB.Connection") 		
		DataConn.Open Session("Connection"), Session("UserID")
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetCustomerContact"
		   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, contactID)
		   .CommandType = adCmdStoredProc	   
		End With
		
		Set rsContact = oCmd.Execute
		set oCmd = nothing
		
		sAttach = "InfoPack_" & infoPackID & ".pdf"
		sAttach = Server.MapPath(sServerPath & sAttach)
		
		'get the originator
		Set oCmd = Server.CreateObject("ADODB.Command")			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUser"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
		   .CommandType = adCmdStoredProc	   
		End With
		
		Set rsUser = oCmd.Execute
		set oCmd = nothing
		
		'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = rsUser("email")
		  .To = rsContact("contactEmail")
		  .CC = ""
		  .BCC = rsUser("email") & ";kevin@hbnext.com"
		end with
		
	'	sGeneralFooter = "<span style=" & chr(34) & "FONT-SIZE: 12pt; COLOR: #000080; FONT-FAMILY: Times New Roman" & chr(34) & ">"
	'	sGeneralFooter = sGeneralFooter & "<div class=MsoNormal><span style=" & chr(34) & "FONT-SIZE: 12pt; COLOR: #000080; FONT-FAMILY: Times New Roman" & chr(34) & "></span><span " & chr(34) & "style=FONT-SIZE: 12pt; COLOR: #000080; FONT-FAMILY: Times New Roman" & chr(34) & "></span><span style=" & chr(34) & "FONT-SIZE: 12pt; COLOR: #999999; FONT-FAMILY: Times New Roman" & chr(34) & ">HB NEXT</span></div>"
	'	sGeneralFooter = sGeneralFooter & "<div class=MsoNormal><span style=" & chr(34) & "FONT-SIZE: 12pt; COLOR: #999999; FONT-FAMILY: Times New Roman" & chr(34) & "></span><strong><b><span style=" & chr(34) & "FONT-SIZE: 12pt; COLOR: #000080; FONT-FAMILY: Times New Roman" & chr(34) & ">office </span></b></strong><span style=" & chr(34) & "COLOR: #999999" & chr(34) & ">678-336-1357 </span><strong><b><span style=" & chr(34) & "COLOR: #000080; FONT-FAMILY: Times New Roman" & chr(34) & ">tf</span></b></strong><span style=" & chr(34) & "COLOR: #000080" & chr(34) & "> </span>1-866-639-8552 � <strong>fax </strong><span style=" & chr(34) & "COLOR: #999999" & chr(34) & ">678-336-1358� </span><b><span style=" & chr(34) & "FONT-WEIGHT: bold; COLOR: #000080" & chr(34) & ">cell</span></b><span style=" & chr(34) & "COLOR: #999999" & chr(34) & "> " &  rsUser("cellPhone") & "</span></div>"
	'	sGeneralFooter = sGeneralFooter & "<div class=" & chr(34) & "MsoNormal" & chr(34) & "><span style=" & chr(34) & "COLOR: #999999" & chr(34) & "></span><em><i><span style=" & chr(34) & "FONT-SIZE: 12pt; COLOR: #808080; FONT-FAMILY: Times New Roman" & chr(34) & ">" & chr(34) & "The Power of Instant Access to Critical Data" & chr(34) & "</span></i></em></div>"
	'	sGeneralFooter = sGeneralFooter & "<div class=" & chr(34) & "MsoNormal" & chr(34) & "><em><i><span style=" & chr(34) & "FONT-SIZE: 12pt; COLOR: #808080; FONT-FAMILY: Times New Roman" & chr(34) & "> </span></i></em><span style=" & chr(34) & "FONT-SIZE: 12pt; FONT-FAMILY: Times New Roman" & chr(34) & "> <span style=" & chr(34) & "COLOR: #999999" & chr(34) & "><a title=" & chr(34) & "http://www.hbnext.com" & chr(34) & " href=" & chr(34) & "http://www.hbnext.com/" & chr(34) & "><span title=" & chr(34) & "http://www.hbnext.com" & chr(34) & " style=" & chr(34) & "COLOR: #999999" & chr(34) & ">www.hbnext.com</span></a>"
	'	sGeneralFooter = sGeneralFooter & "<o:p></p></span></span></div>"
	'	sGeneralFooter = sGeneralFooter & "<p class=" & chr(34) & "MsoNormal" & chr(34) & "><span style=" & chr(34) & "FONT-SIZE: 12pt; FONT-FAMILY: Times New Roman" & chr(34) & ">"
	'	sGeneralFooter = sGeneralFooter & "<o:p> </p></span><span style=" & chr(34) & "FONT-SIZE: 10pt; COLOR: #000080; FONT-FAMILY: Palatino Linotype" & chr(34) & ">The information contained in this transmission may contain privileged and confidential information.  It is intended only for the use of the person(s) named above. If you are not the intended recipient, you are hereby notified that any review, dissemination, distribution or duplication of this communication is strictly prohibited. If you are not the intended recipient, please contact the sender by reply email and destroy all copies of the original message. To reply to our email administrator directly, please send an email to <a title=" & chr(34) & "mailto:rreynolds@hbnext.com" & chr(34) & " href=" & chr(34) & "mailto:rreynolds@hbnext.com" & chr(34) & ">rreynolds@hbnext.com</a></span>"
	'	sGeneralFooter = sGeneralFooter & "<o:p></p></p></span></span></div></span>"	
		
		sSubject = "HB NEXT Information Pack"
		sBody = "Enclosed is an Information Package regarding our services.<br><br>Please call if you have any questions.<br><br>"
		sBody = sBody & rsUser("emailSignature")'sGeneralFooter
		
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  .AddAttachment (sAttach)
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing
	
		bSent = True			
		'############################################################################################################
		
	else
		'email the saved info pack for verification
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = Session("email")
		  .To = "kevin@hbnext.com;kkarikka@hbnext.com;rreynolds@hbnext.com"
		end with
		sSubject = "HB NEXT Information Pack"
		sBody = "There is a new HB NEXT info pack added to the system. Please log in to Sequence and verify the info pack."
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing
	
		bSent = True	
	end if
	
	emailInfoPack = true
	
end function

function sendAutoEmail(reportID,projectID,divisionID)
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	DataConn.Open Session("Connection"), Session("UserID")
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetEmailContent"
	   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
	   .CommandType = adCmdStoredProc	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsSend = oCmd.Execute
	set oCmd = nothing
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetContactsByProjectAuto"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsContact = oCmd.Execute
	set oCmd = nothing
	
	if rsContact.eof then
		'there isn't at least one recipient
		bSendEmail = false
	else
		'there is at least one recipient
		bSendEmail = true
		Do until rsContact.eof
			sTo = sTo & rsContact("email")
		rsContact.movenext
			if not rsContact.eof then
				sTo = sTo & ";"
			end if
		loop
	end if
	
	if bSendEmail = true then
	
		'if the email is a CWMatthews account
		'use a dummy email address
		If session("clientID") = 3 then
			sFrom = "admin@hbnext.com"
			'sBody = sBody & "<br><br>Sent by: " & Session("Email")
		else
			sFrom = Session("Email")
		end if
	
		'get the attachments
		sAttach = "swsir_" & reportID & ".pdf"
		insAttach = sAttach
		sAttach = Server.MapPath(sServerPath & sAttach)
		
	
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "printLogs.asp?projectID=" & projectID & "&type=Open&from=&to=&divisionID=" & divisionID, "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/openItems/OpenItems_" & projectID & ".pdf"), true )
		
		sAttach2 = "OpenItems_" & projectID & ".pdf"
		insAttach2 = sAttach2
		sAttach2 = Server.MapPath(sServerOIPath & sAttach2)
		Set Pdf = nothing
		
		'generate closed items log PDF to send
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "printLogs.asp?projectID=" & projectID & "&type=Corrected&from=&to=&divisionID=" & divisionID, "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/openItems/CorrectedItems_" & projectID & ".pdf"), true )
		
		sAttach3 = "CorrectedItems_" & projectID & ".pdf"
		insAttach3 = sAttach3
		sAttach3 = Server.MapPath(sServerOIPath & sAttach3)
		Set Pdf = nothing
		
		'############################################################################################################
			Set oCdoMail = Server.CreateObject("CDO.Message")
			Set oCdoConf = Server.CreateObject("CDO.Configuration")
			
			If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
			with oCdoConf
			  .Fields.Item(sConfURL & "sendusing") = 2
			  .Fields.Item(sConfURL & "smtpserver") = "localhost"
			  .Fields.Item(sConfURL & "smtpserverport") = 25
			  .Fields.Update
			end with
			
			with oCdoMail
			  .From = sFrom
			  .To = sTo
			  .CC = ""
			  .BCC = ""
			end with
			sSubject = rsSend("customerName") & " - " & rsSend("projectName") & " - " & rsSend("reportType") & " - " & date()
			sBody = "Report # " & reportID & "<br><br>"
			sBody = sBody & "To view open items or images for this report, click or paste the following link into your browser.<br>"
			sBody = sBody & "<a href=" & sPDFPath & "index.asp?sURLRed=openItems.asp?projectID=" & projectID & ">" & sPDFPath & "index.asp?sURLRed=openItems.asp?projectID=" & projectID & "</a>"
			sBody = sBody & "Please note: You will need a user account to log in with."
			
			'if the email is a CWMatthews account
			'add to the body of the email
			If session("clientID") = 3 then
				sBody = sBody & "<br><br>Sent by: " & Session("Email")
			end if
			
			with oCdoMail
			  .Subject = sSubject
			  .TextBody = sBody
			  .HTMLBody = sBody
			  .AddAttachment (sAttach)
			  .AddAttachment (sAttach2)
			  .AddAttachment (sAttach3)
			end with
		
			oCdoMail.Configuration = oCdoConf
			oCdoMail.Send
			Set oCdoConf = Nothing
			Set oCdoMail = Nothing
	
			bSent = True
			
				
			'############################################################################################################
			sCC = ""
			sBCC = ""
			'add the email details to the database		
			'Open connection and insert into the database
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddEmail"
			   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
			   .parameters.Append .CreateParameter("@sendto", adVarChar, adParamInput, 8000, sTo)
			   .parameters.Append .CreateParameter("@cc", adVarChar, adParamInput, 8000, sCC)
			   .parameters.Append .CreateParameter("@bcc", adVarChar, adParamInput, 8000, sBCC)
			   .parameters.Append .CreateParameter("@subject", adVarChar, adParamInput, 8000, sSubject)
			   .parameters.Append .CreateParameter("@message", adVarChar, adParamInput, 8000, sBody)
			   .parameters.Append .CreateParameter("@attach1", adVarChar, adParamInput, 200, insAttach)
			   .parameters.Append .CreateParameter("@attach2", adVarChar, adParamInput, 200, insAttach2)
			   .parameters.Append .CreateParameter("@attach3", adVarChar, adParamInput, 200, insAttach3)
			   .parameters.Append .CreateParameter("@dateSent", adDBTimeStamp, adParamInput, 8, now())
			   .CommandType = adCmdStoredProc
			End With
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			Set rsAdd = oCmd.Execute
			Set oCmd = nothing

			'log the details
			If reportID <> "" then
				logDetails Session("ID"),reportID,"Report E-Mailed","Report e-mailed to: " & sTo
			end if
			
	end if
	
	sendAutoEmail = true
	
end function

function checkEmailAddress(emailAddress)
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	DataConn.Open Session("Connection"), Session("UserID")
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spCheckEmailAddress"
	   .parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 100, emailAddress)
	   .CommandType = adCmdStoredProc
	   
	End With
	
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	
	if not rs.eof then
		checkEmailAddress = "true"
	else
		checkEmailAddress = "false"
	end if
		
	rs.close
	set rs = nothing
	Set oCmd = nothing
	
end function


Function buildDefaultLists(customerID,divisionID,clientID)

	'response.Write divisionID
	
	'Open connection and insert into the database
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	If divisionID = "0" then
	'	response.Write "test"
		'add the default division
		'and get the divisionID to use later
		division = "default"	
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddDivision"
		   .parameters.Append .CreateParameter("@customer", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@division", adVarChar, adParamInput, 200, division)
		   .CommandType = adCmdStoredProc
		End With
		Set rsDivision = oCmd.Execute
		
		divisionID = rsDivision("Identity")
		
		Set oCmd = nothing
		rsDivision.close
		set rsDivision = nothing
	end if
	
	'get the question categories from the default categories table
	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetDefaultCategories"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsDefaultCategories = oCmd.Execute
	Set oCmd = nothing
	
	i=1
	do until rsDefaultCategories.eof
		'create new question categories for this new project
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddCategory"
		   .parameters.Append .CreateParameter("@category", adVarChar, adParamInput, 200, rsDefaultCategories("category"))
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsCategory = oCmd.Execute
		categoryID = rsCategory("Identity")
		
		Set oCmd = nothing
		
		'get all of the default questions for this category
		Set oCmd = Server.CreateObject("ADODB.Command")							
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetDefaultQuestion"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsDefaultCategories("categoryID"))
		   .CommandType = adCmdStoredProc		   
		End With
		Set rsDefaultQuestion = oCmd.Execute
		Set oCmd = nothing
		
		'loop through the questions and insert them into the database
		do until rsDefaultQuestion.eof
		
			Set oCmd = Server.CreateObject("ADODB.Command")
			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spAddQuestion"
			   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, categoryID)
			   .parameters.Append .CreateParameter("@question", adVarChar, adParamInput, 1000, rsDefaultQuestion("question"))
			   .parameters.Append .CreateParameter("@secondLanguage", adVarChar, adParamInput, 1000, "")
			   .parameters.Append .CreateParameter("@sortNumber", adInteger, adParamInput, 8, rsDefaultQuestion("sortNumber"))
			   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)	
			   .parameters.Append .CreateParameter("@weekly", adBoolean, adParamInput, 1, rsDefaultQuestion("weekly"))
			   .parameters.Append .CreateParameter("@monthly", adBoolean, adParamInput, 1, rsDefaultQuestion("monthly"))	
			   .parameters.Append .CreateParameter("@postRain", adBoolean, adParamInput, 1, rsDefaultQuestion("postRain"))	 
			   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID) 
			   .parameters.Append .CreateParameter("@annual", adBoolean, adParamInput, 1, False)
			   .parameters.Append .CreateParameter("@questionType", adInteger, adParamInput, 8, rsDefaultQuestion("questionType"))  
			   .parameters.Append .CreateParameter("@petroleum", adBoolean, adParamInput, 1, False)
			   .parameters.Append .CreateParameter("@UtilBWPR", adBoolean, adParamInput, 1, False)
			   .CommandType = adCmdStoredProc
			End With
					
			Set rsQuestion = oCmd.Execute
			Set oCmd = Nothing
			
		rsDefaultQuestion.movenext
		loop
		
	rsDefaultCategories.movenext
	i=i+1
	loop
	

		
	i = 0
	'add the inspection types to the default division
	'get the question categories from the default categories table
	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetDefaultInspectionTypes"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsDefaultInspectionTypes = oCmd.Execute
	Set oCmd = nothing
	
	Do until rsDefaultInspectionTypes.eof
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddInspectionType"
		   .parameters.Append .CreateParameter("@inspectionType", adVarChar, adParamInput, 100, rsDefaultInspectionTypes("inspectionType"))
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsIT = oCmd.Execute
	rsDefaultInspectionTypes.movenext
	loop
	
	
	i = 0
	'add the weather conditions
	Set oCmd = Server.CreateObject("ADODB.Command")	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetDefaultWeatherTypes"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsDefaultWeatherTypes = oCmd.Execute
	Set oCmd = nothing
	
	Do until rsDefaultWeatherTypes.eof
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddWeatherType"
		   .parameters.Append .CreateParameter("@weatherType", adVarChar, adParamInput, 50,rsDefaultWeatherTypes("weatherType"))
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsWC = oCmd.Execute
	rsDefaultWeatherTypes.movenext
	loop
	
	
	i = 0
	'add the project status
	Set oCmd = Server.CreateObject("ADODB.Command")	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetDefaultProjectStatus"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsDefaultProjectStatus = oCmd.Execute
	Set oCmd = nothing
	
	Do until rsDefaultProjectStatus.eof
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddProjectStatus"
		   .parameters.Append .CreateParameter("@projectStatus", adVarChar, adParamInput, 50, rsDefaultProjectStatus("projectStatus"))
		   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsPS = oCmd.Execute
	rsDefaultProjectStatus.movenext
	loop
		
	
	
	buildDefaultLists = True
	

End function

Function addJournalEntry(userID,itemName,itemDay,itemMonth,itemYear,itemEntry,projectID)
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spAddJournal"
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
	   .parameters.Append .CreateParameter("@itemName", adVarChar, adParamInput, 100, itemName)
	   .parameters.Append .CreateParameter("@itemDay", adInteger, adParamInput, 8, itemDay)
	   .parameters.Append .CreateParameter("@itemMonth", adInteger, adParamInput, 8, itemMonth)
	   .parameters.Append .CreateParameter("@itemYear", adInteger, adParamInput, 8, itemYear)
	   .parameters.Append .CreateParameter("@itemEntry", adVarChar, adParamInput, 5000, itemEntry)
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc
	End With
	Set rs = oCmd.Execute
	addJournalEntry = true
end function


Function assignSuperAdmin(projectID)
	'get a list of the superAdmins
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 		
	DataConn.Open Session("Connection"), Session("UserID")
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetSuperAdmins"
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rs = oCmd.Execute
	
	do until rs.eof
	
		Set oCmd = Server.CreateObject("ADODB.Command")				
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddUserProject"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, rs("userID"))
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsUserProject = oCmd.Execute
		Set oCmd = nothing
		
	rs.movenext
	loop

	assignSuperAdmin = "True"
end function	

Function routeReport(reportID,userID)
	'email the report to the user
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")

	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetOSHAEmailContent"
	   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsSend = oCmd.Execute
	set oCmd = nothing
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetUser"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
	   .CommandType = adCmdStoredProc	   
	End With
	Set rsContact = oCmd.Execute
	set oCmd = nothing

	sTo = sTo & rsContact("email")
	sFrom = Session("Email")
	
	'get the attachments
	sAttach = "report_" & reportID & "Ent.pdf"
	insAttach = sAttach
	sAttach = Server.MapPath(sServerPath & sAttach)
	
	'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = sFrom
		  .To = sTo
		  .CC = ""
		  .BCC = ""
		end with
		select case rsSend("reportType")
			case 1
				sRepType = "Mock OSHA"
			case 2
				sRepType = "Mock EPA"
		end select
		sSubject = rsSend("customerName") & " - " & rsSend("projectName") & " - " & sRepType & " - " & date()
		sBody = "Report # " & reportID & "<br>Please review this report and log-in to the " & appName & " system to approve."
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  .AddAttachment (sAttach)
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

		bSent = True
		
			
		'############################################################################################################
'		sCC = ""
'		sBCC = ""
'		Set oCmd = Server.CreateObject("ADODB.Command")
'		
'		With oCmd
'		   .ActiveConnection = DataConn
'		   .CommandText = "spAddEmail"
'		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
'		   .parameters.Append .CreateParameter("@sendto", adVarChar, adParamInput, 8000, sTo)
'		   .parameters.Append .CreateParameter("@cc", adVarChar, adParamInput, 8000, sCC)
'		   .parameters.Append .CreateParameter("@bcc", adVarChar, adParamInput, 8000, sBCC)
'		   .parameters.Append .CreateParameter("@subject", adVarChar, adParamInput, 8000, sSubject)
'		   .parameters.Append .CreateParameter("@message", adVarChar, adParamInput, 8000, sBody)
'		   .parameters.Append .CreateParameter("@attach1", adVarChar, adParamInput, 200, insAttach)
'		   .parameters.Append .CreateParameter("@attach2", adVarChar, adParamInput, 200, insAttach2)
'		   .parameters.Append .CreateParameter("@attach3", adVarChar, adParamInput, 200, insAttach3)
'		   .parameters.Append .CreateParameter("@dateSent", adDBTimeStamp, adParamInput, 8, now())
'		   .CommandType = adCmdStoredProc
'		End With
'		Set rsAdd = oCmd.Execute
'		Set oCmd = nothing
'
		'log the details
		If reportID <> "" then
			logDetails Session("ID"),reportID,"Report E-Mailed for review","Report e-mailed to: " & sTo
		end if
	
	routeReport = true
end function


function notifyAdmintoCertify(reportID)
'spGetAdministrators
	'email the report to the user
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")

	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetOSHAEmailContent"
	   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsSend = oCmd.Execute
	set oCmd = nothing
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAdministrators"
	   .CommandType = adCmdStoredProc	   
	End With
	Set rsContact = oCmd.Execute
	set oCmd = nothing

	Do until rsContact.eof
		sTo = sTo & rsContact("email")
	rsContact.movenext
		if not rsContact.eof then
			sTo = sTo & ";"
		end if
	loop
	
	sFrom = Session("Email")

	sAttach = "report_" & reportID & "Ent.pdf"
	insAttach = sAttach
	sAttach = Server.MapPath(sServerPath & sAttach)
	'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = sFrom
		  .To = sTo
		  .CC = ""
		  .BCC = ""
		end with
		select case rsSend("reportType")
			case 1
				sRepType = "Mock OSHA"
			case 2
				sRepType = "Mock EPA"
		end select
		sSubject = rsSend("customerName") & " - " & rsSend("projectName") & " - " & sRepType & " - " & date()
		sBody = "Report # " & reportID & "<br>This report has been pre-approved. Please do a final review of this report and log-in to the " & appName & " system to do a final approval."
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  .AddAttachment (sAttach)
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

		bSent = True
	
	notifyAdmintoCertify = true

end function		


function updateLDADate(projectID,LDADate)
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")

	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spEditLDADate"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	   .parameters.Append .CreateParameter("@LDADate", adDBTimeStamp, adParamInput, 8, LDADate)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsLDA = oCmd.Execute
	set oCmd = nothing
	
	updateLDADate = "True"
end function


function checkInitialInspection(projectID)
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")

	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spCheckForInitialInspection"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc	   
	End With
				
	Set rsInit = oCmd.Execute
	set oCmd = nothing
	
	if rsInit.eof then
		'get the project
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetProject"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .CommandType = adCmdStoredProc	   
		End With
					
		Set rsProj = oCmd.Execute
		set oCmd = nothing
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
		'response.Write rsProj("projectName")
		
		'send email
		'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
		end with
		
		with oCdoMail
		  .From = "support@hbnext.com"
		  .To = "randy.wise@gatrans.com;dana.heil@gatrans.com"
		  .BCC = "rreynolds@hbnext.com"
		end with
		sSubject = "Initial Inspection for " & rsProj("projectName") & "  - " & date()
		sBody = "This project has had an initial inspection."
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If

		bSent = True
	end if
	
	checkInitialInspection = "True"
end function

%>