<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
	projectID = request("projectID")
	
	On Error Resume Next

	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")

	'Create command for customer list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProject"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc
	   
	End With
	
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
	sLat = trim(rs("latitude"))
	sLon = trim(rs("longitude"))
	sProject = trim(rs("projectName"))
	sCity = trim(rs("city"))
	sState = trim(rs("state"))
	'sCustomer = "The Company" 
	
	'get the customer
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomer"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("customerID"))
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsCustomer = oCmd.Execute
	Set oCmd = nothing
	
	sCustomer = trim(rsCustomer("customerName"))
%>

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Google Maps</title>
	<link rel="stylesheet" href="includes/main.css" type="text/css">
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAPQts5Wl9cGhcu2ISBde5TxSUTLAe3mm0zsWbW_Yk4-4OcxLVqRQevddqY6dGhXA5XRLMSOMBo_eLvA" type="text/javascript"></script>
  </head>
  <body onunload="GUnload()">

    <div id="map" style="width: 550px; height: 450px"></div>
    <noscript><b>JavaScript must be enabled in order for you to use Google Maps.</b> 
      However, it seems JavaScript is either disabled or not supported by your browser. 
      To view Google Maps, enable JavaScript by changing your browser options, and then 
      try again.
    </noscript>
 
 <%if sLat <> "" then%>

    <script type="text/javascript">
    //<![CDATA[
    
    if (GBrowserIsCompatible()) { 

      // A function to create the marker and set up the event window
      // Dont try to unroll this function. It has to be here for the function closure
      // Each instance of the function preserves the contends of a different instance
      // of the "marker" and "html" variables which will be needed later when the event triggers.    
      function createMarker(point,html) {
        var marker = new GMarker(point);
        GEvent.addListener(marker, "click", function() {
          marker.openInfoWindowHtml(html);
        });
		
		// The new marker "mouseover" listener        
        GEvent.addListener(marker,"mouseover", function() {
          marker.openInfoWindowHtml(html);
        });
        return marker;
      }

      // Display the map, with some controls and set the initial location 
      var map = new GMap2(document.getElementById("map"));
      map.addControl(new GLargeMapControl());
      map.addControl(new GMapTypeControl());
      map.setCenter(new GLatLng(<%=sLat%>,<%=sLon%>),13);
    
      // Set up three markers with info windows 
    
      var point = new GLatLng(<%=sLat%>,<%=sLon%>);
      var marker = createMarker(point,'<div style="width:240px"><font face="Arial, Helvetica, sans-serif"><%=sCustomer%> - <%=sProject%></font></div>')
      map.addOverlay(marker);



    }
    
    // display a warning if the browser was not compatible
    else {
      alert("Sorry, the Google Maps API is not compatible with this browser");
    }

    // This Javascript is based on code provided by the
    // Blackpool Community Church Javascript Team
    // http://www.commchurch.freeserve.co.uk/   
    // http://econym.googlepages.com/index.htm

    //]]>
    </script>
	
	
<%else%>	
	
	<br>
	<strong>
	This map shows the city and state we have on file.<br>
	To be more accurate, edit this project and enter the longitude and lititude in the NEXT application.
	</strong>
	<script type="text/javascript">
    //<![CDATA[
    
	
    if (GBrowserIsCompatible()) { 

      var map = new GMap(document.getElementById("map"));
      map.addControl(new GLargeMapControl());
      map.addControl(new GMapTypeControl());
      map.setCenter(new GLatLng(20,0),2);
      
      // ====== Create a Client Geocoder ======
      var geo = new GClientGeocoder(new GGeocodeCache()); 

      // ====== Array for decoding the failure codes ======
      var reasons=[];
      reasons[G_GEO_SUCCESS]            = "Success";
      reasons[G_GEO_MISSING_ADDRESS]    = "Missing Address: The address was either missing or had no value.";
      reasons[G_GEO_UNKNOWN_ADDRESS]    = "Unknown Address:  No corresponding geographic location could be found for the specified address.";
      reasons[G_GEO_UNAVAILABLE_ADDRESS]= "Unavailable Address:  The geocode for the given address cannot be returned due to legal or contractual reasons.";
      reasons[G_GEO_BAD_KEY]            = "Bad Key: The API key is either invalid or does not match the domain for which it was given";
      reasons[G_GEO_TOO_MANY_QUERIES]   = "Too Many Queries: The daily geocoding quota for this site has been exceeded.";
      reasons[G_GEO_SERVER_ERROR]       = "Server error: The geocoding request could not be successfully processed.";

      showAddress()
	  
      // ====== Geocoding ======
      function showAddress() {
        var search = '<%=sCity%>, <%=sState%>';
        // ====== Perform the Geocoding ======        
        geo.getLatLng(search, function (point)
          { 
            // ===== If that was successful, plot the point and centre the map ======
            if (point) {
              var marker = new GMarker(point);
              map.addOverlay(marker);
              map.setCenter(point,13);
            }
            // ====== Decode the error status ======
            else {
              // ==Look to see if the query was cached ==
              var result=geo.getCache().get(search);
              if (result) {
                var reason="Code "+result.Status.code;
                if (reasons[result.Status.code]) {
                  reason = reasons[result.Status.code]
                }
              } else {
                var reason = "";
              } 
              alert('Could not find "'+search+ '" ' + reason);
            }
          }
        );
      }
    }
    
    // display a warning if the browser was not compatible
    else {
      alert("Sorry, the Google Maps API is not compatible with this browser");
    }

    // This Javascript is based on code provided by the
    // Blackpool Community Church Javascript Team
    // http://www.commchurch.freeserve.co.uk/   
    // http://econym.googlepages.com/index.htm

    //]]>
    </script>
	
	
<%end if%>	
	
	
	
  </body>

</html>