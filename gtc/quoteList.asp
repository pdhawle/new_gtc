<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn
clientID = request("clientID")
archive = request("ar")
searchString = request("searchString")
sInit = request("init")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")

if sInit = "true" then
	'get the top 20 quotes
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetQuotesTop"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
else
	if searchString = "" then
		if archive = "true" then
			sp = "spGetArchivedQuotes"
		else
			sp = "spGetQuotes"
		end if
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = sp
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
		   .CommandType = adCmdStoredProc   
		End With
	else
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spSearchQuoteByProject"
		   .parameters.Append .CreateParameter("@searchString", adVarchar, adParamInput, 100, searchString)
		   .CommandType = adCmdStoredProc   
		End With
	end if		
end if
	
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClient"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsClient = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
<!-- Begin
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to archive this quote?")) {
    document.location = delUrl;
  }


}

function dept_onchange(quoteList) {
   document.quoteList.action = "quoteList.asp";
   quoteList.submit(); 
}
//  End -->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Inspection Quote List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				
				<!--this is the nav bar for the sub pages  -->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addQuote&clientID=<%=clientID%>" class="footerLink">build project/add quote</a>&nbsp;&nbsp;
						<%'if session("Admin") = "True" Then%>
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp?fp=quoteList" class="footerLink">customer list</a>&nbsp;&nbsp;
						<%'end if%>
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="expQuoteList.asp?clientID=<%=clientID%>" class="footerLink">export to excel</a>&nbsp;&nbsp;
						<!--<span class="footerLink">|</span>&nbsp;&nbsp;<a href="" class="footerLink">customer quotes report</a>&nbsp;&nbsp;-->
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="assignedCustomerList.asp?clientID=<%=clientID%>" class="footerLink">assigned customer list</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="nonAreaManagerList.asp?clientID=<%=clientID%>" class="footerLink">non-area manager list</a>&nbsp;&nbsp;
						<%if archive = "true" then%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="quoteList.asp?clientID=<%=clientID%>" class="footerLink">active quotes</a>&nbsp;&nbsp;
						<%else%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="quoteList.asp?ar=true&clientID=<%=clientID%>" class="footerLink">archived quotes</a>&nbsp;&nbsp;
						<%end if%>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="20"></td></tr>	
										<tr>
											<td colspan="20">
												<%if sInit = "true" then%>
													<span style="color:#EE0000"><strong>NOTE: </strong>Only the latest 20 records have been returned. Use the search feature or click the "view all quotes" link below to see more.</span><br><br>
												<%end if%>
											
												<strong>Note:</strong> The PDF of the quote will not be generated until you assign the customer.<br>
												Bid dates in <font color="#EE0000">red</font> have expired.<br>
												<img src="images/statusGreen.gif" align="awarded"> = have been awarded.<br>
												<img src="images/statusYellow.gif" alt="not awarded yet"> = have not been awarded yet.<br>
												<img src="images/statusBlue.gif" alt="contract won"> = contract won by <%=rsClient("clientName")%>.<br>
												<img src="images/statusRed.gif" alt="contract lost by <%=rsClient("clientName")%>"> = contract lost by <%=rsClient("clientName")%>.<br><br>
												<form action="quoteList.asp?clientID=<%=clientID%>" method="post" name="search">
												<b>Search Quotes By Project Name</b><br>
												<input type="text" name="searchString" size="30" maxlength="100" value="<%=request("searchString")%>">&nbsp;&nbsp;<input type="submit" value="Search" class="formButton"/>
												</form>
												<%if searchString <> "" or sInit = "true" then%>
													&nbsp;&nbsp;<a href="quoteList.asp?clientID=<%=clientID%>">view all quotes</a>
												<%end if%>
												<br><br>
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Quote ID</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Project Name</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Project Type</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Start Date</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Status</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Bid Date</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Date Created</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Created By</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td align="center"><span class="searchText">Customers Assigned</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td align="center"><span class="searchText">Action</span></td>
										</tr>
										<%if rs.eof then%>
											<tr><td colspan="10">There are currently no quotes</td></tr>
										<%else
											blnChange = true
											do until rs.eof
												'check to see if the bid date has passed
												'if so, make the data red
												If cdate(formatDateTime(rs("bidDate"),2)) < cdate(formatDateTime(now(),2)) then
													bPassed = "True"
												else
													bPassed = "False"
												end if
												
												
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td></td>
													<td><%=rs("quoteID")%></td>
													<td></td>
													<td>
														<%response.Write rs("projectName")%>
													</td>
													<td></td>
													<td>
														<%response.Write rs("projectType")%>
													</td>
													<td></td>
													<td>
														<%response.Write rs("jobStartDate")%>
													</td>
													<td></td>
													<td>
														<%'this is where the status icons will appear
														If rs("awarded") = "True" then%>
															<img src="images/statusGreen.gif" alt="awarded">
															<%select case rs("wonContract")
																case "False"%>
																	<img src="images/statusRed.gif" alt="contract lost by <%=rsClient("clientName")%>">
																<%case "True"%>
																	<img src="images/statusBlue.gif" alt="contract won by <%=rsClient("clientName")%>">
																<%case else%>
															<%end select%>
														<%else%>
															<img src="images/statusYellow.gif" alt="not awarded yet">
														<%end if%>
													</td>
													<td></td>
													<td>
														<%if bPassed = "True" then
															response.Write "<font color=#EE0000>" & formatDateTime(rs("bidDate"),2) & "</font>"
														else
															response.Write formatDateTime(rs("bidDate"),2)
														end if
														%>
													</td>
													<td></td>
													<td><%=formatDateTime(rs("dateCreated"),2)%></td>
													<td></td>
													<td><%=rs("createdBy")%></td>
													<td></td>
													<%'get the number of customers assigned to this quote
													
													Set oCmd = Server.CreateObject("ADODB.Command")
	
													With oCmd
													   .ActiveConnection = DataConn
													   .CommandText = "spGetQuotesAssigned"
													   .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, rs("quoteID"))
													   .CommandType = adCmdStoredProc   
													End With
															
													Set rsAssigned = oCmd.Execute
													Set oCmd = nothing
													%>
													<td align="center"><%=rsAssigned("numberAssigned")%></td>
													<td></td>
													<td align="center">														
														<a href="quoteCustomerList.asp?clientID=<%=clientID%>&quoteID=<%=rs("quoteID")%>"><img src="images/add.gif" alt="assign customer/generate quote" border="0"></a>&nbsp;
														<a href="form.asp?id=<%=rs("quoteID")%>&formType=editQuote&clientID=<%=clientID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;
														<a href="form.asp?formType=addQuoteComments&quoteID=<%=rs("quoteID")%>&clientID=<%=clientID%>"><img src="images/file.gif" alt="add/view comments" border="0"></a>&nbsp;
														<%if archive <> "true" then%>
														<input type="image" src="images/expl_up.gif" alt="Archive Quote" border="0" onClick="return confirmDelete('process.asp?processType=archiveQuote&quoteID=<%=rs("quoteID")%>&clientID=<%=clientID%>')">													
														<%end if%>
													</td>
												
												</tr>
												
										<%	rs.movenext
										if blnChange = true then
											blnChange = false
										else
											blnChange = true
										end if
										loop
										end if%>										
									</table>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>