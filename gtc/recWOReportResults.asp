<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

'iCustomerID = request("customer")
iDivisionID = request("division")
iProjectID = request("projectID")
openClosed = request("openClosed")
clientID = request("clientID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

if iProjectID = "All" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWorkOrdersByDivision" 'by division
	   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, openClosed)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
	
else
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWorkOrdersByProject" 'by project
	   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iProjectID)
	   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, openClosed)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
end if

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Work Order Reconciliation Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expRecWOReportResults.asp?customer=<%=iCustomerID%>&division=<%=iDivisionID%>&projectID=<%=iProjectID%>&openClosed=<%=openClosed%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="chooseRecWO.asp?clientID=<%=clientID%>" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="7">
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr bgcolor="#666666">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><span class="searchText">Work Order ID</span></td>
														<td><span class="searchText">Created By</span></td>
														<td><span class="searchText">Division</span></td>
														<td><span class="searchText">Project</span></td>
														<td><span class="searchText">Work Order Date</span></td>
														<td><span class="searchText">Work Order Amount</span></td>
													</tr>
											
													<%If rsReport.eof then %>
															<tr><td></td><td colspan="7">there are no reports to display</td></tr>
														<%else
															blnChange = false
															dim i
															i = 0
															Do until rsReport.eof
																If blnChange = true then%>
																	<tr class="rowColor">
																<%else%>
																	<tr>
																<%end if%>
																	<td></td>
																	<td><a href="downloads/workOrder_<%=rsReport("workOrderID")%>.pdf" target="_blank"><%=rsReport("workOrderID")%></a></td>
																	<td><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></td>
																	<td><%=rsReport("division")%></td>
																	<td><%=rsReport("projectName")%></td>
																	<td><%=rsReport("dateAdded")%></td>
																	<td>
																		<%
																		'get the total amount of the work order for this field
																		Set oCmd = Server.CreateObject("ADODB.Command")
																		With oCmd
																		   .ActiveConnection = DataConn
																		   .CommandText = "spGetWorkCost"
																		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("workOrderID"))
																		   .CommandType = adCmdStoredProc   
																		End With
																					
																		Set rsCost = oCmd.Execute
																		Set oCmd = nothing
																		
																		response.Write formatcurrency(rsCost("cost"),2)
																		%>
																	</td>
																</tr>
															<%
															
															rsReport.movenext
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
															i = i + 1
															loop%>
												
														<%end if%>
													
												</table>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>