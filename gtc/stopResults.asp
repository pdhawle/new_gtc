<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
dtFrom = request("fromDate")
dtTo = request("toDate")


If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStopsByDate"
   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Stop Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expStopResults.asp?clientID=<%=clientID%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="chooseStop.asp?clientID=<%=clientID%>" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr><td></td><td colspan="5">Date Range: <strong><%=dtFrom%> - <%=dtTo%></strong></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Customer</span></td>
											<td><span class="searchText">Project</span></td>
											<td><span class="searchText">Account Manager</span></td>
											<td><span class="searchText">Stop/End Date</span></td>
											<td width="10"></td>
											<td><span class="searchText">Primary Inspector</span></td>
											<td><span class="searchText">Monthly Rate</span></td>
											<!--<td width="10"></td>
											<td><span class="searchText">Billing Days</span></td>
											<td><span class="searchText">Prorated Amount</span></td>
											<td><span class="searchText">Total Billed</span></td>-->
											
										</tr>
								
										<%If rsReport.eof then %>
												<tr><td></td><td colspan="9">there are no projects to display</td></tr>
											<%else
												blnChange = false
												dim i
												i = 0
												irate = 0
												iFee = 0
												Do until rsReport.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td><a href="customerList.asp?id=<%=left(rsReport("customerName"),1)%>"><%=rsReport("customerName")%></a></td>
														<td><a href="form.asp?id=<%=rsReport("projectID")%>&formType=editProject&customerID=<%=rsReport("customerID")%>&divisionID=<%=rsReport("divisionID")%>"><%=rsReport("projectName")%></a></td>
														<td><%=rsReport("fName") & " " & rsReport("lName")%></td>
														<td><%=rsReport("endDate")%></td>
														<td></td>
														<td><%=rsReport("firstName") & " " & rsReport("lastName")%></td>
														<td>
															<%
															If not isnull(rsReport("billRate")) then
																irate = irate + rsReport("billRate")
															end if %>
															<a href="form.asp?id=<%=rsReport("projectID")%>&formType=editRates&customerID=<%=rsReport("customerID")%>&divisionID=<%=rsReport("divisionID")%>"><%=formatcurrency(rsReport("billRate"),2)%></a>
														</td>
														<!--<td width="10"></td>
														<td><%'=rsReport("newStartBillingDays")%></td>
														<td>
															<%'calculate the amount
														'	iProRate = (rsReport("billRate")/21)*rsReport("newStartBillingDays")
														'	response.Write formatcurrency(iProRate,2)
														'	If not isnull(iProRate) then
														'		iTotProRate = iTotProRate + iProRate
														'	end if
															
															%>
														</td>
														<td>
															<%'iBilled = iProRate + rsReport("activationFee") + rsReport("billRate")
														'	response.Write formatcurrency(iBilled,2)
														'	If not isnull(iBilled) then
														'		iBilledTotal = iBilledTotal + iBilled
														'	end if
															%>
														</td>-->
													</tr>
												<%
												
												rsReport.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												i = i + 1
												loop%>
												
												<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td colspan="7" align="right"><strong>Totals:</strong>&nbsp;&nbsp;</td>
													<td><%=formatcurrency(irate,2)%></td>
													<!--<td width="10"></td>
													<td>&nbsp;</td>
													<td><%'=formatcurrency(iTotProRate,2)%></td>
													<td><%'=formatcurrency(iBilledTotal,2)%></td>-->
												</tr>
												
											<%end if%>
										
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>