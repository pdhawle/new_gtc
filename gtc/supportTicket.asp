<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

supportTicketID = request("supportTicketID")
action = request("action")

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
'get the user's signature, if available
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
   .CommandType = adCmdStoredProc
   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript" src="includes/validator.js" type="text/javascript"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<!--#include virtual="/ASPSpellCheck/ASPSpellInclude.inc"-->
</head>
<body>
<form action="process.asp" method="post" name="myForm">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sSupportTitle%></span><span class="Header"> - Support Request</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="650" border="0" cellpadding="0" cellspacing="0">
										<%if action = "closeTicket" then
											'get the support ticket
											On Error Resume Next

											Set DataConn = Server.CreateObject("ADODB.Connection") 
											DataConn.Open Session("Connection"), Session("UserID")
											
											Set oCmd = Server.CreateObject("ADODB.Command")	
											With oCmd
											   .ActiveConnection = DataConn
											   .CommandText = "spGetSupportTicket"
											   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, supportTicketID)
											   .CommandType = adCmdStoredProc   
											End With
														
											Set rs = oCmd.Execute
											Set oCmd = nothing
											%>
											<tr>
												<td valign="top" align="right"><strong>Requestor:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<%=rs("firstName")%>&nbsp;<%=rs("lastName")%>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right" width="130"><strong>Date Requested:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<%=rs("dateAdded")%>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Importance:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<%=rs("importance")%>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Category:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<%=rs("category")%>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Message:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<%=rs("message")%>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Close Ticket:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="checkbox" name="closeTicket">
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Message:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<textarea name="smessage" id="smessage" cols="70" rows="10"><br /><br><%=rsUser("emailSignature")%></textarea>
													<script type="text/javascript">
														CKEDITOR.replace( 'smessage' );											
													</script><br />
													<% 
													dim myLink
													set myLink = new AspSpellLink
													myLink.fields="iframe:0" 'Sets the target Text Field(s) to be Spell-Checked
													response.write myLink.imageButtonHTML("","","")
													set myLink=nothing
													%>
												
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td colspan="3" align="right">
													<input type="hidden" name="supportTicketID" value="<%=supportTicketID%>" />
													<input type="hidden" name="processType" value="closeSupportTicket" />
													<input type="submit" value="Close Ticket/Send Message" class="formButton"/>
												</td>
											</tr>

										
										<%else%>
											<tr>
												<td><strong>From:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<%=session("name")%>&nbsp;(<%=session("email")%>)
													<input type="hidden" name="from" value="<%=session("email")%>" />
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td><strong>Importance:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<select name="importance">
														<option value="High">High (to be solved today)</option>							
														<option value="Medium">Medium (to be solved this week)</option>
														<option value="Low">Low (when the opportunity presents itself)</option>
													</select>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td><strong>Manager:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<select name="manager">
														<option value="Stormwater Manager">Stormwater Manager</option>							
														<option value="NOI Manager<">NOI Manager</option>
														<option value="Customer Manager">Customer Manager</option>
														<option value="Sales Manager">Sales Manager</option>
														<option value="HR Manager">HR Manager</option>
														<option value="Risk Manager">Risk Manager</option>
														<option value="Equipment Manager">Equipment Manager</option>
														<option value="Safety Manager">Safety Manager</option>
														<option value="Sequence Dashboard">Sequence Dashboard</option>
														<option value="Other">Other</option>
													</select>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td><strong>Category:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<select name="category">
														<option value="Application Error/Bug Report">Application Error/Bug Report</option>
														<option value="Assign Inspector">Assign Inspector</option>
														<option value="Client Request">Client Request</option>
														<option value="De-Activate Customer">De-Activate Customer</option>							
														<option value="Delete Report">Delete Report</option>
														<option value="Information Change">Information Change</option>
														<option value="New Customer">New Customer</option>
														<option value="New Project Start">New Project Start</option>
														<option value="Other">Other</option>
														<option value="Project End">Project End</option>
														<option value="Rate Change">Rate Change</option>
													</select>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>			
											<tr>
												<td valign="top"><strong>Message:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<textarea name="smessage" id="smessage" cols="70" rows="10"><br /><br><%=rsUser("emailSignature")%></textarea>
													<script type="text/javascript">
														CKEDITOR.replace( 'smessage' );											
													</script><br />
													<% 
													set myLink = new AspSpellLink
													myLink.fields="iframe:0" 'Sets the target Text Field(s) to be Spell-Checked
													response.write myLink.imageButtonHTML("","","")
													set myLink=nothing
													%>
												
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td colspan="3" align="right">
													<input type="hidden" name="processType" value="sendSupportRequest" />
													<input type="submit" value="Submit Request" class="formButton"/>
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td colspan="3">
													If you have an urgent need (system down, system error), please call 678-336-1357.
												</td>
											</tr>
										<%end if%>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>