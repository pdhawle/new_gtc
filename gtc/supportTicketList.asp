<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

sMode = request("mode")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")

select case sMode
	Case "closed"
		sp = "spGetClosedSupportTickets"
	Case else
		sp = "spGetOpenSupportTickets"
end select
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = sp
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this support ticket? This will also delete all correspondence regarding this ticket.")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Support Ticket List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						<%if sMode = "closed" then%>
						&nbsp;&nbsp;<a href="supportTicketList.asp" class="footerLink">open tickets</a>
						
						<%else%>
						&nbsp;&nbsp;<a href="supportTicketList.asp?mode=closed" class="footerLink">closed tickets</a>
						<%end if%>
					
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<%if sMode = "closed" then%>
											<tr bgcolor="#666666">
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td><span class="searchText">Ticket #</span></td>
												<td><span class="searchText">Date Requested</span></td>
												<td><span class="searchText">Requestor</span></td>
												<td><span class="searchText">Priority</span></td>
												<td><span class="searchText">Category</span></td>
												<td><span class="searchText">View Ticket</span></td>				
												<td><span class="searchText">Date Closed</span></td>
												<td align="center"><span class="searchText">Delete</span></td>
											</tr>
											<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<%If rs.eof then%>
												<tr><td></td><td colspan="10">there are no records to display</td></tr>
											<%else
												blnChange = true
												Do until rs.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><%=rs("supportTicketID")%></td>
														<td><%=rs("dateAdded")%></td>
														<td><a href="mailto:<%=rs("email")%>"><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></a></td>
														<td><%=rs("importance")%></td>
														<td><%=rs("category")%></td>
														<td><a href="supportTicketDetails.asp?supportTicketID=<%=rs("supportTicketID")%>" title="Support Ticket" rel="gb_page_center[820, 500]">view ticket</a></td>
														<td><%=rs("dateClosed")%></td>
														<td align="center"><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("supportTicketID")%>&processType=deleteSupportTicket')"></td>
													</tr>
												<%rs.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												loop
											end if%>
										<%else%>
											<tr bgcolor="#666666">
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td><span class="searchText">Ticket #</span></td>
												<td><span class="searchText">Date Requested</span></td>
												<td><span class="searchText">Requestor</span></td>
												<td><span class="searchText">Priority</span></td>
												<td><span class="searchText">Category</span></td>
												<td><span class="searchText">View Ticket</span></td>				
												<td><span class="searchText">Close Ticket/Send Message</span></td>
												<td align="center"><span class="searchText">Delete</span></td>
											</tr>
											<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<%If rs.eof then%>
												<tr><td></td><td colspan="10">there are no records to display</td></tr>
											<%else
												blnChange = true
												Do until rs.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><%=rs("supportTicketID")%></td>
														<td><%=rs("dateAdded")%></td>
														<td><a href="mailto:<%=rs("email")%>"><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></a></td>
														<td><%=rs("importance")%></td>
														<td><%=rs("category")%></td>
														<td><a href="supportTicketDetails.asp?supportTicketID=<%=rs("supportTicketID")%>" title="Support Ticket" rel="gb_page_center[820, 500]">view ticket</a></td>
														<td><a href="supportTicket.asp?action=closeTicket&supportTicketID=<%=rs("supportTicketID")%>">close ticket/send message</a></td>
														<td align="center"><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("supportTicketID")%>&processType=deleteSupportTicket')"></td>
													</tr>
												<%rs.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												loop
											end if%>
										<%end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>