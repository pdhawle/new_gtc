<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Templates</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><strong>Open and print templates</strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td>
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td><strong>ECGC Timesheet&nbsp;&nbsp;</strong></td>
														<td><a href="pdfTemplates/TimeSheetECGC.pdf" target="_blank"><img src="images/pdf.gif" border="0" align="PDF"></a></td>
													</tr>
													<tr><td colspan="2" height="5"></td></tr>
													<tr>
														<td><strong>HB NEXT Expense Form (Monthly)&nbsp;&nbsp;</strong></td>
														<td><a href="pdfTemplates/HBNEXTExpenseForm-(Monthly).pdf" target="_blank"><img src="images/pdf.gif" border="0" align="PDF"></a></td>
													</tr>
													<tr><td colspan="2" height="5"></td></tr>
													<tr>
														<td><strong>HB NEXT Expense Form (Weekly)&nbsp;&nbsp;</strong></td>
														<td><a href="pdfTemplates/HBNEXTExpenseForm-(Weekly).pdf" target="_blank"><img src="images/pdf.gif" border="0" align="PDF"></a></td>
													</tr>
													<tr><td colspan="2" height="5"></td></tr>
													<tr>
														<td><strong>HB NEXT Report Of Absence&nbsp;&nbsp;</strong></td>
														<td><a href="pdfTemplates/HBNEXTReportofAbsenceForm.pdf" target="_blank"><img src="images/pdf.gif" border="0" align="PDF"></a></td>
													</tr>
													<tr><td colspan="2" height="5"></td></tr>
													<tr>
														<td><strong>HB NEXT Site Inspection Form (Construction)&nbsp;&nbsp;</strong></td>
														<td><a href="pdfTemplates/HBNEXTSiteInspectionForm-Const.pdf" target="_blank"><img src="images/pdf.gif" border="0" align="PDF"></a></td>
													</tr>
													<tr><td colspan="2" height="5"></td></tr>
													<tr>
														<td><strong>Shaw Timesheet</strong></td>
														<td><a href="pdfTemplates/TimeSheetSHAW.pdf" target="_blank"><img src="images/pdf.gif" border="0" align="PDF"></a></td>
													</tr>
													
													
												</table>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>