<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

timeID = Request("timeID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetTime"
   .parameters.Append .CreateParameter("@timeID", adInteger, adParamInput, 8, timeID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@timeID", adInteger, adParamInput, 8, rs("userID"))
   .CommandType = adCmdStoredProc   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<form name="openItems" method="post" action="process.asp">
<table width="400" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<tr>
		<td colspan="3" class="colorBars">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table cellpadding="0" cellspacing="0" border="0">
										<tr><td valign="top"><strong>Employee Name:</strong>&nbsp;</td><td><%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></td></tr>
										<tr><td valign="top"><strong>Project:</strong> </td><td><%=rs("projectName")%></td></tr>
										<tr><td valign="top"><strong>Cost Code:</strong> </td><td><%=rs("code")%>&nbsp;-&nbsp;<%=rs("type")%></td></tr>
										<tr><td valign="top"><strong>Hours:</strong> </td><td><%=rs("hours")%></td></tr>
										<tr><td valign="top"><strong>Billable:</strong> </td><td><%=setYesNo(rs("billable"))%></td></tr>
										<tr><td valign="top"><strong>Comments:</strong> </td><td><%=rs("comments")%></td></tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>