<!--#include file="includes/constants.inc"-->
<%
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Tools List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												<a href="mileage.asp"><b>My Mileage</b></a> - Log mileage to calendar view.<br>
												<a href="timeSheet.asp"><b>My Timesheet</b></a> - Create and share an online time sheet.<br>
												<a href="calendar.asp"><b>My Calendar</b></a> - Create and share an online task scheduler and manager.<br>
												<a href="journal.asp"><b>My Blog/Journal</b></a> - Create and share an blog/journal.<br>
												<%if session("clientID") = 1 then'only for NEXT users%>
													<a href="weeklyProjSummary.asp"><b>My Weekly Project Summary</b></a> - Summarize the project week.<br>
												<%end if%>
												<!--<a href="">Query Closed Action Items</a>  Select and view reports with resolved <em>Action Required</em> items.<br>
												<a href="custReportsArchive.asp"><b>Archive Reports</b></a> - Select reports to download for archival purposes.  <br>
												<a href="monthlySummary.asp"><b>Site Inspection Monthly Summary Report</b></a> - View and download a monthy snapshot of a given project.  <br>-->
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>