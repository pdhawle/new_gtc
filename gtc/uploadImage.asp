<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
'projectID = trim(Request("project"))
'questionID= trim(Request("questionID"))
reportID = request("reportID")
divisionID=request("divisionID")



On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOpenItemsByReport"
	.parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
   .CommandType = adCmdStoredProc
   
End With

Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProjectByReport"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, reportID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsProject = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this file? This can NOT be undone.")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table align="center" width="900" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3" class="colorBars">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="20" height="1"></td>
		<td>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td></td><td colspan="10"><span class="grayHeader"><%=sNPDESTitle%> - View/Upload images</span></td>
				</tr>
				<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td></td>
					<td colspan="10">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<!--<tr><td colspan="15">Customer: <strong><%'=rsInfo("customerName")%></strong></td></tr>-->
							<tr><td colspan="15">Project: <strong><%=rsProject("projectName")%></strong></td></tr>
							<tr><td colspan="15">Report #: <strong><a href="downloads/swsir_<%=rs("reportID")%>.pdf" target="_blank"><%=reportID%></a></strong></td></tr>
							<tr><td colspan="15"><img src="images/pix.gif" width="1" height="10"></td></tr>	
							<tr>
								<td colspan="10">
									<strong>Add photos to report</strong>
								</td>
							</tr>
							<tr><td colspan="15"><img src="images/pix.gif" width="1" height="3"></td></tr>							
							<tr><td colspan="10"><a href="uploadImageAI.asp?reportID=<%=reportID%>&divisionID=<%=divisionID%>">upload image</a></td></tr>
							<tr>
								<td colspan="9"><br>
									<strong>Uploaded images to this report:</strong><br>
									<%'this is where we are listing the images
	
									'Dim dirname, mypath, fso, folder, filez, FileCount
									Set fso = CreateObject("Scripting.FileSystemObject")
									
									
									sPath = "downloads/project_" & rsProject("projectID")
									If not fso.FolderExists(server.mappath(sPath)) Then 
										'response.Write server.mappath(sPath)
										fso.CreateFolder(server.mappath(sPath))
									end if
									
									sPath = "downloads/project_" & rsProject("projectID") & "/report_" & reportID
									If not fso.FolderExists(server.mappath(sPath)) Then 
										'response.Write server.mappath(sPath)
										fso.CreateFolder(server.mappath(sPath))
									end if									
					
									Set folder = fso.GetFolder(server.mappath(sPath))
									'Response.Write folder
									Set filez = folder.Files			
									'FileCount = folder.Files.Count
									%>
									<%' Now To the Runtime code:
								'	Dim strPath 'Path of directory to show
								'	Dim objFSO 'FileSystemObject variable
								'	Dim objFolder 'Folder variable
								'	Dim objItem 'Variable used To Loop through the contents of the folder
									strPath = sPath & "/"
									
									'response.Write strPath & "<br>"
									'response.Write strPath
									' Create our FSO
									Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
									' Get a handle on our folder
					
									Set objFolder = objFSO.GetFolder(Server.MapPath(strPath))
									%>
									<table>
									<%i = 0
										For Each objItem In objFolder.Files
									'	Set oCmd = Server.CreateObject("ADODB.Command")
									'	With oCmd
									'	   .ActiveConnection = DataConn
									'	   .CommandText = "spCheckForCaption"
									'	   .parameters.Append .CreateParameter("@fileName", adVarchar, adParamInput, 100, objItem.Name)
									'	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, reportID)
									'	   .CommandType = adCmdStoredProc   
									'	End With
									'				
									'	Set rsCap = oCmd.Execute
									'	Set oCmd = nothing
									%>
										<tr>
											<td><input type="image" src="images/remove.gif" width="11" height="13" border="0" alt="Delete" onClick="return confirmDelete('filedelete.asp?file=<%=objItem.Name%>&imageID=<%=rsCap("imageID")%>&projectID=<%=rsProject("projectID")%>&reportID=<%=reportID%>&responsiveActionID=<%=rs("responsiveActionID")%>&divisionID=<%=divisionID%>')"></td>
											<td valign="top"><%response.Write "&nbsp;<a href=" & strPath&objItem.Name & " target=_blank>" & objItem.Name & "</a><br>"%></td>																	
											<td>
											<%'if not rsCap.rof then%>
												<%'=rsCap("caption")%>
											<%'end if%>
										</td>
										<!--<td><a href="edit_form_Caption.asp?fileName=<%'=trim(objItem.Name)%>&imageID=<%'=rsCap("imageID")%>&reportID=<%'=reportID%>&projectID=<%'=rsProject("projectID")%>&repType=<%'=repType%>&responsiveActionID=<%'=rs("responsiveActionID")%>&divisionID=<%'=divisionID%>">(edit caption)</a></td>-->
										</tr>
									<%i=i+1
									NEXT%>
									</table>
									<%
									
									'response.Write i & "<br>"
									
									If i=0 then
										response.Write "there are no images attached to this report"
									end if
									
									Set objItem = Nothing
									Set objFolder = Nothing
									Set objFSO = Nothing
									i=0
									%><br><br>
								</td>
							</tr>
							
							
							
							<tr><td colspan="15"><img src="images/pix.gif" width="1" height="3"></td></tr>
							<tr bgcolor="#666666">
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><span class="searchText">Report Date</span><br><img src="images/pix.gif" width="60" height="1"></td>
								<td><img src="images/pix.gif" width="10" height="1"></td>
								<td><span class="searchText"><a href="openItems.asp?sort=true&projectID=<%'=projectID%>&questionID=<%'=questionID%>&rte=<%'=rte%>" class="footerLink">Ref# (sort)</a></span></td>
								<td><img src="images/pix.gif" width="10" height="1"></td>
								<td><span class="searchText">Responsive Action Needed</span></td>
								<td><img src="images/pix.gif" width="10" height="1"></td>
								<td><span class="searchText">Location</span></td>
								<td><img src="images/pix.gif" width="10" height="1"></td>
								<td><span class="searchText">Action</span></td>
							</tr>
							<tr><td colspan="15"></td><td><img src="images/pix.gif" width="1" height="10"></td></tr>
							<%If rs.eof then%>
								<tr><td></td><td colspan="9">there are no responsive actions on this report. as a result, you cannot upload images</td></tr>
							<%else
								blnChange = true
								Do until rs.eof
									If blnChange = true then%>
										<tr class="rowColor">
									<%else%>
										<tr>
									<%end if%>
										<td><img src="images/pix.gif" width="5" height="1"></td>
										<td><%=rs("inspectionDate")%></td>
										<td></td>
										<td><%=rs("reportID") & "-" & rs("referenceNumber")%></td>
										<td></td>
										<td><%=rs("actionNeeded")%></td>
										<td></td>
										<td><%=rs("location")%></td>
										<td></td>
										<td><a href="uploadImageAI.asp?reportID=<%=reportID%>&responsiveActionID=<%=rs("responsiveActionID")%>&referenceNumber=<%=rs("referenceNumber")%>">upload image</a></td>
									</tr>
									<%If blnChange = true then%>
										<tr class="rowColor">
									<%else%>
										<tr>
									<%end if%>
										<td></td>
										<td colspan="9"><br>
											<strong>Uploaded images:</strong><br>
											<%'this is where we are listing the images
											
											'sPath = "downloads/project_" & rsProject("projectID") & "/report_" & reportID & "/responsiveAction_" & rs("responsiveActionID")
											'response.Write sPath & "<br>" 
			
											'Dim dirname, mypath, fso, folder, filez, FileCount
											Set fso = CreateObject("Scripting.FileSystemObject")
											
											
											sPath = "downloads/project_" & rsProject("projectID")
											If not fso.FolderExists(server.mappath(sPath)) Then 
												'response.Write server.mappath(sPath)
												fso.CreateFolder(server.mappath(sPath))
											end if
											
											sPath = "downloads/project_" & rsProject("projectID") & "/report_" & reportID
											If not fso.FolderExists(server.mappath(sPath)) Then 
												'response.Write server.mappath(sPath)
												fso.CreateFolder(server.mappath(sPath))
											end if
											
											sPath = "downloads/project_" & rsProject("projectID") & "/report_" & reportID & "/responsiveAction_" & rs("responsiveActionID")
											If not fso.FolderExists(server.mappath(sPath)) Then 
												'response.Write server.mappath(sPath)
												fso.CreateFolder(server.mappath(sPath))
											end if
											
							
											Set folder = fso.GetFolder(server.mappath(sPath))
											'Response.Write folder
											Set filez = folder.Files			
											'FileCount = folder.Files.Count
											%>
											<%' Now To the Runtime code:
											Dim strPath 'Path of directory to show
											Dim objFSO 'FileSystemObject variable
											Dim objFolder 'Folder variable
											Dim objItem 'Variable used To Loop through the contents of the folder
											strPath = sPath & "/"
											
											'response.Write strPath & "<br>"
											'response.Write strPath
											' Create our FSO
											Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
											' Get a handle on our folder
							
											Set objFolder = objFSO.GetFolder(Server.MapPath(strPath))
											%>
											<table>
											<%'i = 0
											For Each objItem In objFolder.Files
											'replaceText()%>
												<tr>
													<td><input type="image" src="images/remove.gif" width="11" height="13" border="0" alt="Delete" onClick="return confirmDelete('filedelete.asp?file=<%=objItem.Name%>&projectID=<%=rsProject("projectID")%>&reportID=<%=reportID%>&responsiveActionID=<%=rs("responsiveActionID")%>')"></td>
													<td valign="top"><%response.Write "&nbsp;<a href=" & strPath&objItem.Name & " target=_blank>" & objItem.Name & "</a><br>"%></td>																	
												</tr>
											<%i=i+1
											NEXT%>
											</table>
											<%
											
											'response.Write i & "<br>"
											
											If i=0 then
												response.Write "there are no images attached to this responsive action"
											end if
											
											Set objItem = Nothing
											Set objFolder = Nothing
											Set objFSO = Nothing
											i=0
											%><br><br>
										</td>
									</tr>
								<%
								
								rs.movenext
								
								if blnChange = true then
									blnChange = false
								else
									blnChange = true
								end if
								
								
								
								loop
							end if%>
							
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/pix.gif" width="20" height="1"></td>
	</tr>
	<tr bgcolor="#FFFFFF"><td colspan="3" height="20"></td></tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</body>
</html>
