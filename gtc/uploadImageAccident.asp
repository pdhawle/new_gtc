<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
'projectID = trim(Request("project"))
'questionID= trim(Request("questionID"))
accidentID = request("accidentID")



On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAccidentReport"
	.parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
   .CommandType = adCmdStoredProc
   
End With

Set rs = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this file? This can NOT be undone.")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table align="center" width="900" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3" class="colorBars">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="20" height="1"></td>
		<td>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td></td><td colspan="10"><span class="grayHeader"><%=sNPDESTitle%> - Upload Image(s)</span></td>
				</tr>
				<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td></td>
					<td colspan="10">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr><td colspan="15"><img src="images/pix.gif" width="1" height="3"></td></tr>
							<tr bgcolor="#666666">
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><span class="searchText">Created By</span></td>
								<td><img src="images/pix.gif" width="10" height="1"></td>
								<td><span class="searchText">Date Created</span></td>
								<td><img src="images/pix.gif" width="10" height="1"></td>
								<td><span class="searchText">Action</span></td>
							</tr>
							<tr><td colspan="15"></td><td><img src="images/pix.gif" width="1" height="10"></td></tr>
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></td>
								<td></td>
								<td><%=formatDateTime(rs("dateEntered"),2)%></td>
								<td></td>
								<td><a href="uploadImageAcc.asp?accidentID=<%=accidentID%>">upload image</a></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="9"><br>
									<strong>Uploaded images:</strong><br>
									<%'this is where we are listing the images
									
									'sPath = "downloads/project_" & rsProject("projectID") & "/report_" & reportID & "/responsiveAction_" & rs("responsiveActionID")
									'response.Write sPath & "<br>" 
	
									'Dim dirname, mypath, fso, folder, filez, FileCount
									Set fso = CreateObject("Scripting.FileSystemObject")
									
									
									sPath = "downloads/accident_" & accidentID
									If not fso.FolderExists(server.mappath(sPath)) Then 
										'response.Write server.mappath(sPath)
										fso.CreateFolder(server.mappath(sPath))
									end if									
					
									Set folder = fso.GetFolder(server.mappath(sPath))
									'Response.Write folder
									Set filez = folder.Files			
									'FileCount = folder.Files.Count
									%>
									<%' Now To the Runtime code:
									Dim strPath 'Path of directory to show
									Dim objFSO 'FileSystemObject variable
									Dim objFolder 'Folder variable
									Dim objItem 'Variable used To Loop through the contents of the folder
									strPath = sPath & "/"
									
									'response.Write strPath & "<br>"
									'response.Write strPath
									' Create our FSO
									Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
									' Get a handle on our folder
					
									Set objFolder = objFSO.GetFolder(Server.MapPath(strPath))
									%>
									<table>
									<%i = 0
									For Each objItem In objFolder.Files
									'replaceText()%>
										<tr>
											<td><input type="image" src="images/remove.gif" width="11" height="13" border="0" alt="Delete" onClick="return confirmDelete('filedelete.asp?file=<%=objItem.Name%>&projectID=<%=rsProject("projectID")%>&reportID=<%=reportID%>&responsiveActionID=<%=rs("responsiveActionID")%>')"></td>
											<td valign="top"><%response.Write "&nbsp;<a href=" & strPath&objItem.Name & " target=_blank>" & objItem.Name & "</a><br>"%></td>																	
										</tr>
									<%i=i+1
									NEXT%>
									</table>
									<%
									
									'response.Write i & "<br>"
									
									If i=0 then
										response.Write "there are no images attached to this accident report"
									end if
									
									Set objItem = Nothing
									Set objFolder = Nothing
									Set objFSO = Nothing
									i=0
									%><!--<br>
									there are no images attached to this responsive action--><br><br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/pix.gif" width="20" height="1"></td>
	</tr>
	<tr bgcolor="#FFFFFF"><td colspan="3" height="20"></td></tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</body>
</html>
