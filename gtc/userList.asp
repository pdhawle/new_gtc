<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
sPage = "userList"
sSearchString = request("search")
sLetter = request("id")


ssort = request("sort")
if ssort = "" then
	columnName = "company"
else
	columnName = ssort
end if




Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")

If sLetter = "" then
	sLetter = "@"
end if


if sSearchString <> "" then
	Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spSearchUsers"
		   .parameters.Append .CreateParameter("@id", adVarchar, adParamInput, 100, sSearchString)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		sLetter = "Search results for: " & sSearchString
		
else
	If session("superAdmin") = "True" Then
		Select Case sLetter
			Case "ALL"
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetUserListSort"
				   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
				   .CommandType = adCmdStoredProc   
				End With
			Case "inactive"
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetUserListInactiveSort"
				   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
				   .CommandType = adCmdStoredProc   
				End With
			Case "ARCHIVED"
				
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetUserListArchivedSort"
				   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
				   .CommandType = adCmdStoredProc   
				End With
		
			Case else
			
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetUserListByLetterSort"
				   .parameters.Append .CreateParameter("@letterID", adVarchar, adParamInput, 50, sLetter)
				   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
				   .CommandType = adCmdStoredProc   
				End With
		end Select
	else
		Select Case sLetter
			Case "ALL"
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetUserListForClientSort"
				   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
				   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
				   .CommandType = adCmdStoredProc   
				End With
			Case "inactive"
						With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetUserListInactiveForClientSort"
				   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
				   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
				   .CommandType = adCmdStoredProc   
				End With
		
			Case else
			
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetUserListByLetterForClient"
				   .parameters.Append .CreateParameter("@letterID", adVarchar, adParamInput, 50, sLetter)
				   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
				   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
				   .CommandType = adCmdStoredProc   
				End With
		end Select
	end if
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
end if
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to archive this user?")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>							
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - User List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form_addUser.asp?clientID=<%=session("clientID")%>" class="footerLink">add user</a>					
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td></td><td colspan="10"><!--#include file="includes/pageNav.asp"-->&nbsp;(sorted by company name)</td></tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td></td>
											<td colspan="8">
												<form name="customerList" method="post" action="userList.asp">
													<input type="text" name="search" value="<%=sSearchString%>">&nbsp;&nbsp;<input type="submit" name="searchBtn" value="Search" class="formButton">
												</form>										
											</td>
										</tr>
										<tr><td></td><td colspan="20"><strong><%if sLetter <> "@" then response.Write sLetter%></strong></td></tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr bgcolor="#666666" height="35">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText"><a href="userList.asp?id=<%=sLetter%>&sort=lastName&search=<%=sSearchString%>" style="color:#721A32;text-decoration:underline;font-weight:bold;">Name</a></span></td>
											<td><span class="searchText"><a href="userList.asp?id=<%=sLetter%>&sort=company&search=<%=sSearchString%>" style="color:#721A32;text-decoration:underline;font-weight:bold;">Company</a></span></td>
											<td><span class="searchText"><a href="userList.asp?id=<%=sLetter%>&sort=email&search=<%=sSearchString%>" style="color:#721A32;text-decoration:underline;font-weight:bold;">Email</a></span></td>
											<td><span class="searchText">Supervisor</span>&nbsp;&nbsp;</td>
											<td align="center"><span class="searchText">Edit</span></td>
											<td align="center"><span class="searchText">Create/Edit</span></td>
											<td align="center"><span class="searchText">Read Only</span></td>
											<td align="center"><span class="searchText">Admin</span></td>
											<td align="center"><span class="searchText">Active</span></td>
											<%if Session("superAdmin") = "True" then%>
												<td align="center"><span class="searchText">Archive</span></td>
											<%end if%>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="10">there are no customers that start with this letter</td></tr>
										<%else
											blnChange = true
											i = 0
											Do until rs.eof
												i = i+ 1
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("firstName") & " " & rs("lastName")%></td>
													<td><%=rs("company")%></td>
													<td><a href="mailto:<%=rs("email")%>"><%=rs("email")%></a></td>
													<td><a href="supervisorList.asp?userID=<%=rs("userID")%>">View</a></td>
													<td align="center"><a href="form.asp?id=<%=rs("userID")%>&formType=editUser"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a></td>
													<td align="center">
														<%If rs("inspector") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%end if%>	
													</td>
													<td align="center">
														<%If rs("customerAdmin") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%end if%>	
													</td>						
													<td align="center">
														<%If rs("isAdmin") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%end if%>	
													</td>
													<td align="center">	
														<%If rs("isActive") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%else%>
															<a href="process.asp?processType=activateUser&id=<%=rs("userID")%>&email=<%=rs("email")%>">activate</a>
														<%end if%>						
														
													</td>
													
													<%if Session("superAdmin") = "True" then
													'only if the user is not active
													If rs("isActive") <> "True" Then%>
														<td align="center">	
															<%If rs("isArchived") = "True" Then%>
																<input type="image" src="images/expl_up.gif" alt="Un-Archive User" border="0" onClick="return confirmDelete('process.asp?processType=archiveUser&userID=<%=rs("userID")%>&sLetter=<%=sLetter%>&archive=False')">
															<%else%>
																<input type="image" src="images/expl_up.gif" alt="Archive User" border="0" onClick="return confirmDelete('process.asp?processType=archiveUser&userID=<%=rs("userID")%>&sLetter=<%=sLetter%>&archive=True')">
															<%end if%>
														</td>
													<%else%>
														<td>&nbsp;</td>
													<%end if%>
													<%end if%>
												</tr>
											<%rs.movenext
											
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>