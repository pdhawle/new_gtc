<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn
'response.Write session("canApprove")
projectID = Request("projectID")
clientID = request("clientID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedProjects"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsProjects = oCmd.Execute
Set oCmd = nothing
'response.Write projectID
if projectID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWorkOrders"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
end if

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
<!-- Begin
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this work order?")) {
    document.location = delUrl;
  }


}


function dept_onchange(workOrderList) {
   document.workOrderList.action = "workOrdersList.asp";
   workOrderList.submit(); 
}
//  End -->
</script>
</head>
<body>
<form name="workOrderList" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Work Orders List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<%if projectID <> "" then%>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">						
						&nbsp;&nbsp;<a href="form.asp?formType=addWorkOrder&projectID=<%=projectID%>&bNew=True&clientID=<%=clientID%>" class="footerLink">generate new work order</a>	&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="closedWorkOrdersList.asp?projectID=<%=projectID%>&clientID=<%=clientID%>" class="footerLink">view closed work orders</a>&nbsp;&nbsp;
						<!--view all the approved work order items-->
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="approvedWorkOrderItemsList.asp?projectID=<%=projectID%>&clientID=<%=clientID%>" class="footerLink" title="Work Order Items List" rel="gb_page_fs[]">view approved work order items</a>&nbsp;&nbsp;
						<!--view all the processed work orders-->
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="processedWorkOrdersList.asp?projectID=<%=projectID%>&clientID=<%=clientID%>" class="footerLink">view processed work orders</a>&nbsp;&nbsp;
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<%end if%>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td colspan="20">
												<!--get a dropdown of all of the assigned projects for the user-->
													Please select a project to view and print work orders.<br>
													<select name="projectID" onChange="return dept_onchange(workOrderList)">
														<option value="">--Select Project--</option>
														<%do until rsProjects.eof
														if trim(rsProjects("projectID")) = trim(projectID) then%>
															<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
														<%else%>
															<option value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
														<%end if
														rsProjects.movenext
														loop%>
													</select>
											</td>
										</tr>
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="20"></td></tr>	
										<tr>
											<td colspan="20">
												If work order has been approved, you will be able to print the PDF and close the work order. <br>If 
												the work order has not been approved, a user that has approval rights will need to approve the work order.<BR>
												By closing the work order, all of the corresponding open items will be closed.
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">ID</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<!--<td><span class="searchText">Project</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>-->
											<td><span class="searchText">Date Created</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Created By</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td align="center"><span class="searchText">Action</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td align="center"><span class="searchText">Approved</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<%'if rs("isApproved") = "True" Then 'close the item%>
												<!--<td align="center"><span class="searchText">Close</span></td>
												<td><img src="images/pix.gif" width="10" height="1"></td>
												<td><span class="searchText">Date Closed</span></td>
												<td><img src="images/pix.gif" width="10" height="1"></td>
												<td><span class="searchText">Closed By</span></td>
											<%'else 'approve the item%>
											<td align="center"><span class="searchText">Approve</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Date Approved</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Approved By</span></td>-->
											<%'end if%>
											<td></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<%if projectID <> "" then
											if rs.eof then%>
												<tr><td colspan="20">&nbsp;There are no work orders for this project.</td></tr>
											<%else
												blnChange = true
												do until rs.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
													<td></td>
													<td><%=rs("workOrderID")%></td>
													<!--<td></td>
													<td><%'=rs("projectName")%></td>-->
													<td></td>
													<td><%=formatDateTime(rs("dateAdded"),2)%></td>
													<td></td>
													<td><%=rs("firstName") & " " & rs("lastName")%></td>
													<td></td>
													<%if rs("isApproved") = "True" Then%>
														<td align="center"><a href="downloads/workOrder_<%=rs("workOrderID")%>.pdf" target="_blank">view/print</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="form.asp?formType=sendEmail&workOrderID=<%=rs("workOrderID")%>&projectID=<%=projectID%>&bNew=True&clientID=<%=clientID%>&projName=<%=rs("projectName")%>&custName=<%=rs("customerName")%>">email</td>
													<%else%>
														<td align="center">
															<a href="workOrderDetails.asp?workOrderID=<%=rs("workOrderID")%>" title="Work Order # <%=rs("workOrderID")%>" rel="gb_page_fs[]">preview</a>&nbsp;|&nbsp;<a href="workOrderItemsList.asp?projectID=<%=projectID%>&clientID=<%=clientID%>&workOrderID=<%=rs("workOrderID")%>&projName=<%=rs("projectName")%>">Approve Items</a>
															<%if Session("deleteWorkOrder") = "True" then%>
															<br><a href="#" onClick="return confirmDelete('process.asp?id=<%=rs("workOrderID")%>&processType=deleteWorkOrder&projectID=<%=projectID%>&clientID=<%=clientID%>')">delete</a>
															<%end if%>
														</td>
													<%end if%>
													<td></td>
													<%if rs("isApproved") = "True" Then%>
														<td align="center"><strong>Yes</strong></td>
														<td></td>
														<%if session("closeWorkOrder") = "True" then%>
															<td align="center" valign="top">
																<strong>Close</strong><br>
																<input type="checkbox" name="closeWorkOrder" value="<%=rs("workOrderID")%>">
															</td>
															<td></td>
															<td valign="top">
																<strong>Date Closed</strong><br>
																<input type="text" name="dateClosed<%=rs("workOrderID")%>" maxlength="10" size="8"/>&nbsp;<a href="javascript:displayDatePicker('dateClosed<%=rs("workOrderID")%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
															</td>
															<td></td>
															<td valign="top">
																<%
																'Create command for contacts
																'brings in the contacts from the project section where they are set up
																Set oCmd = Server.CreateObject("ADODB.Command")
																
																With oCmd
																   .ActiveConnection = DataConn
																   .CommandText = "spGetAssignedUsers"
																	.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
																   .CommandType = adCmdStoredProc
																   
																End With
																
																	If Err Then
																%>
																		<!--#include file="includes/FatalError.inc"-->
																<%
																	End If
																			
																Set rsUsers = oCmd.Execute
																Set oCmd = nothing
																%>
																<strong>Closed By</strong><br>
																<select name="closedBy<%=rs("workOrderID")%>">
																		<option value="<%=session("Name")%>"><%=session("Name")%></option>
																	<%do until rsUsers.eof%>										
																		<option value="<%=rsUsers("firstName") & " " & rsUsers("lastName")%>"><%=rsUsers("firstName") & " " & rsUsers("lastName")%></option>
																	<%rsUsers.movenext
																	loop%>
																</select>
															</td>
														<%else%>
															<td colspan="5">you are not allowed to close work orders</td>
														<%end if%>
													<%else
														if session("canApprove") = "True" then%>
															
															<td align="center"><strong>NO</strong></td>
															<td></td>
															<td colspan="5" align="center">work orders must be approved before they can be closed</td>
														<%else%>
															<td colspan="7">you do not have approval rights</td>
													<%end if
													
													end if%>
												</tr>
											
												<%
												'get the values from all the checkboxes
												sAllVal = sAllVal & rs("workOrderID")
												
												rs.movenext
												
												if not rs.eof then
													sAllVal = sAllVal & ","
												end if
												
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												loop
											end if%>
										<%end if%>
										
										<!--<tr>
											<td></td>
											<td colspan="25">
												Please select the people you want to send the approval notice to:<br>
												<%'Create command for contacts
												'brings in the contacts from the project section where they are set up
											'	Set oCmd = Server.CreateObject("ADODB.Command")
												
											'	With oCmd
											'	   .ActiveConnection = DataConn
											'	   .CommandText = "spGetContactsByProject"
											'		.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
											'	   .CommandType = adCmdStoredProc
											'	   
											'	End With

															
											'	Set rsContacts = oCmd.Execute
											'	Set oCmd = nothing
												
												'get the contacts assigned to this project
											'	do until rsContacts.eof%>
													<input type="checkbox" checked="checked">&nbsp;<a href="mailto:<%'=rsContacts("email")%>"><%'=rsContacts("email")%></a><br>
												<%'rsContacts.movenext
												'loop%>
												<br>
												<a href="">add contacts</a>
											</td>
										</tr>-->
										
										<tr>
											<td colspan="25" align="right">
												<input type="hidden" name="clientID" value="<%=clientID%>">
												<input type="hidden" name="allBoxes" value="<%=sAllVal%>">
												<input type="hidden" name="processType" value="approveCloseWorkOrders" />
												<br><input type="submit" value="Update" class="formButton"/>
											</td>
										</tr>
										
									</table>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>